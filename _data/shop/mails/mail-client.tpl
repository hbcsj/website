<div style="font-family=Calibri, Verdana, Arial; width: 100%;">
    Bonjour %PRENOM%<br><br>Vous avez pass&eacute; la commande suivante :<br><br>
    NOM : %NOM%<br>
    PRENOM : %PRENOM%<br>
    EMAIL : %EMAIL%<br>
    TELEPHONE : %TELEPHONE%<br>
    MESSAGE : <b>%MESSAGE%</b>
    <br><br>
    <table style="margin-top: 20px;">
        <tr>
            <th colspan="2" style="padding: 3px; border-bottom: 1px solid #dddddd;">Article</th>
            <th style="text-align: center; padding: 3px; border-bottom: 1px solid #dddddd;">Quantit&eacute;</th>
            <th style="text-align: center; padding: 3px; border-bottom: 1px solid #dddddd;">Prix</th>
        </tr>
        %LIGNES_COMMANDE%
        <tr>
            <td colspan="3" style="padding: 3px; border-bottom: 1px solid #dddddd;"><b>TOTAL</b></td>
            <td style="text-align: center; padding: 3px; border-bottom: 1px solid #dddddd;"><b>%MONTANT_TOTAL% &euro;</b></td>
        </tr>
    </table>
    <br><br>
    Elle a &eacute;t&eacute; enregistr&eacute;e sous la r&eacute;f&eacute;rence <b>%REF_COMMANDE%</b> et sera transmise &agrave; notre fournisseur. Nous vous la remettrons une fois r&eacute;ceptionn&eacute;e.<br><br>
    Pour le paiement (%MONTANT_TOTAL% &euro;), vous pouvez :<br>
    - payer en monnaie<br>
    - payer par ch&egrave;que (&agrave; l'ordre du HBC Saint Jean)<br>
    - faire un virement au RIB dont voici les coordonn&eacute;es :<br>
    Nom : %NOM_RIB_HBCSJ%<br>
    IBAN : %IBAN_RIB_HBCSJ%<br>
    BIC / Swift : %BIC_SWIFT_RIB_HBCSJ%<br><br>
    <b>
        Si vous r&eacute;glez par monnaie ou ch&egrave;que, veuillez :<br>
        - tout regrouper dans une enveloppe<br>
        - y indiquer la r&eacute;f&eacute;rence '%REF_COMMANDE%'<br>
        - ou simplement joindre ce mail imprim&eacute;<br><br>
        ceci afin d'assurer une bonne tra&ccedil;abilit&eacute; du paiement.
    </b><br><br>
    <b>
        Si vous r&eacute;glez par virement, pensez &agrave; envoyer un mail / SMS pour pr&eacute;ciser qu'il a &eacute;t&eacute; effectu&eacute;.
    </b><br><br>
	<b>
		Cette commande doit donner lieu &agrave; un accus&eacute; de r&eacute;ception de notre part (un simple mail). Si vous n'avez rien re&ccedil;u d'ici une journ&eacute;e, relancez-nous, car il y a pu avoir 
		un probl&egrave;me.
	</b><br><br>
    Recevez nos plus sportives salutations,<br><br>L'&eacute;quipe de la boutique du HBC Saint Jean,<br>
    %BOUTIQUE_EMAIL%
</div>