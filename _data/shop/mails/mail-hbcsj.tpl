<div style="font-family=Calibri, Verdana, Arial; width: 100%;">
    Bonjour,<br><br>La commande suivante vient d'&ecirc;tre pass&eacute;e :<br><br>
    NOM : %NOM%<br>
    PRENOM : %PRENOM%<br>
    EMAIL : %EMAIL%<br>
    TELEPHONE : %TELEPHONE%<br>
    MESSAGE : <b>%MESSAGE%</b>
    <br><br>
    <table style="margin-top: 20px;">
        <tr>
            <th colspan="2" style="padding: 3px; border-bottom: 1px solid #dddddd;">Article</th>
            <th style="text-align: center; padding: 3px; border-bottom: 1px solid #dddddd;">Quantit&eacute;</th>
            <th style="text-align: center; padding: 3px; border-bottom: 1px solid #dddddd;">Prix</th>
        </tr>
        %LIGNES_COMMANDE%
        <tr>
            <td colspan="3" style="padding: 3px; border-bottom: 1px solid #dddddd;"><b>TOTAL</b></td>
            <td style="text-align: center; padding: 3px; border-bottom: 1px solid #dddddd;"><b>%MONTANT_TOTAL% &euro;</b></td>
        </tr>
    </table>
    <br><br>
    Merci d'enregistrer cette commande (R&eacute;f <b>%REF_COMMANDE%</b>)
</div>
