:toc:

= http://www.hbc-saintjean.fr[Site du HBC Saint-Jean] - Déploiement en ligne

== Récupération des "node_modules"

Les "*node modules*" contiennent les bibliothèques nécessaires à la construction du site déployable en ligne (minification des fichiers JavaScript, transformation des fichiers LESS en fichiers CSS, "uglification" des fichiers CSS...).

Dans le dossier `www` du serveur Apache, cloner https://gitlab.com/hbcsj/node_modules.git[le projet Git "node_modules"] dans un dossier appelé par exemple `node_modules`.

== Vérification du fichier "htaccess"

Le fichier "*.htaccess*" qui sera déployé en ligne est `-htaccess.prod`.

Avant de déployer, modifier le contenu de ce fichier si nécessaire.

== Vérification de la configuration

Le fichier de configuration qui sera déployé en ligne est `src/conf/config.ini.prod`.

Avant de déployer, modifier le contenu de ce fichier si nécessaire.

== Lancement du programme "build"

Lancer le script `build.cmd`, qui va produire un dossier `target` à la racine de `hbcsj-site`, contenant l'intégralité des souces du site déployable.

[TIP]
====
Au préalable, vérifier que le `PHP_PROGRAM` configuré dans `build.cmd` est bien valide.
====

== Déploiement en ligne

Avec un client FTP :

* se connecter à ftp.hbc-saintjean.fr
* aller dans le dossier `public_html`
* remplacer l'ensemble des dossiers et fichiers contenus dans `target`