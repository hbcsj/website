ALTER TABLE `hbcsj_categorie`
ADD COLUMN `position_affichage` int(11) NOT NULL DEFAULT 1;

CREATE INDEX `cat_idx_position_affichage`
 ON `hbcsj_categorie`
 ( `position_affichage` );

UPDATE `hbcsj_categorie` SET `position_affichage` = `id`;
UPDATE `hbcsj_categorie` SET `position_affichage` = (`position_affichage` + 2) WHERE `id` > 2;
UPDATE `hbcsj_categorie` SET `position_affichage` = (`position_affichage` + 2) WHERE `id` > 4;

INSERT INTO `hbcsj_categorie` (`nom`, `abreviation`, `nb_equipes`, `regroupement_matchs`, `position_affichage`)
VALUES ('-20 Gar&ccedil;ons', '20G', 0, 0, 3);
INSERT INTO `hbcsj_categorie` (`nom`, `abreviation`, `nb_equipes`, `regroupement_matchs`, `position_affichage`)
VALUES ('-20 Filles', '20F', 0, 0, 4);
INSERT INTO `hbcsj_categorie` (`nom`, `abreviation`, `nb_equipes`, `regroupement_matchs`, `position_affichage`)
VALUES ('-17 Gar&ccedil;ons', '17G', 0, 0, 7);
INSERT INTO `hbcsj_categorie` (`nom`, `abreviation`, `nb_equipes`, `regroupement_matchs`, `position_affichage`)
VALUES ('-17 Filles', '17F', 0, 0, 8);