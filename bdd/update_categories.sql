update hbcsj_licencie_joue_dans_categorie 
set hbcsj_licencie_joue_dans_categorie.categorie_id = <new_categorie_id> 
where hbcsj_licencie_joue_dans_categorie.licencie_id in (
    select id from hbcsj_licencie 
    where hbcsj_licencie_joue_dans_categorie.categorie_id = <old_categorie_id>
    and date_format(hbcsj_licencie.date_naissance, '%Y') = '<annee_de_naissance>' 
    and hbcsj_licencie.id = hbcsj_licencie_joue_dans_categorie.licencie_id
)