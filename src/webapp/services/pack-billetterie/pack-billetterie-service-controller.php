<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/dao/pack-billetterie-dao.php");

class PackBilletterieServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("pack-billetterie", true);
    }

    public function save() {
        $this->logNavigation("save", array(
            "id" => $_GET["id"],
            "nom" => $_POST["nom"],
            "nbPlaces" => $_POST["nbPlaces"],
            "prix" => $_POST["prix"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(POST, "nom"),
                array(POST, "nbPlaces"),
                array(POST, "prix")
            ));
            if ($checkParams) {
                $getId = $_GET["id"];
                $postNom = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["nom"]));
                $postNbPlaces = trim($_POST["nbPlaces"]);
                $postPrix = trim($_POST["prix"]);

                $packBilletterieDAO = new PackBilletterieDAO($this->getDatabaseConnection());

                $packBilletterie = array();
                if (HTTPUtils::paramExists(GET, "id")) {
                    $packBilletterieById = $packBilletterieDAO->getById($getId);
                    if ($packBilletterieById != null) {
                        $packBilletterie = $packBilletterieById;
                    } else {
                        $response->success = false;
                        $this->sendCheckError(HTTP_404, "Le pack billetterie (idPackBilletterie = '".$getId."') n'existe pas");
                    }
                }

                $packBilletterie[PACK_BILLETTERIE_NOM] = $postNom;
                $packBilletterie[PACK_BILLETTERIE_NB_PLACES] = $postNbPlaces;
                $packBilletterie[PACK_BILLETTERIE_PRIX] = $postPrix;

                $actionType = ACTION_TYPE_CREATION;
                if (array_key_exists(PACK_BILLETTERIE_ID, $packBilletterie)) {
                    $actionType = ACTION_TYPE_MODIFICATION;
                    $packBilletterieDAO->update($packBilletterie);
                } else {
                    $packBilletterie[PACK_BILLETTERIE_ID] = $packBilletterieDAO->create($packBilletterie);
                }

                if (!isAdminConnected_superadmin()) {
                    MailUtils::notifySuperadmin(
                        PACK_BILLETTERIE_TABLE_NAME, 
                        $actionType, 
                        array(
                            array(
                                OBJET_ASSOCIE_TYPE => PACK_BILLETTERIE_TABLE_NAME,
                                OBJET_ASSOCIE_ID => $packBilletterie[PACK_BILLETTERIE_ID],
                                OBJET_ASSOCIE_LABEL => $packBilletterie[PACK_BILLETTERIE_NOM]
                            )
                        ), 
                        $this->getDatabaseConnection()
                    );
                }

                $response->success = true;
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function delete() {
        $this->logNavigation("delete", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                require_once("common/php/dao/commande-item-billetterie-dao.php");

                $postId = trim($_POST["id"]);

                $packBilletterieDAO = new PackBilletterieDAO($this->getDatabaseConnection());
                $commandeItemBilletterieDAO = new CommandeItemBilletterieDAO($this->getDatabaseConnection());
            
                $packBilletterie = $packBilletterieDAO->getById($postId);
                if ($packBilletterie != null) {
                    if (sizeof($commandeItemBilletterieDAO->getByPackBilletterieId($postId)) == 0) {
                        $packBilletterieDAO->delete($postId);

                        if (!isAdminConnected_superadmin()) {
                            MailUtils::notifySuperadmin(
                                PACK_BILLETTERIE_TABLE_NAME, 
                                ACTION_TYPE_SUPPRESSION, 
                                array(
                                    array(
                                        OBJET_ASSOCIE_TYPE => PACK_BILLETTERIE_TABLE_NAME,
                                        OBJET_ASSOCIE_ID => $packBilletterie[PACK_BILLETTERIE_ID],
                                        OBJET_ASSOCIE_LABEL => $packBilletterie[PACK_BILLETTERIE_NOM]
                                    )
                                ), 
                                $this->getDatabaseConnection()
                            );
                        }

                        $response->success = true;
                    } else {
                        $response->success = false;
                        $this->sendCheckError(HTTP_400, "Le pack billetterie (idPackBilletterie = '".$postId."') a des commandes associees");
                    }
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le pack billetterie (idPackBilletterie = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}