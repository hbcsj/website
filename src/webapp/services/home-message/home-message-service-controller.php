<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/dao/home-message-dao.php");
require_once("common/php/dao/criticite-dao.php");

class HomeMessageServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("home-message", true);
    }

    public function save() {
        $this->logNavigation("save", array(
            "id" => $_GET["id"],
            "texte" => $_POST["texte"],
            "criticiteId" => $_POST["criticiteId"]
        ));

        $response = new stdClass;

        if (isAdminConnected_editeur()) {
            $checkParams = $this->checkParams(array(
                array(POST, "texte"),
                array(POST, "criticiteId")
            ));
            if ($checkParams) {
                require_once("common/php/dao/criticite-dao.php");

                $getId = $_GET["id"];
                $postTexte = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["texte"]));
                $postCriticiteId = trim($_POST["criticiteId"]);

                $homeMessageDAO = new HomeMessageDAO($this->getDatabaseConnection());

                $homeMessage = array();
                if (HTTPUtils::paramExists(GET, "id")) {
                    $homeMessageById = $homeMessageDAO->getById($getId);
                    if ($homeMessageById != null) {
                        $homeMessage = $homeMessageById;
                    } else {
                        $response->success = false;
                        $this->sendCheckError(HTTP_404, "Le pack billetterie (idHomeMessage = '".$getId."') n'existe pas");
                    }
                }

                $criticiteDAO = new CriticiteDAO($this->getDatabaseConnection());
                if ($criticiteDAO->getById($postCriticiteId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le criticite (idCriticite = '".$postCriticiteId."') n'existe pas");
                }

                $homeMessage[HOME_MESSAGE_TEXTE] = $postTexte;
                $homeMessage[HOME_MESSAGE_CRITICITE_ID] = $postCriticiteId;

                $actionType = ACTION_TYPE_CREATION;
                if (array_key_exists(HOME_MESSAGE_ID, $homeMessage)) {
                    $actionType = ACTION_TYPE_MODIFICATION;
                    $homeMessageDAO->update($homeMessage);
                } else {
                    $homeMessage[HOME_MESSAGE_ID] = $homeMessageDAO->create($homeMessage);
                }

                if (!isAdminConnected_superadmin()) {
                    MailUtils::notifySuperadmin(
                        HOME_MESSAGE_TABLE_NAME, 
                        $actionType, 
                        array(
                            array(
                                OBJET_ASSOCIE_TYPE => HOME_MESSAGE_TABLE_NAME,
                                OBJET_ASSOCIE_ID => $homeMessage[HOME_MESSAGE_ID],
                                OBJET_ASSOCIE_LABEL => $homeMessage[HOME_MESSAGE_TEXTE]
                            )
                        ), 
                        $this->getDatabaseConnection()
                    );
                }

                $response->success = true;
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function delete() {
        $this->logNavigation("delete", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_editeur()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                $postId = trim($_POST["id"]);

                $homeMessageDAO = new HomeMessageDAO($this->getDatabaseConnection());
            
                $homeMessage = $homeMessageDAO->getById($postId);
                if ($homeMessage != null) {
                    $homeMessageDAO->delete($postId);

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            HOME_MESSAGE_TABLE_NAME, 
                            ACTION_TYPE_SUPPRESSION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => HOME_MESSAGE_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $homeMessage[HOME_MESSAGE_ID],
                                    OBJET_ASSOCIE_LABEL => $homeMessage[HOME_MESSAGE_TEXTE]
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le home message (idHomeMessage = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}