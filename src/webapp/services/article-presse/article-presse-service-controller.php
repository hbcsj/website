<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("common/php/dao/article-presse-dao.php");

class ArticlePresseServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("article-presse", true);
    }

    public function toggleValidation() {
        $this->logNavigation("toggleValidation", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_superadmin()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                $postId = trim($_POST["id"]);

                $articlePresseDAO = new ArticlePresseDAO($this->getDatabaseConnection());
            
                $articlePresse = $articlePresseDAO->getById($postId);
                if ($articlePresse != null) {
                    if ($articlePresse[ARTICLE_PRESSE_VISIBLE_SUR_SITE] == 1) {
                        $articlePresse[ARTICLE_PRESSE_VISIBLE_SUR_SITE] = 0;
                    } else {
                        $articlePresse[ARTICLE_PRESSE_VISIBLE_SUR_SITE] = 1;
                    }
                    $articlePresse[ARTICLE_PRESSE_EMAIL_DERNIER_EDITEUR] = $GLOBALS[CONF][SUPERADMIN_EMAIL];
                    $articlePresseDAO->update($articlePresse);

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "L'article de presse (idArticlePresse = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function delete() {
        $this->logNavigation("delete", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_bureau()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                $postId = trim($_POST["id"]);

                $articlePresseDAO = new ArticlePresseDAO($this->getDatabaseConnection());

                $articlePresse = $articlePresseDAO->getById($postId);
                if ($articlePresse != null) {
                    $articlePresseDAO->delete($postId); 
                    
                    $articleFile = PathUtils::getArticlePresseImgFile($postId);
                    if (file_exists($articleFile)) {
                        unlink($articleFile);
                    }
                    $articleMediumFile = PathUtils::getArticlePresseImgMediumFile($postId);
                    if (file_exists($articleMediumFile)) {
                        unlink($articleMediumFile);
                    }
                    $articleSmallFile = PathUtils::getArticlePresseImgSmallFile($postId);
                    if (file_exists($articleSmallFile)) {
                        unlink($articleSmallFile);
                    }
                    $articleHomeFile = PathUtils::getArticlePresseImgHomeFile($postId);
                    if (file_exists($articleHomeFile)) {
                        unlink($articleHomeFile);
                    }

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            ARTICLE_PRESSE_TABLE_NAME, 
                            ACTION_TYPE_SUPPRESSION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => ARTICLE_PRESSE_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $articlePresse[ARTICLE_PRESSE_ID],
                                    OBJET_ASSOCIE_LABEL => ARTICLE_PRESSE_TITRE
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "L'article de presse (idArticlePresse = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}

?>