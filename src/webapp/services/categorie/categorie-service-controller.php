<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/dao/categorie-dao.php");

class CategorieServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("categorie", true);
    }

    public function getNbEquipes() {
        $this->logNavigation("getNbEquipes", array(
            "id" => $_GET["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_coach()) {
            $checkParams = $this->checkParams(array(
                array(GET, "id")
            ));
            if ($checkParams) {
                $getId = $_GET["id"];

                $categorieDAO = new CategorieDAO($this->getDatabaseConnection());

                $categorie = $categorieDAO->getById($getId);
                if ($categorie == null) {
                    $response->nbEquipes = 0;
                    $this->sendCheckError(HTTP_404, "La categorie (idCategorie = '".$getId."') n'existe pas");
                }

                $response->nbEquipes = $categorie[CATEGORIE_NB_EQUIPES];
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function save() {
        $this->logNavigation("save", array(
            "id" => $_GET["id"],
            "nbEquipes" => $_POST["nbEquipes"],
            "regroupementMatchs" => $_POST["regroupementMatchs"],
        ));

        $response = new stdClass;

        if (isAdminConnected_coach()) {
            $checkParams = $this->checkParams(array(
                array(GET, "id"),
                array(POST, "nbEquipes")
            ));
            if ($checkParams) {
                $getId = $_GET["id"];
                $postNbEquipes = trim($_POST["nbEquipes"]);
                $postRegroupementMatchs = $_POST["regroupementMatchs"];

                $categorieDAO = new CategorieDAO($this->getDatabaseConnection());

                $categorie = array();
                $categorieById = $categorieDAO->getById($getId);
                if ($categorieById != null) {
                    $categorie = $categorieById;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La categorie (idCategorie = '".$getId."') n'existe pas");
                }

                $categorie[CATEGORIE_NB_EQUIPES] = $postNbEquipes;
                $categorie[CATEGORIE_REGROUPEMENT_MATCHS] = $postRegroupementMatchs;

                $categorieDAO->update($categorie);

                if (!isAdminConnected_superadmin()) {
                    MailUtils::notifySuperadmin(
                        CATEGORIE_TABLE_NAME, 
                        ACTION_TYPE_MODIFICATION, 
                        array(
                            array(
                                OBJET_ASSOCIE_TYPE => CATEGORIE_TABLE_NAME,
                                OBJET_ASSOCIE_ID => $getId,
                                OBJET_ASSOCIE_LABEL => $categorie[CATEGORIE_NOM]
                            )
                        ), 
                        $this->getDatabaseConnection()
                    );
                }

                $response->success = true;
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function addCoach() {
        $this->logNavigation("addCoach", array(
            "categorieId" => $_GET["categorieId"],
            "licencieId" => $_POST["licencieId"],
            "enSoutien" => $_POST["enSoutien"]
        ));

        $response = new stdClass;

        if (isAdminConnected_bureau()) {
            $checkParams = $this->checkParams(array(
                array(GET, "categorieId"),
                array(POST, "licencieId")
            ));
            if ($checkParams) {
                require_once("common/php/dao/licencie-dao.php");
                require_once("common/php/dao/licencie-coache-categorie-dao.php");

                $getCategorieId = $_GET["categorieId"];
                $postLicencieId = trim($_POST["licencieId"]);
                $postEnSoutien = $_POST["enSoutien"];

                $categorieDAO = new CategorieDAO($this->getDatabaseConnection());
                $categorie = $categorieDAO->getById($getCategorieId);
                if ($categorie == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La categorie (idCategorie = '".$getCategorieId."') n'existe pas");
                }

                $licencieDAO = new LicencieDAO($this->getDatabaseConnection());
                $licencie = $licencieDAO->getById($postLicencieId);
                if ($licencie == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le licencie (idLicencie = '".$postLicencieId."') n'existe pas");
                } else {
                    $licencie[LICENCIE_ACTIF] = 1;
                    $licencieDAO->update($licencie);
                }

                $licencieCoacheCategorieDAO = new LicencieCoacheCategorieDAO($this->getDatabaseConnection());

                $licencieCoacheCategorieId = array();
                $licencieCoacheCategorieId[LICENCIE_COACHE_CATEGORIE_LICENCIE_ID] = $postLicencieId;
                $licencieCoacheCategorieId[LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID] = $getCategorieId;

                if ($licencieCoacheCategorieDAO->getById($licencieCoacheCategorieId) != null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_400, "Le licencieCoacheCategorie (idLicencie = '".$postLicencieId."', idCategorie = '".$getCategorieId."') existe deja");
                } else {
                    $licencieCoacheCategorie = array();
                    $licencieCoacheCategorie[LICENCIE_COACHE_CATEGORIE_LICENCIE_ID] = $postLicencieId;
                    $licencieCoacheCategorie[LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID] = $getCategorieId;
                    $licencieCoacheCategorie[LICENCIE_COACHE_CATEGORIE_EN_SOUTIEN] = $postEnSoutien;

                    $licencieCoacheCategorieDAO->create($licencieCoacheCategorie);

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            LICENCIE_COACHE_CATEGORIE_TABLE_NAME, 
                            ACTION_TYPE_CREATION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => CATEGORIE_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $getCategorieId,
                                    OBJET_ASSOCIE_LABEL => $categorie[CATEGORIE_NOM]
                                ),
                                array(
                                    OBJET_ASSOCIE_TYPE => LICENCIE_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $postLicencieId,
                                    OBJET_ASSOCIE_LABEL => $licencie[LICENCIE_NOM]." ".$licencie[LICENCIE_PRENOM]
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function updateCoach() {
        $this->logNavigation("updateCoach", array(
            "categorieId" => $_GET["categorieId"],
            "licencieId" => $_POST["licencieId"],
            "enSoutien" => $_POST["enSoutien"]
        ));

        $response = new stdClass;

        if (isAdminConnected_bureau()) {
            $checkParams = $this->checkParams(array(
                array(GET, "categorieId"),
                array(GET, "licencieId")
            ));
            if ($checkParams) {
                require_once("common/php/dao/licencie-dao.php");
                require_once("common/php/dao/licencie-coache-categorie-dao.php");

                $getCategorieId = $_GET["categorieId"];
                $getLicencieId = $_GET["licencieId"];
                $postEnSoutien = $_POST["enSoutien"];

                $categorieDAO = new CategorieDAO($this->getDatabaseConnection());
                $categorie = $categorieDAO->getById($getCategorieId);
                if ($categorie == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La categorie (idCategorie = '".$getCategorieId."') n'existe pas");
                }

                $licencieDAO = new LicencieDAO($this->getDatabaseConnection());
                $licencie = $licencieDAO->getById($getLicencieId);
                if ($licencie == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le licencie (idLicencie = '".$getLicencieId."') n'existe pas");
                }

                $licencieCoacheCategorieDAO = new LicencieCoacheCategorieDAO($this->getDatabaseConnection());

                $licencieCoacheCategorieId = array();
                $licencieCoacheCategorieId[LICENCIE_COACHE_CATEGORIE_LICENCIE_ID] = $getLicencieId;
                $licencieCoacheCategorieId[LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID] = $getCategorieId;

                $licencieCoacheCategorie = $licencieCoacheCategorieDAO->getById($licencieCoacheCategorieId);
                if ($licencieCoacheCategorie == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le licencieCoacheCategorie (idLicencie = '".$getLicencieId."', idCategorie = '".$getCategorieId."') n'existe pas");
                }

                $licencieCoacheCategorie[LICENCIE_COACHE_CATEGORIE_EN_SOUTIEN] = $postEnSoutien;

                $licencieCoacheCategorieDAO->update($licencieCoacheCategorie);

                if (!isAdminConnected_superadmin()) {
                    MailUtils::notifySuperadmin(
                        LICENCIE_COACHE_CATEGORIE_TABLE_NAME, 
                        ACTION_TYPE_MODIFICATION, 
                        array(
                            array(
                                OBJET_ASSOCIE_TYPE => CATEGORIE_TABLE_NAME,
                                OBJET_ASSOCIE_ID => $getCategorieId,
                                OBJET_ASSOCIE_LABEL => $categorie[CATEGORIE_NOM]
                            ),
                            array(
                                OBJET_ASSOCIE_TYPE => LICENCIE_TABLE_NAME,
                                OBJET_ASSOCIE_ID => $getLicencieId,
                                OBJET_ASSOCIE_LABEL => $licencie[LICENCIE_NOM]." ".$licencie[LICENCIE_PRENOM]
                            )
                        ), 
                        $this->getDatabaseConnection()
                    );
                }

                $response->success = true;
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function deleteCoach() {
        $this->logNavigation("deleteCoach", array(
            "categorieId" => $_POST["categorieId"],
            "licencieId" => $_POST["licencieId"]
        ));

        $response = new stdClass;

        if (isAdminConnected_bureau()) {
            $checkParams = $this->checkParams(array(
                array(POST, "categorieId"),
                array(POST, "licencieId")
            ));
            if ($checkParams) {
                require_once("common/php/dao/licencie-dao.php");
                require_once("common/php/dao/licencie-coache-categorie-dao.php");

                $postCategorieId = $_POST["categorieId"];
                $postLicencieId = trim($_POST["licencieId"]);

                $categorieDAO = new CategorieDAO($this->getDatabaseConnection());
                $categorie = $categorieDAO->getById($postCategorieId);
                if ($categorie == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La categorie (idCategorie = '".$postCategorieId."') n'existe pas");
                }

                $licencieDAO = new LicencieDAO($this->getDatabaseConnection());
                $licencie = $licencieDAO->getById($postLicencieId);
                if ($licencie == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le licencie (idLicencie = '".$postLicencieId."') n'existe pas");
                }

                $licencieCoacheCategorieDAO = new LicencieCoacheCategorieDAO($this->getDatabaseConnection());

                $licencieCoacheCategorieId = array();
                $licencieCoacheCategorieId[LICENCIE_COACHE_CATEGORIE_LICENCIE_ID] = $postLicencieId;
                $licencieCoacheCategorieId[LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID] = $postCategorieId;

                if ($licencieCoacheCategorieDAO->getById($licencieCoacheCategorieId) != null) {
                    $licencieCoacheCategorieDAO->delete($licencieCoacheCategorieId);

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            LICENCIE_COACHE_CATEGORIE_TABLE_NAME, 
                            ACTION_TYPE_SUPPRESSION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => CATEGORIE_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $postCategorieId,
                                    OBJET_ASSOCIE_LABEL => $categorie[CATEGORIE_NOM]
                                ),
                                array(
                                    OBJET_ASSOCIE_TYPE => LICENCIE_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $postLicencieId,
                                    OBJET_ASSOCIE_LABEL => $licencie[LICENCIE_NOM]." ".$licencie[LICENCIE_PRENOM]
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le licencie coache categorie (idLicencie = '".$postLicencieId."' / idCategorie = '".$postCategorieId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}