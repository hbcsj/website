<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("common/php/dao/actualite-dao.php");

class ActualiteServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("actualite", true);
    }

    public function toggleValidation() {
        $this->logNavigation("toggleValidation", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_superadmin()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                $postId = trim($_POST["id"]);

                $actualiteDAO = new ActualiteDAO($this->getDatabaseConnection());
            
                $actualite = $actualiteDAO->getById($postId);
                if ($actualite != null) {
                    if ($actualite[ACTUALITE_VISIBLE_SUR_SITE] == 1) {
                        $actualite[ACTUALITE_VISIBLE_SUR_SITE] = 0;
                    } else {
                        $actualite[ACTUALITE_VISIBLE_SUR_SITE] = 1;
                    }
                    $actualite[ACTUALITE_EMAIL_DERNIER_EDITEUR] = $GLOBALS[CONF][SUPERADMIN_EMAIL];
                    $actualiteDAO->update($actualite);

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "L'actualit&eacute; (idActualite = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function delete() {
        $this->logNavigation("delete", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_bureau()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                $postId = trim($_POST["id"]);

                $actualiteDAO = new ActualiteDAO($this->getDatabaseConnection());

                $actualite = $actualiteDAO->getById($postId);
                if ($actualite != null) {
                    $actualiteDAO->delete($postId); 
                    
                    $imageActuFile = PathUtils::getActualiteImgFile($postId);
                    if (file_exists($imageActuFile)) {
                        unlink($imageActuFile);
                    }
                    $actuHomeFile = PathUtils::getActualiteHomeFile($postId);
                    if (file_exists($actuHomeFile)) {
                        unlink($actuHomeFile);
                    }
                    $actuFile = PathUtils::getActualiteFile($postId);
                    if (file_exists($actuFile)) {
                        unlink($actuFile);
                    }

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            ACTUALITE_TABLE_NAME, 
                            ACTION_TYPE_SUPPRESSION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => ACTUALITE_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $actualite[ACTUALITE_ID],
                                    OBJET_ASSOCIE_LABEL => ACTUALITE_TITRE
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "L'actualit&eacute; (idActualite = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}

?>