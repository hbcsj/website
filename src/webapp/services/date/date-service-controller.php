<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/date-utils.php");

class DateServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("date");
    }

    public function getSlashDate() {
        $this->logNavigation("getSlashDate", null);

        $response = new stdClass;
        $response->date = date(SLASH_DATE_FORMAT);

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function getSlashDateDebutSaison() {
        $this->logNavigation("getSlashDateDebutSaison", null);

        $response = new stdClass;
        $response->date = DateUtils::get_slashDate_debutSaison();

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function getSlashDateAvecAjout() {
        $this->logNavigation("getSlashDateAvecAjout", null);

        $response = new stdClass;
        
        $checkParams = $this->checkParams(array(
            array(GET, "ajout")
        ));
        if ($checkParams) {
            $response->date = DateUtils::ajouterJoursADateDuJour($_GET["ajout"], SLASH_DATE_FORMAT); 
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}