<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("common/php/dao/album-photo-dao.php");

class AlbumPhotoServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("album-photo", true);
    }

    public function toggleValidation() {
        $this->logNavigation("toggleValidation", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_superadmin()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                $postId = trim($_POST["id"]);

                $albumPhotoDAO = new AlbumPhotoDAO($this->getDatabaseConnection());
            
                $albumPhoto = $albumPhotoDAO->getById($postId);
                if ($albumPhoto != null) {
                    if ($albumPhoto[ALBUM_PHOTO_VISIBLE_SUR_SITE] == 1) {
                        $albumPhoto[ALBUM_PHOTO_VISIBLE_SUR_SITE] = 0;
                    } else {
                        $albumPhoto[ALBUM_PHOTO_VISIBLE_SUR_SITE] = 1;
                    }
                    $albumPhoto[ALBUM_PHOTO_EMAIL_DERNIER_EDITEUR] = $GLOBALS[CONF][SUPERADMIN_EMAIL];
                    $albumPhotoDAO->update($albumPhoto);

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "L'album photo (idAlbumPhoto = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function delete() {
        $this->logNavigation("delete", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_bureau()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                $postId = trim($_POST["id"]);

                $albumPhotoDAO = new AlbumPhotoDAO($this->getDatabaseConnection());

                $albumPhoto = $albumPhotoDAO->getById($postId);
                if ($albumPhoto != null) {
                    $albumPhotoDAO->delete($postId); 
                    
                    $albumFile = PathUtils::getAlbumPhotoImgFile($postId);
                    if (file_exists($albumFile)) {
                        unlink($albumFile);
                    }
                    $albumSmallFile = PathUtils::getAlbumPhotoImgSmallFile($postId);
                    if (file_exists($albumSmallFile)) {
                        unlink($albumSmallFile);
                    }

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            ALBUM_PHOTO_TABLE_NAME, 
                            ACTION_TYPE_SUPPRESSION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => ALBUM_PHOTO_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $albumPhoto[ALBUM_PHOTO_ID],
                                    OBJET_ASSOCIE_LABEL => ALBUM_PHOTO_TITRE
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "L'album photo (idAlbumPhoto = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}

?>