<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/dao/competition-dao.php");

class CompetitionServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("competition", true);
    }

    public function getCompetitions() {
        $this->logNavigation("getCompetitions", null);

        $response = new stdClass;

        if (isAdminConnected_coach()) {
            require_once("common/php/dao/categorie-dao.php");

            $competitionDAO = new CompetitionDAO($this->getDatabaseConnection());

            $competitions = $competitionDAO->getAll(
                COMPETITION_TABLE_NAME.".".COMPETITION_EN_COURS." DESC, ".
                COMPETITION_TABLE_NAME.".".COMPETITION_CATEGORIE_ID.", ".
                COMPETITION_TABLE_NAME.".".COMPETITION_NUM_EQUIPE.", ".
                COMPETITION_TABLE_NAME.".".COMPETITION_ID." DESC"
            );
            
            $response->competitions = $this->getCompetitionsResponseObject($competitions);
        } else {
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function getCompetitionsByCategorie() {
        $this->logNavigation("getCompetitionsByCategorie", array(
            "idCategorie" => $_GET["idCategorie"]
        ));

        $response = new stdClass;

        if (isAdminConnected_coach()) {
            $checkParams = $this->checkParams(array(
                array(GET, "idCategorie")
            ));
            if ($checkParams) {
                require_once("common/php/dao/categorie-dao.php");

                $getIdCategorie = $_GET["idCategorie"];

                $categorieDAO = new CategorieDAO($this->getDatabaseConnection());
                if ($categorieDAO->getById($getIdCategorie) == null) {
                    $this->sendCheckError(HTTP_404, "La categorie (idCategorie = '".$getIdCategorie."') n'existe pas");
                }

                $competitionDAO = new CompetitionDAO($this->getDatabaseConnection());

                $competitions = $competitionDAO->getByCategorieId(
                    $getIdCategorie, 
                    COMPETITION_TABLE_NAME.".".COMPETITION_EN_COURS." DESC, ".
                    COMPETITION_TABLE_NAME.".".COMPETITION_NUM_EQUIPE.", ".
                    COMPETITION_TABLE_NAME.".".COMPETITION_ID." DESC"
                );
                
                $response->competitions = $this->getCompetitionsResponseObject($competitions);
            }
        } else {
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function save() {
        $this->logNavigation("save", array(
            "id" => $_GET["id"],
            "codeCompetition" => $_POST["codeCompetition"],
            "nom" => $_POST["nom"],
            "numEquipe" => $_POST["numEquipe"],
            "urlSiteFFHB" => $_POST["urlSiteFFHB"],
            "urlSiteLigue" => $_POST["urlSiteLigue"],
            "enCours" => $_POST["enCours"],
            "visibleSurSite" => $_POST["visibleSurSite"],
            "emailDernierEditeur" => $_POST["emailDernierEditeur"],
            "categorieId" => $_POST["categorieId"],
            "saisonId" => $_POST["saisonId"]
        ));

        $response = new stdClass;

        if (isAdminConnected_coach()) {
            $checkParams = $this->checkParams(array(
                array(POST, "nom"),
                array(POST, "saisonId"),
                array(POST, "emailDernierEditeur")
            ));
            if ($checkParams) {
                require_once("common/php/dao/categorie-dao.php");
                require_once("common/php/dao/saison-dao.php");

                $getId = $_GET["id"];
                $postCodeCompetition = StringUtils::convertAccentsToHtmlSpecialChars(trim(strtoupper($_POST["codeCompetition"])));
                $postNom = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["nom"]));
                $postNumEquipe = StringUtils::convertEmptyStringToNull(trim($_POST["numEquipe"]));
                $postUrlSiteFFHB = trim($_POST["urlSiteFFHB"]);
                $postUrlSiteLigue = trim($_POST["urlSiteLigue"]);
                $postEnCours = $_POST["enCours"];
                $postVisibleSurSite = $_POST["visibleSurSite"];
                $postEmailDernierEditeur = trim($_POST["emailDernierEditeur"]);
                $postCategorieId = StringUtils::convertEmptyStringToNull(trim($_POST["categorieId"]));
                $postSaisonId = trim($_POST["saisonId"]);

                $categorieDAO = new CategorieDAO($this->getDatabaseConnection());
                if ($postCategorieId != null && $categorieDAO->getById($postCategorieId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La categorie (idCategorie = '".$postCategorieId."') n'existe pas");
                }

                $saisonDAO = new SaisonDAO($this->getDatabaseConnection());
                if ($saisonDAO->getById($postSaisonId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La saison (idSaison = '".$postSaisonId."') n'existe pas");
                }

                $competitionDAO = new CompetitionDAO($this->getDatabaseConnection());

                $competition = array();
                if (HTTPUtils::paramExists(GET, "id")) {
                    $competitionById = $competitionDAO->getById($getId);
                    if ($competitionById != null) {
                        $competition = $competitionById;
                    } else {
                        $response->success = false;
                        $this->sendCheckError(HTTP_404, "La competition (idCompetition = '".$getId."') n'existe pas");
                    }
                }

                $competition[COMPETITION_CODE] = $postCodeCompetition;
                $competition[COMPETITION_NOM] = $postNom;
                $competition[COMPETITION_NUM_EQUIPE] = $postNumEquipe;
                $competition[COMPETITION_URL_SITE_FFHB] = $postUrlSiteFFHB;
                $competition[COMPETITION_URL_SITE_LIGUE] = $postUrlSiteLigue;
                $competition[COMPETITION_EN_COURS] = $postEnCours;
                if (isAdminConnected_bureau()) {
                    $competition[COMPETITION_VISIBLE_SUR_SITE] = $postVisibleSurSite;
                } else {
                    $competition[COMPETITION_VISIBLE_SUR_SITE] = 0;
                }
                $competition[COMPETITION_EMAIL_DERNIER_EDITEUR] = $postEmailDernierEditeur;
                $competition[COMPETITION_CATEGORIE_ID] = $postCategorieId;
                $competition[COMPETITION_SAISON_ID] = $postSaisonId;

                $actionType = ACTION_TYPE_CREATION;
                if (array_key_exists(COMPETITION_ID, $competition)) {
                    $actionType = ACTION_TYPE_MODIFICATION;
                    $competitionDAO->update($competition);
                } else {
                    $competition[COMPETITION_ID] = $competitionDAO->create($competition);
                }

                if (!isAdminConnected_superadmin()) {
                    MailUtils::notifySuperadmin(
                        COMPETITION_TABLE_NAME, 
                        $actionType, 
                        array(
                            array(
                                OBJET_ASSOCIE_TYPE => COMPETITION_TABLE_NAME,
                                OBJET_ASSOCIE_ID => $competition[COMPETITION_ID],
                                OBJET_ASSOCIE_LABEL => $competition[COMPETITION_NOM]
                            )
                        ), 
                        $this->getDatabaseConnection()
                    );
                }

                $response->success = true;
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function toggleValidation() {
        $this->logNavigation("toggleValidation", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_superadmin()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                $postId = trim($_POST["id"]);

                $competitionDAO = new CompetitionDAO($this->getDatabaseConnection());
            
                $competition = $competitionDAO->getById($postId);
                if ($competition != null) {
                    if ($competition[COMPETITION_VISIBLE_SUR_SITE] == 1) {
                        $competition[COMPETITION_VISIBLE_SUR_SITE] = 0;
                    } else {
                        $competition[COMPETITION_VISIBLE_SUR_SITE] = 1;
                    }
                    $competition[COMPETITION_EMAIL_DERNIER_EDITEUR] = $GLOBALS[CONF][SUPERADMIN_EMAIL];
                    $competitionDAO->update($competition);

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La competition (idCompetition = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function delete() {
        $this->logNavigation("delete", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_bureau()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                require_once("common/php/dao/evenement-dao.php");

                $postId = trim($_POST["id"]);

                $competitionDAO = new CompetitionDAO($this->getDatabaseConnection());
                $evenementDAO = new EvenementDAO($this->getDatabaseConnection());
            
                $competition = $competitionDAO->getById($postId);
                if ($competition != null) {
                    $evenementsCompetition = $evenementDAO->getByCompetitionId($postId);
                    if (sizeof($evenementsCompetition) > 0) {
                        foreach ($evenementsCompetition as $evenementCompetition) {
                            $evenementCompetition[EVENEMENT_COMPETITION_ID] = null;
                            $evenementDAO->update($evenementCompetition);
                        }
                    }

                    $competitionDAO->delete($postId);

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            COMPETITION_TABLE_NAME, 
                            ACTION_TYPE_SUPPRESSION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => COMPETITION_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $competition[COMPETITION_ID],
                                    OBJET_ASSOCIE_LABEL => $competition[COMPETITION_NOM]
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La competition (idCompetition = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    private function getCompetitionsResponseObject($competitions) {
        $competitionsResponseObject = array();

        $categorieDAO = new CategorieDAO($this->getDatabaseConnection());

        if (sizeof($competitions) > 0) {
            foreach ($competitions as $competition) {
                $competitionResponseObject = array();
                $competitionResponseObject["id"] = $competition[COMPETITION_ID];
                $codeCompetition = $competition[COMPETITION_CODE];
                $nomCategorie = $competition[COMPETITION_NOM];
                if ($competition[COMPETITION_CATEGORIE_ID] != null) {
                    $nomCategorie = $categorieDAO->getById($competition[COMPETITION_CATEGORIE_ID])[CATEGORIE_NOM];
                    if ($competition[COMPETITION_NUM_EQUIPE] != null) {
                        $nomCategorie .= " ".$competition[COMPETITION_NUM_EQUIPE];
                    }
                    $nomCategorie .= " - ";
                    $nomCategorie .= $competition[COMPETITION_NOM];
                }
                $competitionResponseObject["nom"] = $nomCategorie;
                $competitionsResponseObject[] = $competitionResponseObject;
            }
        }

        return $competitionsResponseObject;
    }
}