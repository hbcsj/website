<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("common/php/dao/hhsj-dao.php");

class HHSJServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("hhsj", true);
    }

    public function toggleValidation() {
        $this->logNavigation("toggleValidation", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_superadmin()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                $postId = trim($_POST["id"]);

                $hhsjDAO = new HHSJDAO($this->getDatabaseConnection());
            
                $hhsj = $hhsjDAO->getById($postId);
                if ($hhsj != null) {
                    if ($hhsj[HHSJ_VISIBLE_SUR_SITE] == 1) {
                        $hhsj[HHSJ_VISIBLE_SUR_SITE] = 0;
                    } else {
                        $hhsj[HHSJ_VISIBLE_SUR_SITE] = 1;
                    }
                    $hhsj[HHSJ_EMAIL_DERNIER_EDITEUR] = $GLOBALS[CONF][SUPERADMIN_EMAIL];
                    $hhsjDAO->update($hhsj);

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le Hand-Hebdo (idHHSJ = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function delete() {
        $this->logNavigation("delete", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_bureau()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                $postId = trim($_POST["id"]);

                $hhsjDAO = new HHSJDAO($this->getDatabaseConnection());

                $hhsj = $hhsjDAO->getById($postId);
                if ($hhsj != null) {
                    $hhsjDAO->delete($postId); 
                    
                    $hhsjFile = PathUtils::getHHSJImgFile($postId);
                    if (file_exists($hhsjFile)) {
                        unlink($hhsjFile);
                    }
                    $hhsjMediumFile = PathUtils::getHHSJImgMediumFile($postId);
                    if (file_exists($hhsjMediumFile)) {
                        unlink($hhsjMediumFile);
                    }
                    $hhsjSmallFile = PathUtils::getHHSJImgSmallFile($postId);
                    if (file_exists($hhsjSmallFile)) {
                        unlink($hhsjSmallFile);
                    }
                    $hhsjHomeFile = PathUtils::getHHSJImgHomeFile($postId);
                    if (file_exists($hhsjHomeFile)) {
                        unlink($hhsjHomeFile);
                    }
                    $hhsjFile = PathUtils::getHHSJFile($postId);
                    if (file_exists($hhsjFile)) {
                        unlink($hhsjFile);
                    }

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            HHSJ_TABLE_NAME, 
                            ACTION_TYPE_SUPPRESSION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => HHSJ_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $hhsj[HHSJ_ID],
                                    OBJET_ASSOCIE_LABEL => HHSJ_TITRE
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le Hand-Hebdo (idHHSJ = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}

?>