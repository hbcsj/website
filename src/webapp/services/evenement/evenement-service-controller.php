<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("common/php/dao/evenement-dao.php");

class EvenementServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("evenement", true);
    }

    public function save() {
        $this->logNavigation("save", array(
            "id" => $_GET["id"],
            "codeRencontre" => $_POST["codeRencontre"],
            "nom" => $_POST["nom"],
            "date" => $_POST["date"],
            "heure" => $_POST["heure"],
            "numEquipe" => $_POST["numEquipe"],
            "adversaire" => $_POST["adversaire"],
            "adresse" => $_POST["adresse"],
            "codePostal" => $_POST["codePostal"],
            "ville" => $_POST["ville"],
            "latitude" => $_POST["latitude"],
            "longitude" => $_POST["longitude"],
            "aDomicile" => $_POST["aDomicile"],
            "scoreHBCSJ" => $_POST["scoreHBCSJ"],
            "scoreAdversaire" => $_POST["scoreAdversaire"],
            "visibleSurSite" => $_POST["visibleSurSite"],
            "emailDernierEditeur" => $_POST["emailDernierEditeur"],
            "gymnaseId" => $_POST["gymnaseId"],
            "competitionId" => $_POST["competitionId"],
            "typeEvenementId" => $_POST["typeEvenementId"],
            "categorieIds" => $_POST["categorieIds"]
        ));

        $response = new stdClass;

        if (isAdminConnected_coach()) {
            $checkParams = $this->checkParams(array(
                array(POST, "typeEvenementId"),
                array(POST, "date"),
                array(POST, "heure"),
                array(POST, "categorieIds"),
                array(POST, "emailDernierEditeur")
            ));
            if ($checkParams) {
                require_once("common/php/dao/competition-dao.php");
                require_once("common/php/dao/type-evenement-dao.php");
                require_once("common/php/dao/categorie-participe-a-evenement-dao.php");
                require_once("common/php/dao/gymnase-dao.php");
                require_once("common/php/dao/categorie-dao.php");

                $getId = $_GET["id"];
                $postCodeRencontre = StringUtils::convertAccentsToHtmlSpecialChars(trim(strtoupper($_POST["codeRencontre"])));
                $postNom = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["nom"]));
                $postDateHeure = DateUtils::convert_slashDateTime_to_sqlDateTime(trim($_POST["date"]).SLASH_DATE_TIME_SEPARATOR.trim($_POST["heure"]));
                $postNumEquipe = StringUtils::convertEmptyStringToNull(trim($_POST["numEquipe"]));
                $postAdversaire = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["adversaire"]));
                $postAdresse = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["adresse"]));
                $postCodePostal = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["codePostal"]));
                $postVille = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["ville"]));
                $postLatitude = StringUtils::convertEmptyStringToNull(trim($_POST["latitude"]));
                $postLongitude = StringUtils::convertEmptyStringToNull(trim($_POST["longitude"]));
                $postADomicile = $_POST["aDomicile"];
                $postScoreHBCSJ = StringUtils::convertEmptyStringToNull(trim($_POST["scoreHBCSJ"]));
                $postScoreAdversaire = StringUtils::convertEmptyStringToNull(trim($_POST["scoreAdversaire"]));
                $postVisibleSurSite = $_POST["visibleSurSite"];
                $postEmailDernierEditeur = trim($_POST["emailDernierEditeur"]);
                $postGymnaseId = StringUtils::convertEmptyStringToNull(trim($_POST["gymnaseId"]));
                $postCompetitionId = StringUtils::convertEmptyStringToNull(trim($_POST["competitionId"]));
                $postTypeEvenementId = trim($_POST["typeEvenementId"]);
                $postCategorieIds = $_POST["categorieIds"];

                $categorieDAO = new CategorieDAO($this->getDatabaseConnection());
                foreach ($postCategorieIds as $categorieId) {
                    if ($categorieDAO->getById($categorieId) == null) {
                        $response->success = false;
                        $this->sendCheckError(HTTP_404, "La categorie (idCategorie = '".$categorieId."') n'existe pas");
                    }
                }

                $typeEvenementDAO = new TypeEvenementDAO($this->getDatabaseConnection());
                if ($typeEvenementDAO->getById($postTypeEvenementId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le type d'evenement (idTypeEvenement = '".$postTypeEvenementId."') n'existe pas");
                }

                $gymnaseDAO = new GymnaseDAO($this->getDatabaseConnection());
                if ($postGymnaseId != null && $gymnaseDAO->getById($postGymnaseId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le gymnase (idGymnase = '".$postGymnaseId."') n'existe pas");
                }

                $competitionDAO = new CompetitionDAO($this->getDatabaseConnection());
                if ($postCompetitionId != null && $competitionDAO->getById($postCompetitionId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La competition (idCompetition = '".$postCompetitionId."') n'existe pas");
                }

                $evenementDAO = new EvenementDAO($this->getDatabaseConnection());
                $categorieParticipeAEvenementDAO = new CategorieParticipeAEvenementDAO($this->getDatabaseConnection());

                $evenement = array();
                if (HTTPUtils::paramExists(GET, "id")) {
                    $evenementById = $evenementDAO->getById($getId);
                    if ($evenementById != null) {
                        $evenement = $evenementById;
                    } else {
                        $response->success = false;
                        $this->sendCheckError(HTTP_404, "L'evenement (idEvenement = '".$getId."') n'existe pas");
                    }
                }

                $evenement[EVENEMENT_CODE] = $postCodeRencontre;
                $evenement[EVENEMENT_NOM] = $postNom;
                $evenement[EVENEMENT_DATE_HEURE] = $postDateHeure;
                $evenement[EVENEMENT_NUM_EQUIPE] = $postNumEquipe;
                $evenement[EVENEMENT_ADVERSAIRE] = $postAdversaire;
                $evenement[EVENEMENT_ADRESSE] = $postAdresse;
                $evenement[EVENEMENT_CODE_POSTAL] = $postCodePostal;
                $evenement[EVENEMENT_VILLE] = $postVille;
                $evenement[EVENEMENT_LATITUDE] = $postLatitude;
                $evenement[EVENEMENT_LONGITUDE] = $postLongitude;
                $evenement[EVENEMENT_A_DOMICILE] = $postADomicile;
                $evenement[EVENEMENT_SCORE_HBCSJ] = $postScoreHBCSJ;
                $evenement[EVENEMENT_SCORE_ADVERSAIRE] = $postScoreAdversaire;
                if (isAdminConnected_bureau()) {
                    $evenement[EVENEMENT_VISIBLE_SUR_SITE] = $postVisibleSurSite;
                } else {
                    $evenement[EVENEMENT_VISIBLE_SUR_SITE] = 0;
                }
                $evenement[EVENEMENT_EMAIL_DERNIER_EDITEUR] = $postEmailDernierEditeur;
                $evenement[EVENEMENT_COMPETITION_ID] = $postCompetitionId;
                $evenement[EVENEMENT_GYMNASE_ID] = $postGymnaseId;
                $evenement[EVENEMENT_TYPE_EVENEMENT_ID] = $postTypeEvenementId;

                $actionType = ACTION_TYPE_CREATION;
                if (array_key_exists(EVENEMENT_ID, $evenement)) {
                    $actionType = ACTION_TYPE_MODIFICATION;
                    $evenementDAO->update($evenement);
                    $categorieParticipeAEvenementDAO->deleteByEvenementId($evenement[EVENEMENT_ID]);
                } else {
                    $evenement[EVENEMENT_ID] = $evenementDAO->create($evenement);
                }

                foreach ($postCategorieIds as $categorieId) {
                    if ($evenement[EVENEMENT_ID] != 0) {
                        $categorieParticipeAEvenement = array();
                        $categorieParticipeAEvenement[CATEGORIE_PARTICIPE_A_EVENEMENT_EVENEMENT_ID] = $evenement[EVENEMENT_ID];
                        $categorieParticipeAEvenement[CATEGORIE_PARTICIPE_A_EVENEMENT_CATEGORIE_ID] = $categorieId;
                        $categorieParticipeAEvenementDAO->create($categorieParticipeAEvenement);
                    }
                }

                if (!isAdminConnected_superadmin()) {
                    MailUtils::notifySuperadmin(
                        EVENEMENT_TABLE_NAME, 
                        $actionType, 
                        array(
                            array(
                                OBJET_ASSOCIE_TYPE => EVENEMENT_TABLE_NAME,
                                OBJET_ASSOCIE_ID => $evenement[EVENEMENT_ID],
                                OBJET_ASSOCIE_LABEL => EVENEMENT_TYPE_EVENEMENT_ID."=".$evenement[EVENEMENT_TYPE_EVENEMENT_ID].", ".EVENEMENT_DATE_HEURE."=".$evenement[EVENEMENT_DATE_HEURE].", ".EVENEMENT_NOM."=".$evenement[EVENEMENT_NOM]
                            )
                        ), 
                        $this->getDatabaseConnection()
                    );
                }

                $response->success = true;
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function toggleValidation() {
        $this->logNavigation("toggleValidation", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_superadmin()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                $postId = trim($_POST["id"]);

                $evenementDAO = new EvenementDAO($this->getDatabaseConnection());
            
                $evenement = $evenementDAO->getById($postId);
                if ($evenement != null) {
                    if ($evenement[EVENEMENT_VISIBLE_SUR_SITE] == 1) {
                        $evenement[EVENEMENT_VISIBLE_SUR_SITE] = 0;
                    } else {
                        $evenement[EVENEMENT_VISIBLE_SUR_SITE] = 1;
                    }
                    $evenement[EVENEMENT_EMAIL_DERNIER_EDITEUR] = $GLOBALS[CONF][SUPERADMIN_EMAIL];
                    $evenementDAO->update($evenement);

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "L'evenement (idEvenement = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function actionClubExists() {
        $this->logNavigation("actionClubExists", array(
            "evenementId" => $_GET["evenementId"],
            "licencieId" => $_GET["licencieId"],
            "typeActionClubId" => $_GET["typeActionClubId"]
        ));

        $response = new stdClass;

        if (isAdminConnected_coach()) {
            $checkParams = $this->checkParams(array(
                array(GET, "evenementId"),
                array(GET, "licencieId"),
                array(GET, "typeActionClubId")
            ));
            if ($checkParams) {
                require_once("common/php/dao/action-club-dao.php");
                require_once("common/php/dao/licencie-dao.php");
                require_once("common/php/dao/type-action-club-dao.php");

                $getEvenementId = $_GET["evenementId"];
                $getLicencieId = $_GET["licencieId"];
                $getTypeActionClubId = $_GET["typeActionClubId"];

                $evenementDAO = new EvenementDAO($this->getDatabaseConnection());
                if ($evenementDAO->getById($getEvenementId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "L'evenement' (idEvenement = '".$getEvenementId."') n'existe pas");
                }

                $licencieDAO = new LicencieDAO($this->getDatabaseConnection());
                if ($licencieDAO->getById($getLicencieId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le licencie (idLicencie = '".$getLicencieId."') n'existe pas");
                }

                $typeActionClubDAO = new TypeActionClubDAO($this->getDatabaseConnection());
                if ($typeActionClubDAO->getById($getTypeActionClubId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le type d'action club (idActionClub = '".$getTypeActionClubId."') n'existe pas");
                }

                $actionClubDAO = new ActionClubDAO($this->getDatabaseConnection());

                $actionClubId = array();
                $actionClubId[ACTION_CLUB_TYPE_ACTION_CLUB_ID] = $getTypeActionClubId;
                $actionClubId[ACTION_CLUB_LICENCIE_ID] = $getLicencieId;
                $actionClubId[ACTION_CLUB_EVENEMENT_ID] = $getEvenementId;

                $response->actionClubExists = ($actionClubDAO->getById($actionClubId) != null);
            }
        } else {
            $response->actionClubExists = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function addActionClub() {
        $this->logNavigation("addActionClub", array(
            "evenementId" => $_GET["evenementId"],
            "licencieId" => $_POST["licencieId"],
            "typeActionClubId" => $_POST["typeActionClubId"]
        ));

        $response = new stdClass;

        if (isAdminConnected_coach()) {
            $checkParams = $this->checkParams(array(
                array(GET, "evenementId"),
                array(POST, "licencieId"),
                array(POST, "typeActionClubId")
            ));
            if ($checkParams) {
                require_once("common/php/dao/licencie-dao.php");
                require_once("common/php/dao/action-club-dao.php");
                require_once("common/php/dao/type-action-club-dao.php");

                $getEvenementId = $_GET["evenementId"];
                $postLicencieId = trim($_POST["licencieId"]);
                $postTypeActionClubId = trim($_POST["typeActionClubId"]);

                $evenementDAO = new EvenementDAO($this->getDatabaseConnection());
                $evenement = $evenementDAO->getById($getEvenementId);
                if ($evenement == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "L'evenement' (idEvenement = '".$getEvenementId."') n'existe pas");
                }

                $licencieDAO = new LicencieDAO($this->getDatabaseConnection());
                $licencie = $licencieDAO->getById($postLicencieId);
                if ($licencie == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le licencie (idLicencie = '".$postLicencieId."') n'existe pas");
                }

                $typeActionClubDAO = new TypeActionClubDAO($this->getDatabaseConnection());
                $typeActionClub = $typeActionClubDAO->getById($postTypeActionClubId);
                if ($typeActionClub == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le type d'action club (idActionClub = '".$postTypeActionClubId."') n'existe pas");
                }

                $actionClubDAO = new ActionClubDAO($this->getDatabaseConnection());

                $actionClubId = array();
                $actionClubId[ACTION_CLUB_TYPE_ACTION_CLUB_ID] = $postTypeActionClubId;
                $actionClubId[ACTION_CLUB_LICENCIE_ID] = $postLicencieId;
                $actionClubId[ACTION_CLUB_EVENEMENT_ID] = $getEvenementId;

                if ($actionClubDAO->getById($actionClubId) != null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_400, "L'action club (idTypeActionClub = '".$postTypeActionClubId."', idLicencie = '".$postLicencieId."', idEvenement = '".$getEvenementId."') existe deja");
                } else {
                    $actionClub = array();
                    $actionClub[ACTION_CLUB_TYPE_ACTION_CLUB_ID] = $postTypeActionClubId;
                    $actionClub[ACTION_CLUB_LICENCIE_ID] = $postLicencieId;
                    $actionClub[ACTION_CLUB_EVENEMENT_ID] = $getEvenementId;

                    $actionClubDAO->create($actionClub);

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            ACTION_CLUB_TABLE_NAME, 
                            ACTION_TYPE_CREATION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => TYPE_ACTION_CLUB_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $postTypeActionClubId,
                                    OBJET_ASSOCIE_LABEL => $typeActionClub[TYPE_ACTION_CLUB_LIBELLE]
                                ),
                                array(
                                    OBJET_ASSOCIE_TYPE => EVENEMENT_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $getEvenementId,
                                    OBJET_ASSOCIE_LABEL => EVENEMENT_TYPE_EVENEMENT_ID."=".$evenement[EVENEMENT_TYPE_EVENEMENT_ID].", ".EVENEMENT_DATE_HEURE."=".$evenement[EVENEMENT_DATE_HEURE].", ".EVENEMENT_NOM."=".$evenement[EVENEMENT_NOM]
                                ),
                                array(
                                    OBJET_ASSOCIE_TYPE => LICENCIE_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $postLicencieId,
                                    OBJET_ASSOCIE_LABEL => $licencie[LICENCIE_NOM]." ".$licencie[LICENCIE_PRENOM]
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function deleteActionClub() {
        $this->logNavigation("deleteActionClub", array(
            "evenementId" => $_POST["evenementId"],
            "licencieId" => $_POST["licencieId"],
            "typeActionClubId" => $_POST["typeActionClubId"]
        ));

        $response = new stdClass;

        if (isAdminConnected_coach()) {
            $checkParams = $this->checkParams(array(
                array(POST, "evenementId"),
                array(POST, "licencieId"),
                array(POST, "typeActionClubId")
            ));
            if ($checkParams) {
                require_once("common/php/dao/licencie-dao.php");
                require_once("common/php/dao/action-club-dao.php");
                require_once("common/php/dao/type-action-club-dao.php");

                $postEvenementId = $_POST["evenementId"];
                $postLicencieId = trim($_POST["licencieId"]);
                $postTypeActionClubId = trim($_POST["typeActionClubId"]);

                $evenementDAO = new EvenementDAO($this->getDatabaseConnection());
                $evenement = $evenementDAO->getById($postEvenementId);
                if ($evenement == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "L'evenement' (idEvenement = '".$postEvenementId."') n'existe pas");
                }

                $licencieDAO = new LicencieDAO($this->getDatabaseConnection());
                $licencie = $licencieDAO->getById($postLicencieId);
                if ($licencie == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le licencie (idLicencie = '".$postLicencieId."') n'existe pas");
                }

                $typeActionClubDAO = new TypeActionClubDAO($this->getDatabaseConnection());
                $typeActionClub = $typeActionClubDAO->getById($postTypeActionClubId);
                if ($typeActionClub == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le type d'action club (idActionClub = '".$postTypeActionClubId."') n'existe pas");
                }

                $actionClubDAO = new ActionClubDAO($this->getDatabaseConnection());

                $actionClubId = array();
                $actionClubId[ACTION_CLUB_TYPE_ACTION_CLUB_ID] = $postTypeActionClubId;
                $actionClubId[ACTION_CLUB_LICENCIE_ID] = $postLicencieId;
                $actionClubId[ACTION_CLUB_EVENEMENT_ID] = $postEvenementId;

                if ($actionClubDAO->getById($actionClubId) != null) {
                    $actionClubDAO->delete($actionClubId);

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            ACTION_CLUB_TABLE_NAME, 
                            ACTION_TYPE_SUPPRESSION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => TYPE_ACTION_CLUB_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $postTypeActionClubId,
                                    OBJET_ASSOCIE_LABEL => $typeActionClub[TYPE_ACTION_CLUB_LIBELLE]
                                ),
                                array(
                                    OBJET_ASSOCIE_TYPE => EVENEMENT_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $postEvenementId,
                                    OBJET_ASSOCIE_LABEL => EVENEMENT_TYPE_EVENEMENT_ID."=".$evenement[EVENEMENT_TYPE_EVENEMENT_ID].", ".EVENEMENT_DATE_HEURE."=".$evenement[EVENEMENT_DATE_HEURE].", ".EVENEMENT_NOM."=".$evenement[EVENEMENT_NOM]
                                ),
                                array(
                                    OBJET_ASSOCIE_TYPE => LICENCIE_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $postLicencieId,
                                    OBJET_ASSOCIE_LABEL => $licencie[LICENCIE_NOM]." ".$licencie[LICENCIE_PRENOM]
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "L'action club (idTypeActionClub = '".$postTypeActionClubId."', idLicencie = '".$postLicencieId."', idEvenement = '".$postEvenementId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function addArbitrage() {
        $this->logNavigation("addArbitrage", array(
            "evenementId" => $_GET["evenementId"],
            "licencieId" => $_POST["licencieId"]
        ));

        $response = new stdClass;

        if (isAdminConnected_coach()) {
            $checkParams = $this->checkParams(array(
                array(GET, "evenementId"),
                array(POST, "licencieId")
            ));
            if ($checkParams) {
                require_once("common/php/dao/licencie-dao.php");
                require_once("common/php/dao/arbitrage-dao.php");

                $getEvenementId = $_GET["evenementId"];
                $postLicencieId = trim($_POST["licencieId"]);

                $evenementDAO = new EvenementDAO($this->getDatabaseConnection());
                $evenement = $evenementDAO->getById($getEvenementId);
                if ($evenement == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "L'evenement' (idEvenement = '".$getEvenementId."') n'existe pas");
                }

                $licencieDAO = new LicencieDAO($this->getDatabaseConnection());
                $licencie = $licencieDAO->getById($postLicencieId);
                if ($licencie == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le licencie (idLicencie = '".$postLicencieId."') n'existe pas");
                }

                $arbitrageDAO = new ArbitrageDAO($this->getDatabaseConnection());

                $arbitrageId = array();
                $arbitrageId[ARBITRAGE_LICENCIE_ID] = $postLicencieId;
                $arbitrageId[ARBITRAGE_EVENEMENT_ID] = $getEvenementId;

                if ($arbitrageDAO->getById($arbitrageId) != null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_400, "L'arbitrage (idLicencie = '".$postLicencieId."', idEvenement = '".$getEvenementId."') existe deja");
                } else {
                    $arbitrage = array();
                    $arbitrage[ARBITRAGE_LICENCIE_ID] = $postLicencieId;
                    $arbitrage[ARBITRAGE_EVENEMENT_ID] = $getEvenementId;

                    $arbitrageDAO->create($arbitrage);

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            ARBITRAGE_TABLE_NAME, 
                            ACTION_TYPE_CREATION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => EVENEMENT_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $getEvenementId,
                                    OBJET_ASSOCIE_LABEL => EVENEMENT_TYPE_EVENEMENT_ID."=".$evenement[EVENEMENT_TYPE_EVENEMENT_ID].", ".EVENEMENT_DATE_HEURE."=".$evenement[EVENEMENT_DATE_HEURE].", ".EVENEMENT_NOM."=".$evenement[EVENEMENT_NOM]
                                ),
                                array(
                                    OBJET_ASSOCIE_TYPE => LICENCIE_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $postLicencieId,
                                    OBJET_ASSOCIE_LABEL => $licencie[LICENCIE_NOM]." ".$licencie[LICENCIE_PRENOM]
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function deleteArbitrage() {
        $this->logNavigation("deleteArbitrage", array(
            "evenementId" => $_POST["evenementId"],
            "licencieId" => $_POST["licencieId"]
        ));

        $response = new stdClass;

        if (isAdminConnected_coach()) {
            $checkParams = $this->checkParams(array(
                array(POST, "evenementId"),
                array(POST, "licencieId")
            ));
            if ($checkParams) {
                require_once("common/php/dao/licencie-dao.php");
                require_once("common/php/dao/arbitrage-dao.php");

                $postEvenementId = $_POST["evenementId"];
                $postLicencieId = trim($_POST["licencieId"]);

                $evenementDAO = new EvenementDAO($this->getDatabaseConnection());
                $evenement = $evenementDAO->getById($postEvenementId);
                if ($evenement == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "L'evenement' (idEvenement = '".$postEvenementId."') n'existe pas");
                }

                $licencieDAO = new LicencieDAO($this->getDatabaseConnection());
                $licencie = $licencieDAO->getById($postLicencieId);
                if ($licencie == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le licencie (idLicencie = '".$postLicencieId."') n'existe pas");
                }

                $arbitrageDAO = new ArbitrageDAO($this->getDatabaseConnection());

                $arbitrageId = array();
                $arbitrageId[ARBITRAGE_LICENCIE_ID] = $postLicencieId;
                $arbitrageId[ARBITRAGE_EVENEMENT_ID] = $postEvenementId;

                if ($arbitrageDAO->getById($arbitrageId) != null) {
                    $arbitrageDAO->delete($arbitrageId);

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            ARBITRAGE_TABLE_NAME, 
                            ACTION_TYPE_SUPPRESSION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => EVENEMENT_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $postEvenementId,
                                    OBJET_ASSOCIE_LABEL => EVENEMENT_TYPE_EVENEMENT_ID."=".$evenement[EVENEMENT_TYPE_EVENEMENT_ID].", ".EVENEMENT_DATE_HEURE."=".$evenement[EVENEMENT_DATE_HEURE].", ".EVENEMENT_NOM."=".$evenement[EVENEMENT_NOM]
                                ),
                                array(
                                    OBJET_ASSOCIE_TYPE => LICENCIE_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $postLicencieId,
                                    OBJET_ASSOCIE_LABEL => $licencie[LICENCIE_NOM]." ".$licencie[LICENCIE_PRENOM]
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "L'arbitrage (idLicencie = '".$postLicencieId."', idEvenement = '".$postEvenementId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function delete() {
        $this->logNavigation("delete", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_bureau()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                require_once("common/php/dao/action-club-dao.php");
                require_once("common/php/dao/arbitrage-dao.php");
                require_once("common/php/dao/dispo-dao.php");
                require_once("common/php/dao/histo-dispo-dao.php");
                require_once("common/php/dao/categorie-participe-a-evenement-dao.php");

                $postId = trim($_POST["id"]);

                $evenementDAO = new EvenementDAO($this->getDatabaseConnection());
                $actionClubDAO = new ActionClubDAO($this->getDatabaseConnection());
                $arbitrageDAO = new ArbitrageDAO($this->getDatabaseConnection());
                $dispoDAO = new DispoDAO($this->getDatabaseConnection());
                $histoDispoDAO = new HistoDispoDAO($this->getDatabaseConnection());
                $categorieParticipeAEvenementDAO = new CategorieParticipeAEvenementDAO($this->getDatabaseConnection());
            
                $evenement = $evenementDAO->getById($postId);
                if ($evenement != null) {
                    $actionClubDAO->deleteByEvenementId($postId);
                    $arbitrageDAO->deleteByEvenementId($postId);
                    $dispoDAO->deleteByEvenementId($postId);
                    $histoDispoDAO->deleteByEvenementId($postId);
                    $categorieParticipeAEvenementDAO->deleteByEvenementId($postId);
                    $evenementDAO->delete($postId); 
                    
                    $fdmFile = PathUtils::getFDMMatchFile($postId);
                    if (file_exists($fdmFile)) {
                        unlink($fdmFile);
                    }

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            EVENEMENT_TABLE_NAME, 
                            ACTION_TYPE_SUPPRESSION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => EVENEMENT_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $evenement[EVENEMENT_ID],
                                    OBJET_ASSOCIE_LABEL => EVENEMENT_TYPE_EVENEMENT_ID."=".$evenement[EVENEMENT_TYPE_EVENEMENT_ID].", ".EVENEMENT_DATE_HEURE."=".$evenement[EVENEMENT_DATE_HEURE].", ".EVENEMENT_NOM."=".$evenement[EVENEMENT_NOM]
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "L'evenement (idEvenement = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function deleteFDM() {
        $this->logNavigation("delete", array(
            "idEvenement" => $_POST["idEvenement"]
        ));

        $response = new stdClass;

        if (isAdminConnected_bureau()) {
            $checkParams = $this->checkParams(array(
                array(POST, "idEvenement")
            ));
            if ($checkParams) {
                require_once("common/php/managers/evenement-manager.php");

                $postIdEvenement = trim($_POST["idEvenement"]);

                $evenementDAO = new EvenementDAO($this->getDatabaseConnection());
            
                $evenement = $evenementDAO->getById($postIdEvenement);
                if ($evenement != null) {
                    $fdmFile = PathUtils::getFDMMatchFile($postIdEvenement);
                    if (file_exists($fdmFile)) {
                        unlink($fdmFile);
                    }

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            EVENEMENT_TABLE_NAME, 
                            ACTION_TYPE_MODIFICATION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => EVENEMENT_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $evenement[EVENEMENT_ID],
                                    OBJET_ASSOCIE_LABEL => MATCH_FDM
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "L'evenement (idEvenement = '".$postIdEvenement."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}