<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/dao/match-billetterie-dao.php");

class MatchBilletterieServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("match-billetterie", true);
    }

    public function save() {
        $this->logNavigation("save", array(
            "id" => $_GET["id"],
            "date" => $_POST["date"],
            "heure" => $_POST["heure"],
            "prix1place" => $_POST["prix1Place"],
            "prix3places" => $_POST["prix3Places"],
            "prixPublic" => $_POST["prixPublic"],
            "enVente" => $_POST["enVente"],
            "dateLimiteCommande" => $_POST["dateLimiteCommande"],
            "adversaireId" => $_POST["adversaireId"],
            "competitionId" => $_POST["competitionId"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(POST, "date"),
                array(POST, "heure"),
                array(POST, "prix1Place"),
                array(POST, "prix3Places"),
                array(POST, "prixPublic"),
                array(POST, "dateLimiteCommande"),
                array(POST, "adversaireId"),
                array(POST, "competitionId")
            ));
            if ($checkParams) {
                require_once("common/php/dao/adversaire-billetterie-dao.php");
                require_once("common/php/dao/competition-billetterie-dao.php");

                $getId = $_GET["id"];
                $postDateHeure = DateUtils::convert_slashDateTime_to_sqlDateTime(trim($_POST["date"]).SLASH_DATE_TIME_SEPARATOR.trim($_POST["heure"]));
                $postPrix1Place = trim($_POST["prix1Place"]);
                $postPrix3Places = trim($_POST["prix3Places"]);
                $postPrixPublic = trim($_POST["prixPublic"]);
                $postDateLimiteCommande = DateUtils::convert_slashDate_to_sqlDate(trim($_POST["dateLimiteCommande"]));
                $postEnVente = $_POST["enVente"];
                $postAdversaireId = trim($_POST["adversaireId"]);
                $postCompetitionId = trim($_POST["competitionId"]);

                $adversaireBilletterieDAO = new AdversaireBilletterieDAO($this->getDatabaseConnection());
                if ($postAdversaireId != null && $adversaireBilletterieDAO->getById($postAdversaireId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "L'adversaire billetterie' (idAdversaireBilletterie = '".$postAdversaireId."') n'existe pas");
                }

                $competitionBilletterieDAO = new CompetitionBilletterieDAO($this->getDatabaseConnection());
                if ($postCompetitionId != null && $competitionBilletterieDAO->getById($postCompetitionId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La competition billetterie (idCompetitionBilletterie = '".$postCompetitionId."') n'existe pas");
                }

                $matchBilletterieDAO = new MatchBilletterieDAO($this->getDatabaseConnection());

                $matchBilletterie = array();
                if (HTTPUtils::paramExists(GET, "id")) {
                    $matchBilletterieById = $matchBilletterieDAO->getById($getId);
                    if ($matchBilletterieById != null) {
                        $matchBilletterie = $matchBilletterieById;
                    } else {
                        $response->success = false;
                        $this->sendCheckError(HTTP_404, "Le match billetterie (idMatchBilletterie = '".$getId."') n'existe pas");
                    }
                }
                
                $matchBilletterie[MATCH_BILLETTERIE_DATE_HEURE] = $postDateHeure;
                $matchBilletterie[MATCH_BILLETTERIE_PRIX_1_PLACE] = $postPrix1Place;
                $matchBilletterie[MATCH_BILLETTERIE_PRIX_3_PLACES] = $postPrix3Places;
                $matchBilletterie[MATCH_BILLETTERIE_PRIX_PUBLIC] = $postPrixPublic;
                $matchBilletterie[MATCH_BILLETTERIE_DATE_LIMITE_COMMANDE] = $postDateLimiteCommande;
                $matchBilletterie[MATCH_BILLETTERIE_EN_VENTE] = $postEnVente;
                $matchBilletterie[MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID] = $postAdversaireId;
                $matchBilletterie[MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID] = $postCompetitionId;

                $actionType = ACTION_TYPE_CREATION;
                if (array_key_exists(MATCH_BILLETTERIE_ID, $matchBilletterie)) {
                    $actionType = ACTION_TYPE_MODIFICATION;
                    $matchBilletterieDAO->update($matchBilletterie);
                } else {
                    $matchBilletterie[MATCH_BILLETTERIE_ID] = $matchBilletterieDAO->create($matchBilletterie);
                }

                if (!isAdminConnected_superadmin()) {
                    MailUtils::notifySuperadmin(
                        MATCH_BILLETTERIE_TABLE_NAME, 
                        $actionType, 
                        array(
                            array(
                                OBJET_ASSOCIE_TYPE => MATCH_BILLETTERIE_TABLE_NAME,
                                OBJET_ASSOCIE_ID => $matchBilletterie[MATCH_BILLETTERIE_ID],
                                OBJET_ASSOCIE_LABEL => MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID."=".$matchBilletterie[MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID].", ".MATCH_BILLETTERIE_DATE_HEURE."=".$matchBilletterie[MATCH_BILLETTERIE_DATE_HEURE]
                            )
                        ), 
                        $this->getDatabaseConnection()
                    );
                }

                $response->success = true;
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function delete() {
        $this->logNavigation("delete", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                require_once("common/php/dao/commande-item-billetterie-dao.php");

                $postId = trim($_POST["id"]);

                $matchBilletterieDAO = new MatchBilletterieDAO($this->getDatabaseConnection());
                $commandeItemBilletterieDAO = new CommandeItemBilletterieDAO($this->getDatabaseConnection());
            
                $matchBilletterie = $matchBilletterieDAO->getById($postId);
                if ($matchBilletterie != null) {
                    if (sizeof($commandeItemBilletterieDAO->getByMatchBilletterieId($postId)) == 0) {
                        $matchBilletterieDAO->delete($postId);

                        if (!isAdminConnected_superadmin()) {
                            MailUtils::notifySuperadmin(
                                MATCH_BILLETTERIE_TABLE_NAME, 
                                ACTION_TYPE_SUPPRESSION, 
                                array(
                                    array(
                                        OBJET_ASSOCIE_TYPE => MATCH_BILLETTERIE_TABLE_NAME,
                                        OBJET_ASSOCIE_ID => $matchBilletterie[MATCH_BILLETTERIE_ID],
                                        OBJET_ASSOCIE_LABEL => MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID."=".$matchBilletterie[MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID].", ".MATCH_BILLETTERIE_DATE_HEURE."=".$matchBilletterie[MATCH_BILLETTERIE_DATE_HEURE]
                                    )
                                ), 
                                $this->getDatabaseConnection()
                            );
                        }

                        $response->success = true;
                    } else {
                        $response->success = false;
                        $this->sendCheckError(HTTP_400, "Le match billetterie (idMatchBilletterie = '".$postId."') a des commandes associees");
                    }
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le match billetterie (idMatchBilletterie = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}