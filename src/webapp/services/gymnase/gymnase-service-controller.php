<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/dao/evenement-dao.php");
require_once("common/php/dao/gymnase-dao.php");

class GymnaseServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("gymnase", true);
    }

    public function getGymnaseById() {
        $this->logNavigation("getGymnaseById", array(
            "id" => $_GET["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_coach()) {
            $checkParams = $this->checkParams(array(
                array(GET, "id")
            ));
            if ($checkParams) {
                $getId = $_GET["id"];

                $gymnaseDAO = new GymnaseDAO($this->getDatabaseConnection());
            
                $gymnase = $gymnaseDAO->getById($getId);
                if ($gymnase == null) {
                    $this->sendCheckError(HTTP_404, "Le gymnase (idGymnase = '".$getId."') n'existe pas");
                }
                
                $response = $this->getGymnaseResponseObject($gymnase);
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function save() {
        $this->logNavigation("save", array(
            "id" => $_GET["id"],
            "nom" => $_POST["nom"],
            "adresse" => $_POST["adresse"],
            "codePostal" => $_POST["codePostal"],
            "ville" => $_POST["ville"],
            "latitude" => $_POST["latitude"],
            "longitude" => $_POST["longitude"],
            "aDomicile" => $_POST["aDomicile"]
        ));

        $response = new stdClass;

        if (isAdminConnected_coach()) {
            $checkParams = $this->checkParams(array(
                array(POST, "nom"),
                array(POST, "ville")
            ));
            if ($checkParams) {
                $getId = $_GET["id"];
                $postNom = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["nom"]));
                $postAdresse = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["adresse"]));
                $postCodePostal = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["codePostal"]));
                $postVille = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["ville"]));
                $postLatitude = StringUtils::convertEmptyStringToNull(trim($_POST["latitude"]));
                $postLongitude = StringUtils::convertEmptyStringToNull(trim($_POST["longitude"]));
                $postADomicile = $_POST["aDomicile"];

                $gymnaseDAO = new GymnaseDAO($this->getDatabaseConnection());

                $gymnase = array();
                if (HTTPUtils::paramExists(GET, "id")) {
                    $gymnaseById = $gymnaseDAO->getById($getId);
                    if ($gymnaseById != null) {
                        $gymnase = $gymnaseById;
                    } else {
                        $response->success = false;
                        $this->sendCheckError(HTTP_404, "Le gymnase (idGymnase = '".$getId."') n'existe pas");
                    }
                }

                $gymnase[GYMNASE_NOM] = $postNom;
                $gymnase[GYMNASE_ADRESSE] = $postAdresse;
                $gymnase[GYMNASE_CODE_POSTAL] = $postCodePostal;
                $gymnase[GYMNASE_VILLE] = $postVille;
                $gymnase[GYMNASE_LATITUDE] = $postLatitude;
                $gymnase[GYMNASE_LONGITUDE] = $postLongitude;
                $gymnase[GYMNASE_A_DOMICILE] = $postADomicile;

                $actionType = ACTION_TYPE_CREATION;
                if (array_key_exists(GYMNASE_ID, $gymnase)) {
                    $actionType = ACTION_TYPE_MODIFICATION;
                    $gymnaseDAO->update($gymnase);
                } else {
                    $gymnase[GYMNASE_ID] = $gymnaseDAO->create($gymnase);
                }

                if (!isAdminConnected_superadmin()) {
                    MailUtils::notifySuperadmin(
                        GYMNASE_TABLE_NAME, 
                        $actionType, 
                        array(
                            array(
                                OBJET_ASSOCIE_TYPE => GYMNASE_TABLE_NAME,
                                OBJET_ASSOCIE_ID => $gymnase[GYMNASE_ID],
                                OBJET_ASSOCIE_LABEL => $gymnase[GYMNASE_NOM]." - ".$gymnase[GYMNASE_VILLE]
                            )
                        ), 
                        $this->getDatabaseConnection()
                    );
                }

                $response->success = true;
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function delete() {
        $this->logNavigation("delete", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_coach()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                $postId = trim($_POST["id"]);

                $gymnaseDAO = new GymnaseDAO($this->getDatabaseConnection());
                $evenementDAO = new EvenementDAO($this->getDatabaseConnection());
            
                $gymnase = $gymnaseDAO->getById($postId);
                if ($gymnase != null) {
                    $evenementsGymnase = $evenementDAO->getByGymnaseId($postId);
                    if (sizeof($evenementsGymnase) > 0) {
                        foreach ($evenementsGymnase as $evenementGymnase) {
                            $evenementGymnase[EVENEMENT_GYMNASE_ID] = null;
                            $evenementDAO->update($evenementGymnase);
                        }
                    }

                    $gymnaseDAO->delete($postId);

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            GYMNASE_TABLE_NAME, 
                            ACTION_TYPE_SUPPRESSION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => GYMNASE_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $gymnase[GYMNASE_ID],
                                    OBJET_ASSOCIE_LABEL => $gymnase[GYMNASE_NOM]." - ".$gymnase[GYMNASE_VILLE]
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le gymnase (idGymnase = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    private function getGymnaseResponseObject($gymnase) {
        $gymnaseResponseObject = array();

        $gymnaseResponseObject = array();
        $gymnaseResponseObject["id"] = $gymnase[GYMNASE_ID];
        $gymnaseResponseObject["nom"] = $gymnase[GYMNASE_NOM];
        $gymnaseResponseObject["adresse"] = $gymnase[GYMNASE_ADRESSE];
        $gymnaseResponseObject["codePostal"] = $gymnase[GYMNASE_CODE_POSTAL];
        $gymnaseResponseObject["ville"] = $gymnase[GYMNASE_VILLE];
        $gymnaseResponseObject["latitude"] = $gymnase[GYMNASE_LATITUDE];
        $gymnaseResponseObject["longitude"] = $gymnase[GYMNASE_LONGITUDE];
        $gymnaseResponseObject["aDomicile"] = $gymnase[GYMNASE_A_DOMICILE];

        return $gymnaseResponseObject;
    }
}