<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/constants/session-constants.php");
require_once("common/php/dao/mots-de-passes-dao.php");

class ConnexionServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("connexion", true);
    }

    public function checkAdmin() {
        $this->logNavigation("checkAdmin", array(
            "mdp" => $_POST["mdp"]
        ));

        $postMdp = $_POST["mdp"];

        $response = new stdClass;

        $response->isMdpValid = false;
        $motsDePasseDAO = new MotsDePassesDAO($this->getDatabaseConnection());
        $motsDePasse = $motsDePasseDAO->getSingleton();
        
        if (md5($postMdp) == $motsDePasse[MOTS_DE_PASSES_SUPERADMIN]) {
            $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_SUPERADMIN] = true;
            $response->isMdpValid = true;
            $response->adminProfile = ADMIN_SUPERADMIN;
        } else if (md5($postMdp) == $motsDePasse[MOTS_DE_PASSES_BUREAU]) {
            $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_BUREAU] = true;
            $response->isMdpValid = true;
            $response->adminProfile = ADMIN_BUREAU;
        } else if (md5($postMdp) == $motsDePasse[MOTS_DE_PASSES_COACH]) {
            $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_COACH] = true;
            $response->isMdpValid = true;
            $response->adminProfile = ADMIN_COACH;
        } else if (md5($postMdp) == $motsDePasse[MOTS_DE_PASSES_EDITEUR]) {
            $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_EDITEUR] = true;
            $response->isMdpValid = true;
            $response->adminProfile = ADMIN_EDITEUR;
        } else if (md5($postMdp) == $motsDePasse[MOTS_DE_PASSES_COMMERCIAL]) {
            $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_COMMERCIAL] = true;
            $response->isMdpValid = true;
            $response->adminProfile = ADMIN_COMMERCIAL;
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function checkDispos() {
        $this->logNavigation("checkDispos", array(
            "mdp" => $_POST["mdp"]
        ));

        $postMdp = $_POST["mdp"];

        $response = new stdClass;

        $response->isMdpValid = false;
        $motsDePasseDAO = new MotsDePassesDAO($this->getDatabaseConnection());
        $motsDePasse = $motsDePasseDAO->getSingleton();
        if (md5($postMdp) == $motsDePasse[MOTS_DE_PASSES_DISPOS]) {
            $_SESSION[SESSION_HBCSJ][IS_DISPOS_CONNECTED] = true;
            $response->isMdpValid = true;
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function logout() {
        session_destroy();
        $response = new stdClass;
        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}

?>