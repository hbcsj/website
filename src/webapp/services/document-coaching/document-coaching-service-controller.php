<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("common/php/dao/document-coaching-dao.php");

class DocumentCoachingServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("document-coaching", true);
    }

    public function delete() {
        $this->logNavigation("delete", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_coach()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                $postId = trim($_POST["id"]);

                $documentCoachingDAO = new DocumentCoachingDAO($this->getDatabaseConnection());

                $document = $documentCoachingDAO->getById($postId);
                if ($document != null) {
                    $documentCoachingDAO->delete($postId); 
                    
                    if ($document[DOCUMENT_COACHING_FICHIER] != null && $document[DOCUMENT_COACHING_FICHIER] != "") {
                        $fichier = PathUtils::getDocumentCoachingFile($document[DOCUMENT_COACHING_FICHIER], $document[DOCUMENT_COACHING_ID]);
                        if (file_exists($fichier)) {
                            unlink($fichier);
                        }
                    }

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            DOCUMENT_COACHING_TABLE_NAME, 
                            ACTION_TYPE_SUPPRESSION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => DOCUMENT_COACHING_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $document[DOCUMENT_COACHING_ID],
                                    OBJET_ASSOCIE_LABEL => DOCUMENT_COACHING_TITRE
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le document coaching (idDocumentCoaching = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}