<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/dispos-utils.php");
require_once("common/php/dao/dispo-dao.php");
require_once("common/php/dao/histo-dispo-dao.php");

class DispoServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("dispo", true);
    }

    public function save() {
        $this->logNavigation("save", array(
            "licencieId" => $_POST["licencieId"],
            "typeId" => $_POST["typeId"],
            "reponseId" => $_POST["reponseId"],
            "evenementIds" => $_POST["evenementIds"]
        ));

        $response = new stdClass;

        if (isDisposConnected()) {
            $checkParams = $this->checkParams(array(
                array(POST, "licencieId"),
                array(POST, "typeId"),
                array(POST, "reponseId"),
                array(POST, "evenementIds")
            ));
            if ($checkParams) {
                require_once("common/php/dao/licencie-dao.php");
                require_once("common/php/dao/type-dispo-dao.php");
                require_once("common/php/dao/reponse-dispo-dao.php");
                require_once("common/php/dao/evenement-dao.php");

                $postLicencieId = trim($_POST["licencieId"]);
                $postTypeId = trim($_POST["typeId"]);
                $postReponseId = trim($_POST["reponseId"]);
                $postEvenementIds = explode(",", $_POST["evenementIds"]);

                $licencieDAO = new LicencieDAO($this->getDatabaseConnection());
                if ($licencieDAO->getById($postLicencieId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le licencie (idLicencie = '".$postLicencieId."') n'existe pas");
                }

                $typeDispoDAO = new TypeDispoDAO($this->getDatabaseConnection());
                if ($typeDispoDAO->getById($postTypeId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le type de dispo (idTypeDispo = '".$postTypeId."') n'existe pas");
                }

                $reponseDispoDAO = new ReponseDispoDAO($this->getDatabaseConnection());
                if ($postReponseId != DISPO_PAS_DE_REPONSE_REPONSE_ID && $reponseDispoDAO->getById($postReponseId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La reponse de dispo (idReponseDispo = '".$postReponseId."') n'existe pas");
                }

                $evenementDAO = new EvenementDAO($this->getDatabaseConnection());
                foreach ($postEvenementIds as $evenementId) {
                    if ($evenementDAO->getById($evenementId) == null) {
                        $response->success = false;
                        $this->sendCheckError(HTTP_404, "L'evenement' (idEvenement = '".$evenementId."') n'existe pas");
                    }
                }

                $dispoDAO = new DispoDAO($this->getDatabaseConnection());
                $histoDispoDAO = new HistoDispoDAO($this->getDatabaseConnection());

                foreach ($postEvenementIds as $evenementId) {
                    $dispo = array();
                    $dispo[DISPO_LICENCIE_ID] = $postLicencieId;
                    $dispo[DISPO_EVENEMENT_ID] = $evenementId;
                    $dispo[DISPO_TYPE_DISPO_ID] = $postTypeId;
                    $dispoDAO->delete($dispo);
                    if ($postReponseId != DISPO_PAS_DE_REPONSE_REPONSE_ID) {
                        $dispo[DISPO_REPONSE_DISPO_ID] = $postReponseId;
                        $dispoDAO->create($dispo);
                    }

                    $histoDispo = array();
                    $histoDispo[HISTO_DISPO_DATE_HEURE] = date(SQL_DATE_TIME_FORMAT);
                    $histoDispo[HISTO_DISPO_LICENCIE_ID] = $postLicencieId;
                    $histoDispo[HISTO_DISPO_EVENEMENT_ID] = $evenementId;
                    $histoDispo[HISTO_DISPO_TYPE_DISPO_ID] = $postTypeId;
                    if ($postReponseId != DISPO_PAS_DE_REPONSE_REPONSE_ID) {
                        $histoDispo[HISTO_DISPO_REPONSE_DISPO_ID] = $postReponseId;
                    }
                    $histoDispoDAO->create($histoDispo);
                }

                $response->success = true;
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function saveAction() {
        $this->logNavigation("saveAction", array(
            "licencieId" => $_POST["licencieId"],
            "typeId" => $_POST["typeId"],
            "evenementId" => $_POST["evenementId"]
        ));

        $response = new stdClass;

        if (isDisposConnected()) {
            $checkParams = $this->checkParams(array(
                array(POST, "licencieId"),
                array(POST, "typeId"),
                array(POST, "evenementId")
            ));
            if ($checkParams) {
                require_once("common/php/dao/licencie-dao.php");
                require_once("common/php/dao/type-dispo-dao.php");
                require_once("common/php/dao/evenement-dao.php");

                $postLicencieId = trim($_POST["licencieId"]);
                $postTypeId = trim($_POST["typeId"]);
                $postEvenementId = trim($_POST["evenementId"]);

                $licencieDAO = new LicencieDAO($this->getDatabaseConnection());
                if ($licencieDAO->getById($postLicencieId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le licencie (idLicencie = '".$postLicencieId."') n'existe pas");
                }

                $typeDispoDAO = new TypeDispoDAO($this->getDatabaseConnection());
                if ($typeDispoDAO->getById($postTypeId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le type de dispo (idTypeDispo = '".$postTypeId."') n'existe pas");
                }

                $evenementDAO = new EvenementDAO($this->getDatabaseConnection());
                if ($evenementDAO->getById($postEvenementId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "L'evenement' (idEvenement = '".$postEvenementId."') n'existe pas");
                }

                $dispoDAO = new DispoDAO($this->getDatabaseConnection());
                $histoDispoDAO = new HistoDispoDAO($this->getDatabaseConnection());

                $dispo = array();
                $dispo[DISPO_LICENCIE_ID] = $postLicencieId;
                $dispo[DISPO_EVENEMENT_ID] = $postEvenementId;
                $dispo[DISPO_TYPE_DISPO_ID] = $postTypeId;
                $dispoDAO->delete($dispo);
                $dispo[DISPO_REPONSE_DISPO_ID] = DISPO_DISPONIBLE_REPONSE_ID;
                $dispoDAO->create($dispo);

                $histoDispo = array();
                $histoDispo[HISTO_DISPO_DATE_HEURE] = date(SQL_DATE_TIME_FORMAT);
                $histoDispo[HISTO_DISPO_LICENCIE_ID] = $postLicencieId;
                $histoDispo[HISTO_DISPO_EVENEMENT_ID] = $postEvenementId;
                $histoDispo[HISTO_DISPO_TYPE_DISPO_ID] = $postTypeId;
                $histoDispo[HISTO_DISPO_REPONSE_DISPO_ID] = DISPO_DISPONIBLE_REPONSE_ID;
                $histoDispoDAO->create($histoDispo);

                $response->success = true;
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function deleteAction() {
        $this->logNavigation("deleteAction", array(
            "licencieId" => $_POST["licencieId"],
            "typeId" => $_POST["typeId"],
            "evenementId" => $_POST["evenementId"]
        ));

        $response = new stdClass;

        if (isDisposConnected()) {
            $checkParams = $this->checkParams(array(
                array(POST, "licencieId"),
                array(POST, "typeId"),
                array(POST, "evenementId")
            ));
            if ($checkParams) {
                require_once("common/php/dao/licencie-dao.php");
                require_once("common/php/dao/type-dispo-dao.php");
                require_once("common/php/dao/evenement-dao.php");

                $postLicencieId = trim($_POST["licencieId"]);
                $postTypeId = trim($_POST["typeId"]);
                $postEvenementId = trim($_POST["evenementId"]);

                $licencieDAO = new LicencieDAO($this->getDatabaseConnection());
                if ($licencieDAO->getById($postLicencieId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le licencie (idLicencie = '".$postLicencieId."') n'existe pas");
                }

                $typeDispoDAO = new TypeDispoDAO($this->getDatabaseConnection());
                if ($typeDispoDAO->getById($postTypeId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le type de dispo (idTypeDispo = '".$postTypeId."') n'existe pas");
                }

                $evenementDAO = new EvenementDAO($this->getDatabaseConnection());
                if ($evenementDAO->getById($postEvenementId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "L'evenement' (idEvenement = '".$postEvenementId."') n'existe pas");
                }

                $dispoDAO = new DispoDAO($this->getDatabaseConnection());
                $histoDispoDAO = new HistoDispoDAO($this->getDatabaseConnection());
                $dispoId = array();
                $dispoId[DISPO_LICENCIE_ID] = $postLicencieId;
                $dispoId[DISPO_EVENEMENT_ID] = $postEvenementId;
                $dispoId[DISPO_TYPE_DISPO_ID] = $postTypeId;
                $dispoDAO->delete($dispoId);

                $histoDispo = array();
                $histoDispo[HISTO_DISPO_DATE_HEURE] = date(SQL_DATE_TIME_FORMAT);
                $histoDispo[HISTO_DISPO_LICENCIE_ID] = $postLicencieId;
                $histoDispo[HISTO_DISPO_EVENEMENT_ID] = $postEvenementId;
                $histoDispo[HISTO_DISPO_TYPE_DISPO_ID] = $postTypeId;
                $histoDispo[HISTO_DISPO_REPONSE_DISPO_ID] = DISPO_PAS_DE_REPONSE_REPONSE_ID;
                $histoDispoDAO->create($histoDispo);

                $response->success = true;
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}