<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/dao/fournisseur-produit-boutique-dao.php");

class FournisseurProduitBoutiqueServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("fournisseur-produit-boutique", true);
    }

    public function save() {
        $this->logNavigation("save", array(
            "id" => $_GET["id"],
            "nom" => $_POST["nom"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(POST, "nom")
            ));
            if ($checkParams) {
                $getId = $_GET["id"];
                $postNom = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["nom"]));

                $fournisseurProduitBoutiqueDAO = new FournisseurProduitBoutiqueDAO($this->getDatabaseConnection());

                $fournisseurProduitBoutique = array();
                if (HTTPUtils::paramExists(GET, "id")) {
                    $fournisseurProduitBoutiqueById = $fournisseurProduitBoutiqueDAO->getById($getId);
                    if ($fournisseurProduitBoutiqueById != null) {
                        $fournisseurProduitBoutique = $fournisseurProduitBoutiqueById;
                    } else {
                        $response->success = false;
                        $this->sendCheckError(HTTP_404, "Le fournisseurProduitBoutique (idFournisseurProduitBoutique = '".$getId."') n'existe pas");
                    }
                }

                $fournisseurProduitBoutique[FOURNISSEUR_PRODUIT_BOUTIQUE_NOM] = $postNom;

                $actionType = ACTION_TYPE_CREATION;
                if (array_key_exists(FOURNISSEUR_PRODUIT_BOUTIQUE_ID, $fournisseurProduitBoutique)) {
                    $actionType = ACTION_TYPE_MODIFICATION;
                    $fournisseurProduitBoutiqueDAO->update($fournisseurProduitBoutique);
                } else {
                    $fournisseurProduitBoutique[FOURNISSEUR_PRODUIT_BOUTIQUE_ID] = $fournisseurProduitBoutiqueDAO->create($fournisseurProduitBoutique);
                }

                if (!isAdminConnected_superadmin()) {
                    MailUtils::notifySuperadmin(
                        FOURNISSEUR_PRODUIT_BOUTIQUE_TABLE_NAME, 
                        $actionType, 
                        array(
                            array(
                                OBJET_ASSOCIE_TYPE => FOURNISSEUR_PRODUIT_BOUTIQUE_TABLE_NAME,
                                OBJET_ASSOCIE_ID => $fournisseurProduitBoutique[FOURNISSEUR_PRODUIT_BOUTIQUE_ID],
                                OBJET_ASSOCIE_LABEL => $fournisseurProduitBoutique[FOURNISSEUR_PRODUIT_BOUTIQUE_NOM]
                            )
                        ), 
                        $this->getDatabaseConnection()
                    );
                }

                $response->success = true;
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function delete() {
        $this->logNavigation("delete", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                require_once("common/php/dao/commande-fournisseur-boutique-dao.php");
                require_once("common/php/dao/livraison-fournisseur-boutique-dao.php");
                require_once("common/php/dao/produit-boutique-dao.php");

                $postId = trim($_POST["id"]);

                $fournisseurProduitBoutiqueDAO = new FournisseurProduitBoutiqueDAO($this->getDatabaseConnection());
                $commandeFournisseurBoutiqueDAO = new CommandeFournisseurBoutiqueDAO($this->getDatabaseConnection());
                $livraisonFournisseurBoutiqueDAO = new LivraisonFournisseurBoutiqueDAO($this->getDatabaseConnection());
                $produitBoutiqueDAO = new ProduitBoutiqueDAO($this->getDatabaseConnection());
            
                $fournisseurProduitBoutique = $fournisseurProduitBoutiqueDAO->getById($postId);
                if ($fournisseurProduitBoutique != null) {
                    if (
                        sizeof($commandeFournisseurBoutiqueDAO->getByFournisseurProduitBoutiqueId($postId)) == 0 && 
                        sizeof($livraisonFournisseurBoutiqueDAO->getByFournisseurProduitBoutiqueId($postId)) == 0 && 
                        sizeof($produitBoutiqueDAO->getByFournisseurProduitBoutiqueId($postId)) == 0
                        ) {
                        $fournisseurProduitBoutiqueDAO->delete($postId);

                        if (!isAdminConnected_superadmin()) {
                            MailUtils::notifySuperadmin(
                                FOURNISSEUR_PRODUIT_BOUTIQUE_TABLE_NAME, 
                                ACTION_TYPE_SUPPRESSION, 
                                array(
                                    array(
                                        OBJET_ASSOCIE_TYPE => FOURNISSEUR_PRODUIT_BOUTIQUE_TABLE_NAME,
                                        OBJET_ASSOCIE_ID => $fournisseurProduitBoutique[FOURNISSEUR_PRODUIT_BOUTIQUE_ID],
                                        OBJET_ASSOCIE_LABEL => $fournisseurProduitBoutique[FOURNISSEUR_PRODUIT_BOUTIQUE_NOM]
                                    )
                                ), 
                                $this->getDatabaseConnection()
                            );
                        }

                        $response->success = true;
                    } else {
                        $response->success = false;
                        $this->sendCheckError(HTTP_400, "Le fournisseurProduitBoutique (idFournisseurProduitBoutique = '".$postId."') a des commandes / livraisons associees");
                    }
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le fournisseurProduitBoutique (idFournisseurProduitBoutique = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}