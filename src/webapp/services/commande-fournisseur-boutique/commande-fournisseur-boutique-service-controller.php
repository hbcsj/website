<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/dao/commande-fournisseur-boutique-dao.php");

class CommandeFournisseurBoutiqueServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("commande-fournisseur-boutique", true);
    }

    public function save() {
        $this->logNavigation("save", array(
            "id" => $_GET["id"],
            "date" => $_POST["date"],
            "fournisseurProduitBoutiqueId" => $_POST["fournisseurProduitBoutiqueId"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(POST, "date"),
                array(POST, "fournisseurProduitBoutiqueId")
            ));
            if ($checkParams) {
                require_once("common/php/dao/fournisseur-produit-boutique-dao.php");

                $getId = $_GET["id"];
                $postDate = DateUtils::convert_slashDate_to_sqlDate(trim($_POST["date"]));
                $postFournisseurProduitBoutiqueId = trim($_POST["fournisseurProduitBoutiqueId"]);
                
                $fournisseurProduitBoutiqueDAO = new FournisseurProduitBoutiqueDAO($this->getDatabaseConnection());
                if ($fournisseurProduitBoutiqueDAO->getById($postFournisseurProduitBoutiqueId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le fournisseurProduitBoutique (idFournisseurProduitBoutique = '".$postFournisseurProduitBoutiqueId."') n'existe pas");
                }

                $commandeFournisseurBoutiqueDAO = new CommandeFournisseurBoutiqueDAO($this->getDatabaseConnection());

                $commandeFournisseurBoutique = array();
                if (HTTPUtils::paramExists(GET, "id")) {
                    $commandeFournisseurBoutiqueById = $commandeFournisseurBoutiqueDAO->getById($getId);
                    if ($commandeFournisseurBoutiqueById != null) {
                        $commandeFournisseurBoutique = $commandeFournisseurBoutiqueById;
                    } else {
                        $response->success = false;
                        $this->sendCheckError(HTTP_404, "La commandeFournisseurBoutique (idCommandeFournisseurBoutique = '".$getId."') n'existe pas");
                    }
                }

                $commandeFournisseurBoutique[COMMANDE_FOURNISSEUR_BOUTIQUE_DATE] = $postDate;
                $commandeFournisseurBoutique[COMMANDE_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID] = $postFournisseurProduitBoutiqueId;

                $actionType = ACTION_TYPE_CREATION;
                if (array_key_exists(COMMANDE_FOURNISSEUR_BOUTIQUE_ID, $commandeFournisseurBoutique)) {
                    $actionType = ACTION_TYPE_MODIFICATION;
                    $commandeFournisseurBoutiqueDAO->update($commandeFournisseurBoutique);
                } else {
                    $commandeFournisseurBoutique[COMMANDE_FOURNISSEUR_BOUTIQUE_ID] = $commandeFournisseurBoutiqueDAO->create($commandeFournisseurBoutique);
                }

                if (!isAdminConnected_superadmin()) {
                    MailUtils::notifySuperadmin(
                        COMMANDE_FOURNISSEUR_BOUTIQUE_TABLE_NAME, 
                        $actionType, 
                        array(
                            array(
                                OBJET_ASSOCIE_TYPE => COMMANDE_FOURNISSEUR_BOUTIQUE_TABLE_NAME,
                                OBJET_ASSOCIE_ID => $commandeFournisseurBoutique[COMMANDE_FOURNISSEUR_BOUTIQUE_ID],
                                OBJET_ASSOCIE_LABEL => $commandeFournisseurBoutique[COMMANDE_FOURNISSEUR_BOUTIQUE_DATE]
                            )
                        ), 
                        $this->getDatabaseConnection()
                    );
                }

                $response->success = true;
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function delete() {
        $this->logNavigation("delete", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                require_once("common/php/dao/commande-item-boutique-dao.php");

                $postId = trim($_POST["id"]);

                $commandeFournisseurBoutiqueDAO = new CommandeFournisseurBoutiqueDAO($this->getDatabaseConnection());
                $commandeItemBoutiqueDAO = new CommandeItemBoutiqueDAO($this->getDatabaseConnection());
            
                $commandeFournisseurBoutique = $commandeFournisseurBoutiqueDAO->getById($postId);
                if ($commandeFournisseurBoutique != null) {
                    if (sizeof($commandeItemBoutiqueDAO->getByCommandeFournisseurBoutiqueId($postId)) == 0) {
                        $commandeFournisseurBoutiqueDAO->delete($postId);

                        if (!isAdminConnected_superadmin()) {
                            MailUtils::notifySuperadmin(
                                COMMANDE_FOURNISSEUR_BOUTIQUE_TABLE_NAME, 
                                ACTION_TYPE_SUPPRESSION, 
                                array(
                                    array(
                                        OBJET_ASSOCIE_TYPE => COMMANDE_FOURNISSEUR_BOUTIQUE_TABLE_NAME,
                                        OBJET_ASSOCIE_ID => $commandeFournisseurBoutique[COMMANDE_FOURNISSEUR_BOUTIQUE_ID],
                                        OBJET_ASSOCIE_LABEL => $commandeFournisseurBoutique[COMMANDE_FOURNISSEUR_BOUTIQUE_DATE]
                                    )
                                ), 
                                $this->getDatabaseConnection()
                            );
                        }

                        $response->success = true;
                    } else {
                        $response->success = false;
                        $this->sendCheckError(HTTP_400, "La commandeFournisseurBoutique (idCommandeFournisseurBoutique = '".$postId."') a des commandes / livraisons associees");
                    }
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La commandeFournisseurBoutique (idCommandeFournisseurBoutique = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}