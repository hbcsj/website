<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/dao/commande-item-billetterie-dao.php");

class CommandeBilletterieServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("commande-billetterie", true);
    }

    public function saveLinkPack() {
        $this->logNavigation("saveLinkPack", array(
            "id" => $_GET["id"],
            "packBilletterieId" => $_POST["packBilletterieId"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(GET, "id")
            ));
            if ($checkParams) {
                require_once("common/php/dao/pack-billetterie-dao.php");

                $getId = $_GET["id"];
                $postPackBilletterieId = StringUtils::convertEmptyStringToNull(trim($_POST["packBilletterieId"]));

                $packBilletterieDAO = new PackBilletterieDAO($this->getDatabaseConnection());
                if ($postPackBilletterieId != null && $packBilletterieDAO->getById($postPackBilletterieId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le pack billetterie (idPackBilletterie = '".$postPackBilletterieId."') n'existe pas");
                }

                $commandeItemBilletterieDAO = new CommandeItemBilletterieDAO($this->getDatabaseConnection());

                $commandeItemBilletterie = array();
                $commandeItemBilletterieById = $commandeItemBilletterieDAO->getById($getId);
                if ($commandeItemBilletterieById != null) {
                    $commandeItemBilletterie = $commandeItemBilletterieById;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La commande billetterie (idCommandeItemBilletterie = '".$getId."') n'existe pas");
                }

                $commandeItemBilletterie[COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID] = $postPackBilletterieId;

                $commandeItemBilletterieDAO->update($commandeItemBilletterie);

                if (!isAdminConnected_superadmin()) {
                    MailUtils::notifySuperadmin(
                        COMMANDE_ITEM_BILLETTERIE_TABLE_NAME, 
                        ACTION_TYPE_MODIFICATION, 
                        array(
                            array(
                                OBJET_ASSOCIE_TYPE => COMMANDE_ITEM_BILLETTERIE_TABLE_NAME,
                                OBJET_ASSOCIE_ID => $commandeItemBilletterie[COMMANDE_ITEM_BILLETTERIE_ID],
                                OBJET_ASSOCIE_LABEL => COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID."=".$postPackBilletterieId
                            )
                        ), 
                        $this->getDatabaseConnection()
                    );
                }

                $response->success = true;
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function toggleDonne() {
        $this->logNavigation("toggleDonne", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                $postId = trim($_POST["id"]);

                $commandeItemBilletterieDAO = new CommandeItemBilletterieDAO($this->getDatabaseConnection());
            
                $commandeItemBilletterie = $commandeItemBilletterieDAO->getById($postId);
                if ($commandeItemBilletterie != null) {
                    if ($commandeItemBilletterie[COMMANDE_ITEM_BILLETTERIE_DONNE] == 1) {
                        $commandeItemBilletterie[COMMANDE_ITEM_BILLETTERIE_DONNE] = 0;
                    } else {
                        $commandeItemBilletterie[COMMANDE_ITEM_BILLETTERIE_DONNE] = 1;
                    }
                    $commandeItemBilletterieDAO->update($commandeItemBilletterie);

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            COMMANDE_ITEM_BILLETTERIE_TABLE_NAME, 
                            ACTION_TYPE_MODIFICATION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => COMMANDE_ITEM_BILLETTERIE_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $commandeItemBilletterie[COMMANDE_ITEM_BILLETTERIE_ID],
                                    OBJET_ASSOCIE_LABEL => COMMANDE_ITEM_BILLETTERIE_DONNE."=".$commandeItemBilletterie[COMMANDE_ITEM_BILLETTERIE_DONNE]
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La commande billetterie (idCommandeItemBilletterie = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}