<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/dao/licencie-dao.php");

class LicencieServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("licencie", true);
    }

    public function getMailingList() {
        $this->logNavigation("getMailingList", array(
            "ids" => $_GET["ids"]
        ));

        $response = new stdClass;

        if (isAdminConnected_coach()) {
            $checkParams = $this->checkParams(array(
                array(GET, "ids")
            ));
            if ($checkParams) {
                $getIds = $_GET["ids"];

                $response->mailingList = array();

                $licencieDAO = new LicencieDAO($this->getDatabaseConnection());

                $licencies = $licencieDAO->getInIds($getIds);

                if (sizeof($licencies) > 0) {
                    foreach ($licencies as $licencie) {
                        if ($licencie[LICENCIE_EMAIL_1] != "") {
                            if (!in_array($licencie[LICENCIE_EMAIL_1], $response->mailingList)) {
                                $response->mailingList[] = $licencie[LICENCIE_EMAIL_1];
                            }
                        }
                        if ($licencie[LICENCIE_EMAIL_2] != "") {
                            if (!in_array($licencie[LICENCIE_EMAIL_2], $response->mailingList)) {
                                $response->mailingList[] = $licencie[LICENCIE_EMAIL_2];
                            }
                        }
                        if ($licencie[LICENCIE_EMAIL_3] != "") {
                            if (!in_array($licencie[LICENCIE_EMAIL_3], $response->mailingList)) {
                                $response->mailingList[] = $licencie[LICENCIE_EMAIL_3];
                            }
                        }
                    }
                }
            }
        } else {
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function save() {
        $this->logNavigation("save", array(
            "id" => $_GET["id"],
            "nom" => $_POST["nom"],
            "prenom" => $_POST["prenom"],
            "dateNaissance" => $_POST["dateNaissance"],
            "adresse" => $_POST["adresse"],
            "codePostal" => $_POST["codePostal"],
            "ville" => $_POST["ville"],
            "telephone1" => $_POST["telephone1"],
            "telephone2" => $_POST["telephone2"],
            "telephone3" => $_POST["telephone3"],
            "email1" => $_POST["email1"],
            "email2" => $_POST["email2"],
            "email3" => $_POST["email3"],
            "gardien" => $_POST["gardien"],
            "arbitre" => $_POST["arbitre"],
            "bureau" => $_POST["bureau"],
            "ca" => $_POST["ca"],
            "lateraliteId" => $_POST["lateraliteId"],
            "categorieIds" => $_POST["categorieIds"]
        ));

        $response = new stdClass;

        if (isAdminConnected_coach()) {
            $checkParams = $this->checkParams(array(
                array(POST, "nom"),
                array(POST, "prenom")
            ));
            if ($checkParams) {
                require_once("common/php/dao/categorie-dao.php");
                require_once("common/php/dao/lateralite-dao.php");
                require_once("common/php/dao/licencie-joue-dans-categorie-dao.php");

                $getId = $_GET["id"];
                $postNom = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["nom"]));
                $postPrenom = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["prenom"]));
                $postDateNaissance = DateUtils::convert_slashDate_to_sqlDate(trim($_POST["dateNaissance"]));
                $postAdresse = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["adresse"]));
                $postCodePostal = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["codePostal"]));
                $postVille = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["ville"]));
                $postTelephone1 = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["telephone1"]));
                $postTelephone2 = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["telephone2"]));
                $postTelephone3 = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["telephone3"]));
                $postEmail1 = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["email1"]));
                $postEmail2 = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["email2"]));
                $postEmail3 = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["email3"]));
                $postGardien = $_POST["gardien"];
                $postArbitre = $_POST["arbitre"];
                $postBureau = $_POST["bureau"];
                $postCA = $_POST["ca"];
                $postLateraliteId = StringUtils::convertEmptyStringToNull(trim($_POST["lateraliteId"]));
                $postCategorieIds = ($_POST["categorieIds"] != "") ? $_POST["categorieIds"] : array();

                $categorieDAO = new CategorieDAO($this->getDatabaseConnection());
                if (sizeof($postCategorieIds) > 0) {
                    foreach ($postCategorieIds as $categorieId) {
                        if ($categorieId != "" && $categorieDAO->getById($categorieId) == null) {
                            $response->success = false;
                            $this->sendCheckError(HTTP_404, "La categorie (idCategorie = '".$categorieId."') n'existe pas");
                        }
                    }
                }

                $lateraliteDAO = new LateraliteDAO($this->getDatabaseConnection());
                if ($postLateraliteId != null && $lateraliteDAO->getById($postLateraliteId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La lateralite (idLateralite = '".$postLateraliteId."') n'existe pas");
                }

                $licencieDAO = new LicencieDAO($this->getDatabaseConnection());
                $licencieJoueDansCategorieDAO = new LicencieJoueDansCategorieDAO($this->getDatabaseConnection());

                $licencie = array();
                if (HTTPUtils::paramExists(GET, "id")) {
                    $licencieById = $licencieDAO->getById($getId);
                    if ($licencieById != null) {
                        $licencie = $licencieById;
                    } else {
                        $response->success = false;
                        $this->sendCheckError(HTTP_404, "Le licencie (idLicencie = '".$getId."') n'existe pas");
                    }
                }

                $licencie[LICENCIE_NOM] = $postNom;
                $licencie[LICENCIE_PRENOM] = $postPrenom;
                $licencie[LICENCIE_DATE_NAISSANCE] = $postDateNaissance;
                $licencie[LICENCIE_ADRESSE] = $postAdresse;
                $licencie[LICENCIE_CODE_POSTAL] = $postCodePostal;
                $licencie[LICENCIE_VILLE] = $postVille;
                $licencie[LICENCIE_TELEPHONE_1] = $postTelephone1;
                $licencie[LICENCIE_TELEPHONE_2] = $postTelephone2;
                $licencie[LICENCIE_TELEPHONE_3] = $postTelephone3;
                $licencie[LICENCIE_EMAIL_1] = $postEmail1;
                $licencie[LICENCIE_EMAIL_2] = $postEmail2;
                $licencie[LICENCIE_EMAIL_3] = $postEmail3;
                $licencie[LICENCIE_GARDIEN] = $postGardien;
                $licencie[LICENCIE_ARBITRE] = $postArbitre;
                $licencie[LICENCIE_BUREAU] = $postBureau;
                $licencie[LICENCIE_CA] = $postCA;
                $licencie[LICENCIE_LATERALITE_ID] = $postLateraliteId;

                $actionType = ACTION_TYPE_CREATION;
                if (array_key_exists(LICENCIE_ID, $licencie)) {
                    $actionType = ACTION_TYPE_MODIFICATION;
                    
                    if (
                        sizeof($postCategorieIds) > 0 || 
                        $licencie[LICENCIE_ARBITRE] == 1  || 
                        $licencie[LICENCIE_BUREAU] == 1  || 
                        $licencie[LICENCIE_CA] == 1
                    ) {
                        $licencie[LICENCIE_ACTIF] = "1";
                    }

                    $licencieDAO->update($licencie);
                    $licencieJoueDansCategorieDAO->deleteByLicencieId($licencie[LICENCIE_ID]);
                } else {
                    $licencie[LICENCIE_ACTIF] = "1";
                    $licencie[LICENCIE_ID] = $licencieDAO->create($licencie);
                }

                foreach ($postCategorieIds as $categorieId) {
                    if ($categorieId != "" && $licencie[LICENCIE_ID] != 0) {
                        $licencieJoueDansCategorie = array();
                        $licencieJoueDansCategorie[LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID] = $licencie[LICENCIE_ID];
                        $licencieJoueDansCategorie[LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID] = $categorieId;
                        $licencieJoueDansCategorieDAO->create($licencieJoueDansCategorie);
                    }
                }

                if (!isAdminConnected_superadmin()) {
                    MailUtils::notifySuperadmin(
                        LICENCIE_TABLE_NAME, 
                        $actionType, 
                        array(
                            array(
                                OBJET_ASSOCIE_TYPE => LICENCIE_TABLE_NAME,
                                OBJET_ASSOCIE_ID => $licencie[LICENCIE_ID],
                                OBJET_ASSOCIE_LABEL => $licencie[LICENCIE_NOM]." ".$licencie[LICENCIE_PRENOM]
                            )
                        ), 
                        $this->getDatabaseConnection()
                    );
                }

                $response->success = true;
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function toggleActif() {
        $this->logNavigation("toggleActif", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_coach()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                require_once("common/php/dao/licencie-coache-categorie-dao.php");
                require_once("common/php/dao/licencie-joue-dans-categorie-dao.php");

                $postId = trim($_POST["id"]);

                $licencieDAO = new LicencieDAO($this->getDatabaseConnection());
                $licencieCoacheCategorieDAO = new LicencieCoacheCategorieDAO($this->getDatabaseConnection());
                $licencieJoueDansCategorieDAO = new LicencieJoueDansCategorieDAO($this->getDatabaseConnection());
            
                $licencie = $licencieDAO->getById($postId);
                if ($licencie != null) {
                    if ($licencie[LICENCIE_ACTIF] == 1) {
                        $licencie[LICENCIE_ACTIF] = 0;
                        $licencieCoacheCategorieDAO->deleteByLicencieId($postId);
                        $licencieJoueDansCategorieDAO->deleteByLicencieId($postId);
                    } else {
                        $licencie[LICENCIE_ACTIF] = 1;
                    }
                    $licencieDAO->update($licencie);

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            LICENCIE_TABLE_NAME, 
                            ACTION_TYPE_MODIFICATION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => LICENCIE_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $licencie[LICENCIE_ID],
                                    OBJET_ASSOCIE_LABEL => LICENCIE_ACTIF." ".$licencie[LICENCIE_ACTIF]
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le licenci&eacute; (idLicencie = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function delete() {
        $this->logNavigation("delete", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_coach()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                require_once("common/php/dao/action-club-dao.php");
                require_once("common/php/dao/arbitrage-dao.php");
                require_once("common/php/dao/dispo-dao.php");
                require_once("common/php/dao/histo-dispo-dao.php");
                require_once("common/php/dao/licencie-coache-categorie-dao.php");
                require_once("common/php/dao/licencie-joue-dans-categorie-dao.php");

                $postId = trim($_POST["id"]);

                $licencieDAO = new LicencieDAO($this->getDatabaseConnection());
                $actionClubDAO = new ActionClubDAO($this->getDatabaseConnection());
                $arbitrageDAO = new ArbitrageDAO($this->getDatabaseConnection());
                $dispoDAO = new DispoDAO($this->getDatabaseConnection());
                $histoDispoDAO = new HistoDispoDAO($this->getDatabaseConnection());
                $licencieCoacheCategorieDAO = new LicencieCoacheCategorieDAO($this->getDatabaseConnection());
                $licencieJoueDansCategorieDAO = new LicencieJoueDansCategorieDAO($this->getDatabaseConnection());
            
                $licencie = $licencieDAO->getById($postId);
                if ($licencie != null) {
                    $actionClubDAO->deleteByLicencieId($postId);
                    $arbitrageDAO->deleteByLicencieId($postId);
                    $dispoDAO->deleteByLicencieId($postId);
                    $histoDispoDAO->deleteByLicencieId($postId);
                    $licencieCoacheCategorieDAO->deleteByLicencieId($postId);
                    $licencieJoueDansCategorieDAO->deleteByLicencieId($postId);
                    $licencieDAO->delete($postId);

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            LICENCIE_TABLE_NAME, 
                            ACTION_TYPE_SUPPRESSION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => LICENCIE_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $licencie[LICENCIE_ID],
                                    OBJET_ASSOCIE_LABEL => $licencie[LICENCIE_NOM]." ".$licencie[LICENCIE_PRENOM]
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le licencie (idLicencie = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}