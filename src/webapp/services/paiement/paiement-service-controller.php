<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/dao/paiement-dao.php");

class PaiementServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("paiement", true);
    }

    public function save() {
        $this->logNavigation("save", array(
            "id" => $_GET["id"],
            "montant" => $_POST["montant"],
            "date" => $_POST["date"],
            "reference" => $_POST["reference"],
            "titulaireCompte" => $_POST["titulaireCompte"],
            "banque" => $_POST["banque"],
            "commentaire" => $_POST["commentaire"],
            "typePaiementId" => $_POST["typePaiementId"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(POST, "montant"),
                array(POST, "date"),
                array(POST, "typePaiementId")
            ));
            if ($checkParams) {
                require_once("common/php/dao/type-paiement-dao.php");

                $getId = $_GET["id"];
                $postMontant = trim($_POST["montant"]);
                $postDate = DateUtils::convert_slashDate_to_sqlDate(trim($_POST["date"]));
                $postReference = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["reference"]));
                $postTitulaireCompte = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["titulaireCompte"]));
                $postBanque = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["banque"]));
                $postCommentaire = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["commentaire"]));
                $postTypePaiementId = trim($_POST["typePaiementId"]);

                $typePaiementDAO = new TypePaiementDAO($this->getDatabaseConnection());
                if ($typePaiementDAO->getById($postTypePaiementId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le type paiement (idTypePaiement = '".$postTypePaiementId."') n'existe pas");
                }

                $paiementDAO = new PaiementDAO($this->getDatabaseConnection());

                $paiement = array();
                if (HTTPUtils::paramExists(GET, "id")) {
                    $paiementById = $paiementDAO->getById($getId);
                    if ($paiementById != null) {
                        $paiement = $paiementById;
                    } else {
                        $response->success = false;
                        $this->sendCheckError(HTTP_404, "Le paiement (idPaiement = '".$getId."') n'existe pas");
                    }
                }

                $paiement[PAIEMENT_MONTANT] = $postMontant;
                $paiement[PAIEMENT_DATE] = $postDate;
                $paiement[PAIEMENT_REFERENCE] = $postReference;
                $paiement[PAIEMENT_TITULAIRE_COMPTE] = $postTitulaireCompte;
                $paiement[PAIEMENT_BANQUE] = $postBanque;
                $paiement[PAIEMENT_COMMENTAIRE] = $postCommentaire;
                $paiement[PAIEMENT_TYPE_PAIEMENT_ID] = $postTypePaiementId;

                $actionType = ACTION_TYPE_CREATION;
                if (array_key_exists(PAIEMENT_ID, $paiement)) {
                    $actionType = ACTION_TYPE_MODIFICATION;
                    $paiementDAO->update($paiement);
                } else {
                    $paiement[PAIEMENT_ID] = $paiementDAO->create($paiement);
                }

                if (!isAdminConnected_superadmin()) {
                    MailUtils::notifySuperadmin(
                        PAIEMENT_TABLE_NAME, 
                        $actionType, 
                        array(
                            array(
                                OBJET_ASSOCIE_TYPE => PAIEMENT_TABLE_NAME,
                                OBJET_ASSOCIE_ID => $paiement[PAIEMENT_ID],
                                OBJET_ASSOCIE_LABEL => $paiement[PAIEMENT_TITULAIRE_COMPTE]." - ".$paiement[PAIEMENT_MONTANT]." euros"
                            )
                        ), 
                        $this->getDatabaseConnection()
                    );
                }

                $response->success = true;
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function saveLinkCommande() {
        $this->logNavigation("save", array(
            "paiementId" => $_POST["paiementId"],
            "commandeId" => $_POST["commandeId"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(POST, "paiementId"),
                array(POST, "commandeId")
            ));
            if ($checkParams) {
                require_once("common/php/dao/paiement-commande-dao.php");
                require_once("common/php/dao/commande-dao.php");

                $postIdPaiement = trim($_POST["paiementId"]);
                $postIdCommande = trim($_POST["commandeId"]);

                $paiementDAO = new PaiementDAO($this->getDatabaseConnection());
                $paiement = $paiementDAO->getById($postIdPaiement);
                if ($paiement == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le paiement (idPaiement = '".$postIdPaiement."') n'existe pas");
                }

                $commandeDAO = new CommandeDAO($this->getDatabaseConnection());
                $commande = $commandeDAO->getById($postIdCommande);
                if ($commande == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La commande (idCommande = '".$postIdCommande."') n'existe pas");
                }

                $paiementCommandeDAO = new PaiementCommandeDAO($this->getDatabaseConnection());
            
                $paiementCommande = array();
                $paiementCommande[PAIEMENT_COMMANDE_PAIEMENT_ID] = $postIdPaiement;
                $paiementCommande[PAIEMENT_COMMANDE_COMMANDE_ID] = $postIdCommande;

                $paiementCommandeDAO->create($paiementCommande);

                if (!isAdminConnected_superadmin()) {
                    MailUtils::notifySuperadmin(
                        PAIEMENT_COMMANDE_TABLE_NAME, 
                        ACTION_TYPE_CREATION, 
                        array(
                            array(
                                OBJET_ASSOCIE_TYPE => PAIEMENT_TABLE_NAME,
                                OBJET_ASSOCIE_ID => $paiement[PAIEMENT_ID],
                                OBJET_ASSOCIE_LABEL => $paiement[PAIEMENT_TITULAIRE_COMPTE]." - ".$paiement[PAIEMENT_MONTANT]." euros"
                            ),
                            array(
                                OBJET_ASSOCIE_TYPE => COMMANDE_TABLE_NAME,
                                OBJET_ASSOCIE_ID => $commande[COMMANDE_ID],
                                OBJET_ASSOCIE_LABEL => $commande[COMMANDE_REFERENCE]." - ".$commande[COMMANDE_NOM]." ".$commande[COMMANDE_PRENOM]
                            )
                        ), 
                        $this->getDatabaseConnection()
                    );
                }

                $response->success = true;
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function delete() {
        $this->logNavigation("delete", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                require_once("common/php/dao/paiement-commande-dao.php");

                $postId = trim($_POST["id"]);

                $paiementDAO = new PaiementDAO($this->getDatabaseConnection());
                $paiementCommandeDAO = new PaiementCommandeDAO($this->getDatabaseConnection());
            
                if ($paiementDAO->getById($postId) != null) {
                    $paiementCommandeDAO->deleteByPaiementId($postId);
                    $paiementDAO->delete($postId);

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            PAIEMENT_TABLE_NAME, 
                            ACTION_TYPE_SUPPRESSION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => PAIEMENT_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $paiement[PAIEMENT_ID],
                                    OBJET_ASSOCIE_LABEL => $paiement[PAIEMENT_TITULAIRE_COMPTE]." - ".$paiement[PAIEMENT_MONTANT]." euros"
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le paiement (idPaiement = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function deletePaiementCommande() {
        $this->logNavigation("deletePaiementCommande", array(
            "paiementId" => $_POST["paiementId"],
            "commandeId" => $_POST["commandeId"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(POST, "paiementId"),
                array(POST, "commandeId")
            ));
            if ($checkParams) {
                require_once("common/php/dao/paiement-commande-dao.php");
                require_once("common/php/dao/commande-dao.php");

                $postIdPaiement = trim($_POST["paiementId"]);
                $postIdCommande = trim($_POST["commandeId"]);

                $paiementDAO = new PaiementDAO($this->getDatabaseConnection());
                $paiement = $paiementDAO->getById($postIdPaiement);
                if ($paiement == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le paiement (idPaiement = '".$postIdPaiement."') n'existe pas");
                }

                $commandeDAO = new CommandeDAO($this->getDatabaseConnection());
                $commande = $commandeDAO->getById($postIdCommande);
                if ($commande == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La commande (idCommande = '".$postIdCommande."') n'existe pas");
                }

                $paiementCommandeDAO = new PaiementCommandeDAO($this->getDatabaseConnection());
            
                $idPaiementCommande = array();
                $idPaiementCommande[PAIEMENT_COMMANDE_PAIEMENT_ID] = $postIdPaiement;
                $idPaiementCommande[PAIEMENT_COMMANDE_COMMANDE_ID] = $postIdCommande;
                
                if ($paiementCommandeDAO->getById($idPaiementCommande) != null) {
                    $paiementCommandeDAO->delete($idPaiementCommande);

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            PAIEMENT_COMMANDE_TABLE_NAME, 
                            ACTION_TYPE_SUPPRESSION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => PAIEMENT_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $paiement[PAIEMENT_ID],
                                    OBJET_ASSOCIE_LABEL => $paiement[PAIEMENT_TITULAIRE_COMPTE]." - ".$paiement[PAIEMENT_MONTANT]." euros"
                                ),
                                array(
                                    OBJET_ASSOCIE_TYPE => COMMANDE_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $commande[COMMANDE_ID],
                                    OBJET_ASSOCIE_LABEL => $commande[COMMANDE_REFERENCE]." - ".$commande[COMMANDE_NOM]." ".$commande[COMMANDE_PRENOM]
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le paiement commande (idPaiement = '".$postIdPaiement."' / idCommande = '".$postIdCommande."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}