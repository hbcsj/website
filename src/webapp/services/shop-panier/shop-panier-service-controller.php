<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("core/php/lib/mailer.php");
require_once("common/php/constants/config-constants.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/lib/shop-utils.php");

class ShopPanierServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("shop-panier", true);
    }

    public function getMontantTotal() {
        $this->logNavigation("getMontantTotal", null);

        $montantTotal = 0;

        if (shopPanierExists()) {
            if (sizeof($_SESSION[SESSION_HBCSJ][SHOP_PANIER_ARTICLES]) > 0) {
                foreach ($_SESSION[SESSION_HBCSJ][SHOP_PANIER_ARTICLES] as $articlePanier) {
                    $montantTotal += $articlePanier[SHOP_PANIER_PRIX];
                }
            }
        }

        $response = new stdClass;
        $response->montant = $montantTotal;

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function getNbArticles() {
        $this->logNavigation("getNbArticles", null);

        $nbArticles = 0;

        if (shopPanierExists()) {
            if (sizeof($_SESSION[SESSION_HBCSJ][SHOP_PANIER_ARTICLES]) > 0) {
                $nbArticles = sizeof($_SESSION[SESSION_HBCSJ][SHOP_PANIER_ARTICLES]);
            }
        }

        $response = new stdClass;
        $response->nbArticles = $nbArticles;

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
    
    public function saveCommandeBilletterie() {
        $this->logNavigation("saveCommandeBilletterie", array(
            "matchBilletterieId" => $_POST["matchBilletterieId"],
            "nbPlaces" => $_POST["nbPlaces"],
            "prixTotal" => $_POST["prixTotal"]
        ));

        $response = new stdClass;

        $checkParams = $this->checkParams(array(
            array(POST, "matchBilletterieId"),
            array(POST, "nbPlaces"),
            array(POST, "prixTotal")
        ));

        if ($checkParams) {
            require_once("common/php/dao/match-billetterie-dao.php");
            
            $postMatchId = trim($_POST["matchBilletterieId"]);
            $postNbPlaces = trim($_POST["nbPlaces"]);
            $postPrixTotal = trim($_POST["prixTotal"]);

            $matchBilletterieDAO = new MatchBilletterieDAO($this->getDatabaseConnection());
            if ($matchBilletterieDAO->getById($postMatchId) == null) {
                $response->success = false;
                $this->sendCheckError(HTTP_404, "Le match billetterie (idMatchBilletterie = '".$postMatchId."') n'existe pas");
            }

            checkShopPanierInitialized();

            $articlePanier = array();
            $articlePanier[SHOP_PANIER_PRIX] = $postPrixTotal;
            $articlePanier[SHOP_PANIER_TYPE] = SHOP_PANIER_TYPE_BILLETTERIE;
            $articlePanier[SHOP_PANIER_PRODUIT_ID] = $postMatchId;
            $articlePanier[SHOP_PANIER_QUANTITE] = $postNbPlaces;

            $_SESSION[SESSION_HBCSJ][SHOP_PANIER_ARTICLES][time()] = $articlePanier;

            $response->success = true;
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
    
    public function saveCommandeBoutique() {
        $this->logNavigation("saveCommandeBoutique", array(
            "produitBoutiqueId" => $_POST["produitBoutiqueId"],
            "nbArticles" => $_POST["nbArticles"],
            "taille" => $_POST["taille"],
            "couleur" => $_POST["couleur"],
            "flocageNom" => $_POST["flocageNom"],
            "flocageNumeroAvant" => $_POST["flocageNumeroAvant"],
            "flocageNumeroArriere" => $_POST["flocageNumeroArriere"],
            "flocageInitiales" => $_POST["flocageInitiales"],
            "prixFournisseurTotal" => $_POST["prixFournisseurTotal"],
            "prixTotal" => $_POST["prixTotal"]
        ));

        $response = new stdClass;

        $checkParams = $this->checkParams(array(
            array(POST, "produitBoutiqueId"),
            array(POST, "nbArticles"),
            array(POST, "prixFournisseurTotal"),
            array(POST, "prixTotal")
        ));

        if ($checkParams) {
            require_once("common/php/dao/produit-boutique-dao.php");
            
            $postProduitId = trim($_POST["produitBoutiqueId"]);
            $postNbArticles = trim($_POST["nbArticles"]);
            $postTaille = trim($_POST["taille"]);
            $postCouleur = trim($_POST["couleur"]);
            $postFlocageNom = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["flocageNom"]));
            $postFlocageNumeroAvant = trim($_POST["flocageNumeroAvant"]);
            $postFlocageNumeroArriere = trim($_POST["flocageNumeroArriere"]);
            $postFlocageInitiales = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["flocageInitiales"]));
            $postPrixFournisseurTotal = trim($_POST["prixFournisseurTotal"]);
            $postPrixTotal = trim($_POST["prixTotal"]);

            $produitBoutiqueDAO = new ProduitBoutiqueDAO($this->getDatabaseConnection());
            if ($produitBoutiqueDAO->getById($postProduitId) == null) {
                $response->success = false;
                $this->sendCheckError(HTTP_404, "Le produit boutique (idProduitBoutique = '".$postProduitId."') n'existe pas");
            }

            checkShopPanierInitialized();

            $articlePanier = array();
            $articlePanier[SHOP_PANIER_PRIX] = $postPrixTotal;
            $articlePanier[SHOP_PANIER_PRIX_FOURNISSEUR] = $postPrixFournisseurTotal;
            $articlePanier[SHOP_PANIER_TYPE] = SHOP_PANIER_TYPE_BOUTIQUE;
            $articlePanier[SHOP_PANIER_PRODUIT_ID] = $postProduitId;
            $articlePanier[SHOP_PANIER_QUANTITE] = $postNbArticles;
            $articlePanier[SHOP_PANIER_TAILLE] = $postTaille;
            $articlePanier[SHOP_PANIER_COULEUR] = $postCouleur;
            $articlePanier[SHOP_PANIER_FLOCAGE_NOM] = $postFlocageNom;
            $articlePanier[SHOP_PANIER_FLOCAGE_NUMERO_AVANT] = $postFlocageNumeroAvant;
            $articlePanier[SHOP_PANIER_FLOCAGE_NUMERO_ARRIERE] = $postFlocageNumeroArriere;
            $articlePanier[SHOP_PANIER_FLOCAGE_INITIALES] = $postFlocageInitiales;

            $_SESSION[SESSION_HBCSJ][SHOP_PANIER_ARTICLES][time()] = $articlePanier;

            $response->success = true;
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function sendCommande() {
        $this->logNavigation("sendCommande", array(
            "nom" => $_POST["nom"],
            "prenom" => $_POST["prenom"],
            "email" => $_POST["email"],
            "telephone" => $_POST["telephone"],
            "message" => $_POST["message"]
        ));

        $response = new stdClass;

        $checkParams = $this->checkParams(array(
            array(POST, "nom"),
            array(POST, "prenom"),
            array(POST, "email"),
            array(POST, "telephone")
        ));

        if ($checkParams) {
            $postNom = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["nom"]));
            $postPrenom = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["prenom"]));
            $postEmail = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["email"]));
            $postTelephone = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["telephone"]));
            $postMessage = StringUtils::convertAccentsToHtmlSpecialChars(trim(nl2br($_POST["message"])));
            
            require_once("common/php/dao/match-billetterie-dao.php");
            require_once("common/php/dao/adversaire-billetterie-dao.php");
            require_once("common/php/dao/competition-billetterie-dao.php");
            require_once("common/php/dao/commande-item-billetterie-dao.php");
            require_once("common/php/dao/produit-boutique-dao.php");
            require_once("common/php/dao/categorie-produit-boutique-dao.php");
            require_once("common/php/dao/marque-produit-boutique-dao.php");
            require_once("common/php/dao/commande-item-boutique-dao.php");
            require_once("common/php/dao/commande-dao.php");
            require_once("common/php/dao/infos-club-dao.php");
            require_once("common/php/dao/emails-dao.php");
            require_once("common/php/dao/saison-dao.php");

            $matchBilletterieDAO = new MatchBilletterieDAO($this->getDatabaseConnection());
            $adversaireBilletterieDAO = new AdversaireBilletterieDAO($this->getDatabaseConnection());
            $competitionBilletterieDAO = new CompetitionBilletterieDAO($this->getDatabaseConnection());
            $commandeItemBilletterieDAO = new CommandeItemBilletterieDAO($this->getDatabaseConnection());
            $produitBoutiqueDAO = new ProduitBoutiqueDAO($this->getDatabaseConnection());
            $categorieProduitBoutiqueDAO = new CategorieProduitBoutiqueDAO($this->getDatabaseConnection());
            $marqueProduitBoutiqueDAO = new MarqueProduitBoutiqueDAO($this->getDatabaseConnection());
            $commandeItemBoutiqueDAO = new CommandeItemBoutiqueDAO($this->getDatabaseConnection());
            $commandeDAO = new CommandeDAO($this->getDatabaseConnection());
            $infosClubDAO = new InfosClubDAO($this->getDatabaseConnection());
            $emailsDAO = new EmailsDAO($this->getDatabaseConnection());
            $saisonDAO = new SaisonDAO($this->getDatabaseConnection());

            $saisonCourante = $saisonDAO->getByAnneeDebutSaison(DateUtils::get_annee_debutSaison());
            $commande = array();
            $commande[COMMANDE_REFERENCE] = time()."-".strtoupper(substr($postNom, 0, 1).substr($postPrenom, 0, 1));
            $commande[COMMANDE_DATE_HEURE] = date(SQL_DATE_TIME_FORMAT);
            $commande[COMMANDE_NOM] = $postNom;
            $commande[COMMANDE_PRENOM] = $postPrenom;
            $commande[COMMANDE_EMAIL] = $postEmail;
            $commande[COMMANDE_TELEPHONE] = $postTelephone;
            $commande[COMMANDE_PAYE] = 0;
            $commande[COMMANDE_SAISON_ID] = ($saisonCourante != null) ? $saisonCourante[SAISON_ID] : null;

            $commande[COMMANDE_ID] = $commandeDAO->create($commande);

            $montantTotal = 0;
            $lignesCommandes = "";
            foreach ($_SESSION[SESSION_HBCSJ][SHOP_PANIER_ARTICLES] as $key => $articlePanier) {
                $montantTotal += $articlePanier[SHOP_PANIER_PRIX];
                
                if ($articlePanier[SHOP_PANIER_TYPE] == SHOP_PANIER_TYPE_BOUTIQUE) {
                    $commandeItemBoutique = array();
                    $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_QUANTITE] = $articlePanier[SHOP_PANIER_QUANTITE];
                    $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_TAILLE] = $articlePanier[SHOP_PANIER_TAILLE];
                    $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_COULEUR] = $articlePanier[SHOP_PANIER_COULEUR];
                    $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_FLOCAGE_NOM] = $articlePanier[SHOP_PANIER_FLOCAGE_NOM];
                    $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_FLOCAGE_NUMERO_ARRIERE] = $articlePanier[SHOP_PANIER_FLOCAGE_NUMERO_ARRIERE];
                    $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_FLOCAGE_NUMERO_AVANT] = $articlePanier[SHOP_PANIER_FLOCAGE_NUMERO_AVANT];
                    $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_FLOCAGE_INITIALES] = $articlePanier[SHOP_PANIER_FLOCAGE_INITIALES];
                    $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_PRIX] = $articlePanier[SHOP_PANIER_PRIX];
                    $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_PRIX_FOURNISSEUR] = $articlePanier[SHOP_PANIER_PRIX_FOURNISSEUR];
                    $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_DONNE] = 0;
                    $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_PRODUIT_BOUTIQUE_ID] = $articlePanier[SHOP_PANIER_PRODUIT_ID];
                    $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_TYPE_SOURCE_PRODUIT_BOUTIQUE_ID] = COMMANDE_ITEM_BOUTIQUE_NON_COMMANDE_TYPE_SOURCE_ID;
                    $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_COMMANDE_ID] = $commande[COMMANDE_ID];

                    $commandeItemBoutiqueDAO->create($commandeItemBoutique);

                    $produit = $produitBoutiqueDAO->getById($articlePanier[SHOP_PANIER_PRODUIT_ID]);
                    $categorie = $categorieProduitBoutiqueDAO->getById($produit[PRODUIT_BOUTIQUE_CATEGORIE_PRODUIT_BOUTIQUE_ID]);
                    $marque = $marqueProduitBoutiqueDAO->getById($produit[PRODUIT_BOUTIQUE_MARQUE_PRODUIT_BOUTIQUE_ID]);

                    $lignesCommandes .= "<tr>";
                    $lignesCommandes .= "<td style=\"width: 150px; padding: 3px; border-bottom: 1px solid #dddddd;\">".SHOP_PANIER_TYPE_BOUTIQUE."</td>";
                    $lignesCommandes .= "<td style=\"width: 300px; padding: 3px; border-bottom: 1px solid #dddddd;\">";
                    $lignesCommandes .= "<b>".$produit[PRODUIT_BOUTIQUE_NOM]."</b>";
                    $lignesCommandes .= " - ".$produit[PRODUIT_BOUTIQUE_INDICATION];
                    
                    if ($categorie != null) {
                        $lignesCommandes .= "<br>".$categorie[CATEGORIE_PRODUIT_BOUTIQUE_NOM];
                    }
                    if ($marque != null) {
                        $lignesCommandes .= "<br>".$marque[MARQUE_PRODUIT_BOUTIQUE_NOM];
                    }
                    if ($articlePanier[SHOP_PANIER_TAILLE]) {
                        $lignesCommandes .= "<br>Taille : ".$articlePanier[SHOP_PANIER_TAILLE];
                    }
                    if ($articlePanier[SHOP_PANIER_COULEUR]) {
                        $lignesCommandes .= "<br>Couleur : ".$articlePanier[SHOP_PANIER_COULEUR];
                    }
                    if ($articlePanier[SHOP_PANIER_FLOCAGE_NOM]) {
                        $lignesCommandes .= "<br>Flocage nom : ".$articlePanier[SHOP_PANIER_FLOCAGE_NOM];
                    }
                    if ($articlePanier[SHOP_PANIER_FLOCAGE_NUMERO_ARRIERE]) {
                        $lignesCommandes .= "<br>Flocage n&deg; arri&egrave;re : ".$articlePanier[SHOP_PANIER_FLOCAGE_NUMERO_ARRIERE];
                    }
                    if ($articlePanier[SHOP_PANIER_FLOCAGE_NUMERO_AVANT]) {
                        $lignesCommandes .= "<br>Flocage n&deg; avant : ".$articlePanier[SHOP_PANIER_FLOCAGE_NUMERO_AVANT];
                    }
                    if ($articlePanier[SHOP_PANIER_FLOCAGE_INITIALES]) {
                        $lignesCommandes .= "<br>Flocage initiales : ".$articlePanier[SHOP_PANIER_FLOCAGE_INITIALES];
                    }
                    $lignesCommandes .= "</td>";
                    $lignesCommandes .= "<td style=\"text-align: center; width: 100px; padding: 3px; border-bottom: 1px solid #dddddd;\">".$articlePanier[SHOP_PANIER_QUANTITE]."</td>";
                    $lignesCommandes .= "<td style=\"text-align: center; width: 100px; padding: 3px; border-bottom: 1px solid #dddddd;\">".$articlePanier[SHOP_PANIER_PRIX]." &euro;</td>";
                    $lignesCommandes .= "</tr>";
                } else if ($articlePanier[SHOP_PANIER_TYPE] == SHOP_PANIER_TYPE_BILLETTERIE) {
                    $commandeItemBilletterie = array();
                    $commandeItemBilletterie[COMMANDE_ITEM_BILLETTERIE_QUANTITE] = $articlePanier[SHOP_PANIER_QUANTITE];
                    $commandeItemBilletterie[COMMANDE_ITEM_BILLETTERIE_PRIX] = $articlePanier[SHOP_PANIER_PRIX];
                    $commandeItemBilletterie[COMMANDE_ITEM_BILLETTERIE_DONNE] = 0;
                    $commandeItemBilletterie[COMMANDE_ITEM_BILLETTERIE_MATCH_BILLETTERIE_ID] = $articlePanier[SHOP_PANIER_PRODUIT_ID];
                    $commandeItemBilletterie[COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID] = $commande[COMMANDE_ID];

                    $commandeItemBilletterieDAO->create($commandeItemBilletterie);

                    $match = $matchBilletterieDAO->getById($articlePanier[SHOP_PANIER_PRODUIT_ID]);
                    $adversaire = $adversaireBilletterieDAO->getById($match[MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID]);
                    $competition = $competitionBilletterieDAO->getById($match[MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID]);

                    $lignesCommandes .= "<tr>";
                    $lignesCommandes .= "<td style=\"width: 150px; padding: 3px; border-bottom: 1px solid #dddddd;\">".SHOP_PANIER_TYPE_BILLETTERIE."</td>";
                    $lignesCommandes .= "<td style=\"width: 300px; padding: 3px; border-bottom: 1px solid #dddddd;\">";
                    $lignesCommandes .= "<b>Fenix VS ".$adversaire[ADVERSAIRE_BILLETTERIE_NOM]."</b>";
                    $lignesCommandes .= "<br>".$competition[COMPETITION_BILLETTERIE_NOM];
                    $lignesCommandes .= "<br>".DateUtils::convert_sqlDateTime_to_slashDateTime($match[MATCH_BILLETTERIE_DATE_HEURE]);
                    $lignesCommandes .= "</td>";
                    $lignesCommandes .= "<td style=\"text-align: center; width: 100px; padding: 3px; border-bottom: 1px solid #dddddd;\">".$articlePanier[SHOP_PANIER_QUANTITE]."</td>";
                    $lignesCommandes .= "<td style=\"text-align: center; width: 100px; padding: 3px; border-bottom: 1px solid #dddddd;\">".$articlePanier[SHOP_PANIER_PRIX]." &euro;</td>";
                    $lignesCommandes .= "</tr>";
                }
            }

            $mailClientTemplate = FileUtils::getFileContent(MAIL_CLIENT_SHOP_TEMPLATE_FILE); 
            $mailHBCSJTemplate = FileUtils::getFileContent(MAIL_HBCSJ_SHOP_TEMPLATE_FILE);
            $infosClub = $infosClubDAO->getSingleton();
            $emails = $emailsDAO->getSingleton();

            $mailClient = str_replace("%PRENOM%", $postPrenom, $mailClientTemplate);
            $mailHBCSJ = str_replace("%PRENOM%", $postPrenom, $mailHBCSJTemplate);
            $mailClient = str_replace("%NOM%", $postNom, $mailClient);
            $mailHBCSJ = str_replace("%NOM%", $postNom, $mailHBCSJ);
            $mailClient = str_replace("%EMAIL%", $postEmail, $mailClient);
            $mailHBCSJ = str_replace("%EMAIL%", $postEmail, $mailHBCSJ);
            $mailClient = str_replace("%TELEPHONE%", $postTelephone, $mailClient);
            $mailHBCSJ = str_replace("%TELEPHONE%", $postTelephone, $mailHBCSJ);
            $mailClient = str_replace("%MESSAGE%", $postMessage, $mailClient);
            $mailHBCSJ = str_replace("%MESSAGE%", $postMessage, $mailHBCSJ);
            $mailClient = str_replace("%LIGNES_COMMANDE%", $lignesCommandes, $mailClient);
            $mailHBCSJ = str_replace("%LIGNES_COMMANDE%", $lignesCommandes, $mailHBCSJ);
            $mailClient = str_replace("%MONTANT_TOTAL%", $montantTotal, $mailClient);
            $mailHBCSJ = str_replace("%MONTANT_TOTAL%", $montantTotal, $mailHBCSJ);
            $mailClient = str_replace("%REF_COMMANDE%", $commande[COMMANDE_REFERENCE], $mailClient);
            $mailHBCSJ = str_replace("%REF_COMMANDE%", $commande[COMMANDE_REFERENCE], $mailHBCSJ);
            $mailClient = str_replace("%NOM_RIB_HBCSJ%", $infosClub[INFOS_CLUB_NOM_RIB], $mailClient);
            $mailClient = str_replace("%IBAN_RIB_HBCSJ%", $infosClub[INFOS_CLUB_IBAN_RIB], $mailClient);
            $mailClient = str_replace("%BIC_SWIFT_RIB_HBCSJ%", $infosClub[INFOS_CLUB_BIC_SWIFT_RIB], $mailClient);
            $mailClient = str_replace("%BOUTIQUE_EMAIL%", $emails[EMAILS_BOUTIQUE], $mailClient);

            $sujetMailClient = "[Shop ".$infosClub[INFOS_CLUB_NOM_XS]."] - Votre commande";
            $sujetMailHBCSJ = "[Shop ".$infosClub[INFOS_CLUB_NOM_XS]."] - Commande de ".$postPrenom." ".$postNom;
            $mailerClient = new Mailer($GLOBALS[CONF][NOREPLY_EMAIL], $postEmail, $sujetMailClient, $mailClient);
            $mailerHBCSJ = new Mailer($GLOBALS[CONF][NOREPLY_EMAIL], $emails[EMAILS_BOUTIQUE], $sujetMailHBCSJ, $mailHBCSJ);
            $mailerHBCSJ->setMailsCci($GLOBALS[CONF][SUPERADMIN_EMAIL]);
            $mailerClient->send();
            $mailerHBCSJ->send();

            MailUtils::test("mail-shop-client-test.html", $mailClient);
            MailUtils::test("mail-shop-hbcsj-test.html", $mailHBCSJ);

            $_SESSION[SESSION_HBCSJ][SHOP_PANIER_ARTICLES] = array();

            $response->success = true;
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function deleteArticle() {
        $this->logNavigation("deleteArticle", array(
            "idArticlePanier" => $_POST["idArticlePanier"]
        ));

        $response = new stdClass;

        $checkParams = $this->checkParams(array(
            array(POST, "idArticlePanier")
        ));
        if ($checkParams) {
            $postId = trim($_POST["idArticlePanier"]);

            if (array_key_exists($postId, $_SESSION[SESSION_HBCSJ][SHOP_PANIER_ARTICLES])) {
                unset($_SESSION[SESSION_HBCSJ][SHOP_PANIER_ARTICLES][$postId]);

                $response->success = true;
            } else {
                $response->success = false;
                $this->sendCheckError(HTTP_404, "L'article (idArticlePanier = '".$postId."') n'existe pas");
            }
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}