<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("core/php/lib/mailer.php");
require_once("common/php/constants/config-constants.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/mail-utils.php");

class NewsletterServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("newsletter", true);
    }

    public function getNewsFromDate() {
        $this->logNavigation("sendNewsletter", array(
            "date" => $_GET["date"]
        ));

        $response = new stdClass;

        $checkParams = $this->checkParams(array(
            array(GET, "date")
        ));

        if ($checkParams) {
            $getDate = DateUtils::convert_slashDate_to_sqlDate(trim($_GET["date"]));

            require_once("common/php/dao/categorie-dao.php");
            $categorieDAO = new CategorieDAO($this->getDatabaseConnection());
            $nbEquipesInscrites = $categorieDAO->getNbEquipesInscrites()[CATEGORIE_NB_EQUIPES_INSCRITES];

            require_once("common/php/dao/evenement-dao.php");
            $evenementDAO = new EvenementDAO($this->getDatabaseConnection());

            $response->derniersResultats = array();
            $derniersResultats = $evenementDAO->getVisiblesEntreDatesByTypeLimited(
                $getDate, 
                date(SQL_DATE_TIME_FORMAT), 
                EVENEMENT_MATCH_TYPE_ID, 
                $nbEquipesInscrites
            );
            if (sizeof($derniersResultats) > 0) {
                foreach ($derniersResultats as $dernierResultat) {
                    $response->derniersResultats[] = $dernierResultat[EVENEMENT_ID];
                }
            }

            require_once("common/php/dao/actualite-dao.php");
            $actualiteDAO = new ActualiteDAO($this->getDatabaseConnection());

            $response->news = array();
            $dernieresNews = $actualiteDAO->getVisiblesApresDate(
                $getDate, 
                ACTUALITE_TABLE_NAME.".".ACTUALITE_DATE_HEURE." DESC"
            );
            if (sizeof($dernieresNews) > 0) {
                foreach ($dernieresNews as $derniereActu) {
                    $response->news[] = $derniereActu[ACTUALITE_ID];
                }
            }

            require_once("common/php/managers/presse-manager.php");
            $presseManager = new PresseManager($this->getDatabaseConnection());

            $response->articlesPresse = array();
            $derniersArticlesPresse = $presseManager->getArticlesPresseApresDate(
                $getDate
            );
            if (sizeof($derniersArticlesPresse) > 0) {
                foreach ($derniersArticlesPresse as $dernierArticlePresse) {
                    if (array_key_exists(HHSJ_TYPE_HHSJ_ID, $dernierArticlePresse)) {
                        $response->articlesPresse[] = HHSJ_ID_PREFIX.$dernierArticlePresse[HHSJ_ID];
                    } else {
                        $response->articlesPresse[] = ARTICLE_PRESSE_ID_PREFIX.$dernierArticlePresse[ARTICLE_PRESSE_ID];
                    }
                }
            }
        } else {
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function sendNewsletter() {
        $this->logNavigation("sendNewsletter", null);

        $response = new stdClass;

        if (isAdminConnected_superadmin()) {
            $checkParams = $this->checkParams(array(
                array(POST, "newsletter")
            ));

            if ($checkParams) {
                $postNewsletter = StringUtils::convertAccentsToHtmlSpecialChars(trim(nl2br($_POST["newsletter"])));

                require_once("common/php/dao/infos-club-dao.php");
                require_once("common/php/dao/licencie-dao.php");

                $infosClubDAO = new InfosClubDAO($this->getDatabaseConnection());
                $licencieDAO = new LicencieDAO($this->getDatabaseConnection());

                $mailNewsletterTemplate = FileUtils::getFileContent(NEWSLETTER_TEMPLATE_FILE); 
                $infosClub = $infosClubDAO->getSingleton();

                $mailNewsletter = str_replace("%MESSAGE%", $postNewsletter, $mailNewsletterTemplate);

                $mailingList = array();
                $licencies = $licencieDAO->getAll();

                if (sizeof($licencies) > 0) {
                    foreach ($licencies as $licencie) {
                        if ($licencie[LICENCIE_EMAIL_1] != "") {
                            if (!in_array($licencie[LICENCIE_EMAIL_1], $mailingList)) {
                                $mailingList[] = $licencie[LICENCIE_EMAIL_1];
                            }
                        }
                        if ($licencie[LICENCIE_EMAIL_2] != "") {
                            if (!in_array($licencie[LICENCIE_EMAIL_2], $mailingList)) {
                                $mailingList[] = $licencie[LICENCIE_EMAIL_2];
                            }
                        }
                        if ($licencie[LICENCIE_EMAIL_3] != "") {
                            if (!in_array($licencie[LICENCIE_EMAIL_3], $mailingList)) {
                                $mailingList[] = $licencie[LICENCIE_EMAIL_3];
                            }
                        }
                    }
                }

                $sujet = "[".$infosClub[INFOS_CLUB_NOM_XS]."] - Newsletter";
                $mailer = new Mailer($GLOBALS[CONF][NOREPLY_EMAIL], null, $sujet, $mailNewsletter);
                $mailer->setMailsCci(implode(",", $mailingList));
                $mailer->send();

                MailUtils::test("newsletter-test.html", $mailNewsletter);

                $response->success = true;
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}