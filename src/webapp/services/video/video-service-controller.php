<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("common/php/dao/video-dao.php");

class VideoServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("video", true);
    }

    public function toggleValidation() {
        $this->logNavigation("toggleValidation", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_superadmin()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                $postId = trim($_POST["id"]);

                $videoDAO = new VideoDAO($this->getDatabaseConnection());
            
                $video = $videoDAO->getById($postId);
                if ($video != null) {
                    if ($video[VIDEO_VISIBLE_SUR_SITE] == 1) {
                        $video[VIDEO_VISIBLE_SUR_SITE] = 0;
                    } else {
                        $video[VIDEO_VISIBLE_SUR_SITE] = 1;
                    }
                    $video[VIDEO_EMAIL_DERNIER_EDITEUR] = $GLOBALS[CONF][SUPERADMIN_EMAIL];
                    $videoDAO->update($video);

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La video (idVideo = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function delete() {
        $this->logNavigation("delete", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_bureau()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                $postId = trim($_POST["id"]);

                $videoDAO = new VideoDAO($this->getDatabaseConnection());

                $video = $videoDAO->getById($postId);
                if ($video != null) {
                    $videoDAO->delete($postId); 
                    
                    $videoFile = PathUtils::getVideoImgFile($postId);
                    if (file_exists($videoFile)) {
                        unlink($videoFile);
                    }
                    $videoSmallFile = PathUtils::getVideoImgSmallFile($postId);
                    if (file_exists($videoSmallFile)) {
                        unlink($videoSmallFile);
                    }

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            VIDEO_TABLE_NAME, 
                            ACTION_TYPE_SUPPRESSION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => VIDEO_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $video[VIDEO_ID],
                                    OBJET_ASSOCIE_LABEL => VIDEO_TITRE
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La video (idVideo = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}

?>