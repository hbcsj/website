<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/dao/livraison-fournisseur-boutique-dao.php");

class LivraisonFournisseurBoutiqueServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("livraison-fournisseur-boutique", true);
    }

    public function save() {
        $this->logNavigation("save", array(
            "id" => $_GET["id"],
            "date" => $_POST["date"],
            "fournisseurProduitBoutiqueId" => $_POST["fournisseurProduitBoutiqueId"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(POST, "date"),
                array(POST, "fournisseurProduitBoutiqueId")
            ));
            if ($checkParams) {
                require_once("common/php/dao/fournisseur-produit-boutique-dao.php");

                $getId = $_GET["id"];
                $postDate = DateUtils::convert_slashDate_to_sqlDate(trim($_POST["date"]));
                $postFournisseurProduitBoutiqueId = trim($_POST["fournisseurProduitBoutiqueId"]);
                
                $fournisseurProduitBoutiqueDAO = new FournisseurProduitBoutiqueDAO($this->getDatabaseConnection());
                if ($fournisseurProduitBoutiqueDAO->getById($postFournisseurProduitBoutiqueId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le fournisseurProduitBoutique (idFournisseurProduitBoutique = '".$postFournisseurProduitBoutiqueId."') n'existe pas");
                }

                $livraisonFournisseurBoutiqueDAO = new LivraisonFournisseurBoutiqueDAO($this->getDatabaseConnection());

                $livraisonFournisseurBoutique = array();
                if (HTTPUtils::paramExists(GET, "id")) {
                    $livraisonFournisseurBoutiqueById = $livraisonFournisseurBoutiqueDAO->getById($getId);
                    if ($livraisonFournisseurBoutiqueById != null) {
                        $livraisonFournisseurBoutique = $livraisonFournisseurBoutiqueById;
                    } else {
                        $response->success = false;
                        $this->sendCheckError(HTTP_404, "La livraisonFournisseurBoutique (idLivraisonFournisseurBoutique = '".$getId."') n'existe pas");
                    }
                }

                $livraisonFournisseurBoutique[LIVRAISON_FOURNISSEUR_BOUTIQUE_DATE] = $postDate;
                $livraisonFournisseurBoutique[LIVRAISON_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID] = $postFournisseurProduitBoutiqueId;

                $actionType = ACTION_TYPE_CREATION;
                if (array_key_exists(LIVRAISON_FOURNISSEUR_BOUTIQUE_ID, $livraisonFournisseurBoutique)) {
                    $actionType = ACTION_TYPE_MODIFICATION;
                    $livraisonFournisseurBoutiqueDAO->update($livraisonFournisseurBoutique);
                } else {
                    $livraisonFournisseurBoutique[LIVRAISON_FOURNISSEUR_BOUTIQUE_ID] = $livraisonFournisseurBoutiqueDAO->create($livraisonFournisseurBoutique);
                }

                if (!isAdminConnected_superadmin()) {
                    MailUtils::notifySuperadmin(
                        LIVRAISON_FOURNISSEUR_BOUTIQUE_TABLE_NAME, 
                        $actionType, 
                        array(
                            array(
                                OBJET_ASSOCIE_TYPE => LIVRAISON_FOURNISSEUR_BOUTIQUE_TABLE_NAME,
                                OBJET_ASSOCIE_ID => $livraisonFournisseurBoutique[LIVRAISON_FOURNISSEUR_BOUTIQUE_ID],
                                OBJET_ASSOCIE_LABEL => $livraisonFournisseurBoutique[LIVRAISON_FOURNISSEUR_BOUTIQUE_NOM]
                            )
                        ), 
                        $this->getDatabaseConnection()
                    );
                }

                $response->success = true;
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function delete() {
        $this->logNavigation("delete", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                require_once("common/php/dao/commande-item-boutique-dao.php");

                $postId = trim($_POST["id"]);

                $livraisonFournisseurBoutiqueDAO = new LivraisonFournisseurBoutiqueDAO($this->getDatabaseConnection());
                $commandeItemBoutiqueDAO = new CommandeItemBoutiqueDAO($this->getDatabaseConnection());
            
                $livraisonFournisseurBoutique = $livraisonFournisseurBoutiqueDAO->getById($postId);
                if ($livraisonFournisseurBoutique != null) {
                    if (sizeof($commandeItemBoutiqueDAO->getByLivraisonFournisseurBoutiqueId($postId)) == 0) {
                        $livraisonFournisseurBoutiqueDAO->delete($postId);

                        if (!isAdminConnected_superadmin()) {
                            MailUtils::notifySuperadmin(
                                LIVRAISON_FOURNISSEUR_BOUTIQUE_TABLE_NAME, 
                                ACTION_TYPE_SUPPRESSION, 
                                array(
                                    array(
                                        OBJET_ASSOCIE_TYPE => LIVRAISON_FOURNISSEUR_BOUTIQUE_TABLE_NAME,
                                        OBJET_ASSOCIE_ID => $livraisonFournisseurBoutique[LIVRAISON_FOURNISSEUR_BOUTIQUE_ID],
                                        OBJET_ASSOCIE_LABEL => $livraisonFournisseurBoutique[LIVRAISON_FOURNISSEUR_BOUTIQUE_DATE]
                                    )
                                ), 
                                $this->getDatabaseConnection()
                            );
                        }

                        $response->success = true;
                    } else {
                        $response->success = false;
                        $this->sendCheckError(HTTP_400, "La livraisonFournisseurBoutique (idLivraisonFournisseurBoutique = '".$postId."') a des commandes / livraisons associees");
                    }
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le livraisonFournisseurBoutique (idLivraisonFournisseurBoutique = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}