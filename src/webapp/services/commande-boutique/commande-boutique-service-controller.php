<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/dao/commande-item-boutique-dao.php");

class CommandeBoutiqueServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("commande-boutique", true);
    }

    public function saveLinkCommandeFournisseur() {
        $this->logNavigation("saveLinkCommandeFournisseur", array(
            "id" => $_GET["id"],
            "commandeFournisseurBoutiqueId" => $_POST["commandeFournisseurBoutiqueId"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(GET, "id")
            ));
            if ($checkParams) {
                require_once("common/php/dao/commande-fournisseur-boutique-dao.php");

                $getId = $_GET["id"];
                $postCommandeFournisseurBoutiqueId = StringUtils::convertEmptyStringToNull(trim($_POST["commandeFournisseurBoutiqueId"]));

                $commandeFournisseurBoutiqueDAO = new CommandeFournisseurBoutiqueDAO($this->getDatabaseConnection());
                if ($postCommandeFournisseurBoutiqueId != null && $commandeFournisseurBoutiqueDAO->getById($postCommandeFournisseurBoutiqueId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La commande fournisseur boutique (idCommandeFournisseurBoutique = '".$postCommandeFournisseurBoutiqueId."') n'existe pas");
                }

                $commandeItemBoutiqueDAO = new CommandeItemBoutiqueDAO($this->getDatabaseConnection());

                $commandeItemBoutique = array();
                $commandeItemBoutiqueById = $commandeItemBoutiqueDAO->getById($getId);
                if ($commandeItemBoutiqueById != null) {
                    $commandeItemBoutique = $commandeItemBoutiqueById;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La commande boutique (idCommandeItemBoutique = '".$getId."') n'existe pas");
                }

                $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_COMMANDE_FOURNISSEUR_BOUTIQUE_ID] = $postCommandeFournisseurBoutiqueId;
                if ($postCommandeFournisseurBoutiqueId != null) {
                    $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_TYPE_SOURCE_PRODUIT_BOUTIQUE_ID] = COMMANDE_ITEM_BOUTIQUE_COMMANDE_TYPE_SOURCE_ID;
                } else {
                    $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_TYPE_SOURCE_PRODUIT_BOUTIQUE_ID] = COMMANDE_ITEM_BOUTIQUE_NON_COMMANDE_TYPE_SOURCE_ID;
                }

                $commandeItemBoutiqueDAO->update($commandeItemBoutique);

                if (!isAdminConnected_superadmin()) {
                    MailUtils::notifySuperadmin(
                        COMMANDE_ITEM_BOUTIQUE_TABLE_NAME, 
                        ACTION_TYPE_MODIFICATION, 
                        array(
                            array(
                                OBJET_ASSOCIE_TYPE => COMMANDE_ITEM_BOUTIQUE_TABLE_NAME,
                                OBJET_ASSOCIE_ID => $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_ID],
                                OBJET_ASSOCIE_LABEL => COMMANDE_ITEM_BOUTIQUE_COMMANDE_FOURNISSEUR_BOUTIQUE_ID."=".$postCommandeFournisseurBoutiqueId
                            )
                        ), 
                        $this->getDatabaseConnection()
                    );
                }

                $response->success = true;
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function saveLinkLivraisonFournisseur() {
        $this->logNavigation("saveLinkLivraisonFournisseur", array(
            "id" => $_GET["id"],
            "livraisonFournisseurBoutiqueId" => $_POST["livraisonFournisseurBoutiqueId"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(GET, "id")
            ));
            if ($checkParams) {
                require_once("common/php/dao/livraison-fournisseur-boutique-dao.php");

                $getId = $_GET["id"];
                $postLivraisonFournisseurBoutiqueId = StringUtils::convertEmptyStringToNull(trim($_POST["livraisonFournisseurBoutiqueId"]));

                $livraisonFournisseurBoutiqueDAO = new LivraisonFournisseurBoutiqueDAO($this->getDatabaseConnection());
                if ($postLivraisonFournisseurBoutiqueId != null && $livraisonFournisseurBoutiqueDAO->getById($postLivraisonFournisseurBoutiqueId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La livraison fournisseur boutique (idCommandeFournisseurBoutique = '".$postLivraisonFournisseurBoutiqueId."') n'existe pas");
                }

                $commandeItemBoutiqueDAO = new CommandeItemBoutiqueDAO($this->getDatabaseConnection());

                $commandeItemBoutique = array();
                $commandeItemBoutiqueById = $commandeItemBoutiqueDAO->getById($getId);
                if ($commandeItemBoutiqueById != null) {
                    $commandeItemBoutique = $commandeItemBoutiqueById;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La commande boutique (idCommandeItemBoutique = '".$getId."') n'existe pas");
                }

                $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_LIVRAISON_FOURNISSEUR_BOUTIQUE_ID] = $postLivraisonFournisseurBoutiqueId;

                $commandeItemBoutiqueDAO->update($commandeItemBoutique);

                if (!isAdminConnected_superadmin()) {
                    MailUtils::notifySuperadmin(
                        COMMANDE_ITEM_BOUTIQUE_TABLE_NAME, 
                        ACTION_TYPE_MODIFICATION, 
                        array(
                            array(
                                OBJET_ASSOCIE_TYPE => COMMANDE_ITEM_BOUTIQUE_TABLE_NAME,
                                OBJET_ASSOCIE_ID => $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_ID],
                                OBJET_ASSOCIE_LABEL => COMMANDE_ITEM_BOUTIQUE_LIVRAISON_FOURNISSEUR_BOUTIQUE_ID."=".$postLivraisonFournisseurBoutiqueId
                            )
                        ), 
                        $this->getDatabaseConnection()
                    );
                }

                $response->success = true;
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function saveLinkSource() {
        $this->logNavigation("saveLinkSource", array(
            "id" => $_GET["id"],
            "typeSourceProduitBoutiqueId" => $_POST["typeSourceProduitBoutiqueId"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(GET, "id")
            ));
            if ($checkParams) {
                require_once("common/php/dao/type-source-produit-boutique-dao.php");

                $getId = $_GET["id"];
                $postTypeSourceProduitBoutiqueId = trim($_POST["typeSourceProduitBoutiqueId"]);

                $typeSourceProduitBoutiqueDAO = new TypeSourceProduitBoutiqueDAO($this->getDatabaseConnection());
                if ($postTypeSourceProduitBoutiqueId != null && $typeSourceProduitBoutiqueDAO->getById($postTypeSourceProduitBoutiqueId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le type source produit boutique (idTypeSourceProduitBoutique = '".$postTypeSourceProduitBoutiqueId."') n'existe pas");
                }

                $commandeItemBoutiqueDAO = new CommandeItemBoutiqueDAO($this->getDatabaseConnection());

                $commandeItemBoutique = array();
                $commandeItemBoutiqueById = $commandeItemBoutiqueDAO->getById($getId);
                if ($commandeItemBoutiqueById != null) {
                    $commandeItemBoutique = $commandeItemBoutiqueById;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La commande boutique (idCommandeItemBoutique = '".$getId."') n'existe pas");
                }

                $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_TYPE_SOURCE_PRODUIT_BOUTIQUE_ID] = $postTypeSourceProduitBoutiqueId;

                $commandeItemBoutiqueDAO->update($commandeItemBoutique);

                if (!isAdminConnected_superadmin()) {
                    MailUtils::notifySuperadmin(
                        COMMANDE_ITEM_BOUTIQUE_TABLE_NAME, 
                        ACTION_TYPE_MODIFICATION, 
                        array(
                            array(
                                OBJET_ASSOCIE_TYPE => COMMANDE_ITEM_BOUTIQUE_TABLE_NAME,
                                OBJET_ASSOCIE_ID => $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_ID],
                                OBJET_ASSOCIE_LABEL => COMMANDE_ITEM_BOUTIQUE_TYPE_SOURCE_PRODUIT_BOUTIQUE_ID."=".$postTypeSourceProduitBoutiqueId
                            )
                        ), 
                        $this->getDatabaseConnection()
                    );
                }

                $response->success = true;
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function toggleDonne() {
        $this->logNavigation("toggleDonne", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                $postId = trim($_POST["id"]);

                $commandeItemBoutiqueDAO = new CommandeItemBoutiqueDAO($this->getDatabaseConnection());
            
                $commandeItemBoutique = $commandeItemBoutiqueDAO->getById($postId);
                if ($commandeItemBoutique != null) {
                    if ($commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_DONNE] == 1) {
                        $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_DONNE] = 0;
                    } else {
                        $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_DONNE] = 1;
                    }
                    $commandeItemBoutiqueDAO->update($commandeItemBoutique);

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            COMMANDE_ITEM_BOUTIQUE_TABLE_NAME, 
                            ACTION_TYPE_MODIFICATION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => COMMANDE_ITEM_BOUTIQUE_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_ID],
                                    OBJET_ASSOCIE_LABEL => COMMANDE_ITEM_BOUTIQUE_DONNE."=".$commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_DONNE]
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La commande boutique (idCommandeItemBoutique = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}