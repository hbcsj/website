<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/dao/commande-dao.php");

class CommandeServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("commande", true);
    }

    public function togglePaye() {
        $this->logNavigation("togglePaye", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                $postId = trim($_POST["id"]);

                $commandeDAO = new CommandeDAO($this->getDatabaseConnection());
            
                $commande = $commandeDAO->getById($postId);
                if ($commande != null) {
                    if ($commande[COMMANDE_PAYE] == 1) {
                        $commande[COMMANDE_PAYE] = 0;
                    } else {
                        $commande[COMMANDE_PAYE] = 1;
                    }
                    $commandeDAO->update($commande);

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            COMMANDE_TABLE_NAME, 
                            ACTION_TYPE_MODIFICATION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => COMMANDE_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $postId,
                                    OBJET_ASSOCIE_LABEL => $commande[COMMANDE_REFERENCE]." - ".$commande[COMMANDE_NOM]." ".$commande[COMMANDE_PRENOM]
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La commande (idCommande = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function delete() {
        $this->logNavigation("delete", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                require_once("common/php/dao/commande-item-billetterie-dao.php");
                require_once("common/php/dao/commande-item-boutique-dao.php");
                require_once("common/php/dao/paiement-commande-dao.php");

                $postId = trim($_POST["id"]);

                $commandeDAO = new CommandeDAO($this->getDatabaseConnection());
                $commandeItemBilletterieDAO = new CommandeItemBilletterieDAO($this->getDatabaseConnection());
                $commandeItemBoutiqueDAO = new CommandeItemBoutiqueDAO($this->getDatabaseConnection());
                $paiementCommandeDAO = new PaiementCommandeDAO($this->getDatabaseConnection());
            
                $commande = $commandeDAO->getById($postId);
                if ($commande != null) {
                    $commandeItemBilletterieDAO->deleteByCommandeId($postId);
                    $commandeItemBoutiqueDAO->deleteByCommandeId($postId);
                    $paiementCommandeDAO->deleteByCommandeId($postId);
                    $commandeDAO->delete($postId); 

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            COMMANDE_TABLE_NAME, 
                            ACTION_TYPE_SUPPRESSION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => COMMANDE_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $commande[COMMANDE_ID],
                                    OBJET_ASSOCIE_LABEL => COMMANDE_REFERENCE."=".$commande[COMMANDE_REFERENCE]
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La commande (idCommande = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}