<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/stock-produit-boutique-dao.php");

class StockProduitBoutiqueServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("stock-produit-boutique", true);
    }

    public function save() {
        $this->logNavigation("save", array(
            "produitBoutiqueId" => $_GET["produitBoutiqueId"],
            "taille" => $_GET["taille"],
            "stock" => $_POST["quantiteStock"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(GET, "produitBoutiqueId"),
                array(GET, "taille"),
                array(POST, "quantiteStock")
            ));
            if ($checkParams) {
                require_once("common/php/dao/produit-boutique-dao.php");

                $getProduitBoutiqueId = $_GET["produitBoutiqueId"];
                $getTaille = trim($_GET["taille"]);
                $postQuantiteStock = trim($_POST["quantiteStock"]);

                $produitBoutiqueDAO = new ProduitBoutiqueDAO($this->getDatabaseConnection());
                $produitBoutique = $produitBoutiqueDAO->getById($getProduitBoutiqueId);
                if ($produitBoutique == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le produit boutique (idProduitBoutique = '".$getProduitBoutiqueId."') n'existe pas");
                }

                $stockProduitBoutiqueDAO = new StockProduitBoutiqueDAO($this->getDatabaseConnection());

                $stockProduitBoutiqueId = array();
                $stockProduitBoutiqueId[STOCK_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID] = $getProduitBoutiqueId;
                $stockProduitBoutiqueId[STOCK_PRODUIT_BOUTIQUE_TAILLE] = $getTaille;

                $stock = $stockProduitBoutiqueDAO->getById($stockProduitBoutiqueId);
                $actionType = null;

                if ($stock != null) {
                    if ($postQuantiteStock == 0) {
                        $actionType = ACTION_TYPE_SUPPRESSION;
                        $stockProduitBoutiqueDAO->delete($stockProduitBoutiqueId);
                    } else {
                        $actionType = ACTION_TYPE_MODIFICATION;
                        $stock[STOCK_PRODUIT_BOUTIQUE_QUANTITE] = $postQuantiteStock;
                        $stockProduitBoutiqueDAO->update($stock);
                    }
                } else {
                    if ($postQuantiteStock != 0) {
                        $actionType = ACTION_TYPE_CREATION;
                        $stock = array();
                        $stock[STOCK_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID] = $getProduitBoutiqueId;
                        $stock[STOCK_PRODUIT_BOUTIQUE_TAILLE] = $getTaille;
                        $stock[STOCK_PRODUIT_BOUTIQUE_TAILLE] = $getTaille;
                        $stock[STOCK_PRODUIT_BOUTIQUE_QUANTITE] = $postQuantiteStock;
                        $stockProduitBoutiqueDAO->create($stock);
                    }
                }

                if (!isAdminConnected_superadmin()) {
                    MailUtils::notifySuperadmin(
                        STOCK_PRODUIT_BOUTIQUE_TABLE_NAME, 
                        $actionType, 
                        array(
                            array(
                                OBJET_ASSOCIE_TYPE => PRODUIT_BOUTIQUE_TABLE_NAME,
                                OBJET_ASSOCIE_ID => $produitBoutique[PRODUIT_BOUTIQUE_ID],
                                OBJET_ASSOCIE_LABEL => $produitBoutique[PRODUIT_BOUTIQUE_NOM]." - ".$produitBoutique[PRODUIT_BOUTIQUE_INDICATION]
                            ),
                            array(
                                OBJET_ASSOCIE_TYPE => "taille",
                                OBJET_ASSOCIE_ID => $stock[STOCK_PRODUIT_BOUTIQUE_TAILLE],
                                OBJET_ASSOCIE_LABEL => $stock[STOCK_PRODUIT_BOUTIQUE_TAILLE]
                            )
                        ), 
                        $this->getDatabaseConnection()
                    );
                }

                $response->success = true;
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}