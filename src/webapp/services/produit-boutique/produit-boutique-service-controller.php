<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("common/php/dao/produit-boutique-dao.php");

class ProduitBoutiqueServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("produit-boutique", true);
    }

    public function prixExists() {
        $this->logNavigation("addPrix", array(
            "produitBoutiqueId" => $_GET["produitBoutiqueId"],
            "taille" => $_GET["taille"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(GET, "produitBoutiqueId"),
            ));
            if ($checkParams) {
                require_once("common/php/dao/prix-produit-boutique-dao.php");

                $getProduitBoutiqueId = $_GET["produitBoutiqueId"];
                $getTaille = trim($_GET["taille"]);

                $produitBoutiqueDAO = new ProduitBoutiqueDAO($this->getDatabaseConnection());
                if ($produitBoutiqueDAO->getById($getProduitBoutiqueId) == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le produit boutique (idProduitBoutique = '".$getProduitBoutiqueId."') n'existe pas");
                }

                $prixProduitBoutiqueDAO = new PrixProduitBoutiqueDAO($this->getDatabaseConnection());

                $prixProduitBoutiqueId = array();
                $prixProduitBoutiqueId[PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID] = $getProduitBoutiqueId;
                $prixProduitBoutiqueId[PRIX_PRODUIT_BOUTIQUE_TAILLE] = $getTaille;

                $response->prixExists = ($prixProduitBoutiqueDAO->getById($prixProduitBoutiqueId) != null);
            }
        } else {
            $response->prixExists = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function addPrix() {
        $this->logNavigation("addPrix", array(
            "produitBoutiqueId" => $_GET["produitBoutiqueId"],
            "taille" => $_POST["taille"],
            "prix" => $_POST["prix"],
            "prixFournisseur" => $_POST["prixFournisseur"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(GET, "produitBoutiqueId"),
                array(POST, "prix"),
                array(POST, "prixFournisseur")
            ));
            if ($checkParams) {
                require_once("common/php/dao/prix-produit-boutique-dao.php");

                $getProduitBoutiqueId = $_GET["produitBoutiqueId"];
                $postTaille = trim($_POST["taille"]);
                $postPrix = trim($_POST["prix"]);
                $postPrixFournisseur = trim($_POST["prixFournisseur"]);

                $produitBoutiqueDAO = new ProduitBoutiqueDAO($this->getDatabaseConnection());
                $produitBoutique = $produitBoutiqueDAO->getById($getProduitBoutiqueId);
                if ($produitBoutique == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le produit boutique (idProduitBoutique = '".$getProduitBoutiqueId."') n'existe pas");
                }

                $prixProduitBoutiqueDAO = new PrixProduitBoutiqueDAO($this->getDatabaseConnection());

                $prixProduitBoutiqueId = array();
                $prixProduitBoutiqueId[PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID] = $getProduitBoutiqueId;
                $prixProduitBoutiqueId[PRIX_PRODUIT_BOUTIQUE_TAILLE] = $postTaille;

                if ($prixProduitBoutiqueDAO->getById($prixProduitBoutiqueId) != null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_400, "Le prix produit boutique (idProduitBoutique = '".$getProduitBoutiqueId."', taille = '".$postTaille."') existe deja");
                } else {
                    $prixProduitBoutique = array();
                    $prixProduitBoutique[PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID] = $getProduitBoutiqueId;
                    $prixProduitBoutique[PRIX_PRODUIT_BOUTIQUE_TAILLE] = $postTaille;
                    $prixProduitBoutique[PRIX_PRODUIT_BOUTIQUE_PRIX] = $postPrix;
                    $prixProduitBoutique[PRIX_PRODUIT_BOUTIQUE_PRIX_FOURNISSEUR] = $postPrixFournisseur;

                    $prixProduitBoutiqueDAO->create($prixProduitBoutique);

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            PRIX_PRODUIT_BOUTIQUE_TABLE_NAME, 
                            ACTION_TYPE_CREATION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => PRIX_PRODUIT_BOUTIQUE_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $produitBoutique[PRODUIT_BOUTIQUE_ID],
                                    OBJET_ASSOCIE_LABEL => $produitBoutique[PRODUIT_BOUTIQUE_NOM]." - ".$produitBoutique[PRODUIT_BOUTIQUE_INDICATION]
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function updatePrix() {
        $this->logNavigation("updatePrix", array(
            "produitBoutiqueId" => $_GET["produitBoutiqueId"],
            "taille" => $_POST["taille"],
            "prix" => $_POST["prix"],
            "prixFournisseur" => $_POST["prixFournisseur"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(GET, "produitBoutiqueId"),
                array(POST, "prix"),
                array(POST, "prixFournisseur")
            ));
            if ($checkParams) {
                require_once("common/php/dao/prix-produit-boutique-dao.php");

                $getProduitBoutiqueId = $_GET["produitBoutiqueId"];
                $getTaille = trim($_GET["taille"]);
                $postTaille = trim($_POST["taille"]);
                $postPrix = trim($_POST["prix"]);
                $postPrixFournisseur = trim($_POST["prixFournisseur"]);

                $produitBoutiqueDAO = new ProduitBoutiqueDAO($this->getDatabaseConnection());
                $produitBoutique = $produitBoutiqueDAO->getById($getProduitBoutiqueId);
                if ($produitBoutique == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La produit boutique (idProduitBoutique = '".$getProduitBoutiqueId."') n'existe pas");
                }

                $prixProduitBoutiqueDAO = new PrixProduitBoutiqueDAO($this->getDatabaseConnection());

                $prixProduitBoutiqueId = array();
                $prixProduitBoutiqueId[PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID] = $getProduitBoutiqueId;
                $prixProduitBoutiqueId[PRIX_PRODUIT_BOUTIQUE_TAILLE] = $getTaille;

                $prixProduitBoutique = $prixProduitBoutiqueDAO->getById($prixProduitBoutiqueId);
                if ($prixProduitBoutiqueId == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le prix produit boutique (idProduitBoutique = '".$getProduitBoutiqueId."', taille = '".$getTaille."') n'existe pas");
                }

                $prixProduitBoutiqueDAO->delete($prixProduitBoutique);

                $prixProduitBoutique[PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID] = $getProduitBoutiqueId;
                $prixProduitBoutique[PRIX_PRODUIT_BOUTIQUE_TAILLE] = $postTaille;
                $prixProduitBoutique[PRIX_PRODUIT_BOUTIQUE_PRIX] = $postPrix;
                $prixProduitBoutique[PRIX_PRODUIT_BOUTIQUE_PRIX_FOURNISSEUR] = $postPrixFournisseur;

                $prixProduitBoutiqueDAO->create($prixProduitBoutique);

                if (!isAdminConnected_superadmin()) {
                    MailUtils::notifySuperadmin(
                        PRIX_PRODUIT_BOUTIQUE_TABLE_NAME, 
                        ACTION_TYPE_MODIFICATION, 
                        array(
                            array(
                                OBJET_ASSOCIE_TYPE => PRIX_PRODUIT_BOUTIQUE_TABLE_NAME,
                                OBJET_ASSOCIE_ID => $produitBoutique[PRODUIT_BOUTIQUE_ID],
                                OBJET_ASSOCIE_LABEL => $produitBoutique[PRODUIT_BOUTIQUE_NOM]." - ".$produitBoutique[PRODUIT_BOUTIQUE_INDICATION]
                            )
                        ), 
                        $this->getDatabaseConnection()
                    );
                }

                $response->success = true;
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function deletePrix() {
        $this->logNavigation("deletePrix", array(
            "produitBoutiqueId" => $_POST["produitBoutiqueId"],
            "taille" => $_POST["taille"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(POST, "produitBoutiqueId")
            ));
            if ($checkParams) {
                require_once("common/php/dao/prix-produit-boutique-dao.php");

                $postProduitBoutiqueId = $_POST["produitBoutiqueId"];
                $postTaille = trim($_POST["taille"]);

                $produitBoutiqueDAO = new ProduitBoutiqueDAO($this->getDatabaseConnection());
                $produitBoutique = $produitBoutiqueDAO->getById($postProduitBoutiqueId);
                if ($produitBoutique == null) {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "La produit boutique (idProduitBoutique = '".$postProduitBoutiqueId."') n'existe pas");
                }

                $prixProduitBoutiqueDAO = new PrixProduitBoutiqueDAO($this->getDatabaseConnection());

                $prixProduitBoutiqueId = array();
                $prixProduitBoutiqueId[PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID] = $postProduitBoutiqueId;
                $prixProduitBoutiqueId[PRIX_PRODUIT_BOUTIQUE_TAILLE] = $postTaille;

                if ($prixProduitBoutiqueDAO->getById($prixProduitBoutiqueId) != null) {
                    $prixProduitBoutiqueDAO->delete($prixProduitBoutiqueId);

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            PRIX_PRODUIT_BOUTIQUE_TABLE_NAME, 
                            ACTION_TYPE_SUPPRESSION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => PRIX_PRODUIT_BOUTIQUE_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $produitBoutique[PRODUIT_BOUTIQUE_ID],
                                    OBJET_ASSOCIE_LABEL => $produitBoutique[PRODUIT_BOUTIQUE_NOM]." - ".$produitBoutique[PRODUIT_BOUTIQUE_INDICATION]
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le prix produit boutique (idProduitBoutique = '".$postProduitBoutiqueId."' / taille = '".$postTaille."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function delete() {
        $this->logNavigation("delete", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_commercial()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                require_once("common/php/dao/commande-item-boutique-dao.php");
                require_once("common/php/dao/prix-produit-boutique-dao.php");

                $postId = trim($_POST["id"]);

                $produitBoutiqueDAO = new ProduitBoutiqueDAO($this->getDatabaseConnection());
                $commandeItemBoutiqueDAO = new CommandeItemBoutiqueDAO($this->getDatabaseConnection());
                $prixProduitBoutiqueDAO = new PrixProduitBoutiqueDAO($this->getDatabaseConnection());
            
                $produitBoutique = $produitBoutiqueDAO->getById($postId);
                if ($produitBoutique != null) {
                    if (sizeof($commandeItemBoutiqueDAO->getByProduitBoutiqueId($postId)) == 0) {
                        $prixProduitBoutiqueDAO->deleteByProduitBoutiqueId($postId);
                        $produitBoutiqueDAO->delete($postId);

                        $imgProduitBoutiqueFile = PathUtils::getProduitBoutiqueImgFile($postId);
                        if (file_exists($imgProduitBoutiqueFile)) {
                            unlink($imgProduitBoutiqueFile);
                        }
                        $descriptionProduitBoutiqueFile = PathUtils::getProduitBoutiqueDescriptionFile($postId);
                        if (file_exists($descriptionProduitBoutiqueFile)) {
                            unlink($descriptionProduitBoutiqueFile);
                        }

                        if (!isAdminConnected_superadmin()) {
                            MailUtils::notifySuperadmin(
                                PRODUIT_BOUTIQUE_TABLE_NAME, 
                                ACTION_TYPE_SUPPRESSION, 
                                array(
                                    array(
                                        OBJET_ASSOCIE_TYPE => PRODUIT_BOUTIQUE_TABLE_NAME,
                                        OBJET_ASSOCIE_ID => $produitBoutique[PRODUIT_BOUTIQUE_ID],
                                        OBJET_ASSOCIE_LABEL => $produitBoutique[PRODUIT_BOUTIQUE_NOM]." - ".$produitBoutique[PRODUIT_BOUTIQUE_INDICATION]
                                    )
                                ), 
                                $this->getDatabaseConnection()
                            );
                        }

                        $response->success = true;
                    } else {
                        $response->success = false;
                        $this->sendCheckError(HTTP_400, "Le produit boutique (idProduitBoutique = '".$postId."') a des commandes associees");
                    }
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "Le produit boutique (idProduitBoutique = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}