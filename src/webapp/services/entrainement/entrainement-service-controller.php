<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/dao/entrainement-dao.php");

class EntrainementServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("entrainement", true);
    }

    public function save() {
        $this->logNavigation("save", array(
            "jour" => $_POST["jour"],
            "heureDebut" => $_POST["heureDebut"],
            "heureFin" => $_POST["heureFin"],
            "categorieId" => $_POST["categorieId"]
        ));

        $response = new stdClass;

        if (isAdminConnected_bureau()) {
            $checkParams = $this->checkParams(array(
                array(POST, "jour"),
                array(POST, "heureDebut"),
                array(POST, "heureFin"),
                array(POST, "categorieId")
            ));
            if ($checkParams) {
                $postJour = trim($_POST["jour"]);
                $postHeureDebut = DateUtils::convert_timeWithoutSeconds_to_sqlTime($_POST["heureDebut"]);
                $postHeureFin = DateUtils::convert_timeWithoutSeconds_to_sqlTime($_POST["heureFin"]);
                $postCategorieId = trim($_POST["categorieId"]);

                $entrainementDAO = new EntrainementDAO($this->getDatabaseConnection());

                $entrainement = array();
                $entrainement[ENTRAINEMENT_JOUR] = $postJour;
                $entrainement[ENTRAINEMENT_HEURE_DEBUT] = $postHeureDebut;
                $entrainement[ENTRAINEMENT_HEURE_FIN] = $postHeureFin;
                $entrainement[ENTRAINEMENT_CATEGORIE_ID] = $postCategorieId;

                $entrainement[ENTRAINEMENT_ID] = $entrainementDAO->create($entrainement);

                if (!isAdminConnected_superadmin()) {
                    MailUtils::notifySuperadmin(
                        ENTRAINEMENT_TABLE_NAME, 
                        ACTION_TYPE_CREATION, 
                        array(
                            array(
                                OBJET_ASSOCIE_TYPE => ENTRAINEMENT_TABLE_NAME,
                                OBJET_ASSOCIE_ID => $entrainement[ENTRAINEMENT_ID],
                                OBJET_ASSOCIE_LABEL => ENTRAINEMENT_CATEGORIE_ID."=".$entrainement[ENTRAINEMENT_CATEGORIE_ID].", ".ENTRAINEMENT_JOUR."=".$entrainement[ENTRAINEMENT_JOUR].", ".ENTRAINEMENT_HEURE_DEBUT."=".$entrainement[ENTRAINEMENT_HEURE_DEBUT].", ".ENTRAINEMENT_HEURE_FIN."=".$entrainement[ENTRAINEMENT_HEURE_FIN]
                            )
                        ), 
                        $this->getDatabaseConnection()
                    );
                }

                $response->success = true;
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }

    public function delete() {
        $this->logNavigation("delete", array(
            "id" => $_POST["id"]
        ));

        $response = new stdClass;

        if (isAdminConnected_bureau()) {
            $checkParams = $this->checkParams(array(
                array(POST, "id")
            ));
            if ($checkParams) {
                $postId = trim($_POST["id"]);

                $entrainementDAO = new EntrainementDAO($this->getDatabaseConnection());
            
                $entrainement = $entrainementDAO->getById($postId);
                if ($entrainement != null) {
                    $entrainementDAO->delete($postId);

                    if (!isAdminConnected_superadmin()) {
                        MailUtils::notifySuperadmin(
                            ENTRAINEMENT_TABLE_NAME, 
                            ACTION_TYPE_SUPPRESSION, 
                            array(
                                array(
                                    OBJET_ASSOCIE_TYPE => ENTRAINEMENT_TABLE_NAME,
                                    OBJET_ASSOCIE_ID => $entrainement[ENTRAINEMENT_ID],
                                    OBJET_ASSOCIE_LABEL => ENTRAINEMENT_CATEGORIE_ID."=".$entrainement[ENTRAINEMENT_CATEGORIE_ID].", ".ENTRAINEMENT_JOUR."=".$entrainement[ENTRAINEMENT_JOUR].", ".ENTRAINEMENT_HEURE_DEBUT."=".$entrainement[ENTRAINEMENT_HEURE_DEBUT].", ".ENTRAINEMENT_HEURE_FIN."=".$entrainement[ENTRAINEMENT_HEURE_FIN]
                                )
                            ), 
                            $this->getDatabaseConnection()
                        );
                    }

                    $response->success = true;
                } else {
                    $response->success = false;
                    $this->sendCheckError(HTTP_404, "L'entrainement (idEntrainement = '".$postId."') n'existe pas");
                }
            }
        } else {
            $response->success = false;
            $this->sendCheckError(HTTP_401, "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; ce service");
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}

?>