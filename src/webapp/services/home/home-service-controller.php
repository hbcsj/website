<?php
require_once("core/php/controllers/abstract-service-controller.php");
require_once("core/php/lib/mailer.php");

class HomeServiceCtrl extends AbstractServiceCtrl {

    public function __construct() {
        parent::__construct("home", true);
    }

    public function sendMessage() {
        $this->logNavigation("sendMessage", array(
            "nom" => $_POST["nom"],
            "email" => $_POST["email"],
            "message" => $_POST["message"]
        ));

        $response = new stdClass;

        $checkParams = $this->checkParams(array(
            array(POST, "nom"),
            array(POST, "email"),
            array(POST, "message")
        ));
        if ($checkParams) {
            require_once("common/php/dao/infos-club-dao.php");
            
            $postNom = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["nom"]));
            $postEmail = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["email"]));
            $postMessage = StringUtils::convertAccentsToHtmlSpecialChars(trim(nl2br($_POST["message"])));

            $infosClubDAO = new InfosClubDAO($this->getDatabaseConnection());
            $infosClub = $infosClubDAO->getSingleton();

            $sujet = "[Site du ".$infosClub[INFOS_CLUB_NOM_XS]."] - Message de ".$postNom;
            $mailer = new Mailer($postEmail, $infosClub[INFOS_CLUB_EMAIL], $sujet, $postMessage);
            $mailer->send();

            $response->success = true;
        }

        $this->generateResponse($response, CONTENT_TYPE_JSON);
    }
}