<?php
require_once("core/php/controllers/abstract-uploader-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("common/php/dao/article-presse-dao.php");
require_once("common/php/dao/journal-article-presse-dao.php");

class ArticlePresseUploaderCtrl extends AbstractUploaderCtrl {

    private $articlePresseDAO;
    private $journalArticlePresseDAO;

    public function __construct($pageName) {
		parent::__construct($pageName, array(
            "id" => $_GET["id"],
            "titre" => $_POST["titre"],
            "url" => $_POST["url"],
            "email-dernier-editeur" => $_POST["email-dernier-editeur"],
            "visible-sur-site" => $_POST["visible-sur-site"],
            "journal-article-presse-id" => $_POST["journal-article-presse-id"],
            "article-presse-image" => $_FILES["article-presse-image"],
            "article-presse-image-medium" => $_FILES["article-presse-image-medium"],
            "article-presse-image-small" => $_FILES["article-presse-image-small"],
            "article-presse-image-home" => $_FILES["article-presse-image-home"]
        ), true);
		
		if (isAdminConnected_editeur()) {
            $this->articlePresseDAO = new ArticlePresseDAO($this->getDatabaseConnection());
            $this->journalArticlePresseDAO = new JournalArticlePresseDAO($this->getDatabaseConnection());
            
            if ($this->checkRequest()) {
                $this->doPost();
            }
        } else {
			$this->setSuccess(false);
            $this->sendCheckError(
                HTTP_401, 
                "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cet uploader"
            );
		}
	}

	private function checkRequest() {
		$checkParams = $this->checkParams(array(
            array(POST, "titre"),
            array(POST, "url"),
            array(POST, "email-dernier-editeur"),
            array(POST, "journal-article-presse-id")
        ));
		if (!$checkParams) {
			$this->setSuccess(false);
            $this->sendCheckError(HTTP_400);
        }
        return $checkParams;
    }
    
    public function doPost() {
        $getId = $_GET["id"];
        $postTitre = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["titre"]));
        $postUrl = trim($_POST["url"]);
        $postVisibleSurSite = ($_POST["visible-sur-site"] == "on") ? "1" : "0";
        $postEmailDernierEditeur = trim($_POST["email-dernier-editeur"]);
        $postJournalArticlePresseId = trim($_POST["journal-article-presse-id"]);

        $journalArticlePresse = $this->journalArticlePresseDAO->getById($postJournalArticlePresseId);
        if ($journalArticlePresse == null) {
            $this->setSuccess(false);
            $this->sendCheckError(HTTP_404, "Le journal (idJournalArticlePresse = '".$postJournalArticlePresseId."') n'existe pas");
        }

        $articlePresse = array();
        if (HTTPUtils::paramExists(GET, "id")) {
            $articlePresseById = $this->articlePresseDAO->getById($getId);
            if ($articlePresseById != null) {
                $articlePresse = $articlePresseById;
            } else {
                $this->setSuccess(false);
                $this->sendCheckError(HTTP_404, "L'article de presse (idArticlePresse = '".$getId."') n'existe pas");
            }
        }
        $articlePresse[ARTICLE_PRESSE_TITRE] = $postTitre;
        $articlePresse[ARTICLE_PRESSE_URL] = $postUrl;
        $articlePresse[ARTICLE_PRESSE_VISIBLE_SUR_SITE] = $postVisibleSurSite;
        $articlePresse[ARTICLE_PRESSE_EMAIL_DERNIER_EDITEUR] = $postEmailDernierEditeur;
        $articlePresse[ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID] = $postJournalArticlePresseId;

        $actionType = ACTION_TYPE_CREATION;
        if (array_key_exists(ARTICLE_PRESSE_ID, $articlePresse)) {
            $actionType = ACTION_TYPE_MODIFICATION;
            $this->articlePresseDAO->update($articlePresse);
            
            if ($_FILES["article-presse-image"]["name"] != null && $_FILES["article-presse-image"]["name"] != "") {
                $articlePresseImgFile = PathUtils::getArticlePresseImgFile($articlePresse[ARTICLE_PRESSE_ID]);
                if (file_exists($articlePresseImgFile)) {
                    unlink($articlePresseImgFile);
                }
                move_uploaded_file($_FILES["article-presse-image"]["tmp_name"], $articlePresseImgFile);
            }
            
            if ($_FILES["article-presse-image-medium"]["name"] != null && $_FILES["article-presse-image-medium"]["name"] != "") {
                $articlePresseImgMediumFile = PathUtils::getArticlePresseImgMediumFile($articlePresse[ARTICLE_PRESSE_ID]);
                if (file_exists($articlePresseImgMediumFile)) {
                    unlink($articlePresseImgMediumFile);
                }
                move_uploaded_file($_FILES["article-presse-image-medium"]["tmp_name"], $articlePresseImgMediumFile);
            }
            
            if ($_FILES["article-presse-image-small"]["name"] != null && $_FILES["article-presse-image-small"]["name"] != "") {
                $articlePresseImgSmallFile = PathUtils::getArticlePresseImgSmallFile($articlePresse[ARTICLE_PRESSE_ID]);
                if (file_exists($articlePresseImgSmallFile)) {
                    unlink($articlePresseImgSmallFile);
                }
                move_uploaded_file($_FILES["article-presse-image-small"]["tmp_name"], $articlePresseImgSmallFile);
            }

            if ($_FILES["article-presse-image-home"]["name"] != null && $_FILES["article-presse-image-home"]["name"] != "") {
                $articlePresseImgHomeFile = PathUtils::getArticlePresseImgHomeFile($articlePresse[ARTICLE_PRESSE_ID]);
                if (file_exists($articlePresseImgHomeFile)) {
                    unlink($articlePresseImgHomeFile);
                }
                move_uploaded_file($_FILES["article-presse-image-home"]["tmp_name"], $articlePresseImgHomeFile);
            }
        } else {
            $articlePresse[ARTICLE_PRESSE_DATE] = date(SQL_DATE_FORMAT);
            $articlePresse[ARTICLE_PRESSE_ID] = $this->articlePresseDAO->create($articlePresse);
            move_uploaded_file($_FILES["article-presse-image"]["tmp_name"], PathUtils::getArticlePresseImgFile($articlePresse[ARTICLE_PRESSE_ID]));
            move_uploaded_file($_FILES["article-presse-image-medium"]["tmp_name"], PathUtils::getArticlePresseImgMediumFile($articlePresse[ARTICLE_PRESSE_ID]));
            move_uploaded_file($_FILES["article-presse-image-small"]["tmp_name"], PathUtils::getArticlePresseImgSmallFile($articlePresse[ARTICLE_PRESSE_ID]));
            move_uploaded_file($_FILES["article-presse-image-home"]["tmp_name"], PathUtils::getArticlePresseImgHomeFile($articlePresse[ARTICLE_PRESSE_ID]));
        }

        if (!isAdminConnected_superadmin()) {
            MailUtils::notifySuperadmin(
                ARTICLE_PRESSE_TABLE_NAME, 
                $actionType, 
                array(
                    array(
                        OBJET_ASSOCIE_TYPE => ARTICLE_PRESSE_TABLE_NAME,
                        OBJET_ASSOCIE_ID => $articlePresse[ARTICLE_PRESSE_ID],
                        OBJET_ASSOCIE_LABEL => $articlePresse[ARTICLE_PRESSE_TITRE]
                    )
                ), 
                $this->getDatabaseConnection()
            );
        }

        $this->setSuccess(true);
    }
}

?>