ArticlePresseUploaderController = {};

ArticlePresseUploaderController.control_uploadArticlePresse = function() {
    window.top.ViewUtils.activeButtonAndHideLoader('#modal-save-article-presse__bt-sauvegarder', '#modal-save-article-presse__bt-sauvegarder .loader');
    if ($('#success').text() == 'true') {
        window.top.FormUtils.displayFormResultMessage('#modal-save-article-presse .form-result-message', 'Article de presse sauvegard&eacute;', FormUtils.resultsTypes.ok);
        window.top.AdminEditionPresseController.load_StatsArticlesPresse();
        window.top.AdminEditionPresseController.load_TableArticlesPresse();
        window.top.$('#modal-save-article-presse__bt-sauvegarder').prop('disabled', true);
    } else {
        window.top.ViewUtils.activeButtonAndHideLoader('#modal-save-article-presse__bt-sauvegarder', '#modal-save-article-presse__bt-sauvegarder .loader');
        window.top.FormUtils.displayFormResultMessage('#modal-save-article-presse .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
    }
};

initializeUploader = function() {
    ArticlePresseUploaderController.control_uploadArticlePresse();
};