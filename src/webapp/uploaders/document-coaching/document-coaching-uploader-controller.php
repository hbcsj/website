<?php
require_once("core/php/controllers/abstract-uploader-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("common/php/dao/document-coaching-dao.php");
require_once("common/php/dao/type-document-coaching-dao.php");

class DocumentCoachingUploaderCtrl extends AbstractUploaderCtrl {

    private $documentCoachingDAO;
    private $typeDocumentCoachingDAO;

    private $withFile;

    public function __construct($pageName) {
		parent::__construct($pageName, array(
            "id" => $_GET["id"],
            "titre" => $_POST["titre"],
            "mots-clefs" => $_POST["mots-clefs"],
            "url" => $_POST["url"],
            "nom-dernier-editeur" => $_POST["nom-dernier-editeur"],
            "type-document-coaching-id" => $_POST["type-document-coaching-id"],
            "document-file" => $_FILES["document-file"]
        ), true);
		
		if (isAdminConnected_coach()) {
            $this->documentCoachingDAO = new DocumentCoachingDAO($this->getDatabaseConnection());
            $this->typeDocumentCoachingDAO = new TypeDocumentCoachingDAO($this->getDatabaseConnection());
            
            if ($this->checkRequest()) {
                $this->doPost();
            }
        } else {
			$this->setSuccess(false);
            $this->sendCheckError(
                HTTP_401, 
                "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cet uploader"
            );
		}
	}

	private function checkRequest() {
		$checkParams = $this->checkParams(array(
            array(POST, "titre"),
            array(POST, "nom-dernier-editeur"),
            array(POST, "type-document-coaching-id")
        ));
		if (!$checkParams) {
			$this->setSuccess(false);
            $this->sendCheckError(HTTP_400);
        } else {
            $this->withFile = ($_FILES["document-file"]["name"] != null && $_FILES["document-file"]["name"] != "");
            if (!HTTPUtils::paramExists(GET, "id") && !$this->withFile && trim($_POST["url"]) == "") {
                $this->setSuccess(false);
                $this->sendCheckError(HTTP_400, "Il faut renseigner au moins un fichier OU une url");
            }
        }
        return $checkParams;
	}
    
    public function doPost() {
        $getId = $_GET["id"];
        $postTitre = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["titre"]));
        $postMotsClefs = strtolower(StringUtils::removeHtmlSpecialChars(StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["mots-clefs"]))));
        $postUrl = trim($_POST["url"]);
        $postNomDernierEditeur = trim($_POST["nom-dernier-editeur"]);
        $postTypeDocumentCoachingId = trim($_POST["type-document-coaching-id"]);

        $typeDocumentCoaching = $this->typeDocumentCoachingDAO->getById($postTypeDocumentCoachingId);
        if ($typeDocumentCoaching == null) {
            $this->setSuccess(false);
            $this->sendCheckError(HTTP_404, "Le type document coaching (idTypeDocumentCoaching = '".$postTypeDocumentCoachingId."') n'existe pas");
        }

        $documentCoaching = array();
        if (HTTPUtils::paramExists(GET, "id")) {
            $documentCoachingById = $this->documentCoachingDAO->getById($getId);
            if ($documentCoachingById != null) {
                $documentCoaching = $documentCoachingById;
            } else {
                $this->setSuccess(false);
                $this->sendCheckError(HTTP_404, "Le documentCoaching (idDocumentCoaching = '".$getId."') n'existe pas");
            }
        } else {
            $documentCoaching[DOCUMENT_COACHING_FICHIER] = "";
        }
        $documentCoaching[DOCUMENT_COACHING_TITRE] = $postTitre;
        $documentCoaching[DOCUMENT_COACHING_MOTS_CLEFS] =  strtolower(StringUtils::removeHtmlSpecialChars($typeDocumentCoaching[TYPE_DOCUMENT_COACHING_LIBELLE])).",".$postMotsClefs;
        if ($this->withFile) {
            $documentCoaching[DOCUMENT_COACHING_FICHIER] = $_FILES["document-file"]["name"];
        }
        $documentCoaching[DOCUMENT_COACHING_URL] = $postUrl;
        $documentCoaching[DOCUMENT_COACHING_NOM_EDITEUR] = $postNomDernierEditeur;
        $documentCoaching[DOCUMENT_COACHING_DATE_HEURE_MODIFICATION] = date(SQL_DATE_TIME_FORMAT);
        $documentCoaching[DOCUMENT_COACHING_TYPE_DOCUMENT_COACHING_ID] = $postTypeDocumentCoachingId;

        $actionType = ACTION_TYPE_CREATION;
        if (array_key_exists(DOCUMENT_COACHING_ID, $documentCoaching)) {
            $actionType = ACTION_TYPE_MODIFICATION;
            $this->documentCoachingDAO->update($documentCoaching);
            if ($this->withFile) {
                $docCoachingFile = PathUtils::getDocumentCoachingFile($documentCoaching[DOCUMENT_COACHING_FICHIER], $documentCoaching[DOCUMENT_COACHING_ID]);
                if (file_exists($docCoachingFile)) {
                    unlink($docCoachingFile);
                }
                move_uploaded_file($_FILES["document-file"]["tmp_name"], $docCoachingFile);
            }
        } else {
            $documentCoaching[DOCUMENT_COACHING_ID] = $this->documentCoachingDAO->create($documentCoaching);
            if ($this->withFile) {
                move_uploaded_file($_FILES["document-file"]["tmp_name"], PathUtils::getDocumentCoachingFile($documentCoaching[DOCUMENT_COACHING_FICHIER], $documentCoaching[DOCUMENT_COACHING_ID]));
            }
        }

        if (!isAdminConnected_superadmin()) {
            MailUtils::notifySuperadmin(
                DOCUMENT_COACHING_TABLE_NAME, 
                $actionType, 
                array(
                    array(
                        OBJET_ASSOCIE_TYPE => DOCUMENT_COACHING_TABLE_NAME,
                        OBJET_ASSOCIE_ID => $documentCoaching[DOCUMENT_COACHING_ID],
                        OBJET_ASSOCIE_LABEL => $documentCoaching[DOCUMENT_COACHING_TITRE]
                    )
                ), 
                $this->getDatabaseConnection()
            );
        }

        $this->setSuccess(true);
    }
}

?>