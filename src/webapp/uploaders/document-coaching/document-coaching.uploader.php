<?php
require_once("core/php/resources/uploader.php");
require_once("common/php/lib/admin-utils.php");

$uploader = new Uploader(
    "webapp/uploaders/document-coaching/", 
    "document-coaching-uploader", 
    "DocumentCoachingUploaderCtrl"
);
$ctrl = $uploader->getController();

if (isAdminConnected_coach()) {
    ?>
    <span id="success"><?php echo $ctrl->getSuccess(); ?></span>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401);
}

$uploader->finalizeUploader();
?>