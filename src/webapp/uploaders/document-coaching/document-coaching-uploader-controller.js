DocumentCoachingUploaderController = {};

DocumentCoachingUploaderController.control_uploadDocumentCoaching = function() {
    window.top.ViewUtils.activeButtonAndHideLoader('#modal-save-document__bt-sauvegarder', '#modal-save-document__bt-sauvegarder .loader');
    if ($('#success').text() == 'true') {
        window.top.FormUtils.displayFormResultMessage('#modal-save-document .form-result-message', 'Document sauvegard&eacute;', FormUtils.resultsTypes.ok);
        window.top.AdminCoachingDocumentsController.load_TableDocuments();
        window.top.$('#modal-save-document__bt-sauvegarder').prop('disabled', true);
    } else {
        window.top.ViewUtils.activeButtonAndHideLoader('#modal-save-document__bt-sauvegarder', '#modal-save-document__bt-sauvegarder .loader');
        window.top.FormUtils.displayFormResultMessage('#modal-save-document .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
    }
};

initializeUploader = function() {
    DocumentCoachingUploaderController.control_uploadDocumentCoaching();
};