AlbumPhotoUploaderController = {};

AlbumPhotoUploaderController.control_uploadAlbumPhoto = function() {
    window.top.ViewUtils.activeButtonAndHideLoader('#modal-save-album-photo__bt-sauvegarder', '#modal-save-album-photo__bt-sauvegarder .loader');
    if ($('#success').text() == 'true') {
        window.top.FormUtils.displayFormResultMessage('#modal-save-album-photo .form-result-message', 'Album photo sauvegard&eacute;', FormUtils.resultsTypes.ok);
        window.top.AdminEditionPhotosVideosController.load_TableAlbumsPhotos();
        window.top.$('#modal-save-album-photo__bt-sauvegarder').prop('disabled', true);
    } else {
        window.top.ViewUtils.activeButtonAndHideLoader('#modal-save-album-photo__bt-sauvegarder', '#modal-save-album-photo__bt-sauvegarder .loader');
        window.top.FormUtils.displayFormResultMessage('#modal-save-album-photo .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
    }
};

initializeUploader = function() {
    AlbumPhotoUploaderController.control_uploadAlbumPhoto();
};