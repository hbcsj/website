<?php
require_once("core/php/controllers/abstract-uploader-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("common/php/dao/album-photo-dao.php");

class AlbumPhotoUploaderCtrl extends AbstractUploaderCtrl {

    private $albumPhotoDAO;

    public function __construct($pageName) {
		parent::__construct($pageName, array(
            "id" => $_GET["id"],
            "nom" => $_POST["nom"],
            "url" => $_POST["url"],
            "email-dernier-editeur" => $_POST["email-dernier-editeur"],
            "visible-sur-site" => $_POST["visible-sur-site"],
            "album-photo-image" => $_FILES["album-photo-image"],
            "album-photo-image-small" => $_FILES["album-photo-image-small"]
        ), true);
		
		if (isAdminConnected_editeur()) {
            $this->albumPhotoDAO = new AlbumPhotoDAO($this->getDatabaseConnection());
            
            if ($this->checkRequest()) {
                $this->doPost();
            }
        } else {
			$this->setSuccess(false);
            $this->sendCheckError(
                HTTP_401, 
                "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cet uploader"
            );
		}
	}

	private function checkRequest() {
		$checkParams = $this->checkParams(array(
            array(POST, "nom"),
            array(POST, "url"),
            array(POST, "email-dernier-editeur")
        ));
		if (!$checkParams) {
			$this->setSuccess(false);
            $this->sendCheckError(HTTP_400);
        }
        return $checkParams;
    }
    
    public function doPost() {
        $getId = $_GET["id"];
        $postNom = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["nom"]));
        $postUrl = trim($_POST["url"]);
        $postVisibleSurSite = ($_POST["visible-sur-site"] == "on") ? "1" : "0";
        $postEmailDernierEditeur = trim($_POST["email-dernier-editeur"]);

        $albumPhoto = array();
        if (HTTPUtils::paramExists(GET, "id")) {
            $albumPhotoById = $this->albumPhotoDAO->getById($getId);
            if ($albumPhotoById != null) {
                $albumPhoto = $albumPhotoById;
            } else {
                $this->setSuccess(false);
                $this->sendCheckError(HTTP_404, "L'album photo (idAlbumPhoto = '".$getId."') n'existe pas");
            }
        }
        $albumPhoto[ALBUM_PHOTO_NOM] = $postNom;
        $albumPhoto[ALBUM_PHOTO_URL] = $postUrl;
        $albumPhoto[ALBUM_PHOTO_VISIBLE_SUR_SITE] = $postVisibleSurSite;
        $albumPhoto[ALBUM_PHOTO_EMAIL_DERNIER_EDITEUR] = $postEmailDernierEditeur;

        $actionType = ACTION_TYPE_CREATION;
        if (array_key_exists(ALBUM_PHOTO_ID, $albumPhoto)) {
            $actionType = ACTION_TYPE_MODIFICATION;
            $this->albumPhotoDAO->update($albumPhoto);
            
            if ($_FILES["album-photo-image"]["name"] != null && $_FILES["album-photo-image"]["name"] != "") {
                $albumPhotoImgFile = PathUtils::getAlbumPhotoImgFile($albumPhoto[ALBUM_PHOTO_ID]);
                if (file_exists($albumPhotoImgFile)) {
                    unlink($albumPhotoImgFile);
                }
                move_uploaded_file($_FILES["album-photo-image"]["tmp_name"], $albumPhotoImgFile);
            }
            
            if ($_FILES["album-photo-image-small"]["name"] != null && $_FILES["album-photo-image-small"]["name"] != "") {
                $albumPhotoImgSmallFile = PathUtils::getAlbumPhotoImgSmallFile($albumPhoto[ALBUM_PHOTO_ID]);
                if (file_exists($albumPhotoImgSmallFile)) {
                    unlink($albumPhotoImgSmallFile);
                }
                move_uploaded_file($_FILES["album-photo-image-small"]["tmp_name"], $albumPhotoImgSmallFile);
            }
        } else {
            $albumPhoto[ALBUM_PHOTO_ID] = $this->albumPhotoDAO->create($albumPhoto);
            move_uploaded_file($_FILES["album-photo-image"]["tmp_name"], PathUtils::getAlbumPhotoImgFile($albumPhoto[ALBUM_PHOTO_ID]));
            move_uploaded_file($_FILES["album-photo-image-small"]["tmp_name"], PathUtils::getAlbumPhotoImgSmallFile($albumPhoto[ALBUM_PHOTO_ID]));
        }

        if (!isAdminConnected_superadmin()) {
            MailUtils::notifySuperadmin(
                ALBUM_PHOTO_TABLE_NAME, 
                $actionType, 
                array(
                    array(
                        OBJET_ASSOCIE_TYPE => ALBUM_PHOTO_TABLE_NAME,
                        OBJET_ASSOCIE_ID => $albumPhoto[ALBUM_PHOTO_ID],
                        OBJET_ASSOCIE_LABEL => $albumPhoto[ALBUM_PHOTO_NOM]
                    )
                ), 
                $this->getDatabaseConnection()
            );
        }

        $this->setSuccess(true);
    }
}

?>