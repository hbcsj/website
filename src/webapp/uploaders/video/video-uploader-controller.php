<?php
require_once("core/php/controllers/abstract-uploader-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("common/php/dao/video-dao.php");

class VideoUploaderCtrl extends AbstractUploaderCtrl {

    private $videoDAO;

    public function __construct($pageName) {
		parent::__construct($pageName, array(
            "id" => $_GET["id"],
            "nom" => $_POST["nom"],
            "url" => $_POST["url"],
            "email-dernier-editeur" => $_POST["email-dernier-editeur"],
            "visible-sur-site" => $_POST["visible-sur-site"],
            "video-image" => $_FILES["video-image"],
            "video-image-small" => $_FILES["video-image-small"]
        ), true);
		
		if (isAdminConnected_editeur()) {
            $this->videoDAO = new VideoDAO($this->getDatabaseConnection());
            
            if ($this->checkRequest()) {
                $this->doPost();
            }
        } else {
			$this->setSuccess(false);
            $this->sendCheckError(
                HTTP_401, 
                "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cet uploader"
            );
		}
	}

	private function checkRequest() {
		$checkParams = $this->checkParams(array(
            array(POST, "nom"),
            array(POST, "url"),
            array(POST, "email-dernier-editeur")
        ));
		if (!$checkParams) {
			$this->setSuccess(false);
            $this->sendCheckError(HTTP_400);
        }
        return $checkParams;
    }
    
    public function doPost() {
        $getId = $_GET["id"];
        $postNom = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["nom"]));
        $postUrl = trim($_POST["url"]);
        $postVisibleSurSite = ($_POST["visible-sur-site"] == "on") ? "1" : "0";
        $postEmailDernierEditeur = trim($_POST["email-dernier-editeur"]);

        $video = array();
        if (HTTPUtils::paramExists(GET, "id")) {
            $videoById = $this->videoDAO->getById($getId);
            if ($videoById != null) {
                $video = $videoById;
            } else {
                $this->setSuccess(false);
                $this->sendCheckError(HTTP_404, "La video (idVideo = '".$getId."') n'existe pas");
            }
        }
        $video[VIDEO_NOM] = $postNom;
        $video[VIDEO_URL] = $postUrl;
        $video[VIDEO_VISIBLE_SUR_SITE] = $postVisibleSurSite;
        $video[VIDEO_EMAIL_DERNIER_EDITEUR] = $postEmailDernierEditeur;

        $actionType = ACTION_TYPE_CREATION;
        if (array_key_exists(VIDEO_ID, $video)) {
            $actionType = ACTION_TYPE_MODIFICATION;
            $this->videoDAO->update($video);
            
            if ($_FILES["video-image"]["name"] != null && $_FILES["video-image"]["name"] != "") {
                $videoImgFile = PathUtils::getVideoImgFile($video[VIDEO_ID]);
                if (file_exists($videoImgFile)) {
                    unlink($videoImgFile);
                }
                move_uploaded_file($_FILES["video-image"]["tmp_name"], $videoImgFile);
            }
            
            if ($_FILES["video-image-small"]["name"] != null && $_FILES["video-image-small"]["name"] != "") {
                $videoImgSmallFile = PathUtils::getVideoImgSmallFile($video[VIDEO_ID]);
                if (file_exists($videoImgSmallFile)) {
                    unlink($videoImgSmallFile);
                }
                move_uploaded_file($_FILES["video-image-small"]["tmp_name"], $videoImgSmallFile);
            }
        } else {
            $video[VIDEO_ID] = $this->videoDAO->create($video);
            move_uploaded_file($_FILES["video-image"]["tmp_name"], PathUtils::getVideoImgFile($video[VIDEO_ID]));
            move_uploaded_file($_FILES["video-image-small"]["tmp_name"], PathUtils::getVideoImgSmallFile($video[VIDEO_ID]));
        }

        if (!isAdminConnected_superadmin()) {
            MailUtils::notifySuperadmin(
                VIDEO_TABLE_NAME, 
                $actionType, 
                array(
                    array(
                        OBJET_ASSOCIE_TYPE => VIDEO_TABLE_NAME,
                        OBJET_ASSOCIE_ID => $video[VIDEO_ID],
                        OBJET_ASSOCIE_LABEL => $video[VIDEO_NOM]
                    )
                ), 
                $this->getDatabaseConnection()
            );
        }

        $this->setSuccess(true);
    }
}

?>