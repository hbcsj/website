VideoUploaderController = {};

VideoUploaderController.control_uploadVideo = function() {
    window.top.ViewUtils.activeButtonAndHideLoader('#modal-save-video__bt-sauvegarder', '#modal-save-video__bt-sauvegarder .loader');
    if ($('#success').text() == 'true') {
        window.top.FormUtils.displayFormResultMessage('#modal-save-video .form-result-message', 'Vid&eacute;o sauvegard&eacute;e', FormUtils.resultsTypes.ok);
        window.top.AdminEditionPhotosVideosController.load_TableVideos();
        window.top.$('#modal-save-video__bt-sauvegarder').prop('disabled', true);
    } else {
        window.top.ViewUtils.activeButtonAndHideLoader('#modal-save-video__bt-sauvegarder', '#modal-save-video__bt-sauvegarder .loader');
        window.top.FormUtils.displayFormResultMessage('#modal-save-video .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
    }
};

initializeUploader = function() {
    VideoUploaderController.control_uploadVideo();
};