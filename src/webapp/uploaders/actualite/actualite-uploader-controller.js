ActualiteUploaderController = {};

ActualiteUploaderController.control_uploadActualite = function() {
    window.top.ViewUtils.activeButtonAndHideLoader('#modal-save-actualite__bt-sauvegarder', '#modal-save-actualite__bt-sauvegarder .loader');
    if ($('#success').text() == 'true') {
        window.top.FormUtils.displayFormResultMessage('#modal-save-actualite .form-result-message', 'Actualit&eacute; sauvegard&eacute;', FormUtils.resultsTypes.ok);
        window.top.AdminEditionNewsController.load_StatsNews();
        window.top.AdminEditionNewsController.load_TableNews();
        window.top.$('#modal-save-actualite__bt-sauvegarder').prop('disabled', true);
    } else {
        window.top.ViewUtils.activeButtonAndHideLoader('#modal-save-actualite__bt-sauvegarder', '#modal-save-actualite__bt-sauvegarder .loader');
        window.top.FormUtils.displayFormResultMessage('#modal-save-actualite .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
    }
};

initializeUploader = function() {
    ActualiteUploaderController.control_uploadActualite();
};