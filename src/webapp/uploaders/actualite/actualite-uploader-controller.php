<?php
require_once("core/php/controllers/abstract-uploader-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("common/php/dao/actualite-dao.php");

class ActualiteUploaderCtrl extends AbstractUploaderCtrl {

    private $actualiteDAO;

    public function __construct($pageName) {
		parent::__construct($pageName, array(
            "id" => $_GET["id"],
            "titre" => $_POST["titre"],
            "email-dernier-editeur" => $_POST["email-dernier-editeur"],
            "visible-sur-site" => $_POST["visible-sur-site"],
            "actualite-image" => $_FILES["actualite-image"],
            "actualite-sommaire" => $_FILES["actualite-sommaire"],
            "actualite-contenu" => $_FILES["actualite-contenu"]
        ), true);
		
		if (isAdminConnected_editeur()) {
            $this->actualiteDAO = new ActualiteDAO($this->getDatabaseConnection());
            
            if ($this->checkRequest()) {
                $this->doPost();
            }
        } else {
			$this->setSuccess(false);
            $this->sendCheckError(
                HTTP_401, 
                "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cet uploader"
            );
		}
	}

	private function checkRequest() {
		$checkParams = $this->checkParams(array(
            array(POST, "titre"),
            array(POST, "email-dernier-editeur")
        ));
		if (!$checkParams) {
			$this->setSuccess(false);
            $this->sendCheckError(HTTP_400);
        }
        return $checkParams;
    }
    
    public function doPost() {
        $getId = $_GET["id"];
        $postTitre = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["titre"]));
        $postVisibleSurSite = ($_POST["visible-sur-site"] == "on") ? "1" : "0";
        $postEmailDernierEditeur = trim($_POST["email-dernier-editeur"]);

        $actualite = array();
        if (HTTPUtils::paramExists(GET, "id")) {
            $actualiteById = $this->actualiteDAO->getById($getId);
            if ($actualiteById != null) {
                $actualite = $actualiteById;
            } else {
                $this->setSuccess(false);
                $this->sendCheckError(HTTP_404, "L'actualit&eacute; (idActualite = '".$getId."') n'existe pas");
            }
        }
        $actualite[ACTUALITE_TITRE] = $postTitre;
        $actualite[ACTUALITE_VISIBLE_SUR_SITE] = $postVisibleSurSite;
        $actualite[ACTUALITE_EMAIL_DERNIER_EDITEUR] = $postEmailDernierEditeur;

        $actionType = ACTION_TYPE_CREATION;
        if (array_key_exists(ACTUALITE_ID, $actualite)) {
            $actionType = ACTION_TYPE_MODIFICATION;
            $this->actualiteDAO->update($actualite);
            
            if ($_FILES["actualite-sommaire"]["name"] != null && $_FILES["actualite-sommaire"]["name"] != "") {
                $actualiteHomeFile = PathUtils::getActualiteHomeFile($actualite[ACTUALITE_ID]);
                if (file_exists($actualiteHomeFile)) {
                    unlink($actualiteHomeFile);
                }
                move_uploaded_file($_FILES["actualite-sommaire"]["tmp_name"], $actualiteHomeFile);
            }

            if ($_FILES["actualite-contenu"]["name"] != null && $_FILES["actualite-contenu"]["name"] != "") {
                $actualiteFile = PathUtils::getActualiteFile($actualite[ACTUALITE_ID]);
                if (file_exists($actualiteFile)) {
                    unlink($actualiteFile);
                }
                move_uploaded_file($_FILES["actualite-contenu"]["tmp_name"], $actualiteFile);
            }
            
            if ($_FILES["actualite-image"]["name"] != null && $_FILES["actualite-image"]["name"] != "") {
                $actualiteImgFile = PathUtils::getActualiteImgFile($actualite[ACTUALITE_ID]);
                if (file_exists($actualiteImgFile)) {
                    unlink($actualiteImgFile);
                }
                move_uploaded_file($_FILES["actualite-image"]["tmp_name"], $actualiteImgFile);
            }
        } else {
            $actualite[ACTUALITE_DATE_HEURE] = date(SQL_DATE_TIME_FORMAT);
            $actualite[ACTUALITE_ID] = $this->actualiteDAO->create($actualite);
            move_uploaded_file($_FILES["actualite-image"]["tmp_name"], PathUtils::getActualiteImgFile($actualite[ACTUALITE_ID]));
            move_uploaded_file($_FILES["actualite-sommaire"]["tmp_name"], PathUtils::getActualiteHomeFile($actualite[ACTUALITE_ID]));
            move_uploaded_file($_FILES["actualite-contenu"]["tmp_name"], PathUtils::getActualiteFile($actualite[ACTUALITE_ID]));
        }

        if (!isAdminConnected_superadmin()) {
            MailUtils::notifySuperadmin(
                ACTUALITE_TABLE_NAME, 
                $actionType, 
                array(
                    array(
                        OBJET_ASSOCIE_TYPE => ACTUALITE_TABLE_NAME,
                        OBJET_ASSOCIE_ID => $actualite[ACTUALITE_ID],
                        OBJET_ASSOCIE_LABEL => $actualite[ACTUALITE_TITRE]
                    )
                ), 
                $this->getDatabaseConnection()
            );
        }

        $this->setSuccess(true);
    }
}

?>