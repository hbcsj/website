<?php
require_once("core/php/controllers/abstract-uploader-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("common/php/dao/evenement-dao.php");
require_once("common/php/managers/evenement-manager.php");

class MatchFDMUploaderCtrl extends AbstractUploaderCtrl {

    private $evenementDAO;

    public function __construct($pageName) {
		parent::__construct($pageName, array(
            "idEvenement" => $_GET["idEvenement"],
            "fdm-file" => $_FILES["fdm-file"]
        ), true);
		
		if (isAdminConnected_coach()) {
            $this->evenementDAO = new EvenementDAO($this->getDatabaseConnection());
            
            $this->doPost();
        } else {
			$this->setSuccess(false);
            $this->sendCheckError(
                HTTP_401, 
                "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cet uploader"
            );
		}
	}
    
    public function doPost() {
        $getIdEvenement = $_GET["idEvenement"];

        $evenement = $this->evenementDAO->getById($getIdEvenement);
        if ($evenement == null) {
            $this->setSuccess(false);
            $this->sendCheckError(HTTP_404, "L'evenement (idEvenement = '".$getIdEvenement."') n'existe pas");
        }

        $fdmFile = PathUtils::getFDMMatchFile($getIdEvenement);
        $actionType = ACTION_TYPE_CREATION;
        if (file_exists($fdmFile)) {
            $actionType = ACTION_TYPE_MODIFICATION;
            unlink($fdmFile);
        }
        move_uploaded_file($_FILES["fdm-file"]["tmp_name"], $fdmFile);

        if (!isAdminConnected_superadmin()) {
            MailUtils::notifySuperadmin(
                EVENEMENT_TABLE_NAME, 
                $actionType, 
                array(
                    array(
                        OBJET_ASSOCIE_TYPE => EVENEMENT_TABLE_NAME,
                        OBJET_ASSOCIE_ID => $evenement[EVENEMENT_ID],
                        OBJET_ASSOCIE_LABEL => MATCH_FDM
                    )
                ), 
                $this->getDatabaseConnection()
            );
        }

        $this->setSuccess(true);
    }
}

?>