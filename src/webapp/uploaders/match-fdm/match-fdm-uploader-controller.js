MatchFDMUploaderController = {};

MatchFDMUploaderController.control_uploadFDMMatch = function() {
    window.top.ViewUtils.activeButtonAndHideLoader('#modal-fdm-match__bt-sauvegarder', '#modal-fdm-match__bt-sauvegarder .loader');
    if ($('#success').text() == 'true') {
        window.top.FormUtils.displayFormResultMessage('#modal-fdm-match .form-result-message', 'Feuille de match sauvegard&eacute;e', FormUtils.resultsTypes.ok);
        window.top.AdminEvenementsEvenementsController.load_TableEvenements();
        window.top.$('#modal-fdm-match__bt-sauvegarder').prop('disabled', true);
    } else {
        window.top.ViewUtils.activeButtonAndHideLoader('#modal-fdm-match__bt-sauvegarder', '#modal-fdm-match__bt-sauvegarder .loader');
        window.top.FormUtils.displayFormResultMessage('#modal-fdm-match .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
    }
};

initializeUploader = function() {
    MatchFDMUploaderController.control_uploadFDMMatch();
};