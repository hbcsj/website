HHSJUploaderController = {};

HHSJUploaderController.control_uploadHHSJ = function() {
    window.top.ViewUtils.activeButtonAndHideLoader('#modal-save-hhsj__bt-sauvegarder', '#modal-save-hhsj__bt-sauvegarder .loader');
    if ($('#success').text() == 'true') {
        window.top.FormUtils.displayFormResultMessage('#modal-save-hhsj .form-result-message', 'Hand-Hebdo sauvegard&eacute;', FormUtils.resultsTypes.ok);
        window.top.AdminEditionHHSJController.load_StatsHHSJ();
        window.top.AdminEditionHHSJController.load_TableHHSJ();
        window.top.$('#modal-save-hhsj__bt-sauvegarder').prop('disabled', true);
    } else {
        window.top.ViewUtils.activeButtonAndHideLoader('#modal-save-hhsj__bt-sauvegarder', '#modal-save-hhsj__bt-sauvegarder .loader');
        window.top.FormUtils.displayFormResultMessage('#modal-save-hhsj .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
    }
};

initializeUploader = function() {
    HHSJUploaderController.control_uploadHHSJ();
};