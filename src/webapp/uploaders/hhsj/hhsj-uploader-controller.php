<?php
require_once("core/php/controllers/abstract-uploader-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("common/php/dao/hhsj-dao.php");
require_once("common/php/dao/type-hhsj-dao.php");

class HHSJUploaderCtrl extends AbstractUploaderCtrl {

    private $hhsjDAO;
    private $typeHHSJDAO;

    public function __construct($pageName) {
		parent::__construct($pageName, array(
            "id" => $_GET["id"],
            "titre" => $_POST["titre"],
            "email-dernier-editeur" => $_POST["email-dernier-editeur"],
            "visible-sur-site" => $_POST["visible-sur-site"],
            "type-hhsj-id" => $_POST["type-hhsj-id"],
            "hhsj-image" => $_FILES["hhsj-image"],
            "hhsj-image-medium" => $_FILES["hhsj-image-medium"],
            "hhsj-image-small" => $_FILES["hhsj-image-small"],
            "hhsj-image-home" => $_FILES["hhsj-image-home"],
            "hhsj-fichier" => $_FILES["hhsj-fichier"]
        ), true);
		
		if (isAdminConnected_editeur()) {
            $this->hhsjDAO = new HHSJDAO($this->getDatabaseConnection());
            $this->typeHHSJDAO = new TypeHHSJDAO($this->getDatabaseConnection());
            
            if ($this->checkRequest()) {
                $this->doPost();
            }
        } else {
			$this->setSuccess(false);
            $this->sendCheckError(
                HTTP_401, 
                "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cet uploader"
            );
		}
	}

	private function checkRequest() {
		$checkParams = $this->checkParams(array(
            array(POST, "titre"),
            array(POST, "email-dernier-editeur"),
            array(POST, "type-hhsj-id")
        ));
		if (!$checkParams) {
			$this->setSuccess(false);
            $this->sendCheckError(HTTP_400);
        }
        return $checkParams;
    }
    
    public function doPost() {
        $getId = $_GET["id"];
        $postTitre = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["titre"]));
        $postVisibleSurSite = ($_POST["visible-sur-site"] == "on") ? "1" : "0";
        $postEmailDernierEditeur = trim($_POST["email-dernier-editeur"]);
        $postTypeHHSJId = trim($_POST["type-hhsj-id"]);

        $typeHHSJ = $this->typeHHSJDAO->getById($postTypeHHSJId);
        if ($typeHHSJ == null) {
            $this->setSuccess(false);
            $this->sendCheckError(HTTP_404, "Le type (idTypeHHSJ = '".$postTypeHHSJId."') n'existe pas");
        }

        $hhsj = array();
        if (HTTPUtils::paramExists(GET, "id")) {
            $hhsjById = $this->hhsjDAO->getById($getId);
            if ($hhsjById != null) {
                $hhsj = $hhsjById;
            } else {
                $this->setSuccess(false);
                $this->sendCheckError(HTTP_404, "L'article de presse (idHHSJ = '".$getId."') n'existe pas");
            }
        }
        $hhsj[HHSJ_TITRE] = $postTitre;
        $hhsj[HHSJ_VISIBLE_SUR_SITE] = $postVisibleSurSite;
        $hhsj[HHSJ_EMAIL_DERNIER_EDITEUR] = $postEmailDernierEditeur;
        $hhsj[HHSJ_TYPE_HHSJ_ID] = $postTypeHHSJId;

        $actionType = ACTION_TYPE_CREATION;
        if (array_key_exists(HHSJ_ID, $hhsj)) {
            $actionType = ACTION_TYPE_MODIFICATION;
            $this->hhsjDAO->update($hhsj);
            
            if ($_FILES["hhsj-image"]["name"] != null && $_FILES["hhsj-image"]["name"] != "") {
                $hhsjImgFile = PathUtils::getHHSJImgFile($hhsj[HHSJ_ID]);
                if (file_exists($hhsjImgFile)) {
                    unlink($hhsjImgFile);
                }
                move_uploaded_file($_FILES["hhsj-image"]["tmp_name"], $hhsjImgFile);
            }
            
            if ($_FILES["hhsj-image-medium"]["name"] != null && $_FILES["hhsj-image-medium"]["name"] != "") {
                $hhsjImgMediumFile = PathUtils::getHHSJImgMediumFile($hhsj[HHSJ_ID]);
                if (file_exists($hhsjImgMediumFile)) {
                    unlink($hhsjImgMediumFile);
                }
                move_uploaded_file($_FILES["hhsj-image-medium"]["tmp_name"], $hhsjImgMediumFile);
            }
            
            if ($_FILES["hhsj-image-small"]["name"] != null && $_FILES["hhsj-image-small"]["name"] != "") {
                $hhsjImgSmallFile = PathUtils::getHHSJImgSmallFile($hhsj[HHSJ_ID]);
                if (file_exists($hhsjImgSmallFile)) {
                    unlink($hhsjImgSmallFile);
                }
                move_uploaded_file($_FILES["hhsj-image-small"]["tmp_name"], $hhsjImgSmallFile);
            }

            if ($_FILES["hhsj-image-home"]["name"] != null && $_FILES["hhsj-image-home"]["name"] != "") {
                $hhsjImgHomeFile = PathUtils::getHHSJImgHomeFile($hhsj[HHSJ_ID]);
                if (file_exists($hhsjImgHomeFile)) {
                    unlink($hhsjImgHomeFile);
                }
                move_uploaded_file($_FILES["hhsj-image-home"]["tmp_name"], $hhsjImgHomeFile);
            }

            if ($_FILES["hhsj-fichier"]["name"] != null && $_FILES["hhsj-fichier"]["name"] != "") {
                $hhsjFile = PathUtils::getHHSJFile($hhsj[HHSJ_ID]);
                if (file_exists($hhsjFile)) {
                    unlink($hhsjFile);
                }
                move_uploaded_file($_FILES["hhsj-fichier"]["tmp_name"], $hhsjFile);
            }
        } else {
            $hhsj[HHSJ_DATE] = date(SQL_DATE_FORMAT);
            $hhsj[HHSJ_ID] = $this->hhsjDAO->create($hhsj);
            move_uploaded_file($_FILES["hhsj-image"]["tmp_name"], PathUtils::getHHSJImgFile($hhsj[HHSJ_ID]));
            move_uploaded_file($_FILES["hhsj-image-medium"]["tmp_name"], PathUtils::getHHSJImgMediumFile($hhsj[HHSJ_ID]));
            move_uploaded_file($_FILES["hhsj-image-small"]["tmp_name"], PathUtils::getHHSJImgSmallFile($hhsj[HHSJ_ID]));
            move_uploaded_file($_FILES["hhsj-image-home"]["tmp_name"], PathUtils::getHHSJImgHomeFile($hhsj[HHSJ_ID]));
            move_uploaded_file($_FILES["hhsj-fichier"]["tmp_name"], PathUtils::getHHSJFile($hhsj[HHSJ_ID]));
        }

        if (!isAdminConnected_superadmin()) {
            MailUtils::notifySuperadmin(
                HHSJ_TABLE_NAME, 
                $actionType, 
                array(
                    array(
                        OBJET_ASSOCIE_TYPE => HHSJ_TABLE_NAME,
                        OBJET_ASSOCIE_ID => $hhsj[HHSJ_ID],
                        OBJET_ASSOCIE_LABEL => $hhsj[HHSJ_TITRE]
                    )
                ), 
                $this->getDatabaseConnection()
            );
        }

        $this->setSuccess(true);
    }
}

?>