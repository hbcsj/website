ProduitBoutiqueUploaderController = {};

ProduitBoutiqueUploaderController.control_uploadProduit = function() {
    window.top.ViewUtils.activeButtonAndHideLoader('#modal-save-produit-boutique__bt-sauvegarder', '#modal-save-produit-boutique__bt-sauvegarder .loader');
    if ($('#success').text() == 'true') {
        window.top.FormUtils.displayFormResultMessage('#modal-save-produit-boutique .form-result-message', 'Produit sauvegard&eacute;', FormUtils.resultsTypes.ok);
        window.top.AdminShopBoutiqueProduitsController.load_TableProduits();
        window.top.$('#modal-save-produit-boutique__bt-sauvegarder').prop('disabled', true);
    } else {
        window.top.ViewUtils.activeButtonAndHideLoader('#modal-save-produit-boutique__bt-sauvegarder', '#modal-save-produit-boutique__bt-sauvegarder .loader');
        window.top.FormUtils.displayFormResultMessage('#modal-save-produit-boutique .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
    }
};

initializeUploader = function() {
    ProduitBoutiqueUploaderController.control_uploadProduit();
};