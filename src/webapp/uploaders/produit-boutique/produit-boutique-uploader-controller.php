<?php
require_once("core/php/controllers/abstract-uploader-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/mail-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("common/php/dao/produit-boutique-dao.php");
require_once("common/php/dao/categorie-produit-boutique-dao.php");
require_once("common/php/dao/marque-produit-boutique-dao.php");
require_once("common/php/dao/tailles-possibles-produit-boutique-dao.php");
require_once("common/php/dao/fournisseur-produit-boutique-dao.php");

class ProduitBoutiqueUploaderCtrl extends AbstractUploaderCtrl {

    private $produitBoutiqueDAO;
    private $categorieProduitBoutiqueDAO;
    private $marqueProduitBoutiqueDAO;
    private $taillesPossiblesProduitBoutiqueDAO;
    private $fournisseurProduitBoutiqueDAO;

    public function __construct($pageName) {
		parent::__construct($pageName, array(
            "id" => $_GET["id"],
            "nom" => $_POST["nom"],
            "indication" => $_POST["indication"],
            "couleurs" => $_POST["couleurs"],
            "references" => $_POST["references"],
            "description" => $_POST["description"],
            "flocage-logo-inclus" => $_POST["flocage-logo-inclus"],
            "flocable-nom" => $_POST["flocable-nom"],
            "prix-flocage-nom" => $_POST["prix-flocage-nom"],
            "prix-flocage-fournisseur-nom" => $_POST["prix-flocage-fournisseur-nom"],
            "flocable-numero-arriere" => $_POST["flocable-numero-arriere"],
            "prix-flocage-numero-arriere" => $_POST["prix-flocage-numero-arriere"],
            "prix-flocage-fournisseur-numero-arriere" => $_POST["prix-flocage-fournisseur-numero-arriere"],
            "flocable-numero-avant" => $_POST["flocable-numero-avant"],
            "prix-flocage-numero-avant" => $_POST["prix-flocage-numero-avant"],
            "prix-flocage-fournisseur-numero-avant" => $_POST["prix-flocage-fournisseur-numero-avant"],
            "flocable-initiales" => $_POST["flocable-initiales"],
            "prix-flocage-initiales" => $_POST["prix-flocage-initiales"],
            "prix-flocage-fournisseur-initiales" => $_POST["prix-flocage-fournisseur-initiales"],
            "en-vente" => $_POST["en-vente"],
            "categorie-produit-boutique-id" => $_POST["categorie-produit-boutique-id"],
            "fournisseur-produit-boutique-id" => $_POST["fournisseur-produit-boutique-id"],
            "marque-produit-boutique-id" => $_POST["marque-produit-boutique-id"],
            "tailles-possibles-produit-boutique-id" => $_POST["tailles-possibles-produit-boutique-id"],
            "img-file" => $_FILES["img-file"]
        ), true);
		
		if (isAdminConnected_commercial()) {
            $this->produitBoutiqueDAO = new ProduitBoutiqueDAO($this->getDatabaseConnection());
            $this->categorieProduitBoutiqueDAO = new CategorieProduitBoutiqueDAO($this->getDatabaseConnection());
            $this->marqueProduitBoutiqueDAO = new MarqueProduitBoutiqueDAO($this->getDatabaseConnection());
            $this->taillesPossiblesProduitBoutiqueDAO = new TaillesPossiblesProduitBoutiqueDAO($this->getDatabaseConnection());
            $this->fournisseurProduitBoutiqueDAO = new FournisseurProduitBoutiqueDAO($this->getDatabaseConnection());
            
            if ($this->checkRequest()) {
                $this->doPost();
            }
        } else {
			$this->setSuccess(false);
            $this->sendCheckError(
                HTTP_401, 
                "Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cet uploader"
            );
		}
	}

	private function checkRequest() {
		$checkParams = $this->checkParams(array(
            array(POST, "nom"),
            array(POST, "description"),
            array(POST, "categorie-produit-boutique-id"),
            array(POST, "fournisseur-produit-boutique-id"),
            array(POST, "tailles-possibles-produit-boutique-id")
        ));
		if (!$checkParams) {
			$this->setSuccess(false);
            $this->sendCheckError(HTTP_400);
        }
        return $checkParams;
	}
    
    public function doPost() {
        $getId = $_GET["id"];
        $postNom = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["nom"]));
        $postIndication = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["indication"]));
        $postCouleurs = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["couleurs"]));
        $postReferences = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["references"]));
        $postDescription = StringUtils::convertAccentsToHtmlSpecialChars(trim($_POST["description"]));
        $postFlocageLogoInclus = (HTTPUtils::paramExists(POST, "flocage-logo-inclus") && $_POST["flocage-logo-inclus"] != "") ? 1 : 0;
        $postFlocableNom = (HTTPUtils::paramExists(POST, "flocable-nom") && $_POST["flocable-nom"] != "") ? 1 : 0;
        $postPrixFlocageNom = StringUtils::convertEmptyStringToNull(trim($_POST["prix-flocage-nom"]));
        $postPrixFlocageFournisseurNom = StringUtils::convertEmptyStringToNull(trim($_POST["prix-flocage-fournisseur-nom"]));
        $postFlocableNumeroArriere = (HTTPUtils::paramExists(POST, "flocable-numero-arriere") && $_POST["flocable-numero-arriere"] != "") ? 1 : 0;
        $postPrixFlocageNumeroArriere = StringUtils::convertEmptyStringToNull(trim($_POST["prix-flocage-numero-arriere"]));
        $postPrixFlocageFournisseurNumeroArriere = StringUtils::convertEmptyStringToNull(trim($_POST["prix-flocage-fournisseur-numero-arriere"]));
        $postFlocableNumeroAvant = (HTTPUtils::paramExists(POST, "flocable-numero-avant") && $_POST["flocable-numero-avant"] != "") ? 1 : 0;
        $postPrixFlocageNumeroAvant = StringUtils::convertEmptyStringToNull(trim($_POST["prix-flocage-numero-avant"]));
        $postPrixFlocageFournisseurNumeroAvant = StringUtils::convertEmptyStringToNull(trim($_POST["prix-flocage-fournisseur-numero-avant"]));
        $postFlocableInitiales = (HTTPUtils::paramExists(POST, "flocable-initiales") && $_POST["flocable-initiales"] != "") ? 1 : 0;
        $postPrixFlocageInitiales = StringUtils::convertEmptyStringToNull(trim($_POST["prix-flocage-initiales"]));
        $postPrixFlocageFournisseurInitiales = StringUtils::convertEmptyStringToNull(trim($_POST["prix-flocage-fournisseur-initiales"]));
        $postEnVente = (HTTPUtils::paramExists(POST, "en-vente") && $_POST["en-vente"] != "") ? 1 : 0;
        $postPosition = trim($_POST["position"]);
        $postCategorieProduitBoutiqueId = trim($_POST["categorie-produit-boutique-id"]);
        $postFournisseurProduitBoutiqueId = trim($_POST["fournisseur-produit-boutique-id"]);
        $postMarqueProduitBoutiqueId = StringUtils::convertEmptyStringToNull(trim($_POST["marque-produit-boutique-id"]));
        $postTaillesPossiblesProduitBoutiqueId = trim($_POST["tailles-possibles-produit-boutique-id"]);

        if ($this->categorieProduitBoutiqueDAO->getById($postCategorieProduitBoutiqueId) == null) {
            $this->setSuccess(false);
            $this->sendCheckError(HTTP_404, "La categorie produit boutique (idCategorieProduitBoutique = '".$postCategorieProduitBoutiqueId."') n'existe pas");
        }

        if ($this->fournisseurProduitBoutiqueDAO->getById($postFournisseurProduitBoutiqueId) == null) {
            $this->setSuccess(false);
            $this->sendCheckError(HTTP_404, "Le fournisseur produit boutique (idFournisseurProduitBoutique = '".$postFournisseurProduitBoutiqueId."') n'existe pas");
        }

        if ($postMarqueProduitBoutiqueId != null && $this->marqueProduitBoutiqueDAO->getById($postMarqueProduitBoutiqueId) == null) {
            $this->setSuccess(false);
            $this->sendCheckError(HTTP_404, "La marque produit boutique (idMarqueBoutique = '".$postMarqueProduitBoutiqueId."') n'existe pas");
        }

        if ($this->taillesPossiblesProduitBoutiqueDAO->getById($postTaillesPossiblesProduitBoutiqueId) == null) {
            $this->setSuccess(false);
            $this->sendCheckError(HTTP_404, "Les tailles possibles produit boutique (idTaillesPossiblesProduitBoutique = '".$postTaillesPossiblesProduitBoutiqueId."') n'existe pas");
        }

        $produitBoutique = array();
        if (HTTPUtils::paramExists(GET, "id")) {
            $produitBoutiqueById = $this->produitBoutiqueDAO->getById($getId);
            if ($produitBoutiqueById != null) {
                $produitBoutique = $produitBoutiqueById;
            } else {
                $this->setSuccess(false);
                $this->sendCheckError(HTTP_404, "Le produit boutique (idProduitBoutique = '".$getId."') n'existe pas");
            }
        }
        $produitBoutique[PRODUIT_BOUTIQUE_NOM] = $postNom;
        $produitBoutique[PRODUIT_BOUTIQUE_INDICATION] =  $postIndication;
        $produitBoutique[PRODUIT_BOUTIQUE_REFERENCES] = $postReferences;
        $produitBoutique[PRODUIT_BOUTIQUE_COULEURS] = $postCouleurs;
        $produitBoutique[PRODUIT_BOUTIQUE_FLOCAGE_LOGO_INCLUS] = $postFlocageLogoInclus;
        $produitBoutique[PRODUIT_BOUTIQUE_FLOCABLE_NOM] = $postFlocableNom;
        $produitBoutique[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_NOM] = $postPrixFlocageNom;
        $produitBoutique[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_FOURNISSEUR_NOM] = $postPrixFlocageFournisseurNom;
        $produitBoutique[PRODUIT_BOUTIQUE_FLOCABLE_NUMERO_ARRIERE] = $postFlocableNumeroArriere;
        $produitBoutique[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_NUMERO_ARRIERE] = $postPrixFlocageNumeroArriere;
        $produitBoutique[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_FOURNISSEUR_NUMERO_ARRIERE] = $postPrixFlocageFournisseurNumeroArriere;
        $produitBoutique[PRODUIT_BOUTIQUE_FLOCABLE_NUMERO_AVANT] = $postFlocableNumeroAvant;
        $produitBoutique[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_NUMERO_AVANT] = $postPrixFlocageNumeroAvant;
        $produitBoutique[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_FOURNISSEUR_NUMERO_AVANT] = $postFlocableNumeroAvant;
        $produitBoutique[PRODUIT_BOUTIQUE_FLOCABLE_INITIALES] = $postFlocableInitiales;
        $produitBoutique[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_INITIALES] = $postPrixFlocageInitiales;
        $produitBoutique[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_FOURNISSEUR_INITIALES] = $postPrixFlocageFournisseurInitiales;
        $produitBoutique[PRODUIT_BOUTIQUE_EN_VENTE] = $postEnVente;
        $produitBoutique[PRODUIT_BOUTIQUE_POSITION_DANS_CATEGORIE] = $postPosition;
        $produitBoutique[PRODUIT_BOUTIQUE_CATEGORIE_PRODUIT_BOUTIQUE_ID] = $postCategorieProduitBoutiqueId;
        $produitBoutique[PRODUIT_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID] = $postFournisseurProduitBoutiqueId;
        $produitBoutique[PRODUIT_BOUTIQUE_MARQUE_PRODUIT_BOUTIQUE_ID] = $postMarqueProduitBoutiqueId;
        $produitBoutique[PRODUIT_BOUTIQUE_TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_ID] = $postTaillesPossiblesProduitBoutiqueId;

        $actionType = ACTION_TYPE_CREATION;
        if (array_key_exists(PRODUIT_BOUTIQUE_ID, $produitBoutique)) {
            $actionType = ACTION_TYPE_MODIFICATION;
            $this->produitBoutiqueDAO->update($produitBoutique);
            if ($_FILES["img-file"]["tmp_name"] != null && $_FILES["img-file"]["tmp_name"] != "") {
                $imgProduitBoutiqueFile = PathUtils::getProduitBoutiqueImgFile($produitBoutique[PRODUIT_BOUTIQUE_ID]);
                if (file_exists($imgProduitBoutiqueFile)) {
                    unlink($imgProduitBoutiqueFile);
                }
                move_uploaded_file($_FILES["img-file"]["tmp_name"], $imgProduitBoutiqueFile);
            }
        } else {
            $produitBoutique[PRODUIT_BOUTIQUE_ID] = $this->produitBoutiqueDAO->create($produitBoutique);
            move_uploaded_file($_FILES["img-file"]["tmp_name"], PathUtils::getProduitBoutiqueImgFile($produitBoutique[PRODUIT_BOUTIQUE_ID]));
        }

        $descriptionProduitBoutiqueFile = PathUtils::getProduitBoutiqueDescriptionFile($produitBoutique[PRODUIT_BOUTIQUE_ID]);
        if (file_exists($descriptionProduitBoutiqueFile)) {
            unlink($descriptionProduitBoutiqueFile);
        }
        $fp = fopen($descriptionProduitBoutiqueFile, "w");
        fputs($fp, $postDescription);
        fclose($fp);

        if (!isAdminConnected_superadmin()) {
            MailUtils::notifySuperadmin(
                PRODUIT_BOUTIQUE_TABLE_NAME, 
                $actionType, 
                array(
                    array(
                        OBJET_ASSOCIE_TYPE => PRODUIT_BOUTIQUE_TABLE_NAME,
                        OBJET_ASSOCIE_ID => $produitBoutique[PRODUIT_BOUTIQUE_ID],
                        OBJET_ASSOCIE_LABEL => $produitBoutique[PRODUIT_BOUTIQUE_NOM]." - ".$produitBoutique[PRODUIT_BOUTIQUE_INDICATION]
                    )
                ), 
                $this->getDatabaseConnection()
            );
        }

        $this->setSuccess(true);
    }
}

?>