<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/infos-club-dao.php");

class HeaderCtrl extends AbstractViewCtrl {

	private $infosClubDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		$this->infosClubDAO = new InfosClubDAO($this->getDatabaseConnection());
	}

    public function getNomClub() {
        return $this->infosClubDAO->getSingleton()[INFOS_CLUB_NOM];
    }
}

?>