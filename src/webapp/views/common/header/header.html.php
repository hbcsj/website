<?php
require_once("core/php/resources/view.php");

$view = new View(
    "webapp/views/common/header/", 
    "header", 
    "HeaderCtrl"
);
$headerCtrl = $view->getController();
?>
<header>
    <div id="header-logo"></div>
    <div id="header-nom-club"><?php echo $headerCtrl->getNomClub(); ?></div>
</header>
