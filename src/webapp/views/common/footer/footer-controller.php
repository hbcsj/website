<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/infos-club-dao.php");

class FooterCtrl extends AbstractViewCtrl {

	private $infosClubDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		$this->infosClubDAO = new InfosClubDAO($this->getDatabaseConnection());
	}

    public function getInfosClub() {
        return $this->infosClubDAO->getSingleton();
    }
}

?>