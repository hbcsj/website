<?php
require_once("core/php/resources/view.php");

$view = new View(
    "webapp/views/common/footer/", 
    "footer", 
    "FooterCtrl"
);
$footerCtrl = $view->getController();

$infosClub = $footerCtrl->getInfosClub();
?>
<footer>
    <div class="row">
        <div class="col-xs-4">
            <b>Contact</b> : <a href="mailto:<?php echo $infosClub[INFOS_CLUB_EMAIL]; ?>"><?php echo $infosClub[INFOS_CLUB_EMAIL]; ?></a>
        </div>
        <div class="col-xs-4 text-center">
            <img src="<?php echo SRC_PATH; ?>assets/img/icons/icon-responsive.png" title="Adapt&eacute; pour ordinateurs, tablettes et smartphones">
        </div>
        <div class="col-xs-4 text-right">
            <?php echo $infosClub[INFOS_CLUB_ADRESSE].", ".$infosClub[INFOS_CLUB_CODE_POSTAL]." ".$infosClub[INFOS_CLUB_VILLE]; ?>
        </div>
    </div>
</footer>
