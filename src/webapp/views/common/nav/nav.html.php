<nav>
    <div id="nav-logo">
        <img src="<?php echo SRC_PATH; ?>assets/img/logo.png">
    </div>
    <div class="nav-links">
        <a href="<?php echo ROOT_PATH; ?>./" page="home">Accueil</a>
        <a href="<?php echo ROOT_PATH; ?>news" page="news">Actus</a>
        <a href="<?php echo ROOT_PATH; ?>club" page="club">Le club</a>
        <a href="<?php echo ROOT_PATH; ?>entrainements" page="entrainements">S'entra&icirc;ner</a>
        <a href="<?php echo ROOT_PATH; ?>inscription" page="inscription">S'inscrire</a>
        <a href="<?php echo ROOT_PATH; ?>agenda?date=<?php echo date("Ym"); ?>" page="agenda">Agenda</a>
        <a href="<?php echo ROOT_PATH; ?>medias" page="medias">M&eacute;dias</a>
        <a href="<?php echo ROOT_PATH; ?>shop" target="_blank">Shop</a>
    </div>
    <div class="loader" id="loader-page"></div>
</nav>
