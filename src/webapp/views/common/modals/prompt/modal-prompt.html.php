<div class="modal fade" id="modal-prompt">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" id="modal-prompt__content">
            <div class="modal-header"></div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-3 text-center" id="modal-prompt__icon">
                        <span class="glyphicon glyphicon-question-sign"></span>
                    </div>
                    <div class="col-xs-9 text-center" id="modal-prompt__message"></div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-6 text-center">
                        <button id="modal-prompt__bt-ok" class="btn btn-default">OK</button>
                    </div>
                    <div class="col-xs-6 text-center">
                        <button id="modal-prompt__bt-ko" class="btn btn-default">Annuler</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>