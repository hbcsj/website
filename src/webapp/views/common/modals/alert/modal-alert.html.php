<div class="modal fade" id="modal-alert">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" id="modal-alert__content">
            <div class="modal-header"></div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-3 text-center" id="modal-alert__icon">
                        <span class="glyphicon"></span>
                    </div>
                    <div class="col-xs-9 text-center" id="modal-alert__message"></div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>