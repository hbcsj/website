<div id="mdp-form" class="form-inline">
    <div class="form-group">
        <label for="mdp-form__input-password">Mot de passe :</label>
        <input type="password" class="form-control" id="mdp-form__input-password">
        <button id="mdp-form__bt-connexion" class="btn btn-default">
            <div class="button__text">Connexion</div>
            <div class="loader"></div>
        </button>
    </div>
    <div class="row">
        <div class="col-xs-12 animated fadeIn" id="mdp-form__error"></div>
    </div>
</div>