<?php
require_once("core/php/resources/page.php");
require_once("common/php/constants/data-constants.php");

$page = new Page(
    "webapp/views/club/documents/", 
    "club-documents", 
    "ClubDocumentsCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/common/header/header.html.php");
require_once("webapp/views/common/nav/nav.html.php");
require_once("webapp/views/club/sub-menu/sub-menu-club.html.php");

$documents = $ctrl->getDocuments();
?>

<div class="container-fluid animated fadeIn">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
            <h1>Documents</h1>
            <div class="row">
                <div class="col-xs-10 col-lg-8 col-xs-offset-1 col-lg-offset-2">
                    <?php
                    if (sizeof($documents) > 0) {
                        ?>
                        <table>
                            <tbody>
                                <?php
                                foreach ($documents as $document) {
                                    ?>
                                    <tr>
                                        <td><?php echo $document[DOCUMENT_NOM]; ?></td>
                                        <td class="text-center">
                                            <a href="<?php echo SRC_PATH.DOCS_FOLDER."/".$document[DOCUMENT_FICHIER]; ?>" target="_blank">T&eacute;l&eacute;charger</a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
require_once("webapp/views/common/footer/footer.html.php");

require_once("webapp/views/common/modals/alert/modal-alert.html.php");
?>

<?php
$page->finalizePage();
?>