<?php
require_once("core/php/controllers/abstract-page-controller.php");
require_once("common/php/dao/document-dao.php");

class ClubDocumentsCtrl extends AbstractPageCtrl {

	private $documentDAO;
	
    public function __construct($pageName) {
		parent::__construct($pageName, null, true);

        $this->documentDAO = new DocumentDAO($this->getDatabaseConnection());
	}

	public function getDocuments() {
		return $this->documentDAO->getVisiblesSurSite(DOCUMENT_TABLE_NAME.".".DOCUMENT_NOM);
	}
}

?>