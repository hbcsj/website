<?php
require_once("core/php/controllers/abstract-page-controller.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/dao/categorie-dao.php");
require_once("common/php/dao/competition-dao.php");
require_once("common/php/dao/evenement-dao.php");
require_once("common/php/managers/categorie-manager.php");
require_once("common/php/managers/evenement-manager.php");

class ClubEquipesEquipeCtrl extends AbstractPageCtrl {

    private $categorie;

	private $categorieDAO;
	private $competitionDAO;
	private $evenementDAO;

	private $categorieManager;
	private $evenementManager;

    public function __construct($pageName) {
		parent::__construct($pageName, array(
            "id" => $_GET["id"]
		), true);

		$this->categorieDAO = new CategorieDAO($this->getDatabaseConnection());
		$this->competitionDAO = new CompetitionDAO($this->getDatabaseConnection());
		$this->evenementDAO = new EvenementDAO($this->getDatabaseConnection());
		
    	$this->categorieManager = new CategorieManager($this->getDatabaseConnection());
    	$this->evenementManager = new EvenementManager($this->getDatabaseConnection());

        $this->checkRequest();
	}
    
    private function checkRequest() {
		$checkParams = $this->checkParams(array(
			array(GET, "id")
		));
		if ($checkParams) {
			$this->checkIdCategorie();
		}
	}

	private function checkIdCategorie() {
		$this->categorie = $this->categorieDAO->getById($_GET["id"]);
			
		if ($this->categorie == null) {
			$this->sendCheckError(
				HTTP_404, 
				"La categorie (idCategorie = '".$_GET["id"]."') n'existe pas", 
				"404"
			);
		}
    }
    
    public function getCategorie() {
        return $this->categorie;
    }

    public function getCoachsCategorie() {
        return $this->categorieManager->getCoachsCategorie($this->categorie[CATEGORIE_ID]);
    }

    public function getCompetitionsCategorie() {
        return $this->competitionDAO->getVisiblesEnCoursByCategorie($this->categorie[CATEGORIE_ID], COMPETITION_TABLE_NAME.".".COMPETITION_NUM_EQUIPE);
    }

    public function getDernieresRencontres() {
        return $this->evenementDAO->getAllByParams(
			null, 
			date(SQL_DATE_TIME_FORMAT), 
			$this->categorie[CATEGORIE_ID], 
			null, 
			EVENEMENT_MATCH_TYPE_ID, 
			null, 
			null, 
			null, 
			null, 
			true, 
			EVENEMENT_TABLE_NAME.".".EVENEMENT_DATE_HEURE." DESC",
			$GLOBALS[CONF][NB_MAX_RENCONTRES_A_AFFICHER_CATEGORIE]
		);
    }

    public function getProchainesRencontres() {
        return $this->evenementDAO->getAllByParams(
			date(SQL_DATE_TIME_FORMAT), 
			null, 
			$this->categorie[CATEGORIE_ID], 
			null, 
			EVENEMENT_MATCH_TYPE_ID, 
			null, 
			null, 
			null, 
			null, 
			true, 
			EVENEMENT_TABLE_NAME.".".EVENEMENT_DATE_HEURE,
			$GLOBALS[CONF][NB_MAX_RENCONTRES_A_AFFICHER_CATEGORIE]
		);
    }

	public function getRecapMatchProps($evenement) {
	  return $this->evenementManager->getRecapMatchProps($evenement);
	}
}

?>