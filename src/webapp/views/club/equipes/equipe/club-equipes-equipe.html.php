<?php
require_once("core/php/resources/page.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/lib/path-utils.php");
require_once("lib/php/mobile-detect.php");

$page = new Page(
    "webapp/views/club/equipes/equipe/", 
    "club-equipes-equipe", 
    "ClubEquipesEquipeCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/common/header/header.html.php");
require_once("webapp/views/common/nav/nav.html.php");
require_once("webapp/views/club/sub-menu/sub-menu-club.html.php");

$mobileDetect = new Mobile_Detect();
$categorie = $ctrl->getCategorie();
$coachs = $ctrl->getCoachsCategorie();
$competitions = $ctrl->getCompetitionsCategorie();
$dernieresRencontres = $ctrl->getDernieresRencontres();
$prochainesRencontres = $ctrl->getProchainesRencontres();
?>

<div class="container-fluid animated fadeIn">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
            <h1><?php echo $categorie[CATEGORIE_NOM]; ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
            <div class="row row-equipe<?php echo $classBordered; ?>">
                <div class="col-xs-4">
                    <img src="<?php echo SRC_PATH.PathUtils::getPhotoCategorie($categorie[CATEGORIE_ABREVIATION])."?".time(); ?>" img-modal="<?php echo SRC_PATH.PathUtils::getPhotoCategorie($categorie[CATEGORIE_ABREVIATION]); ?>" class="img-equipe">
                </div>
                <div class="col-xs-7 col-xs-offset-1">
                    <div class="coachs-equipe">
                        <b>Coach(s) / responsable(s) : </b>
                        <?php
                        if (sizeof($coachs) > 0) {
                            $i = 0;
                            foreach ($coachs as $coach) {
                                if ($coach[COACH_EN_SOUTIEN]) {
                                    echo "(";
                                }
                                echo $coach[LICENCIE_PRENOM]." ".$coach[LICENCIE_NOM];
                                if ($coach[COACH_EN_SOUTIEN]) {
                                    echo ")";
                                }

                                if ($i < (sizeof($coachs) - 1)) {
                                    echo ", ";
                                }
                                $i++;
                            }
                        }
                        ?>
                    </div>
                    <?php
                    if (sizeof($competitions) > 0) {
                        ?>
                        <ul class="compet-equipe">
                            <b>Comp&eacute;tition(s) : </b>
                            <?php
                            foreach ($competitions as $competition) {
                                ?>
                                <li>
                                    <?php 
                                    if ($competition[NUM_EQUIPE] != null) {
                                        echo "Equipe ".$competition[NUM_EQUIPE]." : ";
                                    }
                                    if ($competition[COMPETITION_URL_SITE_FFHB] != "") {
                                        echo "<a href=\"".$competition[COMPETITION_URL_SITE_FFHB]."\" target=\"_blank\">".$competition[COMPETITION_NOM]."</a>"; 
                                    } else {
                                        echo $competition[COMPETITION_NOM]; 
                                    }
                                    ?>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php
    if ($mobileDetect->isMobile() || $mobileDetect->isTablet()) {
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h2>Derniers r&eacute;sultats</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <?php
                if (sizeof($dernieresRencontres) > 0) {
                    ?>
                    <table class="table-evenements">
                        <tbody>
                            <?php
                            foreach ($dernieresRencontres as $derniereRencontre) {
                                $propsDerniereRencontre = $ctrl->getRecapMatchProps($derniereRencontre);
                                ?>
                                <tr>
                                    <td class="td-go-to-competition" competition-href="<?php echo $propsDerniereRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]; ?>">
                                        <?php echo substr($propsDerniereRencontre[EVENEMENT_JOUR], 0, 3).". ".str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDateTime_to_slashDateTime($derniereRencontre[EVENEMENT_DATE_HEURE])); ?>
                                    </td>
                                    <td class="td-go-to-competition" competition-href="<?php echo $propsDerniereRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]; ?>">
                                        <?php echo $propsDerniereRencontre[MATCH_EQUIPE_DOMICILE]." VS ".$propsDerniereRencontre[MATCH_EQUIPE_EXTERIEUR]; ?>
                                    </td>
                                    <td class="resultat-<?php echo $propsDerniereRencontre[MATCH_CLASS_RESULTAT]; ?> td-go-to-competition" competition-href="<?php echo $propsDerniereRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]; ?>">
                                        <?php
                                        if ($propsDerniereRencontre[MATCH_SCORE_DOMICILE] != "" || $propsDerniereRencontre[MATCH_SCORE_EXTERIEUR] != "") {
                                            echo $propsDerniereRencontre[MATCH_TEXT_RESULTAT];
                                        }
                                        ?>
                                    </td>
                                    <td class="td-go-to-competition">
                                        <?php
                                        if ($propsDerniereRencontre[MATCH_FDM] != null) {
                                            ?>
                                            <a class="link-with-img" href="<?php echo SRC_PATH.$propsDerniereRencontre[MATCH_FDM]; ?>" target="_blank" title="Afficher la feuille de match">
                                                <span class="glyphicon glyphicon-open-file"></span>
                                            </a>
                                            <?php
                                        } else {
                                            echo "&nbsp;";
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        ?>
                        </tbody>
                    </table>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h2>Prochaines rencontres</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <?php
                if (sizeof($prochainesRencontres) > 0) {
                    ?>
                    <table class="table-evenements">
                        <tbody>
                            <?php
                            foreach ($prochainesRencontres as $prochaineRencontre) {
                                $propsProchaineRencontre = $ctrl->getRecapMatchProps($prochaineRencontre);
                                ?>
                                <tr>
                                    <td class="td-go-to-competition" competition-href="<?php echo $propsProchaineRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]; ?>">
                                        <?php echo substr($propsProchaineRencontre[EVENEMENT_JOUR], 0, 3).". ".str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDateTime_to_slashDateTime($prochaineRencontre[EVENEMENT_DATE_HEURE])); ?>
                                    </td>
                                    <td class="td-go-to-competition" competition-href="<?php echo $propsProchaineRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]; ?>">
                                        <?php echo $propsProchaineRencontre[MATCH_EQUIPE_DOMICILE]." VS ".$propsProchaineRencontre[MATCH_EQUIPE_EXTERIEUR]; ?>
                                    </td>
                                    <td class="td-go-to-competition" competition-href="<?php echo $propsProchaineRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]; ?>">
                                        <?php 
                                        if ($propsProchaineRencontre[EVENEMENT_GOOGLE_MAP_URL] != "") {
                                            echo "<a href=\"".$propsProchaineRencontre[EVENEMENT_GOOGLE_MAP_URL]."\" target=\"blank\">".$prochaineRencontre[EVENEMENT_VILLE]."</a>"; 
                                        } else {
                                            echo "&nbsp;";
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        ?>
                        </tbody>
                    </table>
                <?php
                }
                ?>
            </div>
        </div>
        <?php
    } else {
        ?>
        <div class="row">
            <div class="col-xs-5 col-xs-offset-1">
                <h2>Derniers r&eacute;sultats</h2>
            </div>
            <div class="col-xs-5">
                <h2>Prochaines rencontres</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-5 col-xs-offset-1">
                <?php
                if (sizeof($dernieresRencontres) > 0) {
                    ?>
                    <table class="table-evenements">
                        <tbody>
                            <?php
                            foreach ($dernieresRencontres as $derniereRencontre) {
                                $propsDerniereRencontre = $ctrl->getRecapMatchProps($derniereRencontre);
                                ?>
                                <tr>
                                    <td class="td-go-to-competition" competition-href="<?php echo $propsDerniereRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]; ?>">
                                        <?php echo substr($propsDerniereRencontre[EVENEMENT_JOUR], 0, 3).". ".str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDateTime_to_slashDateTime($derniereRencontre[EVENEMENT_DATE_HEURE])); ?>
                                    </td>
                                    <td class="td-go-to-competition" competition-href="<?php echo $propsDerniereRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]; ?>">
                                        <?php echo $propsDerniereRencontre[MATCH_EQUIPE_DOMICILE]." VS ".$propsDerniereRencontre[MATCH_EQUIPE_EXTERIEUR]; ?>
                                    </td>
                                    <td class="resultat-<?php echo $propsDerniereRencontre[MATCH_CLASS_RESULTAT]; ?> td-go-to-competition" competition-href="<?php echo $propsDerniereRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]; ?>">
                                        <?php
                                        if ($propsDerniereRencontre[MATCH_SCORE_DOMICILE] != "" || $propsDerniereRencontre[MATCH_SCORE_EXTERIEUR] != "") {
                                            echo $propsDerniereRencontre[MATCH_TEXT_RESULTAT];
                                        }
                                        ?>
                                    </td>
                                    <td class="td-go-to-competition">
                                        <?php
                                        if ($propsDerniereRencontre[MATCH_FDM] != null) {
                                            ?>
                                            <a class="link-with-img" href="<?php echo SRC_PATH.$propsDerniereRencontre[MATCH_FDM]; ?>" target="_blank" title="Afficher la feuille de match">
                                                <span class="glyphicon glyphicon-open-file"></span>
                                            </a>
                                            <?php
                                        } else {
                                            echo "&nbsp;";
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        ?>
                        </tbody>
                    </table>
                <?php
                }
                ?>
            </div>
            <div class="col-xs-5">
                <?php
                if (sizeof($prochainesRencontres) > 0) {
                    ?>
                    <table class="table-evenements">
                        <tbody>
                            <?php
                            foreach ($prochainesRencontres as $prochaineRencontre) {
                                $propsProchaineRencontre = $ctrl->getRecapMatchProps($prochaineRencontre);
                                ?>
                                <tr>
                                    <td class="td-go-to-competition" competition-href="<?php echo $propsProchaineRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]; ?>">
                                        <?php echo substr($propsProchaineRencontre[EVENEMENT_JOUR], 0, 3).". ".str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDateTime_to_slashDateTime($prochaineRencontre[EVENEMENT_DATE_HEURE])); ?>
                                    </td>
                                    <td class="td-go-to-competition" competition-href="<?php echo $propsProchaineRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]; ?>">
                                        <?php echo $propsProchaineRencontre[MATCH_EQUIPE_DOMICILE]." VS ".$propsProchaineRencontre[MATCH_EQUIPE_EXTERIEUR]; ?>
                                    </td>
                                    <td class="td-go-to-competition" competition-href="<?php echo $propsProchaineRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]; ?>">
                                        <?php 
                                        if ($propsProchaineRencontre[EVENEMENT_GOOGLE_MAP_URL] != "") {
                                            echo "<a href=\"".$propsProchaineRencontre[EVENEMENT_GOOGLE_MAP_URL]."\" target=\"blank\">".$prochaineRencontre[EVENEMENT_VILLE]."</a>"; 
                                        } else {
                                            echo "&nbsp;";
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        ?>
                        </tbody>
                    </table>
                <?php
                }
                ?>
            </div>
        </div>
        <?php
    }
    ?>
</div>

<?php
require_once("webapp/views/common/footer/footer.html.php");

require_once("webapp/views/common/modals/alert/modal-alert.html.php");
require_once("webapp/views/common/modals/img/modal-img.html.php");
?>

<?php
$page->finalizePage();
?>