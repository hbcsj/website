ClubEquipesEquipeController = {};

ClubEquipesEquipeController.control_MatchsLinks = function() {
    $('td.td-go-to-competition').unbind('click');
    $('td.td-go-to-competition').click(function() {
        var urlFFHB = $(this).attr('competition-href');
        if (urlFFHB != null && urlFFHB != undefined && urlFFHB != '') {
            window.open(urlFFHB, '_blank');
        }
    });
};

initializePage = function() {
    MainWebsiteController.initializePage('club', 'equipes');

    ClubEquipesEquipeController.control_MatchsLinks();
};