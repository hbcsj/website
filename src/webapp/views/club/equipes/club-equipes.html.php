<?php
require_once("core/php/resources/page.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/lib/path-utils.php");

$page = new Page(
    "webapp/views/club/equipes/", 
    "club-equipes", 
    "ClubEquipesCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/common/header/header.html.php");
require_once("webapp/views/common/nav/nav.html.php");
require_once("webapp/views/club/sub-menu/sub-menu-club.html.php");

$equipes = $ctrl->getEquipes();
?>

<div class="container-fluid animated fadeIn">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
            <h1>Equipes</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
            <?php
            if (sizeof($equipes) > 0) {
                $i = 0;
                foreach ($equipes as $equipe) {
                    $classBordered = "";
                    if ($i < (sizeof($equipes) - 1)) {
                        $classBordered = " row-equipe-bordered";
                    }
                    ?>
                    <div class="row row-equipe<?php echo $classBordered; ?>">
                        <div class="col-xs-4">
                            <a href="<?php echo ROOT_PATH; ?>club/equipes/equipe?id=<?php echo $equipe[CATEGORIE_ID]; ?>" title="Aller sur la page des <?php echo $equipe[CATEGORIE_NOM]; ?>">
                                <img src="<?php echo SRC_PATH.PathUtils::getPhotoCategorie($equipe[CATEGORIE_ABREVIATION])."?".time(); ?>" class="img-equipe">
                            </a>
                            <div class="div-link-bottom-image">
                                <a href="<?php echo ROOT_PATH; ?>club/equipes/equipe?id=<?php echo $equipe[CATEGORIE_ID]; ?>">Aller sur la page des <?php echo $equipe[CATEGORIE_NOM]; ?></a>
                            </div>
                        </div>
                        <div class="col-xs-7 col-xs-offset-1">
                            <div class="nom-equipe">
                                <?php echo $equipe[CATEGORIE_NOM]; ?>
                                &nbsp;-&nbsp;
                                <span class="nb-equipes"><?php echo $equipe[CATEGORIE_NB_EQUIPES]; ?> &eacute;quipe(s) engag&eacute;e(s)</span>
                            </div>
                            <div class="coachs-equipe">
                                <b>Coach(s) / responsable(s) : </b>
                                <?php 
                                $coachs = $ctrl->getCoachsCategorie($equipe[CATEGORIE_ID]);
                                if (sizeof($coachs) > 0) {
                                    $i = 0;
                                    foreach ($coachs as $coach) {
                                        if ($coach[COACH_EN_SOUTIEN]) {
                                            echo "(";
                                        }
                                        echo $coach[LICENCIE_PRENOM]." ".$coach[LICENCIE_NOM];
                                        if ($coach[COACH_EN_SOUTIEN]) {
                                            echo ")";
                                        }

                                        if ($i < (sizeof($coachs) - 1)) {
                                            echo ", ";
                                        }
                                        $i++;
                                    }
                                }
                                ?>
                            </div>
                            <?php
                            $competitions = $ctrl->getCompetitionsCategorie($equipe[CATEGORIE_ID]);
                            if (sizeof($competitions) > 0) {
                                ?>
                                <ul class="compet-equipe">
                                    <b>Comp&eacute;tition(s) : </b>
                                    <?php
                                    foreach ($competitions as $competition) {
                                        ?>
                                        <li>
                                            <?php 
                                            if ($competition[NUM_EQUIPE] != null) {
                                                echo "Equipe ".$competition[NUM_EQUIPE]." : ";
                                            }
                                            if ($competition[COMPETITION_URL_SITE_FFHB] != "") {
                                                echo "<a href=\"".$competition[COMPETITION_URL_SITE_FFHB]."\" target=\"_blank\">".$competition[COMPETITION_NOM]."</a>"; 
                                            } else {
                                                echo $competition[COMPETITION_NOM]; 
                                            }
                                            ?>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                    $i++;
                }
            }
            ?>
        </div>
    </div>
</div>

<?php
require_once("webapp/views/common/footer/footer.html.php");

require_once("webapp/views/common/modals/alert/modal-alert.html.php");
?>

<?php
$page->finalizePage();
?>