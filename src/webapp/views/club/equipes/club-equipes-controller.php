<?php
require_once("core/php/controllers/abstract-page-controller.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/dao/categorie-dao.php");
require_once("common/php/dao/competition-dao.php");
require_once("common/php/managers/categorie-manager.php");

class ClubEquipesCtrl extends AbstractPageCtrl {

	private $categorieDAO;
	private $competitionDAO;

	private $categorieManager;

    public function __construct($pageName) {
		parent::__construct($pageName, null, true);

		$this->categorieDAO = new CategorieDAO($this->getDatabaseConnection());
		$this->competitionDAO = new CompetitionDAO($this->getDatabaseConnection());
		
    	$this->categorieManager = new CategorieManager($this->getDatabaseConnection());
	}
    
    public function getEquipes() {
        return $this->categorieDAO->getAvecEquipesInscrites(CATEGORIE_TABLE_NAME.".".CATEGORIE_POSITION_AFFICHAGE);
    }

    public function getCoachsCategorie($categorieId) {
        return $this->categorieManager->getCoachsCategorie($categorieId);
    }

    public function getCompetitionsCategorie($categorieId) {
        return $this->competitionDAO->getVisiblesEnCoursByCategorie($categorieId, COMPETITION_TABLE_NAME.".".COMPETITION_NUM_EQUIPE);
    }
}

?>