<?php
require_once("core/php/resources/page.php");
require_once("common/php/constants/data-constants.php");

$page = new Page(
    "webapp/views/club/reglement/", 
    "club-reglement", 
    "ClubReglementCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/common/header/header.html.php");
require_once("webapp/views/common/nav/nav.html.php");
require_once("webapp/views/club/sub-menu/sub-menu-club.html.php");
?>

<div class="container-fluid animated fadeIn">
    <?php
    $clubReglementFileContent = FileUtils::getFileContent(CLUB_REGLEMENT_FILE); 

    if ($clubReglementFileContent != "") {
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <?php echo $clubReglementFileContent; ?>
            </div>
        </div>
        <?php
    }
    ?>
</div>

<?php
require_once("webapp/views/common/footer/footer.html.php");

require_once("webapp/views/common/modals/alert/modal-alert.html.php");
?>

<?php
$page->finalizePage();
?>