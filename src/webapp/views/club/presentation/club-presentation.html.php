<?php
require_once("core/php/resources/page.php");
require_once("common/php/constants/data-constants.php");

$page = new Page(
    "webapp/views/club/presentation/", 
    "club-presentation", 
    "ClubPresentationCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/common/header/header.html.php");
require_once("webapp/views/common/nav/nav.html.php");
require_once("webapp/views/club/sub-menu/sub-menu-club.html.php");
?>

<div class="container-fluid animated fadeIn">
    <?php
    $clubPresentationFileContent = FileUtils::getFileContent(CLUB_PRESENTATION_FILE); 

    if ($clubPresentationFileContent != "") {
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <?php echo $clubPresentationFileContent; ?>
            </div>
        </div>
        <?php
    }
    ?>
</div>

<?php
require_once("webapp/views/common/footer/footer.html.php");

require_once("webapp/views/common/modals/alert/modal-alert.html.php");
?>

<?php
$page->finalizePage();
?>