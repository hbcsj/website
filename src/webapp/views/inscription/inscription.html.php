<?php
require_once("core/php/resources/page.php");
require_once("common/php/constants/data-constants.php");

$page = new Page(
    "webapp/views/inscription/", 
    "inscription", 
    "InscriptionCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/common/header/header.html.php");
require_once("webapp/views/common/nav/nav.html.php");
?>

<div class="container-fluid animated fadeIn">
    <?php
    $inscriptionFileContent = FileUtils::getFileContent(INSCRIPTION_FILE); 

    if ($inscriptionFileContent != "") {
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <?php echo $inscriptionFileContent; ?>
            </div>
        </div>
        <?php
    }
    ?>
</div>

<?php
require_once("webapp/views/common/footer/footer.html.php");

require_once("webapp/views/common/modals/alert/modal-alert.html.php");
?>

<?php
$page->finalizePage();
?>