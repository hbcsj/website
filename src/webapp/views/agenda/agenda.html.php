<?php
require_once("core/php/resources/page.php");
require_once("common/php/lib/date-utils.php");
require_once("lib/php/mobile-detect.php");

$page = new Page(
    "webapp/views/agenda/", 
    "agenda", 
    "AgendaCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/common/header/header.html.php");
require_once("webapp/views/common/nav/nav.html.php");

$mobileDetect = new Mobile_Detect();
?>

<div class="container-fluid animated fadeIn">
    <?php
    if ($ctrl->isAgendaJour()) {
        $typesEvenementsJour = $ctrl->getTypesEvenementsDate($ctrl->getJour());
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h1>
                    <?php 
                    echo DateUtils::getNomJour(
                        date("w", mktime(0, 0, 0, $ctrl->getMois(), $ctrl->getJour(), $ctrl->getAnnee()))
                    )." ".$ctrl->getJour()." ".DateUtils::getMois()[$ctrl->getMois()]." ".$ctrl->getAnnee();
                    ?>
                    <a class="btn btn-default" id="bt-back" href="<?php echo ROOT_PATH; ?>agenda?date=<?php echo $ctrl->getAnnee().$ctrl->getMois(); ?>">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-arrow-left"></span>
                        </div>
                        <div class="button__text">Retour</div>
                    </a>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-5 col-xs-offset-1 link-jour text-left">
                <a class="btn btn-default" href="<?php echo ROOT_PATH; ?>agenda?date=<?php echo $ctrl->getAnneePrecedente().$ctrl->getMoisPrecedent().$ctrl->getJourPrecedent(); ?>">
                    <div class="button__text">
                        <?php 
                        echo DateUtils::getNomJour(
                            date("w", mktime(0, 0, 0, $ctrl->getMoisPrecedent(), $ctrl->getJourPrecedent(), $ctrl->getAnneePrecedente()))
                        )." ".$ctrl->getJourPrecedent()." ".DateUtils::getMois()[$ctrl->getMoisPrecedent()]." ".$ctrl->getAnneePrecedente();
                        ?>
                    </div>
                </a>
            </div>
            <div class="col-xs-5 link-jour text-right">
                <a class="btn btn-default" href="<?php echo ROOT_PATH; ?>agenda?date=<?php echo $ctrl->getAnneeSuivante().$ctrl->getMoisSuivant().$ctrl->getJourSuivant(); ?>">
                    <div class="button__text">
                        <?php 
                        echo DateUtils::getNomJour(
                            date("w", mktime(0, 0, 0, $ctrl->getMoisSuivant(), $ctrl->getJourSuivant(), $ctrl->getAnneeSuivante()))
                        )." ".$ctrl->getJourSuivant()." ".DateUtils::getMois()[$ctrl->getMoisSuivant()]." ".$ctrl->getAnneeSuivante();
                        ?>
                    </div>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <?php
                if (sizeof($typesEvenementsJour) > 0) {
                    foreach ($typesEvenementsJour as $typeEvenement) {
                        $evenements = $ctrl->getEvenementsByType($typeEvenement[TYPE_EVENEMENT_ID]);
                        ?>
                        <h2><?php echo $typeEvenement[TYPE_EVENEMENT_LIBELLE]; ?></h2>
                        <table class="table-evenements">
                            <tbody>
                                <?php
                                foreach ($evenements as $evenement) {
                                    $evenementProps = $ctrl->getRecapEvenementProps($evenement);
                                    ?>
                                    <tr>
                                        <td class="td1">
                                            <?php 
                                            $dateHeure = DateUtils::convert_sqlDateTime_to_slashDateTime($evenement[EVENEMENT_DATE_HEURE]); 
                                            $dateHeureSplitted = explode(SLASH_DATE_TIME_SEPARATOR, $dateHeure);
                                            echo $dateHeureSplitted[1];
                                            ?>
                                        </td>
                                        <td class="td2">
                                            <?php 
                                            if ($evenementProps[EVENEMENT_GOOGLE_MAP_URL] != "") {
                                                echo "<a href=\"".$evenementProps[EVENEMENT_GOOGLE_MAP_URL]."\" target=\"blank\">".$evenement[EVENEMENT_VILLE]."</a>"; 
                                            } else {
                                                echo "&nbsp;";
                                            }
                                            ?>
                                        </td>
                                        <?php
                                        if ($evenement[EVENEMENT_TYPE_EVENEMENT_ID] == EVENEMENT_MATCH_TYPE_ID) {
                                            ?>
                                            <td class="td3">
                                                <?php 
                                                echo $evenementProps[MATCH_EQUIPE_DOMICILE]." VS ".$evenementProps[MATCH_EQUIPE_EXTERIEUR];
                                                echo "<br><br>"; 
                                                if ($evenementProps[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB] != "") {
                                                    ?>
                                                    <a href="<?php echo $evenementProps[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]; ?>" target="_blank">
                                                    <?php
                                                }
                                                echo $evenementProps[MATCH_COMPETITION][COMPETITION_NOM];
                                                if ($evenementProps[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB] != "") {
                                                    echo "</a>";
                                                }
                                                ?>
                                            </td>
                                            <td class="td0 resultat-<?php echo $evenementProps[MATCH_CLASS_RESULTAT]; ?>">
                                                <?php
                                                if ($evenement[EVENEMENT_TYPE_EVENEMENT_ID] == EVENEMENT_MATCH_TYPE_ID && 
                                                    (
                                                        $evenementProps[MATCH_SCORE_DOMICILE] != "" || 
                                                        $evenementProps[MATCH_SCORE_EXTERIEUR] != ""
                                                    )
                                                ) {
                                                    echo $evenementProps[MATCH_TEXT_RESULTAT];
                                                }
                                                ?>
                                            </td>
                                            <td class="td00">
                                                <?php
                                                if ($evenementProps[MATCH_FDM] != null) {
                                                    ?>
                                                    <a class="link-with-img" href="<?php echo SRC_PATH.$evenementProps[MATCH_FDM]; ?>" target="_blank" title="Afficher la feuille de match">
                                                        <span class="glyphicon glyphicon-open-file"></span>
                                                    </a>
                                                    <?php
                                                } else {
                                                    echo "&nbsp;";
                                                }
                                                ?>
                                            </td>
                                            <?php
                                        } else {
                                            ?>
                                            <td class="td4">
                                                <?php 
                                                if (sizeof($evenementProps[EVENEMENT_CATEGORIES]) > 0) {
                                                    if (!$ctrl->toutesLesCategoriesParticipentAEvenement($evenementProps[EVENEMENT_CATEGORIES])) {
                                                        $nomsCategories = array();
                                                        foreach ($evenementProps[EVENEMENT_CATEGORIES] as $evenementCategorie) {
                                                            $nomsCategories[] = $evenementCategorie[CATEGORIE_NOM];
                                                        }
                                                        echo implode(", ", $nomsCategories);                  
                                                        echo "<br><br>";
                                                    }
                                                }                              
                                                echo $evenement[EVENEMENT_NOM];
                                                ?>
                                            </td>
                                            <?php
                                        }
                                        ?>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                        <?php
                    }
                } else {
                    ?>
                    <h2 class="text-center">Aucun &eacute;v&eacute;nement</h2>
                    <?php
                }
                ?>
            </div>
        </div>
        <?php
    } else {
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h1><?php echo DateUtils::getMois()[$ctrl->getMois()]." ".$ctrl->getAnnee(); ?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-2 col-xs-offset-1 div-link-mois">
                <?php
                if ($mobileDetect->isMobile() || $mobileDetect->isTablet()) {
                    ?>
                    <a class="btn btn-default" href="<?php echo ROOT_PATH; ?>agenda?date=<?php echo $ctrl->getAnneePrecedente().$ctrl->getMoisPrecedent(); ?>">
                        <div class="button__text">
                            <?php echo DateUtils::getMois()[$ctrl->getMoisPrecedent()]."<br>".$ctrl->getAnneePrecedente(); ?>
                        </div>
                    </a>
                    <?php
                } else {
                    ?>
                    <a class="btn btn-default" href="<?php echo ROOT_PATH; ?>agenda?date=<?php echo $ctrl->getAnneePrecedente().$ctrl->getMoisPrecedent(); ?>">
                        <div class="button__text">
                            <?php echo DateUtils::getMois()[$ctrl->getMoisPrecedent()]." ".$ctrl->getAnneePrecedente(); ?>
                        </div>
                    </a>
                    <?php
                }
                ?>
            </div>
            <div class="col-xs-6">
                <table id="table-mois">
                    <thead>
                        <tr>
                            <th>Lun.</th>
                            <th>Mar.</th>
                            <th>Mer.</th>
                            <th>Jeu.</th>
                            <th>Ven.</th>
                            <th>Sam.</th>
                            <th>Dim.</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php
                            $dateCourante = 1;

                            $posPremJour = date("w", mktime(0, 0, 0, $ctrl->getMois(), 1, $ctrl->getAnnee())) - 1;
                            if ($posPremJour == -1) {
                                $posPremJour = 6;
                            }

                            for ($i = 0 ; $i < 42 ; $i++) {
                                if (($i % 7) == 0) {
                                    echo "</tr><tr>";
                                }

                                if (($i < $posPremJour) || ($i >= DateUtils::nbJoursDansMois($ctrl->getMois(), $ctrl->getAnnee()) + $posPremJour)) {
                                    echo "<td class=\"td-empty\">&nbsp;</td>";
                                } else {
                                    $classToday = "";

                                    $dateCouranteStr = $dateCourante;
                                    if ($dateCourante < 10) {
                                        $dateCouranteStr = "0".$dateCourante;
                                    }

                                    if (date("Ymd") == $ctrl->getAnnee().$ctrl->getMois().$dateCouranteStr) {
                                        $classToday = " td-today";
                                    }
                                    
                                    $typesEvenementsJour = $ctrl->getTypesEvenementsDate($dateCouranteStr);
                                    
                                    if (sizeof($typesEvenementsJour) == 0) {
                                        ?>
                                        <td class="<?php echo $classToday; ?>">
                                            <table>
                                                <tbody>
                                                    <tr><td class="date-agenda"><?php echo $dateCourante; ?></td><td></td><td></td></tr>
                                                    <tr><td></td><td></td><td></td></tr>
                                                    <tr><td></td><td></td><td></td></tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <?php
                                    } else {
                                    ?>
                                        <td class="td-clickable<?php echo $classToday; ?>" date="<?php echo $ctrl->getAnnee().$ctrl->getMois().$dateCouranteStr; ?>">
                                            <table>
                                                <tbody>
                                        
                                        <?php
                                        if (sizeof($typesEvenementsJour) == 1) {
                                            ?>
                                            <tr>
                                                <td class="date-agenda"><?php echo $dateCourante; ?></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="type-evenement" colspan="2" rowspan="2" title="<?php echo $typesEvenementsJour[0][TYPE_EVENEMENT_LIBELLE]; ?>">
                                                    <img src="<?php echo SRC_PATH.TYPES_EVENEMENTS_FOLDER."/".$typesEvenementsJour[0][TYPE_EVENEMENT_ID].".".IMG_TYPES_EVENEMENTS_EXTENSION; ?>">
                                                </td>
                                            </tr>
                                            <tr><td></td></tr>
                                            <?php
                                        } else if (sizeof($typesEvenementsJour) == 2) {
                                            ?>
                                            <tr>
                                                <td class="date-agenda"><?php echo $dateCourante; ?></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="type-evenement" colspan="2" rowspan="2" title="<?php echo $typesEvenementsJour[0][TYPE_EVENEMENT_LIBELLE]; ?>">
                                                    <img src="<?php echo SRC_PATH.TYPES_EVENEMENTS_FOLDER."/".$typesEvenementsJour[0][TYPE_EVENEMENT_ID].".".IMG_TYPES_EVENEMENTS_EXTENSION; ?>">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="type-evenement" title="<?php echo $typesEvenementsJour[1][TYPE_EVENEMENT_LIBELLE]; ?>">
                                                    <img src="<?php echo SRC_PATH.TYPES_EVENEMENTS_FOLDER."/".$typesEvenementsJour[1][TYPE_EVENEMENT_ID].".".IMG_TYPES_EVENEMENTS_EXTENSION; ?>">
                                                </td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <?php
                                        } else if (sizeof($typesEvenementsJour) == 3) {
                                        ?>
                                            <tr>
                                                <td class="date-agenda"><?php echo $dateCourante; ?></td>
                                                <td></td>
                                                <td class="type-evenement" title="<?php echo $typesEvenementsJour[1][TYPE_EVENEMENT_LIBELLE]; ?>">
                                                    <img src="<?php echo SRC_PATH.TYPES_EVENEMENTS_FOLDER."/".$typesEvenementsJour[1][TYPE_EVENEMENT_ID].".".IMG_TYPES_EVENEMENTS_EXTENSION; ?>">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="type-evenement" colspan="2" rowspan="2" title="<?php echo $typesEvenementsJour[0][TYPE_EVENEMENT_LIBELLE]; ?>">
                                                    <img src="<?php echo SRC_PATH.TYPES_EVENEMENTS_FOLDER."/".$typesEvenementsJour[0][TYPE_EVENEMENT_ID].".".IMG_TYPES_EVENEMENTS_EXTENSION; ?>">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="type-evenement" title="<?php echo $typesEvenementsJour[2][TYPE_EVENEMENT_LIBELLE]; ?>">
                                                    <img src="<?php echo SRC_PATH.TYPES_EVENEMENTS_FOLDER."/".$typesEvenementsJour[2][TYPE_EVENEMENT_ID].".".IMG_TYPES_EVENEMENTS_EXTENSION; ?>">
                                                </td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                        
                                                </tbody>
                                            </table>
                                        </td>
                                        <?php
                                    }

                                    $dateCourante += 1;
                                }
                            }
                            ?>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-xs-2 link-mois text-right">
                <?php
                if ($mobileDetect->isMobile() || $mobileDetect->isTablet()) {
                    ?>
                    <a class="btn btn-default" href="<?php echo ROOT_PATH; ?>agenda?date=<?php echo $ctrl->getAnneeSuivante().$ctrl->getMoisSuivant(); ?>">
                        <div class="button__text">
                            <?php echo DateUtils::getMois()[$ctrl->getMoisSuivant()]."<br>".$ctrl->getAnneeSuivante(); ?>
                        </div>
                    </a>
                    <?php
                } else {
                    ?>
                    <a class="btn btn-default" href="<?php echo ROOT_PATH; ?>agenda?date=<?php echo $ctrl->getAnneeSuivante().$ctrl->getMoisSuivant(); ?>">
                        <div class="button__text">
                            <?php echo DateUtils::getMois()[$ctrl->getMoisSuivant()]." ".$ctrl->getAnneeSuivante(); ?>
                        </div>
                    </a>
                    <?php
                }
                ?>
            </div>
        </div>
        <?php
    }
    ?>
</div>

<?php
require_once("webapp/views/common/footer/footer.html.php");

require_once("webapp/views/common/modals/alert/modal-alert.html.php");
?>

<?php
$page->finalizePage();
?>