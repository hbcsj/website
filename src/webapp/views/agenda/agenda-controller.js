AgendaController = {};

AgendaController.control_AgendaMois = function() {
    if (Utils.exists('#table-mois')) {
        control_TdsClickables();
    }
    
    function control_TdsClickables() {
        $('td.td-clickable').click(function() {
            var date = $(this).attr("date");
            window.location.href = rootPath+'agenda?date='+date;
        });
    }
};

initializePage = function() {
    MainWebsiteController.initializePage('agenda', null);

    AgendaController.control_AgendaMois();
};