<?php
require_once("core/php/controllers/abstract-page-controller.php");
require_once("common/php/dao/categorie-dao.php");
require_once("common/php/dao/evenement-dao.php");
require_once("common/php/dao/type-evenement-dao.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/managers/evenement-manager.php");

class AgendaCtrl extends AbstractPageCtrl {

  private $isAgendaJour;
  private $date;
  private $annee;
  private $anneePrecedente;
  private $anneeSuivante;
  private $mois;
  private $moisPrecedent;
  private $moisSuivant;
  private $jour;
  private $jourPrecedent;
  private $jourSuivant;

  private $categorieDAO;
  private $evenementDAO;
  private $typeEvenementDAO;

  private $evenementManager;

  public function __construct($pageName) {
    parent::__construct($pageName, array(
      "date" => $_GET["date"]
    ), true);

    $this->categorieDAO = new CategorieDAO($this->getDatabaseConnection());
    $this->evenementDAO = new EvenementDAO($this->getDatabaseConnection());
    $this->typeEvenementDAO = new TypeEvenementDAO($this->getDatabaseConnection());

    $this->evenementManager = new EvenementManager($this->getDatabaseConnection());

    $this->checkRequest();
  }
  
  private function checkRequest() {
    $checkParams = $this->checkParams(array(
      array(GET, "date")
    ));
    if ($checkParams) {
      $this->checkDate();
    }
  }

  private function checkDate() {
    if ((strlen($_GET["date"]) != 6 && strlen($_GET["date"]) != 8) || !is_numeric($_GET["date"])) {
      $this->sendCheckError(
        HTTP_400, 
        "La date (date = '".$_GET["date"]."') doit etre composee de 6 ou 8 chiffres", 
        "400"
      );
    } else {
      $this->setDateInfos();

      if ($this->isAgendaJour) {
        if ($this->jour <= 0 || DateUtils::nbJoursDansMois($this->mois, $this->annee) < $this->jour) {
          $this->sendCheckError(
            HTTP_400, 
            "Le jour (jour = '".$this->jour."') n'est pas valide", 
            "400"
          );
        }
      }

      if ($this->mois <= 0 || $this->mois > 12) {
        $this->sendCheckError(
          HTTP_400, 
          "Le mois (mois = '".$this->mois."') n'est pas valide", 
          "400"
        );
      }
    }
  }

  private function setDateInfos() {
    $this->isAgendaJour = (strlen($_GET["date"]) == 8);
    $this->date = $_GET["date"];
    $this->annee = substr($this->date, 0, 4);
    $this->mois = substr($this->date, 4, 2);
    $this->jour = null;
    if ($this->isAgendaJour) {
      $this->jour = substr($this->date, 6, 2);
    }

    if ($this->isAgendaJour) {
      $dateJourPrecedent = date(SQL_DATE_FORMAT, mktime(0, 0, 0, $this->mois, $this->jour - 1, $this->annee));
      $dateJourPrecedentSplitted = explode(SQL_DATE_SEPARATOR, $dateJourPrecedent);
      $this->jourPrecedent = $dateJourPrecedentSplitted[2];
      $this->moisPrecedent = $dateJourPrecedentSplitted[1];
      $this->anneePrecedente = $dateJourPrecedentSplitted[0];

      $dateJourSuivant = date(SQL_DATE_FORMAT, mktime(0, 0, 0, $this->mois, $this->jour + 1, $this->annee));
      $dateJourSuivantSplitted = explode(SQL_DATE_SEPARATOR, $dateJourSuivant);
      $this->jourSuivant = $dateJourSuivantSplitted[2];
      $this->moisSuivant = $dateJourSuivantSplitted[1];
      $this->anneeSuivante = $dateJourSuivantSplitted[0];
    } else {
      $this->moisPrecedent = $this->mois - 1;
      if ($this->moisPrecedent == 0) {
        $this->moisPrecedent = "12";
      } else if ($this->moisPrecedent < 10) {
        $this->moisPrecedent = "0".$this->moisPrecedent;
      }
      $this->moisSuivant = $this->mois + 1;
      if ($this->moisSuivant == 13) {
        $this->moisSuivant = "01";
      } else if ($this->moisSuivant < 10) {
        $this->moisSuivant = "0".$this->moisSuivant;
      }
      $this->anneePrecedente = $this->annee;
      if ($this->mois == "01") {
        $this->anneePrecedente = $this->anneePrecedente - 1;
      }
      $this->anneeSuivante = $this->annee;
      if ($this->mois == "12") {
        $this->anneeSuivante = $this->anneeSuivante + 1;
      }
    }
  }

  public function isAgendaJour() {
    return $this->isAgendaJour;
  }

  public function getJour() {
    return $this->jour;
  }

  public function getJourPrecedent() {
    return $this->jourPrecedent;
  }

  public function getJourSuivant() {
    return $this->jourSuivant;
  }

  public function getMois() {
    return $this->mois;
  }

  public function getMoisPrecedent() {
    return $this->moisPrecedent;
  }

  public function getMoisSuivant() {
    return $this->moisSuivant;
  }

  public function getAnnee() {
    return $this->annee;
  }

  public function getAnneePrecedente() {
    return $this->anneePrecedente;
  }

  public function getAnneeSuivante() {
    return $this->anneeSuivante;
  }

  public function getTypesEvenementsDate($jour) {
    return $this->typeEvenementDAO->getTypesEvenementsByDate($this->annee.SQL_DATE_SEPARATOR.$this->mois.SQL_DATE_SEPARATOR.$jour);
  }

  public function getEvenementsByType($typeEvenementId) {
    return $this->evenementDAO->getAllByParams(
      $this->annee.SQL_DATE_SEPARATOR.$this->mois.SQL_DATE_SEPARATOR.$this->jour, 
      $this->anneeSuivante.SQL_DATE_SEPARATOR.$this->moisSuivant.SQL_DATE_SEPARATOR.$this->jourSuivant, 
      null, 
      null, 
      $typeEvenementId, 
      null, 
      null, 
      null, 
      null, 
      true, 
      EVENEMENT_TABLE_NAME.".".EVENEMENT_DATE_HEURE
    );
  }

  public function getRecapEvenementProps($evenement) {
    return $this->evenementManager->getRecapEvenementProps($evenement);
  }

	public function toutesLesCategoriesParticipentAEvenement($categoriesEvenement) {
		return (sizeof($categoriesEvenement) == sizeof($this->categorieDAO->getAll()));
	}
}

?>