<?php
require_once("core/php/resources/page.php");

$page = new Page(
    "webapp/views/error/404/", 
    "error-404", 
    "Error404Ctrl"
);
$ctrl = $page->getController();
?>

<div class="container-error animated fadeIn">
    <h1>Site du <?php echo $ctrl->getNomClub(); ?></h1>
    <h2>La page demand&eacute;e n'existe pas</h2>
</div>

<?php
$page->finalizePage();
?>