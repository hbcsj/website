<?php
require_once("core/php/resources/page.php");

$page = new Page(
    "webapp/views/error/400/", 
    "error-400", 
    "Error400Ctrl"
);
$ctrl = $page->getController();
?>

<div class="container-error animated fadeIn">
    <h1>Site du <?php echo $ctrl->getNomClub(); ?></h1>
    <h2>Bad request</h2>
</div>

<?php
$page->finalizePage();
?>