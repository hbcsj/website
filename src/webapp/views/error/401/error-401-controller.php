<?php
require_once("core/php/controllers/abstract-page-controller.php");
require_once("common/php/dao/infos-club-dao.php");

class Error401Ctrl extends AbstractPageCtrl {

	private $infosClubDAO;
    
    public function __construct($pageName) {
		parent::__construct($pageName, null, true);

		$this->infosClubDAO = new InfosClubDAO($this->getDatabaseConnection());
	}

    public function getNomClub() {
        return $this->infosClubDAO->getSingleton()[INFOS_CLUB_NOM];
    }
}

?>