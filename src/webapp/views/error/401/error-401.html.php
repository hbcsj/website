<?php
require_once("core/php/resources/page.php");

$page = new Page(
    "webapp/views/error/401/", 
    "error-401", 
    "Error401Ctrl"
);
$ctrl = $page->getController();
?>

<div class="container-error animated fadeIn">
    <h1>Site du <?php echo $ctrl->getNomClub(); ?></h1>
    <h2>Unauthorized access</h2>
</div>

<?php
$page->finalizePage();
?>