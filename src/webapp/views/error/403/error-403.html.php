<?php
require_once("core/php/resources/page.php");

$page = new Page(
    "webapp/views/error/403/", 
    "error-403", 
    "Error403Ctrl"
);
$ctrl = $page->getController();
?>

<div class="container-error animated fadeIn">
    <h1>Site du <?php echo $ctrl->getNomClub(); ?></h1>
    <h2>Forbidden access</h2>
</div>

<?php
$page->finalizePage();
?>