<?php
require_once("core/php/controllers/abstract-page-controller.php");
require_once("common/php/dao/categorie-dao.php");
require_once("common/php/dao/entrainement-dao.php");
require_once("common/php/dao/ressources-externes-dao.php");
require_once("common/php/managers/gymnase-manager.php");
require_once("common/php/managers/categorie-manager.php");

class EntrainementsCtrl extends AbstractPageCtrl {

    private $categorieDAO;
    private $entrainementDAO;
    private $ressourcesExternesDAO;

    private $gymnaseManager;
    private $categorieManager;

    public function __construct($pageName) {
        parent::__construct($pageName, null, true);

        $this->categorieDAO = new CategorieDAO($this->getDatabaseConnection());
        $this->entrainementDAO = new EntrainementDAO($this->getDatabaseConnection());
        $this->ressourcesExternesDAO = new RessourcesExternesDAO($this->getDatabaseConnection());

        $this->gymnaseManager = new GymnaseManager($this->getDatabaseConnection());
        $this->categorieManager = new CategorieManager($this->getDatabaseConnection());
    }

    public function getRessourcesExternes() {
        return $this->ressourcesExternesDAO->getSingleton();
    }

    public function getGymnasePrincipal() {
        return $this->gymnaseManager->getGymnasePrincipal();
    }

    public function getCategoriesAvecEquipesInscrites() {
        return $this->categorieDAO->getAvecEquipesInscrites(CATEGORIE_TABLE_NAME.".".CATEGORIE_POSITION_AFFICHAGE);
    }

    public function getEntrainementsCategorie($categorieId) {
        return $this->entrainementDAO->getByCategorie($categorieId);
    }

    public function getCoachsCategorie($categorieId) {
        return $this->categorieManager->getCoachsCategorie($categorieId);
    }
}

?>