<?php
require_once("core/php/resources/page.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/lib/date-utils.php");

$page = new Page(
    "webapp/views/entrainements/", 
    "entrainements", 
    "EntrainementsCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/common/header/header.html.php");
require_once("webapp/views/common/nav/nav.html.php");

$ressourcesExternes = $ctrl->getRessourcesExternes();
$gymnasePrincipal = $ctrl->getGymnasePrincipal();
$categories = $ctrl->getCategoriesAvecEquipesInscrites();
?>

<div class="container-fluid animated fadeIn">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
            <h1>Entra&icirc;nements</h1>
            <h2>Gymnase</h2>
            <p>
                <a href="<?php echo $ressourcesExternes[GOOGLE_MAP_URL]."=".$gymnasePrincipal[GYMNASE_LATITUDE].",".$gymnasePrincipal[GYMNASE_LONGITUDE]; ?>" target="_blank"><?php echo $gymnasePrincipal[GYMNASE_NOM]; ?></a>, <?php echo $gymnasePrincipal[GYMNASE_ADRESSE].", ".$gymnasePrincipal[GYMNASE_CODE_POSTAL]." ".$gymnasePrincipal[GYMNASE_VILLE]; ?>
            </p>
            <h2>Horaires</h2>
            <div class="row">
                <div class="col-xs-12 col-lg-10 col-lg-offset-1">
                    <table>
                        <tbody>
                            <?php
                            if (sizeof($categories) > 0) {
                                foreach ($categories as $categorie) {
                                ?>
                                <tr>
                                    <td><b><?php echo $categorie[CATEGORIE_NOM]; ?></b></td>
                                    <td>
                                        <?php
                                        $entrainements = $ctrl->getEntrainementsCategorie($categorie[CATEGORIE_ID]);
                                        if (sizeof($entrainements) > 0) {
                                            $i = 0;
                                            foreach ($entrainements as $entrainement) {
                                                echo DateUtils::getNomJour($entrainement[ENTRAINEMENT_JOUR])." - ".DateUtils::convert_sqlTime_to_timeWithoutSeconds($entrainement[ENTRAINEMENT_HEURE_DEBUT])." &agrave ".DateUtils::convert_sqlTime_to_timeWithoutSeconds($entrainement[ENTRAINEMENT_HEURE_FIN]);

                                                if ($i < (sizeof($entrainements) - 1)) {
                                                    echo "<br>";
                                                }
                                                $i++;
                                            }
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <?php 
                                        $coachs = $ctrl->getCoachsCategorie($categorie[CATEGORIE_ID]);
                                        if (sizeof($coachs) > 0) {
                                            $i = 0;
                                            foreach ($coachs as $coach) {
                                                if ($coach[COACH_EN_SOUTIEN]) {
                                                    echo "(";
                                                }
                                                echo $coach[LICENCIE_PRENOM]." ".$coach[LICENCIE_NOM];
                                                if ($coach[COACH_EN_SOUTIEN]) {
                                                    echo ")";
                                                }

                                                if ($i < (sizeof($coachs) - 1)) {
                                                    echo "<br>";
                                                }
                                                $i++;
                                            }
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
require_once("webapp/views/common/footer/footer.html.php");

require_once("webapp/views/common/modals/alert/modal-alert.html.php");
?>

<?php
$page->finalizePage();
?>