<?php
require_once("core/php/resources/view.php");
require_once("lib/php/mobile-detect.php");

$view = new View(
    "webapp/views/home/sub-menu/", 
    "sub-menu-home", 
    "SubMenuHomeCtrl"
);
$subMenuHomeCtrl = $view->getController();

$mobileDetect = new Mobile_Detect();
$ressourcesExternes = $subMenuHomeCtrl->getRessourcesExternes();
?>

<div class="sub-menu">
    <?php
    if ($mobileDetect->isMobile() || $mobileDetect->isTablet()) {
        ?>
        <a href="#" div-target="news" class="link-home">News</a>
        <a href="#" div-target="pratique" class="link-home">Pratique</a>
        <a href="#" div-target="matchs" class="link-home">R&eacute;sult. / Prog.</a>
        <a href="#" div-target="presse" class="link-home">Presse</a>
        <a href="<?php echo ROOT_PATH; ?>dispos" target="_blank">
            <span class="glyphicon glyphicon-calendar"></span>
            Dispos
        </a>
        <a href="<?php echo ROOT_PATH; ?>admin" target="_blank">
            <span class="glyphicon glyphicon-cog"></span>
            Admin
        </a>
        <div id="links-home-reseaux-sociaux">
            <a href="<?php echo $ressourcesExternes[RESSOURCES_EXTERNES_FACEBOOK_URL].$ressourcesExternes[RESSOURCES_EXTERNES_FACEBOOK_ID]; ?>" target="_blank" title="Nous suivre sur Facebook">
                <img src="<?php echo SRC_PATH; ?>assets/img/icons/icon-facebook-small.png">
            </a>
            <a href="<?php echo $ressourcesExternes[RESSOURCES_EXTERNES_INSTAGRAM_URL].$ressourcesExternes[RESSOURCES_EXTERNES_INSTAGRAM_ID]; ?>" target="_blank" title="Nous suivre sur Instagram">
                <img src="<?php echo SRC_PATH; ?>assets/img/icons/icon-instagram-small.png">
            </a>
            <a href="<?php echo $ressourcesExternes[RESSOURCES_EXTERNES_TWITTER_URL].$ressourcesExternes[RESSOURCES_EXTERNES_TWITTER_ID]; ?>" target="_blank" title="Nous suivre sur Twitter">
                <img src="<?php echo SRC_PATH; ?>assets/img/icons/icon-twitter-small.png">
            </a>
            <a href="<?php echo $ressourcesExternes[RESSOURCES_EXTERNES_YOUTUBE_URL].$ressourcesExternes[RESSOURCES_EXTERNES_YOUTUBE_ID]; ?>" target="_blank" title="Nous suivre sur YouTube">
                <img src="<?php echo SRC_PATH; ?>assets/img/icons/icon-youtube-small.png">
            </a>
        </div>
        <?php
    } else {
        ?>
        <a href="#" div-target="news-pratique" class="link-home">News / Pratique</a>
        <a href="#" div-target="matchs" class="link-home" class="link-home">R&eacute;sultats / Programme</a>
        <a href="#" div-target="presse" class="link-home">Presse</a>
        <a href="dispos" target="_blank">
            <span class="glyphicon glyphicon-calendar"></span>
            Dispos
        </a>
        <a href="admin" target="_blank">
            <span class="glyphicon glyphicon-cog"></span>
            Admin
        </a>
        <div id="links-home-reseaux-sociaux">
            <a href="<?php echo $ressourcesExternes[RESSOURCES_EXTERNES_FACEBOOK_URL].$ressourcesExternes[RESSOURCES_EXTERNES_FACEBOOK_ID]; ?>" target="_blank" title="Nous suivre sur Facebook">
                <img src="<?php echo SRC_PATH; ?>assets/img/icons/icon-facebook-small.png">
            </a>
            <a href="<?php echo $ressourcesExternes[RESSOURCES_EXTERNES_INSTAGRAM_URL].$ressourcesExternes[RESSOURCES_EXTERNES_INSTAGRAM_ID]; ?>" target="_blank" title="Nous suivre sur Instagram">
                <img src="<?php echo SRC_PATH; ?>assets/img/icons/icon-instagram-small.png">
            </a>
            <a href="<?php echo $ressourcesExternes[RESSOURCES_EXTERNES_TWITTER_URL].$ressourcesExternes[RESSOURCES_EXTERNES_TWITTER_ID]; ?>" target="_blank" title="Nous suivre sur Twitter">
                <img src="<?php echo SRC_PATH; ?>assets/img/icons/icon-twitter-small.png">
            </a>
            <a href="<?php echo $ressourcesExternes[RESSOURCES_EXTERNES_YOUTUBE_URL].$ressourcesExternes[RESSOURCES_EXTERNES_YOUTUBE_ID]; ?>" target="_blank" title="Nous suivre sur YouTube">
                <img src="<?php echo SRC_PATH; ?>assets/img/icons/icon-youtube-small.png">
            </a>
        </div>
        <?php
    }
    ?>
</div>
