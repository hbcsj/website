<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/ressources-externes-dao.php");

class SubMenuHomeCtrl extends AbstractViewCtrl {

	private $ressourcesExternesDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		$this->ressourcesExternesDAO = new RessourcesExternesDAO($this->getDatabaseConnection());
	}

    public function getRessourcesExternes() {
        return $this->ressourcesExternesDAO->getSingleton();
    }
}

?>