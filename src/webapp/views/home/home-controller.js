HomeController = {};

HomeController.control_HomeLinks = function() {
    $('a.link-home').click(function() {
        var divTarget = $(this).attr('div-target');
        var scrollToPosition = $('#'+divTarget).offset().top - 190;
        $('html, body').animate({
            scrollTop: scrollToPosition
        }, 1000);
    });
};

HomeController.control_MessageContact = function() {
    onClick_btEnvoyer();

    function onClick_btEnvoyer() {
        $('#form-message-contact__bt-envoyer').unbind('click');
        $('#form-message-contact__bt-envoyer').click(function() {
            FormUtils.hideFormErrors('#form-message-contact');
            ViewUtils.unsetHTMLAndHide('#form-message-contact .form-result-message');
            ViewUtils.desactiveButtonAndShowLoader('#form-message-contact__bt-envoyer', '#form-message-contact__bt-envoyer .loader');

            var formValidation = FormUtils.validateForm('#form-message-contact');
            
            if (formValidation) {
                var inputNom = $.trim($('#form-message-contact__input-nom').val());
                var inputEmail = $.trim($('#form-message-contact__input-email').val());
                var inputMessage = $.trim($('#form-message-contact__input-message').val());

                sendMessage(inputNom, inputEmail, inputMessage);
            } else {
                ViewUtils.activeButtonAndHideLoader('#form-message-contact__bt-envoyer', '#form-message-contact__bt-envoyer .loader');
            }
        });
    }

    function sendMessage(inputNom, inputEmail, inputMessage) {
        AjaxUtils.callService(
            'home', 
            'sendMessage', 
            AjaxUtils.dataTypes.json, 
            AjaxUtils.requestTypes.post, 
            null, 
            {
                nom: inputNom,
                email: inputEmail,
                message: inputMessage
            }, 
            function(response) {
                if (response.success) {
                    FormUtils.displayFormResultMessage('#form-message-contact .form-result-message', 'Message envoy&eacute;', FormUtils.resultsTypes.ok);
                    $('#form-message-contact__bt-envoyer .loader').hide();
                } else {
                    ViewUtils.activeButtonAndHideLoader('#form-message-contact__bt-envoyer', '#form-message-contact__bt-envoyer .loader');
                    FormUtils.displayFormResultMessage('#form-message-contact .form-result-message', 'Erreur durant l\'envoi du message', FormUtils.resultsTypes.error);
                }
            },
            function(response) {
                ViewUtils.activeButtonAndHideLoader('#form-message-contact__bt-envoyer', '#form-message-contact__bt-envoyer .loader');
                FormUtils.displayFormResultMessage('#form-message-contact .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
            }
        );
    }
};

HomeController.control_MatchsLinks = function() {
    $('td.td-go-to-competition').unbind('click');
    $('td.td-go-to-competition').click(function() {
        var urlFFHB = $(this).attr('competition-href');
        if (urlFFHB != null && urlFFHB != undefined && urlFFHB != '') {
            window.open(urlFFHB, '_blank');
        }
    });
};

initializePage = function() {
    MainWebsiteController.initializePage('home', null);

    HomeController.control_HomeLinks();
    HomeController.control_MessageContact();
    HomeController.control_MatchsLinks();
};