<?php
require_once("core/php/resources/page.php");
require_once("common/php/constants/config-constants.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("common/php/managers/presse-manager.php");
require_once("lib/php/mobile-detect.php");

$page = new Page(
    "webapp/views/home/", 
    "home", 
    "HomeCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/common/header/header.html.php");
require_once("webapp/views/common/nav/nav.html.php");
require_once("webapp/views/home/sub-menu/sub-menu-home.html.php");

$mobileDetect = new Mobile_Detect();
$homeMessages = $ctrl->getHomeMessages();
$dernieresNews = $ctrl->getDernieresActualites();
$dernieresRencontres = $ctrl->getDernieresRencontres();
$prochainesRencontres = $ctrl->getProchainesRencontres();
$derniersArticlesPresse = $ctrl->getDerniersArticlesPresse();
$ressourcesExternes = $ctrl->getRessourcesExternes();
$gymnasePrincipal = $ctrl->getGymnasePrincipal();
?>

<div class="container-fluid animated fadeIn">
<?php
    if (file_exists(HOME_IMG_FILE)) {
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <img class="home-img" src="<?php echo SRC_PATH.HOME_IMG_FILE."?".time(); ?>">
            </div>
        </div>
        <?php
    }

    if (sizeof($homeMessages) > 0) {
        foreach ($homeMessages as $homeMessage) {
            $glyphicon = "info-sign";
            if ($homeMessage[HOME_MESSAGE_CRITICITE_ID] == HOME_MESSAGE_WARNING_CRITICITE_ID) {
                $glyphicon = "warning-sign";
            } else if ($homeMessage[HOME_MESSAGE_CRITICITE_ID] == HOME_MESSAGE_DANGER_CRITICITE_ID) {
                $glyphicon = "exclamation-sign";
            }
            ?>
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1">
                    <div class="jumbotron jumbotron-<?php echo $ctrl->getLibelleCriticite($homeMessage[HOME_MESSAGE_CRITICITE_ID]); ?>">
                        <span class="glyphicon glyphicon-<?php echo $glyphicon; ?>"></span>
                        <?php echo $homeMessage[HOME_MESSAGE_TEXTE]; ?>
                    </div>
                </div>
            </div>
            <?php
        }
    }

    if ($mobileDetect->isMobile() || $mobileDetect->isTablet()) {
        ?>
        <div class="row" id="news">
            <div class="col-xs-10 col-xs-offset-1">
                <h1>Derni&egrave;res nouvelles</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <?php
                if (sizeof($dernieresNews) > 0) {
                    $i = 0;
                    foreach ($dernieresNews as $actu) {
                        $classBordered = "";
                        if ($i < (sizeof($dernieresNews) - 1)) {
                            $classBordered = " row-actu-bordered";
                        }
                        ?>
                        <div class="row row-actu<?php echo $classBordered; ?>">
                            <div class="col-xs-4">
                                <a class="link-with-img" href="news/actu?id=<?php echo $actu[ACTUALITE_ID]; ?>" title="Lire l'article complet">
                                    <img src="<?php echo SRC_PATH.PathUtils::getActualiteImgFile($actu[ACTUALITE_ID]); ?>" class="img-actu">
                                </a>
                                <div class="div-link-bottom-image">
                                    <a href="<?php echo ROOT_PATH; ?>news/actu?id=<?php echo $actu[ACTUALITE_ID]; ?>">Lire l'article complet</a>
                                </div>
                            </div>
                            <div class="col-xs-7 col-xs-offset-1">
                                <div class="date-publication">
                                    Publi&eacute; le <?php echo str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDateTime_to_slashDateTime($actu[ACTUALITE_DATE_HEURE])); ?>
                                </div>
                                <div class="titre-actu"><?php echo $actu[ACTUALITE_TITRE]; ?></div>
                                <div class="resume-actu">
                                    <?php echo nl2br(FileUtils::getFileContent(PathUtils::getActualiteHomeFile($actu[ACTUALITE_ID]))); ?>
                                </div>
                            </div>
                        </div>
                        <?php
                        $i++;
                    }
                }
                ?>
            </div>
        </div>
        <div class="row" id="pratique">
            <div class="col-xs-10 col-xs-offset-1">
                <h1>Nous trouver</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <div class="row" id="row-nous-trouver-mobile">
                    <div class="col-xs-4">
                        <a class="link-with-img" href="<?php echo $ressourcesExternes[RESSOURCES_EXTERNES_GOOGLE_MAP_URL].$gymnasePrincipal[GYMNASE_LATITUDE].",".$gymnasePrincipal[GYMNASE_LONGITUDE]; ?>" target="_blank" title="Visualiser sur Google Map">
                            <img class="img-google-map" src="<?php echo SRC_PATH; ?>assets/img/gymnase-map.png">
                        </a>
                    </div>
                    <div class="col-xs-7 col-xs-offset-1">
                    <?php echo $gymnasePrincipal[GYMNASE_NOM]."<br>".$gymnasePrincipal[GYMNASE_ADRESSE]."<br>".$gymnasePrincipal[GYMNASE_CODE_POSTAL]." ".$gymnasePrincipal[GYMNASE_VILLE]; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h1>Nous contacter</h1>
            </div>
        </div>
        <div class="form-horizontal" id="form-message-contact">
            <div class="form-group">
                <label for="form-message-contact__input-nom" class="col-xs-3 col-xs-offset-1 control-label required">Votre nom :</label>
                <div class="col-xs-6 form-input" for="form-message-contact__input-nom">
                    <input type="text" class="form-control" id="form-message-contact__input-nom" required>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-message-contact__input-nom"></div>
            </div>
            <div class="form-group">
                <label for="form-message-contact__input-email" class="col-xs-3 col-xs-offset-1 control-label required">Votre email :</label>
                <div class="col-xs-6 form-input" for="form-message-contact__input-email">
                    <input type="text" class="form-control" id="form-message-contact__input-email" pattern="^[A-Za-z0-9._-]+@[A-Za-z0-9.-]{2,}[.][A-Za-z]{2,3}$" pattern-indication="Le format de l'email est invalide" required>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-message-contact__input-email"></div>
            </div>
            <div class="form-group">
                <label for="form-message-contact__input-message" class="col-xs-3 col-xs-offset-1 control-label required">Votre message :</label>
                <div class="col-xs-6 form-input" for="form-message-contact__input-message">
                    <textarea class="form-control" rows="3" id="form-message-contact__input-message" required></textarea>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-message-contact__input-message"></div>
            </div>
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 text-required">Les champs marqu&eacute;s d'une * sont obligatoires</div>
            </div>
            <div class="row">
                <div class="col-xs-3 col-xs-offset-1">
                    <button id="form-message-contact__bt-envoyer" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-envelope"></span>
                        </div>
                        <div class="button__text">Envoyer</div>
                        <div class="loader"></div>
                    </button>
                </div>
                <div class="col-xs-7">
                    <div class="form-result-message"></div>
                </div>
            </div>
        </div>
        <div class="row" id="matchs">
            <div class="col-xs-10 col-xs-offset-1">
                <h1>Derniers r&eacute;sultats</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <?php
                if (sizeof($dernieresRencontres) > 0) {
                    ?>
                    <table class="table-evenements">
                        <tbody>
                            <?php
                            foreach ($dernieresRencontres as $derniereRencontre) {
                                $propsDerniereRencontre = $ctrl->getRecapMatchProps($derniereRencontre);
                                ?>
                                <tr>
                                    <td class="td1 td-go-to-competition" competition-href="<?php echo $propsDerniereRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]; ?>">
                                        <?php echo substr($propsDerniereRencontre[EVENEMENT_JOUR], 0, 3).". ".str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDateTime_to_slashDateTime($derniereRencontre[EVENEMENT_DATE_HEURE])); ?>
                                    </td>
                                    <td class="td1 td-go-to-competition" competition-href="<?php echo $propsDerniereRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]; ?>">
                                        <?php echo $propsDerniereRencontre[MATCH_EQUIPE_DOMICILE]." VS ".$propsDerniereRencontre[MATCH_EQUIPE_EXTERIEUR]; ?>
                                    </td>
                                    <td class="td2 resultat-<?php echo $propsDerniereRencontre[MATCH_CLASS_RESULTAT]; ?> td-go-to-competition" competition-href="<?php echo $propsDerniereRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]; ?>">
                                        <?php
                                        if ($propsDerniereRencontre[MATCH_SCORE_DOMICILE] != "" || $propsDerniereRencontre[MATCH_SCORE_EXTERIEUR] != "") {
                                            echo $propsDerniereRencontre[MATCH_TEXT_RESULTAT];
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        ?>
                        </tbody>
                    </table>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h1>Prochaines rencontres</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <?php
                if (sizeof($prochainesRencontres) > 0) {
                    ?>
                    <table class="table-evenements">
                        <tbody>
                            <?php
                            foreach ($prochainesRencontres as $prochaineRencontre) {
                                $propsProchaineRencontre = $ctrl->getRecapMatchProps($prochaineRencontre);
                                ?>
                                <tr>
                                    <td class="td1 td-go-to-competition" competition-href="<?php echo $propsProchaineRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]; ?>">
                                        <?php echo substr($propsProchaineRencontre[EVENEMENT_JOUR], 0, 3).". ".str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDateTime_to_slashDateTime($prochaineRencontre[EVENEMENT_DATE_HEURE])); ?>
                                    </td>
                                    <td class="td1 td-go-to-competition" competition-href="<?php echo $propsProchaineRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]; ?>">
                                        <?php echo $propsProchaineRencontre[MATCH_EQUIPE_DOMICILE]." VS ".$propsProchaineRencontre[MATCH_EQUIPE_EXTERIEUR]; ?>
                                    </td>
                                    <td class="td2">
                                        <?php 
                                        if ($propsProchaineRencontre[EVENEMENT_GOOGLE_MAP_URL] != "") {
                                            echo "<a href=\"".$propsProchaineRencontre[EVENEMENT_GOOGLE_MAP_URL]."\" target=\"blank\">".$prochaineRencontre[EVENEMENT_VILLE]."</a>"; 
                                        } else {
                                            echo "&nbsp;";
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        ?>
                        </tbody>
                    </table>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="row" id="presse">
            <div class="col-xs-10 col-xs-offset-1">
                <h1>Derniers articles de presse</h1>
            </div>
        </div>
        <?php
        if (sizeof($derniersArticlesPresse) > 0) {
            foreach ($derniersArticlesPresse as $articlePresse) {
                ?>
                <div class="row row-presse">
                <?php
                if (array_key_exists(HHSJ_TYPE_HHSJ_ID, $articlePresse)) {
                    ?>
                    <div class="col-xs-5 col-xs-offset-1">
                        <img src="<?php echo SRC_PATH.PathUtils::getHHSJImgHomeFile($articlePresse[HHSJ_ID]); ?>" img-modal="<?php echo SRC_PATH.PathUtils::getHHSJImgFile($articlePresse[HHSJ_ID]); ?>" title="Afficher la une" class="img-presse">
                    </div>
                    <div class="col-xs-4 col-xs-offset-1">
                        <div class="date-publication">
                            Publi&eacute; le <?php echo DateUtils::convert_sqlDate_to_slashDate($articlePresse[HHSJ_DATE]); ?>
                        </div>
                        <b><?php echo $ctrl->getLibelleTypeHandHebdo($articlePresse[HHSJ_TYPE_HHSJ_ID]); ?></b>
                        <br>
                        <?php
                        if ($articlePresse[HHSJ_TYPE_HHSJ_ID] == TYPE_HHSJ_MAGAZINE_COMPLET_ID || $articlePresse[HHSJ_TYPE_HHSJ_ID] == TYPE_HHSJ_MAGAZINE_COMPLET_ID) {
                            ?>
                            <a href="<?php echo SRC_PATH.HAND_HEBDOS_SAINT_JEANNAIS_FOLDER."/".$articlePresse[HHSJ_ID].".".HAND_HEBDOS_SAINT_JEANNAIS_COMPLET_EXTENSION; ?>" target="_blank">
                                <b>T&eacute;l&eacute;charger</b>
                            </a>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="col-xs-5 col-xs-offset-1">
                        <img src="<?php echo SRC_PATH.PathUtils::getArticlePresseImgHomeFile($articlePresse[ARTICLE_PRESSE_ID]); ?>" img-modal="<?php echo SRC_PATH.PathUtils::getArticlePresseImgFile($articlePresse[ARTICLE_PRESSE_ID]); ?>" title="Afficher l'article" class="img-presse">
                    </div>
                    <div class="col-xs-4 col-xs-offset-1">
                        <div class="date-publication">
                            Publi&eacute; le <?php echo DateUtils::convert_sqlDate_to_slashDate($articlePresse[ARTICLE_PRESSE_DATE]); ?>
                        </div>
                        <b><?php echo $ctrl->getNomJournalArticlePresse($articlePresse[ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID]); ?></b>
                        <br>
                        <a href="<?php echo $articlePresse[ARTICLE_PRESSE_URL]; ?>" target="_blank">
                            <b>Consulter l'article</b>
                        </a>
                    </div>
                    <?php
                }
                ?>
                </div>
                <?php
            }
        }
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h1>Nos partenaires</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 text-center">
                <a class="link-with-img" href="#" title="Aller sur la page des partenaires" alert-message="Prochainement...">
                    <img class="img-partenaires" src="<?php echo SRC_PATH; ?>assets/img/partenaires.png">
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h1>R&eacute;seaux sociaux</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 text-center">
                <a class="link-with-img" href="<?php echo $ressourcesExternes[RESSOURCES_EXTERNES_FACEBOOK_URL].$ressourcesExternes[RESSOURCES_EXTERNES_FACEBOOK_ID]; ?>" target="_blank" title="Nous suivre sur Facebook">
                    <img class="img-reseau-social" src="<?php echo SRC_PATH; ?>assets/img/logo-facebook.png">
                </a>
                <a class="link-with-img" href="<?php echo $ressourcesExternes[RESSOURCES_EXTERNES_INSTAGRAM_URL].$ressourcesExternes[RESSOURCES_EXTERNES_INSTAGRAM_ID]; ?>" target="_blank" title="Nous suivre sur Instagram">
                    <img class="img-reseau-social" src="<?php echo SRC_PATH; ?>assets/img/logo-instagram.png">
                </a>
                <a class="link-with-img" href="<?php echo $ressourcesExternes[RESSOURCES_EXTERNES_TWITTER_URL].$ressourcesExternes[RESSOURCES_EXTERNES_TWITTER_ID]; ?>" target="_blank" title="Nous suivre sur Twitter">
                    <img class="img-reseau-social" src="<?php echo SRC_PATH; ?>assets/img/logo-twitter.png">
                </a>
                <a class="link-with-img" href="<?php echo $ressourcesExternes[RESSOURCES_EXTERNES_YOUTUBE_URL].$ressourcesExternes[RESSOURCES_EXTERNES_YOUTUBE_ID]; ?>" target="_blank" title="Nous suivre sur YouTube">
                    <img class="img-reseau-social" src="<?php echo SRC_PATH; ?>assets/img/logo-youtube.png">
                </a>
            </div>
        </div>
        <?php
    } else {
        ?>
        <div class="row" id="news-pratique">
            <div class="col-xs-6 col-xs-offset-1">
                <h1>Derni&egrave;res nouvelles</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-xs-offset-1">
                <?php
                if (sizeof($dernieresNews) > 0) {
                    $i = 0;
                    foreach ($dernieresNews as $actu) {
                        $classBordered = "";
                        if ($i < (sizeof($dernieresNews) - 1)) {
                            $classBordered = " row-actu-bordered";
                        }
                        ?>
                        <div class="row row-actu<?php echo $classBordered; ?>">
                            <div class="col-xs-5">
                                <a class="link-with-img" href="news/actu?id=<?php echo $actu[ACTUALITE_ID]; ?>" title="Lire l'article complet">
                                    <img src="<?php echo SRC_PATH.PathUtils::getActualiteImgFile($actu[ACTUALITE_ID]); ?>" class="img-actu">
                                </a>
                                <div class="div-link-bottom-image">
                                    <a href="<?php echo ROOT_PATH; ?>news/actu?id=<?php echo $actu[ACTUALITE_ID]; ?>">Lire l'article complet</a>
                                </div>
                            </div>
                            <div class="col-xs-6 col-xs-offset-1">
                                <div class="date-publication">
                                    Publi&eacute; le <?php echo str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDateTime_to_slashDateTime($actu[ACTUALITE_DATE_HEURE])); ?>
                                </div>
                                <div class="titre-actu"><?php echo $actu[ACTUALITE_TITRE]; ?></div>
                                <div class="resume-actu">
                                    <?php echo nl2br(FileUtils::getFileContent(PathUtils::getActualiteHomeFile($actu[ACTUALITE_ID]))); ?>
                                </div>
                            </div>
                        </div>
                        <?php
                        $i++;
                    }
                }
                ?>
            </div>
            <div class="col-xs-4">
                <div id="infos-pratiques">
                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                            <h2>Nous trouver</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-5 col-xs-offset-1">
                            <a class="link-with-img" href="<?php echo $ressourcesExternes[RESSOURCES_EXTERNES_GOOGLE_MAP_URL].$gymnasePrincipal[GYMNASE_LATITUDE].",".$gymnasePrincipal[GYMNASE_LONGITUDE]; ?>" target="_blank" title="Visualiser sur Google Map">
                                <img class="img-google-map" src="<?php echo SRC_PATH; ?>assets/img/gymnase-map.png">
                            </a>
                        </div>
                        <div class="col-xs-5">
                            <?php echo $gymnasePrincipal[GYMNASE_NOM]."<br>".$gymnasePrincipal[GYMNASE_ADRESSE]."<br>".$gymnasePrincipal[GYMNASE_CODE_POSTAL]." ".$gymnasePrincipal[GYMNASE_VILLE]; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                            <h2>Nous contacter</h2>
                        </div>
                    </div>
                    <div class="form-horizontal" id="form-message-contact">
                        <div class="form-group">
                            <label for="form-message-contact__input-nom" class="col-xs-3 col-xs-offset-1 control-label required">Votre nom :</label>
                            <div class="col-xs-6 form-input" for="form-message-contact__input-nom">
                                <input type="text" class="form-control" id="form-message-contact__input-nom" required>
                            </div>
                            <div class="col-xs-1 form-icon-validation" for="form-message-contact__input-nom"></div>
                        </div>
                        <div class="form-group">
                            <label for="form-message-contact__input-email" class="col-xs-3 col-xs-offset-1 control-label required">Votre email :</label>
                            <div class="col-xs-6 form-input" for="form-message-contact__input-email">
                                <input type="text" class="form-control" id="form-message-contact__input-email" pattern="^[A-Za-z0-9._-]+@[A-Za-z0-9.-]{2,}[.][A-Za-z]{2,3}$" pattern-indication="Le format de l'email est invalide" required>
                            </div>
                            <div class="col-xs-1 form-icon-validation" for="form-message-contact__input-email"></div>
                        </div>
                        <div class="form-group">
                            <label for="form-message-contact__input-message" class="col-xs-3 col-xs-offset-1 control-label required">Message :</label>
                            <div class="col-xs-6 form-input" for="form-message-contact__input-message">
                                <textarea class="form-control" rows="3" id="form-message-contact__input-message" required></textarea>
                            </div>
                            <div class="col-xs-1 form-icon-validation" for="form-message-contact__input-message"></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-10 col-xs-offset-1 text-required">Les champs marqu&eacute;s d'une * sont obligatoires</div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 col-xs-offset-1">
                                <button id="form-message-contact__bt-envoyer" class="btn btn-default">
                                    <div class="button__icon">
                                        <span class="glyphicon glyphicon-envelope"></span>
                                    </div>
                                    <div class="button__text">Envoyer</div>
                                    <div class="loader"></div>
                                </button>
                            </div>
                            <div class="col-xs-5">
                                <div class="form-result-message"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-5 col-xs-offset-1">
                <h1>Derniers r&eacute;sultats</h1>
            </div>
            <div class="col-xs-5">
                <h1>Prochaines rencontres</h1>
            </div>
        </div>
        <div class="row" id="matchs">
            <div class="col-xs-5 col-xs-offset-1">
                <?php
                if (sizeof($dernieresRencontres) > 0) {
                    ?>
                    <table class="table-evenements">
                        <tbody>
                            <?php
                            foreach ($dernieresRencontres as $derniereRencontre) {
                                $propsDerniereRencontre = $ctrl->getRecapMatchProps($derniereRencontre);
                                ?>
                                <tr>
                                    <td class="td-go-to-competition" competition-href="<?php echo $propsDerniereRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]; ?>">
                                        <?php echo substr($propsDerniereRencontre[EVENEMENT_JOUR], 0, 3).". ".str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDateTime_to_slashDateTime($derniereRencontre[EVENEMENT_DATE_HEURE])); ?>
                                    </td>
                                    <td class="td-go-to-competition" competition-href="<?php echo $propsDerniereRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]; ?>">
                                        <?php echo $propsDerniereRencontre[MATCH_EQUIPE_DOMICILE]." VS ".$propsDerniereRencontre[MATCH_EQUIPE_EXTERIEUR]; ?>
                                    </td>
                                    <td class="resultat-<?php echo $propsDerniereRencontre[MATCH_CLASS_RESULTAT]; ?> td-go-to-competition" competition-href="<?php echo $propsDerniereRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]; ?>">
                                        <?php
                                        if ($propsDerniereRencontre[MATCH_SCORE_DOMICILE] != "" || $propsDerniereRencontre[MATCH_SCORE_EXTERIEUR] != "") {
                                            echo $propsDerniereRencontre[MATCH_TEXT_RESULTAT];
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        ?>
                        </tbody>
                    </table>
                <?php
                }
                ?>
            </div>
            <div class="col-xs-5">
                <?php
                if (sizeof($prochainesRencontres) > 0) {
                    ?>
                    <table class="table-evenements">
                        <tbody>
                            <?php
                            foreach ($prochainesRencontres as $prochaineRencontre) {
                                $propsProchaineRencontre = $ctrl->getRecapMatchProps($prochaineRencontre);
                                ?>
                                <tr>
                                    <td class="td-go-to-competition" competition-href="<?php echo $propsProchaineRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]; ?>">
                                        <?php echo substr($propsProchaineRencontre[EVENEMENT_JOUR], 0, 3).". ".str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDateTime_to_slashDateTime($prochaineRencontre[EVENEMENT_DATE_HEURE])); ?>
                                    </td>
                                    <td class="td-go-to-competition" competition-href="<?php echo $propsProchaineRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]; ?>">
                                        <?php echo $propsProchaineRencontre[MATCH_EQUIPE_DOMICILE]." VS ".$propsProchaineRencontre[MATCH_EQUIPE_EXTERIEUR]; ?>
                                    </td>
                                    <td>
                                        <?php 
                                        if ($propsProchaineRencontre[EVENEMENT_GOOGLE_MAP_URL] != "") {
                                            echo "<a href=\"".$propsProchaineRencontre[EVENEMENT_GOOGLE_MAP_URL]."\" target=\"blank\">".$prochaineRencontre[EVENEMENT_VILLE]."</a>"; 
                                        } else {
                                            echo "&nbsp;";
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        ?>
                        </tbody>
                    </table>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="row" id="presse">
            <div class="col-xs-10 col-xs-offset-1">
                <h1>Derniers articles de presse</h1>
            </div>
        </div>
        <div class="row row-presse">
        <?php
        if (sizeof($derniersArticlesPresse) > 0) {
            $i = 0;
            foreach ($derniersArticlesPresse as $articlePresse) {
                $offset = "";
                if ($i == 0) {
                    $offset = " col-xs-offset-1";
                }
                if (array_key_exists(HHSJ_TYPE_HHSJ_ID, $articlePresse)) {
                    ?>
                    <div class="col-xs-5<?php echo $offset; ?>">
                        <div class="row">
                            <div class="col-xs-6">
                                <img src="<?php echo SRC_PATH.PathUtils::getHHSJImgHomeFile($articlePresse[HHSJ_ID]); ?>" img-modal="<?php echo SRC_PATH.PathUtils::getHHSJImgFile($articlePresse[HHSJ_ID]); ?>" title="Afficher la une" class="img-presse">
                            </div>
                            <div class="col-xs-6">
                                <div class="date-publication">
                                    Publi&eacute; le <?php echo DateUtils::convert_sqlDate_to_slashDate($articlePresse[HHSJ_DATE]); ?>
                                </div>
                                <b><?php echo $ctrl->getLibelleTypeHandHebdo($articlePresse[HHSJ_TYPE_HHSJ_ID]); ?></b>
                                <br>
                                <?php
                                if ($articlePresse[HHSJ_TYPE_HHSJ_ID] == TYPE_HHSJ_MAGAZINE_COMPLET_ID || $articlePresse[HHSJ_TYPE_HHSJ_ID] == TYPE_HHSJ_MAGAZINE_COMPLET_ID) {
                                    ?>
                                    <a href="<?php echo HAND_HEBDOS_SAINT_JEANNAIS_FOLDER."/".$articlePresse[HHSJ_ID].".".HAND_HEBDOS_SAINT_JEANNAIS_COMPLET_EXTENSION; ?>" target="_blank">
                                        <b>T&eacute;l&eacute;charger</b>
                                    </a>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="col-xs-5<?php echo $offset; ?>">
                        <div class="row">
                            <div class="col-xs-6">
                                <img src="<?php echo SRC_PATH.PathUtils::getArticlePresseImgHomeFile($articlePresse[ARTICLE_PRESSE_ID]); ?>" img-modal="<?php echo SRC_PATH.PathUtils::getArticlePresseImgFile($articlePresse[ARTICLE_PRESSE_ID]); ?>" title="Afficher l'article" class="img-presse">
                            </div>
                            <div class="col-xs-6">
                                <div class="date-publication">
                                    Publi&eacute; le <?php echo DateUtils::convert_sqlDate_to_slashDate($articlePresse[ARTICLE_PRESSE_DATE]); ?>
                                </div>
                                <b><?php echo $ctrl->getNomJournalArticlePresse($articlePresse[ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID]); ?></b>
                                <br>
                                <a href="<?php echo $articlePresse[ARTICLE_PRESSE_URL]; ?>" target="_blank">
                                    <b>Consulter l'article</b>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                $i++;
            }
        }
        ?>
        </div>
        <div class="row">
            <div class="col-xs-5 col-xs-offset-1">
                <h1>Nos partenaires</h1>
            </div>
            <div class="col-xs-5">
                <h1>R&eacute;seaux sociaux</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-5 col-xs-offset-1">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <a class="link-with-img" href="#" title="Aller sur la page des partenaires" alert-message="Prochainement...">
                            <img class="img-partenaires" src="<?php echo SRC_PATH; ?>assets/img/partenaires.png">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-xs-5 text-center">
                <a class="link-with-img" href="<?php echo $ressourcesExternes[RESSOURCES_EXTERNES_FACEBOOK_URL].$ressourcesExternes[RESSOURCES_EXTERNES_FACEBOOK_ID]; ?>" target="_blank" title="Nous suivre sur Facebook">
                    <img class="img-reseau-social" src="<?php echo SRC_PATH; ?>assets/img/logo-facebook.png">
                </a>
                <a class="link-with-img" href="<?php echo $ressourcesExternes[RESSOURCES_EXTERNES_INSTAGRAM_URL].$ressourcesExternes[RESSOURCES_EXTERNES_INSTAGRAM_ID]; ?>" target="_blank" title="Nous suivre sur Instagram">
                    <img class="img-reseau-social" src="<?php echo SRC_PATH; ?>assets/img/logo-instagram.png">
                </a>
                <a class="link-with-img" href="<?php echo $ressourcesExternes[RESSOURCES_EXTERNES_TWITTER_URL].$ressourcesExternes[RESSOURCES_EXTERNES_TWITTER_ID]; ?>" target="_blank" title="Nous suivre sur Twitter">
                    <img class="img-reseau-social" src="<?php echo SRC_PATH; ?>assets/img/logo-twitter.png">
                </a>
                <a class="link-with-img" href="<?php echo $ressourcesExternes[RESSOURCES_EXTERNES_YOUTUBE_URL].$ressourcesExternes[RESSOURCES_EXTERNES_YOUTUBE_ID]; ?>" target="_blank" title="Nous suivre sur YouTube">
                    <img class="img-reseau-social" src="<?php echo SRC_PATH; ?>assets/img/logo-youtube.png">
                </a>
            </div>
        </div>
        <?php
    }
?>
</div>

<?php
require_once("webapp/views/common/footer/footer.html.php");

require_once("webapp/views/common/modals/alert/modal-alert.html.php");
require_once("webapp/views/common/modals/img/modal-img.html.php");
?>

<?php
$page->finalizePage();
?>