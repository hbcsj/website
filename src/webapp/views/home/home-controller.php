<?php
require_once("core/php/controllers/abstract-page-controller.php");
require_once("common/php/constants/config-constants.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/dao/home-message-dao.php");
require_once("common/php/dao/criticite-dao.php");
require_once("common/php/dao/evenement-dao.php");
require_once("common/php/dao/categorie-dao.php");
require_once("common/php/dao/actualite-dao.php");
require_once("common/php/dao/type-hhsj-dao.php");
require_once("common/php/dao/journal-article-presse-dao.php");
require_once("common/php/dao/ressources-externes-dao.php");
require_once("common/php/managers/evenement-manager.php");
require_once("common/php/managers/presse-manager.php");
require_once("common/php/managers/gymnase-manager.php");

class HomeCtrl extends AbstractPageCtrl {
    
    private $evenementManager;
    private $presseManager;
    private $gymnaseManager;

	private $homeMessageDAO;
	private $criticiteDAO;
	private $actualiteDAO;
    private $evenementDAO;
    private $categorieDAO;
    private $typeHHSJDAO;
    private $journalArticlePresseDAO;
    private $ressourcesExternesDAO;

    private $nbEquipesInscrites;

    public function __construct($pageName) {
		parent::__construct($pageName, null, true);

        $this->evenementManager = new EvenementManager($this->getDatabaseConnection());
        $this->presseManager = new PresseManager($this->getDatabaseConnection());
        $this->gymnaseManager = new GymnaseManager($this->getDatabaseConnection());

		$this->homeMessageDAO = new HomeMessageDAO($this->getDatabaseConnection());
		$this->criticiteDAO = new CriticiteDAO($this->getDatabaseConnection());
		$this->actualiteDAO = new ActualiteDAO($this->getDatabaseConnection());
		$this->categorieDAO = new CategorieDAO($this->getDatabaseConnection());
		$this->evenementDAO = new EvenementDAO($this->getDatabaseConnection());
		$this->typeHHSJDAO = new EvenementDAO($this->getDatabaseConnection());
		$this->journalArticlePresseDAO = new JournalArticlePresseDAO($this->getDatabaseConnection());
        $this->ressourcesExternesDAO = new RessourcesExternesDAO($this->getDatabaseConnection());

        $this->setNbEquipesInscrites();
    }
    
    public function getHomeMessages() {
        return $this->homeMessageDAO->getAll(HOME_MESSAGE_TABLE_NAME.".".HOME_MESSAGE_ID." DESC");
    }

    public function getLibelleCriticite($criticiteId) {
        $criticite = $this->criticiteDAO->getById($criticiteId);
        return $criticite[CRITICITE_LIBELLE];
    }
    
    public function getDernieresActualites() {
        return $this->actualiteDAO->getVisiblesLimited(
            $GLOBALS[CONF][NB_MAX_ACTUS_HOME], 
			ACTUALITE_TABLE_NAME.".".ACTUALITE_DATE_HEURE." DESC"
		);
    }

    public function getDernieresRencontres() {
        return $this->evenementDAO->getVisiblesAvantDateByTypeLimited(
            date(SQL_DATE_TIME_FORMAT), 
            EVENEMENT_MATCH_TYPE_ID, 
            $this->nbEquipesInscrites
        );
    }

    public function getProchainesRencontres() {
        return $this->evenementDAO->getVisiblesApresDateByTypeLimited(
            date(SQL_DATE_TIME_FORMAT), 
            EVENEMENT_MATCH_TYPE_ID, 
            $this->nbEquipesInscrites
        );
    }

    public function getRessourcesExternes() {
        return $this->ressourcesExternesDAO->getSingleton();
    }

    public function getRecapMatchProps($match) {
        return $this->evenementManager->getRecapMatchProps($match);
    }

    public function getDerniersArticlesPresse() {
        return $this->presseManager->getDerniersArticlesPresse($GLOBALS[CONF][NB_MAX_ARTICLES_PRESSE_HOME]);
    }

    public function getLibelleTypeHandHebdo($typeHhsjId) {
        $type = $this->typeHHSJDAO->getById($typeHhsjId);
        return $type[TYPE_HHSJ_LIBELLE];
    }

    public function getNomJournalArticlePresse($journalId) {
        $journal = $this->journalArticlePresseDAO->getById($journalId);
        return $journal[JOURNAL_ARTICLE_PRESSE_NOM];
    }

    public function getGymnasePrincipal() {
        return $this->gymnaseManager->getGymnasePrincipal();
    }

    private function setNbEquipesInscrites() {
        $this->nbEquipesInscrites = $this->categorieDAO->getNbEquipesInscrites()[CATEGORIE_NB_EQUIPES_INSCRITES];
    }
}

?>