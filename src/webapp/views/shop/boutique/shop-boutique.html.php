<?php
header("Location: ".ROOT_PATH."/");

/* require_once("core/php/resources/page.php");
require_once("common/php/constants/config-constants.php");
require_once("common/php/lib/path-utils.php");
require_once("lib/php/mobile-detect.php");

$page = new Page(
    "webapp/views/shop/boutique/", 
    "shop-boutique", 
    "ShopBoutiqueCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/shop/common/nav/nav.html.php");
require_once("webapp/views/shop/boutique/sub-menu/sub-menu-shop-boutique.html.php");

$categorie = $ctrl->getCategorie();
$produits = $ctrl->getProduits();
?>

<div class="container-fluid animated fadeIn">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
            <h1>
                <?php echo $categorie[CATEGORIE_PRODUIT_BOUTIQUE_NOM]; ?>
                <button class="btn btn-default" id="bt-panier">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-shopping-cart"></span>
                    </div>
                    <div class="button__text">
                        Votre panier
                        <span class="badge"></span>
                    </div>
                </button>
            </h1>
        </div>
    </div>
    <?php
    if (sizeof($produits) > 0) {
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <div class="row">
                    <?php
                    $i = 0;
                    foreach ($produits as $produit) {
                        $prix = $ctrl->getPrixProduit($produit[PRODUIT_BOUTIQUE_ID]);
                        if (sizeof($prix) > 0) {
                            if ($mobileDetect->isMobile() || $mobileDetect->isTablet()) {
                                ?>
                                <div class="col-xs-6 col-produit" id-produit="<?php echo $produit[PRODUIT_BOUTIQUE_ID]; ?>">
                                    <div class="header-produit">
                                        <span class="nom-produit"><?php echo $produit[PRODUIT_BOUTIQUE_NOM]; ?></span>
                                        <br>
                                        <?php
                                        if ($produit[PRODUIT_BOUTIQUE_INDICATION] != "") {
                                            ?>
                                            <span class="indication-produit"><?php echo $produit[PRODUIT_BOUTIQUE_INDICATION]; ?></span>
                                            <?php
                                        }
                                        if ($produit[PRODUIT_BOUTIQUE_MARQUE_PRODUIT_BOUTIQUE_ID] != null) {
                                            ?>
                                            <img src="<?php echo SRC_PATH.PathUtils::getMarqueProduitBoutiqueImgFile($produit[PRODUIT_BOUTIQUE_MARQUE_PRODUIT_BOUTIQUE_ID]); ?>" class="img-marque">
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <img src="<?php echo SRC_PATH.PathUtils::getProduitBoutiqueImgFile($produit[PRODUIT_BOUTIQUE_ID]); ?>" class="img-produit">
                                    <?php
                                    if ($ctrl->isProduitFlocable($produit)) {
                                        ?>
                                        <div class="personnalisable-produit">Personnalisable</div>
                                        <?php
                                    }
                                    ?>
                                    <div class="prix-produit">
                                        <div class="label-prix">
                                            <?php
                                            if ($ctrl->isProduitFlocable($produit) || sizeof($prix) > 1) {
                                                echo "A partir de";
                                            }
                                            ?>
                                        </div>
                                        <div class="prix">
                                            <?php 
                                            echo $prix[0][PRIX_PRODUIT_BOUTIQUE_PRIX]." &euro;"; 
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                if ((($i + 1) % 2) == 0) {
                                    ?>
                                    </div><div class="row">
                                    <?php
                                }
                            } else {
                                ?>
                                <div class="col-xs-4 col-produit" id-produit="<?php echo $produit[PRODUIT_BOUTIQUE_ID]; ?>">
                                    <div class="header-produit">
                                        <span class="nom-produit"><?php echo $produit[PRODUIT_BOUTIQUE_NOM]; ?></span>
                                        <br>
                                        <?php
                                        if ($produit[PRODUIT_BOUTIQUE_INDICATION] != "") {
                                            ?>
                                            <span class="indication-produit"><?php echo $produit[PRODUIT_BOUTIQUE_INDICATION]; ?></span>
                                            <?php
                                        }
                                        if ($produit[PRODUIT_BOUTIQUE_MARQUE_PRODUIT_BOUTIQUE_ID] != null) {
                                            ?>
                                            <img src="<?php echo SRC_PATH.PathUtils::getMarqueProduitBoutiqueImgFile($produit[PRODUIT_BOUTIQUE_MARQUE_PRODUIT_BOUTIQUE_ID]); ?>" class="img-marque">
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <img src="<?php echo SRC_PATH.PathUtils::getProduitBoutiqueImgFile($produit[PRODUIT_BOUTIQUE_ID]); ?>" class="img-produit">
                                    <?php
                                    if ($ctrl->isProduitFlocable($produit)) {
                                        ?>
                                        <div class="personnalisable-produit">PERSONNALISABLE</div>
                                        <?php
                                    }
                                    ?>
                                    <div class="prix-produit">
                                        <div class="label-prix">
                                            <?php
                                            if ($ctrl->isProduitFlocable($produit) || sizeof($prix) > 1) {
                                                echo "A partir de";
                                            }
                                            ?>
                                        </div>
                                        <div class="prix">
                                            <?php 
                                            echo $prix[0][PRIX_PRODUIT_BOUTIQUE_PRIX]." &euro;"; 
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                if ((($i + 1) % 3) == 0) {
                                    ?>
                                    </div><div class="row">
                                    <?php
                                }
                            }
                            $i++;
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</div>

<?php
require_once("webapp/views/common/footer/footer.html.php");

require_once("webapp/views/common/modals/alert/modal-alert.html.php");

require_once("webapp/views/shop/boutique/modals/save-article-panier/modal-save-article-panier.html.php");
require_once("webapp/views/shop/common/modals/panier/modal-panier.html.php");
?>

<?php
$page->finalizePage(); */
?>