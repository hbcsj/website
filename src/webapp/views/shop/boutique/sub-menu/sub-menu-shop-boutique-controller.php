<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/categorie-produit-boutique-dao.php");

class SubMenuShopBoutiqueCtrl extends AbstractViewCtrl {

	private $categorieProduitBoutiqueDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		$this->categorieProduitBoutiqueDAO = new CategorieProduitBoutiqueDAO($this->getDatabaseConnection());
	}

    public function getCategories() {
        return $this->categorieProduitBoutiqueDAO->getAll(CATEGORIE_PRODUIT_BOUTIQUE_TABLE_NAME.".".CATEGORIE_PRODUIT_BOUTIQUE_ID);
    }
}

?>