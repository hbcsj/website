<?php
require_once("core/php/resources/view.php");
require_once("lib/php/mobile-detect.php");

$view = new View(
    "webapp/views/shop/boutique/sub-menu/", 
    "sub-menu-shop-boutique", 
    "SubMenuShopBoutiqueCtrl"
);
$subMenuShopBoutiqueCtrl = $view->getController();

$mobileDetect = new Mobile_Detect();
$categories = $subMenuShopBoutiqueCtrl->getCategories();
?>

<div class="sub-menu">
    <?php
    if (sizeof($categories) > 0) {
        foreach ($categories as $categorie) {
            ?>
            <a href="<?php echo ROOT_PATH; ?>shop/boutique?categorie=<?php echo $categorie[CATEGORIE_PRODUIT_BOUTIQUE_ID]; ?>" page="categorie-<?php echo $categorie[CATEGORIE_PRODUIT_BOUTIQUE_ID]; ?>"><?php echo $categorie[CATEGORIE_PRODUIT_BOUTIQUE_NOM]; ?></a>
            <?php
        }
    }
    ?>
</div>
