<?php
require_once("core/php/controllers/abstract-page-controller.php");
require_once("common/php/dao/categorie-produit-boutique-dao.php");
require_once("common/php/dao/produit-boutique-dao.php");
require_once("common/php/dao/prix-produit-boutique-dao.php");

class ShopBoutiqueCtrl extends AbstractPageCtrl {

	private $categorieProduitBoutiqueDAO;
	private $produitBoutiqueDAO;
	private $prixProduitBoutiqueDAO;

	private $categorieProduitBoutique;
	
    public function __construct($pageName) {
		parent::__construct($pageName, null, true);

		$this->categorieProduitBoutiqueDAO = new CategorieProduitBoutiqueDAO($this->getDatabaseConnection());
		$this->produitBoutiqueDAO = new ProduitBoutiqueDAO($this->getDatabaseConnection());
		$this->prixProduitBoutiqueDAO = new PrixProduitBoutiqueDAO($this->getDatabaseConnection());

		if (!HTTPUtils::paramExists(GET, "categorie")) {
			$firstCategorie = $this->categorieProduitBoutiqueDAO->getAll(PRODUIT_BOUTIQUE_ID)[0];
			header("Location: ".ROOT_PATH."shop/boutique?categorie=".$firstCategorie[CATEGORIE_PRODUIT_BOUTIQUE_ID]);
		} else {
			$this->checkRequest();
		}
	}

	private function checkRequest() {
		$this->checkCategorie();
	}

	public function checkCategorie() {
		$this->categorieProduitBoutique = $this->categorieProduitBoutiqueDAO->getById($_GET["categorie"]);
			
		if ($this->categorieProduitBoutique == null) {
			$this->sendCheckError(
				HTTP_404, 
				"La categorie produit boutique (idCategorieProduitBoutique = '".$_GET["categorie"]."') n'existe pas", 
				"404"
			);
		}
	}

	public function getCategorie() {
		return $this->categorieProduitBoutique;
	}

	public function getProduits() {
		return $this->produitBoutiqueDAO->getAllByParams(
			null, 
			null, 
			null, 
			1, 
			null, 
			null, 
			$this->categorieProduitBoutique[CATEGORIE_PRODUIT_BOUTIQUE_ID], 
			PRODUIT_BOUTIQUE_TABLE_NAME.".".PRODUIT_BOUTIQUE_POSITION_DANS_CATEGORIE.", ".PRODUIT_BOUTIQUE_TABLE_NAME.".".PRODUIT_BOUTIQUE_NOM.", ".PRODUIT_BOUTIQUE_TABLE_NAME.".".PRODUIT_BOUTIQUE_INDICATION
		);
	}

	public function isProduitFlocable($produit) {
		return (
			($produit[PRODUIT_BOUTIQUE_FLOCABLE_NOM] == 1) ||
			($produit[PRODUIT_BOUTIQUE_FLOCABLE_NUMERO_AVANT] == 1) ||
			($produit[PRODUIT_BOUTIQUE_FLOCABLE_NUMERO_ARRIERE] == 1) ||
			($produit[PRODUIT_BOUTIQUE_FLOCABLE_INITIALES] == 1)
		);
	}

	public function getPrixProduit($produitId) {
		return $this->prixProduitBoutiqueDAO->getByProduitId($produitId);
	}
}

?>