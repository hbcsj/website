ShopBoutiqueController = {};

ShopBoutiqueController.control_Produits = function() {
    $('.col-produit').unbind('click');
    $('.col-produit').click(function() {
        var idProduit = $(this).attr('id-produit');
        open_ModalSaveArticlePanierProduit(idProduit);
    });

    function open_ModalSaveArticlePanierProduit(idProduit) {
        var params = {
            idProduitBoutique: idProduit
        };
        AjaxUtils.loadView(
            'shop/boutique/modals/save-article-panier/modal-save-article-panier-content',
            '#modal-save-article-panier-content',
            params,
            function() {
                calculPrix();
                control_NbArticles();
                control_Taille();
                control_Flocage('nom');
                control_Flocage('numero-arriere');
                control_Flocage('numero-avant');
                control_Flocage('numero-initiales');
                onClick_btAjouter(idProduit);
            },
            '#loader-page'
        );
        $('#modal-save-article-panier').modal('show');

        function control_NbArticles() {
            $('#form-save-article-panier__input-nb-articles').unbind('change');
            $('#form-save-article-panier__input-nb-articles').change(function() {
                calculPrix();
            });
        }

        function control_Taille() {
            $('#form-save-article-panier__input-prix-taille').unbind('change');
            $('#form-save-article-panier__input-prix-taille').change(function() {
                calculPrix();
            });
        }

        function control_Flocage(flocage) {
            $('#form-save-article-panier__input-flocage-'+flocage).unbind('change');
            $('#form-save-article-panier__input-flocage-'+flocage).change(function() {
                calculPrix();
            });
        }

        function calculPrix() {
            var prixUnitaire = 0;
            var prixUnitaireFournisseur = 0;

            if (Utils.exists('#form-save-article-panier__input-prix-base')) {
                prixUnitaire += parseFloat($('#form-save-article-panier__input-prix-base').val());
                prixUnitaireFournisseur += parseFloat($('#form-save-article-panier__input-prix-fournisseur-base').val());
            } else {
                var prixUnitaireStr = $('#form-save-article-panier__input-prix-taille').val();
                var prixUnitaireFournisseurStr = $('#form-save-article-panier__input-prix-taille').find('option:selected').attr('prix-fournisseur');
                prixUnitaire += (prixUnitaireStr != '') ? parseFloat(prixUnitaireStr) : 0;
                prixUnitaireFournisseur += (prixUnitaireFournisseurStr != '') ? parseFloat(prixUnitaireFournisseurStr) : 0;
            }

            if (Utils.exists('#form-save-article-panier__input-flocage-nom')) {
                if ($.trim($('#form-save-article-panier__input-flocage-nom').val()) != '') {
                    prixUnitaire += parseFloat($('#form-save-article-panier__input-prix-flocage-nom').val());
                    prixUnitaireFournisseur += parseFloat($('#form-save-article-panier__input-prix-flocage-fournisseur-nom').val());
                }
            }

            if (Utils.exists('#form-save-article-panier__input-flocage-numero-arriere')) {
                if ($.trim($('#form-save-article-panier__input-flocage-numero-arriere').val()) != '') {
                    prixUnitaire += parseFloat($('#form-save-article-panier__input-prix-flocage-numero-arriere').val());
                    prixUnitaireFournisseur += parseFloat($('#form-save-article-panier__input-prix-flocage-fournisseur-numero-arriere').val());
                }
            }

            if (Utils.exists('#form-save-article-panier__input-flocage-numero-avant')) {
                if ($.trim($('#form-save-article-panier__input-flocage-numero-avant').val()) != '') {
                    prixUnitaire += parseFloat($('#form-save-article-panier__input-prix-flocage-numero-avant').val());
                    prixUnitaireFournisseur += parseFloat($('#form-save-article-panier__input-prix-flocage-fournisseur-numero-avant').val());
                }
            }

            if (Utils.exists('#form-save-article-panier__input-flocage-initiales')) {
                if ($.trim($('#form-save-article-panier__input-flocage-initiales').val()) != '') {
                    prixUnitaire += parseFloat($('#form-save-article-panier__input-prix-flocage-initiales').val());
                    prixUnitaireFournisseur += parseFloat($('#form-save-article-panier__input-prix-flocage-fournisseur-initiales').val());
                }
            }

            var nbArticles = ($('#form-save-article-panier__input-nb-articles').val() != '') ? parseFloat($('#form-save-article-panier__input-nb-articles').val()) : 0;
            var prix = nbArticles * prixUnitaire;
            var prixFournisseur = nbArticles * prixUnitaireFournisseur;

            $('#prix-total').html(prix+' &euro;');
            $('#form-save-article-panier__input-prix-total').val(prix);
            $('#form-save-article-panier__input-prix-fournisseur-total').val(prixFournisseur);
        }

        function onClick_btAjouter(idProduit) {
            $('#modal-save-article-panier__bt-ajouter').unbind('click');
            $('#modal-save-article-panier__bt-ajouter').click(function() {
                FormUtils.hideFormErrors('#modal-save-article-panier #form-save-article-panier');
                ViewUtils.unsetHTMLAndHide('#modal-save-article-panier .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('#modal-save-article-panier__bt-ajouter', '#modal-save-article-panier__bt-ajouter .loader');

                var formValidation = FormUtils.validateForm('#modal-save-article-panier #form-save-article-panier');
                
                if (formValidation) {
                    var inputNbArticles = $('#form-save-article-panier__input-nb-articles').val();

                    var inputTaille = null;
                    if (Utils.exists('#form-save-article-panier__input-prix-taille')) {
                        inputTaille = $('#form-save-article-panier__input-prix-taille').find('option:selected').html();
                    }

                    var inputCouleur = null;
                    if (Utils.exists('#form-save-article-panier__input-couleur')) {
                        inputCouleur = $('#form-save-article-panier__input-couleur').val();
                    }

                    var inputFlocageNom = null;
                    if (Utils.exists('#form-save-article-panier__input-flocage-nom')) {
                        inputFlocageNom = $('#form-save-article-panier__input-flocage-nom').val();
                    }

                    var inputFlocageNumeroAvant = null;
                    if (Utils.exists('#form-save-article-panier__input-flocage-numero-avant')) {
                        inputFlocageNumeroAvant = $('#form-save-article-panier__input-flocage-numero-avant').val();
                    }

                    var inputFlocageNumeroArriere = null;
                    if (Utils.exists('#form-save-article-panier__input-flocage-numero-arriere')) {
                        inputFlocageNumeroArriere = $('#form-save-article-panier__input-flocage-numero-arriere').val();
                    }

                    var inputFlocageInitiales = null;
                    if (Utils.exists('#form-save-article-panier__input-flocage-initiales')) {
                        inputFlocageInitiales = $('#form-save-article-panier__input-flocage-initiales').val();
                    }

                    var inputPrixTotal = $('#form-save-article-panier__input-prix-total').val();
                    var inputPrixFournisseurTotal = $('#form-save-article-panier__input-prix-fournisseur-total').val();

                    saveCommandeBoutiquePanier(idProduit, inputNbArticles, inputTaille, inputCouleur, 
                        inputFlocageNom, inputFlocageNumeroAvant, inputFlocageNumeroArriere, inputFlocageInitiales, 
                        inputPrixTotal, inputPrixFournisseurTotal);
                } else {
                    ViewUtils.activeButtonAndHideLoader('#modal-save-article-panier__bt-ajouter', '#modal-save-article-panier__bt-ajouter .loader');
                }
            });

            function saveCommandeBoutiquePanier(idProduit, inputNbArticles, inputTaille, inputCouleur, 
                inputFlocageNom, inputFlocageNumeroAvant, inputFlocageNumeroArriere, inputFlocageInitiales, 
                inputPrixTotal, inputPrixFournisseurTotal) {
                AjaxUtils.callService(
                    'shop-panier', 
                    'saveCommandeBoutique', 
                    AjaxUtils.dataTypes.json, 
                    AjaxUtils.requestTypes.post, 
                    null, 
                    {
                        produitBoutiqueId: idProduit,
                        nbArticles: inputNbArticles,
                        taille: inputTaille,
                        couleur: inputCouleur,
                        flocageNom: inputFlocageNom,
                        flocageNumeroAvant: inputFlocageNumeroAvant,
                        flocageNumeroArriere: inputFlocageNumeroArriere,
                        flocageInitiales: inputFlocageInitiales,
                        prixTotal: inputPrixTotal,
                        prixFournisseurTotal: inputPrixFournisseurTotal
                    }, 
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-save-article-panier .form-result-message', 'Articles ajout&eacute;s au panier', FormUtils.resultsTypes.ok);
                            $('#modal-save-article-panier__bt-ajouter .loader').hide();
                            ShopController.load_MontantPanier();
                        } else {
                            ViewUtils.activeButtonAndHideLoader('#modal-save-article-panier__bt-ajouter', '#modal-save-article-panier__bt-ajouter .loader');
                            FormUtils.displayFormResultMessage('#modal-save-article-panier .form-result-message', 'Erreur durant l\'ajout', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('#modal-save-article-panier__bt-ajouter', '#modal-save-article-panier__bt-ajouter .loader');
                        FormUtils.displayFormResultMessage('#modal-save-article-panier .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            }
        }
    }
};

initializePage = function() {
    ShopController.initializePage('boutique');
    GlobalController.selectSubMenuLink('categorie-'+Utils.getUrlParams()['categorie']);

    ShopBoutiqueController.control_Produits();
};