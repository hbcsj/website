<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/categorie-produit-boutique-dao.php");
require_once("common/php/dao/produit-boutique-dao.php");
require_once("common/php/dao/prix-produit-boutique-dao.php");
require_once("common/php/dao/tailles-possibles-produit-boutique-dao.php");

class ModalSavePlacesPanierCtrl extends AbstractViewCtrl {
	
	private $produitBoutique;

	private $categorieProduitBoutiqueDAO;
	private $produitBoutiqueDAO;
	private $prixProduitBoutiqueDAO;
	private $taillesPossiblesProduitBoutiqueDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idProduitBoutique" => $_GET["idProduitBoutique"]
		), true);
		
		$this->categorieProduitBoutiqueDAO = new CategorieProduitBoutiqueDAO($this->getDatabaseConnection());
		$this->produitBoutiqueDAO = new ProduitBoutiqueDAO($this->getDatabaseConnection());
		$this->prixProduitBoutiqueDAO = new PrixProduitBoutiqueDAO($this->getDatabaseConnection());
		$this->taillesPossiblesProduitBoutiqueDAO = new TaillesPossiblesProduitBoutiqueDAO($this->getDatabaseConnection());

		$this->checkRequest();
	}

	private function checkRequest() {
		$checkParams = $this->checkParams(array(
			array(GET, "idProduitBoutique")
		));
		if ($checkParams) {
			$this->checkIdProduitBoutique();
		}
	}

	private function checkIdProduitBoutique() {
		$this->produitBoutique = $this->produitBoutiqueDAO->getById($_GET["idProduitBoutique"]);
			
		if ($this->produitBoutique == null) {
			$this->sendCheckError(
				HTTP_404, 
				"Le produit boutique (idProduitBoutique = '".$_GET["idProduitBoutique"]."') n'existe pas", 
				"webapp/views/common/error/404/404.html.php"
			);
		}
	}

	public function getProduitBoutique() {
		return $this->produitBoutique;
	}

	public function getNomCategorie($categorieId) {
		$categorie = $this->categorieProduitBoutiqueDAO->getById($categorieId);
		return $categorie[CATEGORIE_PRODUIT_BOUTIQUE_NOM];
	}

	public function getPrixProduit($produitId) {
		return $this->prixProduitBoutiqueDAO->getByProduitId($produitId);
	}

	public function getTaillesPossibles($taillesPossiblesId) {
		$tailles = $this->taillesPossiblesProduitBoutiqueDAO->getById($taillesPossiblesId);
		return explode(TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TAILLES_SEPARATOR, $tailles[TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TAILLES_POSSIBLES]);
	}

	public function getPrixByTaille($tailleChoisie, $prix) {
		$prixByTaille = array();
		if (sizeof($prix) > 1) {
			foreach ($prix as $prixItem) {
				if ($prixItem[PRIX_PRODUIT_BOUTIQUE_TAILLE] == $tailleChoisie) {
					$prixByTaille[PRIX_PRODUIT_BOUTIQUE_PRIX] = $prixItem[PRIX_PRODUIT_BOUTIQUE_PRIX];
					$prixByTaille[PRIX_PRODUIT_BOUTIQUE_PRIX_FOURNISSEUR] = $prixItem[PRIX_PRODUIT_BOUTIQUE_PRIX_FOURNISSEUR];
				}
			}
		} else {
			$prixByTaille[PRIX_PRODUIT_BOUTIQUE_PRIX] = $prix[0][PRIX_PRODUIT_BOUTIQUE_PRIX];
			$prixByTaille[PRIX_PRODUIT_BOUTIQUE_PRIX_FOURNISSEUR] = $prix[0][PRIX_PRODUIT_BOUTIQUE_PRIX_FOURNISSEUR];
		}
		return $prixByTaille;
	}
}

?>