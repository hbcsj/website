<?php
require_once("core/php/resources/view.php");
require_once("common/php/constants/config-constants.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("lib/php/mobile-detect.php");

$view = new View(
    "webapp/views/shop/boutique/modals/save-article-panier/", 
    "modal-save-article-panier", 
    "ModalSavePlacesPanierCtrl"
);
$ctrl = $view->getController();

$mobileDetect = new Mobile_Detect();
$produit = $ctrl->getProduitBoutique();
$prix = $ctrl->getPrixProduit($produit[PRODUIT_BOUTIQUE_ID]);
?>

<div class="modal-header">
    <div class="modal-title">
        Commande de <?php echo $produit[PRODUIT_BOUTIQUE_NOM]; ?>
    </div>
</div>
<div class="modal-body">
    <div class="row row-infos-produit">
        <div class="col-xs-5 col-xs-offset-1">
            <div id="nom-categorie">
                <?php echo $ctrl->getNomCategorie($produit[PRODUIT_BOUTIQUE_CATEGORIE_PRODUIT_BOUTIQUE_ID]); ?>
            </div>
            <div id="nom-produit">
                <?php echo $produit[PRODUIT_BOUTIQUE_NOM]; ?>
            </div>
            <?php
            if ($produit[PRODUIT_BOUTIQUE_INDICATION] != "") {
                ?>
                <div id="indication-produit">
                    <?php echo $produit[PRODUIT_BOUTIQUE_INDICATION]; ?>
                </div>
                <?php
            }
            ?>
        </div>
        <div class="col-xs-6 text-center">
            <?php
            if ($produit[PRODUIT_BOUTIQUE_MARQUE_PRODUIT_BOUTIQUE_ID] != null) {
                ?>
                <img src="<?php echo SRC_PATH.PathUtils::getMarqueProduitBoutiqueImgFile($produit[PRODUIT_BOUTIQUE_MARQUE_PRODUIT_BOUTIQUE_ID]); ?>" id="img-marque">
                <?php
            }
            ?>
        </div>
    </div>
    <div class="row row-infos-produit">
        <div class="col-xs-10 col-xs-offset-1">
            <div id="img-produit">
                <img src="<?php echo SRC_PATH.PathUtils::getProduitBoutiqueImgFile($produit[PRODUIT_BOUTIQUE_ID]); ?>">
            </div>
            <div id="description-produit">
                <?php echo FileUtils::getFileContent(PathUtils::getProduitBoutiqueDescriptionFile($produit[PRODUIT_BOUTIQUE_ID])); ?>
            </div>
            <?php
            if ($produit[PRODUIT_BOUTIQUE_FLOCAGE_LOGO_INCLUS] == 1) {
                ?>
                <div id="flocage-logo-produit">Flocage du logo inclus</div>
                <?php
            }
            $taillesProduitFile = PathUtils::getProduitBoutiqueTaillesFile($produit[PRODUIT_BOUTIQUE_TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_ID]);
            if (file_exists($taillesProduitFile)) {
                ?>
                <div id="tailles-produit">
                    <?php echo FileUtils::getFileContent($taillesProduitFile); ?>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
    <div class="form-horizontal" id="form-save-article-panier">
        <div class="form-group">
            <label for="form-save-article-panier__input-nb-articles" class="col-xs-5 col-lg-3 control-label required">Nb. articles :</label>
            <div class="col-xs-3 col-lg-2 form-input" for="form-save-article-panier__input-nb-articles">
                <select class="form-control" id="form-save-article-panier__input-nb-articles" required>
                    <option value=""></option>
                    <?php
                    for ($i = 1 ; $i <= $GLOBALS[CONF][NB_MAX_ARTICLES_COMMANDE_SHOP_BOUTIQUE] ; $i++) {
                        ?>
                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                        <?php
                    }
                    ?>
                </select>
                <input type="hidden" id="form-save-article-panier__input-prix-total" value="">
                <input type="hidden" id="form-save-article-panier__input-prix-fournisseur-total" value="">
            </div>
            <div class="col-xs-1 form-icon-validation" for="form-save-article-panier__input-nb-articles"></div>
            <?php
            if (!$mobileDetect->isMobile() && !$mobileDetect->isTablet()) {
                ?>
                <div class="col-lg-4 col-lg-offset-1">
                    <div id="prix-total">0 &euro;</div>
                </div>
                <?php
            }
            ?>
        </div>
        <?php
        if ($produit[PRODUIT_BOUTIQUE_TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_ID] != PRODUIT_BOUTIQUE_TAILLE_UNIQUE_ID) {
            $tailles = $ctrl->getTaillesPossibles($produit[PRODUIT_BOUTIQUE_TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_ID]);
            ?>
            <div class="form-group">
                <label for="form-save-article-panier__input-prix-taille" class="col-xs-5 col-lg-3 control-label required">Taille :</label>
                <div class="col-xs-3 col-lg-2 form-input" for="form-save-article-panier__input-prix-taille">
                    <select class="form-control" id="form-save-article-panier__input-prix-taille" required>
                        <option value=""></option>
                        <?php
                        if (sizeof($tailles) > 0) {
                            foreach ($tailles as $taille) {
                                $prixBase = $ctrl->getPrixByTaille($taille, $prix);
                                ?>
                                <option value="<?php echo $prixBase[PRIX_PRODUIT_BOUTIQUE_PRIX]; ?>" prix-fournisseur="<?php echo $prixBase[PRIX_PRODUIT_BOUTIQUE_PRIX_FOURNISSEUR]; ?>"><?php echo $taille; ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-article-panier__input-prix-taille"></div>
            </div>
            <?php
        } else {
            ?>
            <input type="hidden" id="form-save-article-panier__input-prix-base" value="<?php echo $prix[0][PRIX_PRODUIT_BOUTIQUE_PRIX]; ?>">
            <input type="hidden" id="form-save-article-panier__input-prix-fournisseur-base" value="<?php echo $prix[0][PRIX_PRODUIT_BOUTIQUE_PRIX_FOURNISSEUR]; ?>">
            <?php
        }
        if ($produit[PRODUIT_BOUTIQUE_COULEURS] != "") {
            $couleurs = explode(PRODUIT_BOUTIQUE_COULEURS_REFERENCES_SEPARATOR, $produit[PRODUIT_BOUTIQUE_COULEURS]);
            ?>
            <div class="form-group">
                <label for="form-save-article-panier__input-couleur" class="col-xs-5 col-lg-3 control-label required">Couleur :</label>
                <div class="col-xs-3 col-lg-2 form-input" for="form-save-article-panier__input-couleur">
                    <select class="form-control" id="form-save-article-panier__input-couleur" required>
                        <option value=""></option>
                        <?php
                        foreach ($couleurs as $couleur) {
                            ?>
                            <option value="<?php echo $couleur; ?>"><?php echo $couleur; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-article-panier__input-couleur"></div>
            </div>
            <?php
        }
        if ($produit[PRODUIT_BOUTIQUE_FLOCABLE_NOM]) {
            ?>
            <div class="form-group">
                <label for="form-save-article-panier__input-flocage-nom" class="col-xs-5 col-lg-3 control-label">
                    Flocage nom :
                    <div class="label-indication"><?php echo $GLOBALS[CONF][NB_MAX_CARACTERES_FLOCAGE_NOM];?> caract. max.</div>
                </label>
                <div class="col-xs-3 col-lg-2 form-input" for="form-save-article-panier__input-flocage-nom">
                    <input class="form-control" id="form-save-article-panier__input-flocage-nom" pattern="^[A-Za-z0-9àâäçéèêëìîïòôöùüÿ.-\\s]{1,<?php echo $GLOBALS[CONF][NB_MAX_CARACTERES_FLOCAGE_NOM];?>}$" pattern-indication="Le flocage n'est pas valide">
                    <input type="hidden" id="form-save-article-panier__input-prix-flocage-nom" value="<?php echo $produit[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_NOM]; ?>">
                    <input type="hidden" id="form-save-article-panier__input-prix-flocage-fournisseur-nom" value="<?php echo $produit[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_FOURNISSEUR_NOM]; ?>">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-article-panier__input-flocage-nom"></div>
                <div class="col-xs-2 col-lg-1">
                    <span class="prix-flocage"><?php echo $produit[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_NOM]; ?> &euro;</span>
                </div>
            </div>
            <?php
        }
        if ($produit[PRODUIT_BOUTIQUE_FLOCABLE_NUMERO_ARRIERE]) {
            ?>
            <div class="form-group">
                <label for="form-save-article-panier__input-flocage-numero-arriere" class="col-xs-5 col-lg-3 control-label">Flocage n&deg; arri&egrave;re :</label>
                <div class="col-xs-3 col-lg-2 form-input" for="form-save-article-panier__input-flocage-numero-arriere">
                    <input class="form-control" id="form-save-article-panier__input-flocage-numero-arriere" pattern="^[0-9]{1,2}$" pattern-indication="Le flocage n'est pas valide">
                    <input type="hidden" id="form-save-article-panier__input-prix-flocage-numero-arriere" value="<?php echo $produit[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_NUMERO_ARRIERE]; ?>">
                    <input type="hidden" id="form-save-article-panier__input-prix-flocage-fournisseur-numero-arriere" value="<?php echo $produit[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_FOURNISSEUR_NUMERO_ARRIERE]; ?>">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-article-panier__input-flocage-numero-arriere"></div>
                <div class="col-xs-2 col-lg-1">
                    <span class="prix-flocage"><?php echo $produit[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_NUMERO_ARRIERE]; ?> &euro;</span>
                </div>
            </div>
            <?php
        }
        if ($produit[PRODUIT_BOUTIQUE_FLOCABLE_NUMERO_AVANT]) {
            ?>
            <div class="form-group">
                <label for="form-save-article-panier__input-flocage-numero-avant" class="col-xs-5 col-lg-3 control-label">Flocage n&deg; avant :</label>
                <div class="col-xs-3 col-lg-2 form-input" for="form-save-article-panier__input-flocage-numero-avant">
                    <input class="form-control" id="form-save-article-panier__input-flocage-numero-avant" pattern="^[0-9]{1,2}$" pattern-indication="Le flocage n'est pas valide">
                    <input type="hidden" id="form-save-article-panier__input-prix-flocage-numero-avant" value="<?php echo $produit[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_NUMERO_AVANT]; ?>">
                    <input type="hidden" id="form-save-article-panier__input-prix-flocage-fournisseur-numero-avant" value="<?php echo $produit[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_FOURNISSEUR_NUMERO_AVANT]; ?>">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-article-panier__input-flocage-numero-avant"></div>
                <div class="col-xs-2 col-lg-1">
                    <span class="prix-flocage"><?php echo $produit[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_NUMERO_AVANT]; ?> &euro;</span>
                </div>
            </div>
            <?php
        }
        if ($produit[PRODUIT_BOUTIQUE_FLOCABLE_INITIALES]) {
            ?>
            <div class="form-group">
                <label for="form-save-article-panier__input-flocage-initiales" class="col-xs-5 col-lg-3 control-label">Flocage initiales :</label>
                <div class="col-xs-3 col-lg-2 form-input" for="form-save-article-panier__input-flocage-initiales">
                    <input class="form-control" id="form-save-article-panier__input-flocage-initiales" pattern="^[A-Za-z0-9]{1,3}$" pattern-indication="Le flocage n'est pas valide">
                    <input type="hidden" id="form-save-article-panier__input-prix-flocage-initiales" value="<?php echo $produit[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_INITIALES]; ?>">
                    <input type="hidden" id="form-save-article-panier__input-prix-flocage-fournisseur-initiales" value="<?php echo $produit[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_FOURNISSEUR_INITIALES]; ?>">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-article-panier__input-flocage-initiales"></div>
                <div class="col-xs-2 col-lg-1">
                    <span class="prix-flocage"><?php echo $produit[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_INITIALES]; ?> &euro;</span>
                </div>
            </div>
            <?php
        }
        ?>
        <?php
        if ($mobileDetect->isMobile() || $mobileDetect->isTablet()) {
            ?>
            <div class="row">
                <div class="col-xs-12">
                    <div id="prix-total">0 &euro;</div>
                </div>
            </div>
            <?php
        }
        ?>
        <div class="row">
            <div class="col-xs-12 text-required text-right">Les champs marqu&eacute;s d'une * sont obligatoires</div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <div class="row">
        <div class="col-xs-8">
            <div class="form-result-message"></div>
        </div>
        <div class="col-xs-4 text-right">
            <button id="modal-save-article-panier__bt-ajouter" class="btn btn-default">
                <div class="button__icon">
                    <span class="glyphicon glyphicon-plus"></span>
                </div>
                <div class="button__text">Ajouter</div>
                <div class="loader"></div>
            </button>
        </div>
    </div>
</div>
