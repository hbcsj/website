<nav>
    <div id="nav-logo">
        <img src="<?php echo SRC_PATH; ?>assets/img/logo-shop.png">
    </div>
    <div class="nav-links">
        <!-- a href="<?php //echo ROOT_PATH; ?>shop/boutique" page="boutique">Boutique</a> -->
        <a href="https://boutique.osports.fr/hbc-saint-jean" page="boutique">Boutique</a>
        <a href="<?php echo ROOT_PATH; ?>shop/billetterie" page="billetterie">Billetterie</a> 
    </div>
    <div class="loader" id="loader-page"></div>
</nav>
