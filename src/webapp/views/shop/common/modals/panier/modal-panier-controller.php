<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/shop-utils.php");
require_once("common/php/lib/date-utils.php");

class ModalPanierCtrl extends AbstractViewCtrl {

	public function __construct($viewName) {
        parent::__construct($viewName, null, true);
        
        checkShopPanierInitialized();
    }
    
    public function getLibelleArticlePanier($articlePanier) {
        $libelle = null;

        if ($articlePanier[SHOP_PANIER_TYPE] == SHOP_PANIER_TYPE_BOUTIQUE) {
            require_once("common/php/dao/produit-boutique-dao.php");
            require_once("common/php/dao/categorie-produit-boutique-dao.php");
            require_once("common/php/dao/marque-produit-boutique-dao.php");

            $produitBoutiqueDAO = new ProduitBoutiqueDAO($this->getDatabaseConnection());
            $categorieProduitBoutiqueDAO = new CategorieProduitBoutiqueDAO($this->getDatabaseConnection());
            $marqueProduitBoutiqueDAO = new MarqueProduitBoutiqueDAO($this->getDatabaseConnection());

            $produit = $produitBoutiqueDAO->getById($articlePanier[SHOP_PANIER_PRODUIT_ID]);
            $categorie = $categorieProduitBoutiqueDAO->getById($produit[PRODUIT_BOUTIQUE_CATEGORIE_PRODUIT_BOUTIQUE_ID]);
            $marque = $marqueProduitBoutiqueDAO->getById($produit[PRODUIT_BOUTIQUE_MARQUE_PRODUIT_BOUTIQUE_ID]);

            $libelle = "<b>".$produit[PRODUIT_BOUTIQUE_NOM]."</b>";
            $libelle .= " - ".$produit[PRODUIT_BOUTIQUE_INDICATION];

            if ($categorie != null) {
                $libelle .= "<br>".$categorie[CATEGORIE_PRODUIT_BOUTIQUE_NOM];
            }
            if ($marque != null) {
                $libelle .= "<br>".$marque[MARQUE_PRODUIT_BOUTIQUE_NOM];
            }
            if ($articlePanier[SHOP_PANIER_TAILLE]) {
                $libelle .= "<br>Taille : ".$articlePanier[SHOP_PANIER_TAILLE];
            }
            if ($articlePanier[SHOP_PANIER_COULEUR]) {
                $libelle .= "<br>Couleur : ".$articlePanier[SHOP_PANIER_COULEUR];
            }
            if ($articlePanier[SHOP_PANIER_FLOCAGE_NOM]) {
                $libelle .= "<br>Flocage nom : ".$articlePanier[SHOP_PANIER_FLOCAGE_NOM];
            }
            if ($articlePanier[SHOP_PANIER_FLOCAGE_NUMERO_ARRIERE]) {
                $libelle .= "<br>Flocage n&deg; arri&egrave;re : ".$articlePanier[SHOP_PANIER_FLOCAGE_NUMERO_ARRIERE];
            }
            if ($articlePanier[SHOP_PANIER_FLOCAGE_NUMERO_AVANT]) {
                $libelle .= "<br>Flocage n&deg; avant : ".$articlePanier[SHOP_PANIER_FLOCAGE_NUMERO_AVANT];
            }
            if ($articlePanier[SHOP_PANIER_FLOCAGE_INITIALES]) {
                $libelle .= "<br>Flocage initiales : ".$articlePanier[SHOP_PANIER_FLOCAGE_INITIALES];
            }
        } else if ($articlePanier[SHOP_PANIER_TYPE] == SHOP_PANIER_TYPE_BILLETTERIE) {
            require_once("common/php/dao/match-billetterie-dao.php");
            require_once("common/php/dao/adversaire-billetterie-dao.php");
            require_once("common/php/dao/competition-billetterie-dao.php");

            $matchBilletterieDAO = new MatchBilletterieDAO($this->getDatabaseConnection());
            $adversaireBilletterieDAO = new AdversaireBilletterieDAO($this->getDatabaseConnection());
            $competitionBilletterieDAO = new CompetitionBilletterieDAO($this->getDatabaseConnection());

            $match = $matchBilletterieDAO->getById($articlePanier[SHOP_PANIER_PRODUIT_ID]);
            $adversaire = $adversaireBilletterieDAO->getById($match[MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID]);
            $competition = $competitionBilletterieDAO->getById($match[MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID]);

            $libelle = "<b>Fenix VS ".$adversaire[ADVERSAIRE_BILLETTERIE_NOM]."</b>";
            $libelle .= "<br>".$competition[COMPETITION_BILLETTERIE_NOM];
            $libelle .= "<br>".DateUtils::convert_sqlDateTime_to_slashDateTime($match[MATCH_BILLETTERIE_DATE_HEURE]);
        }

        return $libelle;
    }
}

?>