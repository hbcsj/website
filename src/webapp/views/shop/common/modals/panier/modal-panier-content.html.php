<?php
require_once("core/php/resources/view.php");

$view = new View(
    "webapp/views/shop/common/modals/panier/", 
    "modal-panier", 
    "ModalPanierCtrl"
);
$ctrl = $view->getController();
?>

<div class="modal-header">
    <div class="modal-title">Votre panier</div>
</div>
<div class="modal-body">
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th colspan="2">Article</th>
                <th class="text-center">Quantit&eacute;</th>
                <th class="text-center">Prix</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $montantTotal = 0;
            if (sizeof($_SESSION[SESSION_HBCSJ][SHOP_PANIER_ARTICLES]) > 0) {
                foreach ($_SESSION[SESSION_HBCSJ][SHOP_PANIER_ARTICLES] as $key => $articlePanier) {
                    $montantTotal += $articlePanier[SHOP_PANIER_PRIX];
                    ?>
                    <tr>
                        <td><?php echo $articlePanier[SHOP_PANIER_TYPE]; ?></td>
                        <td><?php echo $ctrl->getLibelleArticlePanier($articlePanier); ?></td>
                        <td class="text-center"><?php echo $articlePanier[SHOP_PANIER_QUANTITE]; ?></td>
                        <td class="text-center"><?php echo $articlePanier[SHOP_PANIER_PRIX]; ?> &euro;</td>
                        <td class="text-center">
                            <span class="glyphicon glyphicon-trash clickable" title="Supprimer" id-article-panier="<?php echo $key; ?>"></span>
                            <div class="loader loader-delete" id-article-panier="<?php echo $key; ?>"></div>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
            <tr>
                <td colspan="3"><b>TOTAL</b></td>
                <td class="text-center"><b><?php echo $montantTotal; ?> &euro;</b></td>
                <td>&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <?php
    if (sizeof($_SESSION[SESSION_HBCSJ][SHOP_PANIER_ARTICLES]) > 0) {
        ?>
        <div class="form-horizontal" id="form-panier">
            <div class="form-group">
                <label for="form-panier__input-nom" class="col-xs-2 control-label required">Nom :</label>
                <div class="col-xs-3 form-input" for="form-panier__input-nom">
                    <input type="text" class="form-control" id="form-panier__input-nom" required>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-panier__input-nom"></div>
                <label for="form-panier__input-prenom" class="col-xs-2 control-label required">Pr&eacute;nom :</label>
                <div class="col-xs-3 form-input" for="form-panier__input-prenom">
                    <input type="text" class="form-control" id="form-panier__input-prenom" required>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-panier__input-prenom"></div>
            </div>
            <div class="form-group">
                <label for="form-panier__input-email" class="col-xs-2 control-label required">Email :</label>
                <div class="col-xs-4 form-input" for="form-panier__input-email">
                    <input type="text" class="form-control" id="form-panier__input-email" required pattern="^[A-Za-z0-9._-]+@[A-Za-z0-9.-]{2,}[.][A-Za-z]{2,3}$" pattern-indication="Le format de l'email est invalide">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-panier__input-email"></div>
                <label for="form-panier__input-telephone" class="col-xs-1 control-label required">T&eacute;l. :</label>
                <div class="col-xs-3 form-input" for="form-panier__input-telephone">
                    <input type="text" class="form-control" id="form-panier__input-telephone" required>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-panier__input-telephone"></div>
            </div>
            <div class="form-group">
                <label for="form-panier__input-message" class="col-xs-2 control-label">Message :</label>
                <div class="col-xs-9 form-input" for="form-panier__input-message">
                    <textarea class="form-control" id="form-panier__input-message" rows="3"></textarea>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-panier__input-message"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-required text-right">Les champs marqu&eacute;s d'une * sont obligatoires</div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-infos-commande text-right">
                    Une fois la commande envoy&eacute;e, vous recevrez un email r&eacute;capitulatif avec la proc&eacute;dure &agrave; suivre pour le paiement.
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</div>
<div class="modal-footer">
    <?php
    if (sizeof($_SESSION[SESSION_HBCSJ][SHOP_PANIER_ARTICLES]) > 0) {
        ?>
        <div class="row">
            <div class="col-xs-8">
                <div class="form-result-message"></div>
            </div>
            <div class="col-xs-4 text-right">
                <button id="modal-panier__bt-commander" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-ok"></span>
                    </div>
                    <div class="button__text">Commander</div>
                    <div class="loader"></div>
                </button>
            </div>
        </div>
        <?php
    }
    ?>
</div>
