<?php
require_once("core/php/resources/page.php");
require_once("common/php/constants/config-constants.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("lib/php/mobile-detect.php");

$page = new Page(
    "webapp/views/shop/billetterie/", 
    "shop-billetterie", 
    "ShopBilletterieCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/shop/common/nav/nav.html.php");

$mobileDetect = new Mobile_Detect();
$matchs = $ctrl->getMatchs();
?>

<div class="container-fluid animated fadeIn">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
            <h1>
                Matchs &agrave; venir
                <button class="btn btn-default" id="bt-panier">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-shopping-cart"></span>
                    </div>
                    <div class="button__text">
                        Votre panier
                        <span class="badge"></span>
                    </div>
                </button>
            </h1>
        </div>
    </div>
    <?php
    if (sizeof($matchs) > 0) {
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <div class="row">
                    <?php
                    $i = 0;
                    foreach ($matchs as $match) {
                        if ($mobileDetect->isMobile() || $mobileDetect->isTablet()) {
                            ?>
                            <div class="col-xs-6 col-match" id-match="<?php echo $match[MATCH_BILLETTERIE_ID]; ?>">
                                <div class="header-match">
                                    <span class="nom-match"><?php echo "Fenix VS ".$ctrl->getNomAdversaire($match[MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID]); ?></span>
                                    <br>
                                    <span class="competition-match"><?php echo $ctrl->getNomCompetition($match[MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID]); ?></span>
                                </div>
                                <div class="date-match">
                                    <?php 
                                    $timestampDateHeureMatch = DateUtils::get_sqlDateTime_timestamp($match[MATCH_BILLETTERIE_DATE_HEURE]);
                                    echo DateUtils::getNomJour(date("w", $timestampDateHeureMatch))." ".date("d", $timestampDateHeureMatch)." ".DateUtils::getMois()[date("m", $timestampDateHeureMatch)]." ".date("Y", $timestampDateHeureMatch);
                                    ?>
                                    <br>
                                    <?php
                                    echo date(SLASH_TIME_FORMAT, $timestampDateHeureMatch);
                                    ?>
                                </div>
                                <img src="<?php echo SRC_PATH; ?>assets/img/logo-fenix.png" class="img-match">
                                <img src="<?php echo SRC_PATH.PathUtils::getAdversaireImgFile($match[MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID]); ?>" class="img-match">
                                <div class="prix-match prix-1place-match">
                                    <div class="label-prix">1 place</div>
                                    <div class="prix-hbcsj"><?php echo $match[MATCH_BILLETTERIE_PRIX_1_PLACE]." &euro;"; ?></div>
                                    <div class="prix-au-lieu-de"><?php echo $match[MATCH_BILLETTERIE_PRIX_PUBLIC]." &euro;"; ?></div>
                                </div>
                                <div class="prix-match prix-3places-match">
                                    <div class="label-prix">3 places</div>
                                    <div class="prix-hbcsj"><?php echo $match[MATCH_BILLETTERIE_PRIX_3_PLACES]." &euro;"; ?></div>
                                    <div class="prix-au-lieu-de"><?php echo (3 * $match[MATCH_BILLETTERIE_PRIX_PUBLIC])." &euro;"; ?></div>
                                </div>
                                <div class="date-limite">
                                    <b>Date limite pour commander</b>
                                    <br>
                                    <?php 
                                    $timestampDateLimiteCommande = DateUtils::get_sqlDate_timestamp($match[MATCH_BILLETTERIE_DATE_LIMITE_COMMANDE]);
                                    echo DateUtils::getNomJour(date("w", $timestampDateLimiteCommande))." ".date("d", $timestampDateLimiteCommande)." ".DateUtils::getMois()[date("m", $timestampDateLimiteCommande)]." ".date("Y", $timestampDateLimiteCommande);
                                    ?>
                                </div>
                            </div>
                            <?php
                            if ((($i + 1) % 2) == 0) {
                                ?>
                                </div><div class="row">
                                <?php
                            }
                        } else {
                            ?>
                            <div class="col-xs-4 col-match" id-match="<?php echo $match[MATCH_BILLETTERIE_ID]; ?>">
                                <div class="header-match">
                                    <span class="nom-match"><?php echo "Fenix VS ".$ctrl->getNomAdversaire($match[MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID]); ?></span>
                                    <br>
                                    <span class="competition-match"><?php echo $ctrl->getNomCompetition($match[MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID]); ?></span>
                                </div>
                                <div class="date-match">
                                    <?php 
                                    $timestampDateHeureMatch = DateUtils::get_sqlDateTime_timestamp($match[MATCH_BILLETTERIE_DATE_HEURE]);
                                    echo DateUtils::getNomJour(date("w", $timestampDateHeureMatch))." ".date("d", $timestampDateHeureMatch)." ".DateUtils::getMois()[date("m", $timestampDateHeureMatch)]." ".date("Y", $timestampDateHeureMatch);
                                    ?>
                                    <br>
                                    <?php
                                    echo date(SLASH_TIME_FORMAT, $timestampDateHeureMatch);
                                    ?>
                                </div>
                                <img src="<?php echo SRC_PATH; ?>assets/img/logo-fenix.png" class="img-match">
                                <img src="<?php echo SRC_PATH.PathUtils::getAdversaireImgFile($match[MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID]); ?>" class="img-match">
                                <div class="prix-match prix-1place-match">
                                    <div class="label-prix">1 place</div>
                                    <div class="prix-hbcsj"><?php echo $match[MATCH_BILLETTERIE_PRIX_1_PLACE]." &euro;"; ?></div>
                                    <div class="prix-au-lieu-de"><?php echo $match[MATCH_BILLETTERIE_PRIX_PUBLIC]." &euro;"; ?></div>
                                </div>
                                <div class="prix-match prix-3places-match">
                                    <div class="label-prix">3 places</div>
                                    <div class="prix-hbcsj"><?php echo $match[MATCH_BILLETTERIE_PRIX_3_PLACES]." &euro;"; ?></div>
                                    <div class="prix-au-lieu-de"><?php echo (3 * $match[MATCH_BILLETTERIE_PRIX_PUBLIC])." &euro;"; ?></div>
                                </div>
                                <div class="date-limite">
                                    <b>Date limite pour commander</b>
                                    <br>
                                    <?php 
                                    $timestampDateLimiteCommande = DateUtils::get_sqlDate_timestamp($match[MATCH_BILLETTERIE_DATE_LIMITE_COMMANDE]);
                                    echo DateUtils::getNomJour(date("w", $timestampDateLimiteCommande))." ".date("d", $timestampDateLimiteCommande)." ".DateUtils::getMois()[date("m", $timestampDateLimiteCommande)]." ".date("Y", $timestampDateLimiteCommande);
                                    ?>
                                </div>
                            </div>
                            <?php
                            if ((($i + 1) % 3) == 0) {
                                ?>
                                </div><div class="row">
                                <?php
                            }
                        }
                        $i++;
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</div>

<?php
require_once("webapp/views/common/footer/footer.html.php");

require_once("webapp/views/common/modals/alert/modal-alert.html.php");

require_once("webapp/views/shop/billetterie/modals/save-places-panier/modal-save-places-panier.html.php");
require_once("webapp/views/shop/common/modals/panier/modal-panier.html.php");
?>

<?php
$page->finalizePage();
?>