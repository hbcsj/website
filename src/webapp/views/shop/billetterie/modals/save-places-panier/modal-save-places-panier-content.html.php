<?php
require_once("core/php/resources/view.php");
require_once("common/php/constants/config-constants.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("lib/php/mobile-detect.php");

$view = new View(
    "webapp/views/shop/billetterie/modals/save-places-panier/", 
    "modal-save-places-panier", 
    "ModalSavePlacesPanierCtrl"
);
$ctrl = $view->getController();

$mobileDetect = new Mobile_Detect();
$match = $ctrl->getMatchBilletterie();
?>

<div class="modal-header">
    <div class="modal-title">
        Commande de places pour Fenix VS <?php echo $ctrl->getNomAdversaire($match[MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID]); ?>
    </div>
</div>
<div class="modal-body">
    <div class="row" id="row-infos-match">
        <div class="col-xs-6 col-xs-offset-1">
            <div id="nom-match">
                <?php echo "Fenix VS ".$ctrl->getNomAdversaire($match[MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID]); ?>
            </div>
            <div id="competition-match">
                <?php echo $ctrl->getNomCompetition($match[MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID]); ?>
            </div>
            <div id="date-match">
                <?php 
                $timestampDateHeureMatch = DateUtils::get_sqlDateTime_timestamp($match[MATCH_BILLETTERIE_DATE_HEURE]);
                echo DateUtils::getNomJour(date("w", $timestampDateHeureMatch))." ".date("d", $timestampDateHeureMatch)." ".DateUtils::getMois()[date("m", $timestampDateHeureMatch)]." ".date("Y", $timestampDateHeureMatch)." &agrave; ".date(SLASH_TIME_FORMAT, $timestampDateHeureMatch);
                ?>
            </div>
        </div>
        <div class="col-xs-2 text-center">
            <img src="<?php echo SRC_PATH; ?>assets/img/logo-fenix.png">
        </div>
        <div class="col-xs-2 text-center">
            <img src="<?php echo SRC_PATH.PathUtils::getAdversaireImgFile($match[MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID]); ?>">
        </div>
    </div>
    <div class="form-horizontal" id="form-save-places-panier">
        <div class="form-group">
            <label for="form-save-places-panier__input-nb-places" class="col-xs-6 col-lg-3 control-label required">Nb. places :</label>
            <div class="col-xs-3 col-lg-2 form-input" for="form-save-places-panier__input-nb-places">
                <select class="form-control" id="form-save-places-panier__input-nb-places" required>
                    <option value=""></option>
                    <?php
                    for ($i = 1 ; $i <= $GLOBALS[CONF][NB_MAX_PLACES_COMMANDE_SHOP_BILLETTERIE] ; $i++) {
                        ?>
                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                        <?php
                    }
                    ?>
                </select>
                <input type="hidden" id="form-save-places-panier__input-prix-total" value="0">
                <input type="hidden" id="form-save-places-panier__input-prix-1-place" value="<?php echo $match[MATCH_BILLETTERIE_PRIX_1_PLACE]; ?>">
                <input type="hidden" id="form-save-places-panier__input-prix-3-places" value="<?php echo $match[MATCH_BILLETTERIE_PRIX_3_PLACES]; ?>">
            </div>
            <div class="col-xs-1 form-icon-validation" for="form-save-places-panier__input-nb-places"></div>
            <?php
            if (!$mobileDetect->isMobile() && !$mobileDetect->isTablet()) {
                ?>
                <div class="col-lg-4 col-lg-offset-1">
                    <div id="prix-total">0 &euro;</div>
                </div>
                <?php
            }
            ?>
        </div>
        <?php
        if ($mobileDetect->isMobile() || $mobileDetect->isTablet()) {
            ?>
            <div class="row">
                <div class="col-xs-12">
                    <div id="prix-total">0 &euro;</div>
                </div>
            </div>
            <?php
        }
        ?>
        <div class="row">
            <div class="col-xs-12 text-required text-right">Les champs marqu&eacute;s d'une * sont obligatoires</div>
        </div>
        <?php
        $conditionsGeneralesFile = FileUtils::getFileContent(CONDITIONS_GENERALES_BILLETTERIE_FILE); 

        if ($conditionsGeneralesFile != "") {
            ?>
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 text-infos-commande text-center">
                    <?php echo $conditionsGeneralesFile; ?>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>
<div class="modal-footer">
    <div class="row">
        <div class="col-xs-8">
            <div class="form-result-message"></div>
        </div>
        <div class="col-xs-4 text-right">
            <button id="modal-save-places-panier__bt-ajouter" class="btn btn-default">
                <div class="button__icon">
                    <span class="glyphicon glyphicon-plus"></span>
                </div>
                <div class="button__text">Ajouter</div>
                <div class="loader"></div>
            </button>
        </div>
    </div>
</div>
