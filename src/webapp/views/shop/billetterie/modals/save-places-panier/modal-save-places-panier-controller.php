<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/match-billetterie-dao.php");
require_once("common/php/dao/adversaire-billetterie-dao.php");
require_once("common/php/dao/competition-billetterie-dao.php");

class ModalSavePlacesPanierCtrl extends AbstractViewCtrl {
	
	private $matchBilletterie;

	private $matchBilletterieDAO;
	private $adversaireBilletterieDAO;
	private $competitionBilletterieDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idMatchBilletterie" => $_GET["idMatchBilletterie"]
		), true);
		
		$this->matchBilletterieDAO = new MatchBilletterieDAO($this->getDatabaseConnection());
		$this->adversaireBilletterieDAO = new AdversaireBilletterieDAO($this->getDatabaseConnection());
		$this->competitionBilletterieDAO = new CompetitionBilletterieDAO($this->getDatabaseConnection());

		$this->checkRequest();
	}

	private function checkRequest() {
		$checkParams = $this->checkParams(array(
			array(GET, "idMatchBilletterie")
		));
		if ($checkParams) {
			$this->checkIdMatchBilletterie();
		}
	}

	private function checkIdMatchBilletterie() {
		$this->matchBilletterie = $this->matchBilletterieDAO->getById($_GET["idMatchBilletterie"]);
			
		if ($this->matchBilletterie == null) {
			$this->sendCheckError(
				HTTP_404, 
				"Le match billetterie (idMatchBilletterie = '".$_GET["idMatchBilletterie"]."') n'existe pas", 
				"webapp/views/common/error/404/404.html.php"
			);
		}
	}

	public function getNomAdversaire($adversaireId) {
		$adversaire = $this->adversaireBilletterieDAO->getById($adversaireId);
		return $adversaire[ADVERSAIRE_BILLETTERIE_NOM];
	}

	public function getNomCompetition($competitionId) {
		$competition = $this->competitionBilletterieDAO->getById($competitionId);
		return $competition[COMPETITION_BILLETTERIE_NOM];
	}

	public function getMatchBilletterie() {
		return $this->matchBilletterie;
	}
}

?>