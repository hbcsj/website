ShopBilletterieController = {};

ShopBilletterieController.control_Matchs = function() {
    $('.col-match').unbind('click');
    $('.col-match').click(function() {
        var idMatch = $(this).attr('id-match');
        open_ModalSavePlacesPanierMatch(idMatch);
    });

    function open_ModalSavePlacesPanierMatch(idMatch) {
        var params = {
            idMatchBilletterie: idMatch
        };
        AjaxUtils.loadView(
            'shop/billetterie/modals/save-places-panier/modal-save-places-panier-content',
            '#modal-save-places-panier-content',
            params,
            function() {
                control_NbPlaces();
                onClick_btAjouter(idMatch);
            },
            '#loader-page'
        );
        $('#modal-save-places-panier').modal('show');

        function control_NbPlaces() {
            $('#form-save-places-panier__input-nb-places').unbind('change');
            $('#form-save-places-panier__input-nb-places').change(function() {
                if ($(this).val() != '') {
                    var nbPlaces = parseInt($(this).val());

                    var placesPrix1Place = nbPlaces % 3;
                    var placesPrix3Places = nbPlaces - placesPrix1Place;

                    var prixTotal = (placesPrix1Place * parseInt($('#form-save-places-panier__input-prix-1-place').val())) + 
                        ((placesPrix3Places / 3) * parseInt($('#form-save-places-panier__input-prix-3-places').val()));

                    $('#form-save-places-panier__input-prix-total').val(prixTotal);
                    $('#prix-total').html(prixTotal+' &euro;');
                } else {
                    $('#prix-total').html('0 &euro;');
                }
            });
        }

        function onClick_btAjouter(idMatch) {
            $('#modal-save-places-panier__bt-ajouter').unbind('click');
            $('#modal-save-places-panier__bt-ajouter').click(function() {
                FormUtils.hideFormErrors('#modal-save-places-panier #form-save-places-panier');
                ViewUtils.unsetHTMLAndHide('#modal-save-places-panier .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('#modal-save-places-panier__bt-ajouter', '#modal-save-places-panier__bt-ajouter .loader');

                var formValidation = FormUtils.validateForm('#modal-save-places-panier #form-save-places-panier');
                
                if (formValidation) {
                    var inputNbPlaces = $('#form-save-places-panier__input-nb-places').val();
                    var inputPrixTotal = $('#form-save-places-panier__input-prix-total').val();

                    saveCommandeBilletteriePanier(idMatch, inputNbPlaces, inputPrixTotal);
                } else {
                    ViewUtils.activeButtonAndHideLoader('#modal-save-places-panier__bt-ajouter', '#modal-save-places-panier__bt-ajouter .loader');
                }
            });

            function saveCommandeBilletteriePanier(idMatch, inputNbPlaces, inputPrixTotal) {
                AjaxUtils.callService(
                    'shop-panier', 
                    'saveCommandeBilletterie', 
                    AjaxUtils.dataTypes.json, 
                    AjaxUtils.requestTypes.post, 
                    null, 
                    {
                        matchBilletterieId: idMatch,
                        nbPlaces: inputNbPlaces,
                        prixTotal: inputPrixTotal
                    }, 
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-save-places-panier .form-result-message', 'Places ajout&eacute;es au panier', FormUtils.resultsTypes.ok);
                            $('#modal-save-places-panier__bt-ajouter .loader').hide();
                            ShopController.load_MontantPanier();
                        } else {
                            ViewUtils.activeButtonAndHideLoader('#modal-save-places-panier__bt-ajouter', '#modal-save-places-panier__bt-ajouter .loader');
                            FormUtils.displayFormResultMessage('#modal-save-places-panier .form-result-message', 'Erreur durant l\'ajout', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('#modal-save-places-panier__bt-ajouter', '#modal-save-places-panier__bt-ajouter .loader');
                        FormUtils.displayFormResultMessage('#modal-save-places-panier .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            }
        }
    }
};

initializePage = function() {
    ShopController.initializePage('billetterie');

    ShopBilletterieController.control_Matchs();
};