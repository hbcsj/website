<?php
require_once("core/php/controllers/abstract-page-controller.php");
require_once("common/php/dao/match-billetterie-dao.php");
require_once("common/php/dao/adversaire-billetterie-dao.php");
require_once("common/php/dao/competition-billetterie-dao.php");
require_once("common/php/lib/date-utils.php");

class ShopBilletterieCtrl extends AbstractPageCtrl {
	
	private $matchBilletterieDAO;
	private $adversaireBilletterieDAO;
	private $competitionBilletterieDAO;

    public function __construct($pageName) {
		parent::__construct($pageName, null, true);

		$this->matchBilletterieDAO = new MatchBilletterieDAO($this->getDatabaseConnection());
		$this->adversaireBilletterieDAO = new AdversaireBilletterieDAO($this->getDatabaseConnection());
		$this->competitionBilletterieDAO = new CompetitionBilletterieDAO($this->getDatabaseConnection());
	}

	public function getMatchs() {
		return $this->matchBilletterieDAO->getEnVenteAVenirAvantDateLimite(
			MATCH_BILLETTERIE_TABLE_NAME.".".MATCH_BILLETTERIE_DATE_HEURE
		);
	}

	public function getNomAdversaire($adversaireId) {
		$adversaire = $this->adversaireBilletterieDAO->getById($adversaireId);
		return $adversaire[ADVERSAIRE_BILLETTERIE_NOM];
	}

	public function getNomCompetition($competitionId) {
		$competition = $this->competitionBilletterieDAO->getById($competitionId);
		return $competition[COMPETITION_BILLETTERIE_NOM];
	}
}

?>