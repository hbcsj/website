<?php
require_once("core/php/resources/page.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("lib/php/mobile-detect.php");

$page = new Page(
    "webapp/views/medias/videos/", 
    "medias-videos", 
    "MediasVideosCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/common/header/header.html.php");
require_once("webapp/views/common/nav/nav.html.php");
require_once("webapp/views/medias/sub-menu/sub-menu-medias.html.php");

$mobileDetect = new Mobile_Detect();
$videos = $ctrl->getVideos();
?>

<div class="container-fluid animated fadeIn">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
            <h1>Vid&eacute;os</h1>
        </div>
    </div>
    <?php
    if (sizeof($videos) > 0) {
        $i = 0;
        ?>
        <div class="row row-video">
        <?php
        foreach ($videos as $video) {
            $colOffset = "";
            if ($mobileDetect->isMobile() || $mobileDetect->isTablet()) {
                if (($i % 2) == 0) {
                    $colOffset = " col-xs-offset-1";
                }
                ?>
                <div class="col-xs-5<?php echo $colOffset; ?> col-video">
                    <div class="header-video">
                        <b><?php echo str_replace(" - ", "<br>", $video[VIDEO_NOM]); ?></b>
                        <br>
                        <a href="<?php echo $video[VIDEO_URL]; ?>" target="_blank">
                            <b>Voir la vid&eacute;o</b>
                        </a>
                    </div>
                    <a href="<?php echo $video[VIDEO_URL]; ?>" target="_blank">
                        <img src="<?php echo SRC_PATH.PathUtils::getVideoImgSmallFile($video[VIDEO_ID]); ?>" img-modal="<?php echo SRC_PATH.PathUtils::getVideoImgSmallFile($video[VIDEO_ID]); ?>" title="Afficher la vid&eacute;o">
                    </a>
                </div>
                <?php
                if ((($i + 1) % 2) == 0) {
                    ?>
                    </div><div class="row row-video">
                    <?php
                }
            } else {
                if (($i % 5) == 0) {
                    $colOffset = " col-xs-offset-1";
                }
                ?>
                <div class="col-xs-2<?php echo $colOffset; ?> col-video">
                    <div class="header-video">
                        <b><?php echo str_replace(" - ", "<br>", $video[VIDEO_NOM]); ?></b>
                        <br>
                        <a href="<?php echo $video[VIDEO_URL]; ?>" target="_blank">
                            <b>Voir la vid&eacute;o</b>
                        </a>
                    </div>
                    <a href="<?php echo $video[VIDEO_URL]; ?>" target="_blank">
                        <img src="<?php echo SRC_PATH.PathUtils::getVideoImgSmallFile($video[VIDEO_ID]); ?>" img-modal="<?php echo SRC_PATH.PathUtils::getVideoImgSmallFile($video[VIDEO_ID]); ?>" title="Afficher la vid&eacute;o">
                    </a>
                </div>
                <?php
                if ((($i + 1) % 5) == 0) {
                    ?>
                    </div><div class="row row-video">
                    <?php
                }
            }
            $i++;
        }
        ?>
        </div>
        <?php
    }
    ?>
</div>

<?php
require_once("webapp/views/common/footer/footer.html.php");

require_once("webapp/views/common/modals/alert/modal-alert.html.php");
?>

<?php
$page->finalizePage();
?>