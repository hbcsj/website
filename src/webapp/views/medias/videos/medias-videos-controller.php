<?php
require_once("core/php/controllers/abstract-page-controller.php");
require_once("common/php/dao/video-dao.php");

class MediasVideosCtrl extends AbstractPageCtrl {

	private $videoDAO;

    public function __construct($pageName) {
        parent::__construct($pageName, null, true);
        
        $this->videoDAO = new VideoDAO($this->getDatabaseConnection());
    }
    
    public function getVideos() {
        return $this->videoDAO->getAll(VIDEO_TABLE_NAME.".".VIDEO_ID." DESC");
    }
}

?>