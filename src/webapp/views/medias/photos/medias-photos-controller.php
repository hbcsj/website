<?php
require_once("core/php/controllers/abstract-page-controller.php");
require_once("common/php/dao/album-photo-dao.php");

class MediasPhotosCtrl extends AbstractPageCtrl {

	private $albumPhotoDAO;

    public function __construct($pageName) {
        parent::__construct($pageName, null, true);
        
        $this->albumPhotoDAO = new AlbumPhotoDAO($this->getDatabaseConnection());
    }
    
    public function getAlbumsPhotos() {
        return $this->albumPhotoDAO->getAll(ALBUM_PHOTO_TABLE_NAME.".".ALBUM_PHOTO_ID." DESC");
    }
}

?>