<?php
require_once("core/php/resources/page.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("lib/php/mobile-detect.php");

$page = new Page(
    "webapp/views/medias/photos/", 
    "medias-photos", 
    "MediasPhotosCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/common/header/header.html.php");
require_once("webapp/views/common/nav/nav.html.php");
require_once("webapp/views/medias/sub-menu/sub-menu-medias.html.php");

$mobileDetect = new Mobile_Detect();
$albumsPhotos = $ctrl->getAlbumsPhotos();
?>

<div class="container-fluid animated fadeIn">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
            <h1>Albums photos</h1>
        </div>
    </div>
    <?php
    if (sizeof($albumsPhotos) > 0) {
        $i = 0;
        ?>
        <div class="row row-album">
        <?php
        foreach ($albumsPhotos as $albumPhoto) {
            $colOffset = "";
            if ($mobileDetect->isMobile() || $mobileDetect->isTablet()) {
                if (($i % 2) == 0) {
                    $colOffset = " col-xs-offset-1";
                }
                ?>
                <div class="col-xs-5<?php echo $colOffset; ?> col-album">
                    <div class="header-album">
                        <b><?php echo str_replace(" - ", "<br>", $albumPhoto[ALBUM_PHOTO_NOM]); ?></b>
                        <br>
                        <a href="<?php echo $albumPhoto[ALBUM_PHOTO_URL]; ?>" target="_blank">
                            <b>Voir l'album</b>
                        </a>
                    </div>
                    <a href="<?php echo $albumPhoto[ALBUM_PHOTO_URL]; ?>" target="_blank">
                        <img src="<?php echo SRC_PATH.PathUtils::getAlbumPhotoImgSmallFile($albumPhoto[ALBUM_PHOTO_ID]); ?>" img-modal="<?php echo SRC_PATH.PathUtils::getAlbumPhotoImgSmallFile($albumPhoto[ALBUM_PHOTO_ID]); ?>" title="Afficher l'album photo">
                    </a>
                </div>
                <?php
                if ((($i + 1) % 2) == 0) {
                    ?>
                    </div><div class="row row-album">
                    <?php
                }
            } else {
                if (($i % 5) == 0) {
                    $colOffset = " col-xs-offset-1";
                }
                ?>
                <div class="col-xs-2<?php echo $colOffset; ?> col-album">
                    <div class="header-album">
                        <b><?php echo str_replace(" - ", "<br>", $albumPhoto[ALBUM_PHOTO_NOM]); ?></b>
                        <br>
                        <a href="<?php echo $albumPhoto[ALBUM_PHOTO_URL]; ?>" target="_blank">
                            <b>Voir l'album</b>
                        </a>
                    </div>
                    <a href="<?php echo $albumPhoto[ALBUM_PHOTO_URL]; ?>" target="_blank">
                        <img src="<?php echo SRC_PATH.PathUtils::getAlbumPhotoImgSmallFile($albumPhoto[ALBUM_PHOTO_ID]); ?>" img-modal="<?php echo SRC_PATH.PathUtils::getAlbumPhotoImgSmallFile($albumPhoto[ALBUM_PHOTO_ID]); ?>" title="Afficher l'album photo">
                    </a>
                </div>
                <?php
                if ((($i + 1) % 5) == 0) {
                    ?>
                    </div><div class="row row-album">
                    <?php
                }
            }
            $i++;
        }
        ?>
        </div>
        <?php
    }
    ?>
</div>

<?php
require_once("webapp/views/common/footer/footer.html.php");

require_once("webapp/views/common/modals/alert/modal-alert.html.php");
?>

<?php
$page->finalizePage();
?>