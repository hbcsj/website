<?php
require_once("core/php/resources/page.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("lib/php/mobile-detect.php");

$page = new Page(
    "webapp/views/medias/hhsj/", 
    "medias-hhsj", 
    "MediasHHSJCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/common/header/header.html.php");
require_once("webapp/views/common/nav/nav.html.php");
require_once("webapp/views/medias/sub-menu/sub-menu-medias.html.php");

$mobileDetect = new Mobile_Detect();
$hhsjs = $ctrl->getHHSJs();
?>

<div class="container-fluid animated fadeIn">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
            <h1>Hand-Hebdos Saint-Jeannais</h1>
        </div>
    </div>
    <?php
    if (sizeof($hhsjs) > 0) {
        $i = 0;
        ?>
        <div class="row row-hhsj">
        <?php
        foreach ($hhsjs as $hhsj) {
            $colOffset = "";
            if ($mobileDetect->isMobile() || $mobileDetect->isTablet()) {
                if (($i % 2) == 0) {
                    $colOffset = " col-xs-offset-1";
                }
                ?>
                <div class="col-xs-5<?php echo $colOffset; ?> col-hhsj">
                    <div class="header-hhsj">
                        <div class="date-publication">
                            Publi&eacute; le <?php echo DateUtils::convert_sqlDate_to_slashDate($hhsj[HHSJ_DATE]); ?>
                        </div>
                        <b><?php echo $ctrl->getLibelleTypeHHSJ($hhsj[HHSJ_TYPE_HHSJ_ID]); ?></b>
                        <br>
                        <?php
                        if ($hhsj[HHSJ_TYPE_HHSJ_ID] == HHSJ_UNE_DE_MAGAZINE_TYPE_ID) {
                            echo "<br>";
                        } else {
                            ?>
                            <a href="<?php echo SRC_PATH.PathUtils::getHHSJFile($hhsj[HHSJ_ID]); ?>" target="_blank">
                                <b>Consulter le Hand-Hebdo</b>
                            </a>
                            <?php
                        }
                        ?>
                    </div>
                    <img src="<?php echo SRC_PATH.PathUtils::getHHSJImgMediumFile($hhsj[HHSJ_ID]); ?>" img-modal="<?php echo SRC_PATH.PathUtils::getHHSJImgFile($hhsj[HHSJ_ID]); ?>" title="Afficher le Hand-Hebdo">
                </div>
                <?php
                if ((($i + 1) % 2) == 0) {
                    ?>
                    </div><div class="row row-hhsj">
                    <?php
                }
            } else {
                if (($i % 5) == 0) {
                    $colOffset = " col-xs-offset-1";
                }
                ?>
                <div class="col-xs-2<?php echo $colOffset; ?> col-hhsj">
                    <div class="header-hhsj">
                        <div class="date-publication">
                            Publi&eacute; le <?php echo DateUtils::convert_sqlDate_to_slashDate($hhsj[HHSJ_DATE]); ?>
                        </div>
                        <b><?php echo $ctrl->getLibelleTypeHHSJ($hhsj[HHSJ_TYPE_HHSJ_ID]); ?></b>
                        <br>
                        <?php
                        if ($hhsj[HHSJ_TYPE_HHSJ_ID] == HHSJ_UNE_DE_MAGAZINE_TYPE_ID) {
                            echo "<br>";
                        } else {
                            ?>
                            <a href="<?php echo SRC_PATH.PathUtils::getHHSJFile($hhsj[HHSJ_ID]); ?>" target="_blank">
                                <b>Consulter le Hand-Hebdo</b>
                            </a>
                            <?php
                        }
                        ?>
                    </div>
                    <img src="<?php echo SRC_PATH.PathUtils::getHHSJImgSmallFile($hhsj[HHSJ_ID]); ?>" img-modal="<?php echo SRC_PATH.PathUtils::getHHSJImgFile($hhsj[HHSJ_ID]); ?>" title="Afficher le Hand-Hebdo">
                </div>
                <?php
                if ((($i + 1) % 5) == 0) {
                    ?>
                    </div><div class="row row-hhsj">
                    <?php
                }
            }
            $i++;
        }
        ?>
        </div>
        <?php
    }
    ?>
</div>

<?php
require_once("webapp/views/common/footer/footer.html.php");

require_once("webapp/views/common/modals/alert/modal-alert.html.php");
require_once("webapp/views/common/modals/img/modal-img.html.php");
?>

<?php
$page->finalizePage();
?>