<?php
require_once("core/php/controllers/abstract-page-controller.php");
require_once("common/php/dao/hhsj-dao.php");
require_once("common/php/dao/type-hhsj-dao.php");

class MediasHHSJCtrl extends AbstractPageCtrl {

	private $hhsjDAO;
	private $typeHHSJDAO;

    public function __construct($pageName) {
        parent::__construct($pageName, null, true);
        
        $this->hhsjDAO = new HHSJDAO($this->getDatabaseConnection());
		$this->typeHHSJDAO = new TypeHHSJDAO($this->getDatabaseConnection());
    }
    
    public function getHHSJs() {
        return $this->hhsjDAO->getAll(HHSJ_TABLE_NAME.".".HHSJ_DATE." DESC");
    }

	public function getLibelleTypeHHSJ($typeHHSJId) {
		$typeHHSJ = $this->typeHHSJDAO->getById($typeHHSJId);
		return $typeHHSJ[TYPE_HHSJ_LIBELLE];
	}
}

?>