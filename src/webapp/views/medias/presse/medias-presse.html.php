<?php
require_once("core/php/resources/page.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("lib/php/mobile-detect.php");

$page = new Page(
    "webapp/views/medias/presse/", 
    "medias-presse", 
    "MediasPresseCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/common/header/header.html.php");
require_once("webapp/views/common/nav/nav.html.php");
require_once("webapp/views/medias/sub-menu/sub-menu-medias.html.php");

$mobileDetect = new Mobile_Detect();
$articlesPresse = $ctrl->getArticlesPresse();
?>

<div class="container-fluid animated fadeIn">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
            <h1>Articles de presse</h1>
        </div>
    </div>
    <?php
    if (sizeof($articlesPresse) > 0) {
        $i = 0;
        ?>
        <div class="row row-article">
        <?php
        foreach ($articlesPresse as $articlePresse) {
            $colOffset = "";
            if ($mobileDetect->isMobile() || $mobileDetect->isTablet()) {
                if (($i % 2) == 0) {
                    $colOffset = " col-xs-offset-1";
                }
                ?>
                <div class="col-xs-5<?php echo $colOffset; ?> col-article">
                    <div class="header-article">
                        <div class="date-publication">
                            Publi&eacute; le <?php echo DateUtils::convert_sqlDate_to_slashDate($articlePresse[ARTICLE_PRESSE_DATE]); ?>
                        </div>
                        <b><?php echo $ctrl->getNomJournalArticlePresse($articlePresse[ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID]); ?></b>
                        <br>
                        <a href="<?php echo $articlePresse[ARTICLE_PRESSE_URL]; ?>" target="_blank">
                            <b>Consulter l'article</b>
                        </a>
                    </div>
                    <img src="<?php echo SRC_PATH.PathUtils::getArticlePresseImgMediumFile($articlePresse[ARTICLE_PRESSE_ID]); ?>" img-modal="<?php echo SRC_PATH.PathUtils::getArticlePresseImgFile($articlePresse[ARTICLE_PRESSE_ID]); ?>" title="Afficher l'article">
                </div>
                <?php
                if ((($i + 1) % 2) == 0) {
                    ?>
                    </div><div class="row row-article">
                    <?php
                }
            } else {
                if (($i % 5) == 0) {
                    $colOffset = " col-xs-offset-1";
                }
                ?>
                <div class="col-xs-2<?php echo $colOffset; ?> col-article">
                    <div class="header-article">
                        <div class="date-publication">
                            Publi&eacute; le <?php echo DateUtils::convert_sqlDate_to_slashDate($articlePresse[ARTICLE_PRESSE_DATE]); ?>
                        </div>
                        <b><?php echo $ctrl->getNomJournalArticlePresse($articlePresse[ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID]); ?></b>
                        <br>
                        <a href="<?php echo $articlePresse[ARTICLE_PRESSE_URL]; ?>" target="_blank">
                            <b>Consulter l'article</b>
                        </a>
                    </div>
                    <img src="<?php echo SRC_PATH.PathUtils::getArticlePresseImgSmallFile($articlePresse[ARTICLE_PRESSE_ID]); ?>" img-modal="<?php echo SRC_PATH.PathUtils::getArticlePresseImgFile($articlePresse[ARTICLE_PRESSE_ID]); ?>" title="Afficher l'article">
                </div>
                <?php
                if ((($i + 1) % 5) == 0) {
                    ?>
                    </div><div class="row row-article">
                    <?php
                }
            }
            $i++;
        }
        ?>
        </div>
        <?php
    }
    ?>
</div>

<?php
require_once("webapp/views/common/footer/footer.html.php");

require_once("webapp/views/common/modals/alert/modal-alert.html.php");
require_once("webapp/views/common/modals/img/modal-img.html.php");
?>

<?php
$page->finalizePage();
?>