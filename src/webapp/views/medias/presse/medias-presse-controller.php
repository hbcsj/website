<?php
require_once("core/php/controllers/abstract-page-controller.php");
require_once("common/php/dao/article-presse-dao.php");
require_once("common/php/dao/journal-article-presse-dao.php");

class MediasPresseCtrl extends AbstractPageCtrl {

	private $articlePresseDAO;
	private $journalArticlePresseDAO;

    public function __construct($pageName) {
        parent::__construct($pageName, null, true);
        
        $this->articlePresseDAO = new ArticlePresseDAO($this->getDatabaseConnection());
        $this->journalArticlePresseDAO = new JournalArticlePresseDAO($this->getDatabaseConnection());
    }
    
    public function getArticlesPresse() {
        return $this->articlePresseDAO->getAll(ARTICLE_PRESSE_TABLE_NAME.".".ARTICLE_PRESSE_DATE." DESC");
    }

	public function getNomJournalArticlePresse($journalArticlePresseId) {
		$journal = $this->journalArticlePresseDAO->getById($journalArticlePresseId);
		return $journal[JOURNAL_ARTICLE_PRESSE_NOM];
	}
}

?>