<?php
require_once("core/php/controllers/abstract-page-controller.php");
require_once("common/php/dao/categorie-dao.php");
require_once("common/php/managers/categorie-manager.php");
require_once("common/php/managers/evenement-manager.php");
require_once("common/php/managers/dispo-manager.php");

class DisposCategorieCtrl extends AbstractPageCtrl {
	
	private $categorie;

	private $categorieDAO;

	private $categorieManager;
	private $evenementManager;
	private $dispoManager;

    public function __construct($pageName) {
		parent::__construct($pageName, array(
            "id" => $_GET["id"]
		), true);
		
		if (isDisposConnected()) {
			$this->categorieDAO = new CategorieDAO($this->getDatabaseConnection());

			$this->categorieManager = new CategorieManager($this->getDatabaseConnection());
			$this->evenementManager = new EvenementManager($this->getDatabaseConnection());
			$this->dispoManager = new DispoManager($this->getDatabaseConnection());

			$this->checkRequest();
		}
	}

	private function checkRequest() {
		$checkParams = $this->checkParams(array(
			array(GET, "id")
		));
		if ($checkParams) {
			$this->checkIdCategorie();
		}
	}

	private function checkIdCategorie() {
		$this->categorie = $this->categorieDAO->getById($_GET["id"]);
			
		if ($this->categorie == null) {
			$this->sendCheckError(
				HTTP_404, 
				"La categorie (idCategorie = '".$_GET["id"]."') n'existe pas", 
				"404"
			);
		}
	}

	public function getCategorie() {
		return $this->categorie;
	}

	public function getLicencies() {
		return $this->categorieManager->getLicenciesCategorie($this->categorie[CATEGORIE_ID], true);
	}

	public function getEvenements() {
		return $this->evenementManager->getEvenementsForDispos($this->categorie);
	}

    public function getRecapEvenementProps($evenement) {
        return $this->evenementManager->getRecapEvenementProps($evenement);
	}

	public function getDisposLicencie($licencieId, $evenements) {
		return $this->dispoManager->getDisposLicencieByEvenementsAndTypeDispo($licencieId, $evenements, DISPO_PARTICIPATION_TYPE_ID);
	}

	public function getDisposEvenementsGrouped($evenementsGrouped) {
		return $this->dispoManager->getParticipationsByEvenementsGrouped($evenementsGrouped, $this->categorie[CATEGORIE_ID]);
	}

	public function areAllNoMatchs($evenementsGrouped) {
		$allNoMatchs = true;

		if (sizeof($evenementsGrouped) > 0) {
			foreach ($evenementsGrouped as $evenement) {
				if ($evenement[EVENEMENT_TYPE_EVENEMENT_ID] == EVENEMENT_MATCH_TYPE_ID) {
					$allNoMatchs = false;
					break;
				}
			}
		}

		return $allNoMatchs;
	}
}

?>