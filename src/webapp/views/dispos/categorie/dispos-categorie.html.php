<?php
require_once("core/php/resources/page.php");
require_once("common/php/lib/dispos-utils.php");
require_once("common/php/lib/date-utils.php");

$page = new Page(
    "webapp/views/dispos/categorie/", 
    "dispos-categorie", 
    "DisposCategorieCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/dispos/common/nav/nav.html.php");
?>

<div class="container-fluid animated fadeIn">
    <?php
    if (isDisposConnected()) {
        $categorie = $ctrl->getCategorie();
        $licencies = $ctrl->getLicencies();
        $evenements = $ctrl->getEvenements();
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h1>
                    <?php echo $categorie[CATEGORIE_NOM]; ?>
                    <button id="dispos__bt-matchs-uniquement" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-check"></span>
                        </div>
                        <div class="button__text">Matchs uniquement</div>
                    </button>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3 col-xs-offset-1">
                <?php
                if (sizeof($licencies) > 0) {
                    foreach ($licencies as $licencie) {
                        $disposLicencie = $ctrl->getDisposLicencie($licencie[LICENCIE_ID], $evenements);
                        ?>
                        <div class="row row-licencie" id-licencie="<?php echo $licencie[LICENCIE_ID]; ?>">
                            <?php echo $licencie[LICENCIE_NOM]." ".$licencie[LICENCIE_PRENOM]; ?>
                            <div class="badges">
                                <span class="badge badge-dispo badge-disponible" id-licencie="<?php echo $licencie[LICENCIE_ID]; ?>">0</span>
                                <span class="badge badge-dispo badge-si-necessaire" id-licencie="<?php echo $licencie[LICENCIE_ID]; ?>">0</span>
                                <span class="badge badge-dispo badge-absent" id-licencie="<?php echo $licencie[LICENCIE_ID]; ?>">0</span>
                                <span class="badge badge-dispo badge-pas-de-reponse" id-licencie="<?php echo $licencie[LICENCIE_ID]; ?>">0</span>
                                <input type="hidden" class="nb-disponible" id-licencie="<?php echo $licencie[LICENCIE_ID]; ?>" value="<?php echo sizeof($disposLicencie[DISPO_DISPONIBLE_REPONSE_ID]); ?>">
                                <input type="hidden" class="nb-disponible-total" id-licencie="<?php echo $licencie[LICENCIE_ID]; ?>" value="<?php echo sizeof($disposLicencie[DISPO_DISPONIBLE_REPONSE_ID.DISPO_TOTAL_SUFFIX]); ?>">
                                <input type="hidden" class="nb-si-necessaire" id-licencie="<?php echo $licencie[LICENCIE_ID]; ?>" value="<?php echo sizeof($disposLicencie[DISPO_SI_NECESSAIRE_REPONSE_ID]); ?>">
                                <input type="hidden" class="nb-si-necessaire-total" id-licencie="<?php echo $licencie[LICENCIE_ID]; ?>" value="<?php echo sizeof($disposLicencie[DISPO_SI_NECESSAIRE_REPONSE_ID.DISPO_TOTAL_SUFFIX]); ?>">
                                <input type="hidden" class="nb-absent" id-licencie="<?php echo $licencie[LICENCIE_ID]; ?>" value="<?php echo sizeof($disposLicencie[DISPO_ABSENT_REPONSE_ID]); ?>">
                                <input type="hidden" class="nb-absent-total" id-licencie="<?php echo $licencie[LICENCIE_ID]; ?>" value="<?php echo sizeof($disposLicencie[DISPO_ABSENT_REPONSE_ID.DISPO_TOTAL_SUFFIX]); ?>">
                                <input type="hidden" class="nb-pas-de-reponse" id-licencie="<?php echo $licencie[LICENCIE_ID]; ?>" value="<?php echo sizeof($disposLicencie[DISPO_PAS_DE_REPONSE_REPONSE_ID]); ?>">
                                <input type="hidden" class="nb-pas-de-reponse-total" id-licencie="<?php echo $licencie[LICENCIE_ID]; ?>" value="<?php echo sizeof($disposLicencie[DISPO_PAS_DE_REPONSE_REPONSE_ID.DISPO_TOTAL_SUFFIX]); ?>">
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
            <div class="col-xs-6 col-xs-offset-1">
                <?php
                if (sizeof($evenements) > 0) {
                    foreach ($evenements as $groupKeyEvenements => $evenementsGrouped) {
                        $disposEvenementsGrouped = $ctrl->getDisposEvenementsGrouped($evenementsGrouped);
                        ?>
                        <div class="row row-evenement" group-key-evenements="<?php echo $groupKeyEvenements; ?>" all-no-matchs="<?php echo ($ctrl->areAllNoMatchs($evenementsGrouped)) ? "1" : "0"; ?>">
                            <div class="date-evenement">
                                <?php
                                $dateTimestamp = DateUtils::get_sqlDateTime_timestamp($evenementsGrouped[0][EVENEMENT_DATE_HEURE]);
                                echo DateUtils::getNomJour(date("w", $dateTimestamp))." ".date(SLASH_DATE_FORMAT, $dateTimestamp);
                                ?>
                            </div>
                            <div class="text-evenement">
                                <?php 
                                if (sizeof($evenementsGrouped) > 0) {
                                    foreach ($evenementsGrouped as $evenement) {
                                        $evenementProps = $ctrl->getRecapEvenementProps($evenement);
                                        ?>
                                        <div class="row" is-match="<?php echo ($evenement[EVENEMENT_TYPE_EVENEMENT_ID] == EVENEMENT_MATCH_TYPE_ID) ? "1" : "0"; ?>">
                                            <div class="col-xs-2">
                                                <?php echo date(SLASH_TIME_FORMAT, DateUtils::get_sqlDateTime_timestamp($evenement[EVENEMENT_DATE_HEURE])); ?>
                                            </div>
                                            <div class="col-xs-10">
                                                <b><?php
                                                    if ($evenement[EVENEMENT_TYPE_EVENEMENT_ID] == EVENEMENT_MATCH_TYPE_ID) {
                                                        echo $evenementProps[MATCH_EQUIPE_DOMICILE]." VS ".$evenementProps[MATCH_EQUIPE_EXTERIEUR]."</b>, ";
                                                    } else {
                                                        echo $evenementProps[EVENEMENT_TYPE];
                                                        echo "<br>";
                                                        echo $evenement[EVENEMENT_NOM]."</b>";
                                                        if ($evenement[EVENEMENT_NOM] != "") {
                                                            echo ", ";
                                                        }
                                                    }
                                                    ?>&agrave; <?php echo $evenement[EVENEMENT_VILLE]; ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                            <div class="badges">
                                <span class="badge badge-dispo badge-disponible"><?php echo sizeof($disposEvenementsGrouped[DISPO_DISPONIBLE_REPONSE_ID]); ?></span>
                                <span class="badge badge-dispo badge-si-necessaire"><?php echo sizeof($disposEvenementsGrouped[DISPO_SI_NECESSAIRE_REPONSE_ID]); ?></span>
                                <span class="badge badge-dispo badge-absent"><?php echo sizeof($disposEvenementsGrouped[DISPO_ABSENT_REPONSE_ID]); ?></span>
                                <span class="badge badge-dispo badge-pas-de-reponse"><?php echo sizeof($disposEvenementsGrouped[DISPO_PAS_DE_REPONSE_REPONSE_ID]); ?></span>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
        </div>
        <?php
    } else {
        require_once("webapp/views/common/mdp-form/mdp-form.html.php");
    }
    ?>
</div>

<?php
require_once("webapp/views/common/modals/alert/modal-alert.html.php");
require_once("webapp/views/common/modals/prompt/modal-prompt.html.php");

$page->finalizePage();
?>