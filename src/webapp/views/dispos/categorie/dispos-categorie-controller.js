DisposCategorieController = {};

DisposCategorieController.control_Licencie = function() {
    $('.row-licencie').unbind('click');
    $('.row-licencie').click(function() {
        var idLicencie = $(this).attr('id-licencie');
        window.location.href = rootPath+'dispos/licencie?id='+idLicencie+'&categorie='+Utils.getUrlParams().id;
    });
};

DisposCategorieController.calculDispos = function(onlyMatchs) {
    $('.row-licencie').each(function() {
        var idLicencie = $(this).attr('id-licencie');

        var totalSuffix = '';
        if (!onlyMatchs) {
            totalSuffix = '-total';
        }

        $('.row-licencie[id-licencie="'+idLicencie+'"] span.badge-disponible').html(
            $('input.nb-disponible'+totalSuffix+'[id-licencie="'+idLicencie+'"]').val()
        );
        $('.row-licencie[id-licencie="'+idLicencie+'"] span.badge-si-necessaire').html($('input.nb-si-necessaire'+totalSuffix+'[id-licencie="'+idLicencie+'"]').val());
        $('.row-licencie[id-licencie="'+idLicencie+'"] span.badge-absent').html($('input.nb-absent'+totalSuffix+'[id-licencie="'+idLicencie+'"]').val());
        $('.row-licencie[id-licencie="'+idLicencie+'"] span.badge-pas-de-reponse').html($('input.nb-pas-de-reponse'+totalSuffix+'[id-licencie="'+idLicencie+'"]').val());
    });
};

DisposCategorieController.control_Evenement = function() {
    $('.row-evenement').unbind('click');
    $('.row-evenement').click(function() {
        var groupKeyEvenements = $(this).attr('group-key-evenements');
        window.location.href = rootPath+'dispos/evenement?key='+groupKeyEvenements+'&categorie='+Utils.getUrlParams().id;
    });
};

initializePage = function() {
    DisposController.initializePage('categories');
    DisposController.control_MatchsUniquement(true, function(onlyMatchs) {
        DisposCategorieController.calculDispos(onlyMatchs);
    });

    DisposCategorieController.control_Licencie();
    DisposCategorieController.control_Evenement();
};