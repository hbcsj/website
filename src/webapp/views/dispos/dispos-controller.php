<?php
require_once("core/php/controllers/abstract-page-controller.php");
require_once("common/php/dao/categorie-dao.php");

class DisposCtrl extends AbstractPageCtrl {
	
	private $categorieDAO;

    public function __construct($pageName) {
		parent::__construct($pageName, null, true);
		
		if (isDisposConnected()) {
			$this->categorieDAO = new CategorieDAO($this->getDatabaseConnection());
		}
	}

	public function getCategories() {
		return $this->categorieDAO->getAll(CATEGORIE_TABLE_NAME.".".CATEGORIE_POSITION_AFFICHAGE);
	}
}

?>