<?php
require_once("core/php/controllers/abstract-page-controller.php");
require_once("common/php/dao/licencie-dao.php");
require_once("common/php/dao/categorie-dao.php");
require_once("common/php/managers/evenement-manager.php");
require_once("common/php/managers/dispo-manager.php");

class DisposLicencieCtrl extends AbstractPageCtrl {
	
	private $categorie;
	private $licencie;

	private $licencieDAO;
	private $categorieDAO;

	private $evenementManager;
	private $dispoManager;

    public function __construct($pageName) {
		parent::__construct($pageName, array(
            "id" => $_GET["id"],
            "id" => $_GET["categorie"]
		), true);
		
		if (isDisposConnected()) {
			$this->licencieDAO = new LicencieDAO($this->getDatabaseConnection());
			$this->categorieDAO = new CategorieDAO($this->getDatabaseConnection());

			$this->evenementManager = new EvenementManager($this->getDatabaseConnection());
			$this->dispoManager = new DispoManager($this->getDatabaseConnection());

			$this->checkRequest();
		}
	}

	private function checkRequest() {
		$checkParams = $this->checkParams(array(
			array(GET, "id"),
			array(GET, "categorie")
		));
		if ($checkParams) {
			$this->checkIdLicencie();
			$this->checkIdCategorie();
		}
	}

	private function checkIdLicencie() {
		$this->licencie = $this->licencieDAO->getById($_GET["id"]);
			
		if ($this->licencie == null) {
			$this->sendCheckError(
				HTTP_404, 
				"Le licencie (idLicencie = '".$_GET["id"]."') n'existe pas", 
				"404"
			);
		}
	}

	private function checkIdCategorie() {
		$this->categorie = $this->categorieDAO->getById($_GET["categorie"]);
			
		if ($this->categorie == null) {
			$this->sendCheckError(
				HTTP_404, 
				"La categorie (idCategorie = '".$_GET["categorie"]."') n'existe pas", 
				"404"
			);
		}
	}

	public function getEvenements() {
		return $this->evenementManager->getEvenementsForDispos($this->categorie);
	}

    public function getRecapEvenementProps($evenement) {
        return $this->evenementManager->getRecapEvenementProps($evenement);
	}

	public function getLicencie() {
		return $this->licencie;
	}

	public function areAllNoMatchs($evenementsGrouped) {
		$allNoMatchs = true;

		if (sizeof($evenementsGrouped) > 0) {
			foreach ($evenementsGrouped as $evenement) {
				if ($evenement[EVENEMENT_TYPE_EVENEMENT_ID] == EVENEMENT_MATCH_TYPE_ID) {
					$allNoMatchs = false;
					break;
				}
			}
		}

		return $allNoMatchs;
	}
}

?>