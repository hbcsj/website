DisposLicencieController = {};

DisposLicencieController.control_Evenement = function() {
    $('.col-evenement').unbind('click');
    $('.col-evenement').click(function() {
        var groupKeyEvenements = $(this).attr('group-key-evenements');
        window.location.href = rootPath+'dispos/evenement?key='+groupKeyEvenements+'&categorie='+Utils.getUrlParams().categorie;
    });
};

DisposLicencieController.control_ReponsesDispos = function() {
    $('.row-reponse-dispo').each(function() {
        var key = $(this).attr('group-key-evenements');
        load_FormReponseDispo(key, null);
    });

    function load_FormReponseDispo(groupKeyEvenements, callback) {
        var idReponseDispo = 'row-reponse-dispo-'+groupKeyEvenements;
        AjaxUtils.loadView(
            'dispos/licencie/views/reponse-dispo/form-reponse-dispo',
            '#'+idReponseDispo,
            {
                key: groupKeyEvenements,
                idLicencie: Utils.getUrlParams().id,
                idCategorie: Utils.getUrlParams().categorie
            },
            function() {
                control_btReponsesDispo();

                if (callback != null) {
                    callback();
                }
            },
            '#loader-page'
        );
    }

    function control_btReponsesDispo() {
        $('.bt-reponse-dispo').unbind('click');
        $('.bt-reponse-dispo').click(function() {
            var typeId = $(this).attr('id-type-dispo');
            var reponseId = $(this).attr('id-reponse-dispo');
            var evenementIds = $(this).attr('ids-evenements');
            var groupKeyEvenements = $(this).attr('group-key-evenements');

            $('#loader-page').show();
            AjaxUtils.callService(
                'dispo', 
                'save', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    licencieId: Utils.getUrlParams().id,
                    typeId: typeId,
                    reponseId: reponseId,
                    evenementIds: evenementIds
                }, 
                function(response) {
                    $('#loader-page').hide();
                    if (response.success) {
                        load_FormReponseDispo(groupKeyEvenements, function() {
                            showSuccessMessage('Dispo sauvegard&eacute;e avec succ&egrave;s', groupKeyEvenements);
                        });
                    } else {
                        showWarningMessage('Erreur durant la sauvegarde de la dispo', groupKeyEvenements);
                    }
                },
                function(response) {
                    $('#loader-page').hide();
                    showWarningMessage('Service indisponible', groupKeyEvenements);
                }
            );
        });
    }

    function showSuccessMessage(message, groupKeyEvenements) {
        var elemSelector = '#row-reponse-dispo-'+groupKeyEvenements;
        $(elemSelector+' .warning-message').hide();
        
        var successMessageElem = $(elemSelector+' .success-message');
        successMessageElem.html(message);
        successMessageElem.show();
    }

    function showWarningMessage(message, groupKeyEvenements) {
        var elemSelector = '#row-reponse-dispo-'+groupKeyEvenements;
        $(elemSelector+' .success-message').hide();
        
        var warningMessageElem = $(elemSelector+' .warning-message');
        warningMessageElem.html(message);
        warningMessageElem.show();
    }
};

initializePage = function() {
    DisposController.initializePage('categories');
    DisposController.control_MatchsUniquement(false, null);

    DisposLicencieController.control_Evenement();
    DisposLicencieController.control_ReponsesDispos();
};