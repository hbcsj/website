<?php
require_once("core/php/resources/page.php");
require_once("common/php/lib/dispos-utils.php");
require_once("common/php/lib/date-utils.php");

$page = new Page(
    "webapp/views/dispos/licencie/", 
    "dispos-licencie", 
    "DisposLicencieCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/dispos/common/nav/nav.html.php");
?>

<div class="container-fluid animated fadeIn">
    <?php
    if (isDisposConnected()) {
        $licencie = $ctrl->getLicencie();
        $evenements = $ctrl->getEvenements();
        ?>
        <div class="col-xs-10 col-xs-offset-1">
            <h1>
                <?php echo $licencie[LICENCIE_PRENOM]." ".$licencie[LICENCIE_NOM]; ?>
                <button id="dispos__bt-back" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-arrow-left"></span>
                    </div>
                    <div class="button__text">Retour</div>
                </button>
                <button id="dispos__bt-reload" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-refresh"></span>
                    </div>
                    <div class="button__text">Actualiser</div>
                </button>
                <button id="dispos__bt-matchs-uniquement" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-unchecked"></span>
                    </div>
                    <div class="button__text">Matchs uniquement</div>
                </button>
            </h1>
        </div>
        <div class="col-xs-10 col-xs-offset-1">
            <?php
            if (sizeof($evenements) > 0) {
                foreach ($evenements as $groupKeyEvenements => $evenementsGrouped) {
                    ?>
                    <div class="row row-evenement" all-no-matchs="<?php echo ($ctrl->areAllNoMatchs($evenementsGrouped)) ? "1" : "0"; ?>">
                        <div class="col-xs-5 col-evenement" group-key-evenements="<?php echo $groupKeyEvenements; ?>">
                            <div class="date-evenement">
                                <?php
                                $dateTimestamp = DateUtils::get_sqlDateTime_timestamp($evenementsGrouped[0][EVENEMENT_DATE_HEURE]);
                                echo DateUtils::getNomJour(date("w", $dateTimestamp))." ".date(SLASH_DATE_FORMAT, $dateTimestamp);
                                ?>
                            </div>
                            <div class="text-evenement">
                                <?php 
                                if (sizeof($evenementsGrouped) > 0) {
                                    foreach ($evenementsGrouped as $evenement) {
                                        $evenementProps = $ctrl->getRecapEvenementProps($evenement);
                                        ?>
                                        <div class="row" is-match="<?php echo ($evenement[EVENEMENT_TYPE_EVENEMENT_ID] == EVENEMENT_MATCH_TYPE_ID) ? "1" : "0"; ?>">
                                            <div class="col-xs-2">
                                                <?php echo date(SLASH_TIME_FORMAT, DateUtils::get_sqlDateTime_timestamp($evenement[EVENEMENT_DATE_HEURE])); ?>
                                            </div>
                                            <div class="col-xs-8">
                                                <b><?php
                                                    if ($evenement[EVENEMENT_TYPE_EVENEMENT_ID] == EVENEMENT_MATCH_TYPE_ID) {
                                                        echo $evenementProps[MATCH_EQUIPE_DOMICILE]." VS ".$evenementProps[MATCH_EQUIPE_EXTERIEUR];
                                                    } else {
                                                        echo $evenementProps[EVENEMENT_TYPE];
                                                        echo "<br>";
                                                        echo $evenement[EVENEMENT_NOM];
                                                    }
                                                    ?></b>, &agrave; <?php echo $evenement[EVENEMENT_VILLE]; ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div class="col-xs-7">
                            <div class="row row-reponse-dispo" id="row-reponse-dispo-<?php echo $groupKeyEvenements; ?>" group-key-evenements="<?php echo $groupKeyEvenements; ?>"></div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <?php
    } else {
        require_once("webapp/views/common/mdp-form/mdp-form.html.php");
    }
    ?>
</div>

<?php
require_once("webapp/views/common/modals/alert/modal-alert.html.php");
require_once("webapp/views/common/modals/prompt/modal-prompt.html.php");

$page->finalizePage();
?>