<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/dispos-utils.php");

$view = new View(
    "webapp/views/dispos/licencie/views/reponse-dispo/", 
    "form-reponse-dispo", 
    "FormReponseDispoCtrl"
);
$ctrl = $view->getController();

if (isDisposConnected()) {
    ?>
    <div class="col-xs-4 col-lg-3">
        <button 
            class="btn btn-default bt-reponse-dispo bt-disponible<?php echo ($ctrl->isSelected(DISPO_DISPONIBLE_REPONSE_ID)) ? " bt-selected" : ""; ?>"
            id-type-dispo="<?php echo DISPO_PARTICIPATION_TYPE_ID; ?>"
            id-reponse-dispo="<?php echo DISPO_DISPONIBLE_REPONSE_ID; ?>"
            group-key-evenements="<?php echo $ctrl->getGroupKeyEvenements(); ?>"
            ids-evenements="<?php echo implode(",", $ctrl->getIdsEvenements()); ?>">
            <div class="button__text">Dispo.</div>
        </button>
    </div>
    <div class="col-xs-4 col-lg-3">
        <button 
            class="btn btn-default bt-reponse-dispo bt-si-necessaire<?php echo ($ctrl->isSelected(DISPO_SI_NECESSAIRE_REPONSE_ID)) ? " bt-selected" : ""; ?>"
            id-type-dispo="<?php echo DISPO_PARTICIPATION_TYPE_ID; ?>"
            id-reponse-dispo="<?php echo DISPO_SI_NECESSAIRE_REPONSE_ID; ?>"
            group-key-evenements="<?php echo $ctrl->getGroupKeyEvenements(); ?>"
            ids-evenements="<?php echo implode(",", $ctrl->getIdsEvenements()); ?>">
            <div class="button__text">Si n&eacute;ces.</div>
        </button>
    </div>
    <div class="col-xs-4 col-lg-3">
        <button 
            class="btn btn-default bt-reponse-dispo bt-absent<?php echo ($ctrl->isSelected(DISPO_ABSENT_REPONSE_ID)) ? " bt-selected" : ""; ?>"
            id-type-dispo="<?php echo DISPO_PARTICIPATION_TYPE_ID; ?>"
            id-reponse-dispo="<?php echo DISPO_ABSENT_REPONSE_ID; ?>"
            group-key-evenements="<?php echo $ctrl->getGroupKeyEvenements(); ?>"
            ids-evenements="<?php echo implode(",", $ctrl->getIdsEvenements()); ?>">
            <div class="button__text">Absent</div>
        </button>
    </div>
    <div class="col-xs-4 col-lg-3">
        <button 
            class="btn btn-default bt-reponse-dispo bt-pas-de-reponse<?php echo ($ctrl->isSelected(DISPO_PAS_DE_REPONSE_REPONSE_ID)) ? " bt-selected" : ""; ?>"
            id-type-dispo="<?php echo DISPO_PARTICIPATION_TYPE_ID; ?>"
            id-reponse-dispo="<?php echo DISPO_PAS_DE_REPONSE_REPONSE_ID; ?>"
            group-key-evenements="<?php echo $ctrl->getGroupKeyEvenements(); ?>"
            ids-evenements="<?php echo implode(",", $ctrl->getIdsEvenements()); ?>">
            <div class="button__text">?</div>
        </button>
    </div>
    <div class="success-message"></div>
    <div class="warning-message"></div>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>