<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/licencie-dao.php");
require_once("common/php/dao/categorie-dao.php");
require_once("common/php/dao/dispo-dao.php");
require_once("common/php/lib/dispos-utils.php");
require_once("common/php/managers/evenement-manager.php");

class FormReponseDispoCtrl extends AbstractViewCtrl {
	
	private $categorie;
	private $licencie;

    private $licencieDAO;
    private $categorieDAO;
    private $dispoDAO;
    
	private $evenementManager;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "key" => $_GET["key"],
            "idLicencie" => $_GET["idLicencie"],
            "idCategorie" => $_GET["idCategorie"]
		), true);

		if (isDisposConnected()) {
			$this->licencieDAO = new LicencieDAO($this->getDatabaseConnection());
			$this->categorieDAO = new CategorieDAO($this->getDatabaseConnection());
			$this->dispoDAO = new DispoDAO($this->getDatabaseConnection());

			$this->evenementManager = new EvenementManager($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$checkParams = $this->checkParams(array(
			array(GET, "key"),
			array(GET, "idLicencie"),
			array(GET, "idCategorie")
		));
		if ($checkParams) {
			$this->checkIdLicencie();
			$this->checkIdCategorie();
			$this->checkEvenementsGroupedKey();
		}
	}

	private function checkIdLicencie() {
		$this->licencie = $this->licencieDAO->getById($_GET["idLicencie"]);
			
		if ($this->licencie == null) {
			$this->sendCheckError(
				HTTP_404, 
				"Le licencie (idLicencie = '".$_GET["idLicencie"]."') n'existe pas", 
				"webapp/views/common/error/404/404.html.php"
			);
		}
	}

	private function checkIdCategorie() {
		$this->categorie = $this->categorieDAO->getById($_GET["idCategorie"]);
			
		if ($this->categorie == null) {
			$this->sendCheckError(
				HTTP_404, 
				"La categorie (idCategorie = '".$_GET["idCategorie"]."') n'existe pas", 
				"webapp/views/common/error/404/404.html.php"
			);
		}
	}

	private function checkEvenementsGroupedKey() {
		$this->evenementsGrouped = $this->evenementManager->getEvenementsGroupedForDisposByKey($_GET["key"], $this->categorie);

		if ($this->evenementsGrouped == null || sizeof($this->evenementsGrouped) == 0) {
			$this->sendCheckError(
				HTTP_404, 
				"Il n'y a aucun evenement (key = '".$_GET["key"]."') groupes pour les dispos", 
				"webapp/views/common/error/404/404.html.php"
			);
		}
    }
    
    public function isSelected($reponseDispoId) {
        $dispoId = array();
        $dispoId[DISPO_LICENCIE_ID] = $this->licencie[LICENCIE_ID];
        $dispoId[DISPO_EVENEMENT_ID] = $this->evenementsGrouped[0][EVENEMENT_ID];
        $dispoId[DISPO_TYPE_DISPO_ID] = DISPO_PARTICIPATION_TYPE_ID;
        $dispo = $this->dispoDAO->getById($dispoId);

        if ($reponseDispoId == DISPO_PAS_DE_REPONSE_REPONSE_ID) {
            return ($dispo == null);
        } else {
            return ($dispo[DISPO_REPONSE_DISPO_ID] == $reponseDispoId);
        }
    }

    public function getIdsEvenements() {
        $evenementsIds = array();

        if (sizeof($this->evenementsGrouped) > 0) {
            foreach ($this->evenementsGrouped as $evenement) {
                $evenementsIds[] = $evenement[EVENEMENT_ID];
            }
        }

        return $evenementsIds;
	}
	
	public function getGroupKeyEvenements() {
		return $_GET["key"];
	}
}

?>