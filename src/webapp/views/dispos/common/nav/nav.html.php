<?php
require_once("common/php/lib/dispos-utils.php");
?>

<nav>
    <div id="nav-logo">
        <img src="<?php echo SRC_PATH; ?>assets/img/logo-dispos.png">
    </div>
    <div class="nav-links">
        <?php
        if (isDisposConnected()) {
            ?>
            <a href="<?php echo ROOT_PATH; ?>dispos" page="categories">Cat&eacute;gories</a>
            <a href="<?php echo ROOT_PATH; ?>dispos/actions" page="actions">Actions club &amp; Arbitrages</a>
            <a href="#" id="link-logout" title="D&eacute;connexion">
                <span class="glyphicon glyphicon-off"></span>
            </a>
            <?php
        }
        ?>
    </div>
    <div class="loader" id="loader-page"></div>
</nav>
