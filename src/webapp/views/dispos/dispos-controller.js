initializePage = function() {
    DisposController.initializePage('categories');

    $('.nom-categorie').unbind('click');
    $('.nom-categorie').click(function() {
        var idCategorie = $(this).attr('id-categorie');
        window.location.href = rootPath+'dispos/categorie?id='+idCategorie;
    });
};