<?php
require_once("core/php/resources/page.php");
require_once("common/php/lib/dispos-utils.php");
require_once("common/php/lib/date-utils.php");

$page = new Page(
    "webapp/views/dispos/evenement/", 
    "dispos-evenement", 
    "DisposEvenementCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/dispos/common/nav/nav.html.php");
?>

<div class="container-fluid animated fadeIn">
    <?php
    if (isDisposConnected()) {
        $categorie = $ctrl->getCategorie();
        $evenementsGrouped = $ctrl->getEvenementsGrouped();
        $disposEvenementsGrouped = $ctrl->getDisposEvenementsGrouped($evenementsGrouped);
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h1>
                    <?php
                    $dateTimestamp = DateUtils::get_sqlDateTime_timestamp($evenementsGrouped[0][EVENEMENT_DATE_HEURE]);
                    echo DateUtils::getNomJour(date("w", $dateTimestamp))." ".date(SLASH_DATE_FORMAT, $dateTimestamp);
                    ?>
                    <button id="dispos__bt-back" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-arrow-left"></span>
                        </div>
                        <div class="button__text">Retour</div>
                    </button>
                    <button id="dispos__bt-reload" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-refresh"></span>
                        </div>
                        <div class="button__text">Actualiser</div>
                    </button>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <?php
                if (sizeof($evenementsGrouped) > 0) {
                    foreach ($evenementsGrouped as $evenement) {
                        $evenementProps = $ctrl->getRecapEvenementProps($evenement);
                        ?>
                        <div class="row row-evenement">
                            <div class="col-xs-2"><?php echo date(SLASH_TIME_FORMAT, DateUtils::get_sqlDateTime_timestamp($evenement[EVENEMENT_DATE_HEURE])); ?></div>
                            <div class="col-xs-10">
                                <?php
                                if ($evenement[EVENEMENT_TYPE_EVENEMENT_ID] == EVENEMENT_MATCH_TYPE_ID) {
                                    echo "<b>".$evenementProps[MATCH_EQUIPE_DOMICILE]." VS ".$evenementProps[MATCH_EQUIPE_EXTERIEUR]."</b>";
                                } else {
                                    echo "<b>".$evenementProps[EVENEMENT_TYPE];
                                    echo "<br>";
                                    echo $evenement[EVENEMENT_NOM]."</b>";
                                }

                                if ($evenementProps[EVENEMENT_GYMNASE] != "") {
                                    echo "<br>".$evenementProps[EVENEMENT_GYMNASE];
                                }

                                if ($evenement[EVENEMENT_ADRESSE] != "") {
                                    echo "<br>".$evenement[EVENEMENT_ADRESSE];
                                }
                                
                                if ($evenement[EVENEMENT_VILLE] != "") {
                                    echo "<br>".$evenement[EVENEMENT_CODE_POSTAL]." ".$evenement[EVENEMENT_VILLE];
                                }

                                if ($evenementProps[EVENEMENT_GOOGLE_MAP_URL] != "") {
                                    echo "<br><a href=\"".$evenementProps[EVENEMENT_GOOGLE_MAP_URL]."\" target=\"blank\">Afficher sur Google Map</a>"; 
                                }
                                
                                if ($evenement[EVENEMENT_LATITUDE] != "" && $evenement[EVENEMENT_LONGITUDE] != "") {
                                    echo "<br><b>GPS : </b>".$evenement[EVENEMENT_LATITUDE].", ".$evenement[EVENEMENT_LONGITUDE];
                                }
                                ?>
                            </div>
                        </div>
                    <?php
                    }
                }
                ?>
            </div>
        </div>
        <div class="row row-recap-dispos">
            <div class="col-xs-10 col-xs-offset-1">
                <div class="titre-recap-dispos disponible">Disponibles (<?php echo sizeof($disposEvenementsGrouped[DISPO_DISPONIBLE_REPONSE_ID]); ?>)</div>
                <div class="liste-licencies-recap-dispos">
                    <?php
                    if (sizeof($disposEvenementsGrouped[DISPO_DISPONIBLE_REPONSE_ID]) > 0) {
                        $licencies = $ctrl->getLicenciesFromDispos($disposEvenementsGrouped[DISPO_DISPONIBLE_REPONSE_ID]);
                        ?>
                        <div class="row">
                            <?php
                            foreach ($licencies as $licencie) {
                                ?>
                                <div class="col-xs-3 col-licencie"><?php echo $licencie[LICENCIE_NOM]." ".$licencie[LICENCIE_PRENOM]; ?></div>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="row row-recap-dispos">
            <div class="col-xs-10 col-xs-offset-1">
                <div class="titre-recap-dispos si-necessaire">Si n&eacute;cessaire (<?php echo sizeof($disposEvenementsGrouped[DISPO_SI_NECESSAIRE_REPONSE_ID]); ?>)</div>
                <div class="liste-licencies-recap-dispos">
                    <?php
                    if (sizeof($disposEvenementsGrouped[DISPO_SI_NECESSAIRE_REPONSE_ID]) > 0) {
                        $licencies = $ctrl->getLicenciesFromDispos($disposEvenementsGrouped[DISPO_SI_NECESSAIRE_REPONSE_ID]);
                        ?>
                        <div class="row">
                            <?php
                            foreach ($licencies as $licencie) {
                                ?>
                                <div class="col-xs-3 col-licencie"><?php echo $licencie[LICENCIE_NOM]." ".$licencie[LICENCIE_PRENOM]; ?></div>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="row row-recap-dispos">
            <div class="col-xs-10 col-xs-offset-1">
                <div class="titre-recap-dispos absent">Absents (<?php echo sizeof($disposEvenementsGrouped[DISPO_ABSENT_REPONSE_ID]); ?>)</div>
                <div class="liste-licencies-recap-dispos">
                    <?php
                    if (sizeof($disposEvenementsGrouped[DISPO_ABSENT_REPONSE_ID]) > 0) {
                        $licencies = $ctrl->getLicenciesFromDispos($disposEvenementsGrouped[DISPO_ABSENT_REPONSE_ID]);
                        ?>
                        <div class="row">
                            <?php
                            foreach ($licencies as $licencie) {
                                ?>
                                <div class="col-xs-3 col-licencie"><?php echo $licencie[LICENCIE_NOM]." ".$licencie[LICENCIE_PRENOM]; ?></div>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="row row-recap-dispos">
            <div class="col-xs-10 col-xs-offset-1">
                <div class="titre-recap-dispos pas-de-reponse">Pas de r&eacute;ponse (<?php echo sizeof($disposEvenementsGrouped[DISPO_PAS_DE_REPONSE_REPONSE_ID]); ?>)</div>
                <div class="liste-licencies-recap-dispos">
                    <?php
                    if (sizeof($disposEvenementsGrouped[DISPO_PAS_DE_REPONSE_REPONSE_ID]) > 0) {
                        $licencies = $ctrl->getLicenciesFromDispos($disposEvenementsGrouped[DISPO_PAS_DE_REPONSE_REPONSE_ID]);
                        ?>
                        <div class="row">
                            <?php
                            foreach ($licencies as $licencie) {
                                ?>
                                <div class="col-xs-3 col-licencie"><?php echo $licencie[LICENCIE_NOM]." ".$licencie[LICENCIE_PRENOM]; ?></div>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php
    } else {
        require_once("webapp/views/common/mdp-form/mdp-form.html.php");
    }
    ?>
</div>

<?php
require_once("webapp/views/common/modals/alert/modal-alert.html.php");
require_once("webapp/views/common/modals/prompt/modal-prompt.html.php");

$page->finalizePage();
?>