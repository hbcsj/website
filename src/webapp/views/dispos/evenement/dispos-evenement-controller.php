<?php
require_once("core/php/controllers/abstract-page-controller.php");
require_once("common/php/dao/categorie-dao.php");
require_once("common/php/dao/licencie-dao.php");
require_once("common/php/managers/evenement-manager.php");
require_once("common/php/managers/dispo-manager.php");

class DisposEvenementCtrl extends AbstractPageCtrl {
	
	private $categorie;
	private $evenementsGrouped;

	private $categorieDAO;
	private $licencieDAO;

	private $evenementManager;
	private $dispoManager;

    public function __construct($pageName) {
		parent::__construct($pageName, array(
            "key" => $_GET["key"],
            "categorie" => $_GET["categorie"]
		), true);
		
		if (isDisposConnected()) {
			$this->categorieDAO = new CategorieDAO($this->getDatabaseConnection());
			$this->licencieDAO = new LicencieDAO($this->getDatabaseConnection());

			$this->evenementManager = new EvenementManager($this->getDatabaseConnection());
			$this->dispoManager = new DispoManager($this->getDatabaseConnection());

			$this->checkRequest();
		}
	}

	private function checkRequest() {
		$checkParams = $this->checkParams(array(
			array(GET, "key"),
			array(GET, "categorie")
		));
		if ($checkParams) {
			$this->checkIdCategorie();
			$this->checkEvenementsGroupedKey();
		}
	}

	private function checkIdCategorie() {
		$this->categorie = $this->categorieDAO->getById($_GET["categorie"]);
			
		if ($this->categorie == null) {
			$this->sendCheckError(
				HTTP_404, 
				"La categorie (idCategorie = '".$_GET["categorie"]."') n'existe pas", 
				"404"
			);
		}
	}

	private function checkEvenementsGroupedKey() {
		$this->evenementsGrouped = $this->evenementManager->getEvenementsGroupedForDisposByKey($_GET["key"], $this->categorie);

		if ($this->evenementsGrouped == null || sizeof($this->evenementsGrouped) == 0) {
			$this->sendCheckError(
				HTTP_404, 
				"Il n'y a aucun evenement (key = '".$_GET["key"]."') groupes pour les dispos", 
				"404"
			);
		}
	}

	public function getRecapEvenementProps($evenement) {
		return $this->evenementManager->getRecapEvenementProps($evenement);
	}

	public function getCategorie() {
		return $this->categorie;
	}

	public function getEvenementsGrouped() {
		return $this->evenementsGrouped;
	}

	public function getDisposEvenementsGrouped($evenementsGrouped) {
		return $this->dispoManager->getParticipationsByEvenementsGrouped($evenementsGrouped, $this->categorie[CATEGORIE_ID]);
	}

	public function getLicenciesFromDispos($dispos) {
		$licencies = array();
		$licencieIds = array();

		if (sizeof($dispos) > 0) {
			foreach ($dispos as $dispo) {
				$licencieIds[] = $dispo[DISPO_LICENCIE_ID];
			}
			$licencies = $this->licencieDAO->getInIds(implode(",", $licencieIds), LICENCIE_TABLE_NAME.".".LICENCIE_NOM.", ".LICENCIE_TABLE_NAME.".".LICENCIE_PRENOM);
		}

		return $licencies;
	}
}

?>