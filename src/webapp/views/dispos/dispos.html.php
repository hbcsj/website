<?php
require_once("core/php/resources/page.php");
require_once("common/php/lib/dispos-utils.php");

$page = new Page(
    "webapp/views/dispos/", 
    "dispos", 
    "DisposCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/dispos/common/nav/nav.html.php");
?>

<div class="container-fluid animated fadeIn">
    <?php
    if (isDisposConnected()) {
        $categories = $ctrl->getCategories();
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h1>S&eacute;lectionnez une cat&eacute;gorie</h1>
            </div>
        </div>
        <?php
        if (sizeof($categories) > 0) {
            ?>
            <div class="row">
            <?php
            $i = 0;
            foreach ($categories as $categorie) {
                $colOffset = "";
                if (($i % 5) == 0) {
                    $colOffset = " col-xs-offset-1";
                }
                ?>
                <div class="col-xs-2<?php echo $colOffset; ?> col-categorie">
                    <div class="nom-categorie" id-categorie="<?php echo $categorie[CATEGORIE_ID]; ?>"><?php echo str_replace(" ", "<br>", $categorie[CATEGORIE_NOM]); ?></div>
                </div>
                <?php
                if ((($i + 1) % 5) == 0) {
                    ?>
                    </div><div class="row">
                    <?php
                }
                $i++;
            }
            ?>
            </div>
            <?php
        }
    } else {
        require_once("webapp/views/common/mdp-form/mdp-form.html.php");
    }
    ?>
</div>

<?php
require_once("webapp/views/common/modals/alert/modal-alert.html.php");
require_once("webapp/views/common/modals/prompt/modal-prompt.html.php");

$page->finalizePage();
?>