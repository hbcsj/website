<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/dispos-utils.php");
require_once("common/php/dao/evenement-dao.php");
require_once("common/php/dao/categorie-dao.php");
require_once("common/php/dao/licencie-dao.php");
require_once("common/php/dao/dispo-dao.php");
require_once("common/php/managers/evenement-manager.php");

class ModalSaveActionCtrl extends AbstractViewCtrl {
	
	private $idTypeDispo;
	private $evenement;

	private $evenementDAO;
	private $categorieDAO;
	private $licencieDAO;

	private $evenementManager;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idTypeDispo" => $_GET["idTypeDispo"],
            "idEvenement" => $_GET["idEvenement"]
		), true);
		
		if (isDisposConnected()) {
			$this->evenementDAO = new EvenementDAO($this->getDatabaseConnection());
			$this->licencieDAO = new LicencieDAO($this->getDatabaseConnection());
			$this->categorieDAO = new CategorieDAO($this->getDatabaseConnection());

			$this->evenementManager = new EvenementManager($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$checkParams = $this->checkParams(array(
			array(GET, "idTypeDispo"),
			array(GET, "idEvenement")
		));
		if ($checkParams) {
			$this->checkIdTypeDispo();
			$this->checkIdEvenement();
		}
	}

	private function checkIdTypeDispo() {
		$this->idTypeDispo = $_GET["idTypeDispo"];
			
		if ($this->idTypeDispo != DISPO_ARBITRAGE_TYPE_ID && $this->idTypeDispo != DISPO_ACTION_CLUB_TYPE_ID) {
			$this->sendCheckError(
				HTTP_404, 
				"Le type de dispo doit etre 'action club' ou 'arbitrage'", 
				"webapp/views/common/error/404/404.html.php"
			);
		}
	}

	private function checkIdEvenement() {
		$this->evenement = $this->evenementDAO->getById($_GET["idEvenement"]);
			
		if ($this->evenement == null) {
			$this->sendCheckError(
				HTTP_404, 
				"L'evenement (idEvenement = '".$_GET["idEvenement"]."') n'existe pas", 
				"webapp/views/common/error/404/404.html.php"
			);
		}
	}

	public function getEvenement() {
		return $this->evenement;
	}

    public function getRecapEvenementProps() {
        return $this->evenementManager->getRecapEvenementProps($this->evenement);
	}

	public function toutesLesCategoriesParticipentAEvenement($categoriesEvenement) {
		return (sizeof($categoriesEvenement) == sizeof($this->categorieDAO->getAll()));
	}

	public function getLicencies($typeDispoId) {
		$arbitre = ($typeDispoId == DISPO_ARBITRAGE_TYPE_ID) ? "1" : null;
		return $this->licencieDAO->getNotInTypeDispoIdByEvenementIdAndReponseId(
			$typeDispoId, 
			$this->evenement[EVENEMENT_ID], 
			DISPO_DISPONIBLE_REPONSE_ID,
			$arbitre,
			LICENCIE_TABLE_NAME.".".LICENCIE_NOM.", ".LICENCIE_TABLE_NAME.".".LICENCIE_PRENOM
		);
	}

	public function getIdTypeDispo() {
		return $this->idTypeDispo;
	}
}

?>