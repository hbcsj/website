<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/dispos-utils.php");

$view = new View(
    "webapp/views/dispos/actions/modals/save/", 
    "modal-save-action", 
    "ModalSaveActionCtrl"
);
$ctrl = $view->getController();

if (isDisposConnected()) {
    $idTypeDispo = $ctrl->getIdTypeDispo();
    $evenement = $ctrl->getEvenement();
    $evenementProps = $ctrl->getRecapEvenementProps();
    ?>
    <div class="modal-header">
        <div class="modal-title">
            Inscrire un licenci&eacute; pour
            <?php
            if ($idTypeDispo == DISPO_ARBITRAGE_TYPE_ID) {
                echo "un arbitrage";
            } else if ($idTypeDispo == DISPO_ACTION_CLUB_TYPE_ID) {
                echo "une action club";
            }
            ?>
        </div>
        <div class="loader loader-modal" for="form-save-action"></div>
    </div>
    <div class="modal-body">
        <div class="form-horizontal" id="form-save-action">
            <div class="row row-recap-evenement">
                <div class="col-xs-12">
                    <?php
                    if ($evenement[EVENEMENT_TYPE_EVENEMENT_ID] == EVENEMENT_MATCH_TYPE_ID) {
                        echo $evenementProps[MATCH_EQUIPE_DOMICILE]." VS ".$evenementProps[MATCH_EQUIPE_EXTERIEUR];
                    } else {
                        if (sizeof($evenementProps[EVENEMENT_CATEGORIES]) > 0) {
                            if (!$ctrl->toutesLesCategoriesParticipentAEvenement($evenementProps[EVENEMENT_CATEGORIES])) {
                                $nomsCategories = array();
                                foreach ($evenementProps[EVENEMENT_CATEGORIES] as $evenementCategorie) {
                                    $nomsCategories[] = $evenementCategorie[CATEGORIE_NOM];
                                }
                                echo implode(", ", $nomsCategories)." - ";
                            }
                        }
                        echo $evenementProps[EVENEMENT_TYPE];
                    }
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label for="form-save-action__input-licencie" class="col-xs-4 control-label required">Licenci&eacute; :</label>
                <div class="col-xs-7 form-input" for="form-save-action__input-licencie">
                    <select class="form-control" id="form-save-action__input-licencie" required>
                        <option value=""></option>
                        <?php 
                        $licenciesActionsClub = $ctrl->getLicencies($idTypeDispo);
                        if (sizeof($licenciesActionsClub) > 0) {
                            foreach ($licenciesActionsClub as $licencie) {
                                ?>
                                <option value="<?php echo $licencie[LICENCIE_ID]; ?>"><?php echo $licencie[LICENCIE_NOM]." ".$licencie[LICENCIE_PRENOM]; ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-action__input-licencie"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-required text-right">Les champs marqu&eacute;s d'une * sont obligatoires</div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-8">
                <div class="form-result-message"></div>
            </div>
            <div class="col-xs-4 text-right">
                <button id="modal-save-action__bt-inscrire" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-ok"></span>
                    </div>
                    <div class="button__text">Inscrire</div>
                    <div class="loader"></div>
                </button>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>