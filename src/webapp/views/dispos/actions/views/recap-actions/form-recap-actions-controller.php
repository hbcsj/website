<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/evenement-dao.php");
require_once("common/php/dao/licencie-dao.php");
require_once("common/php/dao/dispo-dao.php");
require_once("common/php/lib/dispos-utils.php");

class FormRecapActionsCtrl extends AbstractViewCtrl {

	private $evenement;
	private $actionsClub;
	private $arbitrages;

	private $evenementDAO;
	private $licencieDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idEvenement" => $_GET["idEvenement"]
		), true);

		if (isDisposConnected()) {
			$this->evenementDAO = new EvenementDAO($this->getDatabaseConnection());
			$this->licencieDAO = new LicencieDAO($this->getDatabaseConnection());
			$this->dispoDAO = new DispoDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$checkParams = $this->checkParams(array(
			array(GET, "idEvenement")
		));
		if ($checkParams) {
			$this->checkIdEvenement();
		}
	}

	private function checkIdEvenement() {
		$this->evenement = $this->evenementDAO->getById($_GET["idEvenement"]);
			
		if ($this->evenement == null) {
			$this->sendCheckError(
				HTTP_404, 
				"L'evenement' (idEvenement = '".$_GET["idEvenement"]."') n'existe pas", 
				"webapp/views/common/error/404/404.html.php"
			);
		}
	}

	public function getInscriptionsActionsClub() {
		return $this->dispoDAO->getByEvenementIdAndTypeDispoIdAndReponseId(
			$this->evenement[EVENEMENT_ID], 
			DISPO_ACTION_CLUB_TYPE_ID, 
			DISPO_DISPONIBLE_REPONSE_ID
		);
	}

	public function getInscriptionsArbitrages() {
		return $this->dispoDAO->getByEvenementIdAndTypeDispoIdAndReponseId(
			$this->evenement[EVENEMENT_ID], 
			DISPO_ARBITRAGE_TYPE_ID, 
			DISPO_DISPONIBLE_REPONSE_ID
		);
	}

	public function getLicencie($licencieId) {
		return $this->licencieDAO->getById($licencieId);
	}

	public function getIdTypeDispoActionClub() {
		return DISPO_ACTION_CLUB_TYPE_ID;
	}

	public function getIdTypeDispoArbitrage() {
		return DISPO_ARBITRAGE_TYPE_ID;
	}

	public function getIdEvenement() {
		return $this->evenement[EVENEMENT_ID];
	}

	public function isADomicile() {
		return ($this->evenement[EVENEMENT_A_DOMICILE] == 1);
	}
}

?>