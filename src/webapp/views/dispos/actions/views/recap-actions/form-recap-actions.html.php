<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/dispos-utils.php");

$view = new View(
    "webapp/views/dispos/actions/views/recap-actions/", 
    "form-recap-actions", 
    "FormRecapActionsCtrl"
);
$ctrl = $view->getController();

if (isDisposConnected()) {
    $inscriptionsActionsClub = $ctrl->getInscriptionsActionsClub();
    $inscriptionsArbitrages = $ctrl->getInscriptionsArbitrages();
    ?>
    <div class="col-xs-6">
        <h2>
            Actions club
            <span class="badge badge-action-club"><?php echo sizeof($inscriptionsActionsClub); ?></span>
        </h2>
        <?php
        if (sizeof($inscriptionsActionsClub) > 0) {
            foreach ($inscriptionsActionsClub as $inscriptionActionClub) {
                $licencieActionClub = $ctrl->getLicencie($inscriptionActionClub[DISPO_LICENCIE_ID]);
                ?>
                <div class="row">
                    <div class="col-xs-8"><?php echo $licencieActionClub[LICENCIE_NOM]." ".$licencieActionClub[LICENCIE_PRENOM]; ?></div>
                    <div class="col-xs-4">
                        <button class="btn btn-default bt-supprimer-action"
                            id-evenement="<?php echo $ctrl->getIdEvenement(); ?>" 
                            id-licencie="<?php echo $licencieActionClub[LICENCIE_ID]; ?>"
                            id-type-dispo="<?php echo $ctrl->getIdTypeDispoActionClub(); ?>">
                            <div class="button__icon">
                                <span class="glyphicon glyphicon-trash"></span>
                            </div>
                            <div class="button__text">Sup.</div>
                        </button>
                    </div>
                </div>
                <?php
            }
        }
        ?>
        <div class="row">
            <div class="col-xs-12">
                <button class="btn btn-default bt-inscription-action" id-type-dispo="<?php echo $ctrl->getIdTypeDispoActionClub(); ?>" id-evenement="<?php echo $ctrl->getIdEvenement(); ?>">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-plus"></span>
                    </div>
                    <div class="button__text">Ajouter une inscription</div>
                </button>
            </div>
        </div>
    </div>
    <?php
    if ($ctrl->isADomicile()) {
        ?>
        <div class="col-xs-6">
            <h2>
                Arbitrages
                <span class="badge badge-arbitrage"><?php echo sizeof($inscriptionsArbitrages); ?></span>
            </h2>
            <?php
            if (sizeof($inscriptionsArbitrages) > 0) {
                foreach ($inscriptionsArbitrages as $inscriptionArbitrage) {
                    $licencieArbitrage = $ctrl->getLicencie($inscriptionArbitrage[DISPO_LICENCIE_ID]);
                    ?>
                    <div class="row">
                        <div class="col-xs-8"><?php echo $licencieArbitrage[LICENCIE_NOM]." ".$licencieArbitrage[LICENCIE_PRENOM]; ?></div>
                        <div class="col-xs-4">
                            <button class="btn btn-default bt-supprimer-action" 
                                id-evenement="<?php echo $ctrl->getIdEvenement(); ?>" 
                                id-licencie="<?php echo $licencieArbitrage[LICENCIE_ID]; ?>"
                                id-type-dispo="<?php echo $ctrl->getIdTypeDispoArbitrage(); ?>">
                                <div class="button__icon">
                                    <span class="glyphicon glyphicon-trash"></span>
                                </div>
                                <div class="button__text">Sup.</div>
                            </button>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
            <div class="row">
                <div class="col-xs-12">
                    <button class="btn btn-default bt-inscription-action" id-type-dispo="<?php echo $ctrl->getIdTypeDispoArbitrage(); ?>" id-evenement="<?php echo $ctrl->getIdEvenement(); ?>">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-plus"></span>
                        </div>
                        <div class="button__text">Ajouter une inscription</div>
                    </button>
                </div>
            </div>
        </div>
    <?php
    }
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>