<?php
require_once("core/php/controllers/abstract-page-controller.php");
require_once("common/php/managers/evenement-manager.php");

class DisposActionsCtrl extends AbstractPageCtrl {
	
	private $evenementManager;

    public function __construct($pageName) {
		parent::__construct($pageName, null, true);
		
		if (isDisposConnected()) {
			$this->evenementManager = new EvenementManager($this->getDatabaseConnection());
		}
	}

	public function getEvenements() {
		return $this->evenementManager->getEvenementsForActionsClubAndArbitrages();
	}

    public function getRecapEvenementProps($evenement) {
        return $this->evenementManager->getRecapEvenementProps($evenement);
	}
}

?>