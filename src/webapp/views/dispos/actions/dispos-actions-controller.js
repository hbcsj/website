DisposActionsController = {};

DisposActionsController.control_RecapActions = function() {
    $('.row-recap-actions').each(function() {
        var idEvenement = $(this).attr('id-evenement');
        load_FormRecapActions(idEvenement, null);
    });

    function load_FormRecapActions(idEvenement) {
        var idRecapActions = 'row-recap-actions-'+idEvenement;
        AjaxUtils.loadView(
            'dispos/actions/views/recap-actions/form-recap-actions',
            '#'+idRecapActions,
            {
                idEvenement: idEvenement
            },
            function() {
                onClick_btInscriptionAction();
                control_DeleteInscriptionAction();
            },
            '#loader-page'
        );

        function onClick_btInscriptionAction() {
            $('.bt-inscription-action').unbind('click');
            $('.bt-inscription-action').click(function() {
                var idTypeDispo = $(this).attr('id-type-dispo');
                var idEvenement = $(this).attr('id-evenement');
                AjaxUtils.loadView(
                    'dispos/actions/modals/save/modal-save-action-content',
                    '#modal-save-action-content',
                    {
                        idTypeDispo: idTypeDispo,
                        idEvenement: idEvenement
                    },
                    function() {
                        onClick_btInscrireInscriptionAction(idTypeDispo, idEvenement);
                    },
                    '#loader-page'
                );
                $('#modal-save-action').modal('show');
            });

            function onClick_btInscrireInscriptionAction(idTypeDispo, idEvenement) {
                $('#modal-save-action__bt-inscrire').unbind('click');
                $('#modal-save-action__bt-inscrire').click(function() {
                    FormUtils.hideFormErrors('#modal-save-action #form-save-action');
                    ViewUtils.unsetHTMLAndHide('#modal-save-action .form-result-message');
                    ViewUtils.desactiveButtonAndShowLoader('#modal-save-action__bt-inscrire', '#modal-save-action__bt-inscrire .loader');

                    var formValidation = FormUtils.validateForm('#modal-save-action #form-save-action');
                
                    if (formValidation) {
                        var inputLicencie = $('#form-save-action__input-licencie').val();

                        saveAction(idTypeDispo, idEvenement, inputLicencie);
                    } else {
                        ViewUtils.activeButtonAndHideLoader('#modal-save-action__bt-inscrire', '#modal-save-action__bt-inscrire .loader');
                    }
                });

                function saveAction(idTypeDispo, idEvenement, inputLicencie) {
                    AjaxUtils.callService(
                        'dispo', 
                        'saveAction', 
                        AjaxUtils.dataTypes.json, 
                        AjaxUtils.requestTypes.post, 
                        null, 
                        {
                            typeId: idTypeDispo,
                            evenementId: idEvenement,
                            licencieId: inputLicencie
                        }, 
                        function(response) {
                            if (response.success) {
                                FormUtils.displayFormResultMessage('#modal-save-action .form-result-message', 'Action sauvegard&eacute;e', FormUtils.resultsTypes.ok);
                                $('#modal-save-action__bt-inscrire .loader').hide();
                                load_FormRecapActions(idEvenement);
                            } else {
                                ViewUtils.activeButtonAndHideLoader('#modal-save-action__bt-inscrire', '#modal-save-action__bt-inscrire .loader');
                                FormUtils.displayFormResultMessage('#modal-save-action .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
                            }
                        },
                        function(response) {
                            ViewUtils.activeButtonAndHideLoader('#modal-save-action__bt-inscrire', '#modal-save-action__bt-inscrire .loader');
                            FormUtils.displayFormResultMessage('#modal-save-action .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                        }
                    );
                }
            }
        }

        function control_DeleteInscriptionAction() {
            $('.bt-supprimer-action').unbind('click');
            $('.bt-supprimer-action').click(function() {
                var idTypeDispo = $(this).attr('id-type-dispo');
                var idEvenement = $(this).attr('id-evenement');
                var idLicencie = $(this).attr('id-licencie');
            
                ViewUtils.prompt(
                    'Supprimer cette inscription ?', 
                    {
                        btOkText: 'Oui',
                        btKoText: 'Non'
                    },
                    function() {
                        deleteInscription(idTypeDispo, idEvenement, idLicencie);
                    }, 
                    null, 
                    null
                );
            });

            function deleteInscription(idTypeDispo, idEvenement, idLicencie) {
                $('#loader-page').show();
                AjaxUtils.callService(
                    'dispo', 
                    'deleteAction', 
                    AjaxUtils.dataTypes.json, 
                    AjaxUtils.requestTypes.post, 
                    null, 
                    {
                        typeId: idTypeDispo,
                        evenementId: idEvenement,
                        licencieId: idLicencie
                    }, 
                    function(response) {
                        $('#loader-page').hide();
                        if (response.success) {
                            load_FormRecapActions(idEvenement);
                        } else {
                            ViewUtils.alert(
                                'Erreur durant la suppression de l\'inscription', 
                                ViewUtils.alertTypes.error,
                                null
                            );
                        }
                    },
                    function(response) {
                        $('#loader-page').hide();
                        ViewUtils.alert(
                            'Service indisponible', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                );
            }
        }
    }
};

initializePage = function() {
    DisposController.initializePage('actions');
    DisposController.control_MatchsUniquement(false, null);

    DisposActionsController.control_RecapActions();
};