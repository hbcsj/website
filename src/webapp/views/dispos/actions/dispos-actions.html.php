<?php
require_once("core/php/resources/page.php");
require_once("common/php/lib/dispos-utils.php");
require_once("common/php/lib/date-utils.php");

$page = new Page(
    "webapp/views/dispos/actions/", 
    "dispos-actions", 
    "DisposActionsCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/dispos/common/nav/nav.html.php");
?>

<div class="container-fluid animated fadeIn">
    <?php
    if (isDisposConnected()) {
        $evenements = $ctrl->getEvenements();
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h1>
                    Actions club &amp; arbitrages
                    <button id="dispos__bt-matchs-uniquement" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-unchecked"></span>
                        </div>
                        <div class="button__text">Matchs uniquement</div>
                    </button>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <?php
                if (sizeof($evenements) > 0) {
                    foreach ($evenements as $evenement) {
                        ?>
                        <div class="row row-evenement" is-match="<?php echo ($evenement[EVENEMENT_TYPE_EVENEMENT_ID] == EVENEMENT_MATCH_TYPE_ID) ? "1" : "0"; ?>">
                            <div class="date-evenement">
                                <?php
                                $dateTimestamp = DateUtils::get_sqlDateTime_timestamp($evenement[EVENEMENT_DATE_HEURE]);
                                echo DateUtils::getNomJour(date("w", $dateTimestamp))." ".date(SLASH_DATE_FORMAT, $dateTimestamp);
                                ?>
                            </div>
                            <div class="text-evenement">
                                <?php 
                                $evenementProps = $ctrl->getRecapEvenementProps($evenement);
                                ?>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <?php echo date(SLASH_TIME_FORMAT, DateUtils::get_sqlDateTime_timestamp($evenement[EVENEMENT_DATE_HEURE])); ?>
                                    </div>
                                    <div class="col-xs-10">
                                        <?php
                                        if ($evenement[EVENEMENT_TYPE_EVENEMENT_ID] == EVENEMENT_MATCH_TYPE_ID) {
                                            echo "<b>".$evenementProps[MATCH_EQUIPE_DOMICILE]." VS ".$evenementProps[MATCH_EQUIPE_EXTERIEUR]."</b>";
                                        } else {
                                            echo "<b>".$evenementProps[EVENEMENT_TYPE];
                                            echo "<br>";
                                            echo $evenement[EVENEMENT_NOM]."</b>";
                                        }

                                        if ($evenementProps[EVENEMENT_GYMNASE] != "") {
                                            echo "<br>".$evenementProps[EVENEMENT_GYMNASE];
                                        }

                                        if ($evenement[EVENEMENT_ADRESSE] != "") {
                                            echo "<br>".$evenement[EVENEMENT_ADRESSE];
                                        }
                                        
                                        if ($evenement[EVENEMENT_VILLE] != "") {
                                            echo "<br>".$evenement[EVENEMENT_CODE_POSTAL]." ".$evenement[EVENEMENT_VILLE];
                                        }

                                        if ($evenementProps[EVENEMENT_GOOGLE_MAP_URL] != "") {
                                            echo "<br><a href=\"".$evenementProps[EVENEMENT_GOOGLE_MAP_URL]."\" target=\"blank\">Afficher sur Google Map</a>"; 
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="row row-recap-actions" id="row-recap-actions-<?php echo $evenement[EVENEMENT_ID]; ?>" id-evenement="<?php echo $evenement[EVENEMENT_ID]; ?>"></div>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
        </div>
        <?php
    } else {
        require_once("webapp/views/common/mdp-form/mdp-form.html.php");
    }
    ?>
</div>

<?php
require_once("webapp/views/common/modals/alert/modal-alert.html.php");
require_once("webapp/views/common/modals/prompt/modal-prompt.html.php");

if (isDisposConnected()) {
    require_once("webapp/views/dispos/actions/modals/save/modal-save-action.html.php");
}

$page->finalizePage();
?>