<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/superadmin/newsletter/modals/newsletter-preview/", 
    "modal-newsletter-preview", 
    "ModalNewsletterPreviewCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_superadmin()) {
?>
    <div class="modal-header">
        <div class="modal-title">Newsletter</div>
    </div>
    <div class="modal-body">
        <div class="form-horizontal" id="form-newsletter-preview">
            <div class="form-group">
                <div class="col-xs-12 form-input" for="form-newsletter-preview__input-newsletter-html">
                    <textarea class="form-control" id="form-newsletter-preview__input-newsletter-html" rows="10" required><?php echo $ctrl->getNewsletterHTML(); ?></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12" id="newsletter-preview"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-8">
                <div class="form-result-message"></div>
            </div>
            <div class="col-xs-4 text-right">
                <button id="modal-newsletter-preview__bt-envoyer" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-envelope"></span>
                    </div>
                    <div class="button__text">Envoyer</div>
                    <div class="loader"></div>
                </button>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>