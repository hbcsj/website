<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/constants/config-constants.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("common/php/dao/actualite-dao.php");
require_once("common/php/dao/evenement-dao.php");
require_once("common/php/dao/hhsj-dao.php");
require_once("common/php/dao/article-presse-dao.php");
require_once("common/php/dao/journal-article-presse-dao.php");
require_once("common/php/dao/licencie-dao.php");
require_once("common/php/dao/match-billetterie-dao.php");
require_once("common/php/dao/adversaire-billetterie-dao.php");
require_once("common/php/dao/competition-billetterie-dao.php");
require_once("common/php/managers/evenement-manager.php");
require_once("common/php/managers/licencie-manager.php");
require_once("common/php/managers/presse-manager.php");

class ModalNewsletterPreviewCtrl extends AbstractViewCtrl {
	
	private $actualiteDAO;
	private $evenementDAO;
	private $articlePresseDAO;
	private $journalArticlePresseDAO;
	private $hhsjDAO;
	private $licencieDAO;
	private $matchBilletterieDAO;
	private $adversaireBilletterieDAO;
	private $competitionBilletterieDAO;

	private $evenementManager;
	private $licencieManager;

	private $newsletterHTML;

	private $dateDebut;
	private $anniversaires;
	private $billetterieFenix;
	private $news;
	private $prochainesRencontres;
	private $dernieresRencontres;
	private $articlesPresse;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
			"dateDebut" => $_GET["dateDebut"],
            "idsArticlesPresse" => $_GET["idsArticlesPresse"],
            "idsNews" => $_GET["idsNews"],
            "idsProchainesRencontres" => $_GET["idsProchainesRencontres"],
            "idsDernieresRencontres" => $_GET["idsDernieresRencontres"]
		), true);
		
		if (isAdminConnected_coach()) {
			$this->actualiteDAO = new ActualiteDAO($this->getDatabaseConnection());
			$this->evenementDAO = new EvenementDAO($this->getDatabaseConnection());
			$this->hhsjDAO = new HHSJDAO($this->getDatabaseConnection());
			$this->articlePresseDAO = new ArticlePresseDAO($this->getDatabaseConnection());
			$this->journalArticlePresseDAO = new JournalArticlePresseDAO($this->getDatabaseConnection());
			$this->licencieDAO = new LicencieDAO($this->getDatabaseConnection());
			$this->matchBilletterieDAO = new MatchBilletterieDAO($this->getDatabaseConnection());
			$this->adversaireBilletterieDAO = new AdversaireBilletterieDAO($this->getDatabaseConnection());
			$this->competitionBilletterieDAO = new CompetitionBilletterieDAO($this->getDatabaseConnection());

			$this->evenementManager = new EvenementManager($this->getDatabaseConnection());
			$this->licencieManager = new LicencieManager($this->getDatabaseConnection());

			$this->checkRequest();

			$this->setNewsletterHTML();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$checkParams = $this->checkParams(array(
			array(GET, "dateDebut")
		));
		if ($checkParams) {
			$this->dateDebut = DateUtils::convert_slashDate_to_sqlDate($_GET["dateDebut"]);
			$this->anniversaires = ($_GET["anniversaires"] == 1);
			$this->billetterieFenix = ($_GET["billetterieFenix"] == 1);
			$this->checkIdsArticlesPresse();
			$this->checkIdsNews();
			$this->checkIdsProchainesRencontres();
			$this->checkIdsDernieresRencontres();
		}
	}

	private function checkIdsArticlesPresse() {
		$this->articlesPresse = array();

		if (HTTPUtils::paramExists(GET, "idsArticlesPresse")) {
			$articlesPresseIds = explode(",", $_GET["idsArticlesPresse"]);

			if (sizeof($articlesPresseIds) > 0) {
				foreach ($articlesPresseIds as $articlePresseId) {
					$articlePresse = null;
					if (substr($articlePresseId, 0, strlen(HHSJ_ID_PREFIX)) == HHSJ_ID_PREFIX) {
						$articlePresse = $this->hhsjDAO->getById(substr($articlePresseId, strlen(HHSJ_ID_PREFIX)));
					} else if (substr($articlePresseId, 0, strlen(ARTICLE_PRESSE_ID_PREFIX)) == ARTICLE_PRESSE_ID_PREFIX) {
						$articlePresse = $this->articlePresseDAO->getById(substr($articlePresseId, strlen(ARTICLE_PRESSE_ID_PREFIX)));
					}

					if ($articlePresse == null) {
						$this->sendCheckError(
							HTTP_404, 
							"L'articlePresse (idArticlePresse = '".$articlePresseId."') n'existe pas", 
							"webapp/views/common/error/404/404.html.php"
						);
					} else {
						$this->articlesPresse[] = $articlePresse;
					}
				}
			}
		}
	}

	private function checkIdsNews() {
		$this->news = array();

		if (HTTPUtils::paramExists(GET, "idsNews")) {
			$newsIds = explode(",", $_GET["idsNews"]);

			if (sizeof($newsIds) > 0) {
				foreach ($newsIds as $actuId) {
					$actu = $this->actualiteDAO->getById($actuId);
					if ($actu == null) {
						$this->sendCheckError(
							HTTP_404, 
							"L'actualite (idActu = '".$actuId."') n'existe pas", 
							"webapp/views/common/error/404/404.html.php"
						);
					} else {
						$this->news[] = $actu;
					}
				}
			}
		}
	}

	private function checkIdsProchainesRencontres() {
		$this->prochainesRencontres = array();
		
		if (HTTPUtils::paramExists(GET, "idsProchainesRencontres")) {
			$prochainesRencontresIds = explode(",", $_GET["idsProchainesRencontres"]);

			if (sizeof($prochainesRencontresIds) > 0) {
				foreach ($prochainesRencontresIds as $prochaineRencontreId) {
					$prochaineRencontre = $this->evenementDAO->getById($prochaineRencontreId);
					if ($prochaineRencontre == null) {
						$this->sendCheckError(
							HTTP_404, 
							"La prochaine rencontre (idEvenement = '".$prochaineRencontreId."') n'existe pas", 
							"webapp/views/common/error/404/404.html.php"
						);
					} else {
						$this->prochainesRencontres[] = $prochaineRencontre;
					}
				}
			}
		}
	}

	private function checkIdsDernieresRencontres() {
		$this->dernieresRencontres = array();
		
		if (HTTPUtils::paramExists(GET, "idsDernieresRencontres")) {
			$dernieresRencontresIds = explode(",", $_GET["idsDernieresRencontres"]);

			if (sizeof($dernieresRencontresIds) > 0) {
				foreach ($dernieresRencontresIds as $derniereRencontreId) {
					$derniereRencontre = $this->evenementDAO->getById($derniereRencontreId);
					if ($derniereRencontre == null) {
						$this->sendCheckError(
							HTTP_404, 
							"La derniere rencontre (idEvenement = '".$derniereRencontreId."') n'existe pas", 
							"webapp/views/common/error/404/404.html.php"
						);
					} else {
						$this->dernieresRencontres[] = $derniereRencontre;
					}
				}
			}
		}
	}

	public function setNewsletterHTML() {
		$this->newsletterHTML = "";

		if ($this->anniversaires) {
			$licenciesAnniversaire = array();
			$dateCourante = $this->dateDebut;
			while ($dateCourante != DateUtils::ajouterJoursADateDuJour(1, SQL_DATE_FORMAT)) {
				$licenciesNesADateCourante = $this->licencieDAO->getNesADate($dateCourante, LICENCIE_TABLE_NAME.".".LICENCIE_NOM.", ".LICENCIE_TABLE_NAME.".".LICENCIE_PRENOM);

				if (sizeof($licenciesNesADateCourante) > 0) {
					$licenciesAnniversaire[$dateCourante] = $licenciesNesADateCourante;
				}

				$dateCourante = date(SQL_DATE_FORMAT, mktime(0, 0, 0, substr($dateCourante, 5, 2), substr($dateCourante, 8, 2) + 1, substr($dateCourante, 0, 4)));
			}
			
			if (sizeof($licenciesAnniversaire) > 0) {
				$this->newsletterHTML .= "<h1 style=\"color: #FFFFFF; margin-top: 20px; padding: 5px; background-color: #000000; font-weight: bold; font-size: 24px;\">Joyeux anniversaire &agrave;</h1>";
				$this->newsletterHTML .= "<ul style=\"margin-top: 20px;\">";
				foreach ($licenciesAnniversaire as $dateAnniversaire => $licencies) {
					$this->newsletterHTML .= "<li style=\"padding: 3px; margin-left: 50px;\"><b>".substr(DateUtils::convert_sqlDate_to_slashDate($dateAnniversaire), 0, 5)." :</b> ";
					
					$licenciesStrArray = array();
					foreach ($licencies as $licencie) {
						$age = date("Y") - substr($licencie[LICENCIE_DATE_NAISSANCE], 0, 4);
						$categoriesJouees = $this->licencieManager->getCategoriesLicencie($licencie[LICENCIE_ID]);
						$categoriesJoueesStrArray = array();
						if (sizeof($categoriesJouees) > 0) {
							foreach ($categoriesJouees as $categorieJouee) {
								$categoriesJoueesStrArray[] = $categorieJouee[CATEGORIE_NOM];
							}
						}

						$categoriesCoachees = $this->licencieManager->getCategoriesCoacheLicencie($licencie[LICENCIE_ID]);
						$categoriesCoacheesStrArray = array();
						if (sizeof($categoriesCoachees) > 0) {
							foreach ($categoriesCoachees as $categorieCoachee) {
								$categoriesCoacheesStrArray[] = "coach ".$categorieCoachee[CATEGORIE_NOM];
							}
						}

						$licencieStr = $licencie[LICENCIE_PRENOM]." ".$licencie[LICENCIE_NOM]. " (".$age." ans";
						if (sizeof($categoriesJoueesStrArray) > 0) {
							$licencieStr .= ", ".implode(", ", $categoriesJoueesStrArray);
						}
						if (sizeof($categoriesCoacheesStrArray) > 0) {
							$licencieStr .= ", ".implode(", ", $categoriesCoacheesStrArray);
						}
						$licencieStr .= ")";
						$licenciesStrArray[] = $licencieStr;
					}

					$this->newsletterHTML.= implode(" - ", $licenciesStrArray)."</li>";
				}
				$this->newsletterHTML .= "</ul>";
			}
		}

		if ($this->billetterieFenix) {
			$matchs = $this->matchBilletterieDAO->getEnVenteAVenirAvantDateLimite(
				MATCH_BILLETTERIE_TABLE_NAME.".".MATCH_BILLETTERIE_DATE_HEURE
			);

			if (sizeof($matchs) > 0) {
				$this->newsletterHTML .= "<h1 style=\"color: #FFFFFF; margin-top: 20px; padding: 5px; background-color: #000000; font-weight: bold; font-size: 24px;\">Billetterie Fenix - Matchs &agrave; venir</h1>";
				$this->newsletterHTML .= "<table style=\"margin-top: 20px;\">";

				foreach ($matchs as $match) {
					$timestampDateHeureMatch = DateUtils::get_sqlDateTime_timestamp($match[MATCH_BILLETTERIE_DATE_HEURE]);
					$adversaire = $this->adversaireBilletterieDAO->getById($match[MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID]);
					$competition = $this->competitionBilletterieDAO->getById($match[MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID]);
					$this->newsletterHTML .= "<tr>";
					$this->newsletterHTML .= "<td style=\"width: 200px; padding: 3px; border-bottom: 1px solid #dddddd;\">";
					$this->newsletterHTML .= substr(DateUtils::getNomJour(date("w", $timestampDateHeureMatch)), 0, 3).". ".str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDateTime_to_slashDateTime($match[MATCH_BILLETTERIE_DATE_HEURE]));
					$this->newsletterHTML .= "</td>";
					$this->newsletterHTML .= "<td style=\"width: 250px; padding: 3px; border-bottom: 1px solid #dddddd;\">";
					$this->newsletterHTML .= "Fenix VS ".$adversaire[ADVERSAIRE_BILLETTERIE_NOM];
					$this->newsletterHTML .= "</td>";
					$this->newsletterHTML .= "<td style=\"width: 200px; padding: 3px; border-bottom: 1px solid #dddddd;\">";
					$this->newsletterHTML .= $competition[COMPETITION_BILLETTERIE_NOM];
					$this->newsletterHTML .= "</td>";
					$this->newsletterHTML .= "<td style=\"width: 200px; padding: 3px; border-bottom: 1px solid #dddddd;\">";
					$this->newsletterHTML .= "1 place : ".$match[MATCH_BILLETTERIE_PRIX_1_PLACE]." &euro; (<span style=\"text-decoration: line-through;\">".$match[MATCH_BILLETTERIE_PRIX_PUBLIC]." &euro;</span>)<br>";
					$this->newsletterHTML .= "3 places : ".$match[MATCH_BILLETTERIE_PRIX_3_PLACES]." &euro; (<span style=\"text-decoration: line-through;\">".(3 * $match[MATCH_BILLETTERIE_PRIX_PUBLIC])." &euro;</span>)";
					$this->newsletterHTML .= "</td>";
					$this->newsletterHTML .= "</tr>";
				}

				$this->newsletterHTML .= "</table>";
			}
		}

		if (sizeof($this->dernieresRencontres) > 0) {
			$this->newsletterHTML .= "<h1 style=\"color: #FFFFFF; margin-top: 20px; padding: 5px; background-color: #000000; font-weight: bold; font-size: 24px;\">Derniers r&eacute;sultats</h1>";
			$this->newsletterHTML .= "<table style=\"margin-top: 20px;\">";

			foreach ($this->dernieresRencontres as $derniereRencontre) {
				$propsDerniereRencontre = $this->evenementManager->getRecapMatchProps($derniereRencontre);
				$this->newsletterHTML .= "<tr>";
				$this->newsletterHTML .= "<td style=\"width: 200px; padding: 3px; border-bottom: 1px solid #dddddd;\">";
				$this->newsletterHTML .= substr($propsDerniereRencontre[EVENEMENT_JOUR], 0, 3).". ".str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDateTime_to_slashDateTime($derniereRencontre[EVENEMENT_DATE_HEURE]));
				$this->newsletterHTML .= "</td>";
				$this->newsletterHTML .= "<td style=\"width: 250px; padding: 3px; border-bottom: 1px solid #dddddd;\">";
				$this->newsletterHTML .= $propsDerniereRencontre[MATCH_EQUIPE_DOMICILE]." VS ".$propsDerniereRencontre[MATCH_EQUIPE_EXTERIEUR];
				$this->newsletterHTML .= "</td>";

				$color = "";
				if ($propsDerniereRencontre[MATCH_CLASS_RESULTAT] == "victoire") {
					$color = "#00bb00";
				} else if ($propsDerniereRencontre[MATCH_CLASS_RESULTAT] == "defaite") {
					$color = "#bb0000";
				} else if ($propsDerniereRencontre[MATCH_CLASS_RESULTAT] == "nul") {
					$color = "#00add2";
				}

				$this->newsletterHTML .= "<td style=\"width: 100px; color: ".$color."; padding: 3px; border-bottom: 1px solid #dddddd;\">";
				if ($propsDerniereRencontre[MATCH_SCORE_DOMICILE] != "" || $propsDerniereRencontre[MATCH_SCORE_EXTERIEUR] != "") {
					$this->newsletterHTML .= $propsDerniereRencontre[MATCH_TEXT_RESULTAT];
				}
				$this->newsletterHTML .= "</td>";
				$this->newsletterHTML .= "<td style=\"width: 300px; padding: 3px; border-bottom: 1px solid #dddddd;\">";
				if ($propsDerniereRencontre[MATCH_COMPETITION] != null) {
					if ($propsDerniereRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB] != "") {
						$this->newsletterHTML .= "<a href=\"".$propsDerniereRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]."\" style=\"text-decoration: none; color: #00add2;\" target=\"_blank\">";
					}
					$this->newsletterHTML .=  $propsDerniereRencontre[MATCH_COMPETITION][COMPETITION_NOM];
					if ($propsDerniereRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB] != "") {
						$this->newsletterHTML .= "</a>";
					}
				}
				$this->newsletterHTML .= "</td>";
				$this->newsletterHTML .= "</tr>";
			}
			
			$this->newsletterHTML .= "</table>";
		}

		if (sizeof($this->prochainesRencontres) > 0) {
			$this->newsletterHTML .= "<h1 style=\"color: #FFFFFF; margin-top: 20px; padding: 5px; background-color: #000000; font-weight: bold; font-size: 24px;\">Prochaines rencontres</h1>";
			$this->newsletterHTML .= "<table style=\"margin-top: 20px;\">";

			foreach ($this->prochainesRencontres as $prochaineRencontre) {
				$propsProchaineRencontre = $this->evenementManager->getRecapMatchProps($prochaineRencontre);
				$this->newsletterHTML .= "<tr>";
				$this->newsletterHTML .= "<td style=\"width: 200px; padding: 3px; border-bottom: 1px solid #dddddd;\">";
				$this->newsletterHTML .= substr($propsProchaineRencontre[EVENEMENT_JOUR], 0, 3).". ".str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDateTime_to_slashDateTime($prochaineRencontre[EVENEMENT_DATE_HEURE]));
				$this->newsletterHTML .= "</td>";
				$this->newsletterHTML .= "<td style=\"width: 250px; padding: 3px; border-bottom: 1px solid #dddddd;\">";
				$this->newsletterHTML .= $propsProchaineRencontre[MATCH_EQUIPE_DOMICILE]." VS ".$propsProchaineRencontre[MATCH_EQUIPE_EXTERIEUR];
				$this->newsletterHTML .= "</td>";
				$this->newsletterHTML .= "<td style=\"width: 400px; padding: 3px; border-bottom: 1px solid #dddddd;\">";
				if ($propsProchaineRencontre[MATCH_COMPETITION] != null) {
					if ($propsProchaineRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB] != "") {
						$this->newsletterHTML .= "<a href=\"".$propsProchaineRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]."\" style=\"text-decoration: none; color: #00add2;\" target=\"_blank\">";
					}
					$this->newsletterHTML .=  $propsProchaineRencontre[MATCH_COMPETITION][COMPETITION_NOM];
					if ($propsProchaineRencontre[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB] != "") {
						$this->newsletterHTML .= "</a>";
					}
				}
				$this->newsletterHTML .= "</td>";
				$this->newsletterHTML .= "</tr>";
			}

			$this->newsletterHTML .= "</table>";
		}

		if (sizeof($this->news) > 0) {
			$this->newsletterHTML .= "<h1 style=\"color: #FFFFFF; margin-top: 20px; padding: 5px; background-color: #000000; font-weight: bold; font-size: 24px;\">Derni&egrave;res nouvelles</h1>";
			$this->newsletterHTML .= "<table style=\"margin-top: 20px;\">";

			foreach ($this->news as $actu) {
				$this->newsletterHTML .= "<tr>";
				$this->newsletterHTML .= "<td style=\"width: 140px; padding: 3px; border-bottom: 1px solid #dddddd;\">";
				$this->newsletterHTML .= "Publi&eacute; le<br>".substr(DateUtils::getNomJour(
					date("w", DateUtils::get_sqlDateTime_timestamp($actu[ACTUALITE_DATE_HEURE]))
				), 0, 3).". ".str_replace(SLASH_DATE_TIME_SEPARATOR, "<br>&agrave; ", DateUtils::convert_sqlDateTime_to_slashDateTime($actu[ACTUALITE_DATE_HEURE]));
				$this->newsletterHTML .= "</td>";
				$this->newsletterHTML .= "<td style=\"width: 220px; padding: 3px; border-bottom: 1px solid #dddddd;\">";
				$this->newsletterHTML .= $actu[ACTUALITE_TITRE];
				$this->newsletterHTML .= "</td>";
				$this->newsletterHTML .= "<td style=\"width: 350px; padding: 3px; border-bottom: 1px solid #dddddd;\">";
				$this->newsletterHTML .= nl2br(FileUtils::getFileContent(PathUtils::getActualiteHomeFile($actu[ACTUALITE_ID])));
				$this->newsletterHTML .= "</td>";
				$this->newsletterHTML .= "<td style=\"width: 140px; padding: 3px; border-bottom: 1px solid #dddddd;\">";
				$this->newsletterHTML .= "<a href=\"".$GLOBALS[CONF][WEBAPP_URL]."news/actu?id=".$actu[ACTUALITE_ID]."\" style=\"text-decoration: none; color: #00add2;\" target=\"_blank\">Lire l'article complet</a>";
				$this->newsletterHTML .= "</td>";
				$this->newsletterHTML .= "</tr>";
			}

			$this->newsletterHTML .= "</table>";
		}

		if (sizeof($this->articlesPresse) > 0) {
			$this->newsletterHTML .= "<h1 style=\"color: #FFFFFF; margin-top: 20px; padding: 5px; background-color: #000000; font-weight: bold; font-size: 24px;\">Revue de presse</h1>";
			$this->newsletterHTML .= "<table style=\"margin-top: 20px;\">";

			foreach ($this->articlesPresse as $articlePresse) {
				if (array_key_exists(HHSJ_TYPE_HHSJ_ID, $articlePresse)) {
					$this->newsletterHTML .= "<tr>";
					$this->newsletterHTML .= "<td style=\"width: 140px; padding: 3px; border-bottom: 1px solid #dddddd;\">";
					$this->newsletterHTML .= "Publi&eacute; le<br>".substr(DateUtils::getNomJour(
						date("w", DateUtils::get_sqlDate_timestamp($articlePresse[HHSJ_DATE]))
					), 0, 3).". ".str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDate_to_slashDate($articlePresse[HHSJ_DATE]));
					$this->newsletterHTML .= "</td>";
					$this->newsletterHTML .= "<td style=\"width: 150px; padding: 3px; border-bottom: 1px solid #dddddd;\">";
					$this->newsletterHTML .= "Hand-Hebdo";
					$this->newsletterHTML .= "</td>";
					$this->newsletterHTML .= "<td style=\"width: 400px; padding: 3px; border-bottom: 1px solid #dddddd;\">";
					$this->newsletterHTML .= $articlePresse[HHSJ_TITRE];
					$this->newsletterHTML .= "</td>";
					$this->newsletterHTML .= "<td style=\"width: 150px; padding: 3px; border-bottom: 1px solid #dddddd;\">";
					if ($articlePresse[HHSJ_TYPE_HHSJ_ID] == TYPE_HHSJ_MAGAZINE_COMPLET_ID || $articlePresse[HHSJ_TYPE_HHSJ_ID] == TYPE_HHSJ_MAGAZINE_COMPLET_ID) {
						$this->newsletterHTML .= "<a href=\"".$GLOBALS[CONF][WEBAPP_URL].HAND_HEBDOS_SAINT_JEANNAIS_FOLDER."/".$articlePresse[HHSJ_ID].".".HAND_HEBDOS_SAINT_JEANNAIS_COMPLET_EXTENSION."\" style=\"text-decoration: none; color: #00add2;\" target=\"_blank\">";
						$this->newsletterHTML .= "T&eacute;l&eacute;charger";
						$this->newsletterHTML .= "</a>";
					} else {
						$this->newsletterHTML .= "<a href=\"".$GLOBALS[CONF][WEBAPP_URL].PathUtils::getHHSJImgHomeFile($articlePresse[HHSJ_ID])."\" target=\"_blank\">";
						$this->newsletterHTML .= "Voir";
						$this->newsletterHTML .= "</a>";
					}
					$this->newsletterHTML .= "</td>";
					$this->newsletterHTML .= "</tr>";
				} else {
					$journal = $this->journalArticlePresseDAO->getById($articlePresse[ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID]);
					$this->newsletterHTML .= "<tr>";
					$this->newsletterHTML .= "<td style=\"width: 140px; padding: 3px; border-bottom: 1px solid #dddddd;\">";
					$this->newsletterHTML .= "Publi&eacute; le<br>".substr(DateUtils::getNomJour(
						date("w", DateUtils::get_sqlDate_timestamp($articlePresse[ARTICLE_PRESSE_DATE]))
					), 0, 3).". ".str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDate_to_slashDate($articlePresse[ARTICLE_PRESSE_DATE]));
					$this->newsletterHTML .= "</td>";
					$this->newsletterHTML .= "<td style=\"width: 150px; padding: 3px; border-bottom: 1px solid #dddddd;\">";
					$this->newsletterHTML .= $journal[JOURNAL_ARTICLE_PRESSE_NOM];
					$this->newsletterHTML .= "</td>";
					$this->newsletterHTML .= "<td style=\"width: 400px; padding: 3px; border-bottom: 1px solid #dddddd;\">";
					$this->newsletterHTML .= $articlePresse[ARTICLE_PRESSE_TITRE];
					$this->newsletterHTML .= "</td>";
					$this->newsletterHTML .= "<td style=\"width: 100px; padding: 3px; border-bottom: 1px solid #dddddd;\">";
					$this->newsletterHTML .= "<a href=\"".$GLOBALS[CONF][WEBAPP_URL].PathUtils::getArticlePresseImgFile($articlePresse[ARTICLE_PRESSE_ID])."\" style=\"text-decoration: none; color: #00add2;\" target=\"_blank\">";
					$this->newsletterHTML .= "T&eacute;l&eacute;charger";
					$this->newsletterHTML .= "</a>";
					$this->newsletterHTML .= "</td>";
					$this->newsletterHTML .= "<td style=\"width: 50px; padding: 3px; border-bottom: 1px solid #dddddd;\">";
					$this->newsletterHTML .= "<a href=\"".$articlePresse[ARTICLE_PRESSE_URL]."\" style=\"text-decoration: none; color: #00add2;\" target=\"_blank\">";
					$this->newsletterHTML .= "Voir";
					$this->newsletterHTML .= "</a>";
					$this->newsletterHTML .= "</td>";
					$this->newsletterHTML .= "</tr>";
				}
			}

			$this->newsletterHTML .= "</table>";
		}
	}

	public function getNewsletterHTML() {
		return $this->newsletterHTML;
	}
}

?>