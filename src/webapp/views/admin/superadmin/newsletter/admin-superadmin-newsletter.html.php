<?php
require_once("core/php/resources/page.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");

$page = new Page(
    "webapp/views/admin/superadmin/newsletter/", 
    "admin-superadmin-newsletter", 
    "AdminSuperadminNewsletterCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/admin/common/nav/nav.html.php");
if (isAdminConnected_superadmin()) {
    require_once("webapp/views/admin/superadmin/sub-menu/sub-menu-admin-superadmin.html.php");
}
?>

<div class="container-fluid animated fadeIn">
    <?php
    if (isAdminConnected_superadmin()) {
        $dernieresNews = $ctrl->getDernieresActualites();
        $dernieresRencontres = $ctrl->getDernieresRencontres();
        $prochainesRencontres = $ctrl->getProchainesRencontres();
        $derniersArticlesPresse = $ctrl->getDerniersArticlesPresse();
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h1>
                    Newsletter
                    <button id="newsletter__bt-generer" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-edit"></span>
                        </div>
                        <div class="button__text">G&eacute;n&eacute;rer</div>
                    </button>
                </h1>
            </div>
        </div>
        <div class="form-horizontal" id="form-newsletter">
            <div class="form-group">
                <div class="col-xs-2 col-xs-offset-1">
                    <h2 class="required">Depuis le ?</h2>
                    <div class="form-input">
                        <div class="input-group">
                            <input type="text" class="form-control" id="form-newsletter__input-date-debut" value="<?php echo DateUtils::ajouterJoursADateDuJour(-7, SLASH_DATE_FORMAT); ?>" pattern="^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$" pattern-indication="Le format doit &ecirc;tre 'jj/mm/aaaa'" required>
                            <span class="input-group-addon">
                                <img src="<?php echo SRC_PATH; ?>assets/img/icons/calendar.png">
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-2 col-xs-offset-1">
                    <h2>Anniversaires ?</h2>
                    <div class="form-input">
                        <input type="checkbox" id="form-newsletter__input-anniversaires">
                    </div>
                </div>
                <div class="col-xs-2 col-xs-offset-1">
                    <h2>Billetterie Fenix ?</h2>
                    <div class="form-input">
                        <input type="checkbox" id="form-newsletter__input-billetterie-fenix">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-10 col-xs-offset-1">
                    <h2 class="required">Message</h2>
                    <div class="form-input">
                        <textarea class="form-control" rows="5" id="form-newsletter__input-message" required><?php echo $ctrl->getNewsletterMessageDefault(); ?></textarea>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-5 col-xs-offset-1">
                    <h2>Derniers r&eacute;sultats</h2>
                    <div class="container-newsletter">
                        <?php
                        if (sizeof($dernieresRencontres) > 0) {
                            ?>
                            <table class="table table-striped table-condensed">
                                <tbody>
                                    <?php
                                    foreach ($dernieresRencontres as $derniereRencontre) {
                                        $propsDerniereRencontre = $ctrl->getRecapMatchProps($derniereRencontre);
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo substr($propsDerniereRencontre[EVENEMENT_JOUR], 0, 3).". ".str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDateTime_to_slashDateTime($derniereRencontre[EVENEMENT_DATE_HEURE])); ?>
                                            </td>
                                            <td>
                                                <?php echo $propsDerniereRencontre[MATCH_EQUIPE_DOMICILE]." VS ".$propsDerniereRencontre[MATCH_EQUIPE_EXTERIEUR]; ?>
                                            </td>
                                            <td class="resultat-<?php echo $propsDerniereRencontre[MATCH_CLASS_RESULTAT]; ?>">
                                                <?php
                                                if ($propsDerniereRencontre[MATCH_SCORE_DOMICILE] != "" || $propsDerniereRencontre[MATCH_SCORE_EXTERIEUR] != "") {
                                                    echo $propsDerniereRencontre[MATCH_TEXT_RESULTAT];
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <input type="checkbox" class="checkbox-derniere-rencontre" id-evenement="<?php echo $derniereRencontre[EVENEMENT_ID]; ?>">
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                ?>
                                </tbody>
                            </table>
                        <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="col-xs-5">
                    <h2>Prochaines rencontres</h2>
                    <div class="container-newsletter">
                        <?php
                        if (sizeof($prochainesRencontres) > 0) {
                            ?>
                            <table class="table table-striped table-condensed">
                                <tbody>
                                    <?php
                                    foreach ($prochainesRencontres as $prochaineRencontre) {
                                        $propsProchaineRencontre = $ctrl->getRecapMatchProps($prochaineRencontre);
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo substr($propsProchaineRencontre[EVENEMENT_JOUR], 0, 3).". ".str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDateTime_to_slashDateTime($prochaineRencontre[EVENEMENT_DATE_HEURE])); ?>
                                            </td>
                                            <td>
                                                <?php echo $propsProchaineRencontre[MATCH_EQUIPE_DOMICILE]." VS ".$propsProchaineRencontre[MATCH_EQUIPE_EXTERIEUR]; ?>
                                            </td>
                                            <td>
                                                <input type="checkbox" class="checkbox-prochaine-rencontre" id-evenement="<?php echo $prochaineRencontre[EVENEMENT_ID]; ?>">
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                ?>
                                </tbody>
                            </table>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-5 col-xs-offset-1">
                    <h2>News</h2>
                    <div class="container-newsletter">
                        <?php
                        if (sizeof($dernieresNews) > 0) {
                            ?>
                            <table class="table table-striped table-condensed">
                                <tbody>
                                    <?php
                                    foreach ($dernieresNews as $actu) {
                                        ?>
                                        <tr>
                                            <td>
                                                <?php 
                                                echo substr(DateUtils::getNomJour(
                                                    date("w", DateUtils::get_sqlDateTime_timestamp($actu[ACTUALITE_DATE_HEURE]))
                                                ), 0, 3).". ".str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDateTime_to_slashDateTime($actu[ACTUALITE_DATE_HEURE])); 
                                                ?>
                                            </td>
                                            <td><?php echo $actu[ACTUALITE_TITRE]; ?></td>
                                            <td>
                                                <input type="checkbox" class="checkbox-actualite" id-actualite="<?php echo $actu[ACTUALITE_ID]; ?>">
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="col-xs-5">
                    <h2>Articles de presse</h2>
                    <div class="container-newsletter">
                        <?php
                        if (sizeof($derniersArticlesPresse) > 0) {
                            ?>
                            <table class="table table-striped table-condensed">
                                <tbody>
                                    <?php
                                    foreach ($derniersArticlesPresse as $articlePresse) {
                                        ?>
                                        <tr>
                                            <?php
                                            if (array_key_exists(HHSJ_TYPE_HHSJ_ID, $articlePresse)) {
                                                ?>
                                                <td>
                                                    <?php 
                                                    echo substr(DateUtils::getNomJour(
                                                        date("w", DateUtils::get_sqlDate_timestamp($articlePresse[HHSJ_DATE]))
                                                    ), 0, 3).". ".str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDate_to_slashDate($articlePresse[HHSJ_DATE])); 
                                                    ?>
                                                </td>
                                                <td>Hand-Hebdo</td>
                                                <td><?php echo $articlePresse[HHSJ_TITRE]; ?></td>
                                                <td>
                                                    <input type="checkbox" class="checkbox-article-presse" id-article-presse="<?php echo $articlePresse[HHSJ_ID]; ?>" is-hhsj="1">
                                                </td>
                                                <?php
                                            } else {
                                                ?>
                                                <td>
                                                    <?php 
                                                    echo substr(DateUtils::getNomJour(
                                                        date("w", DateUtils::get_sqlDate_timestamp($articlePresse[ARTICLE_PRESSE_DATE]))
                                                    ), 0, 3).". ".str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDate_to_slashDate($articlePresse[ARTICLE_PRESSE_DATE])); 
                                                    ?>
                                                </td>
                                                <td><?php echo $ctrl->getNomJournalArticlePresse($articlePresse[ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID]); ?></td>
                                                <td><?php echo $articlePresse[ARTICLE_PRESSE_TITRE]; ?></td>
                                                <td>
                                                    <input type="checkbox" class="checkbox-article-presse" id-article-presse="<?php echo $articlePresse[ARTICLE_PRESSE_ID]; ?>" is-hhsj="0">
                                                </td>
                                                <?php
                                            }
                                            ?>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
    } else {
        require_once("webapp/views/common/mdp-form/mdp-form.html.php");
    }
    ?>
</div>

<?php
require_once("webapp/views/common/modals/alert/modal-alert.html.php");
require_once("webapp/views/common/modals/prompt/modal-prompt.html.php");

if (isAdminConnected_superadmin()) {
    require_once("webapp/views/admin/superadmin/newsletter/modals/newsletter-preview/modal-newsletter-preview.html.php");
}

$page->finalizePage();
?>