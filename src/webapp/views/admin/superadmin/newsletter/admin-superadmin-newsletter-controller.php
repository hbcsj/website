<?php
require_once("core/php/controllers/abstract-page-controller.php");
require_once("common/php/constants/config-constants.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/dao/categorie-dao.php");
require_once("common/php/dao/actualite-dao.php");
require_once("common/php/dao/evenement-dao.php");
require_once("common/php/dao/journal-article-presse-dao.php");
require_once("common/php/managers/evenement-manager.php");
require_once("common/php/managers/presse-manager.php");

class AdminSuperadminNewsletterCtrl extends AbstractPageCtrl {
    
    private $evenementManager;
    private $presseManager;
	
	private $actualiteDAO;
    private $categorieDAO;
    private $evenementDAO;
    private $journalArticlePresseDAO;
	
    public function __construct($pageName) {
		parent::__construct($pageName, null, true);

        $this->evenementManager = new EvenementManager($this->getDatabaseConnection());
        $this->presseManager = new PresseManager($this->getDatabaseConnection());

		$this->actualiteDAO = new ActualiteDAO($this->getDatabaseConnection());
		$this->categorieDAO = new CategorieDAO($this->getDatabaseConnection());
		$this->evenementDAO = new EvenementDAO($this->getDatabaseConnection());
		$this->journalArticlePresseDAO = new JournalArticlePresseDAO($this->getDatabaseConnection());

        $this->setNbEquipesInscrites();
    }
    
    public function getNewsletterMessageDefault() {
        return $GLOBALS[CONF][NEWSLETTER_MESSAGE_DEFAUT];
    }
    
    public function getDernieresActualites() {
        return $this->actualiteDAO->getVisiblesLimited(
            $GLOBALS[CONF][NB_MAX_ACTUS_NEWSLETTER], 
			ACTUALITE_TABLE_NAME.".".ACTUALITE_DATE_HEURE." DESC"
		);
    }

    public function getDernieresRencontres() {
        return $this->evenementDAO->getVisiblesAvantDateByTypeLimited(
            date(SQL_DATE_TIME_FORMAT), 
            EVENEMENT_MATCH_TYPE_ID, 
            $this->nbEquipesInscrites
        );
    }

    public function getProchainesRencontres() {
        return $this->evenementDAO->getVisiblesApresDateByTypeLimited(
            date(SQL_DATE_TIME_FORMAT), 
            EVENEMENT_MATCH_TYPE_ID, 
            $this->nbEquipesInscrites
        );
    }

    public function getRecapMatchProps($match) {
        return $this->evenementManager->getRecapMatchProps($match);
    }

    public function getDerniersArticlesPresse() {
        return $this->presseManager->getDerniersArticlesPresse($GLOBALS[CONF][NB_MAX_ARTICLES_PRESSE_NEWSLETTER]);
    }

    public function getNomJournalArticlePresse($journalId) {
        $journal = $this->journalArticlePresseDAO->getById($journalId);
        return $journal[JOURNAL_ARTICLE_PRESSE_NOM];
    }

    private function setNbEquipesInscrites() {
        $this->nbEquipesInscrites = $this->categorieDAO->getNbEquipesInscrites()[CATEGORIE_NB_EQUIPES_INSCRITES];
    }
}

?>