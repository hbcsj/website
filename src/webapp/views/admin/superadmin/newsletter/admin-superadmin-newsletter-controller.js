AdminSuperadminNewsletterController = {};

AdminSuperadminNewsletterController.hhsjPrefix = 'HHSJ';
AdminSuperadminNewsletterController.articlePressePrefix = 'AP';

AdminSuperadminNewsletterController.precheck_News = function() {
    var dateDebut = $.trim($('#form-newsletter__input-date-debut').val());

    $('#loader-page').show();
    AjaxUtils.callService(
        'newsletter', 
        'getNewsFromDate', 
        AjaxUtils.dataTypes.json, 
        AjaxUtils.requestTypes.get, 
        {
            date: dateDebut
        }, 
        null, 
        function(response) {
            $('#loader-page').hide();
            for (var i in response.derniersResultats) {
                $('input.checkbox-derniere-rencontre[id-evenement='+response.derniersResultats[i]+']').prop('checked', true);
            }
            for (var j in response.news) {
                $('input.checkbox-actualite[id-actualite='+response.news[j]+']').prop('checked', true);
            }
            for (var k in response.articlesPresse) {
                if (response.articlesPresse[k].startsWith(AdminSuperadminNewsletterController.hhsjPrefix)) {
                    $('input.checkbox-article-presse[id-article-presse='+response.articlesPresse[k].replace(AdminSuperadminNewsletterController.hhsjPrefix, '')+'][is-hhsj="1"]').prop('checked', true);
                } else {
                    $('input.checkbox-article-presse[id-article-presse='+response.articlesPresse[k].replace(AdminSuperadminNewsletterController.articlePressePrefix, '')+'][is-hhsj="0"]').prop('checked', true);
                }
            }
        },
        function(response) {
            $('#loader-page').hide();
            ViewUtils.alert(
                'Service indisponible', 
                ViewUtils.alertTypes.error,
                null
            );
        }
    );
};

AdminSuperadminNewsletterController.control_DateDebut = function() {
    $('#form-newsletter__input-date-debut').unbind('change');
    $('#form-newsletter__input-date-debut').change(function() {
        AdminSuperadminNewsletterController.precheck_News();
    });
};

AdminSuperadminNewsletterController.onClick_btGenerer = function() {
    $('#newsletter__bt-generer').unbind('click');
    $('#newsletter__bt-generer').click(function() {
        FormUtils.hideFormErrors('#form-newsletter');
        
        var formValidation = FormUtils.validateForm('#form-newsletter');

        if (formValidation) {
            AdminSuperadminNewsletterController.load_Newsletter();
        }
    });
};

AdminSuperadminNewsletterController.load_Newsletter = function() {
    var idsDernieresRencontres = [];
    var checkboxesDernieresRencontres = $('input.checkbox-derniere-rencontre:checked');
    if (checkboxesDernieresRencontres.length > 0) {
        checkboxesDernieresRencontres.each(function() {
            idsDernieresRencontres.push($(this).attr('id-evenement'));
        });
    }

    var idsProchainesRencontres = [];
    var checkboxesProchainesRencontres = $('input.checkbox-prochaine-rencontre:checked');
    if (checkboxesProchainesRencontres.length > 0) {
        checkboxesProchainesRencontres.each(function() {
            idsProchainesRencontres.push($(this).attr('id-evenement'));
        });
    }

    var idsNews = [];
    var checkboxesActualites = $('input.checkbox-actualite:checked');
    if (checkboxesActualites.length > 0) {
        checkboxesActualites.each(function() {
            idsNews.push($(this).attr('id-actualite'));
        });
    }

    var idsArticlesPresse = [];
    var checkboxesArticlesPresse = $('input.checkbox-article-presse:checked');
    if (checkboxesArticlesPresse.length > 0) {
        checkboxesArticlesPresse.each(function() {
            var prefixHHSJ = ($(this).attr('is-hhsj') == '1') ? AdminSuperadminNewsletterController.hhsjPrefix : AdminSuperadminNewsletterController.articlePressePrefix;
            idsArticlesPresse.push(prefixHHSJ+$(this).attr('id-article-presse'));
        });
    }

    var dateDebut = $.trim($('#form-newsletter__input-date-debut').val());
    var anniversaires = ($('#form-newsletter__input-anniversaires').prop('checked')) ? '1' : '0';
    var billetterieFenix = ($('#form-newsletter__input-billetterie-fenix').prop('checked')) ? '1' : '0';
    var params = {
        dateDebut: dateDebut,
        anniversaires: anniversaires,
        billetterieFenix: billetterieFenix, 
        idsDernieresRencontres: idsDernieresRencontres.join(','),
        idsProchainesRencontres: idsProchainesRencontres.join(','),
        idsNews: idsNews.join(','),
        idsArticlesPresse: idsArticlesPresse.join(',')
    };
    AjaxUtils.loadView(
        'admin/superadmin/newsletter/modals/newsletter-preview/modal-newsletter-preview-content',
        '#modal-newsletter-preview-content',
        params,
        function() {
            setNewsletterHTML();
            onChange_NewsletterHTML();
            onClick_btEnvoyer();
        },
        '#loader-page'
    );
    $('#modal-newsletter-preview').modal('show');

    function setNewsletterHTML() {
        var generatedNewsletterMessage = $('#form-newsletter-preview__input-newsletter-html').val();
        $('#form-newsletter-preview__input-newsletter-html').val($('#form-newsletter__input-message').val()+generatedNewsletterMessage);
        $('#newsletter-preview').html($('#form-newsletter-preview__input-newsletter-html').val());
    }

    function onChange_NewsletterHTML() {
        $('#form-newsletter-preview__input-newsletter-html').unbind('change');
        $('#form-newsletter-preview__input-newsletter-html').change(function() {
            $('#newsletter-preview').html($('#form-newsletter-preview__input-newsletter-html').val());
        });
    }

    function onClick_btEnvoyer() {
        $('#modal-newsletter-preview__bt-envoyer').unbind('click');
        $('#modal-newsletter-preview__bt-envoyer').click(function() {
            FormUtils.hideFormErrors('#modal-newsletter-preview #form-newsletter-preview');
            ViewUtils.unsetHTMLAndHide('#modal-newsletter-preview .form-result-message');
            ViewUtils.desactiveButtonAndShowLoader('#modal-newsletter-preview__bt-envoyer', '#modal-newsletter-preview__bt-envoyer .loader');

            var formValidation = FormUtils.validateForm('#modal-newsletter-preview #form-newsletter-preview');
            
            if (formValidation) {
                var inputNewsletterHTML = $.trim($('#form-newsletter-preview__input-newsletter-html').val());

                sendNewsletter(inputNewsletterHTML);
            } else {
                ViewUtils.activeButtonAndHideLoader('#modal-newsletter-preview__bt-envoyer', '#modal-newsletter-preview__bt-envoyer .loader');
            }
        });

        function sendNewsletter(inputNewsletterHTML) {
            AjaxUtils.callService(
                'newsletter', 
                'sendNewsletter', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    newsletter: inputNewsletterHTML
                }, 
                function(response) {
                    if (response.success) {
                        FormUtils.displayFormResultMessage('#modal-newsletter-preview .form-result-message', 'Newsletter envoy&eacute;e', FormUtils.resultsTypes.ok);
                        $('#modal-newsletter-preview__bt-envoyer .loader').hide();
                    } else {
                        ViewUtils.activeButtonAndHideLoader('#modal-newsletter-preview__bt-envoyer', '#modal-newsletter-preview__bt-envoyer .loader');
                        FormUtils.displayFormResultMessage('#modal-newsletter-preview .form-result-message', 'Erreur durant l\'envoi', FormUtils.resultsTypes.error);
                    }
                },
                function(response) {
                    ViewUtils.activeButtonAndHideLoader('#modal-newsletter-preview__bt-envoyer', '#modal-newsletter-preview__bt-envoyer .loader');
                    FormUtils.displayFormResultMessage('#modal-newsletter-preview .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                }
            );
        }
    }
};

initializePage = function() {
    AdminController.initializePage('superadmin', 'newsletter');

    AdminSuperadminNewsletterController.precheck_News();
    AdminSuperadminNewsletterController.control_DateDebut();
    AdminSuperadminNewsletterController.onClick_btGenerer();
};