<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/coaching/common/modals/mailing-list/", 
    "modal-mailing-list", 
    "ModalMailingListCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
?>
    <div class="modal-header">
        <div class="modal-title">Mailing list</div>
    </div>
    <div class="modal-body">
        <div class="form-horizontal" id="form-mailing-list">
            <div class="form-group">
                <div class="col-xs-12 form-input" for="form-mailing-list__input-mailing-list">
                    <textarea class="form-control" id="form-mailing-list__input-mailing-list" rows="20" readonly><?php echo $ctrl->getMailingList(); ?></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-8">
                <div class="form-result-message"></div>
            </div>
            <div class="col-xs-4 text-right">
                <button id="modal-mailing-list__bt-copier" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-copy"></span>
                    </div>
                    <div class="button__text">Copier</div>
                    <div class="loader"></div>
                </button>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>