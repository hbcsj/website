<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/licencie-dao.php");

class ModalMailingListCtrl extends AbstractViewCtrl {
	
	private $licencieIds;

	private $licencieDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idsLicencie" => $_GET["idsLicencie"],
            "idsCategorie" => $_GET["idsCategorie"]
		), true);
		
		if (isAdminConnected_coach()) {
			$this->licencieDAO = new LicencieDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIds();
	}

	private function checkIds() {
		if (HTTPUtils::paramExists(GET, "idsLicencie")) {
			$this->licencieIds = explode(",", $_GET["idsLicencie"]);

			if (sizeof($this->licencieIds) > 0) {
				foreach ($this->licencieIds as $licencieId) {
					if ($this->licencieDAO->getById($licencieId) == null) {
						$this->sendCheckError(
							HTTP_404, 
							"Le licencie (idLicencie = '".$licencieId."') n'existe pas", 
							"webapp/views/common/error/404/404.html.php"
						);
					}
				}
			}
		} else if (HTTPUtils::paramExists(GET, "idsCategorie")) {
			$this->licencieIds = array();
			$categorieIds = explode(",", $_GET["idsCategorie"]);

			if (sizeof($categorieIds) > 0) {
				foreach ($categorieIds as $categorieId) {
					$licenciesCategorie = $this->licencieDAO->getJoueursAndCoachsByCategorieId($categorieId);

					if (sizeof($licenciesCategorie) > 0) {
						foreach ($licenciesCategorie as $licencieCategorie) {
							$this->licencieIds[] = $licencieCategorie[LICENCIE_ID];
						}
					}
				}
			}
		} else {
			$this->sendCheckError(
				HTTP_400, 
				"Param 'idsLicencie' (".GET.") or 'idsCategorie' (".GET.") missing", 
				"webapp/views/common/error/400/400.html.php"
			);
		}
	}

	public function getMailingList() {
		$mailingList = array();

		if (sizeof($this->licencieIds) > 0) {
			foreach ($this->licencieIds as $licencieId) {
				$licencie = $this->licencieDAO->getById($licencieId);
				if ($licencie[LICENCIE_EMAIL_1] != "" && !in_array($licencie[LICENCIE_EMAIL_1], $mailingList)) {
					$mailingList[] = $licencie[LICENCIE_EMAIL_1];
				}
				if ($licencie[LICENCIE_EMAIL_2] != "" && !in_array($licencie[LICENCIE_EMAIL_2], $mailingList)) {
					$mailingList[] = $licencie[LICENCIE_EMAIL_2];
				}
				if ($licencie[LICENCIE_EMAIL_3] != "" && !in_array($licencie[LICENCIE_EMAIL_3], $mailingList)) {
					$mailingList[] = $licencie[LICENCIE_EMAIL_3];
				}
			}
		}

		return implode(",", $mailingList);
	}
}

?>