<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/coaching/equipes/views/stats/", 
    "stats-equipes", 
    "StatsEquipesCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
    ?>
    <div class="container-stats__section">
        <div class="container-stats__section__title">Nombre de cat&eacute;gories : </div>
        <div class="container-stats__section__value"><?php echo $ctrl->getNbCategories(); ?></div>
    </div>
    <div class="container-stats__section">
        <div class="container-stats__section__title">Nombre d'&eacute;quipes inscrites : </div>
        <div class="container-stats__section__value"><?php echo $ctrl->getNbEquipesInscrites(); ?></div>
    </div>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>