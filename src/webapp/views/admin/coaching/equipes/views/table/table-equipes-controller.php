<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/categorie-dao.php");
require_once("common/php/dao/entrainement-dao.php");
require_once("common/php/dao/licencie-joue-dans-categorie-dao.php");
require_once("common/php/managers/categorie-manager.php");

class TableEquipesCtrl extends AbstractViewCtrl {

	private $categorieDAO;
	private $entrainementDAO;
	private $licencieJoueDansCategorieDAO;

	private $categorieManager;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_coach()) {
			$this->categorieDAO = new CategorieDAO($this->getDatabaseConnection());
			$this->entrainementDAO = new EntrainementDAO($this->getDatabaseConnection());
			$this->licencieJoueDansCategorieDAO = new LicencieJoueDansCategorieDAO($this->getDatabaseConnection());

			$this->categorieManager = new CategorieManager($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getCategories() {
		return $this->categorieDAO->getAll(CATEGORIE_TABLE_NAME.".".CATEGORIE_POSITION_AFFICHAGE);
	}

	public function getCoachsCategorie($categorieId) {
		return $this->categorieManager->getCoachsCategorie($categorieId);
	}

	public function getEntrainementsCategorie($categorieId) {
		return $this->entrainementDAO->getByCategorie($categorieId);
	}

	public function getEffectifCategorie($categorieId) {
		return $this->licencieJoueDansCategorieDAO->getNbByCategorieId($categorieId)[LICENCIE_JOUE_DANS_CATEGORIE_NB_LICENCIES];
	}
}

?>