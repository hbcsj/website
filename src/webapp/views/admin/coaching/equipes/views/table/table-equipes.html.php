<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");

$view = new View(
    "webapp/views/admin/coaching/equipes/views/table/", 
    "table-equipes", 
    "TableEquipesCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
    $categories = $ctrl->getCategories();
    ?>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Nom</th>
                <th class="text-center">Nb. &eacute;quipes</th>
                <th>Coach(s) / responsable(s)</th>
                <th>Entra&icirc;nements</th>
                <th class="text-center">Effectif</th>
                <th>&nbsp;</th>
                <th class="text-center"><span class="glyphicon glyphicon-check clickable" id="check-all-categories"></span></th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (sizeof($categories) > 0) {
                foreach ($categories as $categorie) {
                    ?>
                    <tr>
                        <td><?php echo $categorie[CATEGORIE_NOM]; ?></td>
                        <td class="text-center"><?php echo $categorie[CATEGORIE_NB_EQUIPES]; ?></td>
                        <?php
                        $attributesTdCoachs = "";
                        if (isAdminConnected_bureau()) {
                            $attributesTdCoachs = " class=\"clickable td-coachs-categorie\" id-categorie=\"".$categorie[CATEGORIE_ID]."\" title=\"Editer les coachs\"";
                        }
                        ?>
                        <td<?php echo $attributesTdCoachs; ?>>
                            <?php 
                            $coachs = $ctrl->getCoachsCategorie($categorie[CATEGORIE_ID]);
                            if (sizeof($coachs) > 0) {
                                $i = 0;
                                foreach ($coachs as $coach) {
                                    if ($coach[COACH_EN_SOUTIEN]) {
                                        echo "(";
                                    }
                                    echo $coach[LICENCIE_PRENOM]." ".$coach[LICENCIE_NOM];
                                    if ($coach[COACH_EN_SOUTIEN]) {
                                        echo ")";
                                    }

                                    if ($i < (sizeof($coachs) - 1)) {
                                        echo "<br>";
                                    }
                                    $i++;
                                }
                            }
                            ?>
                        </td>
                        <?php
                        $attributesTdEntrainements = "";
                        if (isAdminConnected_bureau()) {
                            $attributesTdEntrainements = " class=\"clickable td-entrainements-categorie\" id-categorie=\"".$categorie[CATEGORIE_ID]."\" title=\"Editer les entra&icirc;nements\"";
                        }
                        ?>
                        <td<?php echo $attributesTdEntrainements; ?>>
                            <?php
                            $entrainements = $ctrl->getEntrainementsCategorie($categorie[CATEGORIE_ID]);
                            if (sizeof($entrainements) > 0) {
                                $i = 0;
                                foreach ($entrainements as $entrainement) {
                                    echo DateUtils::getNomJour($entrainement[ENTRAINEMENT_JOUR])." - ".DateUtils::convert_sqlTime_to_timeWithoutSeconds($entrainement[ENTRAINEMENT_HEURE_DEBUT])." &agrave ".DateUtils::convert_sqlTime_to_timeWithoutSeconds($entrainement[ENTRAINEMENT_HEURE_FIN]);

                                    if ($i < (sizeof($entrainements) - 1)) {
                                        echo "<br>";
                                    }
                                    $i++;
                                }
                            }
                            ?>
                        </td>
                        <td class="text-center td-effectif-categorie"><?php echo $ctrl->getEffectifCategorie($categorie[CATEGORIE_ID]); ?></td>
                        <td class="text-center">
                            <span class="glyphicon glyphicon-pencil clickable" title="Editer" id-categorie="<?php echo $categorie[CATEGORIE_ID]; ?>"></span>
                        </td>
                        <td class="text-center">
                            <input type="checkbox" class="checkbox-categorie" id-categorie="<?php echo $categorie[CATEGORIE_ID]; ?>" checked>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>