<?php
require_once("core/php/resources/page.php");
require_once("common/php/lib/admin-utils.php");

$page = new Page(
    "webapp/views/admin/coaching/equipes/", 
    "admin-coaching-equipes", 
    "AdminCoachingEquipesCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/admin/common/nav/nav.html.php");
if (isAdminConnected_coach()) {
    require_once("webapp/views/admin/coaching/sub-menu/sub-menu-admin-coaching.html.php");
}
?>

<div class="container-fluid animated fadeIn">
    <?php
    if (isAdminConnected_coach()) {
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h1>
                    Equipes
                    <button id="categories__bt-mailing-list" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-envelope"></span>
                        </div>
                        <div class="button__text">Mailing list</div>
                    </button>
                </h1>
            </div>
        </div>
        <div id="container-stats-categories" class="container-stats"></div>
        <div id="container-table-categories" class="container-table"></div>
        <?php
    } else {
        require_once("webapp/views/common/mdp-form/mdp-form.html.php");
    }
    ?>
</div>

<?php
require_once("webapp/views/common/modals/alert/modal-alert.html.php");
require_once("webapp/views/common/modals/prompt/modal-prompt.html.php");

if (isAdminConnected_coach()) {
    require_once("webapp/views/admin/coaching/equipes/modals/save/modal-save-categorie.html.php");
    require_once("webapp/views/admin/coaching/common/modals/mailing-list/modal-mailing-list.html.php");
}

if (isAdminConnected_bureau()) {
    require_once("webapp/views/admin/coaching/equipes/modals/coachs/modal-coachs-categorie.html.php");
    require_once("webapp/views/admin/coaching/equipes/modals/entrainements/modal-entrainements-categorie.html.php");
}

$page->finalizePage();
?>