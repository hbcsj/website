<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");

$view = new View(
    "webapp/views/admin/coaching/equipes/modals/entrainements/", 
    "modal-entrainements-categorie", 
    "ModalEntrainementsCategorieCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_bureau()) {
    $categorie = $ctrl->getCategorie();
    $entrainements = $ctrl->getEntrainementsCategorie();
    ?>
    <div class="modal-header">
        <div class="modal-title">
            <?php echo $categorie[CATEGORIE_NOM]; ?> - 
            <span class="normal">Entra&icirc;nements</span>
        </div>
        <div class="loader loader-modal" for="form-entrainements-categorie"></div>
    </div>
    <div class="modal-body">
        <div class="form-horizontal" id="form-entrainements-categorie">
            <?php
            if (sizeof($entrainements) > 0) {
                foreach ($entrainements as $entrainement) {
                    ?>
                    <div class="form-group">
                        <label for="form-entrainements-categorie__input-jour-<?php echo $entrainement[ENTRAINEMENT_ID]; ?>" class="col-xs-2 control-label">Jour :</label>
                        <div class="col-xs-2 form-input" for="form-entrainements-categorie__input-jour-<?php echo $entrainement[ENTRAINEMENT_ID]; ?>">
                            <input type="text" class="form-control" value="<?php echo DateUtils::getNomJour($entrainement[ENTRAINEMENT_JOUR]); ?>" readonly>
                        </div>
                        <label for="form-coachs-categorie__input-heure-debut-<?php echo $entrainement[ENTRAINEMENT_ID]; ?>" class="col-xs-2 col-xs-offset-1 control-label">D&eacute;but :</label>
                        <div class="col-xs-2 form-input" for="form-coachs-categorie__input-heure-debut-<?php echo $entrainement[ENTRAINEMENT_ID]; ?>">
                            <input type="text" class="form-control" value="<?php echo DateUtils::convert_sqlTime_to_timeWithoutSeconds($entrainement[ENTRAINEMENT_HEURE_DEBUT]); ?>" readonly>
                        </div>
                        <div class="col-xs-3 text-right">
                            <button class="btn btn-default bt-supprimer-entrainement" id-entrainement="<?php echo $entrainement[ENTRAINEMENT_ID]; ?>">
                                <div class="button__icon">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </div>
                                <div class="button__text">Sup.</div>
                            </button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="form-coachs-categorie__input-heure-fin-<?php echo $entrainement[ENTRAINEMENT_ID]; ?>" class="col-xs-2 col-xs-offset-5 control-label">Fin :</label>
                        <div class="col-xs-2 form-input" for="form-coachs-categorie__input-heure-fin-<?php echo $entrainement[ENTRAINEMENT_ID]; ?>">
                            <input type="text" class="form-control" value="<?php echo DateUtils::convert_sqlTime_to_timeWithoutSeconds($entrainement[ENTRAINEMENT_HEURE_FIN]); ?>" readonly>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <div class="form-horizontal" id="form-add-entrainement-categorie">
            <div class="form-group">
                <label for="form-add-entrainement-categorie__input-jour" class="col-xs-2 control-label required">Jour :</label>
                <div class="col-xs-2 form-input" for="form-add-entrainement-categorie__input-jour">
                    <select class="form-control" id="form-add-entrainement-categorie__input-jour" required>
                        <option value=""></option>
                        <?php
                        for ($jour = 1 ; $jour <= 7 ; $jour++) {
                            ?>
                            <option value="<?php echo $jour; ?>"><?php echo DateUtils::getNomJour($jour); ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-add-coach-categorie__input-licencie"></div>
                <label for="form-add-entrainement-categorie__input-heure-debut" class="col-xs-2 control-label required">D&eacute;but :</label>
                <div class="col-xs-2 form-input" for="form-add-entrainement-categorie__input-heure-debut">
                    <select class="form-control" id="form-add-entrainement-categorie__input-heure-debut" required>
                        <option value=""></option>
                        <?php echo $ctrl->getOptionsHeureEntrainement(); ?>
                    </select>
                </div>
                <div class="col-xs-3 text-right">
                    <button id="modal-entrainements-categorie__bt-ajouter" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-plus"></span>
                        </div>
                        <div class="button__text">OK</div>
                    </button>
                </div>
            </div>
            <div class="form-group">
                <label for="form-add-entrainement-categorie__input-heure-fin" class="col-xs-2 col-xs-offset-5 control-label required">Fin :</label>
                <div class="col-xs-2 form-input" for="form-add-entrainement-categorie__input-heure-fin">
                    <select class="form-control" id="form-add-entrainement-categorie__input-heure-fin" required>
                        <option value=""></option>
                        <?php echo $ctrl->getOptionsHeureEntrainement(); ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-required text-right">Les champs marqu&eacute;s d'une * sont obligatoires</div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-12">
                <div class="form-result-message"></div>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>