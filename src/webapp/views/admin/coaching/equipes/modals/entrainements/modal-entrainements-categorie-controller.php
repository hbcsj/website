<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/dao/categorie-dao.php");
require_once("common/php/dao/entrainement-dao.php");

class ModalEntrainementsCategorieCtrl extends AbstractViewCtrl {
	
	private $categorie;

	private $categorieDAO;
	private $entrainementDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idCategorie" => $_GET["idCategorie"]
		), true);
		
		if (isAdminConnected_bureau()) {
			$this->categorieDAO = new CategorieDAO($this->getDatabaseConnection());
			$this->entrainementDAO = new EntrainementDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$checkParams = $this->checkParams(array(
			array(GET, "idCategorie")
		));
		if ($checkParams) {
			$this->checkIdCategorie();
		}
	}

	private function checkIdCategorie() {
		$this->categorie = $this->categorieDAO->getById($_GET["idCategorie"]);
			
		if ($this->categorie == null) {
			$this->sendCheckError(
				HTTP_404, 
				"La categorie (idCategorie = '".$_GET["idCategorie"]."') n'existe pas", 
				"webapp/views/common/error/404/404.html.php"
			);
		}
	}

	public function getCategorie() {
		return $this->categorie;
	}

	public function getEntrainementsCategorie() {
		return $this->entrainementDAO->getByCategorie($this->categorie[CATEGORIE_ID]);
	}

	public function getOptionsHeureEntrainement() {
		$html = "";

		for ($h = 0 ; $h <= 23 ; $h++) {
			$heure = $h;
			if ($heure < 10) {
				$heure = "0".$heure;
			}

			for ($m = 0 ; $m <= 59 ; $m += 15) {
				$minutes = $m;
				if ($minutes < 10) {
					$minutes = "0".$minutes;
				}

				$html .= "<option value=\"".$heure.SLASH_TIME_SEPARATOR.$minutes."\">".$heure.SLASH_TIME_SEPARATOR.$minutes."</option>";
			}
		}

		return $html;
	}
}

?>