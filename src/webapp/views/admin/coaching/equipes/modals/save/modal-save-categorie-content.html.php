<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/coaching/equipes/modals/save/", 
    "modal-save-categorie", 
    "ModalSaveCategorieCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
    $categorie = $ctrl->getCategorie();
    ?>
    <div class="modal-header">
        <div class="modal-title">
            <?php echo $categorie[CATEGORIE_NOM]; ?> - 
            <span class="normal">Edition</span>
        </div>
    </div>
    <div class="modal-body">
        <div class="form-horizontal" id="form-save-categorie">
            <div class="form-group">
                <label for="form-save-categorie__input-nb-equipes" class="col-xs-3 control-label">Nb. &eacute;quipes :</label>
                <div class="col-xs-2 form-input" for="form-save-categorie__input-nb-equipes">
                    <select class="form-control" id="form-save-categorie__input-nb-equipes">
                        <?php
                        for ($i = 0 ; $i <= 5 ; $i++) {
                            $selected = "";
                            if ($categorie[CATEGORIE_NB_EQUIPES] == $i) {
                                $selected = " selected";
                            }
                            ?>
                            <option value="<?php echo $i; ?>"<?php echo $selected; ?>><?php echo $i; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-categorie__input-nb-equipes"></div>
                <label for="form-save-categorie__input-regroupement-matchs" class="col-xs-3 control-label">Matchs regroup&eacute;s (dispos) ?</label>
                <div class="col-xs-1 form-input" for="form-save-categorie__input-regroupement-matchs">
                    <?php
                    $checked = "";
                    if ($categorie[CATEGORIE_REGROUPEMENT_MATCHS] == 1) {
                        $checked = " checked";
                    }
                    ?>
                    <input type="checkbox" id="form-save-categorie__input-regroupement-matchs"<?php echo $checked; ?>>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-categorie__input-regroupement-matchs"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-8">
                <div class="form-result-message"></div>
            </div>
            <div class="col-xs-4 text-right">
                <button id="modal-save-categorie__bt-sauvegarder" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-ok"></span>
                    </div>
                    <div class="button__text">Sauvegarder</div>
                    <div class="loader"></div>
                </button>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>