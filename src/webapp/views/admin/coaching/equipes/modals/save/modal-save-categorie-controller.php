<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/categorie-dao.php");

class ModalSaveCategorieCtrl extends AbstractViewCtrl {
	
	private $categorie;

	private $categorieDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idCategorie" => $_GET["idCategorie"]
		), true);
		
		if (isAdminConnected_coach()) {
			$this->categorieDAO = new CategorieDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$checkParams = $this->checkParams(array(
			array(GET, "idCategorie")
		));
		if ($checkParams) {
			$this->checkIdCategorie();
		}
	}

	private function checkIdCategorie() {
		$this->categorie = $this->categorieDAO->getById($_GET["idCategorie"]);
			
		if ($this->categorie == null) {
			$this->sendCheckError(
				HTTP_404, 
				"La categorie (idCategorie = '".$_GET["idCategorie"]."') n'existe pas", 
				"webapp/views/common/error/404/404.html.php"
			);
		}
	}

	public function getCategorie() {
		return $this->categorie;
	}
}

?>