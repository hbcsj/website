<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/categorie-dao.php");
require_once("common/php/dao/licencie-dao.php");
require_once("common/php/managers/categorie-manager.php");

class ModalCoachsCategorieCtrl extends AbstractViewCtrl {
	
	private $categorie;

	private $licencieDAO;
	private $categorieDAO;

	private $categorieManager;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idCategorie" => $_GET["idCategorie"]
		), true);
		
		if (isAdminConnected_bureau()) {
			$this->licencieDAO = new LicencieDAO($this->getDatabaseConnection());
			$this->categorieDAO = new CategorieDAO($this->getDatabaseConnection());

			$this->categorieManager = new CategorieManager($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$checkParams = $this->checkParams(array(
			array(GET, "idCategorie")
		));
		if ($checkParams) {
			$this->checkIdCategorie();
		}
	}

	private function checkIdCategorie() {
		$this->categorie = $this->categorieDAO->getById($_GET["idCategorie"]);
			
		if ($this->categorie == null) {
			$this->sendCheckError(
				HTTP_404, 
				"La categorie (idCategorie = '".$_GET["idCategorie"]."') n'existe pas", 
				"webapp/views/common/error/404/404.html.php"
			);
		}
	}

	public function getCategorie() {
		return $this->categorie;
	}

	public function getCoachsCategorie() {
		return $this->categorieManager->getCoachsCategorie($this->categorie[CATEGORIE_ID]);
	}

	public function getLicencies() {
		return $this->licencieDAO->getNotCoachsByCategorieId(
			$this->categorie[CATEGORIE_ID],
			LICENCIE_TABLE_NAME.".".LICENCIE_NOM.", ".LICENCIE_TABLE_NAME.".".LICENCIE_PRENOM
		);
	}
}

?>