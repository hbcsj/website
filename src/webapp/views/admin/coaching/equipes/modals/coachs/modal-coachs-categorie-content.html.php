<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/coaching/equipes/modals/coachs/", 
    "modal-coachs-categorie", 
    "ModalCoachsCategorieCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_bureau()) {
    $categorie = $ctrl->getCategorie();
    $coachs = $ctrl->getCoachsCategorie();
    $licencies = $ctrl->getLicencies();
    ?>
    <div class="modal-header">
        <div class="modal-title">
            <?php echo $categorie[CATEGORIE_NOM]; ?> - 
            <span class="normal">Coachs</span>
        </div>
        <div class="loader loader-modal" for="form-coachs-categorie"></div>
    </div>
    <div class="modal-body">
        <div class="form-horizontal" id="form-coachs-categorie">
            <?php
            if (sizeof($coachs) > 0) {
                foreach ($coachs as $coach) {
                    ?>
                    <div class="form-group">
                        <label for="form-coachs-categorie__input-licencie-<?php echo $coach[LICENCIE_ID]; ?>" class="col-xs-2 control-label">Licenci&eacute; :</label>
                        <div class="col-xs-3 form-input" for="form-coachs-categorie__input-licencie-<?php echo $coach[LICENCIE_ID]; ?>">
                            <input type="text" class="form-control" value="<?php echo $coach[LICENCIE_NOM]." ".$coach[LICENCIE_PRENOM]; ?>" readonly>
                        </div>
                        <label for="form-coachs-categorie__input-en-soutien-<?php echo $coach[LICENCIE_ID]; ?>" class="col-xs-2 col-xs-offset-1 control-label">En soutien ?</label>
                        <div class="col-xs-1 form-input" for="form-coachs-categorie__input-en-soutien-<?php echo $coach[LICENCIE_ID]; ?>">
                            <?php
                            $checked = "";
                            if ($coach[COACH_EN_SOUTIEN] == 1) {
                                $checked = " checked";
                            }
                            ?>
                            <input type="checkbox" id="form-coachs-categorie__input-en-soutien-<?php echo $coach[LICENCIE_ID]; ?>"<?php echo $checked; ?>>
                        </div>
                        <div class="col-xs-3 text-right">
                            <button class="btn btn-default bt-modifier-coach" id-licencie="<?php echo $coach[LICENCIE_ID]; ?>">
                                <div class="button__icon">
                                    <span class="glyphicon glyphicon-ok"></span>
                                </div>
                                <div class="button__text">OK</div>
                            </button>
                            <button class="btn btn-default bt-supprimer-coach" id-licencie="<?php echo $coach[LICENCIE_ID]; ?>">
                                <div class="button__icon">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </div>
                                <div class="button__text">Sup.</div>
                            </button>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <div class="form-horizontal" id="form-add-coach-categorie">
            <div class="form-group">
                <label for="form-add-coach-categorie__input-licencie" class="col-xs-2 control-label required">Licenci&eacute; :</label>
                <div class="col-xs-3 form-input" for="form-add-coach-categorie__input-licencie">
                    <select class="form-control" id="form-add-coach-categorie__input-licencie" required>
                        <option value=""></option>
                        <?php
                        if (sizeof($licencies)) {
                            foreach ($licencies as $licencie) {
                                ?>
                                <option value="<?php echo $licencie[LICENCIE_ID]; ?>"><?php echo $licencie[LICENCIE_NOM]." ".$licencie[LICENCIE_PRENOM]; ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-add-coach-categorie__input-licencie"></div>
                <label for="form-add-coach-categorie__input-en-soutien" class="col-xs-2 control-label">En soutien ?</label>
                <div class="col-xs-1 form-input" for="form-add-coach-categorie__input-en-soutien">
                    <input type="checkbox" id="form-add-coach-categorie__input-en-soutien">
                </div>
                <div class="col-xs-3 text-right">
                    <button id="modal-coachs-categorie__bt-ajouter" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-plus"></span>
                        </div>
                        <div class="button__text">OK</div>
                    </button>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-required text-right">Les champs marqu&eacute;s d'une * sont obligatoires</div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-12">
                <div class="form-result-message"></div>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>