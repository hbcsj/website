AdminCoachingEquipesController = {};

AdminCoachingEquipesController.load_StatsCategories = function() {
    if (Utils.exists('#container-stats-categories')) {
        AjaxUtils.loadView(
            'admin/coaching/equipes/views/stats/stats-equipes',
            '#container-stats-categories',
            null,
            function() {},
            '#loader-page'
        );
    }
};

AdminCoachingEquipesController.load_TableCategories = function() {
    if (Utils.exists('#container-table-categories')) {
        AjaxUtils.loadView(
            'admin/coaching/equipes/views/table/table-equipes',
            '#container-table-categories',
            null,
            function() {
                AdminController.control_checkAllCheckboxes('#check-all-categories', '#container-table-categories table tbody');
                onClick_btMailingList();
                control_EditCoachsCategorie();
                control_EditEntrainementsCategorie();
                control_EditCategorie();
            },
            '#loader-page'
        );
    }

    function onClick_btMailingList() {
        $('#categories__bt-mailing-list').unbind('click');
        $('#categories__bt-mailing-list').click(function() {
            var idsCategorie = [];
            var checkboxesCategorie = $('input.checkbox-categorie:checked');
            
            if (checkboxesCategorie.length > 0) {
                checkboxesCategorie.each(function() {
                    idsCategorie.push($(this).attr('id-categorie'));
                });
                
                var params = {
                    idsCategorie: idsCategorie.join(',')
                };
                AjaxUtils.loadView(
                    'admin/coaching/common/modals/mailing-list/modal-mailing-list-content',
                    '#modal-mailing-list-content',
                    params,
                    function() {
                        $('#modal-mailing-list__bt-copier').attr('data-clipboard-text', $('#form-mailing-list__input-mailing-list').val());
                        onClick_btCopier();
                    },
                    '#loader-page'
                );
                $('#modal-mailing-list').modal('show');
            } else {
                ViewUtils.alert(
                    'Veuillez s&eacute;lectionner au moins une cat&eacute;gorie', 
                    ViewUtils.alertTypes.error,
                    null
                );
            }
        });

        function onClick_btCopier() {
            $('#modal-mailing-list__bt-copier').unbind('click');
            $('#modal-mailing-list__bt-copier').click(function() {
                var clipboard = new ClipboardJS('#modal-mailing-list__bt-copier');
                clipboard.on('success', function(e) {
                    FormUtils.displayFormResultMessage('#modal-mailing-list .form-result-message', 'Mailing list copi&eacute;e', FormUtils.resultsTypes.ok);
                });
            });
        }
    }

    function control_EditCoachsCategorie() {
        $('.td-coachs-categorie').unbind('click');
        $('.td-coachs-categorie').click(function() {
            var idCategorie = $(this).attr('id-categorie');
            open_ModalCoachsCategorie(idCategorie);
        });

        function open_ModalCoachsCategorie(idCategorie) {
            load_ModalCoachs(idCategorie);
            $('#modal-coachs-categorie').modal('show');
        }

        function load_ModalCoachs(idCategorie) {
            var params = {
                idCategorie: idCategorie
            };
            AjaxUtils.loadView(
                'admin/coaching/equipes/modals/coachs/modal-coachs-categorie-content',
                '#modal-coachs-categorie-content',
                params,
                function() {
                    onClick_btModifier(idCategorie);
                    onClick_btSupprimer(idCategorie);
                    onClick_btAjouter(idCategorie);
                },
                '#loader-page'
            );
        }

        function onClick_btModifier(idCategorie) {
            $('.bt-modifier-coach').unbind('click');
            $('.bt-modifier-coach').click(function() {
                var idLicencie = $(this).attr('id-licencie');

                var inputEnSoutien = ($('#form-coachs-categorie__input-en-soutien-'+idLicencie).prop('checked')) ? '1' : '0';

                updateCoach(idCategorie, idLicencie, inputEnSoutien);
            });

            function updateCoach(idCategorie, idLicencie, inputEnSoutien) {
                AjaxUtils.callService(
                    'categorie', 
                    'updateCoach', 
                    AjaxUtils.dataTypes.json, 
                    AjaxUtils.requestTypes.post, 
                    {
                        categorieId: idCategorie,
                        licencieId: idLicencie
                    }, 
                    {
                        enSoutien: inputEnSoutien
                    }, 
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-coachs-categorie .form-result-message', 'Coach modifi&eacute;', FormUtils.resultsTypes.ok);
                            $('.loader-modal[for=form-coachs-categorie]').hide();
                            AdminCoachingEquipesController.load_StatsCategories();
                            AdminCoachingEquipesController.load_TableCategories();
                            load_ModalCoachs(idCategorie);
                        } else {
                            ViewUtils.activeButtonAndHideLoader('.bt-modifier-coach[id-licencie='+idLicencie+']', '.loader-modal[for=form-coachs-categorie]');
                            FormUtils.displayFormResultMessage('#modal-coachs-categorie .form-result-message', 'Erreur durant la modification', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('.bt-modifier-coach[id-licencie='+idLicencie+']', '.loader-modal[for=form-coachs-categorie]');
                        FormUtils.displayFormResultMessage('#modal-coachs-categorie .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            }
        }

        function onClick_btSupprimer(idCategorie) {
            $('.bt-supprimer-coach').unbind('click');
            $('.bt-supprimer-coach').click(function() {
                var idLicencie = $(this).attr('id-licencie');

                FormUtils.hideFormErrors('#modal-coachs-categorie #form-coachs-categorie');
                ViewUtils.unsetHTMLAndHide('#modal-coachs-categorie .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('.bt-supprimer-coach[id-licencie='+idLicencie+']', '.loader-modal[for=form-coachs-categorie]');
                AjaxUtils.callService(
                    'categorie', 
                    'deleteCoach', 
                    AjaxUtils.dataTypes.json, 
                    AjaxUtils.requestTypes.post, 
                    null, 
                    {
                        categorieId: idCategorie,
                        licencieId: idLicencie
                    }, 
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-coachs-categorie .form-result-message', 'Coach supprim&eacute;', FormUtils.resultsTypes.ok);
                            $('.loader-modal[for=form-coachs-categorie]').hide();
                            AdminCoachingEquipesController.load_StatsCategories();
                            AdminCoachingEquipesController.load_TableCategories();
                            load_ModalCoachs(idCategorie);
                        } else {
                            ViewUtils.activeButtonAndHideLoader('#.bt-supprimer-coach[id-licencie='+idLicencie+']', '.loader-modal[for=form-coachs-categorie]');
                            FormUtils.displayFormResultMessage('#modal-coachs-categorie .form-result-message', 'Erreur durant la suppression', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('.bt-supprimer-coach[id-licencie='+idLicencie+']', '.loader-modal[for=form-coachs-categorie]');
                        FormUtils.displayFormResultMessage('#modal-coachs-categorie .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            });
        }

        function onClick_btAjouter(idCategorie) {
            $('#modal-coachs-categorie__bt-ajouter').unbind('click');
            $('#modal-coachs-categorie__bt-ajouter').click(function() {
                FormUtils.hideFormErrors('#modal-coachs-categorie #form-add-coach-categorie');
                ViewUtils.unsetHTMLAndHide('#modal-coachs-categorie .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('#modal-coachs-categorie__bt-ajouter', '.loader-modal[for=form-coachs-categorie]');

                var formValidation = FormUtils.validateForm('#modal-coachs-categorie #form-add-coach-categorie');
                
                if (formValidation) {
                    var inputLicencie = $.trim($('#form-add-coach-categorie__input-licencie').val());
                    var inputEnSoutien = ($('#form-add-coach-categorie__input-en-soutien').prop('checked')) ? '1' : '0';

                    addCoach(idCategorie, inputLicencie, inputEnSoutien);
                } else {
                    ViewUtils.activeButtonAndHideLoader('#modal-coachs-categorie__bt-ajouter', '.loader-modal[for=form-coachs-categorie]');
                }
            });
    
            function addCoach(idCategorie, inputLicencie, inputEnSoutien) {
                AjaxUtils.callService(
                    'categorie', 
                    'addCoach', 
                    AjaxUtils.dataTypes.json, 
                    AjaxUtils.requestTypes.post, 
                    {
                        categorieId: idCategorie
                    }, 
                    {
                        licencieId: inputLicencie,
                        enSoutien: inputEnSoutien
                    }, 
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-coachs-categorie .form-result-message', 'Coach ajout&eacute;', FormUtils.resultsTypes.ok);
                            $('.loader-modal[for=form-coachs-categorie]').hide();
                            AdminCoachingEquipesController.load_StatsCategories();
                            AdminCoachingEquipesController.load_TableCategories();
                            load_ModalCoachs(idCategorie);
                        } else {
                            ViewUtils.activeButtonAndHideLoader('#modal-coachs-categorie__bt-ajouter', '.loader-modal[for=form-coachs-categorie]');
                            FormUtils.displayFormResultMessage('#modal-coachs-categorie .form-result-message', 'Erreur durant l\'ajout', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('#modal-coachs-categorie__bt-ajouter', '.loader-modal[for=form-coachs-categorie]');
                        FormUtils.displayFormResultMessage('#modal-coachs-categorie .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            }
        }
    }

    function control_EditEntrainementsCategorie() {
        $('.td-entrainements-categorie').unbind('click');
        $('.td-entrainements-categorie').click(function() {
            var idCategorie = $(this).attr('id-categorie');
            open_ModalEntrainementsCategorie(idCategorie);
        });

        function open_ModalEntrainementsCategorie(idCategorie) {
            load_ModalEntrainements(idCategorie);
            $('#modal-entrainements-categorie').modal('show');
        }

        function load_ModalEntrainements(idCategorie) {
            var params = {
                idCategorie: idCategorie
            };
            AjaxUtils.loadView(
                'admin/coaching/equipes/modals/entrainements/modal-entrainements-categorie-content',
                '#modal-entrainements-categorie-content',
                params,
                function() {
                    onClick_btSupprimer(idCategorie);
                    onClick_btAjouter(idCategorie);
                },
                '#loader-page'
            );
        }

        function onClick_btSupprimer(idCategorie) {
            $('.bt-supprimer-entrainement').unbind('click');
            $('.bt-supprimer-entrainement').click(function() {
                var idEntrainement = $(this).attr('id-entrainement');

                FormUtils.hideFormErrors('#modal-entrainements-categorie #form-coachs-categorie');
                ViewUtils.unsetHTMLAndHide('#modal-entrainements-categorie .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('.bt-supprimer-entrainement[id-entrainement='+idEntrainement+']', '.loader-modal[for=form-entrainements-categorie]');
                AjaxUtils.callService(
                    'entrainement', 
                    'delete', 
                    AjaxUtils.dataTypes.json, 
                    AjaxUtils.requestTypes.post, 
                    null, 
                    {
                        id: idEntrainement
                    }, 
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-entrainements-categorie .form-result-message', 'Entra&icirc;nement supprim&eacute;', FormUtils.resultsTypes.ok);
                            $('.loader-modal[for=form-entrainements-categorie]').hide();
                            AdminCoachingEquipesController.load_StatsCategories();
                            AdminCoachingEquipesController.load_TableCategories();
                            load_ModalEntrainements(idCategorie);
                        } else {
                            ViewUtils.activeButtonAndHideLoader('#.bt-supprimer-entrainement[id-entrainement='+idEntrainement+']', '.loader-modal[for=form-entrainements-categorie]');
                            FormUtils.displayFormResultMessage('#modal-entrainements-categorie .form-result-message', 'Erreur durant la suppression', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('.bt-supprimer-entrainement[id-entrainement='+idEntrainement+']', '.loader-modal[for=form-entrainements-categorie]');
                        FormUtils.displayFormResultMessage('#modal-entrainements-categorie .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            });
        }

        function onClick_btAjouter(idCategorie) {
            $('#modal-entrainements-categorie__bt-ajouter').unbind('click');
            $('#modal-entrainements-categorie__bt-ajouter').click(function() {
                FormUtils.hideFormErrors('#modal-entrainements-categorie #form-add-entrainement-categorie');
                ViewUtils.unsetHTMLAndHide('#modal-entrainements-categorie .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('#modal-entrainements-categorie__bt-ajouter', '.loader-modal[for=form-entrainements-categorie]');

                var formValidation = FormUtils.validateForm('#modal-entrainements-categorie #form-add-entrainement-categorie');
                
                if (formValidation) {
                    var inputJour = $.trim($('#form-add-entrainement-categorie__input-jour').val());
                    var inputHeureDebut = $.trim($('#form-add-entrainement-categorie__input-heure-debut').val());
                    var inputHeureFin = $.trim($('#form-add-entrainement-categorie__input-heure-fin').val());

                    addEntrainement(idCategorie, inputJour, inputHeureDebut, inputHeureFin);
                } else {
                    ViewUtils.activeButtonAndHideLoader('#modal-entrainements-categorie__bt-ajouter', '.loader-modal[for=form-entrainements-categorie]');
                }
            });
    
            function addEntrainement(idCategorie, inputJour, inputHeureDebut, inputHeureFin) {
                AjaxUtils.callService(
                    'entrainement', 
                    'save', 
                    AjaxUtils.dataTypes.json, 
                    AjaxUtils.requestTypes.post, 
                    null, 
                    {
                        jour: inputJour,
                        heureDebut: inputHeureDebut,
                        heureFin: inputHeureFin,
                        categorieId: idCategorie
                    }, 
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-entrainements-categorie .form-result-message', 'Entra&icrc;nement ajout&eacute;', FormUtils.resultsTypes.ok);
                            $('.loader-modal[for=form-entrainements-categorie]').hide();
                            AdminCoachingEquipesController.load_StatsCategories();
                            AdminCoachingEquipesController.load_TableCategories();
                            load_ModalEntrainements(idCategorie);
                        } else {
                            ViewUtils.activeButtonAndHideLoader('#modal-entrainements-categorie__bt-ajouter', '.loader-modal[for=form-entrainements-categorie]');
                            FormUtils.displayFormResultMessage('#modal-entrainements-categorie .form-result-message', 'Erreur durant l\'ajout', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('#modal-entrainements-categorie__bt-ajouter', '.loader-modal[for=form-entrainements-categorie]');
                        FormUtils.displayFormResultMessage('#modal-entrainements-categorie .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            }
        }
    }

    function control_EditCategorie() {
        $('.glyphicon-pencil').unbind('click');
        $('.glyphicon-pencil').click(function() {
            var idCategorie = $(this).attr('id-categorie');
            open_ModalSaveCategorie(idCategorie);
        });

        function open_ModalSaveCategorie(idCategorie) {
            var params = {
                idCategorie: idCategorie
            };
            AjaxUtils.loadView(
                'admin/coaching/equipes/modals/save/modal-save-categorie-content',
                '#modal-save-categorie-content',
                params,
                function() {
                    onClick_btSauvegarder(idCategorie);
                },
                '#loader-page'
            );
            $('#modal-save-categorie').modal('show');
    
            function onClick_btSauvegarder(idCategorie) {
                $('#modal-save-categorie__bt-sauvegarder').unbind('click');
                $('#modal-save-categorie__bt-sauvegarder').click(function() {
                    FormUtils.hideFormErrors('#modal-save-categorie #form-save-categorie');
                    ViewUtils.unsetHTMLAndHide('#modal-save-categorie .form-result-message');
                    ViewUtils.desactiveButtonAndShowLoader('#modal-save-categorie__bt-sauvegarder', '#modal-save-categorie__bt-sauvegarder .loader');
    
                    var formValidation = FormUtils.validateForm('#modal-save-categorie #form-save-categorie');
                    
                    if (formValidation) {
                        var inputNbEquipes = $.trim($('#form-save-categorie__input-nb-equipes').val());
                        var inputRegroupementMatchs = ($('#form-save-categorie__input-regroupement-matchs').prop('checked')) ? '1' : '0';
    
                        saveCategorie(idCategorie, inputNbEquipes, inputRegroupementMatchs);
                    } else {
                        ViewUtils.activeButtonAndHideLoader('#modal-save-categorie__bt-sauvegarder', '#modal-save-categorie__bt-sauvegarder .loader');
                    }
                });
    
                function saveCategorie(idCategorie, inputNbEquipes, inputRegroupementMatchs) {
                    AjaxUtils.callService(
                        'categorie', 
                        'save', 
                        AjaxUtils.dataTypes.json, 
                        AjaxUtils.requestTypes.post, 
                        {
                            id: idCategorie
                        }, 
                        {
                            nbEquipes: inputNbEquipes,
                            regroupementMatchs: inputRegroupementMatchs
                        }, 
                        function(response) {
                            if (response.success) {
                                FormUtils.displayFormResultMessage('#modal-save-categorie .form-result-message', 'Cat&eacute;gorie sauvegard&eacute;e', FormUtils.resultsTypes.ok);
                                $('#modal-save-categorie__bt-sauvegarder .loader').hide();
                                AdminCoachingEquipesController.load_StatsCategories();
                                AdminCoachingEquipesController.load_TableCategories();
                            } else {
                                ViewUtils.activeButtonAndHideLoader('#modal-save-categorie__bt-sauvegarder', '#modal-save-categorie__bt-sauvegarder .loader');
                                FormUtils.displayFormResultMessage('#modal-save-categorie .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
                            }
                        },
                        function(response) {
                            ViewUtils.activeButtonAndHideLoader('#modal-save-categorie__bt-sauvegarder', '#modal-save-categorie__bt-sauvegarder .loader');
                            FormUtils.displayFormResultMessage('#modal-save-categorie .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                        }
                    );
                }
            }
        }
    }
};

initializePage = function() {
    AdminController.initializePage('coaching', 'equipes');

    AdminCoachingEquipesController.load_StatsCategories();
    AdminCoachingEquipesController.load_TableCategories();
};