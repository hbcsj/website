AdminCoachingLicenciesController = {};

AdminCoachingLicenciesController.load_StatsLicencies = function() {
    if (Utils.exists('#container-stats-licencies')) {
        AjaxUtils.loadView(
            'admin/coaching/licencies/views/stats/stats-licencies',
            '#container-stats-licencies',
            null,
            function() {},
            '#loader-page'
        );
    }
};

AdminCoachingLicenciesController.load_RechercheAndTableLicencies = function() {
    if (Utils.exists('#container-recherche-licencies')) {
        AjaxUtils.loadView(
            'admin/coaching/licencies/views/recherche/recherche-licencies',
            '#container-recherche-licencies',
            null,
            function() {
                FormRechercheUtils.control_toggleFormRecherche();
                onClick_btRechercher();
                onClick_btReset();
                AdminCoachingLicenciesController.load_TableLicencies();
            },
            '#loader-page'
        );
    }

    function onClick_btRechercher() {
        $('#recherche-licencies__bt-rechercher').unbind('click');
        $('#recherche-licencies__bt-rechercher').click(function() {
            AdminCoachingLicenciesController.load_TableLicencies();
        });
    }

    function onClick_btReset() {
        $('#recherche-licencies__bt-reset').unbind('click');
        $('#recherche-licencies__bt-reset').click(function() {
            $('#form-recherche-licencies__input-nom').val('');
            $('#form-recherche-licencies__input-prenom').val('');
            $('#form-recherche-licencies__input-email').val('');
            $('#form-recherche-licencies__input-categorie').val('');
            $('#form-recherche-licencies__input-is-gardien').val('');
            $('#form-recherche-licencies__input-is-arbitre').val('');
            $('#form-recherche-licencies__input-is-bureau').val('');
            $('#form-recherche-licencies__input-is-ca').val('');
            $('#form-recherche-licencies__input-is-actif').val('');
            $('#form-recherche-licencies__input-annee-naissance').val('');

            AdminCoachingLicenciesController.load_TableLicencies();
        });
    }
};

AdminCoachingLicenciesController.load_TableLicencies = function() {
    if (Utils.exists('#container-table-licencies')) {
        var params = {
            nom: $.trim($('#form-recherche-licencies__input-nom').val()),
            prenom: $.trim($('#form-recherche-licencies__input-prenom').val()),
            email: $.trim($('#form-recherche-licencies__input-email').val()),
            categorieId: $('#form-recherche-licencies__input-categorie').val(),
            gardien: $('#form-recherche-licencies__input-is-gardien').val(),
            arbitre: $('#form-recherche-licencies__input-is-arbitre').val(),
            bureau: $('#form-recherche-licencies__input-is-bureau').val(),
            ca: $('#form-recherche-licencies__input-is-ca').val(),
            actif: $('#form-recherche-licencies__input-is-actif').val(),
            anneeNaissance: $('#form-recherche-licencies__input-annee-naissance').val()
        };
        AjaxUtils.loadView(
            'admin/coaching/licencies/views/table/table-licencies',
            '#container-table-licencies',
            params,
            function() {
                AdminController.control_checkAllCheckboxes('#check-all-licencies', '#container-table-licencies table tbody');
                onClick_btMailingList();
                control_ActionsClub();
                control_Arbitrages();
                control_AddLicencie();
                control_EditLicencie();
                control_DeleteLicencie();
                control_ActifLicencie();
            },
            '#loader-page'
        );
    }

    function onClick_btMailingList() {
        $('#licencies__bt-mailing-list').unbind('click');
        $('#licencies__bt-mailing-list').click(function() {
            var idsLicencie = [];
            var checkboxesLicencie = $('input.checkbox-licencie:checked');
            
            if (checkboxesLicencie.length > 0) {
                checkboxesLicencie.each(function() {
                    idsLicencie.push($(this).attr('id-licencie'));
                });
                
                var params = {
                    idsLicencie: idsLicencie.join(',')
                };
                AjaxUtils.loadView(
                    'admin/coaching/common/modals/mailing-list/modal-mailing-list-content',
                    '#modal-mailing-list-content',
                    params,
                    function() {
                        $('#modal-mailing-list__bt-copier').attr('data-clipboard-text', $('#form-mailing-list__input-mailing-list').val());
                        onClick_btCopier();
                    },
                    '#loader-page'
                );
                $('#modal-mailing-list').modal('show');
            } else {
                ViewUtils.alert(
                    'Veuillez s&eacute;lectionner au moins un licencie&eacute;', 
                    ViewUtils.alertTypes.error,
                    null
                );
            }
        });

        function onClick_btCopier() {
            $('#modal-mailing-list__bt-copier').unbind('click');
            $('#modal-mailing-list__bt-copier').click(function() {
                var clipboard = new ClipboardJS('#modal-mailing-list__bt-copier');
                clipboard.on('success', function(e) {
                    FormUtils.displayFormResultMessage('#modal-mailing-list .form-result-message', 'Mailing list copi&eacute;e', FormUtils.resultsTypes.ok);
                });
            });
        }
    }
    
    function control_ActionsClub() {
        $('.bt-licencie-actions-club').unbind('click');
        $('.bt-licencie-actions-club').click(function() {
            var idLicencie = $(this).attr('id-licencie');
            var params = {
                idLicencie: idLicencie
            };
            AjaxUtils.loadView(
                'admin/coaching/licencies/modals/actions-club/modal-actions-club-licencie-content',
                '#modal-actions-club-licencie-content',
                params,
                function() {},
                '#loader-page'
            );
            $('#modal-actions-club-licencie').modal('show');
        });
    }
    
    function control_Arbitrages() {
        $('.bt-licencie-arbitrages').unbind('click');
        $('.bt-licencie-arbitrages').click(function() {
            var idLicencie = $(this).attr('id-licencie');
            var params = {
                idLicencie: idLicencie
            };
            AjaxUtils.loadView(
                'admin/coaching/licencies/modals/arbitrages/modal-arbitrages-licencie-content',
                '#modal-arbitrages-licencie-content',
                params,
                function() {},
                '#loader-page'
            );
            $('#modal-arbitrages-licencie').modal('show');
        });
    }

    function control_AddLicencie() {
        $('#licencies__bt-ajouter').unbind('click');
        $('#licencies__bt-ajouter').click(function() {
            open_ModalSaveLicencie(null);
        });
    }

    function control_EditLicencie() {
        $('.glyphicon-pencil').unbind('click');
        $('.glyphicon-pencil').click(function() {
            var idLicencie = $(this).attr('id-licencie');
            open_ModalSaveLicencie(idLicencie);
        });
    }

    function open_ModalSaveLicencie(idLicencie) {
        var params = null;
        if (idLicencie != null) {
            params = {
                idLicencie: idLicencie
            };
        }
        AjaxUtils.loadView(
            'admin/coaching/licencies/modals/save/modal-save-licencie-content',
            '#modal-save-licencie-content',
            params,
            function() {
                onClick_btSauvegarder(idLicencie);
            },
            '#loader-page'
        );
        $('#modal-save-licencie').modal('show');

        function onClick_btSauvegarder(idLicencie) {
            $('#modal-save-licencie__bt-sauvegarder').unbind('click');
            $('#modal-save-licencie__bt-sauvegarder').click(function() {
                FormUtils.hideFormErrors('#modal-save-licencie #form-save-licencie');
                ViewUtils.unsetHTMLAndHide('#modal-save-licencie .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('#modal-save-licencie__bt-sauvegarder', '#modal-save-licencie__bt-sauvegarder .loader');

                var formValidation = FormUtils.validateForm('#modal-save-licencie #form-save-licencie');
                
                if (formValidation) {
                    var inputNom = $.trim($('#form-save-licencie__input-nom').val());
                    var inputPrenom = $.trim($('#form-save-licencie__input-prenom').val());
                    var inputDateNaissance = $.trim($('#form-save-licencie__input-date-naissance').val());
                    var inputCategories = $('#form-save-licencie__input-categories').val();
                    var inputAdresse = $.trim($('#form-save-licencie__input-adresse').val());
                    var inputCodePostal = $.trim($('#form-save-licencie__input-code-postal').val());
                    var inputVille = $.trim($('#form-save-licencie__input-ville').val());
                    var inputTelephone1 = $.trim($('#form-save-licencie__input-telephone1').val());
                    var inputTelephone2 = $.trim($('#form-save-licencie__input-telephone2').val());
                    var inputTelephone3 = $.trim($('#form-save-licencie__input-telephone3').val());
                    var inputEmail1 = $.trim($('#form-save-licencie__input-email1').val());
                    var inputEmail2 = $.trim($('#form-save-licencie__input-email2').val());
                    var inputEmail3 = $.trim($('#form-save-licencie__input-email3').val());
                    var inputGardien = ($('#form-save-licencie__input-gardien').prop('checked')) ? '1' : '0';
                    var inputArbitre = ($('#form-save-licencie__input-arbitre').prop('checked')) ? '1' : '0';
                    var inputBureau = ($('#form-save-licencie__input-bureau').prop('checked')) ? '1' : '0';
                    var inputCA = ($('#form-save-licencie__input-ca').prop('checked')) ? '1' : '0';
                    var inputLateralite = $.trim($('#form-save-licencie__input-lateralite').val());

                    saveLicencie(idLicencie, inputNom, inputPrenom, inputCategories, inputDateNaissance, inputAdresse, inputCodePostal, inputVille, inputTelephone1, inputTelephone2, inputTelephone3, inputEmail1, inputEmail2, inputEmail3, inputGardien, inputArbitre, inputBureau, inputCA, inputLateralite);
                } else {
                    ViewUtils.activeButtonAndHideLoader('#modal-save-licencie__bt-sauvegarder', '#modal-save-licencie__bt-sauvegarder .loader');
                }
            });

            function saveLicencie(idLicencie, inputNom, inputPrenom, inputCategories, inputDateNaissance, inputAdresse, inputCodePostal, inputVille, inputTelephone1, inputTelephone2, inputTelephone3, inputEmail1, inputEmail2, inputEmail3, inputGardien, inputArbitre, inputBureau, inputCA, inputLateralite) {
                AjaxUtils.callService(
                    'licencie', 
                    'save', 
                    AjaxUtils.dataTypes.json, 
                    AjaxUtils.requestTypes.post, 
                    {
                        id: idLicencie
                    }, 
                    {
                        nom: inputNom,
                        prenom: inputPrenom,
                        categorieIds: inputCategories,
                        dateNaissance: inputDateNaissance,
                        adresse: inputAdresse,
                        codePostal: inputCodePostal,
                        ville: inputVille,
                        telephone1: inputTelephone1,
                        telephone2: inputTelephone2,
                        telephone3: inputTelephone3,
                        email1: inputEmail1,
                        email2: inputEmail2,
                        email3: inputEmail3,
                        gardien: inputGardien,
                        arbitre: inputArbitre,
                        bureau: inputBureau,
                        ca: inputCA,
                        lateraliteId: inputLateralite
                    }, 
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-save-licencie .form-result-message', 'Licenci&eacute; sauvegard&eacute;', FormUtils.resultsTypes.ok);
                            $('#modal-save-licencie__bt-sauvegarder .loader').hide();
                            AdminCoachingLicenciesController.load_StatsLicencies();
                            AdminCoachingLicenciesController.load_TableLicencies();
                        } else {
                            ViewUtils.activeButtonAndHideLoader('#modal-save-licencie__bt-sauvegarder', '#modal-save-licencie__bt-sauvegarder .loader');
                            FormUtils.displayFormResultMessage('#modal-save-licencie .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('#modal-save-licencie__bt-sauvegarder', '#modal-save-licencie__bt-sauvegarder .loader');
                        FormUtils.displayFormResultMessage('#modal-save-licencie .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            }
        }
    }

    function control_DeleteLicencie() {
        $('.glyphicon-trash').unbind('click');
        $('.glyphicon-trash').click(function() {
            var idLicencie = $(this).attr('id-licencie');
            
            ViewUtils.prompt(
                'Supprimer ce licenci&eacute; ?', 
                {
                    btOkText: 'Oui',
                    btKoText: 'Non'
                },
                function() {
                    deleteLicencie(idLicencie);
                }, 
                null, 
                null
            );
        });

        function deleteLicencie(idLicencie) {
            $('.loader-delete[id-licencie='+idLicencie+']').show();
            AjaxUtils.callService(
                'licencie', 
                'delete', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idLicencie
                }, 
                function(response) {
                    $('.loader-delete[id-licencie='+idLicencie+']').hide();
                    if (response.success) {
                        AdminCoachingLicenciesController.load_StatsLicencies();
                        AdminCoachingLicenciesController.load_TableLicencies();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la suppression du licenci&eacute;', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-delete[id-licencie='+idLicencie+']').hide();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        }
    }

    function control_ActifLicencie() {
        $('.icon-actif').unbind('click');
        $('.icon-actif').click(function() {
            var idLicencie = $(this).attr('id-licencie');

            if ($(this).hasClass('glyphicon-remove')) {
                ViewUtils.prompt(
                    'D&eacute;sactiver ce licenci&eacute; ?', 
                    {
                        btOkText: 'Oui',
                        btKoText: 'Non'
                    },
                    function() {
                        activeDesactiveLicencie(idLicencie);
                    }, 
                    null, 
                    null
                );
            } else {
                activeDesactiveLicencie(idLicencie);
            }
        });
        
        function activeDesactiveLicencie(idLicencie) {
            $('.loader-actif[id-licencie='+idLicencie+']').show();
            AjaxUtils.callService(
                'licencie', 
                'toggleActif', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idLicencie
                }, 
                function(response) {
                    $('.loader-actif[id-licencie='+idLicencie+']').hide();
                    if (response.success) {
                        AdminCoachingLicenciesController.load_StatsLicencies();
                        AdminCoachingLicenciesController.load_TableLicencies();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant l\'activation / d&eacute;sactivation du licenci&eacute;', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-actif[id-licencie='+idLicencie+']').hide();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        }
    }
};

initializePage = function() {
    AdminController.initializePage('coaching', 'licencies');

    AdminCoachingLicenciesController.load_StatsLicencies();
    AdminCoachingLicenciesController.load_RechercheAndTableLicencies();
};