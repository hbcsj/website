<?php
require_once("core/php/resources/page.php");
require_once("common/php/lib/admin-utils.php");

$page = new Page(
    "webapp/views/admin/coaching/licencies/", 
    "admin-coaching-licencies", 
    "AdminCoachingLicenciesCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/admin/common/nav/nav.html.php");
if (isAdminConnected_coach()) {
    require_once("webapp/views/admin/coaching/sub-menu/sub-menu-admin-coaching.html.php");
}
?>

<div class="container-fluid animated fadeIn">
    <?php
    if (isAdminConnected_coach()) {
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h1>
                    Licenci&eacute;s
                    <button id="licencies__bt-ajouter" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-plus"></span>
                        </div>
                        <div class="button__text">Ajouter un licenci&eacute;</div>
                    </button>
                    <button id="licencies__bt-mailing-list" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-envelope"></span>
                        </div>
                        <div class="button__text">Mailing list</div>
                    </button>
                </h1>
            </div>
        </div>
        <div id="container-stats-licencies" class="container-stats"></div>
        <div id="container-recherche-licencies" class="container-recherche"></div>
        <div id="container-table-licencies" class="container-table"></div>
        <?php
    } else {
        require_once("webapp/views/common/mdp-form/mdp-form.html.php");
    }
    ?>
</div>

<?php
require_once("webapp/views/common/modals/alert/modal-alert.html.php");
require_once("webapp/views/common/modals/prompt/modal-prompt.html.php");

if (isAdminConnected_coach()) {
    require_once("webapp/views/admin/coaching/licencies/modals/save/modal-save-licencie.html.php");
    require_once("webapp/views/admin/coaching/licencies/modals/actions-club/modal-actions-club-licencie.html.php");
    require_once("webapp/views/admin/coaching/licencies/modals/arbitrages/modal-arbitrages-licencie.html.php");
    require_once("webapp/views/admin/coaching/common/modals/mailing-list/modal-mailing-list.html.php");
}

$page->finalizePage();
?>