<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/licencie-dao.php");

class StatsLicenciesCtrl extends AbstractViewCtrl {

	private $licencieDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_coach()) {
			$this->licencieDAO = new LicencieDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getNbLicencies() {
		return $this->licencieDAO->getNbTotalActifs()[LICENCIE_NB_LICENCIES];
	}
}

?>