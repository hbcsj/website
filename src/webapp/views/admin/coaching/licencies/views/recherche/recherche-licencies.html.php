<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/coaching/licencies/views/recherche/", 
    "recherche-licencies", 
    "RechercheLicenciesCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
    $categories = $ctrl->getCategories();
    ?>
    <div class="container-recherche__title" title="Ouvrir le formulaire de recherche" for="form-recherche-licencies">
        <div class="container-recherche__title__icon">
            <span class="glyphicon glyphicon-search"></span>
        </div>
        <div class="container-recherche__title__text">Recherche</div>
        <div class="container-recherche__title__arrow">
            <span class="glyphicon glyphicon-triangle-bottom"></span>
        </div>
    </div>
    <div class="form-horizontal" id="form-recherche-licencies">
        <div class="form-group">
            <label for="form-recherche-licencies__input-nom" class="col-xs-2 control-label">Nom :</label>
            <div class="col-xs-2 form-input" for="form-recherche-licencies__input-nom">
                <input type="text" class="form-control" id="form-recherche-licencies__input-nom">
            </div>
            <label for="form-recherche-licencies__input-categorie" class="col-xs-2 control-label">Cat&eacute;gorie</label>
            <div class="col-xs-2 form-input" for="form-recherche-licencies__input-categorie">
                <select class="form-control" id="form-recherche-licencies__input-categorie">
                    <option value=""></option>
                    <?php
                    if (sizeof($categories) > 0) {
                        foreach ($categories as $categorie) {
                            ?>
                            <option value="<?php echo $categorie[CATEGORIE_ID]; ?>"><?php echo $categorie[CATEGORIE_NOM]; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <label for="form-recherche-licencies__input-annee-naissance" class="col-xs-2 control-label">Ann&eacute;e de naissance :</label>
            <div class="col-xs-1 form-input" for="form-recherche-licencies__input-annee-naissance">
                <input type="text" class="form-control" id="form-recherche-licencies__input-annee-naissance">
            </div>
        </div>
        <div class="form-group">
            <label for="form-recherche-licencies__input-prenom" class="col-xs-2 control-label">Pr&eacute;nom :</label>
            <div class="col-xs-2 form-input" for="form-recherche-licencies__input-prenom">
                <input type="text" class="form-control" id="form-recherche-licencies__input-prenom">
            </div>
            <label for="form-recherche-licencies__input-is-gardien" class="col-xs-2 control-label">Gardien ?</label>
            <div class="col-xs-1 form-input" for="form-recherche-licencies__input-is-gardien">
                <select class="form-control" id="form-recherche-licencies__input-is-gardien">
                    <option value=""></option>
                    <option value="1">Oui</option>
                    <option value="0">Non</option>
                </select>
            </div>
            <label for="form-recherche-licencies__input-is-arbitre" class="col-xs-2 col-xs-offset-1 control-label">Arbitre ?</label>
            <div class="col-xs-1 form-input" for="form-recherche-licencies__input-is-arbitre">
                <select class="form-control" id="form-recherche-licencies__input-is-arbitre">
                    <option value=""></option>
                    <option value="1">Oui</option>
                    <option value="0">Non</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="form-recherche-licencies__input-email" class="col-xs-2 control-label">Email :</label>
            <div class="col-xs-2 form-input" for="form-recherche-licencies__input-email">
                <input type="text" class="form-control" id="form-recherche-licencies__input-email">
            </div>
            <label for="form-recherche-licencies__input-is-bureau" class="col-xs-2 control-label">Bureau ?</label>
            <div class="col-xs-1 form-input" for="form-recherche-licencies__input-is-bureau">
                <select class="form-control" id="form-recherche-licencies__input-is-bureau">
                    <option value=""></option>
                    <option value="1">Oui</option>
                    <option value="0">Non</option>
                </select>
            </div>
            <label for="form-recherche-licencies__input-is-ca" class="col-xs-2 col-xs-offset-1 control-label">C.A ?</label>
            <div class="col-xs-1 form-input" for="form-recherche-licencies__input-is-ca">
                <select class="form-control" id="form-recherche-licencies__input-is-ca">
                    <option value=""></option>
                    <option value="1">Oui</option>
                    <option value="0">Non</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="form-recherche-licencies__input-is-actif" class="col-xs-2 control-label">Actif ?</label>
            <div class="col-xs-1 form-input" for="form-recherche-licencies__input-is-actif">
                <select class="form-control" id="form-recherche-licencies__input-is-actif">
                    <option value=""></option>
                    <option value="1">Oui</option>
                    <option value="0">Non</option>
                </select>
            </div>
        </div>
    </div>
    <div class="container-recherche__footer" for="form-recherche-licencies">
        <button id="recherche-licencies__bt-rechercher" class="btn btn-default">
            <div class="button__icon">
                <span class="glyphicon glyphicon-search"></span>
            </div>
            <div class="button__text">Chercher</div>
        </button>
        <button id="recherche-licencies__bt-reset" class="btn btn-default">
            <div class="button__icon">
                <span class="glyphicon glyphicon-erase"></span>
            </div>
            <div class="button__text">Reset</div>
        </button>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>