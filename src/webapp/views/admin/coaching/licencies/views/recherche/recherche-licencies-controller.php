<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/categorie-dao.php");

class RechercheLicenciesCtrl extends AbstractViewCtrl {

	private $categorieDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_coach()) {
			$this->categorieDAO = new CategorieDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getCategories() {
		return $this->categorieDAO->getAll(CATEGORIE_TABLE_NAME.".".CATEGORIE_POSITION_AFFICHAGE);
	}
}

?>