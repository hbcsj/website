<?php
require_once("core/php/resources/view.php");
require_once("common/php/constants/config-constants.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");

$view = new View(
    "webapp/views/admin/coaching/licencies/views/table/", 
    "table-licencies", 
    "TableLicenciesCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
    $licencies = $ctrl->getLicencies();
    ?>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Nom</th>
                <th>Pr&eacute;nom</th>
                <th>Cat&eacute;gorie(s)</th>
                <th>Coach /<br>Responsable</th>
                <th class="text-center">Gardien ?</th>
                <th class="text-center">Arbitre ?</th>
                <th class="text-center">Bureau ?</th>
                <th class="text-center">C.A ?</th>
                <th class="text-center">&nbsp;</th>
                <th class="text-center">&nbsp;</th>
                <th colspan="3">&nbsp;</th>
                <th class="text-center"><span class="glyphicon glyphicon-check clickable" id="check-all-licencies"></span></th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (sizeof($licencies) > 0) {
                foreach ($licencies as $licencie) {
                    $bg = "";
                    if (!$licencie[LICENCIE_ACTIF]) {
                        $bg = "bg-warning";
                    }

                    $nbActionsClub = $ctrl->getNbActionsClub($licencie[LICENCIE_ID]);
                    $btActionsClubClass = "bt-error";
                    if ($nbActionsClub >= $GLOBALS[CONF][NB_ACTIONS_CLUB_A_FAIRE]) {
                        $btActionsClubClass = "bt-licencie-actions-club bt-ok";
                    } else if ($nbActionsClub >= 1) {
                        $btActionsClubClass = "bt-licencie-actions-club bt-warning";
                    }

                    if ($licencie[LICENCIE_ARBITRE] == 1) {
                        $nbArbitrages = $ctrl->getNbArbitrages($licencie[LICENCIE_ID]);
                        $btArbitragesClass = "bt-error";
                        if ($nbArbitrages > 0) {
                            $btArbitragesClass = "bt-licencie-arbitrages bt-info";
                        }
                    }
                    ?>
                    <tr>
                        <td class="<?php echo $bg; ?>"><?php echo $licencie[LICENCIE_NOM]; ?></td>
                        <td class="<?php echo $bg; ?>"><?php echo $licencie[LICENCIE_PRENOM]; ?></td>
                        <td class="<?php echo $bg; ?>">
                            <?php
                            $categories = $ctrl->getCategories($licencie[LICENCIE_ID]);
                            if (sizeof($categories) > 0) {
                                $i = 0;
                                foreach ($categories as $categorie) {
                                    echo $categorie[CATEGORIE_NOM];

                                    if ($i < (sizeof($categories) - 1)) {
                                        echo "<br>";
                                    }
                                    $i++;
                                }
                            }
                            ?>
                        </td>
                        <td class="<?php echo $bg; ?>">
                            <?php
                            $categoriesCoachees = $ctrl->getCategoriesCoachees($licencie[LICENCIE_ID]);
                            if (sizeof($categoriesCoachees) > 0) {
                                $j = 0;
                                foreach ($categoriesCoachees as $categorieCoachee) {
                                    if ($categorieCoachee[COACH_EN_SOUTIEN]) {
                                        echo "(";
                                    }
                                    echo $categorieCoachee[CATEGORIE_NOM];
                                    if ($categorieCoachee[COACH_EN_SOUTIEN]) {
                                        echo ")";
                                    }

                                    if ($j < (sizeof($categoriesCoachees) - 1)) {
                                        echo "<br>";
                                    }
                                    $j++;
                                }
                            }
                            ?>
                        </td>
                        <td class="text-center <?php echo $bg; ?>"><?php echo ($licencie[LICENCIE_GARDIEN] == 1) ? "Oui" : "Non"; ?></td>
                        <td class="text-center <?php echo $bg; ?>"><?php echo ($licencie[LICENCIE_ARBITRE] == 1) ? "Oui" : "Non"; ?></td>
                        <td class="text-center <?php echo $bg; ?>"><?php echo ($licencie[LICENCIE_BUREAU] == 1) ? "Oui" : "Non"; ?></td>
                        <td class="text-center <?php echo $bg; ?>"><?php echo ($licencie[LICENCIE_CA] == 1) ? "Oui" : "Non"; ?></td>
                        <td class="text-center <?php echo $bg; ?>">
                            <?php
                            if ($licencie[LICENCIE_ARBITRE] == 1) {
                                ?>
                                <button class="btn btn-default bt-table-admin <?php echo $btArbitragesClass; ?>" id-licencie="<?php echo $licencie[LICENCIE_ID]; ?>">
                                    <div class="button__text"><?php echo $nbArbitrages; ?> arbitrage(s)</div>
                                </button>
                                <?php
                            }
                            ?>
                        </td>
                        <td class="text-center <?php echo $bg; ?>">
                            <button class="btn btn-default bt-table-admin <?php echo $btActionsClubClass; ?>" id-licencie="<?php echo $licencie[LICENCIE_ID]; ?>">
                                <div class="button__text"><?php echo $nbActionsClub; ?> action(s) club</div>
                            </button>
                        </td>
                        <td class="text-center <?php echo $bg; ?>">
                            <span class="glyphicon glyphicon-pencil clickable" title="Editer" id-licencie="<?php echo $licencie[LICENCIE_ID]; ?>"></span>
                        </td>
                        <td class="text-center <?php echo $bg; ?>">
                            <span class="glyphicon glyphicon-trash clickable" title="Supprimer" id-licencie="<?php echo $licencie[LICENCIE_ID]; ?>"></span>
                            <div class="loader loader-delete" id-licencie="<?php echo $licencie[LICENCIE_ID]; ?>"></div>
                        </td>
                        <?php
                        if (!$licencie[LICENCIE_ACTIF]) {
                            ?>
                            <td class="text-center <?php echo $bg; ?>">
                                <span class="glyphicon glyphicon-ok icon-actif clickable" title="Activer" id-licencie="<?php echo $licencie[LICENCIE_ID]; ?>"></span>
                                <div class="loader loader-actif" id-licencie="<?php echo $licencie[LICENCIE_ID]; ?>"></div>
                            </td>
                            <?php
                        } else {
                            ?>
                            <td class="text-center <?php echo $bg; ?>">
                                <span class="glyphicon glyphicon-remove icon-actif clickable" title="D&eacute;sactiver" id-licencie="<?php echo $licencie[LICENCIE_ID]; ?>"></span>
                                <div class="loader loader-actif" id-licencie="<?php echo $licencie[LICENCIE_ID]; ?>"></div>
                            </td>
                            <?php
                        }
                        ?>
                        <td class="text-center <?php echo $bg; ?>">
                            <input type="checkbox" class="checkbox-licencie" id-licencie="<?php echo $licencie[LICENCIE_ID]; ?>" checked>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>