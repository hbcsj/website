<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/dao/licencie-dao.php");
require_once("common/php/dao/action-club-dao.php");
require_once("common/php/dao/arbitrage-dao.php");
require_once("common/php/managers/licencie-manager.php");

class TableLicenciesCtrl extends AbstractViewCtrl {

	private $licencieDAO;
	private $actionClubDAO;
	private $arbitrageDAO;

	private $licencieManager;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_coach()) {
			$this->licencieDAO = new LicencieDAO($this->getDatabaseConnection());
			$this->actionClubDAO = new ActionClubDAO($this->getDatabaseConnection());
			$this->arbitrageDAO = new ArbitrageDAO($this->getDatabaseConnection());

			$this->licencieManager = new LicencieManager($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getLicencies() {
		return $this->licencieDAO->getAllByParams(
			trim(strtolower($_GET["nom"])),
			trim(strtolower($_GET["prenom"])),
			trim(strtolower($_GET["email"])),
			$_GET["categorieId"],
			$_GET["gardien"],
			$_GET["arbitre"],
			$_GET["bureau"],
			$_GET["ca"],
			$_GET["actif"],
			trim($_GET["anneeNaissance"]),
			LICENCIE_TABLE_NAME.".".LICENCIE_NOM.", ".LICENCIE_TABLE_NAME.".".LICENCIE_PRENOM
		);
	}

	public function getCategories($licencieId) {
		return $this->licencieManager->getCategoriesLicencie($licencieId);
	}

	public function getCategoriesCoachees($licencieId) {
		return $this->licencieManager->getCategoriesCoacheLicencie($licencieId);
	}

	public function getNbActionsClub($evenementId) {
		return sizeof($this->actionClubDAO->getByLicencieIdAndDateDebutSaison($evenementId, DateUtils::get_sqlDate_debutSaison()));
	}

	public function getNbArbitrages($evenementId) {
		return sizeof($this->arbitrageDAO->getByLicencieIdAndDateDebutSaison($evenementId, DateUtils::get_sqlDate_debutSaison()));
	}
}

?>