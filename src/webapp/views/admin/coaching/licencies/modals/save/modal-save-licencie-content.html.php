<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/coaching/licencies/modals/save/", 
    "modal-save-licencie", 
    "ModalSaveLicencieCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
?>
    <div class="modal-header">
        <div class="modal-title"><?php echo $ctrl->getTitle(); ?></div>
    </div>
    <div class="modal-body">
        <div class="form-horizontal" id="form-save-licencie">
            <div class="form-group">
                <label for="form-save-licencie__input-nom" class="col-xs-4 control-label required">Nom :</label>
                <div class="col-xs-7 form-input" for="form-save-licencie__input-nom">
                    <input type="text" class="form-control" id="form-save-licencie__input-nom" value="<?php echo $ctrl->getNomLicencie(); ?>" required>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-licencie__input-nom"></div>
            </div>
            <div class="form-group">
                <label for="form-save-licencie__input-prenom" class="col-xs-4 control-label required">Pr&eacute;nom :</label>
                <div class="col-xs-7 form-input" for="form-save-licencie__input-prenom">
                    <input type="text" class="form-control" id="form-save-licencie__input-prenom" value="<?php echo $ctrl->getPrenomLicencie(); ?>" required>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-licencie__input-prenom"></div>
            </div>
            <div class="form-group">
                <label for="form-save-licencie__input-date-naissance" class="col-xs-4 control-label">
                    Date de naissance :
                    <div class="label-indication">(jj/mm/aaaa)</div>
                </label>
                <div class="col-xs-7 form-input" for="form-save-licencie__input-date-naissance">
                    <input type="text" class="form-control" id="form-save-licencie__input-date-naissance" value="<?php echo $ctrl->getDateNaissanceLicencie(); ?>" pattern="^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$" pattern-indication="Le format doit &ecirc;tre 'jj/mm/aaaa'">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-licencie__input-date-naissance"></div>
            </div>
            <div class="form-group">
                <label for="form-save-licencie__input-categories" class="col-xs-4 control-label">Cat&eacute;gorie(s) :</label>
                <div class="col-xs-7 form-input" for="form-save-licencie__input-categories">
                    <select class="form-control" id="form-save-licencie__input-categories" multiple>
                        <option value=""></option>
                        <?php echo $ctrl->getOptionsCategoriesLicencie(); ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-licencie__input-categories"></div>
            </div>
            <div class="form-group">
                <label for="form-save-licencie__input-adresse" class="col-xs-4 control-label">Adresse :</label>
                <div class="col-xs-7 form-input" for="form-save-licencie__input-adresse">
                    <input type="text" class="form-control" id="form-save-licencie__input-adresse" value="<?php echo $ctrl->getAdresseLicencie(); ?>" placeholder="Adresse">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-licencie__input-adresse"></div>
            </div>
            <div class="form-group">
                <div class="col-xs-7 col-xs-offset-4 form-input" for="form-save-licencie__input-code-postal">
                    <input type="text" class="form-control" id="form-save-licencie__input-code-postal" value="<?php echo $ctrl->getCodePostalLicencie(); ?>" placeholder="Code postal">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-licencie__input-code-postal"></div>
            </div>
            <div class="form-group">
                <div class="col-xs-7 col-xs-offset-4 form-input" for="form-save-licencie__input-ville">
                    <input type="text" class="form-control" id="form-save-licencie__input-ville" value="<?php echo $ctrl->getVilleLicencie(); ?>" placeholder="Ville">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-licencie__input-ville"></div>
            </div>
            <div class="form-group">
                <label for="form-save-licencie__input-telephone1" class="col-xs-4 control-label">T&eacute;l&eacute;phone(s) :</label>
                <div class="col-xs-7 form-input" for="form-save-licencie__input-telephone1">
                    <input type="text" class="form-control" id="form-save-licencie__input-telephone1" value="<?php echo $ctrl->getTelephone1Licencie(); ?>" placeholder="T&eacute;l&eacute;phone 1">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-licencie__input-telephone1"></div>
            </div>
            <div class="form-group">
                <div class="col-xs-7 col-xs-offset-4 form-input" for="form-save-licencie__input-telephone2">
                    <input type="text" class="form-control" id="form-save-licencie__input-telephone2" value="<?php echo $ctrl->getTelephone2Licencie(); ?>" placeholder="T&eacute;l&eacute;phone 2">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-licencie__input-telephone2"></div>
            </div>
            <div class="form-group">
                <div class="col-xs-7 col-xs-offset-4 form-input" for="form-save-licencie__input-telephone3">
                    <input type="text" class="form-control" id="form-save-licencie__input-telephone3" value="<?php echo $ctrl->getTelephone3Licencie(); ?>" placeholder="T&eacute;l&eacute;phone 3">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-licencie__input-email3"></div>
            </div>
            <div class="form-group">
                <label for="form-save-licencie__input-email1" class="col-xs-4 control-label">Email(s) :</label>
                <div class="col-xs-7 form-input" for="form-save-licencie__input-email1">
                    <input type="text" class="form-control" id="form-save-licencie__input-email1" value="<?php echo $ctrl->getEmail1Licencie(); ?>" placeholder="Email 1" pattern="^[A-Za-z0-9._-]+@[A-Za-z0-9.-]{2,}[.][A-Za-z]{2,3}$" pattern-indication="Le format de l'email est invalide">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-licencie__input-email1"></div>
            </div>
            <div class="form-group">
                <div class="col-xs-7 col-xs-offset-4 form-input" for="form-save-licencie__input-email2">
                    <input type="text" class="form-control" id="form-save-licencie__input-email2" value="<?php echo $ctrl->getEmail2Licencie(); ?>" placeholder="Email 2" pattern="^[A-Za-z0-9._-]+@[A-Za-z0-9.-]{2,}[.][A-Za-z]{2,3}$" pattern-indication="Le format de l'email est invalide">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-licencie__input-email2"></div>
            </div>
            <div class="form-group">
                <div class="col-xs-7 col-xs-offset-4 form-input" for="form-save-licencie__input-email3">
                    <input type="text" class="form-control" id="form-save-licencie__input-email3" value="<?php echo $ctrl->getEmail3Licencie(); ?>" placeholder="Email 3" pattern="^[A-Za-z0-9._-]+@[A-Za-z0-9.-]{2,}[.][A-Za-z]{2,3}$" pattern-indication="Le format de l'email est invalide">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-licencie__input-email3"></div>
            </div>
            <div class="form-group">
                <label for="form-save-licencie__input-gardien" class="col-xs-4 control-label">Gardien ?</label>
                <div class="col-xs-1 form-input" for="form-save-licencie__input-gardien">
                    <input type="checkbox" id="form-save-licencie__input-gardien"<?php echo $ctrl->getGardienChecked(); ?>>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-licencie__input-gardien"></div>
                <label for="form-save-licencie__input-arbitre" class="col-xs-2 control-label">Arbitre ?</label>
                <div class="col-xs-1 form-input" for="form-save-licencie__input-arbitre">
                    <input type="checkbox" id="form-save-licencie__input-arbitre"<?php echo $ctrl->getArbitreChecked(); ?>>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-licencie__input-arbitre"></div>
            </div>
            <div class="form-group">
                <label for="form-save-licencie__input-bureau" class="col-xs-4 control-label">Bureau ?</label>
                <div class="col-xs-1 form-input" for="form-save-licencie__input-bureau">
                    <input type="checkbox" id="form-save-licencie__input-bureau"<?php echo $ctrl->getBureauChecked(); ?>>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-licencie__input-bureau"></div>
                <label for="form-save-licencie__input-ca" class="col-xs-2 control-label">C.A. ?</label>
                <div class="col-xs-1 form-input" for="form-save-licencie__input-ca">
                    <input type="checkbox" id="form-save-licencie__input-ca"<?php echo $ctrl->getCAChecked(); ?>>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-licencie__input-ca"></div>
            </div>
            <div class="form-group">
                <label for="form-save-licencie__input-lateralite" class="col-xs-4 control-label">Lat&eacute;ralit&eacute; :</label>
                <div class="col-xs-7 form-input" for="form-save-licencie__input-lateralite">
                    <select class="form-control" id="form-save-licencie__input-lateralite">
                        <option value=""></option>
                        <?php echo $ctrl->getOptionsLateraliteLicencie(); ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-licencie__input-lateralite"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-required text-right">Les champs marqu&eacute;s d'une * sont obligatoires</div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-8">
                <div class="form-result-message"></div>
            </div>
            <div class="col-xs-4 text-right">
                <button id="modal-save-licencie__bt-sauvegarder" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-ok"></span>
                    </div>
                    <div class="button__text">Sauvegarder</div>
                    <div class="loader"></div>
                </button>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>