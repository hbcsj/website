<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/dao/licencie-dao.php");
require_once("common/php/dao/categorie-dao.php");
require_once("common/php/dao/lateralite-dao.php");
require_once("common/php/managers/licencie-manager.php");

class ModalSaveLicencieCtrl extends AbstractViewCtrl {
	
	private $licencie;

	private $licencieDAO;
	private $categorieDAO;
	private $lateraliteDAO;

	private $licencieManager;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idLicencie" => $_GET["idLicencie"]
		), true);
		
		if (isAdminConnected_coach()) {
			$this->licencieDAO = new LicencieDAO($this->getDatabaseConnection());
			$this->categorieDAO = new CategorieDAO($this->getDatabaseConnection());
			$this->lateraliteDAO = new LateraliteDAO($this->getDatabaseConnection());

			$this->licencieManager = new LicencieManager($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdLicencie();
	}

	private function checkIdLicencie() {
		if (HTTPUtils::paramExists(GET, "idLicencie")) {
			$this->licencie = $this->licencieDAO->getById($_GET["idLicencie"]);
				
			if ($this->licencie == null) {
				$this->sendCheckError(
					HTTP_404, 
					"Le licencie (idLicencie = '".$_GET["idLicencie"]."') n'existe pas", 
					"webapp/views/common/error/404/404.html.php"
				);
			}
		}
	}

	public function getTitle() {
		$title = "Nouveau licenci&eacute;";
		if ($this->licencie != null) {
			$title = $this->licencie[LICENCIE_PRENOM]." ".$this->licencie[LICENCIE_NOM]." - <span class=\"normal\">Edition</span>";
		}
		return $title;
	}

	public function getNomLicencie() {
		if ($this->licencie != null) {
			return $this->licencie[LICENCIE_NOM];
		}
		return "";
	}

	public function getPrenomLicencie() {
		if ($this->licencie != null) {
			return $this->licencie[LICENCIE_PRENOM];
		}
		return "";
	}

	public function getDateNaissanceLicencie() {
		if ($this->licencie != null) {
			return DateUtils::convert_sqlDate_to_slashDate($this->licencie[LICENCIE_DATE_NAISSANCE]);
		}
		return "";
	}

	public function getOptionsCategoriesLicencie() {
		$html = "";

		$categoriesLicencie = $this->licencieManager->getCategoriesLicencie($this->licencie[LICENCIE_ID]);
		$categorieIdsLicencie = array();
		if (sizeof($categoriesLicencie) > 0) {
			foreach ($categoriesLicencie as $categorieLicencie) {
				$categorieIdsLicencie[] = $categorieLicencie[CATEGORIE_ID];
			}
		}

		$categories = $this->categorieDAO->getAll(CATEGORIE_TABLE_NAME.".".CATEGORIE_POSITION_AFFICHAGE);
		if (sizeof($categories) > 0) {
			foreach ($categories as $categorie) {
				$selected = "";
				if (in_array($categorie[CATEGORIE_ID], $categorieIdsLicencie)) {
					$selected = " selected";
				}
				$html .= "<option value=\"".$categorie[CATEGORIE_ID]."\"".$selected.">".$categorie[CATEGORIE_NOM]."</option>";
			}
		}

		return $html;
	}

	public function getAdresseLicencie() {
		if ($this->licencie != null) {
			return $this->licencie[LICENCIE_ADRESSE];
		}
		return "";
	}

	public function getCodePostalLicencie() {
		if ($this->licencie != null) {
			return $this->licencie[LICENCIE_CODE_POSTAL];
		}
		return "";
	}

	public function getVilleLicencie() {
		if ($this->licencie != null) {
			return $this->licencie[LICENCIE_VILLE];
		}
		return "";
	}

	public function getTelephone1Licencie() {
		if ($this->licencie != null) {
			return $this->licencie[LICENCIE_TELEPHONE_1];
		}
		return "";
	}

	public function getTelephone2Licencie() {
		if ($this->licencie != null) {
			return $this->licencie[LICENCIE_TELEPHONE_2];
		}
		return "";
	}

	public function getTelephone3Licencie() {
		if ($this->licencie != null) {
			return $this->licencie[LICENCIE_TELEPHONE_3];
		}
		return "";
	}

	public function getEmail1Licencie() {
		if ($this->licencie != null) {
			return $this->licencie[LICENCIE_EMAIL_1];
		}
		return "";
	}

	public function getEmail2Licencie() {
		if ($this->licencie != null) {
			return $this->licencie[LICENCIE_EMAIL_2];
		}
		return "";
	}

	public function getEmail3Licencie() {
		if ($this->licencie != null) {
			return $this->licencie[LICENCIE_EMAIL_3];
		}
		return "";
	}

	public function getGardienChecked() {
		$checked = "";
        if ($this->licencie != null && $this->licencie[LICENCIE_GARDIEN]) {
            $checked = " checked=\"checked\"";
        }
        return $checked;
	}

	public function getArbitreChecked() {
		$checked = "";
        if ($this->licencie != null && $this->licencie[LICENCIE_ARBITRE]) {
            $checked = " checked=\"checked\"";
        }
        return $checked;
	}

	public function getBureauChecked() {
		$checked = "";
        if ($this->licencie != null && $this->licencie[LICENCIE_BUREAU]) {
            $checked = " checked=\"checked\"";
        }
        return $checked;
	}

	public function getCAChecked() {
		$checked = "";
        if ($this->licencie != null && $this->licencie[LICENCIE_CA]) {
            $checked = " checked=\"checked\"";
        }
        return $checked;
	}

	public function getOptionsLateraliteLicencie() {
		$html = "";

		$lateralites = $this->lateraliteDAO->getAll();
		if (sizeof($lateralites) > 0) {
			foreach ($lateralites as $lateralite) {
				$selected = "";
				if ($this->licencie[LICENCIE_LATERALITE_ID] == $lateralite[LATERALITE_ID]) {
					$selected = " selected";
				}
				$html .= "<option value=\"".$lateralite[LATERALITE_ID]."\"".$selected.">".$lateralite[LATERALITE_LIBELLE]."</option>";
			}
		}

		return $html;
	}
}

?>