<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/dao/licencie-dao.php");
require_once("common/php/dao/evenement-dao.php");
require_once("common/php/dao/arbitrage-dao.php");
require_once("common/php/managers/evenement-manager.php");

class ModalArbitragesLicencieCtrl extends AbstractViewCtrl {
	
	private $licencie;

	private $licencieDAO;
	private $evenementDAO;
	private $arbitrageDAO;

	private $evenementManager;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idLicencie" => $_GET["idLicencie"]
		), true);
		
		if (isAdminConnected_coach()) {
			$this->licencieDAO = new LicencieDAO($this->getDatabaseConnection());
			$this->evenementDAO = new EvenementDAO($this->getDatabaseConnection());
			$this->arbitrageDAO = new ArbitrageDAO($this->getDatabaseConnection());

			$this->evenementManager = new EvenementManager($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$checkParams = $this->checkParams(array(
			array(GET, "idLicencie")
		));
		if ($checkParams) {
			$this->checkIdLicencie();
		}
	}

	private function checkIdLicencie() {
		$this->licencie = $this->licencieDAO->getById($_GET["idLicencie"]);
			
		if ($this->licencie == null) {
			$this->sendCheckError(
				HTTP_404, 
				"Le licencie (idLicencie = '".$_GET["idLicencie"]."') n'existe pas", 
				"webapp/views/common/error/404/404.html.php"
			);
		}
	}

	public function getLicencie() {
		return $this->licencie;
	}

	public function getEvenementArbitrage($evenementId) {
		return $this->evenementDAO->getById($evenementId);
	}

    public function getRecapEvenementProps($evenement) {
        return $this->evenementManager->getRecapEvenementProps($evenement);
	}

	public function getArbitrages() {
		return $this->arbitrageDAO->getByLicencieIdAndDateDebutSaison($this->licencie[LICENCIE_ID], DateUtils::get_sqlDate_debutSaison());
	}
}

?>