<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/coaching/licencies/modals/arbitrages/", 
    "modal-arbitrages-licencie", 
    "ModalArbitragesLicencieCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
    $arbitrages = $ctrl->getArbitrages();
    $licencie = $ctrl->getLicencie();
    ?>
    <div class="modal-header">
        <div class="modal-title">
            <?php echo $licencie[LICENCIE_PRENOM]." ".$licencie[LICENCIE_NOM]; ?> - 
            <span class="normal">Arbitrages</span>
        </div>
    </div>
    <div class="modal-body">
    <table class="table table-striped table-condensed">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Ev&eacute;nement</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (sizeof($arbitrages) > 0) {
                    foreach ($arbitrages as $arbitrage) {
                        $evenement = $ctrl->getEvenementArbitrage($arbitrage[ARBITRAGE_EVENEMENT_ID]);
                        $evenementProps = $ctrl->getRecapEvenementProps($evenement);
                        ?>
                        <tr>
                            <td>
                                <?php
                                echo $evenementProps[EVENEMENT_JOUR]." ".str_replace(SLASH_DATE_TIME_SEPARATOR, "<br>", DateUtils::convert_sqlDateTime_to_slashDateTime($evenement[EVENEMENT_DATE_HEURE]));
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($evenement[EVENEMENT_TYPE_EVENEMENT_ID] == EVENEMENT_MATCH_TYPE_ID) {
                                    echo $evenementProps[MATCH_EQUIPE_DOMICILE]." VS ".$evenementProps[MATCH_EQUIPE_EXTERIEUR];
                                } else {
                                    echo $evenementProps[EVENEMENT_TYPE];
                                    echo "<br>";
                                    echo $evenement[EVENEMENT_NOM];
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="modal-footer"></div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>