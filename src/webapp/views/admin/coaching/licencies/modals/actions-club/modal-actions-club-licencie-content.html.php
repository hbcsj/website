<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/coaching/licencies/modals/actions-club/", 
    "modal-actions-club-licencie", 
    "ModalActionsClubLicencieCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
    $actionsClub = $ctrl->getActionsClub();
    $licencie = $ctrl->getLicencie();
    ?>
    <div class="modal-header">
        <div class="modal-title">
            <?php echo $licencie[LICENCIE_PRENOM]." ".$licencie[LICENCIE_NOM]; ?> - 
            <span class="normal">Actions club</span>
        </div>
    </div>
    <div class="modal-body">
    <table class="table table-striped table-condensed">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Ev&eacute;nement</th>
                    <th>Type</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (sizeof($actionsClub) > 0) {
                    foreach ($actionsClub as $actionClub) {
                        $evenement = $ctrl->getEvenementActionClub($actionClub[ACTION_CLUB_EVENEMENT_ID]);
                        $evenementProps = $ctrl->getRecapEvenementProps($evenement);
                        $type = $ctrl->getTypeActionClub($actionClub[ACTION_CLUB_TYPE_ACTION_CLUB_ID]);
                        ?>
                        <tr>
                            <td>
                                <?php
                                echo $evenementProps[EVENEMENT_JOUR]." ".str_replace(SLASH_DATE_TIME_SEPARATOR, "<br>", DateUtils::convert_sqlDateTime_to_slashDateTime($evenement[EVENEMENT_DATE_HEURE]));
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($evenement[EVENEMENT_TYPE_EVENEMENT_ID] == EVENEMENT_MATCH_TYPE_ID) {
                                    echo $evenementProps[MATCH_EQUIPE_DOMICILE]." VS ".$evenementProps[MATCH_EQUIPE_EXTERIEUR];
                                } else {
                                    echo $evenementProps[EVENEMENT_TYPE];
                                    echo "<br>";
                                    echo $evenement[EVENEMENT_NOM];
                                }
                                ?>
                            </td>
                            <td><?php echo $type[TYPE_ACTION_CLUB_LIBELLE]; ?></td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="modal-footer"></div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>