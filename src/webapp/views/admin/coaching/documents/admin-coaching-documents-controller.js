AdminCoachingDocumentsController = {};

AdminCoachingDocumentsController.load_StatsDocuments = function() {
    if (Utils.exists('#container-stats-documents')) {
        AjaxUtils.loadView(
            'admin/coaching/documents/views/stats/stats-documents',
            '#container-stats-documents',
            null,
            function() {},
            '#loader-page'
        );
    }
};

AdminCoachingDocumentsController.load_RechercheAndTableDocuments = function() {
    if (Utils.exists('#container-recherche-documents')) {
        AjaxUtils.loadView(
            'admin/coaching/documents/views/recherche/recherche-documents',
            '#container-recherche-documents',
            null,
            function() {
                FormRechercheUtils.control_toggleFormRecherche();
                onClick_btRechercher();
                onClick_btReset();
                AdminCoachingDocumentsController.load_TableDocuments();
            },
            '#loader-page'
        );
    }

    function onClick_btRechercher() {
        $('#recherche-documents__bt-rechercher').unbind('click');
        $('#recherche-documents__bt-rechercher').click(function() {
            AdminCoachingDocumentsController.load_TableDocuments();
        });
    }

    function onClick_btReset() {
        $('#recherche-documents__bt-reset').unbind('click');
        $('#recherche-documents__bt-reset').click(function() {
            $('#form-recherche-documents__input-titre').val('');
            $('#form-recherche-documents__input-type').val('');
            $('#form-recherche-documents__input-mots-clefs').val('');

            AdminCoachingDocumentsController.load_TableDocuments();
        });
    }
};

AdminCoachingDocumentsController.load_TableDocuments = function() {
    if (Utils.exists('#container-table-documents')) {
        var params = {
            titre: $.trim($('#form-recherche-documents__input-titre').val()),
            motsClefs: $('#form-recherche-documents__input-mots-clefs').val(),
            typeDocumentCoachingId: $.trim($('#form-recherche-documents__input-type').val())
        };
        AjaxUtils.loadView(
            'admin/coaching/documents/views/table/table-documents',
            '#container-table-documents',
            params,
            function() {
                control_LinkSearchByTag();
                control_AddDocument();
                control_EditDocument();
                control_DeleteDocument();
            },
            '#loader-page'
        );
    }

    function control_LinkSearchByTag() {
        $('a.link-search-by-tag').unbind('click');
        $('a.link-search-by-tag').click(function() {
            var tag = $(this).attr('tag');
            $('#form-recherche-documents__input-mots-clefs').val(tag);
            $('#form-recherche-documents').show();
            $('.container-recherche__footer[for=form-recherche-documents]').show();
            $('.container-recherche__title[for=form-recherche-documents] .container-recherche__title__arrow span.glyphicon').removeClass('glyphicon-triangle-bottom');
            $('.container-recherche__title[for=form-recherche-documents] .container-recherche__title__arrow span.glyphicon').addClass('glyphicon-triangle-top');
            AdminCoachingDocumentsController.load_TableDocuments();
        });
    }

    function control_AddDocument() {
        $('#documents__bt-ajouter').unbind('click');
        $('#documents__bt-ajouter').click(function() {
            open_ModalSaveDocument(null);
        });
    }

    function control_EditDocument() {
        $('.glyphicon-pencil').unbind('click');
        $('.glyphicon-pencil').click(function() {
            var idDocument = $(this).attr('id-document');
            open_ModalSaveDocument(idDocument);
        });
    }

    function open_ModalSaveDocument(idDocument) {
        var params = null;
        if (idDocument != null) {
            params = {
                idDocumentCoaching: idDocument
            };
        }
        AjaxUtils.loadView(
            'admin/coaching/documents/modals/save/modal-save-document-content',
            '#modal-save-document-content',
            params,
            function() {
                generateBadgesMotsClefs();
                control_TypeDocument();
                onClick_btAjouterMotClef();
                onClick_btSauvegarder(idDocument != null);
            },
            '#loader-page'
        );
        $('#modal-save-document').modal('show');

        function generateBadgesMotsClefs() {
            var motsClefs = $('#form-save-document__input-mots-clefs').val().split(',');
            var motsClefsHTML = '<span class=\"badge mot-clef\">'+$('#form-save-document__input-type').find('option:selected').html()+'</span>';
            for (var i in motsClefs) {
                motsClefsHTML += '<span class=\"badge mot-clef clickable\" title=\"Cliquer pour supprimer\">'+motsClefs[i]+'</span>';
            }
            $('#form-save-document__mots-clefs').html(motsClefsHTML);
            control_DeleteMotsClefs();

            function control_DeleteMotsClefs() {
                $('.badge.mot-clef.clickable').unbind('click');
                $('.badge.mot-clef.clickable').click(function() {
                    var motClef = $(this).html();
                    var motsClefsTmp = $.trim($('#form-save-document__input-mots-clefs').val()).split(',');
                    var motsClefs = [];
                    for (var i in motsClefsTmp) {
                        if (motsClefsTmp[i] != motClef) {
                            motsClefs.push(motsClefsTmp[i]);
                        }
                    }
                    $('#form-save-document__input-mots-clefs').val(motsClefs.join(','));
                    generateBadgesMotsClefs();
                });
            }
        }

        function control_TypeDocument() {
            $('#form-save-document__input-type').unbind('change');
            $('#form-save-document__input-type').change(function() {
                generateBadgesMotsClefs();
            });
        }

        function onClick_btAjouterMotClef() {
            $('#modal-save-document__bt-ajouter-mot-clef').unbind('click');
            $('#modal-save-document__bt-ajouter-mot-clef').click(function() {
                ajouterMotClef($.trim($('#form-save-document__input-mot-clef').val()));
                $('#form-save-document__input-mot-clef').val('');
            });

            function ajouterMotClef(motClef) {
                var motsClefs = $.trim($('#form-save-document__input-mots-clefs').val());
                if (motsClefs != '') {
                    motsClefs += ',';
                }
                motsClefs += motClef;
                $('#form-save-document__input-mots-clefs').val(motsClefs);
                generateBadgesMotsClefs();
            }
        }

        function onClick_btSauvegarder(isUpdate) {
            $('#modal-save-document__bt-sauvegarder').unbind('click');
            $('#modal-save-document__bt-sauvegarder').click(function() {
                FormUtils.hideFormErrors('#modal-save-document #form-save-document');
                ViewUtils.unsetHTMLAndHide('#modal-save-document .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('#modal-save-document__bt-sauvegarder', '#modal-save-document__bt-sauvegarder .loader');

                var formValidation = FormUtils.validateForm('#modal-save-document #form-save-document');
                
                if (formValidation) {
                    var inputFichier = $.trim($('#form-save-document__input-fichier').val());
                    var inputUrl = $.trim($('#form-save-document__input-url').val());

                    if (!isUpdate && inputFichier == '' && inputUrl == '') {
                        var errorFichierUrl = {
                            errorType: FormUtils.errorTypes.unavailableValue,
                            message: 'Il faut renseigner le fichier ou l\'URL'
                        };
                        FormUtils.hideErrors('form-save-document__input-fichier');
                        FormUtils.hideErrors('form-save-document__input-url');
                        FormUtils.displayError('form-save-document__input-fichier', errorFichierUrl)
                        FormUtils.displayError('form-save-document__input-url', errorFichierUrl)
                        ViewUtils.activeButtonAndHideLoader('#modal-save-document__bt-sauvegarder', '#modal-save-document__bt-sauvegarder .loader');
                    } else {
                        document.getElementById('form-save-document').submit();
                    }
                } else {
                    ViewUtils.activeButtonAndHideLoader('#modal-save-document__bt-sauvegarder', '#modal-save-document__bt-sauvegarder .loader');
                }
            });
        }
    }

    function control_DeleteDocument() {
        $('.glyphicon-trash').unbind('click');
        $('.glyphicon-trash').click(function() {
            var idDocument = $(this).attr('id-document');
            
            ViewUtils.prompt(
                'Supprimer ce document ?', 
                {
                    btOkText: 'Oui',
                    btKoText: 'Non'
                },
                function() {
                    deleteDocument(idDocument);
                }, 
                null, 
                null
            );
        });

        function deleteDocument(idDocument) {
            $('.loader-delete[id-document='+idDocument+']').show();
            AjaxUtils.callService(
                'document-coaching', 
                'delete', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idDocument
                }, 
                function(response) {
                    $('.loader-delete[id-document='+idDocument+']').hide();
                    if (response.success) {
                        AdminCoachingDocumentsController.load_StatsDocuments();
                        AdminCoachingDocumentsController.load_TableDocuments();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la suppression du document', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-delete[id-document='+idDocument+']').hide();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        }
    }
};

initializePage = function() {
    AdminController.initializePage('coaching', 'documents');

    AdminCoachingDocumentsController.load_StatsDocuments();
    AdminCoachingDocumentsController.load_RechercheAndTableDocuments();
};