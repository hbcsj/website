<?php
require_once("core/php/resources/page.php");
require_once("common/php/lib/admin-utils.php");

$page = new Page(
    "webapp/views/admin/coaching/documents/", 
    "admin-coaching-documents", 
    "AdminCoachingDocumentsCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/admin/common/nav/nav.html.php");
if (isAdminConnected_coach()) {
    require_once("webapp/views/admin/coaching/sub-menu/sub-menu-admin-coaching.html.php");
}
?>

<div class="container-fluid animated fadeIn">
    <?php
    if (isAdminConnected_coach()) {
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h1>
                    Documents de coaching
                    <button id="documents__bt-ajouter" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-plus"></span>
                        </div>
                        <div class="button__text">Ajouter un document</div>
                    </button>
                </h1>
            </div>
        </div>
        <div id="container-stats-documents" class="container-stats"></div>
        <div id="container-recherche-documents" class="container-recherche"></div>
        <div id="container-table-documents" class="container-table"></div>
        <iframe name="uploader-document-coaching" class="iframe-uploader" src="#"></iframe>
        <?php
    } else {
        require_once("webapp/views/common/mdp-form/mdp-form.html.php");
    }
    ?>
</div>

<?php
require_once("webapp/views/common/modals/alert/modal-alert.html.php");
require_once("webapp/views/common/modals/prompt/modal-prompt.html.php");

if (isAdminConnected_coach()) {
    require_once("webapp/views/admin/coaching/documents/modals/save/modal-save-document.html.php");
}

$page->finalizePage();
?>