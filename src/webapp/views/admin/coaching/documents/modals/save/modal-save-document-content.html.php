<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/coaching/documents/modals/save/", 
    "modal-save-document", 
    "ModalSaveDocumentCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
?>
    <div class="modal-header">
        <div class="modal-title"><?php echo $ctrl->getTitle(); ?></div>
    </div>
    <div class="modal-body">
        <form id="form-save-document" method="post" action="<?php echo ROOT_PATH.UPLOADER_PATH; ?>/document-coaching<?php echo $ctrl->getParamIdDocument(); ?>" enctype="multipart/form-data" class="form-horizontal" target="uploader-document-coaching">
            <div class="form-group">
                <label for="form-save-document__input-titre" class="col-xs-4 control-label required">Titre :</label>
                <div class="col-xs-7 form-input" for="form-save-document__input-titre">
                    <input type="text" class="form-control" id="form-save-document__input-titre" name="titre" value="<?php echo $ctrl->getTitreDocument(); ?>" required>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-document__input-titre"></div>
            </div>
            <div class="form-group">
                <label for="form-save-document__input-type" class="col-xs-4 control-label required">Type :</label>
                <div class="col-xs-7 form-input" for="form-save-document__input-type">
                    <select class="form-control" id="form-save-document__input-type" name="type-document-coaching-id" required>
                        <option value=""></option>
                        <?php echo $ctrl->getOptionsTypesDocument(); ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-document__input-type"></div>
            </div>
            <div class="form-group">
                <label for="form-save-document__input-mot-clef" class="col-xs-4 control-label">Mot clef :</label>
                <div class="col-xs-4 form-input" for="form-save-document__input-mot-clef">
                    <input type="text" class="form-control" id="form-save-document__input-mot-clef" value="">
                    <input type="hidden" id="form-save-document__input-mots-clefs" name="mots-clefs" value="<?php echo $ctrl->getMotsClefsDocument(); ?>">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-document__input-mot-clef"></div>
                <div class="col-xs-2 form-input">
                    <button id="modal-save-document__bt-ajouter-mot-clef" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-plus"></span>
                        </div>
                        <div class="button__text">Ajouter</div>
                        <div class="loader"></div>
                    </button>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-7 col-xs-offset-4 form-input" id="form-save-document__mots-clefs"></div>
            </div>
            <div class="form-group">
                <label for="form-save-document__input-fichier" class="col-xs-4 control-label">Fichier :</label>
                <div class="col-xs-7 form-input" for="form-save-document__input-fichier">
                    <input type="file" class="form-control" id="form-save-document__input-fichier" name="document-file">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-document__input-fichier"></div>
            </div>
            <div class="form-group">
                <label for="form-save-document__input-url" class="col-xs-4 control-label">URL :</label>
                <div class="col-xs-7 form-input" for="form-save-document__input-url">
                    <input type="text" class="form-control" id="form-save-document__input-url" name="url" value="<?php echo $ctrl->getUrlDocument(); ?>">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-document__input-url"></div>
            </div>
            <div class="form-group">
                <label for="form-save-document__input-nom-editeur" class="col-xs-4 control-label required">Votre nom :</label>
                <div class="col-xs-7 form-input" for="form-save-document__input-nom-editeur">
                    <input type="text" class="form-control" id="form-save-document__input-nom-editeur" name="nom-dernier-editeur" value="" required>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-document__input-nom-editeur"></div>
            </div> 
            <div class="row">
                <div class="col-xs-12 text-required text-right">Les champs marqu&eacute;s d'une * sont obligatoires</div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-8">
                <div class="form-result-message"></div>
            </div>
            <div class="col-xs-4 text-right">
                <button id="modal-save-document__bt-sauvegarder" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-ok"></span>
                    </div>
                    <div class="button__text">Sauvegarder</div>
                    <div class="loader"></div>
                </button>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>