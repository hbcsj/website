<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/document-coaching-dao.php");
require_once("common/php/dao/type-document-coaching-dao.php");

class ModalSaveDocumentCtrl extends AbstractViewCtrl {
	
	private $documentCoaching;

	private $documentCoachingDAO;
	private $typeDocumentCoachingDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idDocumentCoaching" => $_GET["idDocumentCoaching"]
		), true);
		
		if (isAdminConnected_coach()) {
			$this->documentCoachingDAO = new DocumentCoachingDAO($this->getDatabaseConnection());
			$this->typeDocumentCoachingDAO = new TypeDocumentCoachingDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdDocument();
	}

	private function checkIdDocument() {
		if (HTTPUtils::paramExists(GET, "idDocumentCoaching")) {
			$this->documentCoaching = $this->documentCoachingDAO->getById($_GET["idDocumentCoaching"]);
				
			if ($this->documentCoaching == null) {
				$this->sendCheckError(
					HTTP_404, 
					"Le document coaching (idDocumentCoaching = '".$_GET["idDocumentCoaching"]."') n'existe pas", 
					"webapp/views/common/error/404/404.html.php"
				);
			}
		}
	}

	public function getTitle() {
		$title = "Nouveau document";
		if ($this->documentCoaching != null) {
			$title = $this->documentCoaching[DOCUMENT_COACHING_TITRE]." - <span class=\"normal\">Edition</span>";
		}
		return $title;
	}

	public function getTitreDocument() {
		if ($this->documentCoaching != null) {
			return $this->documentCoaching[DOCUMENT_COACHING_TITRE];
		}
		return "";
	}

	public function getMotsClefsDocument() {
		if ($this->documentCoaching != null) {
			$motsClefs = explode(",", $this->documentCoaching[DOCUMENT_COACHING_MOTS_CLEFS]);
			array_splice($motsClefs, 0, 1);
			return implode(",", $motsClefs);
		}
		return "";
	}

	public function getUrlDocument() {
		if ($this->documentCoaching != null) {
			return $this->documentCoaching[DOCUMENT_COACHING_URL];
		}
		return "";
	}

	public function getOptionsTypesDocument() {
		$html = "";

		$types = $this->typeDocumentCoachingDAO->getAll(TYPE_DOCUMENT_COACHING_TABLE_NAME.".".TYPE_DOCUMENT_COACHING_LIBELLE);
		if (sizeof($types) > 0) {
			foreach ($types as $type) {
				$selected = "";
				if ($this->documentCoaching[DOCUMENT_COACHING_TYPE_DOCUMENT_COACHING_ID] == $type[TYPE_DOCUMENT_COACHING_ID]) {
					$selected = " selected";
				}
				$html .= "<option value=\"".$type[TYPE_DOCUMENT_COACHING_ID]."\"".$selected.">".$type[TYPE_DOCUMENT_COACHING_LIBELLE]."</option>";
			}
		}

		return $html;
	}

	public function getParamIdDocument() {
		if ($this->documentCoaching != null) {
			return "?id=".$this->documentCoaching[DOCUMENT_COACHING_ID];
		}
		return "";
	}
}

?>