<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/coaching/documents/views/recherche/", 
    "recherche-documents", 
    "RechercheDocumentsCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
    $typesDocuments = $ctrl->getTypesDocumentsCoaching();
    ?>
    <div class="container-recherche__title" title="Ouvrir le formulaire de recherche" for="form-recherche-documents">
        <div class="container-recherche__title__icon">
            <span class="glyphicon glyphicon-search"></span>
        </div>
        <div class="container-recherche__title__text">Recherche</div>
        <div class="container-recherche__title__arrow">
            <span class="glyphicon glyphicon-triangle-bottom"></span>
        </div>
    </div>
    <div class="form-horizontal" id="form-recherche-documents">
        <div class="form-group">
            <label for="form-recherche-documents__input-titre" class="col-xs-1 control-label">Titre :</label>
            <div class="col-xs-2 form-input" for="form-recherche-documents__input-titre">
                <input class="form-control" id="form-recherche-documents__input-titre">
            </div>
            <label for="form-recherche-documents__input-type" class="col-xs-1 control-label">Type :</label>
            <div class="col-xs-2 form-input" for="form-recherche-documents__input-type">
                <select class="form-control" id="form-recherche-documents__input-type">
                    <option value=""></option>
                    <?php
                    if (sizeof($typesDocuments) > 0) {
                        foreach ($typesDocuments as $typesDocument) {
                            ?>
                            <option value="<?php echo $typesDocument[TYPE_DOCUMENT_COACHING_ID]; ?>"><?php echo $typesDocument[TYPE_DOCUMENT_COACHING_LIBELLE]; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <label for="form-recherche-documents__input-mots-clefs" class="col-xs-2 control-label">Mots clefs :</label>
            <div class="col-xs-3 form-input" for="form-recherche-documents__input-mots-clefs">
                <input class="form-control" id="form-recherche-documents__input-mots-clefs">
            </div>
        </div>
    </div>
    <div class="container-recherche__footer" for="form-recherche-documents">
        <button id="recherche-documents__bt-rechercher" class="btn btn-default">
            <div class="button__icon">
                <span class="glyphicon glyphicon-search"></span>
            </div>
            <div class="button__text">Chercher</div>
        </button>
        <button id="recherche-documents__bt-reset" class="btn btn-default">
            <div class="button__icon">
                <span class="glyphicon glyphicon-erase"></span>
            </div>
            <div class="button__text">Reset</div>
        </button>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>