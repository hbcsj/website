<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/type-document-coaching-dao.php");

class RechercheDocumentsCtrl extends AbstractViewCtrl {

	private $typeDocumentCoachingDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_coach()) {
			$this->typeDocumentCoachingDAO = new TypeDocumentCoachingDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getTypesDocumentsCoaching() {
		return $this->typeDocumentCoachingDAO->getAll(TYPE_DOCUMENT_COACHING_TABLE_NAME.".".TYPE_DOCUMENT_COACHING_LIBELLE);
	}
}

?>