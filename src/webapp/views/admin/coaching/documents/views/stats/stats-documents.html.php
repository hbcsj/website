<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/coaching/documents/views/stats/", 
    "stats-documents", 
    "StatsDocumentsCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
    ?>
    <div class="container-stats__section">
        <div class="container-stats__section__title">Nombre de documents : </div>
        <div class="container-stats__section__value"><?php echo $ctrl->getNbDocuments(); ?></div>
    </div>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>