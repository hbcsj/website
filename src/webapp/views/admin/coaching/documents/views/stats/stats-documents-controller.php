<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/document-coaching-dao.php");

class StatsDocumentsCtrl extends AbstractViewCtrl {

	private $documentCoachingDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_coach()) {
			$this->documentCoachingDAO = new DocumentCoachingDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getNbDocuments() {
		return $this->documentCoachingDAO->getNbTotal()[DOCUMENT_NB_DOCUMENTS_COACHING];
	}
}

?>