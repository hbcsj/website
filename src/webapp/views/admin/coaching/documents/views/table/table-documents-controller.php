<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/document-coaching-dao.php");
require_once("common/php/dao/type-document-coaching-dao.php");

class TableDocumentsCtrl extends AbstractViewCtrl {

	private $documentCoachingDAO;
	private $typeDocumentCoachingDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_coach()) {
			$this->documentCoachingDAO = new DocumentCoachingDAO($this->getDatabaseConnection());
			$this->typeDocumentCoachingDAO = new TypeDocumentCoachingDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getDocumentsCoaching() {
		$motsClefs = null;
		if (HTTPUtils::paramExists(GET, "motsClefs")) {
			$motsClefs = explode(",", str_replace(";", ",", str_replace(" ", ",", strtolower(StringUtils::removeHtmlSpecialChars(StringUtils::convertAccentsToHtmlSpecialChars(trim($_GET["motsClefs"])))))));
		}
		
		return $this->documentCoachingDAO->getAllByParams(
			trim(strtolower($_GET["titre"])),
			$motsClefs,
			$_GET["typeDocumentCoachingId"],
			DOCUMENT_COACHING_TABLE_NAME.".".DOCUMENT_COACHING_TYPE_DOCUMENT_COACHING_ID.", ".DOCUMENT_COACHING_TABLE_NAME.".".DOCUMENT_COACHING_TITRE
		);
	}

	public function getLibelleTypeDocumentCoaching($typeDocumentCoachingId) {
		$typeDocument = $this->typeDocumentCoachingDAO->getById($typeDocumentCoachingId);
		return $typeDocument[TYPE_DOCUMENT_COACHING_LIBELLE];
	}

	public function getTags($motsClefs) {
		$tags = array();

		if ($motsClefs != null && $motsClefs != "") {
			$motsClefsArray = explode(",", $motsClefs);
			foreach ($motsClefsArray as $motClef) {
				if (trim($motClef) != "") {
					$tags[] = "#".$motClef;
				}
			}
		}

		return $tags;
	}
}

?>