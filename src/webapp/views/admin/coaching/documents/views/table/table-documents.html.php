<?php
require_once("core/php/resources/view.php");
require_once("common/php/constants/config-constants.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/path-utils.php");

$view = new View(
    "webapp/views/admin/coaching/documents/views/table/", 
    "table-documents", 
    "TableDocumentsCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
    $documents = $ctrl->getDocumentsCoaching();
    ?>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Titre</th>
                <th>Type</th>
                <th>Tags</th>
                <th>Document</th>
                <th>Site web</th>
                <th>Dernier &eacute;diteur</th>
                <th>Modifi&eacute; le</th>
                <th colspan="2">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (sizeof($documents) > 0) {
                foreach ($documents as $document) {
                    $type = $ctrl->getLibelleTypeDocumentCoaching($document[DOCUMENT_COACHING_TYPE_DOCUMENT_COACHING_ID]);
                    $tags = $ctrl->getTags($document[DOCUMENT_COACHING_MOTS_CLEFS]);
                    ?>
                    <tr>
                        <td><?php echo $document[DOCUMENT_COACHING_TITRE]; ?></td>
                        <td><?php echo $type; ?></td>
                        <td>
                            <?php
                            if (sizeof($tags) > 0) {
                                foreach ($tags as $tag) {
                                    ?>
                                    <a href="#" class="link-search-by-tag" tag="<?php echo substr($tag, 1); ?>" title="Rechercher avec le tag '<?php echo substr($tag, 1); ?>'"><?php echo $tag; ?></a><br>
                                    <?php
                                }
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($document[DOCUMENT_COACHING_FICHIER] != null && $document[DOCUMENT_COACHING_FICHIER] != "") {
                                ?>
                                <a href="<?php echo SRC_PATH.PathUtils::getDocumentCoachingFile($document[DOCUMENT_COACHING_FICHIER], $document[DOCUMENT_COACHING_ID]); ?>" target="_blank">Voir</a>
                                <?php
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($document[DOCUMENT_COACHING_URL] != null && $document[DOCUMENT_COACHING_URL] != "") {
                                ?>
                                <a href="<?php echo $document[DOCUMENT_COACHING_URL]; ?>" target="_blank">Voir</a>
                                <?php
                            }
                            ?>
                        </td>
                        <td><?php echo $document[DOCUMENT_COACHING_NOM_EDITEUR]; ?></td>
                        <td><?php echo DateUtils::convert_sqlDateTime_to_slashDateTime($document[DOCUMENT_COACHING_DATE_HEURE_MODIFICATION]); ?></td>
                        <td class="text-center">
                            <span class="glyphicon glyphicon-pencil clickable" title="Editer" id-document="<?php echo $document[DOCUMENT_COACHING_ID]; ?>"></span>
                        </td>
                        <td class="text-center">
                            <span class="glyphicon glyphicon-trash clickable" title="Supprimer" id-document="<?php echo $document[DOCUMENT_COACHING_ID]; ?>"></span>
                            <div class="loader loader-delete" id-document="<?php echo $document[DOCUMENT_COACHING_ID]; ?>"></div>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>