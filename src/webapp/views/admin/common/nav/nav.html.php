<?php
require_once("common/php/lib/admin-utils.php");
?>

<nav>
    <div id="nav-logo">
        <img src="<?php echo SRC_PATH; ?>assets/img/logo-admin.png">
    </div>
    <div class="nav-links">
        <?php
        if (isAdminConnected_coach()) {
            ?>
            <a href="<?php echo ROOT_PATH; ?>admin/coaching" page="coaching">Coaching</a>
            <a href="<?php echo ROOT_PATH; ?>admin/evenements" page="evenements">Evenements</a>
            <?php
        }
        if (isAdminConnected_editeur()) {
            ?>
            <a href="<?php echo ROOT_PATH; ?>admin/edition" page="edition">Edition</a>
            <?php
        }
        if (isAdminConnected_commercial()) {
            ?>
            <a href="<?php echo ROOT_PATH; ?>admin/shop" page="shop">Shop</a>
            <?php
        }
        if (isAdminConnected_superadmin()) {
            ?>
            <a href="<?php echo ROOT_PATH; ?>admin/superadmin" page="superadmin">Superadmin</a>
            <?php
        }
        if (isAdminConnected()) {
            ?>
            <a href="#" id="link-logout" title="D&eacute;connexion">
                <span class="glyphicon glyphicon-off"></span>
            </a>
            <?php
        }
        ?>
    </div>
    <div class="loader" id="loader-page"></div>
</nav>
