<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/dao/match-billetterie-dao.php");
require_once("common/php/dao/adversaire-billetterie-dao.php");
require_once("common/php/dao/competition-billetterie-dao.php");

class ModalSaveMatchBilletterieCtrl extends AbstractViewCtrl {
	
	private $matchBilletterie;

	private $matchBilletterieDAO;
	private $adversaireBilletterieDAO;
	private $competitionBilletterieDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idMatchBilletterie" => $_GET["idMatchBilletterie"]
		), true);
		
		if (isAdminConnected_commercial()) {
			$this->matchBilletterieDAO = new MatchBilletterieDAO($this->getDatabaseConnection());
			$this->adversaireBilletterieDAO = new AdversaireBilletterieDAO($this->getDatabaseConnection());
			$this->competitionBilletterieDAO = new CompetitionBilletterieDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdMatch();
	}

	private function checkIdMatch() {
		if (HTTPUtils::paramExists(GET, "idMatchBilletterie")) {
			$this->matchBilletterie = $this->matchBilletterieDAO->getById($_GET["idMatchBilletterie"]);
				
			if ($this->matchBilletterie == null) {
				$this->sendCheckError(
					HTTP_404, 
					"Le match billetterie (idMatchBilletterie = '".$_GET["idMatchBilletterie"]."') n'existe pas", 
					"webapp/views/common/error/404/404.html.php"
				);
			}
		}
	}

	public function getTitle() {
		$title = "Nouveau match";
		if ($this->matchBilletterie != null) {
			$adversaire = $this->adversaireBilletterieDAO->getById($this->matchBilletterie[MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID]);
			$title = "Match contre ".$adversaire[ADVERSAIRE_BILLETTERIE_NOM]." - <span class=\"normal\">Edition</span>";
		}
		return $title;
	}

	public function getDateMatch() {
		if ($this->matchBilletterie != null) {
			return date(SLASH_DATE_FORMAT, DateUtils::get_sqlDateTime_timestamp($this->matchBilletterie[MATCH_BILLETTERIE_DATE_HEURE]));
		}
		return "";
	}

	public function getOptionsHeureMatch() {
		$html = "";

		for ($h = 0 ; $h <= 23 ; $h++) {
			$heure = $h;
			if ($heure < 10) {
				$heure = "0".$heure;
			}

			for ($m = 0 ; $m <= 59 ; $m += 15) {
				$minutes = $m;
				if ($minutes < 10) {
					$minutes = "0".$minutes;
				}

				$selected = "";
				if ($this->matchBilletterie != null && date(SLASH_TIME_FORMAT, DateUtils::get_sqlDateTime_timestamp($this->matchBilletterie[MATCH_BILLETTERIE_DATE_HEURE])) == $heure.SLASH_TIME_SEPARATOR.$minutes) {
					$selected = " selected";
				}
				$html .= "<option value=\"".$heure.SLASH_TIME_SEPARATOR.$minutes."\"".$selected.">".$heure.SLASH_TIME_SEPARATOR.$minutes."</option>";
			}
		}

		return $html;
	}

	public function getOptionsAdversaireMatch() {
		$html = "";

		$adversaires = $this->adversaireBilletterieDAO->getAll(ADVERSAIRE_BILLETTERIE_TABLE_NAME.".".ADVERSAIRE_BILLETTERIE_NOM);
		if (sizeof($adversaires) > 0) {
			foreach ($adversaires as $adversaire) {
				$selected = "";
				if ($this->matchBilletterie[MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID] == $adversaire[ADVERSAIRE_BILLETTERIE_ID]) {
					$selected = " selected";
				}
				$html .= "<option value=\"".$adversaire[ADVERSAIRE_BILLETTERIE_ID]."\"".$selected.">".$adversaire[ADVERSAIRE_BILLETTERIE_NOM]."</option>";
			}
		}

		return $html;
	}

	public function getOptionsCompetitionMatch() {
		$html = "";

		$competitions = $this->competitionBilletterieDAO->getAll();
		if (sizeof($competitions) > 0) {
			foreach ($competitions as $competition) {
				$selected = "";
				if ($this->matchBilletterie[MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID] == $competition[COMPETITION_BILLETTERIE_ID]) {
					$selected = " selected";
				}
				$html .= "<option value=\"".$competition[COMPETITION_BILLETTERIE_ID]."\"".$selected.">".$competition[COMPETITION_BILLETTERIE_NOM]."</option>";
			}
		}

		return $html;
	}

	public function getPrix1PlaceMatch() {
		if ($this->matchBilletterie != null) {
			return $this->matchBilletterie[MATCH_BILLETTERIE_PRIX_1_PLACE];
		}
		return "";
	}

	public function getPrix3PlacesMatch() {
		if ($this->matchBilletterie != null) {
			return $this->matchBilletterie[MATCH_BILLETTERIE_PRIX_3_PLACES];
		}
		return "";
	}

	public function getPrixPublicMatch() {
		if ($this->matchBilletterie != null) {
			return $this->matchBilletterie[MATCH_BILLETTERIE_PRIX_PUBLIC];
		}
		return "";
	}

	public function getEnVenteChecked() {
		$checked = "";
        if ($this->matchBilletterie != null && $this->matchBilletterie[MATCH_BILLETTERIE_EN_VENTE]) {
            $checked = " checked=\"checked\"";
        }
        return $checked;
	}

	public function getDateLimiteCommandeMatch() {
		if ($this->matchBilletterie != null) {
			return DateUtils::convert_sqlDate_to_slashDate($this->matchBilletterie[MATCH_BILLETTERIE_DATE_LIMITE_COMMANDE]);
		}
		return "";
	}
}

?>