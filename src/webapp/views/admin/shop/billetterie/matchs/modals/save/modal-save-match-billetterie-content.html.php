<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/shop/billetterie/matchs/modals/save/", 
    "modal-save-match-billetterie", 
    "ModalSaveMatchBilletterieCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
?>
    <div class="modal-header">
        <div class="modal-title"><?php echo $ctrl->getTitle(); ?></div>
    </div>
    <div class="modal-body">
        <div class="form-horizontal" id="form-save-match-billetterie">
            <div class="form-group">
                <label for="form-save-match-billetterie__input-date" class="col-xs-4 control-label required">Date / Heure :</label>
                <div class="col-xs-3 form-input" for="form-save-match-billetterie__input-date">
                    <div class="input-group">
                        <input type="text" class="form-control" id="form-save-match-billetterie__input-date" value="<?php echo $ctrl->getDateMatch(); ?>" pattern="^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$" pattern-indication="Le format doit &ecirc;tre 'jj/mm/aaaa'" required>
                        <span class="input-group-addon">
                            <img src="<?php echo SRC_PATH; ?>assets/img/icons/calendar.png">
                        </span>
                    </div>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-match-billetterie__input-date"></div>
                <div class="col-xs-3 form-input" for="form-save-match-billetterie__input-heure">
                    <select class="form-control" id="form-save-match-billetterie__input-heure" required>
                        <option value=""></option>
                        <?php echo $ctrl->getOptionsHeureMatch(); ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-match-billetterie__input-heure"></div>
            </div>
            <div class="form-group">
                <label for="form-save-match-billetterie__input-adversaire" class="col-xs-4 control-label required">Adversaire :</label>
                <div class="col-xs-7 form-input" for="form-save-match-billetterie__input-adversaire">
                    <select class="form-control" id="form-save-match-billetterie__input-adversaire" required>
                        <option value=""></option>
                        <?php echo $ctrl->getOptionsAdversaireMatch(); ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-match-billetterie__input-adversaire"></div>
            </div>
            <div class="form-group">
                <label for="form-save-match-billetterie__input-competition" class="col-xs-4 control-label required">Comp&eacute;tition :</label>
                <div class="col-xs-7 form-input" for="form-save-match-billetterie__input-competition">
                    <select class="form-control" id="form-save-match-billetterie__input-competition" required>
                        <option value=""></option>
                        <?php echo $ctrl->getOptionsCompetitionMatch(); ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-match-billetterie__input-competition"></div>
            </div>
            <div class="form-group">
                <label for="form-save-match-billetterie__input-prix-1-place" class="col-xs-4 control-label required">Prix :</label>
                <div class="col-xs-3 form-input" for="form-save-match-billetterie__input-prix-1-place">
                    <div class="input-group">
                        <input type="text" class="form-control" id="form-save-match-billetterie__input-prix-1-place" placeholder="1 place" value="<?php echo $ctrl->getPrix1PlaceMatch(); ?>" pattern="^[0-9]+(\.)?[0-9]*$" pattern-indication="Le prix doit &ecirc;tre un nombre d&eacute;cimal" required>
                        <span class="input-group-addon">&euro;</span>
                    </div>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-match-billetterie__input-prix-1-place"></div>
                <div class="col-xs-3 form-input" for="form-save-match-billetterie__input-prix-3-places">
                    <div class="input-group">
                        <input type="text" class="form-control" id="form-save-match-billetterie__input-prix-3-places" placeholder="3 places" value="<?php echo $ctrl->getPrix3PlacesMatch(); ?>" pattern="^[0-9]+(\.)?[0-9]*$" pattern-indication="Le prix doit &ecirc;tre un nombre d&eacute;cimal" required>
                        <span class="input-group-addon">&euro;</span>
                    </div>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-match-billetterie__input-prix-3-places"></div>
            </div>
            <div class="form-group">
                <label for="form-save-match-billetterie__input-prix-public" class="col-xs-4 control-label required">Prix public :</label>
                <div class="col-xs-3 form-input" for="form-save-match-billetterie__input-prix-public">
                    <div class="input-group">
                        <input type="text" class="form-control" id="form-save-match-billetterie__input-prix-public" value="<?php echo $ctrl->getPrixPublicMatch(); ?>" pattern="^[0-9]+(\.)?[0-9]*$" pattern-indication="Le prix doit &ecirc;tre un nombre d&eacute;cimal" required>
                        <span class="input-group-addon">&euro;</span>
                    </div>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-match-billetterie__input-prix-public"></div>
            </div>
            <div class="form-group">
                <label for="form-save-match-billetterie__input-en-vente" class="col-xs-4 control-label">En vente ?</label>
                <div class="col-xs-7 form-input" for="form-save-match-billetterie__input-en-vente">
                    <input type="checkbox" id="form-save-match-billetterie__input-en-vente"<?php echo $ctrl->getEnVenteChecked(); ?>>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-match-billetterie__input-en-vente"></div>
            </div>
            <div class="form-group">
                <label for="form-save-match-billetterie__input-date-limite-commande" class="col-xs-4 control-label required">Limite comm. :</label>
                <div class="col-xs-3 form-input" for="form-save-match-billetterie__input-date-limite-commande">
                    <div class="input-group">
                        <input type="text" class="form-control" id="form-save-match-billetterie__input-date-limite-commande" value="<?php echo $ctrl->getDateLimiteCommandeMatch(); ?>" pattern="^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$" pattern-indication="Le format doit &ecirc;tre 'jj/mm/aaaa'" required>
                        <span class="input-group-addon">
                            <img src="<?php echo SRC_PATH; ?>assets/img/icons/calendar.png">
                        </span>
                    </div>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-match-billetterie__input-date-limite-commande"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-required text-right">Les champs marqu&eacute;s d'une * sont obligatoires</div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-8">
                <div class="form-result-message"></div>
            </div>
            <div class="col-xs-4 text-right">
                <button id="modal-save-match-billetterie__bt-sauvegarder" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-ok"></span>
                    </div>
                    <div class="button__text">Sauvegarder</div>
                    <div class="loader"></div>
                </button>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>