<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/match-billetterie-dao.php");
require_once("common/php/dao/adversaire-billetterie-dao.php");
require_once("common/php/dao/competition-billetterie-dao.php");
require_once("common/php/dao/commande-item-billetterie-dao.php");
require_once("common/php/lib/date-utils.php");

class TableMatchsCtrl extends AbstractViewCtrl {

	private $matchBilletterieDAO;
	private $adversaireBilletterieDAO;
	private $competitionBilletterieDAO;
	private $commandeItemBilletterieDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_commercial()) {
			$this->matchBilletterieDAO = new MatchBilletterieDAO($this->getDatabaseConnection());
			$this->adversaireBilletterieDAO = new AdversaireBilletterieDAO($this->getDatabaseConnection());
			$this->competitionBilletterieDAO = new CompetitionBilletterieDAO($this->getDatabaseConnection());
			$this->commandeItemBilletterieDAO = new CommandeItemBilletterieDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getMatchs() {
		return $this->matchBilletterieDAO->getAllByParams(
			DateUtils::convert_slashDate_to_sqlDate(trim(strtolower($_GET["dateDebut"]))),
			DateUtils::convert_slashDate_to_sqlDate(trim(strtolower($_GET["dateFin"]))),
			$_GET["adversaireId"],
			$_GET["competitionId"],
			$_GET["enVente"],
			MATCH_BILLETTERIE_TABLE_NAME.".".MATCH_BILLETTERIE_DATE_HEURE." DESC"
		);
	}

	public function getNomAdversaire($adversaireId) {
		$adversaire = $this->adversaireBilletterieDAO->getById($adversaireId);
		return $adversaire[ADVERSAIRE_BILLETTERIE_NOM];
	}

	public function getNomCompetition($competitionId) {
		$competition = $this->competitionBilletterieDAO->getById($competitionId);
		return $competition[COMPETITION_BILLETTERIE_NOM];
	}

	public function hasCommandesAssociees($matchBilletterieId) {
		return (sizeof($this->commandeItemBilletterieDAO->getByMatchBilletterieId($matchBilletterieId)) > 0);
	}
}

?>