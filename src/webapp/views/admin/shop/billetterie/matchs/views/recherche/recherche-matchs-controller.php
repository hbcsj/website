<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/adversaire-billetterie-dao.php");
require_once("common/php/dao/competition-billetterie-dao.php");

class RechercheMatchsCtrl extends AbstractViewCtrl {

	private $adversaireBilletterieDAO;
	private $competitionBilletterieDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_commercial()) {
			$this->adversaireBilletterieDAO = new AdversaireBilletterieDAO($this->getDatabaseConnection());
			$this->competitionBilletterieDAO = new CompetitionBilletterieDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getAdversaires() {
		return $this->adversaireBilletterieDAO->getAll(ADVERSAIRE_BILLETTERIE_TABLE_NAME.".".ADVERSAIRE_BILLETTERIE_NOM);
	}

	public function getCompetitions() {
		return $this->competitionBilletterieDAO->getAll();
	}
}

?>