<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/shop/billetterie/matchs/views/table/", 
    "table-matchs", 
    "TableMatchsCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
    $matchs = $ctrl->getMatchs();
    ?>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Date</th>
                <th>Adversaire</th>
                <th>Comp&eacute;tition</th>
                <th>Prix</th>
                <th>Prix public</th>
                <th class="text-center">En vente ?</th>
                <th>Date limite commande</th>
                <th colspan="2">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (sizeof($matchs) > 0) {
                foreach ($matchs as $match) {
                    $isEnVente = $match[MATCH_BILLETTERIE_EN_VENTE] == 1 && time() <= DateUtils::get_sqlDateTime_timestamp($match[MATCH_BILLETTERIE_DATE_HEURE]);
                    $bg = (!$isEnVente) ? "bg-error" : "";
                    ?>
                    <tr>
                        <td class="<?php echo $bg; ?>">
                            <?php 
                            echo DateUtils::getNomJour(
                                date("w", DateUtils::get_sqlDateTime_timestamp($match[MATCH_BILLETTERIE_DATE_HEURE]))
                            )."<br>".str_replace(SLASH_DATE_TIME_SEPARATOR, "<br>", DateUtils::convert_sqlDateTime_to_slashDateTime($match[MATCH_BILLETTERIE_DATE_HEURE]));
                            ?>
                        </td>
                        <td class="<?php echo $bg; ?>"><?php echo $ctrl->getNomAdversaire($match[MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID]); ?></td>
                        <td class="<?php echo $bg; ?>"><?php echo $ctrl->getNomCompetition($match[MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID]); ?></td>
                        <td class="<?php echo $bg; ?>">
                            <?php 
                            echo "1 place : ".$match[MATCH_BILLETTERIE_PRIX_1_PLACE]." &euro;<br>";
                            echo "3 places : ".$match[MATCH_BILLETTERIE_PRIX_3_PLACES]." &euro;";
                            ?>
                        </td>
                        <td class="<?php echo $bg; ?>"><?php echo $match[MATCH_BILLETTERIE_PRIX_PUBLIC]." &euro;"; ?></td>
                        <td class="text-center <?php echo $bg; ?>"><?php echo ($isEnVente) ? "Oui" : "Non"; ?></td>
                        <td class="<?php echo $bg; ?>">
                            <?php 
                            echo DateUtils::getNomJour(
                                date("w", DateUtils::get_sqlDate_timestamp($match[MATCH_BILLETTERIE_DATE_LIMITE_COMMANDE]))
                            )."<br>".DateUtils::convert_sqlDate_to_slashDate($match[MATCH_BILLETTERIE_DATE_LIMITE_COMMANDE]);
                            ?>
                        </td>
                        <td class="text-center <?php echo $bg; ?>">
                            <span class="glyphicon glyphicon-pencil clickable" title="Editer" id-match="<?php echo $match[MATCH_BILLETTERIE_ID]; ?>"></span>
                        </td>
                        <td class="text-center <?php echo $bg; ?>">
                            <?php
                            if (!$ctrl->hasCommandesAssociees($match[MATCH_BILLETTERIE_ID])) {
                                ?>
                                <span class="glyphicon glyphicon-trash clickable" title="Supprimer" id-match="<?php echo $match[MATCH_BILLETTERIE_ID]; ?>"></span>
                                <div class="loader loader-delete" id-match="<?php echo $match[MATCH_BILLETTERIE_ID]; ?>"></div>
                                <?php
                            } else {
                                echo "&nbsp;";
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>