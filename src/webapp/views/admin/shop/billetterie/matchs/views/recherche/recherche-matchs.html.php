<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/shop/billetterie/matchs/views/recherche/", 
    "recherche-matchs", 
    "RechercheMatchsCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
    $adversaires = $ctrl->getAdversaires();
    $competitions = $ctrl->getCompetitions();
    ?>
    <div class="container-recherche__title" title="Ouvrir le formulaire de recherche" for="form-recherche-matchs">
        <div class="container-recherche__title__icon">
            <span class="glyphicon glyphicon-search"></span>
        </div>
        <div class="container-recherche__title__text">Recherche</div>
        <div class="container-recherche__title__arrow">
            <span class="glyphicon glyphicon-triangle-bottom"></span>
        </div>
    </div>
    <div class="form-horizontal" id="form-recherche-matchs">
        <div class="form-group">
            <label for="form-recherche-matchs__input-date-debut" class="col-xs-1 col-xs-offset-1 control-label">Date :</label>
            <div class="col-xs-2 form-input" for="form-recherche-matchs__input-date-debut">
                <div class="input-group">
                    <input type="text" class="form-control" id="form-recherche-matchs__input-date-debut">
                    <span class="input-group-addon">
                        <img src="<?php echo SRC_PATH; ?>assets/img/icons/calendar.png">
                    </span>
                </div>
            </div>
            <label for="form-recherche-matchs__input-adversaire" class="col-xs-2 control-label">Adversaire :</label>
            <div class="col-xs-2 form-input" for="form-recherche-matchs__input-adversaire">
                <select class="form-control" id="form-recherche-matchs__input-adversaire">
                    <option value=""></option>
                    <?php
                    if (sizeof($adversaires) > 0) {
                        foreach ($adversaires as $adversaire) {
                            ?>
                            <option value="<?php echo $adversaire[ADVERSAIRE_BILLETTERIE_ID]; ?>"><?php echo $adversaire[ADVERSAIRE_BILLETTERIE_NOM]; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <label for="form-recherche-matchs__input-is-en-vente" class="col-xs-2 control-label">En vente ?</label>
            <div class="col-xs-1 form-input" for="form-recherche-matchs__input-is-en-vente">
                <select class="form-control" id="form-recherche-matchs__input-is-en-vente">
                    <option value=""></option>
                    <option value="1">Oui</option>
                    <option value="0">Non</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-2 col-xs-offset-2 form-input" for="form-recherche-matchs__input-date-fin">
                <div class="input-group">
                    <input type="text" class="form-control" id="form-recherche-matchs__input-date-fin">
                    <span class="input-group-addon">
                        <img src="<?php echo SRC_PATH; ?>assets/img/icons/calendar.png">
                    </span>
                </div>
            </div>
            <label for="form-recherche-matchs__input-competition" class="col-xs-2 control-label">Comp&eacute;tition :</label>
            <div class="col-xs-2 form-input" for="form-recherche-matchs__input-competition">
                <select class="form-control" id="form-recherche-matchs__input-competition">
                    <option value=""></option>
                    <?php
                    if (sizeof($competitions) > 0) {
                        foreach ($competitions as $competition) {
                            ?>
                            <option value="<?php echo $competition[COMPETITION_BILLETTERIE_ID]; ?>"><?php echo $competition[COMPETITION_BILLETTERIE_NOM]; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
    <div class="container-recherche__footer" for="form-recherche-matchs">
        <button id="recherche-matchs__bt-rechercher" class="btn btn-default">
            <div class="button__icon">
                <span class="glyphicon glyphicon-search"></span>
            </div>
            <div class="button__text">Chercher</div>
        </button>
        <button id="recherche-matchs__bt-reset" class="btn btn-default">
            <div class="button__icon">
                <span class="glyphicon glyphicon-erase"></span>
            </div>
            <div class="button__text">Reset</div>
        </button>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>