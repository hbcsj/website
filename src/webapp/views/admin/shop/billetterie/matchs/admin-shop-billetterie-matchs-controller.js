AdminShopBilletterieMatchsController = {};

AdminShopBilletterieMatchsController.today = null;

AdminShopBilletterieMatchsController.load_RechercheAndTableMatchs = function() {
    if (Utils.exists('#container-recherche-matchs')) {
        AjaxUtils.loadView(
            'admin/shop/billetterie/matchs/views/recherche/recherche-matchs',
            '#container-recherche-matchs',
            null,
            function() {
                $('#form-recherche-matchs__input-date-debut').val(AdminShopBilletterieMatchsController.today);

                FormRechercheUtils.control_toggleFormRecherche();
                onClick_btRechercher();
                onClick_btReset();
                AdminShopBilletterieMatchsController.load_TableMatchs();
            },
            '#loader-page'
        );
    }

    function onClick_btRechercher() {
        $('#recherche-matchs__bt-rechercher').unbind('click');
        $('#recherche-matchs__bt-rechercher').click(function() {
            AdminShopBilletterieMatchsController.load_TableMatchs();
        });
    }

    function onClick_btReset() {
        $('#recherche-matchs__bt-reset').unbind('click');
        $('#recherche-matchs__bt-reset').click(function() {
            $('#form-recherche-matchs__input-date-debut').val(AdminShopBilletterieMatchsController.today);
            $('#form-recherche-matchs__input-date-fin').val('');
            $('#form-recherche-matchs__input-adversaire').val('');
            $('#form-recherche-matchs__input-competition').val('');
            $('#form-recherche-matchs__input-is-en-vente').val('');

            AdminShopBilletterieMatchsController.load_TableMatchs();
        });
    }
};

AdminShopBilletterieMatchsController.load_TableMatchs = function() {
    if (Utils.exists('#container-table-matchs')) {
        var params = {
            dateDebut: $.trim($('#form-recherche-matchs__input-date-debut').val()),
            dateFin: $.trim($('#form-recherche-matchs__input-date-fin').val()),
            adversaireId: $('#form-recherche-matchs__input-adversaire').val(),
            competitionId: $('#form-recherche-matchs__input-competition').val(),
            enVente: $('#form-recherche-matchs__input-is-en-vente').val()
        };
        AjaxUtils.loadView(
            'admin/shop/billetterie/matchs/views/table/table-matchs',
            '#container-table-matchs',
            params,
            function() {
                control_AddMatch();
                control_EditMatch();
                control_DeleteMatch();
            },
            '#loader-page'
        );
    }

    function control_AddMatch() {
        $('#matchs__bt-ajouter').unbind('click');
        $('#matchs__bt-ajouter').click(function() {
            open_ModalSaveMatch(null);
        });
    }

    function control_EditMatch() {
        $('.glyphicon-pencil').unbind('click');
        $('.glyphicon-pencil').click(function() {
            var idMatch = $(this).attr('id-match');
            open_ModalSaveMatch(idMatch);
        });
    }

    function open_ModalSaveMatch(idMatch) {
        var params = null;
        if (idMatch != null) {
            params = {
                idMatchBilletterie: idMatch
            };
        }
        AjaxUtils.loadView(
            'admin/shop/billetterie/matchs/modals/save/modal-save-match-billetterie-content',
            '#modal-save-match-billetterie-content',
            params,
            function() {
                onClick_btSauvegarder(idMatch);
            },
            '#loader-page'
        );
        $('#modal-save-match-billetterie').modal('show');

        function onClick_btSauvegarder(idMatch) {
            $('#modal-save-match-billetterie__bt-sauvegarder').unbind('click');
            $('#modal-save-match-billetterie__bt-sauvegarder').click(function() {
                FormUtils.hideFormErrors('#modal-save-match-billetterie #form-save-match-billetterie');
                ViewUtils.unsetHTMLAndHide('#modal-save-match-billetterie .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('#modal-save-match-billetterie__bt-sauvegarder', '#modal-save-match-billetterie__bt-sauvegarder .loader');

                var formValidation = FormUtils.validateForm('#modal-save-match-billetterie #form-save-match-billetterie');
                
                if (formValidation) {
                    var inputDate = $.trim($('#form-save-match-billetterie__input-date').val());
                    var inputHeure = $('#form-save-match-billetterie__input-heure').val();
                    var inputAdversaire = $.trim($('#form-save-match-billetterie__input-adversaire').val());
                    var inputCompetition = $('#form-save-match-billetterie__input-competition').val();
                    var inputPrix1Place = $.trim($('#form-save-match-billetterie__input-prix-1-place').val());
                    var inputPrix3Places = $.trim($('#form-save-match-billetterie__input-prix-3-places').val());
                    var inputPrixPublic = $.trim($('#form-save-match-billetterie__input-prix-public').val());
                    var inputEnVente = ($('#form-save-match-billetterie__input-en-vente').prop('checked')) ? '1' : '0';
                    var inputDateLimiteCommande = $.trim($('#form-save-match-billetterie__input-date-limite-commande').val());

                    saveMatch(idMatch, inputDate, inputHeure, inputAdversaire, inputCompetition, inputPrix1Place, inputPrix3Places, inputPrixPublic, inputEnVente, inputDateLimiteCommande);
                } else {
                    ViewUtils.activeButtonAndHideLoader('#modal-save-match-billetterie__bt-sauvegarder', '#modal-save-match-billetterie__bt-sauvegarder .loader');
                }
            });

            function saveMatch(idMatch, inputDate, inputHeure, inputAdversaire, inputCompetition, inputPrix1Place, inputPrix3Places, inputPrixPublic, inputEnVente, inputDateLimiteCommande) {
                AjaxUtils.callService(
                    'match-billetterie', 
                    'save', 
                    AjaxUtils.dataTypes.json, 
                    AjaxUtils.requestTypes.post, 
                    {
                        id: idMatch
                    }, 
                    {
                        date: inputDate,
                        heure: inputHeure, 
                        prix1Place: inputPrix1Place,
                        prix3Places: inputPrix3Places,
                        prixPublic: inputPrixPublic,
                        enVente: inputEnVente,
                        dateLimiteCommande: inputDateLimiteCommande,
                        adversaireId: inputAdversaire,
                        competitionId: inputCompetition
                    }, 
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-save-match-billetterie .form-result-message', 'Match sauvegard&eacute;', FormUtils.resultsTypes.ok);
                            $('#modal-save-match-billetterie__bt-sauvegarder .loader').hide();
                            AdminShopBilletterieMatchsController.load_TableMatchs();
                        } else {
                            ViewUtils.activeButtonAndHideLoader('#modal-save-match-billetterie__bt-sauvegarder', '#modal-save-match-billetterie__bt-sauvegarder .loader');
                            FormUtils.displayFormResultMessage('#modal-save-match-billetterie .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('#modal-save-match-billetterie__bt-sauvegarder', '#modal-save-match-billetterie__bt-sauvegarder .loader');
                        FormUtils.displayFormResultMessage('#modal-save-match-billetterie .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            }
        }
    }

    function control_DeleteMatch() {
        $('.glyphicon-trash').unbind('click');
        $('.glyphicon-trash').click(function() {
            var idMatch = $(this).attr('id-match');
            
            ViewUtils.prompt(
                'Supprimer ce match ?', 
                {
                    btOkText: 'Oui',
                    btKoText: 'Non'
                },
                function() {
                    deleteMatch(idMatch);
                }, 
                null, 
                null
            );
        });

        function deleteMatch(idMatch) {
            $('.loader-delete[id-match='+idMatch+']').show();
            AjaxUtils.callService(
                'match-billetterie', 
                'delete', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idMatch
                }, 
                function(response) {
                    $('.loader-delete[id-match='+idMatch+']').hide();
                    if (response.success) {
                        AdminShopBilletterieMatchsController.load_TableMatchs();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la suppression du match', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-delete[id-match='+idMatch+']').hide();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        }
    }
};

initializePage = function() {
    AdminController.initializePage('shop', 'billetterie');

    AjaxUtils.callService(
        'date', 
        'getSlashDate', 
        AjaxUtils.dataTypes.json, 
        AjaxUtils.requestTypes.get, 
        null, 
        null, 
        function(response) {
            AdminShopBilletterieMatchsController.today = response.date;

            AdminShopBilletterieMatchsController.load_RechercheAndTableMatchs();
        },
        function(response) {}
    );
};