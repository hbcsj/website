<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/shop/billetterie/packs/views/table/", 
    "table-packs", 
    "TablePacksCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
    $packs = $ctrl->getPacks();
    ?>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Nom</th>
                <th class="text-center">Nb. places</th>
                <th class="text-center">Reste</th>
                <th>Prix</th>
                <th colspan="2">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (sizeof($packs) > 0) {
                foreach ($packs as $pack) {
                    ?>
                    <tr>
                        <td><?php echo $pack[PACK_BILLETTERIE_NOM]; ?></td>
                        <td class="text-center"><?php echo $pack[PACK_BILLETTERIE_NB_PLACES]; ?></td>
                        <td class="text-center"><?php echo $ctrl->getRestePlacesPack($pack); ?></td>
                        <td><?php echo $pack[PACK_BILLETTERIE_PRIX]; ?> &euro;</td>
                        <td class="text-center">
                            <span class="glyphicon glyphicon-pencil clickable" title="Editer" id-pack="<?php echo $pack[PACK_BILLETTERIE_ID]; ?>"></span>
                        </td>
                        <td class="text-center">
                            <?php
                            if (!$ctrl->hasCommandesAssociees($pack[PACK_BILLETTERIE_ID])) {
                                ?>
                                <span class="glyphicon glyphicon-trash clickable" title="Supprimer" id-pack="<?php echo $pack[PACK_BILLETTERIE_ID]; ?>"></span>
                                <div class="loader loader-delete" id-pack="<?php echo $pack[PACK_BILLETTERIE_ID]; ?>"></div>
                                <?php
                            } else {
                                echo "&nbsp;";
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>