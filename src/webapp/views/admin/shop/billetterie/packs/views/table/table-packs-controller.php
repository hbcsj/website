<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/pack-billetterie-dao.php");
require_once("common/php/dao/commande-item-billetterie-dao.php");
require_once("common/php/lib/date-utils.php");

class TablePacksCtrl extends AbstractViewCtrl {

	private $packBilletterieDAO;
	private $commandeItemBilletterieDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_commercial()) {
			$this->packBilletterieDAO = new PackBilletterieDAO($this->getDatabaseConnection());
			$this->commandeItemBilletterieDAO = new CommandeItemBilletterieDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getPacks() {
		return $this->packBilletterieDAO->getAll(PACK_BILLETTERIE_TABLE_NAME.".".PACK_BILLETTERIE_ID." DESC");
	}

	public function getRestePlacesPack($packBilletterie) {
		return $packBilletterie[PACK_BILLETTERIE_NB_PLACES] - 
			$this->commandeItemBilletterieDAO->getNbTotalPlacesByPackId($packBilletterie[PACK_BILLETTERIE_ID])[COMMANDE_ITEM_BILLETTERIE_NB_PLACES];
	}

	public function hasCommandesAssociees($packBilletterieId) {
		return (sizeof($this->commandeItemBilletterieDAO->getByPackBilletterieId($packBilletterieId)) > 0);
	}
}

?>