<?php
require_once("core/php/resources/page.php");
require_once("common/php/lib/admin-utils.php");

$page = new Page(
    "webapp/views/admin/shop/billetterie/packs/", 
    "admin-shop-billetterie-packs", 
    "AdminShopBilletteriePacksCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/admin/common/nav/nav.html.php");
if (isAdminConnected_commercial()) {
    require_once("webapp/views/admin/shop/sub-menu/sub-menu-admin-shop.html.php");
}
?>

<div class="container-fluid animated fadeIn">
    <?php
    if (isAdminConnected_commercial()) {
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <?php require_once("webapp/views/admin/shop/billetterie/header/header-admin-shop-billetterie.html.php"); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h2>
                    Packs
                    <button id="packs__bt-ajouter" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-plus"></span>
                        </div>
                        <div class="button__text">Ajouter un pack</div>
                    </button>
                </h2>
            </div>
        </div>
        <div id="container-table-packs" class="container-table"></div>
        <?php
    } else {
        require_once("webapp/views/common/mdp-form/mdp-form.html.php");
    }
    ?>
</div>

<?php
require_once("webapp/views/common/modals/alert/modal-alert.html.php");
require_once("webapp/views/common/modals/prompt/modal-prompt.html.php");

if (isAdminConnected_commercial()) {
    require_once("webapp/views/admin/shop/billetterie/packs/modals/save/modal-save-pack-billetterie.html.php");
}

$page->finalizePage();
?>