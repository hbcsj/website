AdminShopBilletteriePacksController = {};

AdminShopBilletteriePacksController.load_TablePacks = function() {
    if (Utils.exists('#container-table-packs')) {
        AjaxUtils.loadView(
            'admin/shop/billetterie/packs/views/table/table-packs',
            '#container-table-packs',
            null,
            function() {
                control_AddPack();
                control_EditPack();
                control_DeletePack();
            },
            '#loader-page'
        );
    }

    function control_AddPack() {
        $('#packs__bt-ajouter').unbind('click');
        $('#packs__bt-ajouter').click(function() {
            open_ModalSavePack(null);
        });
    }

    function control_EditPack() {
        $('.glyphicon-pencil').unbind('click');
        $('.glyphicon-pencil').click(function() {
            var idPack = $(this).attr('id-pack');
            open_ModalSavePack(idPack);
        });
    }

    function open_ModalSavePack(idPack) {
        var params = null;
        if (idPack != null) {
            params = {
                idPackBilletterie: idPack
            };
        }
        AjaxUtils.loadView(
            'admin/shop/billetterie/packs/modals/save/modal-save-pack-billetterie-content',
            '#modal-save-pack-billetterie-content',
            params,
            function() {
                onClick_btSauvegarder(idPack);
            },
            '#loader-page'
        );
        $('#modal-save-pack-billetterie').modal('show');

        function onClick_btSauvegarder(idPack) {
            $('#modal-save-pack-billetterie__bt-sauvegarder').unbind('click');
            $('#modal-save-pack-billetterie__bt-sauvegarder').click(function() {
                FormUtils.hideFormErrors('#modal-save-pack-billetterie #form-save-pack-billetterie');
                ViewUtils.unsetHTMLAndHide('#modal-save-pack-billetterie .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('#modal-save-pack-billetterie__bt-sauvegarder', '#modal-save-pack-billetterie__bt-sauvegarder .loader');

                var formValidation = FormUtils.validateForm('#modal-save-pack-billetterie #form-save-pack-billetterie');
                
                if (formValidation) {
                    var inputNom = $.trim($('#form-save-pack-billetterie__input-nom').val());
                    var inputNbPlaces = $.trim($('#form-save-pack-billetterie__input-nb-places').val());
                    var inputPrix = $.trim($('#form-save-pack-billetterie__input-prix').val());

                    savePack(idPack, inputNom, inputNbPlaces, inputPrix);
                } else {
                    ViewUtils.activeButtonAndHideLoader('#modal-save-pack-billetterie__bt-sauvegarder', '#modal-save-pack-billetterie__bt-sauvegarder .loader');
                }
            });

            function savePack(idPack, inputNom, inputNbPlaces, inputPrix) {
                AjaxUtils.callService(
                    'pack-billetterie', 
                    'save', 
                    AjaxUtils.dataTypes.json, 
                    AjaxUtils.requestTypes.post, 
                    {
                        id: idPack
                    }, 
                    {
                        nom: inputNom,
                        nbPlaces: inputNbPlaces, 
                        prix: inputPrix
                    }, 
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-save-pack-billetterie .form-result-message', 'Pack sauvegard&eacute;', FormUtils.resultsTypes.ok);
                            $('#modal-save-pack-billetterie__bt-sauvegarder .loader').hide();
                            AdminShopBilletteriePacksController.load_TablePacks();
                        } else {
                            ViewUtils.activeButtonAndHideLoader('#modal-save-pack-billetterie__bt-sauvegarder', '#modal-save-pack-billetterie__bt-sauvegarder .loader');
                            FormUtils.displayFormResultMessage('#modal-save-pack-billetterie .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('#modal-save-pack-billetterie__bt-sauvegarder', '#modal-save-pack-billetterie__bt-sauvegarder .loader');
                        FormUtils.displayFormResultMessage('#modal-save-pack-billetterie .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            }
        }
    }

    function control_DeletePack() {
        $('.glyphicon-trash').unbind('click');
        $('.glyphicon-trash').click(function() {
            var idPack = $(this).attr('id-pack');
            
            ViewUtils.prompt(
                'Supprimer ce pack ?', 
                {
                    btOkText: 'Oui',
                    btKoText: 'Non'
                },
                function() {
                    deletePack(idPack);
                }, 
                null, 
                null
            );
        });

        function deletePack(idPack) {
            $('.loader-delete[id-pack='+idPack+']').show();
            AjaxUtils.callService(
                'pack-billetterie', 
                'delete', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idPack
                }, 
                function(response) {
                    $('.loader-delete[id-pack='+idPack+']').hide();
                    if (response.success) {
                        AdminShopBilletteriePacksController.load_TablePacks();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la suppression du pack', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-delete[id-pack='+idPack+']').hide();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        }
    }
};

initializePage = function() {
    AdminController.initializePage('shop', 'billetterie');

    AdminShopBilletteriePacksController.load_TablePacks();
};