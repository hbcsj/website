<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/pack-billetterie-dao.php");

class ModalSavePackBilletterieCtrl extends AbstractViewCtrl {
	
	private $packBilletterie;

	private $packBilletterieDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idPackBilletterie" => $_GET["idPackBilletterie"]
		), true);
		
		if (isAdminConnected_commercial()) {
			$this->packBilletterieDAO = new PackBilletterieDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdPack();
	}

	private function checkIdPack() {
		if (HTTPUtils::paramExists(GET, "idPackBilletterie")) {
			$this->packBilletterie = $this->packBilletterieDAO->getById($_GET["idPackBilletterie"]);
				
			if ($this->packBilletterie == null) {
				$this->sendCheckError(
					HTTP_404, 
					"Le pack billetterie (idPackBilletterie = '".$_GET["idPackBilletterie"]."') n'existe pas", 
					"webapp/views/common/error/404/404.html.php"
				);
			}
		}
	}

	public function getTitle() {
		$title = "Nouveau pack";
		if ($this->packBilletterie != null) {
			$title = "Pack '".$this->packBilletterie[PACK_BILLETTERIE_NOM]."' - <span class=\"normal\">Edition</span>";
		}
		return $title;
	}

	public function getNomPack() {
		if ($this->packBilletterie != null) {
			return $this->packBilletterie[PACK_BILLETTERIE_NOM];
		}
		return "";
	}

	public function getNbPlacesPack() {
		if ($this->packBilletterie != null) {
			return $this->packBilletterie[PACK_BILLETTERIE_NB_PLACES];
		}
		return "";
	}

	public function getPrixPack() {
		if ($this->packBilletterie != null) {
			return $this->packBilletterie[PACK_BILLETTERIE_PRIX];
		}
		return "";
	}
}

?>