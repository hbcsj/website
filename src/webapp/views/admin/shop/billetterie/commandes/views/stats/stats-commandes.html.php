<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/shop/billetterie/commandes/views/stats/", 
    "stats-commandes", 
    "StatsCommandesCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
    ?>
    <div class="container-stats__title">Pour la saison en cours</div>
    <div class="container-stats__section">
        <div class="container-stats__section__title">Nombre de commandes :</div>
        <div class="container-stats__section__value"><?php echo $ctrl->getNbCommandes(); ?></div>
    </div>
    <div class="container-stats__section">
        <div class="container-stats__section__title">Nombre de places : </div>
        <div class="container-stats__section__value"><?php echo $ctrl->getNbPlaces(); ?></div>
    </div>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>