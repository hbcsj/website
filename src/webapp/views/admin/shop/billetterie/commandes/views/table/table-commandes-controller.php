<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/adversaire-billetterie-dao.php");
require_once("common/php/dao/competition-billetterie-dao.php");
require_once("common/php/dao/commande-dao.php");
require_once("common/php/dao/commande-item-billetterie-dao.php");
require_once("common/php/dao/pack-billetterie-dao.php");
require_once("common/php/dao/match-billetterie-dao.php");
require_once("common/php/lib/date-utils.php");

define(TABLE_COMMANDES_BILLETTERIE_NB_PLACES, "nb_places");
define(TABLE_COMMANDES_BILLETTERIE_MONTANT, "prix");

class TableCommandesCtrl extends AbstractViewCtrl {

	private $adversaireBilletterieDAO;
	private $competitionBilletterieDAO;
	private $commandeItemBilletterieDAO;
	private $commandeDAO;
	private $matchBilletterieDAO;
	private $packBilletterieDAO;

	private $totaux;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_commercial()) {
			$this->adversaireBilletterieDAO = new AdversaireBilletterieDAO($this->getDatabaseConnection());
			$this->competitionBilletterieDAO = new CompetitionBilletterieDAO($this->getDatabaseConnection());
			$this->commandeItemBilletterieDAO = new CommandeItemBilletterieDAO($this->getDatabaseConnection());
			$this->commandeDAO = new CommandeDAO($this->getDatabaseConnection());
			$this->matchBilletterieDAO = new MatchBilletterieDAO($this->getDatabaseConnection());
			$this->packBilletterieDAO = new PackBilletterieDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getCommandesItems() {
		$this->totaux[TABLE_COMMANDES_BILLETTERIE_NB_PLACES] = 0;
		$this->totaux[TABLE_COMMANDES_BILLETTERIE_MONTANT] = 0;

		$commandesItems = $this->commandeItemBilletterieDAO->getAllByParams(
			$_GET["matchId"], 
			$_GET["packId"], 
			trim($_GET["reference"]), 
			trim($_GET["nom"]),
			trim($_GET["prenom"]), 
			DateUtils::convert_slashDate_to_sqlDate($_GET["dateDebut"]), 
			DateUtils::convert_slashDate_to_sqlDate($_GET["dateFin"]), 
			$_GET["donne"], 
			COMMANDE_TABLE_NAME.".".COMMANDE_DATE_HEURE." DESC"
		);

		if (sizeof($commandesItems) > 0) {
			foreach ($commandesItems as $commandeItem) {
				$this->totaux[TABLE_COMMANDES_BILLETTERIE_NB_PLACES] += $commandeItem[COMMANDE_ITEM_BILLETTERIE_QUANTITE];
				$this->totaux[TABLE_COMMANDES_BILLETTERIE_MONTANT] += $commandeItem[COMMANDE_ITEM_BILLETTERIE_PRIX];
			}
		}

		return $commandesItems;
	}

	public function getNomAdversaire($adversaireId) {
		$adversaire = $this->adversaireBilletterieDAO->getById($adversaireId);
		return $adversaire[ADVERSAIRE_BILLETTERIE_NOM];
	}

	public function getNomCompetition($competitionId) {
		$competition = $this->competitionBilletterieDAO->getById($competitionId);
		return $competition[COMPETITION_BILLETTERIE_NOM];
	}

	public function getNomPack($packId) {
		$pack = $this->packBilletterieDAO->getById($packId);
		return $pack[PACK_BILLETTERIE_NOM];
	}

	public function getCommande($commandeId) {
		return $this->commandeDAO->getById($commandeId);
	}

	public function getMatch($matchId) {
		return $this->matchBilletterieDAO->getById($matchId);
	}

	public function getTotaux() {
		return $this->totaux;
	}
}

?>