<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");

$view = new View(
    "webapp/views/admin/shop/billetterie/commandes/views/recherche/", 
    "recherche-commandes", 
    "RechercheCommandesCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
    $matchs = $ctrl->getMatchs();
    $personnesCommandes = $ctrl->getPersonnesCommandes();
    $packs = $ctrl->getPacks();
    ?>
    <div class="container-recherche__title" title="Ouvrir le formulaire de recherche" for="form-recherche-commandes">
        <div class="container-recherche__title__icon">
            <span class="glyphicon glyphicon-search"></span>
        </div>
        <div class="container-recherche__title__text">Recherche</div>
        <div class="container-recherche__title__arrow">
            <span class="glyphicon glyphicon-triangle-bottom"></span>
        </div>
    </div>
    <div class="form-horizontal" id="form-recherche-commandes">
        <div class="form-group">
            <label for="form-recherche-commandes__input-reference" class="col-xs-2 col-xs-offset-1 control-label">R&eacute;f :</label>
            <div class="col-xs-3 form-input" for="form-recherche-commandes__input-reference">
                <input class="form-control" id="form-recherche-commandes__input-reference">
            </div>
            <label for="form-recherche-commandes__input-nom" class="col-xs-1 control-label">Nom :</label>
            <div class="col-xs-3 form-input" for="form-recherche-commandes__input-nom">
                <select class="form-control" id="form-recherche-commandes__input-nom">
                    <option value=""></option>
                    <?php
                    if (sizeof($personnesCommandes) > 0) {
                        foreach ($personnesCommandes as $personneCommandes) {
                            ?>
                            <option value-nom="<?php echo $personneCommandes[COMMANDE_NOM]; ?>" value-prenom="<?php echo $personneCommandes[COMMANDE_PRENOM]; ?>" value="<?php echo $personneCommandes[COMMANDE_NOM]." ".$personneCommandes[COMMANDE_PRENOM]; ?>"><?php echo $personneCommandes[COMMANDE_NOM]." ".$personneCommandes[COMMANDE_PRENOM]; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="form-recherche-commandes__input-match" class="col-xs-2 col-xs-offset-1 control-label">Match :</label>
            <div class="col-xs-3 form-input" for="form-recherche-commandes__input-match">
                <select class="form-control" id="form-recherche-commandes__input-match">
                    <option value=""></option>
                    <?php
                    if (sizeof($matchs) > 0) {
                        foreach ($matchs as $match) {
                            ?>
                            <option value="<?php echo $match[MATCH_BILLETTERIE_ID]; ?>">Fenix VS <?php echo $ctrl->getNomAdversaire($match[MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID]); ?> (<?php echo $ctrl->getNomCompetition($match[MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID]); ?>) - <?php echo DateUtils::convert_sqlDateTime_to_slashDateTime($match[MATCH_BILLETTERIE_DATE_HEURE]); ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <label for="form-recherche-commandes__input-is-donne" class="col-xs-1 control-label">Donn&eacute; ?</label>
            <div class="col-xs-1 form-input" for="form-recherche-commandes__input-is-donne">
                <select class="form-control" id="form-recherche-commandes__input-is-donne">
                    <option value=""></option>
                    <option value="1">Oui</option>
                    <option value="0">Non</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="form-recherche-commandes__input-date-debut" class="col-xs-2 col-xs-offset-1 control-label">Date commande :</label>
            <div class="col-xs-2 form-input" for="form-recherche-commandes__input-date-debut">
                <div class="input-group">
                    <input type="text" class="form-control" id="form-recherche-commandes__input-date-debut" placeholder="D&eacute;but">
                    <span class="input-group-addon">
                        <img src="<?php echo SRC_PATH; ?>assets/img/icons/calendar.png">
                    </span>
                </div>
            </div>
            <label for="form-recherche-commandes__input-pack" class="col-xs-2 control-label">Pack :</label>
            <div class="col-xs-3 form-input" for="form-recherche-commandes__input-pack">
                <select class="form-control" id="form-recherche-commandes__input-pack">
                    <option value=""></option>
                    <?php
                    if (sizeof($packs) > 0) {
                        foreach ($packs as $pack) {
                            ?>
                            <option value="<?php echo $pack[PACK_BILLETTERIE_ID]; ?>"><?php echo $pack[PACK_BILLETTERIE_NOM]; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-2 col-xs-offset-3 form-input" for="form-recherche-commandes__input-date-fin">
                <div class="input-group">
                    <input type="text" class="form-control" id="form-recherche-commandes__input-date-fin" placeholder="Fin">
                    <span class="input-group-addon">
                        <img src="<?php echo SRC_PATH; ?>assets/img/icons/calendar.png">
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="container-recherche__footer" for="form-recherche-commandes">
        <button id="recherche-commandes__bt-rechercher" class="btn btn-default">
            <div class="button__icon">
                <span class="glyphicon glyphicon-search"></span>
            </div>
            <div class="button__text">Chercher</div>
        </button>
        <button id="recherche-commandes__bt-reset" class="btn btn-default">
            <div class="button__icon">
                <span class="glyphicon glyphicon-erase"></span>
            </div>
            <div class="button__text">Reset</div>
        </button>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>