<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/adversaire-billetterie-dao.php");
require_once("common/php/dao/competition-billetterie-dao.php");
require_once("common/php/dao/match-billetterie-dao.php");
require_once("common/php/dao/commande-item-billetterie-dao.php");
require_once("common/php/dao/pack-billetterie-dao.php");
require_once("common/php/lib/date-utils.php");

class RechercheCommandesCtrl extends AbstractViewCtrl {

	private $adversaireBilletterieDAO;
	private $competitionBilletterieDAO;
	private $matchBilletterieDAO;
	private $commandeItemBilletterieDAO;
	private $packBilletterieDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_commercial()) {
			$this->adversaireBilletterieDAO = new AdversaireBilletterieDAO($this->getDatabaseConnection());
			$this->competitionBilletterieDAO = new CompetitionBilletterieDAO($this->getDatabaseConnection());
			$this->matchBilletterieDAO = new MatchBilletterieDAO($this->getDatabaseConnection());
			$this->commandeItemBilletterieDAO = new CommandeItemBilletterieDAO($this->getDatabaseConnection());
			$this->packBilletterieDAO = new PackBilletterieDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getMatchs() {
		return $this->matchBilletterieDAO->getAll(
			MATCH_BILLETTERIE_TABLE_NAME.".".MATCH_BILLETTERIE_DATE_HEURE." DESC"
		);
	}

	public function getPersonnesCommandes() {
		return $this->commandeItemBilletterieDAO->getPersonnesCommandes();
	}

	public function getNomAdversaire($adversaireId) {
		$adversaire = $this->adversaireBilletterieDAO->getById($adversaireId);
		return $adversaire[ADVERSAIRE_BILLETTERIE_NOM];
	}

	public function getNomCompetition($competitionId) {
		$competition = $this->competitionBilletterieDAO->getById($competitionId);
		return $competition[COMPETITION_BILLETTERIE_NOM];
	}

	public function getPacks() {
		return $this->packBilletterieDAO->getAll(
			PACK_BILLETTERIE_TABLE_NAME.".".PACK_BILLETTERIE_ID." DESC"
		);
	}
}

?>