<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");

$view = new View(
    "webapp/views/admin/shop/billetterie/commandes/views/table/", 
    "table-commandes", 
    "TableCommandesCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
    $commandesItems = $ctrl->getCommandesItems();
    $totaux = $ctrl->getTotaux();
    ?>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Date</th>
                <th>Nom</th>
                <th>Coord.</th>
                <th>R&eacute;f. com<sup>de</sup></th>
                <th>Match</th>
                <th class="text-center">Nb. places</th>
                <th class="text-center">Prix</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th colspan="5">TOTAL</th>
                <th class="text-center"><?php echo $totaux[TABLE_COMMANDES_BILLETTERIE_NB_PLACES]; ?></th>
                <th class="text-center"><?php echo $totaux[TABLE_COMMANDES_BILLETTERIE_MONTANT]; ?> &euro;</th>
                <th colspan="2">&nbsp;</th>
            </tr>
            <?php
            if (sizeof($commandesItems) > 0) {
                foreach ($commandesItems as $commandeItem) {
                    $commande = $ctrl->getCommande($commandeItem[COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID]);
                    $match = $ctrl->getMatch($commandeItem[COMMANDE_ITEM_BILLETTERIE_MATCH_BILLETTERIE_ID]);

                    $btDonneClass = ($commandeItem[COMMANDE_ITEM_BILLETTERIE_DONNE] == 1) ? "bt-ok" : "bt-error";

                    $btPackClass = "bt-ok";
                    $nomPack = $ctrl->getNomPack($commandeItem[COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID]);
                    if ($nomPack == "") {
                        $nomPack = "Aucun";
                        $btPackClass = "bt-warning";
                    }
                    ?>
                    <tr>
                        <td><?php echo str_replace(SLASH_DATE_TIME_SEPARATOR, "<br>", DateUtils::convert_sqlDateTime_to_slashDateTime($commande[COMMANDE_DATE_HEURE])); ?></td>
                        <td><?php echo $commande[COMMANDE_NOM]."<br>".$commande[COMMANDE_PRENOM]; ?></td>
                        <td>
                            @ : <a href="mailto:<?php echo $commande[COMMANDE_EMAIL]; ?>"><?php echo $commande[COMMANDE_EMAIL]; ?></a>
                            <br>
                            T&eacute;l : <a href="tel:<?php echo str_replace(".", "", $commande[COMMANDE_TELEPHONE]); ?>"><?php echo $commande[COMMANDE_TELEPHONE]; ?></a>
                        </td>
                        <td><?php echo $commande[COMMANDE_REFERENCE]; ?></td>
                        <td>
                            Fenix VS <?php echo $ctrl->getNomAdversaire($match[MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID]); ?>
                            <br>
                            <?php echo $ctrl->getNomCompetition($match[MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID]); ?>
                            <br>
                            <?php echo DateUtils::convert_sqlDateTime_to_slashDateTime($match[MATCH_BILLETTERIE_DATE_HEURE]); ?>
                        </td>
                        <td class="text-center"><?php echo $commandeItem[COMMANDE_ITEM_BILLETTERIE_QUANTITE]; ?></td>
                        <td class="text-center"><?php echo $commandeItem[COMMANDE_ITEM_BILLETTERIE_PRIX]; ?> &euro;</td>
                        <td class="text-right">
                            <button class="btn btn-default bt-table-admin bt-commande-item-billetterie-donne <?php echo $btDonneClass; ?>" id-commande-item-billetterie="<?php echo $commandeItem[COMMANDE_ITEM_BILLETTERIE_ID]; ?>">
                                <div class="button__text"><?php echo ($commandeItem[COMMANDE_ITEM_BILLETTERIE_DONNE] == 1) ? "Donn&eacute;" : "Non donn&eacute;"; ?></div>
                                <div class="loader"></div>
                            </button>
                            <br><br>
                            <button class="btn btn-default bt-table-admin bt-commande-item-billetterie-pack <?php echo $btPackClass; ?>" id-commande-item-billetterie="<?php echo $commandeItem[COMMANDE_ITEM_BILLETTERIE_ID]; ?>">
                                <div class="button__text">Pack : <?php echo $nomPack; ?></div>
                            </button>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>