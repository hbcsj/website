<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/commande-dao.php");
require_once("common/php/dao/commande-item-billetterie-dao.php");
require_once("common/php/lib/date-utils.php");

class StatsCommandesCtrl extends AbstractViewCtrl {

	private $commandeItemBilletterieDAO;
	private $commandeDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_commercial()) {
			$this->commandeItemBilletterieDAO = new CommandeItemBilletterieDAO($this->getDatabaseConnection());
			$this->commandeDAO = new CommandeDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getNbCommandes() {
		return $this->commandeDAO->getNbLinkedToCommandeItemBilletterie(DateUtils::get_sqlDate_debutSaison())[COMMANDE_NB_TOTAL];
	}

	public function getNbPlaces() {
		$nbPlaces = $this->commandeItemBilletterieDAO->getNbTotalPlacesAfterDate(DateUtils::get_sqlDate_debutSaison())[COMMANDE_ITEM_BILLETTERIE_NB_PLACES];
		return ($nbPlaces != null) ? $nbPlaces : 0;
	}
}

?>