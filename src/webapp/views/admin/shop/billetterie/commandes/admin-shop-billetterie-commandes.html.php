<?php
require_once("core/php/resources/page.php");
require_once("common/php/lib/admin-utils.php");

$page = new Page(
    "webapp/views/admin/shop/billetterie/commandes/", 
    "admin-shop-billetterie-commandes", 
    "AdminShopBilletterieCommandesCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/admin/common/nav/nav.html.php");
if (isAdminConnected_commercial()) {
    require_once("webapp/views/admin/shop/sub-menu/sub-menu-admin-shop.html.php");
}
?>

<div class="container-fluid animated fadeIn">
    <?php
    if (isAdminConnected_commercial()) {
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <?php require_once("webapp/views/admin/shop/billetterie/header/header-admin-shop-billetterie.html.php"); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h2>Commandes</h2>
            </div>
        </div>
        <div id="container-stats-commandes" class="container-stats"></div>
        <div id="container-recherche-commandes" class="container-recherche"></div>
        <div id="container-table-commandes" class="container-table"></div>
        <?php
    } else {
        require_once("webapp/views/common/mdp-form/mdp-form.html.php");
    }
    ?>
</div>

<?php
require_once("webapp/views/common/modals/alert/modal-alert.html.php");
require_once("webapp/views/common/modals/prompt/modal-prompt.html.php");

if (isAdminConnected_commercial()) {
    require_once("webapp/views/admin/shop/billetterie/commandes/modals/link-pack/modal-link-pack-commande-billetterie.html.php");
}

$page->finalizePage();
?>