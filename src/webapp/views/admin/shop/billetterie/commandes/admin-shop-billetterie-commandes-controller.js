AdminShopBilletterieCommandesController = {};

AdminShopBilletterieCommandesController.dateDebutRecherche = null;

AdminShopBilletterieCommandesController.load_StatsCommandes = function() {
    if (Utils.exists('#container-stats-commandes')) {
        AjaxUtils.loadView(
            'admin/shop/billetterie/commandes/views/stats/stats-commandes',
            '#container-stats-commandes',
            null,
            function() {},
            '#loader-page'
        );
    }
};

AdminShopBilletterieCommandesController.load_RechercheAndTableCommandes = function() {
    if (Utils.exists('#container-recherche-commandes')) {
        AjaxUtils.loadView(
            'admin/shop/billetterie/commandes/views/recherche/recherche-commandes',
            '#container-recherche-commandes',
            null,
            function() {
                $('#form-recherche-commandes__input-date-debut').val(AdminShopBilletterieCommandesController.dateDebutRecherche);

                FormRechercheUtils.control_toggleFormRecherche();
                onClick_btRechercher();
                onClick_btReset();
                AdminShopBilletterieCommandesController.load_TableCommandes();
            },
            '#loader-page'
        );
    }

    function onClick_btRechercher() {
        $('#recherche-commandes__bt-rechercher').unbind('click');
        $('#recherche-commandes__bt-rechercher').click(function() {
            AdminShopBilletterieCommandesController.load_TableCommandes();
        });
    }

    function onClick_btReset() {
        $('#recherche-commandes__bt-reset').unbind('click');
        $('#recherche-commandes__bt-reset').click(function() {
            $('#form-recherche-commandes__input-reference').val('');
            $('#form-recherche-commandes__input-match').val('');
            $('#form-recherche-commandes__input-nom').val('');
            $('#form-recherche-commandes__input-date-debut').val(AdminShopBilletterieCommandesController.dateDebutRecherche);
            $('#form-recherche-commandes__input-date-fin').val('');
            $('#form-recherche-commandes__input-is-donne').val('');
            $('#form-recherche-commandes__input-pack').val('');

            AdminShopBilletterieCommandesController.load_TableCommandes();
        });
    }
};

AdminShopBilletterieCommandesController.load_TableCommandes = function() {
    if (Utils.exists('#container-table-commandes')) {
        var params = {
            reference: $.trim($('#form-recherche-commandes__input-reference').val()),
            matchId: $.trim($('#form-recherche-commandes__input-match').val()),
            nom: $.trim($('#form-recherche-commandes__input-nom').find('option:selected').attr('value-nom')),
            prenom: $.trim($('#form-recherche-commandes__input-nom').find('option:selected').attr('value-prenom')),
            dateDebut: $.trim($('#form-recherche-commandes__input-date-debut').val()),
            dateFin: $.trim($('#form-recherche-commandes__input-date-fin').val()),
            donne: $('#form-recherche-commandes__input-is-donne').val(),
            packId: $.trim($('#form-recherche-commandes__input-pack').val())
        };
        AjaxUtils.loadView(
            'admin/shop/billetterie/commandes/views/table/table-commandes',
            '#container-table-commandes',
            params,
            function() {
                control_LinkPack();
                control_CommandeItemDonne();
            },
            '#loader-page'
        );
    }

    function control_LinkPack() {
        $('.bt-commande-item-billetterie-pack').unbind('click');
        $('.bt-commande-item-billetterie-pack').click(function() {
            var idCommandeItemBilletterie = $(this).attr('id-commande-item-billetterie');
            open_ModalLinkPack(idCommandeItemBilletterie);
        });

        function open_ModalLinkPack(idCommandeItemBilletterie) {
            var params = {
                idCommandeItemBilletterie: idCommandeItemBilletterie
            };
            AjaxUtils.loadView(
                'admin/shop/billetterie/commandes/modals/link-pack/modal-link-pack-commande-billetterie-content',
                '#modal-link-pack-commande-billetterie-content',
                params,
                function() {
                    onClick_btSauvegarder(idCommandeItemBilletterie);
                },
                '#loader-page'
            );
            $('#modal-link-pack-commande-billetterie').modal('show');
            
            function onClick_btSauvegarder(idCommandeItemBilletterie) {
                $('#modal-link-pack-commande-billetterie__bt-sauvegarder').unbind('click');
                $('#modal-link-pack-commande-billetterie__bt-sauvegarder').click(function() {
                    FormUtils.hideFormErrors('#modal-link-pack-commande-billetterie #form-link-pack-commande-billetterie');
                    ViewUtils.unsetHTMLAndHide('#modal-link-pack-commande-billetterie .form-result-message');
                    ViewUtils.desactiveButtonAndShowLoader('#modal-link-pack-commande-billetterie__bt-sauvegarder', '#modal-link-pack-commande-billetterie__bt-sauvegarder .loader');
    
                    var formValidation = FormUtils.validateForm('#modal-link-pack-commande-billetterie #form-link-pack-commande-billetterie');
                    
                    if (formValidation) {
                        var inputPack = $('#form-link-pack-commande-billetterie__input-pack').val();
    
                        saveLinkPackCommandeBilletterie(idCommandeItemBilletterie, inputPack);
                    } else {
                        ViewUtils.activeButtonAndHideLoader('#modal-link-pack-commande-billetterie__bt-sauvegarder', '#modal-link-pack-commande-billetterie__bt-sauvegarder .loader');
                    }
                });
    
                function saveLinkPackCommandeBilletterie(idCommandeItemBilletterie, inputPack) {
                    AjaxUtils.callService(
                        'commande-billetterie', 
                        'saveLinkPack', 
                        AjaxUtils.dataTypes.json, 
                        AjaxUtils.requestTypes.post, 
                        {
                            id: idCommandeItemBilletterie
                        }, 
                        {
                            packBilletterieId: inputPack
                        }, 
                        function(response) {
                            if (response.success) {
                                FormUtils.displayFormResultMessage('#modal-link-pack-commande-billetterie .form-result-message', 'Pack associ&eacute;', FormUtils.resultsTypes.ok);
                                $('#modal-link-pack-commande-billetterie__bt-sauvegarder .loader').hide();
                                AdminShopBilletterieCommandesController.load_StatsCommandes();
                                AdminShopBilletterieCommandesController.load_TableCommandes();
                            } else {
                                ViewUtils.activeButtonAndHideLoader('#modal-link-pack-commande-billetterie__bt-sauvegarder', '#modal-link-pack-commande-billetterie__bt-sauvegarder .loader');
                                FormUtils.displayFormResultMessage('#modal-link-pack-commande-billetterie .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
                            }
                        },
                        function(response) {
                            ViewUtils.activeButtonAndHideLoader('#modal-link-pack-commande-billetterie__bt-sauvegarder', '#modal-link-pack-commande-billetterie__bt-sauvegarder .loader');
                            FormUtils.displayFormResultMessage('#modal-link-pack-commande-billetterie .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                        }
                    );
                }
            }
        }
    }

    function control_CommandeItemDonne() {
        $('.bt-commande-item-billetterie-donne').unbind('click');
        $('.bt-commande-item-billetterie-donne').click(function() {
            var idCommandeItemBilletterie = $(this).attr('id-commande-item-billetterie');
            
            $('.bt-commande-item-billetterie-donne[id-commande-item-billetterie='+idCommandeItemBilletterie+'] .loader').show();
            AjaxUtils.callService(
                'commande-billetterie', 
                'toggleDonne', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idCommandeItemBilletterie
                }, 
                function(response) {
                    $('.bt-commande-item-billetterie-donne[id-commande-item-billetterie='+idCommandeItemBilletterie+'] .loader').show();
                    if (response.success) {
                        AdminShopBilletterieCommandesController.load_TableCommandes();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la modification du commande item billetterie', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.bt-commande-item-billetterie-donne[id-commande-item-billetterie='+idCommandeItemBilletterie+'] .loader').show();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        });
    }
};

initializePage = function() {
    AdminController.initializePage('shop', 'billetterie');

    AjaxUtils.callService(
        'date', 
        'getSlashDateDebutSaison', 
        AjaxUtils.dataTypes.json, 
        AjaxUtils.requestTypes.get, 
        null, 
        null, 
        function(response) {
            AdminShopBilletterieCommandesController.dateDebutRecherche = response.date;

            AdminShopBilletterieCommandesController.load_StatsCommandes();
            AdminShopBilletterieCommandesController.load_RechercheAndTableCommandes();
        },
        function(response) {}
    );
};