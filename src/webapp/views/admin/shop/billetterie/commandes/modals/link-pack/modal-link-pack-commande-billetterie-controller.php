<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/commande-item-billetterie-dao.php");
require_once("common/php/dao/commande-dao.php");
require_once("common/php/dao/pack-billetterie-dao.php");

class ModalLinkPackCommandeBilletterieCtrl extends AbstractViewCtrl {
	
	private $commandeItemBilletterieDAO;
	private $commandeDAO;
	private $packBilletterieDAO;

	private $commandeItemBilletterie;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idCommandeItemBilletterie" => $_GET["idCommandeItemBilletterie"]
		), true);
		
		if (isAdminConnected_commercial()) {
			$this->commandeItemBilletterieDAO = new CommandeItemBilletterieDAO($this->getDatabaseConnection());
			$this->commandeDAO = new CommandeDAO($this->getDatabaseConnection());
			$this->packBilletterieDAO = new PackBilletterieDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdCommandeItemBilletterie();
	}

	private function checkIdCommandeItemBilletterie() {
		$this->commandeItemBilletterie = $this->commandeItemBilletterieDAO->getById($_GET["idCommandeItemBilletterie"]);

		if ($this->commandeItemBilletterie == null) {
			$this->sendCheckError(
				HTTP_404, 
				"La commande billetterie (idCommandeItemBilletterie = '".$_GET["idCommandeItemBilletterie"]."') n'existe pas", 
				"webapp/views/common/error/404/404.html.php"
			);
		}
	}

	public function getOptionsPack() {
		$html = "";

		$packs = $this->packBilletterieDAO->getAll(PACK_BILLETTERIE_TABLE_NAME.".".PACK_BILLETTERIE_ID." DESC");
		if (sizeof($packs) > 0) {
			foreach ($packs as $pack) {
				$selected = "";
				if ($this->commandeItemBilletterie[COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID] == $pack[PACK_BILLETTERIE_ID]) {
					$selected = " selected";
				}
				$html .= "<option value=\"".$pack[PACK_BILLETTERIE_ID]."\"".$selected.">".$pack[PACK_BILLETTERIE_NOM]."</option>";
			}
		}

		return $html;
	}

	public function getReferenceCommande() {
		$commande = $this->commandeDAO->getById($this->commandeItemBilletterie[COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID]);
		return $commande[COMMANDE_REFERENCE];
	}
}

?>