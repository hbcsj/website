<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");

$view = new View(
    "webapp/views/admin/shop/commandes/views/table/", 
    "table-commandes", 
    "TableCommandesCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
    $commandes = $ctrl->getCommandes();
    $totaux = $ctrl->getTotaux();
    ?>
    <table class="table table-condensed">
        <thead>
            <tr>
                <th>Date</th>
                <th>Nom</th>
                <th>Coord.</th>
                <th>R&eacute;f.</th>
                <th class="text-center">Quantit&eacute;</th>
                <th class="text-center">Montant</th>
                <?php
                if (isAdminConnected_commercial()) {
                    ?>
                    <th colspan="2">&nbsp;</th>
                    <?php
                } else {
                    ?>
                    <th>&nbsp;</th>
                    <?php
                }
                ?>
            </tr>
        </thead>
        <tbody>
            <tr class="tr-total">
                <th colspan="5">TOTAL</th>
                <th class="text-center"><?php echo $totaux[TABLE_COMMANDES_MONTANT]; ?> &euro;</th>
                <?php
                if (isAdminConnected_commercial()) {
                    ?>
                    <th colspan="2">&nbsp;</th>
                    <?php
                } else {
                    ?>
                    <th>&nbsp;</th>
                    <?php
                }
                ?>
            </tr>
            <?php
            if (sizeof($commandes) > 0) {
                foreach ($commandes as $commande) {
                    $btPayeClass = ($commande[COMMANDE_PAYE] == 1) ? "bt-ok" : "bt-error";

                    $nbPaiements = $ctrl->getNbPaiementsAssocies($commande[COMMANDE_ID]);
                    $btPaiementsClass = ($nbPaiements == 0) ? "bt-error" : "bt-paiements-commande";
                    ?>
                    <tr class="tr-separator">
                        <?php
                        if (isAdminConnected_commercial()) {
                            ?>
                            <td colspan="8">&nbsp;</td>
                            <?php
                        } else {
                            ?>
                            <td colspan="7">&nbsp;</td>
                            <?php
                        }
                        ?>
                    </tr>
                    <tr class="tr-header-commande" id-commande="<?php echo $commande[COMMANDE_ID]; ?>">
                        <td class="clickable"><?php echo str_replace(SLASH_DATE_TIME_SEPARATOR, "<br>", DateUtils::convert_sqlDateTime_to_slashDateTime($commande[COMMANDE_DATE_HEURE])); ?></td>
                        <td class="clickable"><?php echo $commande[COMMANDE_NOM]."<br>".$commande[COMMANDE_PRENOM]; ?></td>
                        <td class="clickable">
                            @ : <a href="mailto:<?php echo $commande[COMMANDE_EMAIL]; ?>"><?php echo $commande[COMMANDE_EMAIL]; ?></a>
                            <br>
                            T&eacute;l : <a href="tel:<?php echo str_replace(".", "", $commande[COMMANDE_TELEPHONE]); ?>"><?php echo $commande[COMMANDE_TELEPHONE]; ?></a>
                        </td>
                        <td class="clickable"><?php echo $commande[COMMANDE_REFERENCE]; ?></td>
                        <td class="clickable">&nbsp;</td>
                        <td class="clickable text-center"><?php echo $commande[TABLE_COMMANDES_MONTANT]; ?> &euro;</td>
                        <td class="text-right">
                            <button class="btn btn-default bt-table-admin bt-commande-paye <?php echo $btPayeClass; ?>" id-commande="<?php echo $commande[COMMANDE_ID]; ?>">
                                <div class="button__text"><?php echo ($commande[COMMANDE_PAYE] == 1) ? "Pay&eacute;e" : "Non Pay&eacute;e"; ?></div>
                                <div class="loader"></div>
                            </button>
                            <br><br>
                            <button class="btn btn-default bt-table-admin <?php echo $btPaiementsClass; ?>" id-commande="<?php echo $commande[COMMANDE_ID]; ?>">
                                <div class="button__text"><?php echo $nbPaiements; ?> paiement(s) associ&eacute;(s)</div>
                            </button>
                        </td>
                        <?php
                        if (isAdminConnected_commercial()) {
                            ?>
                            <td class="text-center <?php echo $bg; ?>">
                                <span class="glyphicon glyphicon-trash clickable" title="Supprimer" id-commande="<?php echo $commande[COMMANDE_ID]; ?>"></span>
                                <div class="loader loader-delete" id-commande="<?php echo $commande[COMMANDE_ID]; ?>"></div>
                            </td>
                            <?php
                        }
                        ?>
                    </tr>
                    <?php
                    foreach ($commande[COMMANDES_ITEMS] as $commandeItem) {
                        if (array_key_exists(COMMANDE_ITEM_BOUTIQUE_TAILLE, $commandeItem)) {
                            $produit = $ctrl->getProduit($commandeItem[COMMANDE_ITEM_BOUTIQUE_PRODUIT_BOUTIQUE_ID]);
                            ?>
                            <tr class="tr-commande-item" id-commande="<?php echo $commande[COMMANDE_ID]; ?>">
                                <td colspan="3">&nbsp;</td>
                                <td>
                                    <?php 
                                    echo $produit[PRODUIT_BOUTIQUE_NOM]; 
                                    if ($produit[PRODUIT_BOUTIQUE_INDICATION] != "") {
                                        echo " - ".$produit[PRODUIT_BOUTIQUE_INDICATION];
                                    }
                                    echo "<br>".$ctrl->getNomCategorie($produit[PRODUIT_BOUTIQUE_CATEGORIE_PRODUIT_BOUTIQUE_ID]);
                                    if ($produit[PRODUIT_BOUTIQUE_MARQUE_PRODUIT_BOUTIQUE_ID] != null) {
                                        echo " - ".$ctrl->getNomMarque($produit[PRODUIT_BOUTIQUE_MARQUE_PRODUIT_BOUTIQUE_ID]);
                                    }
                                    if ($commandeItem[COMMANDE_ITEM_BOUTIQUE_TAILLE] != "") {
                                        echo "<br>Taille ".$commandeItem[COMMANDE_ITEM_BOUTIQUE_TAILLE];
                                    }
                                    if ($commandeItem[COMMANDE_ITEM_BOUTIQUE_COULEUR] != "") {
                                        echo "<br>".$commandeItem[COMMANDE_ITEM_BOUTIQUE_COULEUR];
                                    }
                                    if ($commandeItem[COMMANDE_ITEM_BOUTIQUE_FLOCAGE_NOM] != "") {
                                        echo "<br>Flocage nom : ".$commandeItem[COMMANDE_ITEM_BOUTIQUE_FLOCAGE_NOM];
                                    }
                                    if ($commandeItem[COMMANDE_ITEM_BOUTIQUE_FLOCAGE_NUMERO] != "") {
                                        echo "<br>Flocage num&eacute;ro : ".$commandeItem[COMMANDE_ITEM_BOUTIQUE_FLOCAGE_NUMERO];
                                    }
                                    if ($commandeItem[COMMANDE_ITEM_BOUTIQUE_FLOCAGE_INITIALES] != "") {
                                        echo "<br>Flocage initiales : ".$commandeItem[COMMANDE_ITEM_BOUTIQUE_FLOCAGE_INITIALES];
                                    }
                                    ?>
                                </td>
                                <td class="text-center"><?php echo $commandeItem[COMMANDE_ITEM_BOUTIQUE_QUANTITE]; ?></td>
                                <td class="text-center"><?php echo $commandeItem[COMMANDE_ITEM_BOUTIQUE_PRIX]; ?> &euro;</td>
                                <?php
                                if (isAdminConnected_commercial()) {
                                    ?>
                                    <td colspan="2">&nbsp;</td>
                                    <?php
                                } else {
                                    ?>
                                    <td>&nbsp;</td>
                                    <?php
                                }
                                ?>
                            </tr>
                            <?php
                        } else {
                            $match = $ctrl->getMatch($commandeItem[COMMANDE_ITEM_BILLETTERIE_MATCH_BILLETTERIE_ID]);
                            ?>
                            <tr class="tr-commande-item" id-commande="<?php echo $commande[COMMANDE_ID]; ?>">
                                <td colspan="3">&nbsp;</td>
                                <td>
                                    Fenix VS <?php echo $ctrl->getNomAdversaire($match[MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID]); ?>
                                    <br>
                                    <?php echo $ctrl->getNomCompetition($match[MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID]); ?>
                                    <br>
                                    <?php echo DateUtils::convert_sqlDateTime_to_slashDateTime($match[MATCH_BILLETTERIE_DATE_HEURE]); ?>
                                </td>
                                <td class="text-center"><?php echo $commandeItem[COMMANDE_ITEM_BILLETTERIE_QUANTITE]; ?></td>
                                <td class="text-center"><?php echo $commandeItem[COMMANDE_ITEM_BILLETTERIE_PRIX]; ?> &euro;</td>
                                <?php
                                if (isAdminConnected_commercial()) {
                                    ?>
                                    <td colspan="2">&nbsp;</td>
                                    <?php
                                } else {
                                    ?>
                                    <td>&nbsp;</td>
                                    <?php
                                }
                                ?>
                            </tr>
                            <?php
                        }
                    }
                }
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>