<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/adversaire-billetterie-dao.php");
require_once("common/php/dao/competition-billetterie-dao.php");
require_once("common/php/dao/commande-dao.php");
require_once("common/php/dao/paiement-dao.php");
require_once("common/php/dao/commande-item-billetterie-dao.php");
require_once("common/php/dao/commande-item-boutique-dao.php");
require_once("common/php/dao/match-billetterie-dao.php");
require_once("common/php/dao/produit-boutique-dao.php");
require_once("common/php/dao/categorie-produit-boutique-dao.php");
require_once("common/php/dao/marque-produit-boutique-dao.php");
require_once("common/php/lib/date-utils.php");

define(TABLE_COMMANDES_MONTANT, "montant");
define(TABLE_COMMANDES_QUANTITE, "quantite");
define(COMMANDES_ITEMS, "commandes_items");

class TableCommandesCtrl extends AbstractViewCtrl {

	private $adversaireBilletterieDAO;
	private $competitionBilletterieDAO;
	private $commandeItemBilletterieDAO;
	private $commandeItemBoutiqueDAO;
	private $commandeDAO;
	private $paiementDAO;
	private $produitBoutiqueDAO;
	private $categorieProduitBoutiqueDAO;
	private $marqueProduitBoutiqueDAO;
	private $matchBilletterieDAO;

	private $totaux;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_commercial()) {
			$this->adversaireBilletterieDAO = new AdversaireBilletterieDAO($this->getDatabaseConnection());
			$this->competitionBilletterieDAO = new CompetitionBilletterieDAO($this->getDatabaseConnection());
			$this->commandeItemBilletterieDAO = new CommandeItemBilletterieDAO($this->getDatabaseConnection());
			$this->commandeItemBoutiqueDAO = new CommandeItemBoutiqueDAO($this->getDatabaseConnection());
			$this->commandeDAO = new CommandeDAO($this->getDatabaseConnection());
			$this->paiementDAO = new PaiementDAO($this->getDatabaseConnection());
			$this->produitBoutiqueDAO = new ProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->categorieProduitBoutiqueDAO = new CategorieProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->marqueProduitBoutiqueDAO = new MarqueProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->matchBilletterieDAO = new MatchBilletterieDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getCommandes() {
		$this->totaux[TABLE_COMMANDES_MONTANT] = 0;

		$commandes = array();
		$commandesTmp = $this->commandeDAO->getAllByParams(
			trim($_GET["reference"]), 
			trim($_GET["nom"]),
			trim($_GET["prenom"]), 
			DateUtils::convert_slashDate_to_sqlDate($_GET["dateDebut"]), 
			DateUtils::convert_slashDate_to_sqlDate($_GET["dateFin"]), 
			$_GET["paye"],
			$_GET["saisonId"],
			COMMANDE_TABLE_NAME.".".COMMANDE_DATE_HEURE." DESC"
		);

		if (sizeof($commandesTmp) > 0) {
			foreach ($commandesTmp as $commande) {
				$totalMontantCommande = 0;
				$totalQuantiteCommande = 0;
				$commande[COMMANDES_ITEMS] = array();

				$commandeItemsBoutique = $this->commandeItemBoutiqueDAO->getByCommandeId($commande[COMMANDE_ID]);
				if (sizeof($commandeItemsBoutique) > 0) {
					foreach ($commandeItemsBoutique as $commandeItemBoutique) {
						$totalQuantiteCommande += $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_QUANTITE];
						$totalMontantCommande += $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_PRIX];
						$this->totaux[TABLE_COMMANDES_MONTANT] += $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_PRIX];
						$commande[COMMANDES_ITEMS][] = $commandeItemBoutique;
					}
				}

				$commandeItemsBilletterie = $this->commandeItemBilletterieDAO->getByCommandeId($commande[COMMANDE_ID]);
				if (sizeof($commandeItemsBilletterie) > 0) {
					foreach ($commandeItemsBilletterie as $commandeItemBilletterie) {
						$totalQuantiteCommande += $commandeItemBilletterie[COMMANDE_ITEM_BILLETTERIE_QUANTITE];
						$totalMontantCommande += $commandeItemBilletterie[COMMANDE_ITEM_BILLETTERIE_PRIX];
						$this->totaux[TABLE_COMMANDES_MONTANT] += $commandeItemBilletterie[COMMANDE_ITEM_BILLETTERIE_PRIX];
						$commande[COMMANDES_ITEMS][] = $commandeItemBilletterie;
					}
				}
				
				$commande[TABLE_COMMANDES_MONTANT] = $totalMontantCommande;
				$commande[TABLE_COMMANDES_QUANTITE] = $totalQuantiteCommande;

				$commandes[] = $commande;
			}
		}

		return $commandes;
	}

	public function getProduit($produitId) {
		return $this->produitBoutiqueDAO->getById($produitId);
	}

	public function getNomAdversaire($adversaireId) {
		$adversaire = $this->adversaireBilletterieDAO->getById($adversaireId);
		return $adversaire[ADVERSAIRE_BILLETTERIE_NOM];
	}

	public function getNomCompetition($competitionId) {
		$competition = $this->competitionBilletterieDAO->getById($competitionId);
		return $competition[COMPETITION_BILLETTERIE_NOM];
	}

	public function getNomMarque($marqueId) {
		$marque = $this->marqueProduitBoutiqueDAO->getById($marqueId);
		return $marque[MARQUE_PRODUIT_BOUTIQUE_NOM];
	}

	public function getNomCategorie($categorieId) {
		$categorie = $this->categorieProduitBoutiqueDAO->getById($categorieId);
		return $categorie[CATEGORIE_PRODUIT_BOUTIQUE_NOM];
	}

	public function getMatch($matchId) {
		return $this->matchBilletterieDAO->getById($matchId);
	}

	public function getTotaux() {
		return $this->totaux;
	}

	public function getNbPaiementsAssocies($commandeId) {
		return sizeof($this->paiementDAO->getByCommandeId($commandeId));
	}
}

?>