<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/commande-dao.php");
require_once("common/php/lib/date-utils.php");

class StatsCommandesCtrl extends AbstractViewCtrl {

	private $commandeDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_commercial()) {
			$this->commandeDAO = new CommandeDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getNbCommandes() {
		return sizeof($this->commandeDAO->getAllByParams(
			null, 
			null, 
			null, 
			DateUtils::get_sqlDate_debutSaison(), 
			null, 
			null,
			null
		));
	}

	public function getNbCommandesPayees() {
		return sizeof($this->commandeDAO->getAllByParams(
			null, 
			null, 
			null, 
			DateUtils::get_sqlDate_debutSaison(), 
			null, 
			true,
			null
		));
	}
}

?>