<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/saison-dao.php");
require_once("common/php/dao/commande-dao.php");
require_once("common/php/lib/date-utils.php");

class RechercheCommandesCtrl extends AbstractViewCtrl {

	private $saisonDAO;
	private $commandeDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_commercial()) {
			$this->saisonDAO = new SaisonDAO($this->getDatabaseConnection());
			$this->commandeDAO = new CommandeDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getSaisons() {
		return $this->saisonDAO->getAll(SAISON_TABLE_NAME.".".SAISON_ID." DESC");
	}

	public function getPersonnesCommandes() {
		return $this->commandeDAO->getPersonnesCommandes();
	}
}

?>