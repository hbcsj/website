<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/commande-dao.php");
require_once("common/php/dao/paiement-dao.php");
require_once("common/php/dao/type-paiement-dao.php");

class ModalPaiementsCommandeCtrl extends AbstractViewCtrl {
	
	private $commandeDAO;
	private $paiementDAO;
	private $typePaiementDAO;

	private $commande;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idCommande" => $_GET["idCommande"]
		), true);
		
		if (isAdminConnected_commercial()) {
			$this->commandeDAO = new CommandeDAO($this->getDatabaseConnection());
			$this->paiementDAO = new PaiementDAO($this->getDatabaseConnection());
			$this->typePaiementDAO = new TypePaiementDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdCommande();
	}

	private function checkIdCommande() {
		$this->commande = $this->commandeDAO->getById($_GET["idCommande"]);

		if ($this->commande == null) {
			$this->sendCheckError(
				HTTP_404, 
				"La commande (idCommande = '".$_GET["idCommande"]."') n'existe pas", 
				"webapp/views/common/error/404/404.html.php"
			);
		}
	}

	public function getCommande() {
		return $this->commande;
	}

	public function getPaiements() {
		return $this->paiementDAO->getByCommandeId($this->commande[COMMANDE_ID]);
	}

	public function getLibelleTypePaiement($typePaiementId) {
		$typePaiement = $this->typePaiementDAO->getById($typePaiementId);
		return $typePaiement[TYPE_PAIEMENT_LIBELLE];
	}

	public function getCommandesAssociees($paiementId) {
		return $this->commandeDAO->getByPaiementId($paiementId);
	}
}

?>