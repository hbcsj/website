<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");

$view = new View(
    "webapp/views/admin/shop/commandes/modals/paiements/", 
    "modal-paiements-commande", 
    "ModalPaiementsCommandeCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
    $commande = $ctrl->getCommande();
    $paiements = $ctrl->getPaiements();
    ?>
    <div class="modal-header">
        <div class="modal-title">Paiem<sup>ts</sup> pour la com<sup>de</sup> '<?php echo $commande[COMMANDE_REFERENCE]; ?>' (<?php echo $commande[COMMANDE_NOM]." ".$commande[COMMANDE_PRENOM]; ?>)</div>
    </div>
    <div class="modal-body">
        <table class="table table-striped table-condensed">
            <thead>
                <tr>
                    <th>Date</th>
                    <th class="text-center">Montant</th>
                    <th>Comm<sup>des</sup> assoc.</th>
                    <th>D&eacute;tails</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (sizeof($paiements) > 0) {
                    foreach ($paiements as $paiement) {
                        $commandesAssociees = $ctrl->getCommandesAssociees($paiement[PAIEMENT_ID]);
                        ?>
                        <tr>
                            <td><?php echo DateUtils::convert_sqlDate_to_slashDate($paiement[PAIEMENT_DATE]); ?></td>
                            <td class="text-center"><?php echo $paiement[PAIEMENT_MONTANT]; ?> &euro;</td>
                            <td>
                                <?php
                                if (sizeof($commandesAssociees) > 0) {
                                    $i = 0;
                                    foreach ($commandesAssociees as $commandeAssociee) {
                                        echo $commandeAssociee[COMMANDE_REFERENCE]." - ".$commandeAssociee[COMMANDE_NOM]." ".$commandeAssociee[COMMANDE_PRENOM];
                                        if ($i < (sizeof($commandesAssociees) - 1)) {
                                            echo "<br>";
                                        }
                                    }
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                echo "Type : ".$ctrl->getLibelleTypePaiement($paiement[PAIEMENT_TYPE_PAIEMENT_ID]);
                                if ($paiement[PAIEMENT_REFERENCE] != "") {
                                    echo "<br>R&eacute;f : ".$paiement[PAIEMENT_REFERENCE];
                                }
                                if ($paiement[PAIEMENT_BANQUE] != "") {
                                    echo "<br>Banque : ".$paiement[PAIEMENT_BANQUE];
                                }
                                if ($paiement[PAIEMENT_TITULAIRE_COMPTE] != "") {
                                    echo "<br>Titulaire : ".$paiement[PAIEMENT_TITULAIRE_COMPTE];
                                }
                                if ($paiement[PAIEMENT_COMMENTAIRE] != "") {
                                    echo "<br>Comm. : ".$paiement[PAIEMENT_COMMENTAIRE];
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="modal-footer"></div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>