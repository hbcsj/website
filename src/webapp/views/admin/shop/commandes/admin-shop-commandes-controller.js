AdminShopCommandesController = {};

AdminShopCommandesController.dateDebutRecherche = null;

AdminShopCommandesController.load_StatsCommandes = function() {
    if (Utils.exists('#container-stats-commandes')) {
        AjaxUtils.loadView(
            'admin/shop/commandes/views/stats/stats-commandes',
            '#container-stats-commandes',
            null,
            function() {},
            '#loader-page'
        );
    }
};

AdminShopCommandesController.load_RechercheAndTableCommandes = function() {
    if (Utils.exists('#container-recherche-commandes')) {
        AjaxUtils.loadView(
            'admin/shop/commandes/views/recherche/recherche-commandes',
            '#container-recherche-commandes',
            null,
            function() {
                $('#form-recherche-commandes__input-date-debut').val(AdminShopCommandesController.dateDebutRecherche);

                FormRechercheUtils.control_toggleFormRecherche();
                onClick_btRechercher();
                onClick_btReset();
                AdminShopCommandesController.load_TableCommandes();
            },
            '#loader-page'
        );
    }

    function onClick_btRechercher() {
        $('#recherche-commandes__bt-rechercher').unbind('click');
        $('#recherche-commandes__bt-rechercher').click(function() {
            AdminShopCommandesController.load_TableCommandes();
        });
    }

    function onClick_btReset() {
        $('#recherche-commandes__bt-reset').unbind('click');
        $('#recherche-commandes__bt-reset').click(function() {
            $('#form-recherche-commandes__input-reference').val('');
            $('#form-recherche-commandes__input-nom').val('');
            $('#form-recherche-commandes__input-date-debut').val(AdminShopCommandesController.dateDebutRecherche);
            $('#form-recherche-commandes__input-date-fin').val('');
            $('#form-recherche-commandes__input-saison').val('');
            $('#form-recherche-commandes__input-is-paye').val('');

            AdminShopCommandesController.load_TableCommandes();
        });
    }
};

AdminShopCommandesController.load_TableCommandes = function() {
    if (Utils.exists('#container-table-commandes')) {
        var params = {
            reference: $.trim($('#form-recherche-commandes__input-reference').val()),
            nom: $.trim($('#form-recherche-commandes__input-nom').find('option:selected').attr('value-nom')),
            prenom: $.trim($('#form-recherche-commandes__input-nom').find('option:selected').attr('value-prenom')),
            dateDebut: $.trim($('#form-recherche-commandes__input-date-debut').val()),
            dateFin: $.trim($('#form-recherche-commandes__input-date-fin').val()),
            saisonId: $.trim($('#form-recherche-commandes__input-saison').val()),
            paye: $('#form-recherche-commandes__input-is-paye').val()
        };
        AjaxUtils.loadView(
            'admin/shop/commandes/views/table/table-commandes',
            '#container-table-commandes',
            params,
            function() {
                control_LignesCommandesItems();
                control_CommandeItemPaye();
                control_PaiementsCommande();
                control_DeleteCommande();
            },
            '#loader-page'
        );
    }

    function control_LignesCommandesItems() {
        $('#container-table-commandes table tr.tr-header-commande td.clickable').unbind('click');
        $('#container-table-commandes table tr.tr-header-commande td.clickable').click(function() {
            var idCommande = $(this).parent().attr('id-commande');
            var trCommandesItemsSelector = $('#container-table-commandes table tr.tr-commande-item[id-commande='+idCommande+']');

            if (trCommandesItemsSelector.css('display') == 'none') {
                trCommandesItemsSelector.css('display', 'table-row');
            } else {
                trCommandesItemsSelector.css('display', 'none');
            }
        });
    }

    function control_CommandeItemPaye() {
        $('.bt-commande-paye').unbind('click');
        $('.bt-commande-paye').click(function() {
            var idCommande = $(this).attr('id-commande');
            
            $('.bt-commande-paye[id-commande='+idCommande+'] .loader').show();
            AjaxUtils.callService(
                'commande', 
                'togglePaye', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idCommande
                }, 
                function(response) {
                    $('.bt-commande-paye[id-commande='+idCommande+'] .loader').show();
                    if (response.success) {
                        AdminShopCommandesController.load_TableCommandes();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la modification du commande', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.bt-commande-paye[id-commande='+idCommande+'] .loader').show();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        });
    }

    function control_PaiementsCommande() {
        $('.bt-paiements-commande').unbind('click');
        $('.bt-paiements-commande').click(function() {
            var idCommande = $(this).attr('id-commande');

            var params = {
                idCommande: idCommande
            };
            AjaxUtils.loadView(
                'admin/shop/commandes/modals/paiements/modal-paiements-commande-content',
                '#modal-paiements-commande-content',
                params,
                function() {},
                '#loader-page'
            );
            $('#modal-paiements-commande').modal('show');
        });
    }

    function control_DeleteCommande() {
        $('.glyphicon-trash').unbind('click');
        $('.glyphicon-trash').click(function() {
            var idCommande = $(this).attr('id-commande');
            
            ViewUtils.prompt(
                'Supprimer cette commande ?', 
                {
                    btOkText: 'Oui',
                    btKoText: 'Non'
                },
                function() {
                    deleteCommande(idCommande);
                }, 
                null, 
                null
            );
        });

        function deleteCommande(idCommande) {
            $('.loader-delete[id-commande='+idCommande+']').show();
            AjaxUtils.callService(
                'commande', 
                'delete', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idCommande
                }, 
                function(response) {
                    $('.loader-delete[id-commande='+idCommande+']').hide();
                    if (response.success) {
                        AdminShopCommandesController.load_StatsCommandes();
                        AdminShopCommandesController.load_TableCommandes();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la suppression de la commande', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-delete[id-commande='+idCommande+']').hide();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        }
    }
};

initializePage = function() {
    AdminController.initializePage('shop', 'commandes');

    AjaxUtils.callService(
        'date', 
        'getSlashDateDebutSaison', 
        AjaxUtils.dataTypes.json, 
        AjaxUtils.requestTypes.get, 
        null, 
        null, 
        function(response) {
            AdminShopCommandesController.dateDebutRecherche = response.date;

            AdminShopCommandesController.load_StatsCommandes();
            AdminShopCommandesController.load_RechercheAndTableCommandes();
        },
        function(response) {}
    );
};