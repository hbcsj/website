AdminShopPaiementsController = {};

AdminShopPaiementsController.dateDebutRecherche = null;

AdminShopPaiementsController.load_RechercheAndTablePaiements = function() {
    if (Utils.exists('#container-recherche-paiements')) {
        AjaxUtils.loadView(
            'admin/shop/paiements/views/recherche/recherche-paiements',
            '#container-recherche-paiements',
            null,
            function() {
                $('#form-recherche-paiements__input-date-debut').val(AdminShopPaiementsController.dateDebutRecherche);

                FormRechercheUtils.control_toggleFormRecherche();
                onClick_btRechercher();
                onClick_btReset();
                AdminShopPaiementsController.load_TablePaiements();
            },
            '#loader-page'
        );
    }

    function onClick_btRechercher() {
        $('#recherche-paiements__bt-rechercher').unbind('click');
        $('#recherche-paiements__bt-rechercher').click(function() {
            AdminShopPaiementsController.load_TablePaiements();
        });
    }

    function onClick_btReset() {
        $('#recherche-paiements__bt-reset').unbind('click');
        $('#recherche-paiements__bt-reset').click(function() {
            $('#form-recherche-paiements__input-type').val('');
            $('#form-recherche-paiements__input-nom').val('');
            $('#form-recherche-paiements__input-reference-commande').val('');
            $('#form-recherche-paiements__input-nom-commande').val('');
            $('#form-recherche-paiements__input-date-debut').val(AdminShopPaiementsController.dateDebutRecherche);
            $('#form-recherche-paiements__input-date-fin').val('');
            $('#form-recherche-paiements__input-reference').val('');
            $('#form-recherche-paiements__input-commentaire').val('');
            $('#form-recherche-paiements__input-saison').val('');
            $('#form-recherche-paiements__input-montant').val('');

            AdminShopPaiementsController.load_TablePaiements();
        });
    }
};

AdminShopPaiementsController.load_TablePaiements = function() {
    if (Utils.exists('#container-table-paiements')) {
        var params = {
            typePaiementId: $.trim($('#form-recherche-paiements__input-type').val()),
            nom: $.trim($('#form-recherche-paiements__input-nom').val()),
            referenceCommande: $.trim($('#form-recherche-paiements__input-reference-commande').val()),
            nomCommande: $.trim($('#form-recherche-paiements__input-nom-commande').find('option:selected').attr('value-nom')),
            prenomCommande: $.trim($('#form-recherche-paiements__input-nom-commande').find('option:selected').attr('value-prenom')),
            dateDebut: $.trim($('#form-recherche-paiements__input-date-debut').val()),
            dateFin: $.trim($('#form-recherche-paiements__input-date-fin').val()),
            reference: $.trim($('#form-recherche-paiements__input-reference').val()),
            commentaire: $.trim($('#form-recherche-paiements__input-commentaire').val()),
            saisonId: $.trim($('#form-recherche-paiements__input-saison').val()),
            montant: $.trim($('#form-recherche-paiements__input-montant').val())
        };
        AjaxUtils.loadView(
            'admin/shop/paiements/views/table/table-paiements',
            '#container-table-paiements',
            params,
            function() {
                control_CommandePaiement();
                control_LinkCommandePaiement();
                control_DeleteLinkCommandePaiement();
                control_AddPaiement();
                control_EditPaiement();
                control_DeletePaiement();
            },
            '#loader-page'
        );
    }

    function control_CommandePaiement() {
        $('.bt-commande-paiement').unbind('click');
        $('.bt-commande-paiement').click(function() {
            var idCommande = $(this).attr('id-commande');

            var params = {
                idCommande: idCommande
            };
            AjaxUtils.loadView(
                'admin/shop/paiements/modals/commande/modal-commande-paiement-content',
                '#modal-commande-paiement-content',
                params,
                function() {},
                '#loader-page'
            );
            $('#modal-commande-paiement').modal('show');
        });
    }

    function control_LinkCommandePaiement() {
        $('.bt-link-commande-paiement').unbind('click');
        $('.bt-link-commande-paiement').click(function() {
            var idPaiement = $(this).attr('id-paiement');

            var params = {
                idPaiement: idPaiement
            };
            AjaxUtils.loadView(
                'admin/shop/paiements/modals/link-commande/modal-link-commande-paiement-content',
                '#modal-link-commande-paiement-content',
                params,
                function() {
                    onClick_btSauvegarder(idPaiement);
                },
                '#loader-page'
            );
            $('#modal-link-commande-paiement').modal('show');
        });

        function onClick_btSauvegarder(idPaiement) {
            $('#modal-link-commande-paiement__bt-sauvegarder').unbind('click');
            $('#modal-link-commande-paiement__bt-sauvegarder').click(function() {
                FormUtils.hideFormErrors('#modal-link-commande-paiement #form-link-commande-paiement');
                ViewUtils.unsetHTMLAndHide('#modal-link-commande-paiement .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('#modal-link-commande-paiement__bt-sauvegarder', '#modal-link-commande-paiement__bt-sauvegarder .loader');

                var formValidation = FormUtils.validateForm('#modal-link-commande-paiement #form-link-commande-paiement');
                
                if (formValidation) {
                    var inputCommande = $('#form-link-commande-paiement__input-commande').val();

                    saveLinkCommandePaiement(idPaiement, inputCommande);
                } else {
                    ViewUtils.activeButtonAndHideLoader('#modal-link-commande-paiement__bt-sauvegarder', '#modal-link-commande-paiement__bt-sauvegarder .loader');
                }
            });

            function saveLinkCommandePaiement(idPaiement, inputCommande) {
                AjaxUtils.callService(
                    'paiement', 
                    'saveLinkCommande', 
                    AjaxUtils.dataTypes.json, 
                    AjaxUtils.requestTypes.post, 
                    null, 
                    {
                        paiementId: idPaiement,
                        commandeId: inputCommande
                    }, 
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-link-commande-paiement .form-result-message', 'Paiement commande sauvegard&eacute;', FormUtils.resultsTypes.ok);
                            $('#modal-link-commande-paiement__bt-sauvegarder .loader').hide();
                            AdminShopPaiementsController.load_TablePaiements();
                        } else {
                            ViewUtils.activeButtonAndHideLoader('#modal-link-commande-paiement__bt-sauvegarder', '#modal-link-commande-paiement__bt-sauvegarder .loader');
                            FormUtils.displayFormResultMessage('#modal-link-commande-paiement .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('#modal-link-commande-paiement__bt-sauvegarder', '#modal-link-commande-paiement__bt-sauvegarder .loader');
                        FormUtils.displayFormResultMessage('#modal-link-commande-paiement .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            }
        }
    }

    function control_DeleteLinkCommandePaiement() {
        $('.bt-delete-link-commande-paiement').unbind('click');
        $('.bt-delete-link-commande-paiement').click(function() {
            var idPaiement = $(this).attr('id-paiement');
            var idCommande = $(this).attr('id-commande');

            $('.bt-delete-link-commande-paiement[id-commande='+idCommande+'][id-paiement='+idPaiement+'] .loader').show();
            AjaxUtils.callService(
                'paiement', 
                'deletePaiementCommande', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    paiementId: idPaiement,
                    commandeId: idCommande
                }, 
                function(response) {
                    $('.bt-delete-link-commande-paiement[id-commande='+idCommande+'][id-paiement='+idPaiement+'] .loader').show();
                    if (response.success) {
                        AdminShopPaiementsController.load_TablePaiements();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la suppression du lien paiement / commande', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.bt-delete-link-commande-paiement[id-commande='+idCommande+'][id-paiement='+idPaiement+'] .loader').show();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        });
    }

    function control_AddPaiement() {
        $('#paiements__bt-ajouter').unbind('click');
        $('#paiements__bt-ajouter').click(function() {
            open_ModalSavePaiement(null);
        });
    }

    function control_EditPaiement() {
        $('.glyphicon-pencil').unbind('click');
        $('.glyphicon-pencil').click(function() {
            var idPaiement = $(this).attr('id-paiement');
            open_ModalSavePaiement(idPaiement);
        });
    }

    function open_ModalSavePaiement(idPaiement) {
        var params = null;
        if (idPaiement != null) {
            params = {
                idPaiement: idPaiement
            };
        }
        AjaxUtils.loadView(
            'admin/shop/paiements/modals/save/modal-save-paiement-content',
            '#modal-save-paiement-content',
            params,
            function() {
                onClick_btSauvegarder(idPaiement);
            },
            '#loader-page'
        );
        $('#modal-save-paiement').modal('show');

        function onClick_btSauvegarder(idPaiement) {
            $('#modal-save-paiement__bt-sauvegarder').unbind('click');
            $('#modal-save-paiement__bt-sauvegarder').click(function() {
                FormUtils.hideFormErrors('#modal-save-paiement #form-save-paiement');
                ViewUtils.unsetHTMLAndHide('#modal-save-paiement .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('#modal-save-paiement__bt-sauvegarder', '#modal-save-paiement__bt-sauvegarder .loader');

                var formValidation = FormUtils.validateForm('#modal-save-paiement #form-save-paiement');
                
                if (formValidation) {
                    var inputMontant = $.trim($('#form-save-paiement__input-montant').val());
                    var inputDate = $.trim($('#form-save-paiement__input-date').val());
                    var inputType = $('#form-save-paiement__input-type').val();
                    var inputReference = $.trim($('#form-save-paiement__input-reference').val());
                    var inputTitulaireCompte = $.trim($('#form-save-paiement__input-titulaire-compte').val());
                    var inputBanque = $.trim($('#form-save-paiement__input-banque').val());
                    var inputCommentaire = $.trim($('#form-save-paiement__input-commentaire').val());

                    savePaiement(idPaiement, inputMontant, inputDate, inputType, inputReference, inputTitulaireCompte, inputBanque, inputCommentaire);
                } else {
                    ViewUtils.activeButtonAndHideLoader('#modal-save-paiement__bt-sauvegarder', '#modal-save-paiement__bt-sauvegarder .loader');
                }
            });

            function savePaiement(idPaiement, inputMontant, inputDate, inputType, inputReference, inputTitulaireCompte, inputBanque, inputCommentaire) {
                AjaxUtils.callService(
                    'paiement', 
                    'save', 
                    AjaxUtils.dataTypes.json, 
                    AjaxUtils.requestTypes.post, 
                    {
                        id: idPaiement
                    }, 
                    {
                        montant: inputMontant,
                        date: inputDate,
                        typePaiementId: inputType,
                        reference: inputReference,
                        titulaireCompte: inputTitulaireCompte,
                        banque: inputBanque,
                        commentaire: inputCommentaire
                    }, 
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-save-paiement .form-result-message', 'Paiement sauvegard&eacute;', FormUtils.resultsTypes.ok);
                            $('#modal-save-paiement__bt-sauvegarder .loader').hide();
                            AdminShopPaiementsController.load_TablePaiements();
                        } else {
                            ViewUtils.activeButtonAndHideLoader('#modal-save-paiement__bt-sauvegarder', '#modal-save-paiement__bt-sauvegarder .loader');
                            FormUtils.displayFormResultMessage('#modal-save-paiement .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('#modal-save-paiement__bt-sauvegarder', '#modal-save-paiement__bt-sauvegarder .loader');
                        FormUtils.displayFormResultMessage('#modal-save-paiement .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            }
        }
    }

    function control_DeletePaiement() {
        $('.glyphicon-trash').unbind('click');
        $('.glyphicon-trash').click(function() {
            var idPaiement = $(this).attr('id-paiement');
            
            ViewUtils.prompt(
                'Supprimer ce paiement ?', 
                {
                    btOkText: 'Oui',
                    btKoText: 'Non'
                },
                function() {
                    deletePaiement(idPaiement);
                }, 
                null, 
                null
            );
        });

        function deletePaiement(idPaiement) {
            $('.loader-delete[id-paiement='+idPaiement+']').show();
            AjaxUtils.callService(
                'paiement', 
                'delete', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idPaiement
                }, 
                function(response) {
                    $('.loader-delete[id-paiement='+idPaiement+']').hide();
                    if (response.success) {
                        AdminShopPaiementsController.load_TablePaiements();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la suppression du paiement', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-delete[id-paiement='+idPaiement+']').hide();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        }
    }
};

initializePage = function() {
    AdminController.initializePage('shop', 'paiements');

    AjaxUtils.callService(
        'date', 
        'getSlashDateDebutSaison', 
        AjaxUtils.dataTypes.json, 
        AjaxUtils.requestTypes.get, 
        null, 
        null, 
        function(response) {
            AdminShopPaiementsController.dateDebutRecherche = response.date;

            AdminShopPaiementsController.load_RechercheAndTablePaiements();
        },
        function(response) {}
    );
};