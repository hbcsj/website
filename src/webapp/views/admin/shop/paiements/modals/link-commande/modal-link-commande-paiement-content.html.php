<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");

$view = new View(
    "webapp/views/admin/shop/paiements/modals/link-commande/", 
    "modal-link-commande-paiement", 
    "ModalLinkCommandePaiementCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
    $paiement = $ctrl->getPaiement();
    $commandes = $ctrl->getCommandes();
    ?>
    <div class="modal-header">
        <div class="modal-title">Associer une commande au paiement du <?php echo DateUtils::convert_sqlDate_to_slashDate($paiement[PAIEMENT_DATE]); ?> (<?php echo $paiement[PAIEMENT_MONTANT]; ?> &euro;)</div>
    </div>
    <div class="modal-body">
        <div class="form-horizontal" id="form-link-commande-paiement">
            <div class="form-group">
                <label for="form-link-commande-paiement__input-commande" class="col-xs-4 control-label required">Commande :</label>
                <div class="col-xs-7 form-input" for="form-link-commande-paiement__input-commande">
                    <select class="form-control" id="form-link-commande-paiement__input-commande" required>
                        <option value=""></option>
                        <?php
                        if (sizeof($commandes) > 0) {
                            foreach ($commandes as $commande) {
                                ?>
                                <option value="<?php echo $commande[COMMANDE_ID]; ?>"><?php echo $commande[COMMANDE_REFERENCE] ?> - <?php echo str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDateTime_to_slashDateTime($commande[COMMANDE_DATE_HEURE])); ?> - <?php echo $commande[COMMANDE_NOM]." ".$commande[COMMANDE_PRENOM]; ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-link-commande-paiement__input-commande"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-required text-right">Les champs marqu&eacute;s d'une * sont obligatoires</div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-8">
                <div class="form-result-message"></div>
            </div>
            <div class="col-xs-4 text-right">
                <button id="modal-link-commande-paiement__bt-sauvegarder" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-ok"></span>
                    </div>
                    <div class="button__text">Sauvegarder</div>
                    <div class="loader"></div>
                </button>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>