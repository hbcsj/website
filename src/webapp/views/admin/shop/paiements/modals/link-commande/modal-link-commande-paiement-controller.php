<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/commande-dao.php");
require_once("common/php/dao/paiement-dao.php");

class ModalLinkCommandePaiementCtrl extends AbstractViewCtrl {
	
	private $commandeDAO;
	private $paiementDAO;

	private $paiement;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idPaiement" => $_GET["idPaiement"]
		), true);
		
		if (isAdminConnected_commercial()) {
			$this->paiementDAO = new PaiementDAO($this->getDatabaseConnection());
			$this->commandeDAO = new CommandeDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdPaiement();
	}

	private function checkIdPaiement() {
		$this->paiement = $this->paiementDAO->getById($_GET["idPaiement"]);

		if ($this->paiement == null) {
			$this->sendCheckError(
				HTTP_404, 
				"Le paiement (idPaiement = '".$_GET["idPaiement"]."') n'existe pas", 
				"webapp/views/common/error/404/404.html.php"
			);
		}
	}

	public function getCommandes() {
		return $this->commandeDAO->getNotLinkedToPaiementByPaiementId($this->paiement[PAIEMENT_ID], COMMANDE_DATE_HEURE." DESC");
	}

	public function getPaiement() {
		return $this->paiement;
	}
}

?>