<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");

$view = new View(
    "webapp/views/admin/shop/paiements/modals/commande/", 
    "modal-commande-paiement", 
    "ModalCommandePaiementCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
    $commande = $ctrl->getCommande();
    ?>
    <div class="modal-header">
        <div class="modal-title">Commande '<?php echo $commande[COMMANDE_REFERENCE]; ?>' (<?php echo $commande[COMMANDE_NOM]." ".$commande[COMMANDE_PRENOM]; ?>)</div>
    </div>
    <div class="modal-body">
        <table class="table table-condensed">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Nom</th>
                    <th>Coord.</th>
                    <th>Produit</th>
                    <th class="text-center">Quantit&eacute;</th>
                    <th class="text-center">Montant</th>
                </tr>
            </thead>
            <tbody>
                <tr class="tr-header-commande">
                    <td><?php echo str_replace(SLASH_DATE_TIME_SEPARATOR, "<br>", DateUtils::convert_sqlDateTime_to_slashDateTime($commande[COMMANDE_DATE_HEURE])); ?></td>
                    <td><?php echo $commande[COMMANDE_NOM]."<br>".$commande[COMMANDE_PRENOM]; ?></td>
                    <td>
                        @ : <a href="mailto:<?php echo $commande[COMMANDE_EMAIL]; ?>"><?php echo $commande[COMMANDE_EMAIL]; ?></a>
                        <br>
                        T&eacute;l : <a href="tel:<?php echo str_replace(".", "", $commande[COMMANDE_TELEPHONE]); ?>"><?php echo $commande[COMMANDE_TELEPHONE]; ?></a>
                    </td>
                    <td><?php echo $commande[COMMANDE_REFERENCE]; ?></td>
                    <td>&nbsp;</td>
                    <td class="text-center"><?php echo $commande[TABLE_COMMANDES_MONTANT]; ?> &euro;</td>
                </tr>
                <?php
                foreach ($commande[COMMANDES_ITEMS] as $commandeItem) {
                    if (array_key_exists(COMMANDE_ITEM_BOUTIQUE_TAILLE, $commandeItem)) {
                        $produit = $ctrl->getProduit($commandeItem[COMMANDE_ITEM_BOUTIQUE_PRODUIT_BOUTIQUE_ID]);
                        ?>
                        <tr class="tr-commande-item" id-commande="<?php echo $commande[COMMANDE_ID]; ?>">
                            <td colspan="3">&nbsp;</td>
                            <td>
                                <?php 
                                echo $produit[PRODUIT_BOUTIQUE_NOM]; 
                                if ($produit[PRODUIT_BOUTIQUE_INDICATION] != "") {
                                    echo " - ".$produit[PRODUIT_BOUTIQUE_INDICATION];
                                }
                                if ($commandeItem[COMMANDE_ITEM_BOUTIQUE_TAILLE] != "") {
                                    echo "<br>Taille ".$commandeItem[COMMANDE_ITEM_BOUTIQUE_TAILLE];
                                }
                                if ($commandeItem[COMMANDE_ITEM_BOUTIQUE_COULEUR] != "") {
                                    echo "<br>".$commandeItem[COMMANDE_ITEM_BOUTIQUE_COULEUR];
                                }
                                if ($commandeItem[COMMANDE_ITEM_BOUTIQUE_FLOCAGE_NOM] != "") {
                                    echo "<br>Flocage nom : ".$commandeItem[COMMANDE_ITEM_BOUTIQUE_FLOCAGE_NOM];
                                }
                                if ($commandeItem[COMMANDE_ITEM_BOUTIQUE_FLOCAGE_NUMERO] != "") {
                                    echo "<br>Flocage num&eacute;ro : ".$commandeItem[COMMANDE_ITEM_BOUTIQUE_FLOCAGE_NUMERO];
                                }
                                if ($commandeItem[COMMANDE_ITEM_BOUTIQUE_FLOCAGE_INITIALES] != "") {
                                    echo "<br>Flocage initiales : ".$commandeItem[COMMANDE_ITEM_BOUTIQUE_FLOCAGE_INITIALES];
                                }
                                ?>
                            </td>
                            <td class="text-center"><?php echo $commandeItem[COMMANDE_ITEM_BOUTIQUE_QUANTITE]; ?></td>
                            <td class="text-center"><?php echo $commandeItem[COMMANDE_ITEM_BOUTIQUE_PRIX]; ?> &euro;</td>
                        </tr>
                        <?php
                    } else {
                        $match = $ctrl->getMatch($commandeItem[COMMANDE_ITEM_BILLETTERIE_MATCH_BILLETTERIE_ID]);
                        ?>
                        <tr class="tr-commande-item" id-commande="<?php echo $commande[COMMANDE_ID]; ?>">
                            <td colspan="3">&nbsp;</td>
                            <td>
                                Fenix VS <?php echo $ctrl->getNomAdversaire($match[MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID]); ?>
                                <br>
                                <?php echo $ctrl->getNomCompetition($match[MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID]); ?>
                                <br>
                                <?php echo DateUtils::convert_sqlDateTime_to_slashDateTime($match[MATCH_BILLETTERIE_DATE_HEURE]); ?>
                            </td>
                            <td class="text-center"><?php echo $commandeItem[COMMANDE_ITEM_BILLETTERIE_QUANTITE]; ?></td>
                            <td class="text-center"><?php echo $commandeItem[COMMANDE_ITEM_BILLETTERIE_PRIX]; ?> &euro;</td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="modal-footer"></div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>