<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/commande-dao.php");
require_once("common/php/dao/commande-item-boutique-dao.php");
require_once("common/php/dao/commande-item-billetterie-dao.php");
require_once("common/php/dao/adversaire-billetterie-dao.php");
require_once("common/php/dao/competition-billetterie-dao.php");
require_once("common/php/dao/match-billetterie-dao.php");
require_once("common/php/dao/produit-boutique-dao.php");
require_once("common/php/dao/categorie-produit-boutique-dao.php");
require_once("common/php/dao/marque-produit-boutique-dao.php");

class ModalCommandePaiementCtrl extends AbstractViewCtrl {
	
	private $commandeDAO;
	private $commandeItemBoutiqueDAO;
	private $commandeItemBilletterieDAO;
	private $adversaireBilletterieDAO;
	private $competitionBilletterieDAO;
	private $matchBilletterieDAO;
	private $produitBoutiqueDAO;
	private $categorieProduitBoutiqueDAO;
	private $marqueProduitBoutiqueDAO;

	private $paiement;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idCommande" => $_GET["idCommande"]
		), true);
		
		if (isAdminConnected_commercial()) {
			$this->commandeDAO = new CommandeDAO($this->getDatabaseConnection());
			$this->commandeItemBoutiqueDAO = new CommandeItemBoutiqueDAO($this->getDatabaseConnection());
			$this->commandeItemBilletterieDAO = new CommandeItemBilletterieDAO($this->getDatabaseConnection());
			$this->adversaireBilletterieDAO = new AdversaireBilletterieDAO($this->getDatabaseConnection());
			$this->competitionBilletterieDAO = new CompetitionBilletterieDAO($this->getDatabaseConnection());
			$this->matchBilletterieDAO = new MatchBilletterieDAO($this->getDatabaseConnection());
			$this->produitBoutiqueDAO = new ProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->categorieProduitBoutiqueDAO = new CategorieProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->marqueProduitBoutiqueDAO = new MarqueProduitBoutiqueDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdCommande();
	}

	private function checkIdCommande() {
		$this->commande = $this->commandeDAO->getById($_GET["idCommande"]);

		if ($this->commande == null) {
			$this->sendCheckError(
				HTTP_404, 
				"La commande (idCommande = '".$_GET["idCommande"]."') n'existe pas", 
				"webapp/views/common/error/404/404.html.php"
			);
		}
	}

	public function getCommande() {
		$totalMontantCommande = 0;
		$totalQuantiteCommande = 0;
		$this->commande[COMMANDES_ITEMS] = array();

		$commandeItemsBoutique = $this->commandeItemBoutiqueDAO->getByCommandeId($this->commande[COMMANDE_ID]);
		if (sizeof($commandeItemsBoutique) > 0) {
			foreach ($commandeItemsBoutique as $commandeItemBoutique) {
				$totalQuantiteCommande += $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_QUANTITE];
				$totalMontantCommande += $commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_PRIX];
				$this->commande[COMMANDES_ITEMS][] = $commandeItemBoutique;
			}
		}

		$commandeItemsBilletterie = $this->commandeItemBilletterieDAO->getByCommandeId($this->commande[COMMANDE_ID]);
		if (sizeof($commandeItemsBilletterie) > 0) {
			foreach ($commandeItemsBilletterie as $commandeItemBilletterie) {
				$totalQuantiteCommande += $commandeItemBilletterie[COMMANDE_ITEM_BILLETTERIE_QUANTITE];
				$totalMontantCommande += $commandeItemBilletterie[COMMANDE_ITEM_BILLETTERIE_PRIX];
				$this->commande[COMMANDES_ITEMS][] = $commandeItemBilletterie;
			}
		}
		
		$this->commande[TABLE_COMMANDES_MONTANT] = $totalMontantCommande;
		$this->commande[TABLE_COMMANDES_QUANTITE] = $totalQuantiteCommande;

		return $this->commande;
	}

	public function getProduit($produitId) {
		return $this->produitBoutiqueDAO->getById($produitId);
	}

	public function getNomAdversaire($adversaireId) {
		$adversaire = $this->adversaireBilletterieDAO->getById($adversaireId);
		return $adversaire[ADVERSAIRE_BILLETTERIE_NOM];
	}

	public function getNomCompetition($competitionId) {
		$competition = $this->competitionBilletterieDAO->getById($competitionId);
		return $competition[COMPETITION_BILLETTERIE_NOM];
	}

	public function getMatch($matchId) {
		return $this->matchBilletterieDAO->getById($matchId);
	}
}

?>