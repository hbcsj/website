<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/dao/paiement-dao.php");
require_once("common/php/dao/type-paiement-dao.php");

class ModalSavePaiementCtrl extends AbstractViewCtrl {
	
	private $paiement;

	private $paiementDAO;
	private $typePaiementDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idPaiement" => $_GET["idPaiement"]
		), true);
		
		if (isAdminConnected_commercial()) {
			$this->paiementDAO = new PaiementDAO($this->getDatabaseConnection());
			$this->typePaiementDAO = new TypePaiementDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdPaiement();
	}

	private function checkIdPaiement() {
		if (HTTPUtils::paramExists(GET, "idPaiement")) {
			$this->paiement = $this->paiementDAO->getById($_GET["idPaiement"]);
				
			if ($this->paiement == null) {
				$this->sendCheckError(
					HTTP_404, 
					"Le paiement (idPaiement = '".$_GET["idPaiement"]."') n'existe pas", 
					"webapp/views/common/error/404/404.html.php"
				);
			}
		}
	}

	public function getTitle() {
		$title = "Nouveau paiement";
		if ($this->paiement != null) {
			$title = "Paiement du ".DateUtils::convert_sqlDate_to_slashDate($this->paiement[PAIEMENT_DATE])." (".$this->paiement[PAIEMENT_MONTANT]." &euro;) - <span class=\"normal\">Edition</span>";
		}
		return $title;
	}

	public function getMontantPaiement() {
		if ($this->paiement != null) {
			return $this->paiement[PAIEMENT_MONTANT];
		}
		return "";
	}

	public function getDatePaiement() {
		if ($this->paiement != null) {
			return DateUtils::convert_sqlDate_to_slashDate($this->paiement[PAIEMENT_DATE]);
		}
		return date(SLASH_DATE_FORMAT);
	}

	public function getReferencePaiement() {
		if ($this->paiement != null) {
			return $this->paiement[PAIEMENT_REFERENCE];
		}
		return "";
	}

	public function getTitulaireComptePaiement() {
		if ($this->paiement != null) {
			return $this->paiement[PAIEMENT_TITULAIRE_COMPTE];
		}
		return "";
	}

	public function getBanquePaiement() {
		if ($this->paiement != null) {
			return $this->paiement[PAIEMENT_BANQUE];
		}
		return "";
	}

	public function getCommentairePaiement() {
		if ($this->paiement != null) {
			return $this->paiement[PAIEMENT_COMMENTAIRE];
		}
		return "";
	}

	public function getOptionsTypePaiement() {
		$html = "";

		$types = $this->typePaiementDAO->getAll();
		if (sizeof($types) > 0) {
			foreach ($types as $type) {
				$selected = "";
				if ($this->paiement[PAIEMENT_TYPE_PAIEMENT_ID] == $type[TYPE_PAIEMENT_ID]) {
					$selected = " selected";
				}
				$html .= "<option value=\"".$type[TYPE_PAIEMENT_ID]."\"".$selected.">".$type[TYPE_PAIEMENT_LIBELLE]."</option>";
			}
		}

		return $html;
	}
}

?>