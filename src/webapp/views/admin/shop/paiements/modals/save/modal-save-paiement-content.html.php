<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/shop/paiements/modals/save/", 
    "modal-save-paiement", 
    "ModalSavePaiementCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
?>
    <div class="modal-header">
        <div class="modal-title"><?php echo $ctrl->getTitle(); ?></div>
    </div>
    <div class="modal-body">
        <div class="form-horizontal" id="form-save-paiement">
            <div class="form-group">
                <label for="form-save-paiement__input-montant" class="col-xs-4 control-label required">Montant :</label>
                <div class="col-xs-2 form-input" for="form-save-paiement__input-montant">
                    <div class="input-group">
                        <input type="text" class="form-control" id="form-save-paiement__input-montant" value="<?php echo $ctrl->getMontantPaiement(); ?>" pattern="^[0-9]+(\.)?[0-9]*$" pattern-indication="Le prix doit &ecirc;tre un nombre d&eacute;cimal" required>
                        <span class="input-group-addon">&euro;</span>
                    </div>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-paiement__input-montant"></div>
                <label for="form-save-paiement__input-date" class="col-xs-1 control-label required">Date :</label>
                <div class="col-xs-3 form-input" for="form-save-paiement__input-date">
                    <div class="input-group">
                        <input type="text" class="form-control" id="form-save-paiement__input-date" value="<?php echo $ctrl->getDatePaiement(); ?>" pattern="^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$" pattern-indication="Le format doit &ecirc;tre 'jj/mm/aaaa'" required>
                        <span class="input-group-addon">
                            <img src="<?php echo SRC_PATH; ?>assets/img/icons/calendar.png">
                        </span>
                    </div>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-paiement__input-date"></div>
            </div>
            <div class="form-group">
                <label for="form-save-paiement__input-titulaire-type" class="col-xs-4 control-label required">Type :</label>
                <div class="col-xs-7 form-input" for="form-save-paiement__input-titulaire-type">
                    <select class="form-control" id="form-save-paiement__input-type" required>
                        <option value=""></option>
                        <?php echo $ctrl->getOptionsTypePaiement(); ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-paiement__input-type"></div>
            </div>
            <div class="form-group">
                <label for="form-save-paiement__input-reference" class="col-xs-4 control-label">R&eacute;f. :</label>
                <div class="col-xs-7 form-input" for="form-save-paiement__input-reference">
                    <input type="text" class="form-control" id="form-save-paiement__input-reference" value="<?php echo $ctrl->getReferencePaiement(); ?>">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-paiement__input-reference"></div>
            </div>
            <div class="form-group">
                <label for="form-save-paiement__input-titulaire-compte" class="col-xs-4 control-label">Tit. compte :</label>
                <div class="col-xs-7 form-input" for="form-save-paiement__input-titulaire-compte">
                    <input type="text" class="form-control" id="form-save-paiement__input-titulaire-compte" value="<?php echo $ctrl->getTitulaireComptePaiement(); ?>">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-paiement__input-titulaire-compte"></div>
            </div>
            <div class="form-group">
                <label for="form-save-paiement__input-banque" class="col-xs-4 control-label">Banque :</label>
                <div class="col-xs-7 form-input" for="form-save-paiement__input-banque">
                    <input type="text" class="form-control" id="form-save-paiement__input-banque" value="<?php echo $ctrl->getBanquePaiement(); ?>">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-paiement__input-banque"></div>
            </div>
            <div class="form-group">
                <label for="form-save-paiement__input-commentaire" class="col-xs-4 control-label">Commentaire :</label>
                <div class="col-xs-7 form-input" for="form-save-paiement__input-commentaire">
                    <textarea class="form-control" id="form-save-paiement__input-commentaire" rows="3"><?php echo $ctrl->getCommentairePaiement(); ?></textarea>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-paiement__input-commentaire"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-required text-right">Les champs marqu&eacute;s d'une * sont obligatoires</div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-8">
                <div class="form-result-message"></div>
            </div>
            <div class="col-xs-4 text-right">
                <button id="modal-save-paiement__bt-sauvegarder" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-ok"></span>
                    </div>
                    <div class="button__text">Sauvegarder</div>
                    <div class="loader"></div>
                </button>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>