<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");

$view = new View(
    "webapp/views/admin/shop/paiements/views/table/", 
    "table-paiements", 
    "TablePaiementsCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
    $paiements = $ctrl->getPaiements();
    ?>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Date</th>
                <th class="text-center">Montant</th>
                <th>Comm<sup>des</sup> assoc.</th>
                <th>D&eacute;tails</th>
                <th colspan="2">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (sizeof($paiements) > 0) {
                foreach ($paiements as $paiement) {
                    $commandes = $ctrl->getCommandesAssociees($paiement[PAIEMENT_ID]);
                    ?>
                    <tr>
                        <td><?php echo DateUtils::convert_sqlDate_to_slashDate($paiement[PAIEMENT_DATE]); ?></td>
                        <td class="text-center"><?php echo $paiement[PAIEMENT_MONTANT]; ?> &euro;</td>
                        <td>
                            <?php
                            if (sizeof($commandes) > 0) {
                                foreach ($commandes as $commande) {
                                    ?>
                                    <div class="row row-commande">
                                        <div class="col-xs-5">
                                            <button class="btn btn-default bt-table-admin bt-commande-paiement" id-commande="<?php echo $commande[COMMANDE_ID]; ?>">
                                                <div class="button__text"><?php echo $commande[COMMANDE_REFERENCE]."<br>".$commande[COMMANDE_NOM]." ".$commande[COMMANDE_PRENOM]; ?></div>
                                            </button>
                                        </div>
                                        <div class="col-xs-7">
                                            <button class="btn btn-default bt-table-admin bt-delete-link-commande-paiement" id-paiement="<?php echo $paiement[PAIEMENT_ID]; ?>" id-commande="<?php echo $commande[COMMANDE_ID]; ?>">
                                                <div class="button__icon">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </div>
                                                <div class="button__text">Sup.</div>
                                                <div class="loader"></div>
                                            </button>
                                        </div>
                                    </div>
                                    <?php
                                }
                                echo "<br>";
                            }
                            ?>
                            <button class="btn btn-default bt-table-admin bt-link-commande-paiement" id-paiement="<?php echo $paiement[PAIEMENT_ID]; ?>">
                                <div class="button__text">Associer une autre commande</div>
                            </button>
                        </td>
                        <td>
                            <?php
                            echo "Type : ".$ctrl->getLibelleTypePaiement($paiement[PAIEMENT_TYPE_PAIEMENT_ID]);
                            if ($paiement[PAIEMENT_REFERENCE] != "") {
                                echo "<br>R&eacute;f : ".$paiement[PAIEMENT_REFERENCE];
                            }
                            if ($paiement[PAIEMENT_BANQUE] != "") {
                                echo "<br>Banque : ".$paiement[PAIEMENT_BANQUE];
                            }
                            if ($paiement[PAIEMENT_TITULAIRE_COMPTE] != "") {
                                echo "<br>Titulaire : ".$paiement[PAIEMENT_TITULAIRE_COMPTE];
                            }
                            if ($paiement[PAIEMENT_COMMENTAIRE] != "") {
                                echo "<br>Comm. : ".$paiement[PAIEMENT_COMMENTAIRE];
                            }
                            ?>
                        </td>
                        <td class="text-center">
                            <span class="glyphicon glyphicon-pencil clickable" title="Editer" id-paiement="<?php echo $paiement[PAIEMENT_ID]; ?>"></span>
                        </td>
                        <td class="text-center">
                            <?php
                            if (sizeof($commandes) == 0) {
                            ?>
                                <span class="glyphicon glyphicon-trash clickable" title="Supprimer" id-paiement="<?php echo $paiement[PAIEMENT_ID]; ?>"></span>
                                <div class="loader loader-delete" id-paiement="<?php echo $paiement[PAIEMENT_ID]; ?>"></div>
                            <?php
                            } else {
                                echo "&nbsp;";
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>