<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/paiement-dao.php");
require_once("common/php/dao/type-paiement-dao.php");
require_once("common/php/dao/commande-dao.php");
require_once("common/php/lib/date-utils.php");

class TablePaiementsCtrl extends AbstractViewCtrl {

	private $paiementDAO;
	private $typePaiementDAO;
	private $commandeDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_commercial()) {
			$this->paiementDAO = new PaiementDAO($this->getDatabaseConnection());
			$this->typePaiementDAO = new TypePaiementDAO($this->getDatabaseConnection());
			$this->commandeDAO = new CommandeDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getPaiements() {
		return $this->paiementDAO->getAllByParams(
			$_GET["typePaiementId"], 
			trim($_GET["nom"]),
			trim($_GET["referenceCommande"]), 
			trim($_GET["nomCommande"]), 
			trim($_GET["prenomCommande"]), 
			DateUtils::convert_slashDate_to_sqlDate($_GET["dateDebut"]), 
			DateUtils::convert_slashDate_to_sqlDate($_GET["dateFin"]), 
			trim($_GET["reference"]),
			trim($_GET["commentaire"]),
			trim($_GET["montant"]),
			$_GET["saisonId"],
			PAIEMENT_TABLE_NAME.".".PAIEMENT_DATE." DESC"
		);
	}

	public function getLibelleTypePaiement($typePaiementId) {
		$typePaiement = $this->typePaiementDAO->getById($typePaiementId);
		return $typePaiement[TYPE_PAIEMENT_LIBELLE];
	}

	public function getCommandesAssociees($paiementId) {
		return $this->commandeDAO->getByPaiementId($paiementId);
	}
}

?>