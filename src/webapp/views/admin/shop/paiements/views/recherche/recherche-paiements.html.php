<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");

$view = new View(
    "webapp/views/admin/shop/paiements/views/recherche/", 
    "recherche-paiements", 
    "RecherchePaiementsCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
    $types = $ctrl->getTypesPaiements();
    $saisons = $ctrl->getSaisons();
    $personnesCommandes = $ctrl->getPersonnesCommandes();
    ?>
    <div class="container-recherche__title" title="Ouvrir le formulaire de recherche" for="form-recherche-paiements">
        <div class="container-recherche__title__icon">
            <span class="glyphicon glyphicon-search"></span>
        </div>
        <div class="container-recherche__title__text">Recherche</div>
        <div class="container-recherche__title__arrow">
            <span class="glyphicon glyphicon-triangle-bottom"></span>
        </div>
    </div>
    <div class="form-horizontal" id="form-recherche-paiements">
        <div class="form-group">
            <label for="form-recherche-paiements__input-type" class="col-xs-2 col-xs-offset-1 control-label">Type :</label>
            <div class="col-xs-3 form-input" for="form-recherche-paiements__input-type">
                <select class="form-control" id="form-recherche-paiements__input-type">
                    <option value=""></option>
                    <?php
                    if (sizeof($types) > 0) {
                        foreach ($types as $type) {
                            ?>
                            <option value="<?php echo $type[TYPE_PAIEMENT_ID]; ?>"><?php echo $type[TYPE_PAIEMENT_LIBELLE]; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <label for="form-recherche-paiements__input-nom" class="col-xs-1 control-label">Nom :</label>
            <div class="col-xs-3 form-input" for="form-recherche-paiements__input-nom">
                <input class="form-control" id="form-recherche-paiements__input-nom">
            </div>
        </div>
        <div class="form-group">
            <label for="form-recherche-paiements__input-reference-commande" class="col-xs-2 col-xs-offset-1 control-label">R&eacute;f com<sup>de</sup> :</label>
            <div class="col-xs-3 form-input" for="form-recherche-paiements__input-reference-commande">
                <input class="form-control" id="form-recherche-paiements__input-reference-commande">
            </div>
            <label for="form-recherche-paiements__input-nom-commande" class="col-xs-1 control-label">Nom com<sup>de</sup> :</label>
            <div class="col-xs-3 form-input" for="form-recherche-paiements__input-nom-commande">
                <select class="form-control" id="form-recherche-paiements__input-nom-commande">
                    <option value=""></option>
                    <?php
                    if (sizeof($personnesCommandes) > 0) {
                        foreach ($personnesCommandes as $personneCommande) {
                            ?>
                            <option value-nom="<?php echo $personneCommande[COMMANDE_NOM]; ?>" value-prenom="<?php echo $personneCommande[COMMANDE_PRENOM]; ?>" value="<?php echo $personneCommande[COMMANDE_NOM]." ".$personneCommande[COMMANDE_PRENOM]; ?>"><?php echo $personneCommande[COMMANDE_NOM]." ".$personneCommande[COMMANDE_PRENOM]; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="form-recherche-paiements__input-date-debut" class="col-xs-2 col-xs-offset-1 control-label">Date paiement :</label>
            <div class="col-xs-2 form-input" for="form-recherche-paiements__input-date-debut">
                <div class="input-group">
                    <input type="text" class="form-control" id="form-recherche-paiements__input-date-debut" placeholder="D&eacute;but">
                    <span class="input-group-addon">
                        <img src="<?php echo SRC_PATH; ?>assets/img/icons/calendar.png">
                    </span>
                </div>
            </div>
            <label for="form-recherche-paiements__input-reference" class="col-xs-2 control-label">R&eacute;f :</label>
            <div class="col-xs-2 form-input" for="form-recherche-paiements__input-reference">
                <input type="text" class="form-control" id="form-recherche-paiements__input-reference">
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-2 col-xs-offset-3 form-input" for="form-recherche-paiements__input-date-fin">
                <div class="input-group">
                    <input type="text" class="form-control" id="form-recherche-paiements__input-date-fin" placeholder="Fin">
                    <span class="input-group-addon">
                        <img src="<?php echo SRC_PATH; ?>assets/img/icons/calendar.png">
                    </span>
                </div>
            </div>
            <label for="form-recherche-paiements__input-montant" class="col-xs-2 control-label">Comment. :</label>
            <div class="col-xs-3 form-input" for="form-recherche-paiements__input-commentaire">
                <input type="text" class="form-control" id="form-recherche-paiements__input-commentaire">
            </div>
        </div>
        <div class="form-group">
            <label for="form-recherche-paiements__input-saison" class="col-xs-2 col-xs-offset-1 control-label">Saison :</label>
            <div class="col-xs-2 form-input" for="form-recherche-paiements__input-saison">
                <select class="form-control" id="form-recherche-paiements__input-saison">
                    <option value=""></option>
                    <?php
                    if (sizeof($saisons) > 0) {
                        foreach ($saisons as $saison) {
                            ?>
                            <option value="<?php echo $saison[SAISON_ID]; ?>"><?php echo $saison[SAISON_LIBELLE]; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <label for="form-recherche-paiements__input-montant" class="col-xs-2 control-label">Montant :</label>
            <div class="col-xs-2 form-input" for="form-recherche-paiements__input-montant">
                <div class="input-group">
                    <input type="text" class="form-control" id="form-recherche-paiements__input-montant">
                    <span class="input-group-addon">&euro;</span>
                </div>
            </div>
        </div>
    </div>
    <div class="container-recherche__footer" for="form-recherche-paiements">
        <button id="recherche-paiements__bt-rechercher" class="btn btn-default">
            <div class="button__icon">
                <span class="glyphicon glyphicon-search"></span>
            </div>
            <div class="button__text">Chercher</div>
        </button>
        <button id="recherche-paiements__bt-reset" class="btn btn-default">
            <div class="button__icon">
                <span class="glyphicon glyphicon-erase"></span>
            </div>
            <div class="button__text">Reset</div>
        </button>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>