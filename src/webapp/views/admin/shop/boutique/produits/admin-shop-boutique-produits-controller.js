AdminShopBoutiqueProduitsController = {};

AdminShopBoutiqueProduitsController.load_StatsProduits = function() {
    if (Utils.exists('#container-stats-produits')) {
        AjaxUtils.loadView(
            'admin/shop/boutique/produits/views/stats/stats-produits',
            '#container-stats-produits',
            null,
            function() {},
            '#loader-page'
        );
    }
};

AdminShopBoutiqueProduitsController.load_RechercheAndTableProduits = function() {
    if (Utils.exists('#container-recherche-produits')) {
        AjaxUtils.loadView(
            'admin/shop/boutique/produits/views/recherche/recherche-produits',
            '#container-recherche-produits',
            null,
            function() {
                $('#form-recherche-produits__input-is-en-vente').val(1);

                FormRechercheUtils.control_toggleFormRecherche();
                onClick_btRechercher();
                onClick_btReset();
                AdminShopBoutiqueProduitsController.load_TableProduits();
            },
            '#loader-page'
        );
    }

    function onClick_btRechercher() {
        $('#recherche-produits__bt-rechercher').unbind('click');
        $('#recherche-produits__bt-rechercher').click(function() {
            AdminShopBoutiqueProduitsController.load_TableProduits();
        });
    }

    function onClick_btReset() {
        $('#recherche-produits__bt-reset').unbind('click');
        $('#recherche-produits__bt-reset').click(function() {
            $('#form-recherche-produits__input-nom').val('');
            $('#form-recherche-produits__input-reference').val('');
            $('#form-recherche-produits__input-is-flocable').val('');
            $('#form-recherche-produits__input-is-en-vente').val(1);
            $('#form-recherche-produits__input-marque').val('');
            $('#form-recherche-produits__input-fournisseur').val('');
            $('#form-recherche-produits__input-categorie').val('');

            AdminShopBoutiqueProduitsController.load_TableProduits();
        });
    }
};

AdminShopBoutiqueProduitsController.load_TableProduits = function() {
    if (Utils.exists('#container-table-produits')) {
        var params = {
            nom: $.trim($('#form-recherche-produits__input-nom').val()),
            reference: $.trim($('#form-recherche-produits__input-reference').val()),
            flocable: $('#form-recherche-produits__input-is-flocable').val(),
            enVente: $('#form-recherche-produits__input-is-en-vente').val(),
            marqueProduitBoutiqueId: $('#form-recherche-produits__input-marque').val(),
            fournisseurProduitBoutiqueId: $('#form-recherche-produits__input-fournisseur').val(),
            categorieProduitBoutiqueId: $('#form-recherche-produits__input-categorie').val()
        };
        AjaxUtils.loadView(
            'admin/shop/boutique/produits/views/table/table-produits',
            '#container-table-produits',
            params,
            function() {
                GlobalController.control_ImgModals();
                control_EditPrixProduit();
                control_AddProduit();
                control_EditProduit();
                control_DeleteProduit();
            },
            '#loader-page'
        );
    }

    function control_EditPrixProduit() {
        $('.bt-prix-produit-boutique').unbind('click');
        $('.bt-prix-produit-boutique').click(function() {
            var idProduit = $(this).attr('id-produit');
            open_ModalPrixProduit(idProduit);
        });

        function open_ModalPrixProduit(idProduit) {
            load_ModalPrixProduit(idProduit);
            $('#modal-prix-produit-boutique').modal('show');
        }

        function load_ModalPrixProduit(idProduit) {
            var params = {
                idProduitBoutique: idProduit
            };
            AjaxUtils.loadView(
                'admin/shop/boutique/produits/modals/prix/modal-prix-produit-boutique-content',
                '#modal-prix-produit-boutique-content',
                params,
                function() {
                    onClick_btModifier(idProduit);
                    onClick_btSupprimer(idProduit);
                    onClick_btAjouter(idProduit);
                },
                '#loader-page'
            );
        }

        function onClick_btModifier(idProduit) {
            $('.bt-modifier-prix').unbind('click');
            $('.bt-modifier-prix').click(function() {
                FormUtils.hideFormErrors('#modal-prix-produit-boutique #form-prix-produit-boutique');
                ViewUtils.unsetHTMLAndHide('#modal-prix-produit-boutique .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('#modal-prix-produit-boutique__bt-ajouter', '.loader-modal[for=form-prix-produit-boutique]');

                var formValidation = FormUtils.validateForm('#modal-prix-produit-boutique #form-prix-produit-boutique');
                
                if (formValidation) {
                    var taille = $(this).attr('taille');

                    var inputTaille = $.trim($('#form-prix-produit-boutique__input-taille-'+taille).val());
                    var inputPrix = $.trim($('#form-prix-produit-boutique__input-prix-'+taille).val());
                    var inputPrixFournisseur = $.trim($('#form-prix-produit-boutique__input-prix-fournisseur-'+taille).val());

                    AjaxUtils.callService(
                        'produit-boutique', 
                        'prixExists', 
                        AjaxUtils.dataTypes.json, 
                        AjaxUtils.requestTypes.get, 
                        {
                            produitBoutiqueId: idProduit,
                            taille: inputTaille
                        }, 
                        null, 
                        function(response) {
                            if (taille != inputTaille && response.prixExists) {
                                ViewUtils.activeButtonAndHideLoader('.bt-modifier-prix[taille="'+taille+'"]', '.loader-modal[for=form-prix-produit-boutique]');
                                FormUtils.displayFormResultMessage('#modal-prix-produit-boutique .form-result-message', 'Ce prix existe d&eacute;j&agrave;', FormUtils.resultsTypes.warning);
                            } else {
                                updatePrix(idProduit, taille, inputTaille, inputPrix, inputPrixFournisseur);
                            }
                        },
                        function(response) {
                            ViewUtils.activeButtonAndHideLoader('#modal-prix-produit-boutique__bt-ajouter', '.loader-modal[for=form-add-prix-produit-boutique]');
                            FormUtils.displayFormResultMessage('#modal-prix-produit-boutique .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                        }
                    );
                }
            });

            function updatePrix(idProduit, taille, inputTaille, inputPrix, inputPrixFournisseur) {
                AjaxUtils.callService(
                    'produit-boutique', 
                    'updatePrix', 
                    AjaxUtils.dataTypes.json, 
                    AjaxUtils.requestTypes.post, 
                    {
                        produitBoutiqueId: idProduit,
                        taille: taille
                    }, 
                    {
                        taille: inputTaille,
                        prix: inputPrix,
                        prixFournisseur: inputPrixFournisseur
                    }, 
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-prix-produit-boutique .form-result-message', 'Prix modifi&eacute;', FormUtils.resultsTypes.ok);
                            $('.loader-modal[for=form-prix-produit-boutique]').hide();
                            AdminShopBoutiqueProduitsController.load_StatsProduits();
                            AdminShopBoutiqueProduitsController.load_TableProduits();
                            load_ModalPrixProduit(idProduit);
                        } else {
                            ViewUtils.activeButtonAndHideLoader('.bt-modifier-prix[taille="'+taille+'"]', '.loader-modal[for=form-prix-produit-boutique]');
                            FormUtils.displayFormResultMessage('#modal-prix-produit-boutique .form-result-message', 'Erreur durant la modification', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('.bt-modifier-prix[taille="'+taille+'"]', '.loader-modal[for=form-prix-produit-boutique]');
                        FormUtils.displayFormResultMessage('#modal-prix-produit-boutique .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            }
        }

        function onClick_btSupprimer(idProduit) {
            $('.bt-supprimer-prix').unbind('click');
            $('.bt-supprimer-prix').click(function() {
                var taille = $(this).attr('taille');

                FormUtils.hideFormErrors('#modal-prix-produit-boutique #form-prix-produit-boutique');
                ViewUtils.unsetHTMLAndHide('#modal-prix-produit-boutique .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('.bt-supprimer-prix[taille="'+taille+'"]', '.loader-modal[for=form-prix-produit-boutique]');
                AjaxUtils.callService(
                    'produit-boutique', 
                    'deletePrix', 
                    AjaxUtils.dataTypes.json, 
                    AjaxUtils.requestTypes.post, 
                    null, 
                    {
                        produitBoutiqueId: idProduit,
                        taille: taille
                    }, 
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-prix-produit-boutique .form-result-message', 'Prix supprim&eacute;', FormUtils.resultsTypes.ok);
                            $('.loader-modal[for=form-prix-produit-boutique]').hide();
                            AdminShopBoutiqueProduitsController.load_StatsProduits();
                            AdminShopBoutiqueProduitsController.load_TableProduits();
                            load_ModalPrixProduit(idProduit);
                        } else {
                            ViewUtils.activeButtonAndHideLoader('#.bt-supprimer-prix[taille="'+taille+'"]', '.loader-modal[for=form-prix-produit-boutique]');
                            FormUtils.displayFormResultMessage('#modal-prix-produit-boutique .form-result-message', 'Erreur durant la suppression', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('.bt-supprimer-prix[taille="'+taille+'"]', '.loader-modal[for=form-prix-produit-boutique]');
                        FormUtils.displayFormResultMessage('#modal-prix-produit-boutique .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            });
        }

        function onClick_btAjouter(idProduit) {
            $('#modal-prix-produit-boutique__bt-ajouter').unbind('click');
            $('#modal-prix-produit-boutique__bt-ajouter').click(function() {
                FormUtils.hideFormErrors('#modal-prix-produit-boutique #form-add-prix-produit-boutique');
                ViewUtils.unsetHTMLAndHide('#modal-prix-produit-boutique .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('#modal-prix-produit-boutique__bt-ajouter', '.loader-modal[for=form-prix-produit-boutique]');

                var formValidation = FormUtils.validateForm('#modal-prix-produit-boutique #form-add-prix-produit-boutique');
                
                if (formValidation) {
                    var inputTaille = $.trim($('#form-add-prix-produit-boutique__input-taille').val());
                    var inputPrix = $.trim($('#form-add-prix-produit-boutique__input-prix').val());
                    var inputPrixFournisseur = $.trim($('#form-add-prix-produit-boutique__input-prix-fournisseur').val());

                    AjaxUtils.callService(
                        'produit-boutique', 
                        'prixExists', 
                        AjaxUtils.dataTypes.json, 
                        AjaxUtils.requestTypes.get, 
                        {
                            produitBoutiqueId: idProduit,
                            taille: inputTaille
                        }, 
                        null, 
                        function(response) {
                            if (response.prixExists) {
                                ViewUtils.activeButtonAndHideLoader('#modal-prix-produit-boutique__bt-ajouter', '.loader-modal[for=form-prix-produit-boutique]');
                                FormUtils.displayFormResultMessage('#modal-prix-produit-boutique .form-result-message', 'Ce prix existe d&eacute;j&agrave;', FormUtils.resultsTypes.warning);
                            } else {
                                addPrix(idProduit, inputTaille, inputPrix, inputPrixFournisseur);
                            }
                        },
                        function(response) {
                            ViewUtils.activeButtonAndHideLoader('#modal-prix-produit-boutique__bt-ajouter', '.loader-modal[for=form-prix-produit-boutique]');
                            FormUtils.displayFormResultMessage('#modal-prix-produit-boutique .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                        }
                    );
                } else {
                    ViewUtils.activeButtonAndHideLoader('#modal-prix-produit-boutique__bt-ajouter', '.loader-modal[for=form-prix-produit-boutique]');
                }
            });
    
            function addPrix(idProduit, inputTaille, inputPrix, inputPrixFournisseur) {
                AjaxUtils.callService(
                    'produit-boutique', 
                    'addPrix', 
                    AjaxUtils.dataTypes.json, 
                    AjaxUtils.requestTypes.post, 
                    {
                        produitBoutiqueId: idProduit
                    }, 
                    {
                        taille: inputTaille,
                        prix: inputPrix,
                        prixFournisseur: inputPrixFournisseur
                    }, 
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-prix-produit-boutique .form-result-message', 'Prix ajout&eacute;', FormUtils.resultsTypes.ok);
                            $('.loader-modal[for=form-add-prix-produit-boutique]').hide();
                            AdminShopBoutiqueProduitsController.load_StatsProduits();
                            AdminShopBoutiqueProduitsController.load_TableProduits();
                            load_ModalPrixProduit(idProduit);
                        } else {
                            ViewUtils.activeButtonAndHideLoader('#modal-prix-produit-boutique__bt-ajouter', '.loader-modal[for=form-prix-produit-boutique]');
                            FormUtils.displayFormResultMessage('#modal-prix-produit-boutique .form-result-message', 'Erreur durant l\'ajout', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('#modal-prix-produit-boutique__bt-ajouter', '.loader-modal[for=form-prix-produit-boutique]');
                        FormUtils.displayFormResultMessage('#modal-prix-produit-boutique .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            }
        }
    }

    function control_AddProduit() {
        $('#produits__bt-ajouter').unbind('click');
        $('#produits__bt-ajouter').click(function() {
            open_ModalSaveProduit(null);
        });
    }

    function control_EditProduit() {
        $('.glyphicon-pencil').unbind('click');
        $('.glyphicon-pencil').click(function() {
            var idProduit = $(this).attr('id-produit');
            open_ModalSaveProduit(idProduit);
        });
    }

    function open_ModalSaveProduit(idProduit) {
        var params = null;
        if (idProduit != null) {
            params = {
                idProduitBoutique: idProduit
            };
        }
        AjaxUtils.loadView(
            'admin/shop/boutique/produits/modals/save/modal-save-produit-boutique-content',
            '#modal-save-produit-boutique-content',
            params,
            function() {
                if (idProduit == null) {
                    $('#form-save-produit-boutique__input-en-vente').prop('checked', true);
                }
                control_InputsFlocages(idProduit);
                onClick_btSauvegarder();
            },
            '#loader-page'
        );
        $('#modal-save-produit-boutique').modal('show');

        function control_InputsFlocages(idProduit) {
            control_InputsFlocage(idProduit, 'nom');
            control_InputsFlocage(idProduit, 'numero-arriere');
            control_InputsFlocage(idProduit, 'numero-avant');
            control_InputsFlocage(idProduit, 'initiales');
        }

        function control_InputsFlocage(idProduit, libFlocage) {
            if (idProduit != null) {
                selectInputsIfFlocableChecked(libFlocage, $('#form-save-produit-boutique__input-flocable-'+libFlocage).prop('checked'));
            } else {
                $('#form-save-produit-boutique__input-prix-flocage-'+libFlocage).prop('disabled', true);
                $('#form-save-produit-boutique__input-prix-flocage-fournisseur-'+libFlocage).prop('disabled', true);
            }
            $('#form-save-produit-boutique__input-flocable-'+libFlocage).unbind('click');
            $('#form-save-produit-boutique__input-flocable-'+libFlocage).change(function() {
                var flocableChecked = $(this).prop('checked');
                FormUtils.hideFormErrors('#modal-save-produit-boutique #form-save-produit-boutique');
                selectInputsIfFlocableChecked(libFlocage, flocableChecked)
            });

            function selectInputsIfFlocableChecked(libFlocage, flocableChecked) {
                if (flocableChecked) {
                    $('label[for=\"form-save-produit-boutique__input-flocable-'+libFlocage+'\"]').addClass('required');
                    $('#form-save-produit-boutique__input-prix-flocage-'+libFlocage).prop('required', true);
                    $('#form-save-produit-boutique__input-prix-flocage-'+libFlocage).prop('disabled', false);
                    $('#form-save-produit-boutique__input-prix-flocage-fournisseur-'+libFlocage).prop('required', true);
                    $('#form-save-produit-boutique__input-prix-flocage-fournisseur-'+libFlocage).prop('disabled', false);
                } else {
                    $('label[for=\"form-save-produit-boutique__input-flocable-'+libFlocage+'\"]').removeClass('required');
                    $('#form-save-produit-boutique__input-prix-flocage-'+libFlocage).prop('required', false);
                    $('#form-save-produit-boutique__input-prix-flocage-'+libFlocage).prop('disabled', true);
                    $('#form-save-produit-boutique__input-prix-flocage-'+libFlocage).val('');
                    $('#form-save-produit-boutique__input-prix-flocage-fournisseur-'+libFlocage).prop('required', false);
                    $('#form-save-produit-boutique__input-prix-flocage-fournisseur-'+libFlocage).prop('disabled', true);
                    $('#form-save-produit-boutique__input-prix-flocage-fournisseur-'+libFlocage).val('');
                }
            }
        }

        function onClick_btSauvegarder() {
            $('#modal-save-produit-boutique__bt-sauvegarder').unbind('click');
            $('#modal-save-produit-boutique__bt-sauvegarder').click(function() {
                FormUtils.hideFormErrors('#modal-save-produit-boutique #form-save-produit-boutique');
                ViewUtils.unsetHTMLAndHide('#modal-save-produit-boutique .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('#modal-save-produit-boutique__bt-sauvegarder', '#modal-save-produit-boutique__bt-sauvegarder .loader');

                var formValidation = FormUtils.validateForm('#modal-save-produit-boutique #form-save-produit-boutique');
                
                if (formValidation) {
                    document.getElementById('form-save-produit-boutique').submit();
                } else {
                    ViewUtils.activeButtonAndHideLoader('#modal-save-produit-boutique__bt-sauvegarder', '#modal-save-produit-boutique__bt-sauvegarder .loader');
                }
            });
        }
    }

    function control_DeleteProduit() {
        $('.glyphicon-trash').unbind('click');
        $('.glyphicon-trash').click(function() {
            var idProduit = $(this).attr('id-produit');
            
            ViewUtils.prompt(
                'Supprimer ce produit ?', 
                {
                    btOkText: 'Oui',
                    btKoText: 'Non'
                },
                function() {
                    deleteProduit(idProduit);
                }, 
                null, 
                null
            );
        });

        function deleteProduit(idProduit) {
            $('.loader-delete[id-produit='+idProduit+']').show();
            AjaxUtils.callService(
                'produit-boutique', 
                'delete', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idProduit
                }, 
                function(response) {
                    $('.loader-delete[id-produit='+idProduit+']').hide();
                    if (response.success) {
                        AdminShopBoutiqueProduitsController.load_StatsProduits();
                        AdminShopBoutiqueProduitsController.load_TableProduits();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la suppression du produit', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-delete[id-produit='+idProduit+']').hide();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        }
    }
};

initializePage = function() {
    AdminController.initializePage('shop', 'boutique');

    AdminShopBoutiqueProduitsController.load_StatsProduits();
    AdminShopBoutiqueProduitsController.load_RechercheAndTableProduits();
};