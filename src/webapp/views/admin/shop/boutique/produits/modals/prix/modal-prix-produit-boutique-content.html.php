<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/shop/boutique/produits/modals/prix/", 
    "modal-prix-produit-boutique", 
    "ModalPrixProduitBoutiqueCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
    $produit = $ctrl->getProduit();
    $prix = $ctrl->getPrix();
    ?>
    <div class="modal-header">
        <div class="modal-title">
            <?php echo $produit[PRODUIT_BOUTIQUE_NOM]; ?> - 
            <span class="normal">Prix</span>
        </div>
        <div class="loader loader-modal" for="form-prix-produit-boutique"></div>
    </div>
    <div class="modal-body">
        <div class="form-horizontal" id="form-prix-produit-boutique">
            <?php
            if (sizeof($prix) > 0) {
                foreach ($prix as $prixItem) {
                    ?>
                    <div class="form-group">
                        <label for="form-prix-produit-boutique__input-taille-<?php echo $prixItem[PRIX_PRODUIT_BOUTIQUE_TAILLE]; ?>" class="col-xs-2 control-label">Taille :</label>
                        <div class="col-xs-1 form-input" for="form-prix-produit-boutique__input-taille-<?php echo $prixItem[PRIX_PRODUIT_BOUTIQUE_TAILLE]; ?>">
                            <input type="text" class="form-control" id="form-prix-produit-boutique__input-taille-<?php echo $prixItem[PRIX_PRODUIT_BOUTIQUE_TAILLE]; ?>" value="<?php echo $prixItem[PRIX_PRODUIT_BOUTIQUE_TAILLE]; ?>">
                        </div>
                        <div class="col-xs-2 col-xs-offset-1 form-input" for="form-prix-produit-boutique__input-prix-<?php echo $prixItem[PRIX_PRODUIT_BOUTIQUE_TAILLE]; ?>">
                            <div class="input-group">
                                <input type="text" class="form-control" id="form-prix-produit-boutique__input-prix-<?php echo $prixItem[PRIX_PRODUIT_BOUTIQUE_TAILLE]; ?>" placeholder="Notre prix" value="<?php echo $prixItem[PRIX_PRODUIT_BOUTIQUE_PRIX]; ?>" pattern="^[0-9]+(\.)?[0-9]*$" pattern-indication="Le prix doit &ecirc;tre un nombre d&eacute;cimal" required>
                                <span class="input-group-addon">&euro;</span>
                            </div>
                        </div>
                        <div class="col-xs-1 form-icon-validation" for="form-prix-produit-boutique__input-prix-<?php echo $prixItem[PRIX_PRODUIT_BOUTIQUE_TAILLE]; ?>"></div>
                        <div class="col-xs-2 form-input" for="form-prix-produit-boutique__input-prix-fournisseur-<?php echo $prixItem[PRIX_PRODUIT_BOUTIQUE_TAILLE]; ?>">
                            <div class="input-group">
                                <input type="text" class="form-control" id="form-prix-produit-boutique__input-prix-fournisseur-<?php echo $prixItem[PRIX_PRODUIT_BOUTIQUE_TAILLE]; ?>" placeholder="Prix fourn." value="<?php echo $prixItem[PRIX_PRODUIT_BOUTIQUE_PRIX_FOURNISSEUR]; ?>" pattern="^[0-9]+(\.)?[0-9]*$" pattern-indication="Le prix doit &ecirc;tre un nombre d&eacute;cimal" required>
                                <span class="input-group-addon">&euro;</span>
                            </div>
                        </div>
                        <div class="col-xs-1 form-icon-validation" for="form-prix-produit-boutique__input-prix-fournisseur-<?php echo $prixItem[PRIX_PRODUIT_BOUTIQUE_TAILLE]; ?>"></div>
                        <div class="col-xs-3 text-right">
                            <button class="btn btn-default bt-modifier-prix" taille="<?php echo $prixItem[PRIX_PRODUIT_BOUTIQUE_TAILLE]; ?>">
                                <div class="button__icon">
                                    <span class="glyphicon glyphicon-ok"></span>
                                </div>
                                <div class="button__text">OK</div>
                            </button>
                            <button class="btn btn-default bt-supprimer-prix" taille="<?php echo $prixItem[PRIX_PRODUIT_BOUTIQUE_TAILLE]; ?>">
                                <div class="button__icon">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </div>
                                <div class="button__text">Sup.</div>
                            </button>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <div class="form-horizontal" id="form-add-prix-produit-boutique">
            <div class="form-group">
                <label for="form-add-prix-produit-boutique__input-taille" class="col-xs-2 control-label">Taille :</label>
                <div class="col-xs-1 form-input" for="form-add-prix-produit-boutique__input-taille">
                    <input type="text" class="form-control" id="form-add-prix-produit-boutique__input-taille">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-add-prix-produit-boutique__input-taille"></div>
                <div class="col-xs-2 form-input" for="form-add-prix-produit-boutique__input-prix">
                    <div class="input-group">
                        <input type="text" class="form-control" id="form-add-prix-produit-boutique__input-prix" placeholder="Notre prix" pattern="^[0-9]+(\.)?[0-9]*$" pattern-indication="Le prix doit &ecirc;tre un nombre d&eacute;cimal" required>
                        <span class="input-group-addon">&euro;</span>
                    </div>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-add-prix-produit-boutique__input-prix"></div>
                <div class="col-xs-2 form-input" for="form-add-prix-produit-boutique__input-prix-fournisseur">
                    <div class="input-group">
                        <input type="text" class="form-control" id="form-add-prix-produit-boutique__input-prix-fournisseur" placeholder="Prix fourn." pattern="^[0-9]+(\.)?[0-9]*$" pattern-indication="Le prix doit &ecirc;tre un nombre d&eacute;cimal" required>
                        <span class="input-group-addon">&euro;</span>
                    </div>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-add-prix-produit-boutique__input-prix-fournisseur"></div>
                <div class="col-xs-3 text-right">
                    <button id="modal-prix-produit-boutique__bt-ajouter" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-plus"></span>
                        </div>
                        <div class="button__text">OK</div>
                    </button>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-required text-right">Les champs marqu&eacute;s d'une * sont obligatoires</div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-12">
                <div class="form-result-message"></div>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>