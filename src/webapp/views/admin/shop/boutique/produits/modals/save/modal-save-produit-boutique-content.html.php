<?php
require_once("core/php/resources/view.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/shop/boutique/produits/modals/save/", 
    "modal-save-produit-boutique", 
    "ModalSaveProduitBoutiqueCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
?>
    <div class="modal-header">
        <div class="modal-title"><?php echo $ctrl->getTitle(); ?></div>
    </div>
    <div class="modal-body">
        <form id="form-save-produit-boutique" method="post" action="<?php echo ROOT_PATH.UPLOADER_PATH; ?>/produit-boutique<?php echo $ctrl->getParamIdProduit(); ?>" enctype="multipart/form-data" class="form-horizontal" target="uploader-produit-boutique">
            <div class="form-group">
                <label for="form-save-produit-boutique__input-nom" class="col-xs-4 control-label required">Nom :</label>
                <div class="col-xs-7 form-input" for="form-save-produit-boutique__input-nom">
                    <input type="text" class="form-control" id="form-save-produit-boutique__input-nom" name="nom" value="<?php echo $ctrl->getNomProduit(); ?>" required>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-produit-boutique__input-nom"></div>
            </div>
            <div class="form-group">
                <label for="form-save-produit-boutique__input-indication" class="col-xs-4 control-label">Indication :</label>
                <div class="col-xs-7 form-input" for="form-save-produit-boutique__input-indication">
                    <input type="text" class="form-control" id="form-save-produit-boutique__input-indication" name="indication" value="<?php echo $ctrl->getIndicationProduit(); ?>">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-produit-boutique__input-indication"></div>
            </div>
            <div class="form-group">
                <?php
                $required = "";
                if ($ctrl->getProduitBoutique() == null) {
                    $required = " required";
                }
                ?>
                <label for="form-save-produit-boutique__input-img-file" class="col-xs-4 control-label<?php echo $required; ?>">
                    Photo <span class="label-indication">(.<?php echo IMG_PRODUIT_BOUTIQUE_EXTENSION; ?> - <?php echo IMG_PRODUIT_BOUTIQUE_TAILLE; ?>)</span> :
                </label>
                <div class="col-xs-7 form-input" for="form-save-produit-boutique__input-img-file">
                    <input type="file" class="form-control" id="form-save-produit-boutique__input-img-file" name="img-file" pattern="^(.)+(\.<?php echo IMG_PRODUIT_BOUTIQUE_EXTENSION; ?>)$" pattern-indication="L'extension doit &ecirc;tre '.<?php echo IMG_PRODUIT_BOUTIQUE_EXTENSION; ?>'"<?php echo $required; ?>>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-produit-boutique__input-img-file"></div>
            </div>
            <div class="form-group">
                <label for="form-save-produit-boutique__input-couleurs" class="col-xs-4 control-label">
                    Couleur(s) :
                    <div class="label-indication">s&eacute;parer par des ','</div>
                </label>
                <div class="col-xs-7 form-input" for="form-save-produit-boutique__input-couleurs">
                    <input type="text" class="form-control" id="form-save-produit-boutique__input-couleurs" name="couleurs" value="<?php echo $ctrl->getCouleursProduit(); ?>">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-produit-boutique__input-couleurs"></div>
            </div>
            <div class="form-group">
                <label for="form-save-produit-boutique__input-references" class="col-xs-4 control-label">
                    R&eacute;f(s) :
                    <div class="label-indication">s&eacute;parer par des ','</div>
                </label>
                <div class="col-xs-7 form-input" for="form-save-produit-boutique__input-references">
                    <input type="text" class="form-control" id="form-save-produit-boutique__input-references" name="references" value="<?php echo $ctrl->getReferencesProduit(); ?>">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-produit-boutique__input-references"></div>
            </div>
            <div class="form-group">
                <label for="form-save-produit-boutique__input-description" class="col-xs-4 control-label required">Description :</label>
                <div class="col-xs-7 form-input" for="form-save-produit-boutique__input-description">
                    <textarea class="form-control" id="form-save-produit-boutique__input-description" rows="8" name="description" required><?php echo $ctrl->getDescriptionProduit(); ?></textarea>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-produit-boutique__input-description"></div>
            </div>
            <div class="form-group">
                <label for="form-save-produit-boutique__input-flocage-logo-inclus" class="col-xs-4 control-label">Flocage logo ?</label>
                <div class="col-xs-7 form-input" for="form-save-produit-boutique__input-flocage-logo-inclus">
                    <input type="checkbox" id="form-save-produit-boutique__input-flocage-logo-inclus" name="flocage-logo-inclus"<?php echo $ctrl->getFlocageLogoInclusChecked(); ?>>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-produit-boutique__input-flocage-logo-inclus"></div>
            </div>
            <div class="form-group">
                <label for="form-save-produit-boutique__input-flocable-nom" class="col-xs-4 control-label">Nom flocable ?</label>
                <div class="col-xs-1 form-input" for="form-save-produit-boutique__input-flocable-nom">
                    <input type="checkbox" id="form-save-produit-boutique__input-flocable-nom" name="flocable-nom"<?php echo $ctrl->getFlocableNomChecked(); ?>>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-produit-boutique__input-flocable-nom"></div>
                <div class="col-xs-2 form-input" for="form-save-produit-boutique__input-prix-flocage-nom">
                    <div class="input-group">
                        <input type="text" class="form-control" id="form-save-produit-boutique__input-prix-flocage-nom" name="prix-flocage-nom" placeholder="Notre prix" value="<?php echo $ctrl->getPrixFlocageNom(); ?>" pattern="^[0-9]+(\.)?[0-9]*$" pattern-indication="Le prix doit &ecirc;tre un nombre d&eacute;cimal">
                        <span class="input-group-addon">&euro;</span>
                    </div>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-produit-boutique__input-prix-flocage-nom"></div>
                <div class="col-xs-2 form-input" for="form-save-produit-boutique__input-prix-flocage-fournisseur-nom">
                    <div class="input-group">
                        <input type="text" class="form-control" id="form-save-produit-boutique__input-prix-flocage-fournisseur-nom" name="prix-flocage-fournisseur-nom" placeholder="Prix fourn." value="<?php echo $ctrl->getPrixFlocageFournisseurNom(); ?>" pattern="^[0-9]+(\.)?[0-9]*$" pattern-indication="Le prix doit &ecirc;tre un nombre d&eacute;cimal">
                        <span class="input-group-addon">&euro;</span>
                    </div>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-produit-boutique__input-prix-flocage-fournisseur-nom"></div>
            </div>
            <div class="form-group">
                <label for="form-save-produit-boutique__input-flocable-numero-arriere" class="col-xs-4 control-label">Num. arr. flocable ?</label>
                <div class="col-xs-1 form-input" for="form-save-produit-boutique__input-flocable-numero-arriere">
                    <input type="checkbox" id="form-save-produit-boutique__input-flocable-numero-arriere" name="flocable-numero-arriere"<?php echo $ctrl->getFlocableNumeroArriereChecked(); ?>>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-produit-boutique__input-flocable-numero-arriere"></div>
                <div class="col-xs-2 form-input" for="form-save-produit-boutique__input-prix-flocage-numero-arriere">
                    <div class="input-group">
                        <input type="text" class="form-control" id="form-save-produit-boutique__input-prix-flocage-numero-arriere" name="prix-flocage-numero-arriere" placeholder="Notre prix" value="<?php echo $ctrl->getPrixFlocageNumeroArriere(); ?>" pattern="^[0-9]+(\.)?[0-9]*$" pattern-indication="Le prix doit &ecirc;tre un nombre d&eacute;cimal">
                        <span class="input-group-addon">&euro;</span>
                    </div>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-produit-boutique__input-prix-flocage-numero-arriere"></div>
                <div class="col-xs-2 form-input" for="form-save-produit-boutique__input-prix-flocage-fournisseur-numero-arriere">
                    <div class="input-group">
                        <input type="text" class="form-control" id="form-save-produit-boutique__input-prix-flocage-fournisseur-numero-arriere" name="prix-flocage-fournisseur-numero-arriere" placeholder="Prix fourn." value="<?php echo $ctrl->getPrixFlocageFournisseurNumeroArriere(); ?>" pattern="^[0-9]+(\.)?[0-9]*$" pattern-indication="Le prix doit &ecirc;tre un nombre d&eacute;cimal">
                        <span class="input-group-addon">&euro;</span>
                    </div>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-produit-boutique__input-prix-flocage-fournisseur-numero-arriere"></div>
            </div>
            <div class="form-group">
                <label for="form-save-produit-boutique__input-flocable-numero-avant" class="col-xs-4 control-label">Num. av. flocable ?</label>
                <div class="col-xs-1 form-input" for="form-save-produit-boutique__input-flocable-numero-avant">
                    <input type="checkbox" id="form-save-produit-boutique__input-flocable-numero-avant" name="flocable-numero-avant"<?php echo $ctrl->getFlocableNumeroAvantChecked(); ?>>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-produit-boutique__input-flocable-numero-avant"></div>
                <div class="col-xs-2 form-input" for="form-save-produit-boutique__input-prix-flocage-numero-avant">
                    <div class="input-group">
                        <input type="text" class="form-control" id="form-save-produit-boutique__input-prix-flocage-numero-avant" name="prix-flocage-numero-avant" placeholder="Notre prix" value="<?php echo $ctrl->getPrixFlocageNumeroAvant(); ?>" pattern="^[0-9]+(\.)?[0-9]*$" pattern-indication="Le prix doit &ecirc;tre un nombre d&eacute;cimal">
                        <span class="input-group-addon">&euro;</span>
                    </div>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-produit-boutique__input-prix-flocage-numero-avant"></div>
                <div class="col-xs-2 form-input" for="form-save-produit-boutique__input-prix-flocage-fournisseur-numero-avant">
                    <div class="input-group">
                        <input type="text" class="form-control" id="form-save-produit-boutique__input-prix-flocage-fournisseur-numero-avant" name="prix-flocage-fournisseur-numero-avant" placeholder="Prix fourn." value="<?php echo $ctrl->getPrixFlocageFournisseurNumeroAvant(); ?>" pattern="^[0-9]+(\.)?[0-9]*$" pattern-indication="Le prix doit &ecirc;tre un nombre d&eacute;cimal">
                        <span class="input-group-addon">&euro;</span>
                    </div>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-produit-boutique__input-prix-flocage-fournisseur-numero-avant"></div>
            </div>
            <div class="form-group">
                <label for="form-save-produit-boutique__input-flocable-initiales" class="col-xs-4 control-label">Init. flocables ?</label>
                <div class="col-xs-1 form-input" for="form-save-produit-boutique__input-flocable-initiales">
                    <input type="checkbox" id="form-save-produit-boutique__input-flocable-initiales" name="flocable-initiales"<?php echo $ctrl->getFlocableInitialesChecked(); ?>>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-produit-boutique__input-flocable-initiales"></div>
                <div class="col-xs-2 form-input" for="form-save-produit-boutique__input-prix-flocage-initiales">
                    <div class="input-group">
                        <input type="text" class="form-control" id="form-save-produit-boutique__input-prix-flocage-initiales" name="prix-flocage-initiales" placeholder="Notre prix" value="<?php echo $ctrl->getPrixFlocageInitiales(); ?>" pattern="^[0-9]+(\.)?[0-9]*$" pattern-indication="Le prix doit &ecirc;tre un nombre d&eacute;cimal">
                        <span class="input-group-addon">&euro;</span>
                    </div>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-produit-boutique__input-prix-flocage-initiales"></div>
                <div class="col-xs-2 form-input" for="form-save-produit-boutique__input-prix-flocage-fournisseur-initiales">
                    <div class="input-group">
                        <input type="text" class="form-control" id="form-save-produit-boutique__input-prix-flocage-fournisseur-initiales" name="prix-flocage-fournisseur-initiales" placeholder="Prix fourn." value="<?php echo $ctrl->getPrixFlocageFournisseurInitiales(); ?>" pattern="^[0-9]+(\.)?[0-9]*$" pattern-indication="Le prix doit &ecirc;tre un nombre d&eacute;cimal">
                        <span class="input-group-addon">&euro;</span>
                    </div>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-produit-boutique__input-prix-flocage-fournisseur-initiales"></div>
            </div>
            <div class="form-group">
                <label for="form-save-produit-boutique__input-en-vente" class="col-xs-4 control-label">En vente ?</label>
                <div class="col-xs-7 form-input" for="form-save-produit-boutique__input-en-vente">
                    <input type="checkbox" id="form-save-produit-boutique__input-en-vente" name="en-vente"<?php echo $ctrl->getEnVenteChecked(); ?>>
                </div>
            </div>
            <div class="form-group">
                <label for="form-save-produit-boutique__input-position" class="col-xs-4 control-label">Position :</label>
                <div class="col-xs-7 form-input" for="form-save-produit-boutique__input-position">
                    <input type="text" class="form-control" id="form-save-produit-boutique__input-position" name="position" value="<?php echo $ctrl->getPositionProduit(); ?>"pattern="^[0-9]+$" pattern-indication="Le prix doit &ecirc;tre un nombre" required>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-produit-boutique__input-position"></div>
            </div>
            <div class="form-group">
                <label for="form-save-produit-boutique__input-categorie" class="col-xs-4 control-label required">Cat&eacute;gorie :</label>
                <div class="col-xs-7 form-input" for="form-save-produit-boutique__input-categorie">
                    <select class="form-control" id="form-save-produit-boutique__input-categorie" name="categorie-produit-boutique-id" required>
                        <option value=""></option>
                        <?php echo $ctrl->getOptionsCategorieProduit(); ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-produit-boutique__input-categorie"></div>
            </div>
            <div class="form-group">
                <label for="form-save-produit-boutique__input-fournisseur" class="col-xs-4 control-label required">Fournisseur :</label>
                <div class="col-xs-7 form-input" for="form-save-produit-boutique__input-fournisseur">
                    <select class="form-control" id="form-save-produit-boutique__input-fournisseur" name="fournisseur-produit-boutique-id" required>
                        <option value=""></option>
                        <?php echo $ctrl->getOptionsFournisseurProduit(); ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-produit-boutique__input-fournisseur"></div>
            </div>
            <div class="form-group">
                <label for="form-save-produit-boutique__input-marque" class="col-xs-4 control-label">Marque :</label>
                <div class="col-xs-7 form-input" for="form-save-produit-boutique__input-marque">
                    <select class="form-control" id="form-save-produit-boutique__input-marque" name="marque-produit-boutique-id">
                        <option value=""></option>
                        <?php echo $ctrl->getOptionsMarqueProduit(); ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-produit-boutique__input-marque"></div>
            </div>
            <div class="form-group">
                <label for="form-save-produit-boutique__input-tailles-possibles" class="col-xs-4 control-label required">Tailles :</label>
                <div class="col-xs-7 form-input" for="form-save-produit-boutique__input-tailles-possibles">
                    <select class="form-control" id="form-save-produit-boutique__input-tailles-possibles" name="tailles-possibles-produit-boutique-id" required>
                        <option value=""></option>
                        <?php echo $ctrl->getOptionsTaillesPossiblesProduit(); ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-produit-boutique__input-tailles-possibles"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-required text-right">Les champs marqu&eacute;s d'une * sont obligatoires</div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-8">
                <div class="form-result-message"></div>
            </div>
            <div class="col-xs-4 text-right">
                <button id="modal-save-produit-boutique__bt-sauvegarder" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-ok"></span>
                    </div>
                    <div class="button__text">Sauvegarder</div>
                    <div class="loader"></div>
                </button>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>