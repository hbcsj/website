<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/produit-boutique-dao.php");
require_once("common/php/dao/prix-produit-boutique-dao.php");

class ModalPrixProduitBoutiqueCtrl extends AbstractViewCtrl {
	
	private $produitBoutique;

	private $produitBoutiqueDAO;
	private $prixProduitBoutiqueDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idProduitBoutique" => $_GET["idProduitBoutique"]
		), true);
		
		if (isAdminConnected_commercial()) {
			$this->prixProduitBoutiqueDAO = new PrixProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->produitBoutiqueDAO = new ProduitBoutiqueDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdProduit();
	}

	private function checkIdProduit() {
		$this->produitBoutique = $this->produitBoutiqueDAO->getById($_GET["idProduitBoutique"]);
			
		if ($this->produitBoutique == null) {
			$this->sendCheckError(
				HTTP_404, 
				"Le produit boutique (idProduitBoutique = '".$_GET["idProduitBoutique"]."') n'existe pas", 
				"webapp/views/common/error/404/404.html.php"
			);
		}
	}

	public function getProduit() {
		return $this->produitBoutique;
	}

	public function getPrix() {
		return $this->prixProduitBoutiqueDAO->getByProduitId($this->produitBoutique[PRODUIT_BOUTIQUE_ID]);
	}
}

?>