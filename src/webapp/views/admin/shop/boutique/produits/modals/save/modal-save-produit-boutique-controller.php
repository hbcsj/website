<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("common/php/dao/produit-boutique-dao.php");
require_once("common/php/dao/categorie-produit-boutique-dao.php");
require_once("common/php/dao/marque-produit-boutique-dao.php");
require_once("common/php/dao/tailles-possibles-produit-boutique-dao.php");
require_once("common/php/dao/fournisseur-produit-boutique-dao.php");

class ModalSaveProduitBoutiqueCtrl extends AbstractViewCtrl {
	
	private $produitBoutique;

	private $produitBoutiqueDAO;
	private $categorieProduitBoutiqueDAO;
	private $fournisseurProduitBoutiqueDAO;
	private $marqueProduitBoutiqueDAO;
	private $taillesPossiblesProduitBoutiqueDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idProduitBoutique" => $_GET["idProduitBoutique"]
		), true);
		
		if (isAdminConnected_commercial()) {
			$this->produitBoutiqueDAO = new ProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->categorieProduitBoutiqueDAO = new CategorieProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->marqueProduitBoutiqueDAO = new MarqueProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->taillesPossiblesProduitBoutiqueDAO = new TaillesPossiblesProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->fournisseurProduitBoutiqueDAO = new FournisseurProduitBoutiqueDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdProduit();
	}

	private function checkIdProduit() {
		if (HTTPUtils::paramExists(GET, "idProduitBoutique")) {
			$this->produitBoutique = $this->produitBoutiqueDAO->getById($_GET["idProduitBoutique"]);
				
			if ($this->produitBoutique == null) {
				$this->sendCheckError(
					HTTP_404, 
					"Le produit boutique (idProduitBoutique = '".$_GET["idProduitBoutique"]."') n'existe pas", 
					"webapp/views/common/error/404/404.html.php"
				);
			}
		}
	}

	public function getTitle() {
		$title = "Nouveau produit";
		if ($this->produitBoutique != null) {
			$title = $this->produitBoutique[PRODUIT_BOUTIQUE_NOM]." - <span class=\"normal\">Edition</span>";
		}
		return $title;
	}

	public function getNomProduit() {
		if ($this->produitBoutique != null) {
			return $this->produitBoutique[PRODUIT_BOUTIQUE_NOM];
		}
		return "";
	}

	public function getIndicationProduit() {
		if ($this->produitBoutique != null) {
			return $this->produitBoutique[PRODUIT_BOUTIQUE_INDICATION];
		}
		return "";
	}

	public function getCouleursProduit() {
		if ($this->produitBoutique != null) {
			return $this->produitBoutique[PRODUIT_BOUTIQUE_COULEURS];
		}
		return "";
	}

	public function getReferencesProduit() {
		if ($this->produitBoutique != null) {
			return $this->produitBoutique[PRODUIT_BOUTIQUE_REFERENCES];
		}
		return "";
	}

	public function getDescriptionProduit() {
		if ($this->produitBoutique != null) {
			return FileUtils::getFileContent(PathUtils::getProduitBoutiqueDescriptionFile($this->produitBoutique[PRODUIT_BOUTIQUE_ID]));
		}
		return "";
	}

	public function getFlocageLogoInclusChecked() {
		$checked = "";
        if ($this->produitBoutique != null && $this->produitBoutique[PRODUIT_BOUTIQUE_FLOCAGE_LOGO_INCLUS]) {
            $checked = " checked=\"checked\"";
        }
        return $checked;
	}

	public function getFlocableNomChecked() {
		$checked = "";
        if ($this->produitBoutique != null && $this->produitBoutique[PRODUIT_BOUTIQUE_FLOCABLE_NOM]) {
            $checked = " checked=\"checked\"";
        }
        return $checked;
	}

	public function getPrixFlocageNom() {
		if ($this->produitBoutique != null) {
			return $this->produitBoutique[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_NOM];
		}
		return "";
	}

	public function getPrixFlocageFournisseurNom() {
		if ($this->produitBoutique != null) {
			return $this->produitBoutique[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_FOURNISSEUR_NOM];
		}
		return "";
	}

	public function getFlocableNumeroArriereChecked() {
		$checked = "";
        if ($this->produitBoutique != null && $this->produitBoutique[PRODUIT_BOUTIQUE_FLOCABLE_NUMERO_ARRIERE]) {
            $checked = " checked=\"checked\"";
        }
        return $checked;
	}

	public function getPrixFlocageNumeroArriere() {
		if ($this->produitBoutique != null) {
			return $this->produitBoutique[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_NUMERO_ARRIERE];
		}
		return "";
	}

	public function getPrixFlocageFournisseurNumeroArriere() {
		if ($this->produitBoutique != null) {
			return $this->produitBoutique[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_FOURNISSEUR_NUMERO_ARRIERE];
		}
		return "";
	}

	public function getFlocableNumeroAvantChecked() {
		$checked = "";
        if ($this->produitBoutique != null && $this->produitBoutique[PRODUIT_BOUTIQUE_FLOCABLE_NUMERO_AVANT]) {
            $checked = " checked=\"checked\"";
        }
        return $checked;
	}

	public function getPrixFlocageNumeroAvant() {
		if ($this->produitBoutique != null) {
			return $this->produitBoutique[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_NUMERO_AVANT];
		}
		return "";
	}

	public function getPrixFlocageFournisseurNumeroAvant() {
		if ($this->produitBoutique != null) {
			return $this->produitBoutique[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_FOURNISSEUR_NUMERO_AVANT];
		}
		return "";
	}

	public function getFlocableInitialesChecked() {
		$checked = "";
        if ($this->produitBoutique != null && $this->produitBoutique[PRODUIT_BOUTIQUE_FLOCABLE_INITIALES]) {
            $checked = " checked=\"checked\"";
        }
        return $checked;
	}

	public function getPrixFlocageInitiales() {
		if ($this->produitBoutique != null) {
			return $this->produitBoutique[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_INITIALES];
		}
		return "";
	}

	public function getPrixFlocageFournisseurInitiales() {
		if ($this->produitBoutique != null) {
			return $this->produitBoutique[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_FOURNISSEUR_INITIALES];
		}
		return "";
	}

	public function getEnVenteChecked() {
		$checked = "";
        if ($this->produitBoutique != null && $this->produitBoutique[PRODUIT_BOUTIQUE_EN_VENTE]) {
            $checked = " checked=\"checked\"";
        }
        return $checked;
	}

	public function getPositionProduit() {
		if ($this->produitBoutique != null) {
			return $this->produitBoutique[PRODUIT_BOUTIQUE_POSITION_DANS_CATEGORIE];
		}
		return "1";
	}

	public function getOptionsCategorieProduit() {
		$html = "";

		$categories = $this->categorieProduitBoutiqueDAO->getAll();
		if (sizeof($categories) > 0) {
			foreach ($categories as $categorie) {
				$selected = "";
				if ($this->produitBoutique[PRODUIT_BOUTIQUE_CATEGORIE_PRODUIT_BOUTIQUE_ID] == $categorie[CATEGORIE_PRODUIT_BOUTIQUE_ID]) {
					$selected = " selected";
				}
				$html .= "<option value=\"".$categorie[CATEGORIE_PRODUIT_BOUTIQUE_ID]."\"".$selected.">".$categorie[CATEGORIE_PRODUIT_BOUTIQUE_NOM]."</option>";
			}
		}

		return $html;
	}

	public function getOptionsFournisseurProduit() {
		$html = "";

		$fournisseurs = $this->fournisseurProduitBoutiqueDAO->getAll(FOURNISSEUR_PRODUIT_BOUTIQUE_TABLE_NAME.".".FOURNISSEUR_PRODUIT_BOUTIQUE_NOM);
		if (sizeof($fournisseurs) > 0) {
			foreach ($fournisseurs as $fournisseur) {
				$selected = "";
				if ($this->produitBoutique[PRODUIT_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID] == $fournisseur[FOURNISSEUR_PRODUIT_BOUTIQUE_ID]) {
					$selected = " selected";
				}
				$html .= "<option value=\"".$fournisseur[FOURNISSEUR_PRODUIT_BOUTIQUE_ID]."\"".$selected.">".$fournisseur[FOURNISSEUR_PRODUIT_BOUTIQUE_NOM]."</option>";
			}
		}

		return $html;
	}

	public function getOptionsMarqueProduit() {
		$html = "";

		$marques = $this->marqueProduitBoutiqueDAO->getAll(MARQUE_PRODUIT_BOUTIQUE_TABLE_NAME.".".MARQUE_PRODUIT_BOUTIQUE_NOM);
		if (sizeof($marques) > 0) {
			foreach ($marques as $marque) {
				$selected = "";
				if ($this->produitBoutique[PRODUIT_BOUTIQUE_MARQUE_PRODUIT_BOUTIQUE_ID] == $marque[MARQUE_PRODUIT_BOUTIQUE_ID]) {
					$selected = " selected";
				}
				$html .= "<option value=\"".$marque[MARQUE_PRODUIT_BOUTIQUE_ID]."\"".$selected.">".$marque[MARQUE_PRODUIT_BOUTIQUE_NOM]."</option>";
			}
		}

		return $html;
	}

	public function getOptionsTaillesPossiblesProduit() {
		$html = "";

		$taillesPossibles = $this->taillesPossiblesProduitBoutiqueDAO->getAll();
		if (sizeof($taillesPossibles) > 0) {
			foreach ($taillesPossibles as $taillePossible) {
				$selected = "";
				if ($this->produitBoutique[PRODUIT_BOUTIQUE_TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_ID] == $taillePossible[MARQUE_PRODUIT_BOUTIQUE_ID]) {
					$selected = " selected";
				}
				$html .= "<option value=\"".$taillePossible[TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_ID]."\"".$selected.">".str_replace(TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TAILLES_SEPARATOR, " - ", $taillePossible[TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TAILLES_POSSIBLES])."</option>";
			}
		}

		return $html;
	}

	public function getParamIdProduit() {
		if ($this->produitBoutique != null) {
			return "?id=".$this->produitBoutique[PRODUIT_BOUTIQUE_ID];
		}
		return "";
	}

	public function getProduitBoutique() {
		return $this->produitBoutique;
	}
}

?>