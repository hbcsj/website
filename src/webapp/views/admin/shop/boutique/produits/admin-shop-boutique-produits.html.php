<?php
require_once("core/php/resources/page.php");
require_once("common/php/lib/admin-utils.php");

$page = new Page(
    "webapp/views/admin/shop/boutique/produits/", 
    "admin-shop-boutique-produits", 
    "AdminShopBoutiqueProduitsCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/admin/common/nav/nav.html.php");
if (isAdminConnected_commercial()) {
    require_once("webapp/views/admin/shop/sub-menu/sub-menu-admin-shop.html.php");
}
?>

<div class="container-fluid animated fadeIn">
    <?php
    if (isAdminConnected_commercial()) {
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <?php require_once("webapp/views/admin/shop/boutique/header/header-admin-shop-boutique.html.php"); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h2>
                    Produits
                    <button id="produits__bt-ajouter" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-plus"></span>
                        </div>
                        <div class="button__text">Ajouter un produit</div>
                    </button>
                </h2>
            </div>
        </div>
        <div id="container-stats-produits" class="container-stats"></div>
        <div id="container-recherche-produits" class="container-recherche"></div>
        <div id="container-table-produits" class="container-table"></div>
        <iframe name="uploader-produit-boutique" class="iframe-uploader" src="#"></iframe>
        <?php
    } else {
        require_once("webapp/views/common/mdp-form/mdp-form.html.php");
    }
    ?>
</div>

<?php
require_once("webapp/views/common/modals/alert/modal-alert.html.php");
require_once("webapp/views/common/modals/prompt/modal-prompt.html.php");

if (isAdminConnected_commercial()) {
    require_once("webapp/views/common/modals/img/modal-img.html.php");
    require_once("webapp/views/admin/shop/boutique/produits/modals/save/modal-save-produit-boutique.html.php");
    require_once("webapp/views/admin/shop/boutique/produits/modals/prix/modal-prix-produit-boutique.html.php");
}

$page->finalizePage();
?>