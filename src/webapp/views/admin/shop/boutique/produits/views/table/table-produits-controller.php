<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/produit-boutique-dao.php");
require_once("common/php/dao/prix-produit-boutique-dao.php");
require_once("common/php/dao/categorie-produit-boutique-dao.php");
require_once("common/php/dao/marque-produit-boutique-dao.php");
require_once("common/php/dao/tailles-possibles-produit-boutique-dao.php");
require_once("common/php/dao/fournisseur-produit-boutique-dao.php");
require_once("common/php/dao/commande-item-boutique-dao.php");

class TableProduitsCtrl extends AbstractViewCtrl {

	private $produitBoutiqueDAO;
	private $prixProduitBoutiqueDAO;
	private $categorieProduitBoutiqueDAO;
	private $marqueProduitBoutiqueDAO;
	private $taillesPossiblesProduitBoutiqueDAO;
	private $fournisseurProduitBoutiqueDAO;
	private $commandeItemBoutiqueDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_commercial()) {
			$this->prixProduitBoutiqueDAO = new PrixProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->produitBoutiqueDAO = new ProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->categorieProduitBoutiqueDAO = new CategorieProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->marqueProduitBoutiqueDAO = new MarqueProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->taillesPossiblesProduitBoutiqueDAO = new TaillesPossiblesProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->fournisseurProduitBoutiqueDAO = new FournisseurProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->commandeItemBoutiqueDAO = new CommandeItemBoutiqueDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getProduits() {
		return $this->produitBoutiqueDAO->getAllByParams(
			trim(strtolower($_GET["nom"])),
			trim(strtolower($_GET["reference"])),
			$_GET["flocable"],
			$_GET["enVente"],
			$_GET["marqueProduitBoutiqueId"],
			$_GET["fournisseurProduitBoutiqueId"],
			$_GET["categorieProduitBoutiqueId"],
			PRODUIT_BOUTIQUE_TABLE_NAME.".".PRODUIT_BOUTIQUE_NOM.", ".PRODUIT_BOUTIQUE_TABLE_NAME.".".PRODUIT_BOUTIQUE_INDICATION.", ".PRODUIT_BOUTIQUE_TABLE_NAME.".".PRODUIT_BOUTIQUE_CATEGORIE_PRODUIT_BOUTIQUE_ID
		);
	}

	public function getPrixProduit($produitId) {
		return $this->prixProduitBoutiqueDAO->getByProduitId($produitId);
	}

	public function getNomCategorie($categorieId) {
		$categorie = $this->categorieProduitBoutiqueDAO->getById($categorieId);
		return $categorie[CATEGORIE_PRODUIT_BOUTIQUE_NOM];
	}

	public function getNomMarque($marqueId) {
		$marque = $this->marqueProduitBoutiqueDAO->getById($marqueId);
		return $marque[MARQUE_PRODUIT_BOUTIQUE_NOM];
	}

	public function getTaillesPossibles($taillesPossiblesId) {
		$taillesPossibles = $this->taillesPossiblesProduitBoutiqueDAO->getById($taillesPossiblesId);
		return explode(TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TAILLES_SEPARATOR, $taillesPossibles[TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TAILLES_POSSIBLES]);
	}

	public function getNomFournisseur($fournisseurId) {
		$fournisseur = $this->fournisseurProduitBoutiqueDAO->getById($fournisseurId);
		return $fournisseur[FOURNISSEUR_PRODUIT_BOUTIQUE_NOM];
	}

	public function hasCommandesAssociees($produitBoutiqueId) {
		return (sizeof($this->commandeItemBoutiqueDAO->getByProduitBoutiqueId($produitBoutiqueId)) > 0);
	}
}

?>