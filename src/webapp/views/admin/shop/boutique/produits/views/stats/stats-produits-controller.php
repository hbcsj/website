<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/produit-boutique-dao.php");

class StatsProduitsCtrl extends AbstractViewCtrl {

	private $produitBoutiqueDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_commercial()) {
			$this->produitBoutiqueDAO = new ProduitBoutiqueDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getNbProduits() {
		return $this->produitBoutiqueDAO->getNbTotal()[PRODUIT_BOUTIQUE_NB_PRODUITS];
	}

	public function getNbProduitsEnVente() {
		return $this->produitBoutiqueDAO->getNbEnVente()[PRODUIT_BOUTIQUE_NB_PRODUITS];
	}
}

?>