<?php
require_once("core/php/resources/view.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/path-utils.php");

$view = new View(
    "webapp/views/admin/shop/boutique/produits/views/table/", 
    "table-produits", 
    "TableProduitsCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
    $produits = $ctrl->getProduits();
    ?>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>&nbsp;</th>
                <th>Nom</th>
                <th>Cat&eacute;g.</th>
                <th>Marque</th>
                <th>Fourniss.</th>
                <th>R&eacute;f.</th>
                <th>Couleurs</th>
                <th>Tailles</th>
                <th>Prix</th>
                <th>Flocage</th>
                <th class="text-center">En vente ?</th>
                <th colspan="2">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (sizeof($produits) > 0) {
                foreach ($produits as $produit) {
                    $prix = $ctrl->getPrixProduit($produit[PRODUIT_BOUTIQUE_ID]);
                    
                    $bg = "";
                    if ($produit[PRODUIT_BOUTIQUE_EN_VENTE] != 1) {
                        $bg = "bg-error";
                    } else if (sizeof($prix) == 0) {
                        $bg = "bg-warning";
                    }
                    ?>
                    <tr>
                        <td class="<?php echo $bg; ?>">
                            <img src="<?php echo SRC_PATH.PathUtils::getProduitBoutiqueImgFile($produit[PRODUIT_BOUTIQUE_ID])."?".time(); ?>" img-modal="<?php echo SRC_PATH.PathUtils::getProduitBoutiqueImgFile($produit[PRODUIT_BOUTIQUE_ID]); ?>" class="img-produit">
                        </td>
                        <td class="<?php echo $bg; ?>">
                            <?php
                            echo $produit[PRODUIT_BOUTIQUE_NOM];
                            if ($produit[PRODUIT_BOUTIQUE_INDICATION] != "") {
                                echo "<br>".$produit[PRODUIT_BOUTIQUE_INDICATION];
                            }
                            ?>
                        </td>
                        <td class="<?php echo $bg; ?>"><?php echo $ctrl->getNomCategorie($produit[PRODUIT_BOUTIQUE_CATEGORIE_PRODUIT_BOUTIQUE_ID]); ?></td>
                        <td class="<?php echo $bg; ?>"><?php echo $ctrl->getNomMarque($produit[PRODUIT_BOUTIQUE_MARQUE_PRODUIT_BOUTIQUE_ID]); ?></td>
                        <td class="<?php echo $bg; ?>"><?php echo $ctrl->getNomFournisseur($produit[PRODUIT_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID]); ?></td>
                        <td class="<?php echo $bg; ?>"><?php echo str_replace(PRODUIT_BOUTIQUE_COULEURS_REFERENCES_SEPARATOR, "<br>", $produit[PRODUIT_BOUTIQUE_REFERENCES]); ?></td>
                        <td class="<?php echo $bg; ?>"><?php echo str_replace(PRODUIT_BOUTIQUE_COULEURS_REFERENCES_SEPARATOR, "<br>", $produit[PRODUIT_BOUTIQUE_COULEURS]); ?></td>
                        <td class="<?php echo $bg; ?>"><?php echo implode("<br>", $ctrl->getTaillesPossibles($produit[PRODUIT_BOUTIQUE_TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_ID])); ?></td>
                        <td class="<?php echo $bg; ?>">
                            <button class="btn btn-default bt-table-admin bt-prix-produit-boutique" id-produit="<?php echo $produit[PRODUIT_BOUTIQUE_ID]; ?>">
                                <div class="button__text">G&eacute;rer les prix</div>
                            </button>
                            <?php
                            if (sizeof($prix) > 0) {
                                foreach ($prix as $prixItem) {
                                    ?>
                                    <div class="div-taille">
                                        <?php
                                        if (sizeof($prix) > 1 || $prixItem[PRIX_PRODUIT_BOUTIQUE_TAILLE] != "") {
                                            echo "<b>Taille '".$prixItem[PRIX_PRODUIT_BOUTIQUE_TAILLE]."'</b><br>";
                                        }
                                        ?>
                                        Notre prix : <?php echo $prixItem[PRIX_PRODUIT_BOUTIQUE_PRIX] ?> &euro;<br>
                                        Prix fourn. : <?php echo $prixItem[PRIX_PRODUIT_BOUTIQUE_PRIX_FOURNISSEUR] ?> &euro;
                                    </div>
                                    <?php
                                }
                            } else {
                                echo "<div class=\"div-taille\"><b>Aucun prix saisi</b></div>";
                            }
                            ?>
                        </td>
                        <td class="<?php echo $bg; ?>">
                            <?php echo ($produit[PRODUIT_BOUTIQUE_FLOCAGE_LOGO_INCLUS] == 1) ? "<div>Flocage logo inclus</div>" : ""; ?>
                            <?php echo ($produit[PRODUIT_BOUTIQUE_FLOCABLE_NOM] == 1) ? "<div>Flocage du nom : ".$produit[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_NOM]." &euro; (fourn. : ".$produit[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_FOURNISSEUR_NOM]." &euro;)</div>" : ""; ?>
                            <?php echo ($produit[PRODUIT_BOUTIQUE_FLOCABLE_NUMERO_ARRIERE] == 1) ? "<div>Flocage du num. arri&egrave;re : ".$produit[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_NUMERO_ARRIERE]." &euro; (fourn. : ".$produit[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_FOURNISSEUR_NUMERO_ARRIERE]." &euro;)</div>" : ""; ?>
                            <?php echo ($produit[PRODUIT_BOUTIQUE_FLOCABLE_NUMERO_AVANT] == 1) ? "<div>Flocage du num. avant : ".$produit[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_NUMERO_AVANT]." &euro; (fourn. : ".$produit[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_FOURNISSEUR_NUMERO_AVANT]." &euro;)</div>" : ""; ?>
                            <?php echo ($produit[PRODUIT_BOUTIQUE_FLOCABLE_INITIALES] == 1) ? "<div>Flocage des initiales : ".$produit[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_INITIALES]." &euro; (fourn. : ".$produit[PRODUIT_BOUTIQUE_PRIX_FLOCAGE_FOURNISSEUR_INITIALES]." &euro;)</div>" : ""; ?>
                        </td>
                        <td class="text-center <?php echo $bg; ?>"><?php echo ($produit[PRODUIT_BOUTIQUE_EN_VENTE] == 1) ? "Oui" : "Non"; ?></td>
                        <td class="text-center <?php echo $bg; ?>">
                            <span class="glyphicon glyphicon-pencil clickable" title="Editer" id-produit="<?php echo $produit[PRODUIT_BOUTIQUE_ID]; ?>"></span>
                        </td>
                        <td class="text-center <?php echo $bg; ?>">
                            <?php
                            if (!$ctrl->hasCommandesAssociees($produit[PRODUIT_BOUTIQUE_ID])) {
                                ?>
                                <span class="glyphicon glyphicon-trash clickable" title="Supprimer" id-produit="<?php echo $produit[PRODUIT_BOUTIQUE_ID]; ?>"></span>
                                <div class="loader loader-delete" id-produit="<?php echo $produit[PRODUIT_BOUTIQUE_ID]; ?>"></div>
                                <?php
                            } else {
                                echo "&nbsp;";
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>