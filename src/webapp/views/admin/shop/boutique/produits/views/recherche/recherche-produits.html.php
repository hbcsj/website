<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/shop/boutique/produits/views/recherche/", 
    "recherche-produits", 
    "RechercheProduitsCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
    $categories = $ctrl->getCategories();
    $marques = $ctrl->getMarques();
    $fournisseurs = $ctrl->getFournisseurs();
    ?>
    <div class="container-recherche__title" title="Ouvrir le formulaire de recherche" for="form-recherche-produits">
        <div class="container-recherche__title__icon">
            <span class="glyphicon glyphicon-search"></span>
        </div>
        <div class="container-recherche__title__text">Recherche</div>
        <div class="container-recherche__title__arrow">
            <span class="glyphicon glyphicon-triangle-bottom"></span>
        </div>
    </div>
    <div class="form-horizontal" id="form-recherche-produits">
        <div class="form-group">
            <label for="form-recherche-produits__input-nom" class="col-xs-2 col-xs-offset-1 control-label">Nom :</label>
            <div class="col-xs-3 form-input" for="form-recherche-produits__input-nom">
                <input type="text" class="form-control" id="form-recherche-produits__input-nom">
            </div>
            <label for="form-recherche-produits__input-reference" class="col-xs-1 control-label">R&eacute;f :</label>
            <div class="col-xs-3 form-input" for="form-recherche-produits__input-reference">
                <input type="text" class="form-control" id="form-recherche-produits__input-reference">
            </div>
        </div>
        <div class="form-group">
            <label for="form-recherche-produits__input-is-flocable" class="col-xs-2 col-xs-offset-1 control-label">Flocable ?</label>
            <div class="col-xs-1 form-input" for="form-recherche-produits__input-is-flocable">
                <select class="form-control" id="form-recherche-produits__input-is-flocable">
                    <option value=""></option>
                    <option value="1">Oui</option>
                    <option value="0">Non</option>
                </select>
            </div>
            <label for="form-recherche-produits__input-is-en-vente" class="col-xs-1 col-xs-offset-2 control-label">En vente ?</label>
            <div class="col-xs-1 form-input" for="form-recherche-produits__input-is-en-vente">
                <select class="form-control" id="form-recherche-produits__input-is-en-vente">
                    <option value=""></option>
                    <option value="1">Oui</option>
                    <option value="0">Non</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="form-recherche-produits__input-marque" class="col-xs-2 col-xs-offset-1 control-label">Marque :</label>
            <div class="col-xs-3 form-input" for="form-recherche-produits__input-marque">
                <select class="form-control" id="form-recherche-produits__input-marque">
                    <option value=""></option>
                    <?php
                    if (sizeof($marques) > 0) {
                        foreach ($marques as $marque) {
                            ?>
                            <option value="<?php echo $marque[MARQUE_PRODUIT_BOUTIQUE_ID]; ?>"><?php echo $marque[MARQUE_PRODUIT_BOUTIQUE_NOM]; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <label for="form-recherche-produits__input-fournisseur" class="col-xs-1 control-label">Fournisseur :</label>
            <div class="col-xs-3 form-input" for="form-recherche-produits__input-fournisseur">
                <select class="form-control" id="form-recherche-produits__input-fournisseur">
                    <option value=""></option>
                    <?php
                    if (sizeof($fournisseurs) > 0) {
                        foreach ($fournisseurs as $fournisseur) {
                            ?>
                            <option value="<?php echo $fournisseur[FOURNISSEUR_PRODUIT_BOUTIQUE_ID]; ?>"><?php echo $fournisseur[FOURNISSEUR_PRODUIT_BOUTIQUE_NOM]; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="form-recherche-produits__input-categorie" class="col-xs-2 col-xs-offset-1 control-label">Cat&eacute;gorie :</label>
            <div class="col-xs-3 form-input" for="form-recherche-produits__input-categorie">
                <select class="form-control" id="form-recherche-produits__input-categorie">
                    <option value=""></option>
                    <?php
                    if (sizeof($categories) > 0) {
                        foreach ($categories as $categorie) {
                            ?>
                            <option value="<?php echo $categorie[CATEGORIE_PRODUIT_BOUTIQUE_ID]; ?>"><?php echo $categorie[CATEGORIE_PRODUIT_BOUTIQUE_NOM]; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
    <div class="container-recherche__footer" for="form-recherche-produits">
        <button id="recherche-produits__bt-rechercher" class="btn btn-default">
            <div class="button__icon">
                <span class="glyphicon glyphicon-search"></span>
            </div>
            <div class="button__text">Chercher</div>
        </button>
        <button id="recherche-produits__bt-reset" class="btn btn-default">
            <div class="button__icon">
                <span class="glyphicon glyphicon-erase"></span>
            </div>
            <div class="button__text">Reset</div>
        </button>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>