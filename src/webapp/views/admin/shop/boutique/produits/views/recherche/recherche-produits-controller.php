<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/categorie-produit-boutique-dao.php");
require_once("common/php/dao/marque-produit-boutique-dao.php");
require_once("common/php/dao/fournisseur-produit-boutique-dao.php");

class RechercheProduitsCtrl extends AbstractViewCtrl {

	private $categorieProduitBoutiqueDAO;
	private $marqueProduitBoutiqueDAO;
	private $fournisseurProduitBoutiqueDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_commercial()) {
			$this->categorieProduitBoutiqueDAO = new CategorieProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->marqueProduitBoutiqueDAO = new MarqueProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->fournisseurProduitBoutiqueDAO = new FournisseurProduitBoutiqueDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getCategories() {
		return $this->categorieProduitBoutiqueDAO->getAll();
	}

	public function getMarques() {
		return $this->marqueProduitBoutiqueDAO->getAll(MARQUE_PRODUIT_BOUTIQUE_TABLE_NAME.".".MARQUE_PRODUIT_BOUTIQUE_NOM);
	}

	public function getFournisseurs() {
		return $this->fournisseurProduitBoutiqueDAO->getAll(FOURNISSEUR_PRODUIT_BOUTIQUE_TABLE_NAME.".".FOURNISSEUR_PRODUIT_BOUTIQUE_NOM);
	}
}

?>