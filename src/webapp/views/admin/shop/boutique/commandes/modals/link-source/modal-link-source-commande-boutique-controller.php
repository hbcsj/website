<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/commande-item-boutique-dao.php");
require_once("common/php/dao/commande-dao.php");
require_once("common/php/dao/type-source-produit-boutique-dao.php");

class ModalLinkSourceCommandeBoutiqueCtrl extends AbstractViewCtrl {
	
	private $commandeItemBoutiqueDAO;
	private $commandeDAO;
	private $typeSourceProduitBoutiqueDAO;

	private $commandeItemBoutique;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idCommandeItemBoutique" => $_GET["idCommandeItemBoutique"]
		), true);
		
		if (isAdminConnected_commercial()) {
			$this->commandeItemBoutiqueDAO = new CommandeItemBoutiqueDAO($this->getDatabaseConnection());
			$this->commandeDAO = new CommandeDAO($this->getDatabaseConnection());
			$this->typeSourceProduitBoutiqueDAO = new TypeSourceProduitBoutiqueDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdCommandeItemBoutique();
	}

	private function checkIdCommandeItemBoutique() {
		$this->commandeItemBoutique = $this->commandeItemBoutiqueDAO->getById($_GET["idCommandeItemBoutique"]);

		if ($this->commandeItemBoutique == null) {
			$this->sendCheckError(
				HTTP_404, 
				"La commande boutique (idCommandeItemBoutique = '".$_GET["idCommandeItemBoutique"]."') n'existe pas", 
				"webapp/views/common/error/404/404.html.php"
			);
		}
	}

	public function getOptionsSource() {
		$html = "";

		$sources = $this->typeSourceProduitBoutiqueDAO->getAll();
		if (sizeof($sources) > 0) {
			foreach ($sources as $source) {
				$selected = "";
				if ($this->commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_TYPE_SOURCE_PRODUIT_BOUTIQUE_ID] == $source[TYPE_SOURCE_PRODUIT_BOUTIQUE_ID]) {
					$selected = " selected";
				}
				$html .= "<option value=\"".$source[TYPE_SOURCE_PRODUIT_BOUTIQUE_ID]."\"".$selected.">".$source[TYPE_SOURCE_PRODUIT_BOUTIQUE_LIBELLE]."</option>";
			}
		}

		return $html;
	}

	public function getReferenceCommande() {
		$commande = $this->commandeDAO->getById($this->commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_COMMANDE_ID]);
		return $commande[COMMANDE_REFERENCE];
	}
}

?>