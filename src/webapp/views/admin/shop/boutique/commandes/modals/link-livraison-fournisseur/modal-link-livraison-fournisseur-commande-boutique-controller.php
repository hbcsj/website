<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/dao/commande-item-boutique-dao.php");
require_once("common/php/dao/commande-dao.php");
require_once("common/php/dao/livraison-fournisseur-boutique-dao.php");
require_once("common/php/dao/fournisseur-produit-boutique-dao.php");

class ModalLinkLivraisonFournisseurCommandeBoutiqueCtrl extends AbstractViewCtrl {
	
	private $commandeItemBoutiqueDAO;
	private $commandeDAO;
	private $livraisonFournisseurBoutiqueDAO;
	private $fournisseurProduitBoutiqueDAO;

	private $commandeItemBoutique;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idCommandeItemBoutique" => $_GET["idCommandeItemBoutique"]
		), true);
		
		if (isAdminConnected_commercial()) {
			$this->commandeItemBoutiqueDAO = new CommandeItemBoutiqueDAO($this->getDatabaseConnection());
			$this->commandeDAO = new CommandeDAO($this->getDatabaseConnection());
			$this->livraisonFournisseurBoutiqueDAO = new LivraisonFournisseurBoutiqueDAO($this->getDatabaseConnection());
			$this->fournisseurProduitBoutiqueDAO = new FournisseurProduitBoutiqueDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdCommandeItemBoutique();
	}

	private function checkIdCommandeItemBoutique() {
		$this->commandeItemBoutique = $this->commandeItemBoutiqueDAO->getById($_GET["idCommandeItemBoutique"]);

		if ($this->commandeItemBoutique == null) {
			$this->sendCheckError(
				HTTP_404, 
				"La commande boutique (idCommandeItemBoutique = '".$_GET["idCommandeItemBoutique"]."') n'existe pas", 
				"webapp/views/common/error/404/404.html.php"
			);
		}
	}

	public function getOptionsLivraisonFournisseur() {
		$html = "";

		$livraisonsFournisseur = $this->livraisonFournisseurBoutiqueDAO->getAll(LIVRAISON_FOURNISSEUR_BOUTIQUE_TABLE_NAME.".".LIVRAISON_FOURNISSEUR_BOUTIQUE_DATE." DESC");
		if (sizeof($livraisonsFournisseur) > 0) {
			foreach ($livraisonsFournisseur as $livraisonFournisseur) {
				$selected = "";
				if ($this->commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_LIVRAISON_FOURNISSEUR_BOUTIQUE_ID] == $livraisonFournisseur[LIVRAISON_FOURNISSEUR_BOUTIQUE_ID]) {
					$selected = " selected";
				}
				$fournisseur = $this->fournisseurProduitBoutiqueDAO->getById($livraisonFournisseur[LIVRAISON_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID]);
				$html .= "<option value=\"".$livraisonFournisseur[LIVRAISON_FOURNISSEUR_BOUTIQUE_ID]."\"".$selected.">".DateUtils::convert_sqlDate_to_slashDate($livraisonFournisseur[LIVRAISON_FOURNISSEUR_BOUTIQUE_DATE])." - ".$fournisseur[FOURNISSEUR_PRODUIT_BOUTIQUE_NOM]."</option>";
			}
		}

		return $html;
	}

	public function getReferenceCommande() {
		$commande = $this->commandeDAO->getById($this->commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_COMMANDE_ID]);
		return $commande[COMMANDE_REFERENCE];
	}
}

?>