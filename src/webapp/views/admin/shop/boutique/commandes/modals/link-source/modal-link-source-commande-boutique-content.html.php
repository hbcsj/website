<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/shop/boutique/commandes/modals/link-source/", 
    "modal-link-source-commande-boutique", 
    "ModalLinkSourceCommandeBoutiqueCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
?>
    <div class="modal-header">
        <div class="modal-title">Associer une source &agrave; la commande '<?php echo $ctrl->getReferenceCommande(); ?>'</div>
    </div>
    <div class="modal-body">
        <div class="form-horizontal" id="form-link-source-commande-boutique">
            <div class="form-group">
                <label for="form-link-source-commande-boutique__input-source" class="col-xs-4 control-label">Source :</label>
                <div class="col-xs-7 form-input" for="form-link-source-commande-boutique__input-source">
                    <select class="form-control" id="form-link-source-commande-boutique__input-source">
                        <?php echo $ctrl->getOptionsSource(); ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-link-source-commande-boutique__input-source"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-8">
                <div class="form-result-message"></div>
            </div>
            <div class="col-xs-4 text-right">
                <button id="modal-link-source-commande-boutique__bt-sauvegarder" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-ok"></span>
                    </div>
                    <div class="button__text">Sauvegarder</div>
                    <div class="loader"></div>
                </button>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>