<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/dao/commande-item-boutique-dao.php");
require_once("common/php/dao/commande-dao.php");
require_once("common/php/dao/commande-fournisseur-boutique-dao.php");
require_once("common/php/dao/fournisseur-produit-boutique-dao.php");

class ModalLinkCommandeFournisseurCommandeBoutiqueCtrl extends AbstractViewCtrl {
	
	private $commandeItemBoutiqueDAO;
	private $commandeDAO;
	private $commandeFournisseurBoutiqueDAO;
	private $fournisseurProduitBoutiqueDAO;

	private $commandeItemBoutique;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idCommandeItemBoutique" => $_GET["idCommandeItemBoutique"]
		), true);
		
		if (isAdminConnected_commercial()) {
			$this->commandeItemBoutiqueDAO = new CommandeItemBoutiqueDAO($this->getDatabaseConnection());
			$this->commandeDAO = new CommandeDAO($this->getDatabaseConnection());
			$this->commandeFournisseurBoutiqueDAO = new CommandeFournisseurBoutiqueDAO($this->getDatabaseConnection());
			$this->fournisseurProduitBoutiqueDAO = new FournisseurProduitBoutiqueDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdCommandeItemBoutique();
	}

	private function checkIdCommandeItemBoutique() {
		$this->commandeItemBoutique = $this->commandeItemBoutiqueDAO->getById($_GET["idCommandeItemBoutique"]);

		if ($this->commandeItemBoutique == null) {
			$this->sendCheckError(
				HTTP_404, 
				"La commande boutique (idCommandeItemBoutique = '".$_GET["idCommandeItemBoutique"]."') n'existe pas", 
				"webapp/views/common/error/404/404.html.php"
			);
		}
	}

	public function getOptionsCommandeFournisseur() {
		$html = "";

		$commandesFournisseur = $this->commandeFournisseurBoutiqueDAO->getAll(COMMANDE_FOURNISSEUR_BOUTIQUE_TABLE_NAME.".".COMMANDE_FOURNISSEUR_BOUTIQUE_DATE." DESC");
		if (sizeof($commandesFournisseur) > 0) {
			foreach ($commandesFournisseur as $commandeFournisseur) {
				$selected = "";
				if ($this->commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_COMMANDE_FOURNISSEUR_BOUTIQUE_ID] == $commandeFournisseur[COMMANDE_FOURNISSEUR_BOUTIQUE_ID]) {
					$selected = " selected";
				}
				$fournisseur = $this->fournisseurProduitBoutiqueDAO->getById($commandeFournisseur[COMMANDE_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID]);
				$html .= "<option value=\"".$commandeFournisseur[COMMANDE_FOURNISSEUR_BOUTIQUE_ID]."\"".$selected.">".DateUtils::convert_sqlDate_to_slashDate($commandeFournisseur[COMMANDE_FOURNISSEUR_BOUTIQUE_DATE])." - ".$fournisseur[FOURNISSEUR_PRODUIT_BOUTIQUE_NOM]."</option>";
			}
		}

		return $html;
	}

	public function getReferenceCommande() {
		$commande = $this->commandeDAO->getById($this->commandeItemBoutique[COMMANDE_ITEM_BOUTIQUE_COMMANDE_ID]);
		return $commande[COMMANDE_REFERENCE];
	}
}

?>