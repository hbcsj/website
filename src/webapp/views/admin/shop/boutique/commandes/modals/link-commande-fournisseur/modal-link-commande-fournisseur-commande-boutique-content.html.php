<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/shop/boutique/commandes/modals/link-commande-fournisseur/", 
    "modal-link-commande-fournisseur-commande-boutique", 
    "ModalLinkCommandeFournisseurCommandeBoutiqueCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
?>
    <div class="modal-header">
        <div class="modal-title">Associer une commande fourn. &agrave; la commande '<?php echo $ctrl->getReferenceCommande(); ?>'</div>
    </div>
    <div class="modal-body">
        <div class="form-horizontal" id="form-link-commande-fournisseur-commande-boutique">
            <div class="form-group">
                <label for="form-link-commande-fournisseur-commande-boutique__input-commande-fournisseur" class="col-xs-4 control-label">Com<sup>de</sup> fourn. :</label>
                <div class="col-xs-7 form-input" for="form-link-commande-fournisseur-commande-boutique__input-commande-fournisseur">
                    <select class="form-control" id="form-link-commande-fournisseur-commande-boutique__input-commande-fournisseur">
                        <option value=""></option>
                        <?php echo $ctrl->getOptionsCommandeFournisseur(); ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-link-commande-fournisseur-commande-boutique__input-commande-fournisseur"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-8">
                <div class="form-result-message"></div>
            </div>
            <div class="col-xs-4 text-right">
                <button id="modal-link-commande-fournisseur-commande-boutique__bt-sauvegarder" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-ok"></span>
                    </div>
                    <div class="button__text">Sauvegarder</div>
                    <div class="loader"></div>
                </button>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>