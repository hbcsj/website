<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/shop/boutique/commandes/modals/link-livraison-fournisseur/", 
    "modal-link-livraison-fournisseur-commande-boutique", 
    "ModalLinkLivraisonFournisseurCommandeBoutiqueCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
?>
    <div class="modal-header">
        <div class="modal-title">Associer une livraison fourn. &agrave; la commande '<?php echo $ctrl->getReferenceCommande(); ?>'</div>
    </div>
    <div class="modal-body">
        <div class="form-horizontal" id="form-link-livraison-fournisseur-commande-boutique">
            <div class="form-group">
                <label for="form-link-livraison-fournisseur-commande-boutique__input-livraison-fournisseur" class="col-xs-4 control-label">Liv<sup>son</sup> fourn. :</label>
                <div class="col-xs-7 form-input" for="form-link-livraison-fournisseur-commande-boutique__input-livraison-fournisseur">
                    <select class="form-control" id="form-link-livraison-fournisseur-commande-boutique__input-livraison-fournisseur">
                        <option value=""></option>
                        <?php echo $ctrl->getOptionsLivraisonFournisseur(); ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-link-livraison-fournisseur-commande-boutique__input-livraison-fournisseur"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-8">
                <div class="form-result-message"></div>
            </div>
            <div class="col-xs-4 text-right">
                <button id="modal-link-livraison-fournisseur-commande-boutique__bt-sauvegarder" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-ok"></span>
                    </div>
                    <div class="button__text">Sauvegarder</div>
                    <div class="loader"></div>
                </button>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>