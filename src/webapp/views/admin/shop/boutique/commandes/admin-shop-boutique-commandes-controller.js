AdminShopBoutiqueCommandesController = {};

AdminShopBoutiqueCommandesController.dateDebutRecherche = null;

AdminShopBoutiqueCommandesController.load_StatsCommandes = function() {
    if (Utils.exists('#container-stats-commandes')) {
        AjaxUtils.loadView(
            'admin/shop/boutique/commandes/views/stats/stats-commandes',
            '#container-stats-commandes',
            null,
            function() {},
            '#loader-page'
        );
    }
};

AdminShopBoutiqueCommandesController.load_RechercheAndTableCommandes = function() {
    if (Utils.exists('#container-recherche-commandes')) {
        AjaxUtils.loadView(
            'admin/shop/boutique/commandes/views/recherche/recherche-commandes',
            '#container-recherche-commandes',
            null,
            function() {
                $('#form-recherche-commandes__input-date-debut').val(AdminShopBoutiqueCommandesController.dateDebutRecherche);

                FormRechercheUtils.control_toggleFormRecherche();
                onClick_btRechercher();
                onClick_btReset();
                AdminShopBoutiqueCommandesController.load_TableCommandes();
            },
            '#loader-page'
        );
    }

    function onClick_btRechercher() {
        $('#recherche-commandes__bt-rechercher').unbind('click');
        $('#recherche-commandes__bt-rechercher').click(function() {
            AdminShopBoutiqueCommandesController.load_TableCommandes();
        });
    }

    function onClick_btReset() {
        $('#recherche-commandes__bt-reset').unbind('click');
        $('#recherche-commandes__bt-reset').click(function() {
            $('#form-recherche-commandes__input-reference').val('');
            $('#form-recherche-commandes__input-produit').val('');
            $('#form-recherche-commandes__input-nom').val('');
            $('#form-recherche-commandes__input-date-debut').val(AdminShopBoutiqueCommandesController.dateDebutRecherche);
            $('#form-recherche-commandes__input-date-fin').val('');
            $('#form-recherche-commandes__input-is-donne').val('');
            $('#form-recherche-commandes__input-source').val('');
            $('#form-recherche-commandes__input-commande-fournisseur').val('');
            $('#form-recherche-commandes__input-livraison-fournisseur').val('');

            AdminShopBoutiqueCommandesController.load_TableCommandes();
        });
    }
};

AdminShopBoutiqueCommandesController.load_TableCommandes = function() {
    if (Utils.exists('#container-table-commandes')) {
        var params = {
            reference: $.trim($('#form-recherche-commandes__input-reference').val()),
            produitId: $.trim($('#form-recherche-commandes__input-produit').val()),
            nom: $.trim($('#form-recherche-commandes__input-nom').find('option:selected').attr('value-nom')),
            prenom: $.trim($('#form-recherche-commandes__input-nom').find('option:selected').attr('value-prenom')),
            dateDebut: $.trim($('#form-recherche-commandes__input-date-debut').val()),
            dateFin: $.trim($('#form-recherche-commandes__input-date-fin').val()),
            donne: $('#form-recherche-commandes__input-is-donne').val(),
            typeSourceProduitBoutiqueId: $('#form-recherche-commandes__input-source').val(),
            commandeFournisseurBoutiqueId: $('#form-recherche-commandes__input-commande-fournisseur').val(),
            livraisonFournisseurBoutiqueId: $('#form-recherche-commandes__input-livraison-fournisseur').val()
        };
        AjaxUtils.loadView(
            'admin/shop/boutique/commandes/views/table/table-commandes',
            '#container-table-commandes',
            params,
            function() {
                control_LinkCommandeFournisseur();
                control_LinkLivraisonFournisseur();
                control_LinkSource();
                control_CommandeItemDonne();
            },
            '#loader-page'
        );
    }

    function control_LinkCommandeFournisseur() {
        $('.bt-commande-item-boutique-commande-fournisseur').unbind('click');
        $('.bt-commande-item-boutique-commande-fournisseur').click(function() {
            var idCommandeItemBoutique = $(this).attr('id-commande-item-boutique');
            open_ModalLinkCommandeFournisseur(idCommandeItemBoutique);
        });

        function open_ModalLinkCommandeFournisseur(idCommandeItemBoutique) {
            var params = {
                idCommandeItemBoutique: idCommandeItemBoutique
            };
            AjaxUtils.loadView(
                'admin/shop/boutique/commandes/modals/link-commande-fournisseur/modal-link-commande-fournisseur-commande-boutique-content',
                '#modal-link-commande-fournisseur-commande-boutique-content',
                params,
                function() {
                    onClick_btSauvegarder(idCommandeItemBoutique);
                },
                '#loader-page'
            );
            $('#modal-link-commande-fournisseur-commande-boutique').modal('show');
            
            function onClick_btSauvegarder(idCommandeItemBoutique) {
                $('#modal-link-commande-fournisseur-commande-boutique__bt-sauvegarder').unbind('click');
                $('#modal-link-commande-fournisseur-commande-boutique__bt-sauvegarder').click(function() {
                    FormUtils.hideFormErrors('#modal-link-commande-fournisseur-commande-boutique #form-link-commande-fournisseur-commande-boutique');
                    ViewUtils.unsetHTMLAndHide('#modal-link-commande-fournisseur-commande-boutique .form-result-message');
                    ViewUtils.desactiveButtonAndShowLoader('#modal-link-commande-fournisseur-commande-boutique__bt-sauvegarder', '#modal-link-commande-fournisseur-commande-boutique__bt-sauvegarder .loader');
    
                    var formValidation = FormUtils.validateForm('#modal-link-commande-fournisseur-commande-boutique #form-link-commande-fournisseur-commande-boutique');
                    
                    if (formValidation) {
                        var inputCommandeFournisseur = $('#form-link-commande-fournisseur-commande-boutique__input-commande-fournisseur').val();
    
                        saveLinkCommandeFournisseurCommandeBoutique(idCommandeItemBoutique, inputCommandeFournisseur);
                    } else {
                        ViewUtils.activeButtonAndHideLoader('#modal-link-commande-fournisseur-commande-boutique__bt-sauvegarder', '#modal-link-commande-fournisseur-commande-boutique__bt-sauvegarder .loader');
                    }
                });
    
                function saveLinkCommandeFournisseurCommandeBoutique(idCommandeItemBoutique, inputCommandeFournisseur) {
                    AjaxUtils.callService(
                        'commande-boutique', 
                        'saveLinkCommandeFournisseur', 
                        AjaxUtils.dataTypes.json, 
                        AjaxUtils.requestTypes.post, 
                        {
                            id: idCommandeItemBoutique
                        }, 
                        {
                            commandeFournisseurBoutiqueId: inputCommandeFournisseur
                        }, 
                        function(response) {
                            if (response.success) {
                                FormUtils.displayFormResultMessage('#modal-link-commande-fournisseur-commande-boutique .form-result-message', 'Commande fournisseur associ&eacute;e', FormUtils.resultsTypes.ok);
                                $('#modal-link-commande-fournisseur-commande-boutique__bt-sauvegarder .loader').hide();
                                AdminShopBoutiqueCommandesController.load_StatsCommandes();
                                AdminShopBoutiqueCommandesController.load_TableCommandes();
                            } else {
                                ViewUtils.activeButtonAndHideLoader('#modal-link-commande-fournisseur-commande-boutique__bt-sauvegarder', '#modal-link-commande-fournisseur-commande-boutique__bt-sauvegarder .loader');
                                FormUtils.displayFormResultMessage('#modal-link-commande-fournisseur-commande-boutique .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
                            }
                        },
                        function(response) {
                            ViewUtils.activeButtonAndHideLoader('#modal-link-commande-fournisseur-commande-boutique__bt-sauvegarder', '#modal-link-commande-fournisseur-commande-boutique__bt-sauvegarder .loader');
                            FormUtils.displayFormResultMessage('#modal-link-commande-fournisseur-commande-boutique .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                        }
                    );
                }
            }
        }
    }

    function control_LinkLivraisonFournisseur() {
        $('.bt-commande-item-boutique-livraison-fournisseur').unbind('click');
        $('.bt-commande-item-boutique-livraison-fournisseur').click(function() {
            var idCommandeItemBoutique = $(this).attr('id-commande-item-boutique');
            open_ModalLinkCommandeFournisseur(idCommandeItemBoutique);
        });

        function open_ModalLinkCommandeFournisseur(idCommandeItemBoutique) {
            var params = {
                idCommandeItemBoutique: idCommandeItemBoutique
            };
            AjaxUtils.loadView(
                'admin/shop/boutique/commandes/modals/link-livraison-fournisseur/modal-link-livraison-fournisseur-commande-boutique-content',
                '#modal-link-livraison-fournisseur-commande-boutique-content',
                params,
                function() {
                    onClick_btSauvegarder(idCommandeItemBoutique);
                },
                '#loader-page'
            );
            $('#modal-link-livraison-fournisseur-commande-boutique').modal('show');
            
            function onClick_btSauvegarder(idCommandeItemBoutique) {
                $('#modal-link-livraison-fournisseur-commande-boutique__bt-sauvegarder').unbind('click');
                $('#modal-link-livraison-fournisseur-commande-boutique__bt-sauvegarder').click(function() {
                    FormUtils.hideFormErrors('#modal-link-livraison-fournisseur-commande-boutique #form-link-livraison-fournisseur-commande-boutique');
                    ViewUtils.unsetHTMLAndHide('#modal-link-livraison-fournisseur-commande-boutique .form-result-message');
                    ViewUtils.desactiveButtonAndShowLoader('#modal-link-livraison-fournisseur-commande-boutique__bt-sauvegarder', '#modal-link-livraison-fournisseur-commande-boutique__bt-sauvegarder .loader');
    
                    var formValidation = FormUtils.validateForm('#modal-link-livraison-fournisseur-commande-boutique #form-link-livraison-fournisseur-commande-boutique');
                    
                    if (formValidation) {
                        var inputLivraisonFournisseur = $('#form-link-livraison-fournisseur-commande-boutique__input-livraison-fournisseur').val();
    
                        saveLinkLivraisonFournisseurCommandeBoutique(idCommandeItemBoutique, inputLivraisonFournisseur);
                    } else {
                        ViewUtils.activeButtonAndHideLoader('#modal-link-livraison-fournisseur-commande-boutique__bt-sauvegarder', '#modal-link-livraison-fournisseur-commande-boutique__bt-sauvegarder .loader');
                    }
                });
    
                function saveLinkLivraisonFournisseurCommandeBoutique(idCommandeItemBoutique, inputLivraisonFournisseur) {
                    AjaxUtils.callService(
                        'commande-boutique', 
                        'saveLinkLivraisonFournisseur', 
                        AjaxUtils.dataTypes.json, 
                        AjaxUtils.requestTypes.post, 
                        {
                            id: idCommandeItemBoutique
                        }, 
                        {
                            livraisonFournisseurBoutiqueId: inputLivraisonFournisseur
                        }, 
                        function(response) {
                            if (response.success) {
                                FormUtils.displayFormResultMessage('#modal-link-livraison-fournisseur-commande-boutique .form-result-message', 'Livraison fournisseur associ&eacute;e', FormUtils.resultsTypes.ok);
                                $('#modal-link-livraison-fournisseur-commande-boutique__bt-sauvegarder .loader').hide();
                                AdminShopBoutiqueCommandesController.load_StatsCommandes();
                                AdminShopBoutiqueCommandesController.load_TableCommandes();
                            } else {
                                ViewUtils.activeButtonAndHideLoader('#modal-link-livraison-fournisseur-commande-boutique__bt-sauvegarder', '#modal-link-livraison-fournisseur-commande-boutique__bt-sauvegarder .loader');
                                FormUtils.displayFormResultMessage('#modal-link-livraison-fournisseur-commande-boutique .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
                            }
                        },
                        function(response) {
                            ViewUtils.activeButtonAndHideLoader('#modal-link-livraison-fournisseur-commande-boutique__bt-sauvegarder', '#modal-link-livraison-fournisseur-commande-boutique__bt-sauvegarder .loader');
                            FormUtils.displayFormResultMessage('#modal-link-livraison-fournisseur-commande-boutique .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                        }
                    );
                }
            }
        }
    }

    function control_LinkSource() {
        $('.bt-commande-item-boutique-source').unbind('click');
        $('.bt-commande-item-boutique-source').click(function() {
            var idCommandeItemBoutique = $(this).attr('id-commande-item-boutique');
            open_ModalLinkSource(idCommandeItemBoutique);
        });

        function open_ModalLinkSource(idCommandeItemBoutique) {
            var params = {
                idCommandeItemBoutique: idCommandeItemBoutique
            };
            AjaxUtils.loadView(
                'admin/shop/boutique/commandes/modals/link-source/modal-link-source-commande-boutique-content',
                '#modal-link-source-commande-boutique-content',
                params,
                function() {
                    onClick_btSauvegarder(idCommandeItemBoutique);
                },
                '#loader-page'
            );
            $('#modal-link-source-commande-boutique').modal('show');
            
            function onClick_btSauvegarder(idCommandeItemBoutique) {
                $('#modal-link-source-commande-boutique__bt-sauvegarder').unbind('click');
                $('#modal-link-source-commande-boutique__bt-sauvegarder').click(function() {
                    FormUtils.hideFormErrors('#modal-link-source-commande-boutique #form-link-source-commande-boutique');
                    ViewUtils.unsetHTMLAndHide('#modal-link-source-commande-boutique .form-result-message');
                    ViewUtils.desactiveButtonAndShowLoader('#modal-link-source-commande-boutique__bt-sauvegarder', '#modal-link-source-commande-boutique__bt-sauvegarder .loader');
    
                    var formValidation = FormUtils.validateForm('#modal-link-source-commande-boutique #form-link-source-commande-boutique');
                    
                    if (formValidation) {
                        var inputSource = $('#form-link-source-commande-boutique__input-source').val();
    
                        saveLinkSourceCommandeBoutique(idCommandeItemBoutique, inputSource);
                    } else {
                        ViewUtils.activeButtonAndHideLoader('#modal-link-source-commande-boutique__bt-sauvegarder', '#modal-link-source-commande-boutique__bt-sauvegarder .loader');
                    }
                });
    
                function saveLinkSourceCommandeBoutique(idCommandeItemBoutique, inputSource) {
                    AjaxUtils.callService(
                        'commande-boutique', 
                        'saveLinkSource', 
                        AjaxUtils.dataTypes.json, 
                        AjaxUtils.requestTypes.post, 
                        {
                            id: idCommandeItemBoutique
                        }, 
                        {
                            typeSourceProduitBoutiqueId: inputSource
                        }, 
                        function(response) {
                            if (response.success) {
                                FormUtils.displayFormResultMessage('#modal-link-source-commande-boutique .form-result-message', 'Source associ&eacute;e', FormUtils.resultsTypes.ok);
                                $('#modal-link-source-commande-boutique__bt-sauvegarder .loader').hide();
                                AdminShopBoutiqueCommandesController.load_StatsCommandes();
                                AdminShopBoutiqueCommandesController.load_TableCommandes();
                            } else {
                                ViewUtils.activeButtonAndHideLoader('#modal-link-source-commande-boutique__bt-sauvegarder', '#modal-link-source-commande-boutique__bt-sauvegarder .loader');
                                FormUtils.displayFormResultMessage('#modal-link-source-commande-boutique .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
                            }
                        },
                        function(response) {
                            ViewUtils.activeButtonAndHideLoader('#modal-link-source-commande-boutique__bt-sauvegarder', '#modal-link-source-commande-boutique__bt-sauvegarder .loader');
                            FormUtils.displayFormResultMessage('#modal-link-source-commande-boutique .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                        }
                    );
                }
            }
        }
    }

    function control_CommandeItemDonne() {
        $('.bt-commande-item-boutique-donne').unbind('click');
        $('.bt-commande-item-boutique-donne').click(function() {
            var idCommandeItemBoutique = $(this).attr('id-commande-item-boutique');
            
            $('.bt-commande-item-boutique-donne[id-commande-item-boutique='+idCommandeItemBoutique+'] .loader').show();
            AjaxUtils.callService(
                'commande-boutique', 
                'toggleDonne', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idCommandeItemBoutique
                }, 
                function(response) {
                    $('.bt-commande-item-boutique-donne[id-commande-item-boutique='+idCommandeItemBoutique+'] .loader').show();
                    if (response.success) {
                        AdminShopBoutiqueCommandesController.load_TableCommandes();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la modification du commande item boutique', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.bt-commande-item-boutique-donne[id-commande-item-boutique='+idCommandeItemBoutique+'] .loader').show();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        });
    }
};

initializePage = function() {
    AdminController.initializePage('shop', 'boutique');

    AjaxUtils.callService(
        'date', 
        'getSlashDateDebutSaison', 
        AjaxUtils.dataTypes.json, 
        AjaxUtils.requestTypes.get, 
        null, 
        null, 
        function(response) {
            AdminShopBoutiqueCommandesController.dateDebutRecherche = response.date;

            AdminShopBoutiqueCommandesController.load_StatsCommandes();
            AdminShopBoutiqueCommandesController.load_RechercheAndTableCommandes();
        },
        function(response) {}
    );
};