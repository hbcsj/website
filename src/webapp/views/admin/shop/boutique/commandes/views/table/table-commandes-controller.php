<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/commande-dao.php");
require_once("common/php/dao/commande-item-boutique-dao.php");
require_once("common/php/dao/produit-boutique-dao.php");
require_once("common/php/dao/categorie-produit-boutique-dao.php");
require_once("common/php/dao/marque-produit-boutique-dao.php");
require_once("common/php/dao/type-source-produit-boutique-dao.php");
require_once("common/php/dao/commande-fournisseur-boutique-dao.php");
require_once("common/php/dao/livraison-fournisseur-boutique-dao.php");
require_once("common/php/dao/fournisseur-produit-boutique-dao.php");
require_once("common/php/lib/date-utils.php");

define(TABLE_COMMANDES_BOUTIQUE_NB_ARTICLES, "nb_places");
define(TABLE_COMMANDES_BOUTIQUE_MONTANT, "prix");
define(TABLE_COMMANDES_BOUTIQUE_MONTANT_FOURNISSEUR, "prix_fournisseur");

class TableCommandesCtrl extends AbstractViewCtrl {

	private $commandeDAO;
	private $commandeItemBoutiqueDAO;
	private $produitBoutiqueDAO;
	private $categorieProduitBoutiqueDAO;
	private $marqueProduitBoutiqueDAO;
	private $typeSourceProduitBoutiqueDAO;
	private $commandeFournisseurBoutiqueDAO;
	private $livraisonFournisseurBoutiqueDAO;
	private $fournisseurProduitBoutiqueDAO;

	private $totaux;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_commercial()) {
			$this->commandeDAO = new CommandeDAO($this->getDatabaseConnection());
			$this->commandeItemBoutiqueDAO = new CommandeItemBoutiqueDAO($this->getDatabaseConnection());
			$this->produitBoutiqueDAO = new ProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->categorieProduitBoutiqueDAO = new CategorieProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->marqueProduitBoutiqueDAO = new MarqueProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->typeSourceProduitBoutiqueDAO = new TypeSourceProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->commandeFournisseurBoutiqueDAO = new CommandeFournisseurBoutiqueDAO($this->getDatabaseConnection());
			$this->livraisonFournisseurBoutiqueDAO = new LivraisonFournisseurBoutiqueDAO($this->getDatabaseConnection());
			$this->fournisseurProduitBoutiqueDAO = new FournisseurProduitBoutiqueDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getCommandesItems() {
		$this->totaux[TABLE_COMMANDES_BOUTIQUE_NB_ARTICLES] = 0;
		$this->totaux[TABLE_COMMANDES_BOUTIQUE_MONTANT] = 0;
		$this->totaux[TABLE_COMMANDES_BOUTIQUE_MONTANT_FOURNISSEUR] = 0;

		$commandesItems = $this->commandeItemBoutiqueDAO->getAllByParams(
			$_GET["produitId"], 
			trim($_GET["reference"]), 
			trim($_GET["nom"]),
			trim($_GET["prenom"]), 
			DateUtils::convert_slashDate_to_sqlDate($_GET["dateDebut"]), 
			DateUtils::convert_slashDate_to_sqlDate($_GET["dateFin"]), 
			$_GET["donne"], 
			$_GET["commandeFournisseurBoutiqueId"],
			$_GET["livraisonFournisseurBoutiqueId"],
			$_GET["typeSourceProduitBoutiqueId"],
			COMMANDE_TABLE_NAME.".".COMMANDE_DATE_HEURE." DESC"
		);

		if (sizeof($commandesItems) > 0) {
			foreach ($commandesItems as $commandeItem) {
				$this->totaux[TABLE_COMMANDES_BOUTIQUE_NB_ARTICLES] += $commandeItem[COMMANDE_ITEM_BOUTIQUE_QUANTITE];
				$this->totaux[TABLE_COMMANDES_BOUTIQUE_MONTANT] += $commandeItem[COMMANDE_ITEM_BOUTIQUE_PRIX];
				$this->totaux[TABLE_COMMANDES_BOUTIQUE_MONTANT_FOURNISSEUR] += $commandeItem[COMMANDE_ITEM_BOUTIQUE_PRIX_FOURNISSEUR];
			}
		}

		return $commandesItems;
	}

	public function getNomMarque($marqueId) {
		$marque = $this->marqueProduitBoutiqueDAO->getById($marqueId);
		return $marque[MARQUE_PRODUIT_BOUTIQUE_NOM];
	}

	public function getNomCategorie($categorieId) {
		$categorie = $this->categorieProduitBoutiqueDAO->getById($categorieId);
		return $categorie[CATEGORIE_PRODUIT_BOUTIQUE_NOM];
	}

	public function getNomTypeSource($typeSourceId) {
		$typeSource = $this->typeSourceProduitBoutiqueDAO->getById($typeSourceId);
		return $typeSource[TYPE_SOURCE_PRODUIT_BOUTIQUE_LIBELLE];
	}

	public function getCommandeFournisseur($commandeFournisseurId) {
		return $this->commandeFournisseurBoutiqueDAO->getById($commandeFournisseurId);
	}

	public function getLivraisonFournisseur($livraisonFournisseurId) {
		return $this->livraisonFournisseurBoutiqueDAO->getById($livraisonFournisseurId);
	}

	public function getNomFournisseur($fournisseurId) {
		$fournisseur = $this->fournisseurProduitBoutiqueDAO->getById($fournisseurId);
		return $fournisseur[FOURNISSEUR_PRODUIT_BOUTIQUE_NOM];
	}

	public function getCommande($commandeId) {
		return $this->commandeDAO->getById($commandeId);
	}

	public function getProduit($produitId) {
		return $this->produitBoutiqueDAO->getById($produitId);
	}

	public function getTotaux() {
		return $this->totaux;
	}
}

?>