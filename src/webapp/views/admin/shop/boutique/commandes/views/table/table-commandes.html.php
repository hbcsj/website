<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");

$view = new View(
    "webapp/views/admin/shop/boutique/commandes/views/table/", 
    "table-commandes", 
    "TableCommandesCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
    $commandesItems = $ctrl->getCommandesItems();
    $totaux = $ctrl->getTotaux();
    ?>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Date</th>
                <th>Nom</th>
                <th>Coord.</th>
                <th>R&eacute;f. com<sup>de</sup></th>
                <th>Produit</th>
                <th class="text-center">Nb. articles</th>
                <th class="text-center">Prix</th>
                <th class="text-center">Prix fourn.</th>
                <th class="text-center">Marge</th>
                <th class="text-center">Com<sup>de</sup><br>fourn.</th>
                <th class="text-center">Liv<sup>son</sup><br>fourn.</th>
                <th colspan="2">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th colspan="5">TOTAL</th>
                <th class="text-center"><?php echo $totaux[TABLE_COMMANDES_BOUTIQUE_NB_ARTICLES]; ?></th>
                <th class="text-center"><?php echo $totaux[TABLE_COMMANDES_BOUTIQUE_MONTANT]; ?> &euro;</th>
                <th class="text-center"><?php echo $totaux[TABLE_COMMANDES_BOUTIQUE_MONTANT_FOURNISSEUR]; ?> &euro;</th>
                <th class="text-center"><?php echo ($totaux[TABLE_COMMANDES_BOUTIQUE_MONTANT] - $totaux[TABLE_COMMANDES_BOUTIQUE_MONTANT_FOURNISSEUR]); ?> &euro;</th>
                <th colspan="4">&nbsp;</th>
            </tr>
            <?php
            if (sizeof($commandesItems) > 0) {
                foreach ($commandesItems as $commandeItem) {
                    $commande = $ctrl->getCommande($commandeItem[COMMANDE_ITEM_BOUTIQUE_COMMANDE_ID]);
                    $produit = $ctrl->getProduit($commandeItem[COMMANDE_ITEM_BOUTIQUE_PRODUIT_BOUTIQUE_ID]);

                    $btDonneClass = ($commandeItem[COMMANDE_ITEM_BOUTIQUE_DONNE] == 1) ? "bt-ok" : "bt-error";

                    $btTypeSourceClass = "bt-ok";
                    if ($commandeItem[COMMANDE_ITEM_BOUTIQUE_TYPE_SOURCE_PRODUIT_BOUTIQUE_ID] == COMMANDE_ITEM_BOUTIQUE_NON_COMMANDE_TYPE_SOURCE_ID) {
                        $btTypeSourceClass = "bt-warning";
                    }

                    $btCommandeFournisseurClass = "bt-ok";
                    $libelleCommandeFournisseur = "";
                    $commandeFournisseur = $ctrl->getCommandeFournisseur($commandeItem[COMMANDE_ITEM_BOUTIQUE_COMMANDE_FOURNISSEUR_BOUTIQUE_ID]);
                    if ($commandeFournisseur == null) {
                        $libelleCommandeFournisseur = "Aucune";
                        $btCommandeFournisseurClass = "bt-warning";
                    } else {
                        $libelleCommandeFournisseur = $ctrl->getNomFournisseur($commandeFournisseur[COMMANDE_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID])."<br>".DateUtils::convert_sqlDate_to_slashDate($commandeFournisseur[COMMANDE_FOURNISSEUR_BOUTIQUE_DATE]);
                        $btCommandeFournisseurClass = "bt-ok";
                    }

                    $btLivraisonFournisseurClass = "bt-ok";
                    $libelleLivraisonFournisseur = "";
                    $livraisonFournisseur = $ctrl->getLivraisonFournisseur($commandeItem[COMMANDE_ITEM_BOUTIQUE_LIVRAISON_FOURNISSEUR_BOUTIQUE_ID]);
                    if ($livraisonFournisseur == null) {
                        $libelleLivraisonFournisseur = "Aucune";
                        $btLivraisonFournisseurClass = "bt-warning";
                    } else {
                        $libelleLivraisonFournisseur = $ctrl->getNomFournisseur($livraisonFournisseur[LIVRAISON_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID])."<br>".DateUtils::convert_sqlDate_to_slashDate($livraisonFournisseur[LIVRAISON_FOURNISSEUR_BOUTIQUE_DATE]);
                        $btLivraisonFournisseurClass = "bt-ok";
                    }
                    ?>
                    <tr>
                        <td><?php echo str_replace(SLASH_DATE_TIME_SEPARATOR, "<br>", DateUtils::convert_sqlDateTime_to_slashDateTime($commande[COMMANDE_DATE_HEURE])); ?></td>
                        <td><?php echo $commande[COMMANDE_NOM]."<br>".$commande[COMMANDE_PRENOM]; ?></td>
                        <td>
                            @ : <a href="mailto:<?php echo $commande[COMMANDE_EMAIL]; ?>"><?php echo $commande[COMMANDE_EMAIL]; ?></a>
                            <br>
                            T&eacute;l : <a href="tel:<?php echo str_replace(".", "", $commande[COMMANDE_TELEPHONE]); ?>"><?php echo $commande[COMMANDE_TELEPHONE]; ?></a>
                        </td>
                        <td><?php echo $commande[COMMANDE_REFERENCE]; ?></td>
                        <td>
                            <?php 
                            echo $produit[PRODUIT_BOUTIQUE_NOM]; 
                            if ($produit[PRODUIT_BOUTIQUE_INDICATION] != "") {
                                echo " - ".$produit[PRODUIT_BOUTIQUE_INDICATION];
                            }
                            echo "<br>".$ctrl->getNomCategorie($produit[PRODUIT_BOUTIQUE_CATEGORIE_PRODUIT_BOUTIQUE_ID]);
                            if ($produit[PRODUIT_BOUTIQUE_MARQUE_PRODUIT_BOUTIQUE_ID] != null) {
                                echo " - ".$ctrl->getNomMarque($produit[PRODUIT_BOUTIQUE_MARQUE_PRODUIT_BOUTIQUE_ID]);
                            }
                            if ($commandeItem[COMMANDE_ITEM_BOUTIQUE_TAILLE] != "") {
                                echo "<br>Taille ".$commandeItem[COMMANDE_ITEM_BOUTIQUE_TAILLE];
                            }
                            if ($commandeItem[COMMANDE_ITEM_BOUTIQUE_COULEUR] != "") {
                                echo "<br>".$commandeItem[COMMANDE_ITEM_BOUTIQUE_COULEUR];
                            }
                            if ($commandeItem[COMMANDE_ITEM_BOUTIQUE_FLOCAGE_NOM] != "") {
                                echo "<br>Flocage nom : ".$commandeItem[COMMANDE_ITEM_BOUTIQUE_FLOCAGE_NOM];
                            }
                            if ($commandeItem[COMMANDE_ITEM_BOUTIQUE_FLOCAGE_NUMERO_AVANT] != "") {
                                echo "<br>Flocage num&eacute;ro avant : ".$commandeItem[COMMANDE_ITEM_BOUTIQUE_FLOCAGE_NUMERO_AVANT];
                            }
                            if ($commandeItem[COMMANDE_ITEM_BOUTIQUE_FLOCAGE_NUMERO_ARRIERE] != "") {
                                echo "<br>Flocage num&eacute;ro arri&egrave;re : ".$commandeItem[COMMANDE_ITEM_BOUTIQUE_FLOCAGE_NUMERO_ARRIERE];
                            }
                            if ($commandeItem[COMMANDE_ITEM_BOUTIQUE_FLOCAGE_INITIALES] != "") {
                                echo "<br>Flocage initiales : ".$commandeItem[COMMANDE_ITEM_BOUTIQUE_FLOCAGE_INITIALES];
                            }
                            ?>
                        </td>
                        <td class="text-center"><?php echo $commandeItem[COMMANDE_ITEM_BOUTIQUE_QUANTITE]; ?></td>
                        <td class="text-center"><?php echo $commandeItem[COMMANDE_ITEM_BOUTIQUE_PRIX]; ?> &euro;</td>
                        <td class="text-center"><?php echo $commandeItem[COMMANDE_ITEM_BOUTIQUE_PRIX_FOURNISSEUR]; ?> &euro;</td>
                        <td class="text-center"><?php echo ($commandeItem[COMMANDE_ITEM_BOUTIQUE_PRIX] - $commandeItem[COMMANDE_ITEM_BOUTIQUE_PRIX_FOURNISSEUR]); ?> &euro;</td>
                        <td class="text-center">
                            <button class="btn btn-default bt-table-admin bt-commande-item-boutique-commande-fournisseur <?php echo $btCommandeFournisseurClass; ?>" id-commande-item-boutique="<?php echo $commandeItem[COMMANDE_ITEM_BOUTIQUE_ID]; ?>">
                                <div class="button__text"><?php echo $libelleCommandeFournisseur; ?></div>
                            </button>
                        </td>
                        <td class="text-center">
                            <button class="btn btn-default bt-table-admin bt-commande-item-boutique-livraison-fournisseur <?php echo $btLivraisonFournisseurClass; ?>" id-commande-item-boutique="<?php echo $commandeItem[COMMANDE_ITEM_BOUTIQUE_ID]; ?>">
                                <div class="button__text"><?php echo $libelleLivraisonFournisseur; ?></div>
                            </button>
                        </td>
                        <td class="text-right">
                            <button class="btn btn-default bt-table-admin bt-commande-item-boutique-donne <?php echo $btDonneClass; ?>" id-commande-item-boutique="<?php echo $commandeItem[COMMANDE_ITEM_BOUTIQUE_ID]; ?>">
                                <div class="button__text"><?php echo ($commandeItem[COMMANDE_ITEM_BOUTIQUE_DONNE] == 1) ? "Donn&eacute;" : "Non donn&eacute;"; ?></div>
                                <div class="loader"></div>
                            </button>
                            <br><br>
                            <button class="btn btn-default bt-table-admin bt-commande-item-boutique-source <?php echo $btTypeSourceClass; ?>" id-commande-item-boutique="<?php echo $commandeItem[COMMANDE_ITEM_BOUTIQUE_ID]; ?>">
                                <div class="button__text"><?php echo $ctrl->getNomTypeSource($commandeItem[COMMANDE_ITEM_BOUTIQUE_TYPE_SOURCE_PRODUIT_BOUTIQUE_ID]); ?></div>
                            </button>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>