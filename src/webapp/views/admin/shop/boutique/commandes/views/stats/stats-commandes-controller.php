<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/commande-item-boutique-dao.php");
require_once("common/php/dao/commande-dao.php");
require_once("common/php/lib/date-utils.php");

class StatsCommandesCtrl extends AbstractViewCtrl {

	private $commandeItemBoutiqueDAO;
	private $commandeDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_commercial()) {
			$this->commandeItemBoutiqueDAO = new CommandeItemBoutiqueDAO($this->getDatabaseConnection());
			$this->commandeDAO = new CommandeDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getNbCommandes() {
		return $this->commandeDAO->getNbLinkedToCommandeItemBoutique(DateUtils::get_sqlDate_debutSaison())[COMMANDE_NB_TOTAL];
	}

	public function getNbArticles() {
		$nbArticles = $this->commandeItemBoutiqueDAO->getNbTotalArticlesAfterDate(DateUtils::get_sqlDate_debutSaison())[COMMANDE_ITEM_BOUTIQUE_NB_ARTICLES];
		return ($nbArticles != null) ? $nbArticles : 0;
	}
}

?>