<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/categorie-produit-boutique-dao.php");
require_once("common/php/dao/produit-boutique-dao.php");
require_once("common/php/dao/commande-item-boutique-dao.php");
require_once("common/php/dao/type-source-produit-boutique-dao.php");
require_once("common/php/dao/commande-fournisseur-boutique-dao.php");
require_once("common/php/dao/livraison-fournisseur-boutique-dao.php");
require_once("common/php/dao/fournisseur-produit-boutique-dao.php");
require_once("common/php/lib/date-utils.php");

class RechercheCommandesCtrl extends AbstractViewCtrl {

	private $categorieProduitBoutiqueDAO;
	private $produitBoutiqueDAO;
	private $commandeItemBoutiqueDAO;
	private $typeSourceProduitBoutiqueDAO;
	private $commandeFournisseurBoutiqueDAO;
	private $livraisonFournisseurBoutiqueDAO;
	private $fournisseurProduitBoutiqueDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_commercial()) {
			$this->categorieProduitBoutiqueDAO = new CategorieProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->produitBoutiqueDAO = new ProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->commandeItemBoutiqueDAO = new CommandeItemBoutiqueDAO($this->getDatabaseConnection());
			$this->typeSourceProduitBoutiqueDAO = new TypeSourceProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->commandeFournisseurBoutiqueDAO = new CommandeFournisseurBoutiqueDAO($this->getDatabaseConnection());
			$this->livraisonFournisseurBoutiqueDAO = new LivraisonFournisseurBoutiqueDAO($this->getDatabaseConnection());
			$this->fournisseurProduitBoutiqueDAO = new FournisseurProduitBoutiqueDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getProduits() {
		return $this->produitBoutiqueDAO->getAll(
			PRODUIT_BOUTIQUE_TABLE_NAME.".".PRODUIT_BOUTIQUE_NOM.", ".PRODUIT_BOUTIQUE_TABLE_NAME.".".PRODUIT_BOUTIQUE_INDICATION.", ".PRODUIT_BOUTIQUE_TABLE_NAME.".".PRODUIT_BOUTIQUE_CATEGORIE_PRODUIT_BOUTIQUE_ID
		);
	}

	public function getPersonnesCommandes() {
		return $this->commandeItemBoutiqueDAO->getPersonnesCommandes();
	}

	public function getSources() {
		return $this->typeSourceProduitBoutiqueDAO->getAll();
	}

	public function getCommandesFournisseur() {
		return $this->commandeFournisseurBoutiqueDAO->getAll(COMMANDE_FOURNISSEUR_BOUTIQUE_TABLE_NAME.".".COMMANDE_FOURNISSEUR_BOUTIQUE_DATE." DESC");
	}

	public function getLivraisonsFournisseur() {
		return $this->livraisonFournisseurBoutiqueDAO->getAll(LIVRAISON_FOURNISSEUR_BOUTIQUE_TABLE_NAME.".".LIVRAISON_FOURNISSEUR_BOUTIQUE_DATE." DESC");
	}

	public function getNomFournisseurProduitBoutique($fournisseurProduitBoutiqueId) {
		$fournisseur = $this->fournisseurProduitBoutiqueDAO->getById($fournisseurProduitBoutiqueId);
		return $fournisseur[FOURNISSEUR_PRODUIT_BOUTIQUE_NOM];
	}

	public function getNomCategorie($categorieId) {
		$categorie = $this->categorieProduitBoutiqueDAO->getById($categorieId);
		return $categorie[CATEGORIE_PRODUIT_BOUTIQUE_NOM];
	}
}

?>