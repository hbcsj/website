<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");

$view = new View(
    "webapp/views/admin/shop/boutique/commandes/views/recherche/", 
    "recherche-commandes", 
    "RechercheCommandesCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
    $produits = $ctrl->getProduits();
    $personnesCommandes = $ctrl->getPersonnesCommandes();
    $sources = $ctrl->getSources();
    $commandesFournisseur = $ctrl->getCommandesFournisseur();
    $livraisonsFournisseur = $ctrl->getLivraisonsFournisseur();
    ?>
    <div class="container-recherche__title" title="Ouvrir le formulaire de recherche" for="form-recherche-commandes">
        <div class="container-recherche__title__icon">
            <span class="glyphicon glyphicon-search"></span>
        </div>
        <div class="container-recherche__title__text">Recherche</div>
        <div class="container-recherche__title__arrow">
            <span class="glyphicon glyphicon-triangle-bottom"></span>
        </div>
    </div>
    <div class="form-horizontal" id="form-recherche-commandes">
        <div class="form-group">
            <label for="form-recherche-commandes__input-reference" class="col-xs-2 col-xs-offset-1 control-label">R&eacute;f :</label>
            <div class="col-xs-3 form-input" for="form-recherche-commandes__input-reference">
                <input class="form-control" id="form-recherche-commandes__input-reference">
            </div>
            <label for="form-recherche-commandes__input-nom" class="col-xs-1 control-label">Nom :</label>
            <div class="col-xs-3 form-input" for="form-recherche-commandes__input-nom">
                <select class="form-control" id="form-recherche-commandes__input-nom">
                    <option value=""></option>
                    <?php
                    if (sizeof($personnesCommandes) > 0) {
                        foreach ($personnesCommandes as $personneCommandes) {
                            ?>
                            <option value-nom="<?php echo $personneCommandes[COMMANDE_NOM]; ?>" value-prenom="<?php echo $personneCommandes[COMMANDE_PRENOM]; ?>" value="<?php echo $personneCommandes[COMMANDE_NOM]." ".$personneCommandes[COMMANDE_PRENOM]; ?>"><?php echo $personneCommandes[COMMANDE_NOM]." ".$personneCommandes[COMMANDE_PRENOM]; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="form-recherche-commandes__input-produit" class="col-xs-2 col-xs-offset-1 control-label">Produit :</label>
            <div class="col-xs-3 form-input" for="form-recherche-commandes__input-produit">
                <select class="form-control" id="form-recherche-commandes__input-produit">
                    <option value=""></option>
                    <?php
                    if (sizeof($produits) > 0) {
                        foreach ($produits as $produit) {
                            ?>
                            <option value="<?php echo $produit[PRODUIT_BOUTIQUE_ID]; ?>">
                                <?php 
                                echo $produit[PRODUIT_BOUTIQUE_NOM]." - ";
                                if ($produit[PRODUIT_BOUTIQUE_INDICATION] != "") {
                                    echo $produit[PRODUIT_BOUTIQUE_INDICATION]." - ";
                                }
                                echo $ctrl->getNomCategorie($produit[PRODUIT_BOUTIQUE_CATEGORIE_PRODUIT_BOUTIQUE_ID]); 
                                ?>
                            </option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <label for="form-recherche-commandes__input-is-donne" class="col-xs-1 control-label">Donn&eacute; ?</label>
            <div class="col-xs-1 form-input" for="form-recherche-commandes__input-is-donne">
                <select class="form-control" id="form-recherche-commandes__input-is-donne">
                    <option value=""></option>
                    <option value="1">Oui</option>
                    <option value="0">Non</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="form-recherche-commandes__input-produit" class="col-xs-2 col-xs-offset-1 control-label">Com<sup>de</sup> fourn. :</label>
            <div class="col-xs-2 form-input" for="form-recherche-commandes__input-commande-fournisseur">
                <select class="form-control" id="form-recherche-commandes__input-commande-fournisseur">
                    <option value=""></option>
                    <?php
                    if (sizeof($commandesFournisseur) > 0) {
                        foreach ($commandesFournisseur as $commandeFournisseur) {
                            ?>
                            <option value="<?php echo $commandeFournisseur[COMMANDE_FOURNISSEUR_BOUTIQUE_ID]; ?>">
                                <?php echo DateUtils::convert_sqlDate_to_slashDate($commandeFournisseur[COMMANDE_FOURNISSEUR_BOUTIQUE_DATE]); ?>
                                &nbsp;-&nbsp;
                                <?php echo $ctrl->getNomFournisseurProduitBoutique($commandeFournisseur[COMMANDE_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID]); ?>
                            </option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <label for="form-recherche-commandes__input-livraison-fournisseur" class="col-xs-2 control-label">Liv<sup>son</sup> fourn. :</label>
            <div class="col-xs-2 form-input" for="form-recherche-commandes__input-livraison-fournisseur">
                <select class="form-control" id="form-recherche-commandes__input-livraison-fournisseur">
                    <option value=""></option>
                    <?php
                    if (sizeof($livraisonsFournisseur) > 0) {
                        foreach ($livraisonsFournisseur as $livraisonFournisseur) {
                            ?>
                            <option value="<?php echo $livraisonFournisseur[LIVRAISON_FOURNISSEUR_BOUTIQUE_ID]; ?>">
                                <?php echo DateUtils::convert_sqlDate_to_slashDate($livraisonFournisseur[LIVRAISON_FOURNISSEUR_BOUTIQUE_DATE]); ?>
                                &nbsp;-&nbsp;
                                <?php echo $ctrl->getNomFournisseurProduitBoutique($livraisonFournisseur[LIVRAISON_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID]); ?>
                            </option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="form-recherche-commandes__input-date-debut" class="col-xs-2 col-xs-offset-1 control-label">Date commande :</label>
            <div class="col-xs-2 form-input" for="form-recherche-commandes__input-date-debut">
                <div class="input-group">
                    <input type="text" class="form-control" id="form-recherche-commandes__input-date-debut" placeholder="D&eacute;but">
                    <span class="input-group-addon">
                        <img src="<?php echo SRC_PATH; ?>assets/img/icons/calendar.png">
                    </span>
                </div>
            </div>
            <label for="form-recherche-commandes__input-source" class="col-xs-2 control-label">Source :</label>
            <div class="col-xs-2 form-input" for="form-recherche-commandes__input-source">
                <select class="form-control" id="form-recherche-commandes__input-source">
                    <option value=""></option>
                    <?php
                    if (sizeof($sources) > 0) {
                        foreach ($sources as $source) {
                            ?>
                            <option value="<?php echo $source[TYPE_SOURCE_PRODUIT_BOUTIQUE_ID]; ?>"><?php echo $source[TYPE_SOURCE_PRODUIT_BOUTIQUE_LIBELLE]; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-2 col-xs-offset-3 form-input" for="form-recherche-commandes__input-date-fin">
                <div class="input-group">
                    <input type="text" class="form-control" id="form-recherche-commandes__input-date-fin" placeholder="Fin">
                    <span class="input-group-addon">
                        <img src="<?php echo SRC_PATH; ?>assets/img/icons/calendar.png">
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="container-recherche__footer" for="form-recherche-commandes">
        <button id="recherche-commandes__bt-rechercher" class="btn btn-default">
            <div class="button__icon">
                <span class="glyphicon glyphicon-search"></span>
            </div>
            <div class="button__text">Chercher</div>
        </button>
        <button id="recherche-commandes__bt-reset" class="btn btn-default">
            <div class="button__icon">
                <span class="glyphicon glyphicon-erase"></span>
            </div>
            <div class="button__text">Reset</div>
        </button>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>