<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/fournisseur-produit-boutique-dao.php");
require_once("common/php/dao/commande-fournisseur-boutique-dao.php");
require_once("common/php/dao/livraison-fournisseur-boutique-dao.php");
require_once("common/php/dao/produit-boutique-dao.php");

class TableFournisseursCtrl extends AbstractViewCtrl {

	private $fournisseurProduitBoutiqueDAO;
	private $commandeFournisseurBoutiqueDAO;
	private $livraisonFournisseurBoutiqueDAO;
	private $produitBoutiqueDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_commercial()) {
			$this->fournisseurProduitBoutiqueDAO = new FournisseurProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->commandeFournisseurBoutiqueDAO = new CommandeFournisseurBoutiqueDAO($this->getDatabaseConnection());
			$this->livraisonFournisseurBoutiqueDAO = new LivraisonFournisseurBoutiqueDAO($this->getDatabaseConnection());
			$this->produitBoutiqueDAO = new ProduitBoutiqueDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getFournisseurs() {
		return $this->fournisseurProduitBoutiqueDAO->getAll(
			FOURNISSEUR_PRODUIT_BOUTIQUE_TABLE_NAME.".".FOURNISSEUR_PRODUIT_BOUTIQUE_NOM
		);
	}

	public function hasCommandesOuLivraisonsOuProduitsAssocies($fournisseurId) {
		return (
			sizeof($this->commandeFournisseurBoutiqueDAO->getByFournisseurProduitBoutiqueId($fournisseurId)) > 0 || 
			sizeof($this->livraisonFournisseurBoutiqueDAO->getByFournisseurProduitBoutiqueId($fournisseurId)) > 0 ||
			sizeof($this->produitBoutiqueDAO->getByFournisseurProduitBoutiqueId($fournisseurId)) > 0
		);
	}
}

?>