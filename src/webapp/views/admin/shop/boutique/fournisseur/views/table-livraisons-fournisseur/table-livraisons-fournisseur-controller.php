<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/fournisseur-produit-boutique-dao.php");
require_once("common/php/dao/livraison-fournisseur-boutique-dao.php");
require_once("common/php/dao/commande-item-boutique-dao.php");

class TableLivraisonsFournisseurCtrl extends AbstractViewCtrl {

	private $fournisseurProduitBoutiqueDAO;
	private $livraisonFournisseurBoutiqueDAO;
	private $commandeItemBoutiqueDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_commercial()) {
			$this->fournisseurProduitBoutiqueDAO = new FournisseurProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->livraisonFournisseurBoutiqueDAO = new LivraisonFournisseurBoutiqueDAO($this->getDatabaseConnection());
			$this->commandeItemBoutiqueDAO = new CommandeItemBoutiqueDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getLivraisons() {
		return $this->livraisonFournisseurBoutiqueDAO->getAll(
			LIVRAISON_FOURNISSEUR_BOUTIQUE_TABLE_NAME.".".LIVRAISON_FOURNISSEUR_BOUTIQUE_DATE." DESC"
		);
	}

	public function getNomFournisseur($fournisseurId) {
		$fournisseur = $this->fournisseurProduitBoutiqueDAO->getById($fournisseurId);
		return $fournisseur[FOURNISSEUR_PRODUIT_BOUTIQUE_NOM];
	}

	public function hasCommandesAssociees($livraisonFournisseurId) {
		return (sizeof($this->commandeItemBoutiqueDAO->getByLivraisonFournisseurBoutiqueId($livraisonFournisseurId)) > 0);
	}
}

?>