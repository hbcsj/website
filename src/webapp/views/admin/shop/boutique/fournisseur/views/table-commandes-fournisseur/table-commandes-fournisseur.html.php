<?php
require_once("core/php/resources/view.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");

$view = new View(
    "webapp/views/admin/shop/boutique/fournisseur/views/table-commandes-fournisseur/", 
    "table-commandes-fournisseur", 
    "TableCommandesFournisseurCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
    $commandes = $ctrl->getCommandes();
    ?>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Fournisseur</th>
                <th>Date</th>
                <th colspan="2">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (sizeof($commandes) > 0) {
                foreach ($commandes as $commande) {
                    ?>
                    <tr>
                        <td><?php echo $ctrl->getNomFournisseur($commande[COMMANDE_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID]); ?></td>
                        <td><?php echo DateUtils::convert_sqlDate_to_slashDate($commande[COMMANDE_FOURNISSEUR_BOUTIQUE_DATE]); ?></td>
                        <td class="text-center">
                            <span class="glyphicon glyphicon-pencil icon-edit-commande-fournisseur clickable" title="Editer" id-commande-fournisseur="<?php echo $commande[COMMANDE_FOURNISSEUR_BOUTIQUE_ID]; ?>"></span>
                        </td>
                        <td class="text-center">
                            <?php
                            if (!$ctrl->hasCommandesAssociees($commande[COMMANDE_FOURNISSEUR_BOUTIQUE_ID])) {
                                ?>
                                <span class="glyphicon glyphicon-trash icon-delete-commande-fournisseur clickable" title="Supprimer" id-commande-fournisseur="<?php echo $commande[COMMANDE_FOURNISSEUR_BOUTIQUE_ID]; ?>"></span>
                                <div class="loader loader-delete" id-commande-fournisseur="<?php echo $commande[COMMANDE_FOURNISSEUR_BOUTIQUE_ID]; ?>"></div>
                                <?php
                            } else {
                                echo "&nbsp;";
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>