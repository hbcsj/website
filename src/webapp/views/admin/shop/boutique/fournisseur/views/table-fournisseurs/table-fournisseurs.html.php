<?php
require_once("core/php/resources/view.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/shop/boutique/fournisseur/views/table-fournisseurs/", 
    "table-fournisseurs", 
    "TableFournisseursCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
    $fournisseurs = $ctrl->getFournisseurs();
    ?>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Nom</th>
                <th colspan="2">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (sizeof($fournisseurs) > 0) {
                foreach ($fournisseurs as $fournisseur) {
                    ?>
                    <tr>
                        <td><?php echo $fournisseur[FOURNISSEUR_PRODUIT_BOUTIQUE_NOM]; ?></td>
                        <td class="text-center">
                            <span class="glyphicon glyphicon-pencil icon-edit-fournisseur clickable" title="Editer" id-fournisseur="<?php echo $fournisseur[FOURNISSEUR_PRODUIT_BOUTIQUE_ID]; ?>"></span>
                        </td>
                        <td class="text-center">
                            <?php
                            if (!$ctrl->hasCommandesOuLivraisonsOuProduitsAssocies($fournisseur[FOURNISSEUR_PRODUIT_BOUTIQUE_ID])) {
                                ?>
                                <span class="glyphicon glyphicon-trash icon-delete-fournisseur clickable" title="Supprimer" id-fournisseur="<?php echo $fournisseur[FOURNISSEUR_PRODUIT_BOUTIQUE_ID]; ?>"></span>
                                <div class="loader loader-delete" id-fournisseur="<?php echo $fournisseur[FOURNISSEUR_PRODUIT_BOUTIQUE_ID]; ?>"></div>
                                <?php
                            } else {
                                echo "&nbsp;";
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>