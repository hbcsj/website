<?php
require_once("core/php/resources/page.php");
require_once("common/php/lib/admin-utils.php");

$page = new Page(
    "webapp/views/admin/shop/boutique/fournisseur/", 
    "admin-shop-boutique-fournisseur", 
    "AdminShopBoutiqueFournisseurCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/admin/common/nav/nav.html.php");
if (isAdminConnected_commercial()) {
    require_once("webapp/views/admin/shop/sub-menu/sub-menu-admin-shop.html.php");
}
?>

<div class="container-fluid animated fadeIn">
    <?php
    if (isAdminConnected_commercial()) {
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <?php require_once("webapp/views/admin/shop/boutique/header/header-admin-shop-boutique.html.php"); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3 col-xs-offset-1">
                <h2>
                    Fournisseurs
                    <button id="fournisseurs__bt-ajouter" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-plus"></span>
                        </div>
                        <div class="button__text">Ajouter</div>
                    </button>
                </h2>
            </div>
            <div class="col-xs-3 col-xs-offset-1">
                <h2>
                    Commandes
                    <button id="commandes-fournisseur__bt-ajouter" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-plus"></span>
                        </div>
                        <div class="button__text">Ajouter</div>
                    </button>
                </h2>
            </div>
            <div class="col-xs-3">
                <h2>
                    Livraisons
                    <button id="livraisons-fournisseur__bt-ajouter" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-plus"></span>
                        </div>
                        <div class="button__text">Ajouter</div>
                    </button>
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3 col-xs-offset-1">
                <div id="container-table-fournisseurs" class="container-table"></div>
            </div>
            <div class="col-xs-3 col-xs-offset-1">
                <div id="container-table-commandes-fournisseur" class="container-table"></div>
            </div>
            <div class="col-xs-3">
                <div id="container-table-livraisons-fournisseur" class="container-table"></div>
            </div>
        </div>
        <?php
    } else {
        require_once("webapp/views/common/mdp-form/mdp-form.html.php");
    }
    ?>
</div>

<?php
require_once("webapp/views/common/modals/alert/modal-alert.html.php");
require_once("webapp/views/common/modals/prompt/modal-prompt.html.php");

if (isAdminConnected_commercial()) {
    require_once("webapp/views/admin/shop/boutique/fournisseur/modals/save-fournisseur/modal-save-fournisseur.html.php");
    require_once("webapp/views/admin/shop/boutique/fournisseur/modals/save-commande-fournisseur/modal-save-commande-fournisseur.html.php");
    require_once("webapp/views/admin/shop/boutique/fournisseur/modals/save-livraison-fournisseur/modal-save-livraison-fournisseur.html.php");
}

$page->finalizePage();
?>