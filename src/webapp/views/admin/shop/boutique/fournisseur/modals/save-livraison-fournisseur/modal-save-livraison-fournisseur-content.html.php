<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/shop/boutique/fournisseur/modals/save-livraison-fournisseur/", 
    "modal-save-livraison-fournisseur", 
    "ModalSaveLivraisonFournisseurCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
?>
    <div class="modal-header">
        <div class="modal-title"><?php echo $ctrl->getTitle(); ?></div>
    </div>
    <div class="modal-body">
        <div class="form-horizontal" id="form-save-livraison-fournisseur">
            <div class="form-group">
                <label for="form-save-livraison-fournisseur__input-fournisseur" class="col-xs-4 control-label required">Fournisseur :</label>
                <div class="col-xs-7 form-input" for="form-save-livraison-fournisseur__input-fournisseur">
                    <select class="form-control" id="form-save-livraison-fournisseur__input-fournisseur" required>
                        <option value=""></option>
                        <?php echo $ctrl->getOptionsFournisseur(); ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-livraison-fournisseur__input-fournisseur"></div>
            </div>
            <div class="form-group">
                <label for="form-save-livraison-fournisseur__input-date" class="col-xs-4 control-label required">Date :</label>
                <div class="col-xs-3 form-input" for="form-save-livraison-fournisseur__input-date">
                    <div class="input-group">
                        <input type="text" class="form-control" id="form-save-livraison-fournisseur__input-date" value="<?php echo $ctrl->getDateLivraisonFournisseur(); ?>" pattern="^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$" pattern-indication="Le format doit &ecirc;tre 'jj/mm/aaaa'" required>
                        <span class="input-group-addon">
                            <img src="<?php echo SRC_PATH; ?>assets/img/icons/calendar.png">
                        </span>
                    </div>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-livraison-fournisseur__input-date"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-required text-right">Les champs marqu&eacute;s d'une * sont obligatoires</div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-8">
                <div class="form-result-message"></div>
            </div>
            <div class="col-xs-4 text-right">
                <button id="modal-save-livraison-fournisseur__bt-sauvegarder" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-ok"></span>
                    </div>
                    <div class="button__text">Sauvegarder</div>
                    <div class="loader"></div>
                </button>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>