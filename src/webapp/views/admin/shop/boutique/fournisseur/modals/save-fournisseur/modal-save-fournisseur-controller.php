<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/fournisseur-produit-boutique-dao.php");

class ModalSaveFournisseurCtrl extends AbstractViewCtrl {
	
	private $fournisseurProduitBoutique;

	private $fournisseurProduitBoutiqueDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idFournisseur" => $_GET["idFournisseur"]
		), true);
		
		if (isAdminConnected_commercial()) {
			$this->fournisseurProduitBoutiqueDAO = new FournisseurProduitBoutiqueDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdFournisseur();
	}

	private function checkIdFournisseur() {
		if (HTTPUtils::paramExists(GET, "idFournisseur")) {
			$this->fournisseurProduitBoutique = $this->fournisseurProduitBoutiqueDAO->getById($_GET["idFournisseur"]);
				
			if ($this->fournisseurProduitBoutique == null) {
				$this->sendCheckError(
					HTTP_404, 
					"Le fournisseur (idFournisseur = '".$_GET["idFournisseur"]."') n'existe pas", 
					"webapp/views/common/error/404/404.html.php"
				);
			}
		}
	}

	public function getTitle() {
		$title = "Nouveau fournisseur";
		if ($this->fournisseurProduitBoutique != null) {
			$title = "Fournisseur '".$this->fournisseurProduitBoutique[FOURNISSEUR_PRODUIT_BOUTIQUE_NOM]."' - <span class=\"normal\">Edition</span>";
		}
		return $title;
	}

	public function getNomFournisseur() {
		if ($this->fournisseurProduitBoutique != null) {
			return $this->fournisseurProduitBoutique[FOURNISSEUR_PRODUIT_BOUTIQUE_NOM];
		}
		return "";
	}
}

?>