<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/dao/pack-billetterie-dao.php");
require_once("common/php/dao/commande-fournisseur-boutique-dao.php");
require_once("common/php/dao/fournisseur-produit-boutique-dao.php");

class ModalSaveCommandeFournisseurCtrl extends AbstractViewCtrl {
	
	private $commandeFournisseur;

	private $commandeFournisseurBoutiqueDAO;
	private $fournisseurProduitBoutiqueDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idCommandeFournisseur" => $_GET["idCommandeFournisseur"]
		), true);
		
		if (isAdminConnected_commercial()) {
			$this->commandeFournisseurBoutiqueDAO = new CommandeFournisseurBoutiqueDAO($this->getDatabaseConnection());
			$this->fournisseurProduitBoutiqueDAO = new FournisseurProduitBoutiqueDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdCommandeFournisseur();
	}

	private function checkIdCommandeFournisseur() {
		if (HTTPUtils::paramExists(GET, "idCommandeFournisseur")) {
			$this->commandeFournisseur = $this->commandeFournisseurBoutiqueDAO->getById($_GET["idCommandeFournisseur"]);
				
			if ($this->commandeFournisseur == null) {
				$this->sendCheckError(
					HTTP_404, 
					"La commandeFournisseurBoutique (idCommandeFournisseur = '".$_GET["idCommandeFournisseur"]."') n'existe pas", 
					"webapp/views/common/error/404/404.html.php"
				);
			}
		}
	}

	public function getTitle() {
		$title = "Nouvelle commande";
		if ($this->commandeFournisseur != null) {
			$title = "Commande du ".DateUtils::convert_sqlDate_to_slashDate($this->commandeFournisseur[COMMANDE_FOURNISSEUR_BOUTIQUE_DATE])." - <span class=\"normal\">Edition</span>";
		}
		return $title;
	}

	public function getOptionsFournisseur() {
		$html = "";

		$fournisseurs = $this->fournisseurProduitBoutiqueDAO->getAll();
		if (sizeof($fournisseurs) > 0) {
			foreach ($fournisseurs as $fournisseur) {
				$selected = "";
				if ($this->commandeFournisseur[COMMANDE_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID] == $fournisseur[FOURNISSEUR_PRODUIT_BOUTIQUE_ID]) {
					$selected = " selected";
				}
				$html .= "<option value=\"".$fournisseur[FOURNISSEUR_PRODUIT_BOUTIQUE_ID]."\"".$selected.">".$fournisseur[FOURNISSEUR_PRODUIT_BOUTIQUE_NOM]."</option>";
			}
		}

		return $html;
	}

	public function getDateCommandeFournisseur() {
		if ($this->commandeFournisseur != null) {
			return DateUtils::convert_sqlDate_to_slashDate($this->commandeFournisseur[COMMANDE_FOURNISSEUR_BOUTIQUE_DATE]);
		}
		return "";
	}
}

?>