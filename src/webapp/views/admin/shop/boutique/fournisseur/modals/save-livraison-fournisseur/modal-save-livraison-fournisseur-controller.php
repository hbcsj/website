<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/dao/pack-billetterie-dao.php");
require_once("common/php/dao/livraison-fournisseur-boutique-dao.php");
require_once("common/php/dao/fournisseur-produit-boutique-dao.php");

class ModalSaveLivraisonFournisseurCtrl extends AbstractViewCtrl {
	
	private $livraisonFournisseur;

	private $livraisonFournisseurBoutiqueDAO;
	private $fournisseurProduitBoutiqueDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idLivraisonFournisseur" => $_GET["idLivraisonFournisseur"]
		), true);
		
		if (isAdminConnected_commercial()) {
			$this->livraisonFournisseurBoutiqueDAO = new LivraisonFournisseurBoutiqueDAO($this->getDatabaseConnection());
			$this->fournisseurProduitBoutiqueDAO = new FournisseurProduitBoutiqueDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdLivraisonFournisseur();
	}

	private function checkIdLivraisonFournisseur() {
		if (HTTPUtils::paramExists(GET, "idLivraisonFournisseur")) {
			$this->livraisonFournisseur = $this->livraisonFournisseurBoutiqueDAO->getById($_GET["idLivraisonFournisseur"]);
				
			if ($this->livraisonFournisseur == null) {
				$this->sendCheckError(
					HTTP_404, 
					"La livraisonFournisseurBoutique (idLivraisonFournisseur = '".$_GET["idLivraisonFournisseur"]."') n'existe pas", 
					"webapp/views/common/error/404/404.html.php"
				);
			}
		}
	}

	public function getTitle() {
		$title = "Nouvelle livraison";
		if ($this->livraisonFournisseur != null) {
			$title = "Livraison du ".DateUtils::convert_sqlDate_to_slashDate($this->livraisonFournisseur[LIVRAISON_FOURNISSEUR_BOUTIQUE_DATE])." - <span class=\"normal\">Edition</span>";
		}
		return $title;
	}

	public function getOptionsFournisseur() {
		$html = "";

		$fournisseurs = $this->fournisseurProduitBoutiqueDAO->getAll();
		if (sizeof($fournisseurs) > 0) {
			foreach ($fournisseurs as $fournisseur) {
				$selected = "";
				if ($this->livraisonFournisseur[LIVRAISON_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID] == $fournisseur[FOURNISSEUR_PRODUIT_BOUTIQUE_ID]) {
					$selected = " selected";
				}
				$html .= "<option value=\"".$fournisseur[FOURNISSEUR_PRODUIT_BOUTIQUE_ID]."\"".$selected.">".$fournisseur[FOURNISSEUR_PRODUIT_BOUTIQUE_NOM]."</option>";
			}
		}

		return $html;
	}

	public function getDateLivraisonFournisseur() {
		if ($this->livraisonFournisseur != null) {
			return DateUtils::convert_sqlDate_to_slashDate($this->livraisonFournisseur[LIVRAISON_FOURNISSEUR_BOUTIQUE_DATE]);
		}
		return "";
	}
}

?>