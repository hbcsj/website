<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/shop/boutique/fournisseur/modals/save-fournisseur/", 
    "modal-save-fournisseur", 
    "ModalSaveFournisseurCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
?>
    <div class="modal-header">
        <div class="modal-title"><?php echo $ctrl->getTitle(); ?></div>
    </div>
    <div class="modal-body">
        <div class="form-horizontal" id="form-save-fournisseur">
            <div class="form-group">
                <label for="form-save-fournisseur__input-nom" class="col-xs-4 control-label required">Nom :</label>
                <div class="col-xs-7 form-input" for="form-save-fournisseur__input-nom">
                    <input type="text" class="form-control" id="form-save-fournisseur__input-nom" value="<?php echo $ctrl->getNomFournisseur(); ?>" required>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-fournisseur__input-nom"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-required text-right">Les champs marqu&eacute;s d'une * sont obligatoires</div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-8">
                <div class="form-result-message"></div>
            </div>
            <div class="col-xs-4 text-right">
                <button id="modal-save-fournisseur__bt-sauvegarder" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-ok"></span>
                    </div>
                    <div class="button__text">Sauvegarder</div>
                    <div class="loader"></div>
                </button>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>