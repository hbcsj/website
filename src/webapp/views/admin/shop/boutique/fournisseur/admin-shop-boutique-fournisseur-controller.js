AdminShopBoutiqueFournisseurController = {};

AdminShopBoutiqueFournisseurController.load_TableCommandesFournisseur = function() {
    if (Utils.exists('#container-table-commandes-fournisseur')) {
        AjaxUtils.loadView(
            'admin/shop/boutique/fournisseur/views/table-commandes-fournisseur/table-commandes-fournisseur',
            '#container-table-commandes-fournisseur',
            null,
            function() {
                control_AddCommandeFournisseur();
                control_EditCommandeFournisseur();
                control_DeleteCommandeFournisseur();
            },
            '#loader-page'
        );
    }

    function control_AddCommandeFournisseur() {
        $('#commandes-fournisseur__bt-ajouter').unbind('click');
        $('#commandes-fournisseur__bt-ajouter').click(function() {
            open_ModalSaveCommandeFournisseur(null);
        });
    }

    function control_EditCommandeFournisseur() {
        $('.icon-edit-commande-fournisseur').unbind('click');
        $('.icon-edit-commande-fournisseur').click(function() {
            var idCommandeFournisseur = $(this).attr('id-commande-fournisseur');
            open_ModalSaveCommandeFournisseur(idCommandeFournisseur);
        });
    }

    function open_ModalSaveCommandeFournisseur(idCommandeFournisseur) {
        var params = null;
        if (idCommandeFournisseur != null) {
            params = {
                idCommandeFournisseur: idCommandeFournisseur
            };
        }
        AjaxUtils.loadView(
            'admin/shop/boutique/fournisseur/modals/save-commande-fournisseur/modal-save-commande-fournisseur-content',
            '#modal-save-commande-fournisseur-content',
            params,
            function() {
                onClick_btSauvegarder(idCommandeFournisseur);
            },
            '#loader-page'
        );
        $('#modal-save-commande-fournisseur').modal('show');

        function onClick_btSauvegarder(idCommandeFournisseur) {
            $('#modal-save-commande-fournisseur__bt-sauvegarder').unbind('click');
            $('#modal-save-commande-fournisseur__bt-sauvegarder').click(function() {
                FormUtils.hideFormErrors('#modal-save-commande-fournisseur #form-save-commande-fournisseur');
                ViewUtils.unsetHTMLAndHide('#modal-save-commande-fournisseur .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('#modal-save-commande-fournisseur__bt-sauvegarder', '#modal-save-commande-fournisseur__bt-sauvegarder .loader');

                var formValidation = FormUtils.validateForm('#modal-save-commande-fournisseur #form-save-commande-fournisseur');
                
                if (formValidation) {
                    var inputDate = $.trim($('#form-save-commande-fournisseur__input-date').val());
                    var inputFournisseur = $('#form-save-commande-fournisseur__input-fournisseur').val();

                    saveCommandeFournisseur(idCommandeFournisseur, inputDate, inputFournisseur);
                } else {
                    ViewUtils.activeButtonAndHideLoader('#modal-save-commande-fournisseur__bt-sauvegarder', '#modal-save-commande-fournisseur__bt-sauvegarder .loader');
                }
            });

            function saveCommandeFournisseur(idCommandeFournisseur, inputDate, inputFournisseur) {
                AjaxUtils.callService(
                    'commande-fournisseur-boutique', 
                    'save', 
                    AjaxUtils.dataTypes.json, 
                    AjaxUtils.requestTypes.post, 
                    {
                        id: idCommandeFournisseur
                    }, 
                    {
                        date: inputDate,
                        fournisseurProduitBoutiqueId: inputFournisseur
                    }, 
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-save-commande-fournisseur .form-result-message', 'Commande sauvegard&eacute;e', FormUtils.resultsTypes.ok);
                            $('#modal-save-commande-fournisseur__bt-sauvegarder .loader').hide();
                            AdminShopBoutiqueFournisseurController.load_TableCommandesFournisseur();
                            AdminShopBoutiqueFournisseurController.load_TableFournisseurs();
                        } else {
                            ViewUtils.activeButtonAndHideLoader('#modal-save-commande-fournisseur__bt-sauvegarder', '#modal-save-commande-fournisseur__bt-sauvegarder .loader');
                            FormUtils.displayFormResultMessage('#modal-save-commande-fournisseur .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('#modal-save-commande-fournisseur__bt-sauvegarder', '#modal-save-commande-fournisseur__bt-sauvegarder .loader');
                        FormUtils.displayFormResultMessage('#modal-save-commande-fournisseur .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            }
        }
    }

    function control_DeleteCommandeFournisseur() {
        $('.icon-delete-commande-fournisseur').unbind('click');
        $('.icon-delete-commande-fournisseur').click(function() {
            var idCommandeFournisseur = $(this).attr('id-commande-fournisseur');
            
            ViewUtils.prompt(
                'Supprimer cette commande ?', 
                {
                    btOkText: 'Oui',
                    btKoText: 'Non'
                },
                function() {
                    deleteCommandeFournisseur(idCommandeFournisseur);
                }, 
                null, 
                null
            );
        });

        function deleteCommandeFournisseur(idCommandeFournisseur) {
            $('.loader-delete[id-commande-fournisseur='+idCommandeFournisseur+']').show();
            AjaxUtils.callService(
                'commande-fournisseur-boutique', 
                'delete', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idCommandeFournisseur
                }, 
                function(response) {
                    $('.loader-delete[id-commande-fournisseur='+idCommandeFournisseur+']').hide();
                    if (response.success) {
                        AdminShopBoutiqueFournisseurController.load_TableCommandesFournisseur();
                        AdminShopBoutiqueFournisseurController.load_TableFournisseurs();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la suppression de la commande', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-delete[id-commande-fournisseur='+idCommandeFournisseur+']').hide();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        }
    }
};

AdminShopBoutiqueFournisseurController.load_TableLivraisonsFournisseur = function() {
    if (Utils.exists('#container-table-livraisons-fournisseur')) {
        AjaxUtils.loadView(
            'admin/shop/boutique/fournisseur/views/table-livraisons-fournisseur/table-livraisons-fournisseur',
            '#container-table-livraisons-fournisseur',
            null,
            function() {
                control_AddLivraisonFournisseur();
                control_EditLivraisonFournisseur();
                control_DeleteLivraisonFournisseur();
            },
            '#loader-page'
        );
    }

    function control_AddLivraisonFournisseur() {
        $('#livraisons-fournisseur__bt-ajouter').unbind('click');
        $('#livraisons-fournisseur__bt-ajouter').click(function() {
            open_ModalSaveLivraisonFournisseur(null);
        });
    }

    function control_EditLivraisonFournisseur() {
        $('.icon-edit-livraison-fournisseur').unbind('click');
        $('.icon-edit-livraison-fournisseur').click(function() {
            var idLivraisonFournisseur = $(this).attr('id-livraison-fournisseur');
            open_ModalSaveLivraisonFournisseur(idLivraisonFournisseur);
        });
    }

    function open_ModalSaveLivraisonFournisseur(idLivraisonFournisseur) {
        var params = null;
        if (idLivraisonFournisseur != null) {
            params = {
                idLivraisonFournisseur: idLivraisonFournisseur
            };
        }
        AjaxUtils.loadView(
            'admin/shop/boutique/fournisseur/modals/save-livraison-fournisseur/modal-save-livraison-fournisseur-content',
            '#modal-save-livraison-fournisseur-content',
            params,
            function() {
                onClick_btSauvegarder(idLivraisonFournisseur);
            },
            '#loader-page'
        );
        $('#modal-save-livraison-fournisseur').modal('show');

        function onClick_btSauvegarder(idLivraisonFournisseur) {
            $('#modal-save-livraison-fournisseur__bt-sauvegarder').unbind('click');
            $('#modal-save-livraison-fournisseur__bt-sauvegarder').click(function() {
                FormUtils.hideFormErrors('#modal-save-livraison-fournisseur #form-save-livraison-fournisseur');
                ViewUtils.unsetHTMLAndHide('#modal-save-livraison-fournisseur .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('#modal-save-livraison-fournisseur__bt-sauvegarder', '#modal-save-livraison-fournisseur__bt-sauvegarder .loader');

                var formValidation = FormUtils.validateForm('#modal-save-livraison-fournisseur #form-save-livraison-fournisseur');
                
                if (formValidation) {
                    var inputDate = $.trim($('#form-save-livraison-fournisseur__input-date').val());
                    var inputFournisseur = $('#form-save-livraison-fournisseur__input-fournisseur').val();

                    saveLivraisonFournisseur(idLivraisonFournisseur, inputDate, inputFournisseur);
                } else {
                    ViewUtils.activeButtonAndHideLoader('#modal-save-livraison-fournisseur__bt-sauvegarder', '#modal-save-livraison-fournisseur__bt-sauvegarder .loader');
                }
            });

            function saveLivraisonFournisseur(idLivraisonFournisseur, inputDate, inputFournisseur) {
                AjaxUtils.callService(
                    'livraison-fournisseur-boutique', 
                    'save', 
                    AjaxUtils.dataTypes.json, 
                    AjaxUtils.requestTypes.post, 
                    {
                        id: idLivraisonFournisseur
                    }, 
                    {
                        date: inputDate,
                        fournisseurProduitBoutiqueId: inputFournisseur
                    }, 
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-save-livraison-fournisseur .form-result-message', 'Livraison sauvegard&eacute;e', FormUtils.resultsTypes.ok);
                            $('#modal-save-livraison-fournisseur__bt-sauvegarder .loader').hide();
                            AdminShopBoutiqueFournisseurController.load_TableLivraisonsFournisseur();
                            AdminShopBoutiqueFournisseurController.load_TableFournisseurs();
                        } else {
                            ViewUtils.activeButtonAndHideLoader('#modal-save-livraison-fournisseur__bt-sauvegarder', '#modal-save-livraison-fournisseur__bt-sauvegarder .loader');
                            FormUtils.displayFormResultMessage('#modal-save-livraison-fournisseur .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('#modal-save-livraison-fournisseur__bt-sauvegarder', '#modal-save-livraison-fournisseur__bt-sauvegarder .loader');
                        FormUtils.displayFormResultMessage('#modal-save-livraison-fournisseur .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            }
        }
    }

    function control_DeleteLivraisonFournisseur() {
        $('.icon-delete-livraison-fournisseur').unbind('click');
        $('.icon-delete-livraison-fournisseur').click(function() {
            var idLivraisonFournisseur = $(this).attr('id-livraison-fournisseur');
            
            ViewUtils.prompt(
                'Supprimer cette livraison ?', 
                {
                    btOkText: 'Oui',
                    btKoText: 'Non'
                },
                function() {
                    deleteLivraisonFournisseur(idLivraisonFournisseur);
                }, 
                null, 
                null
            );
        });

        function deleteLivraisonFournisseur(idLivraisonFournisseur) {
            $('.loader-delete[id-livraison-fournisseur='+idLivraisonFournisseur+']').show();
            AjaxUtils.callService(
                'livraison-fournisseur-boutique', 
                'delete', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idLivraisonFournisseur
                }, 
                function(response) {
                    $('.loader-delete[id-livraison-fournisseur='+idLivraisonFournisseur+']').hide();
                    if (response.success) {
                        AdminShopBoutiqueFournisseurController.load_TableLivraisonsFournisseur();
                        AdminShopBoutiqueFournisseurController.load_TableFournisseurs();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la suppression de la livraison', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-delete[id-livraison-fournisseur='+idLivraisonFournisseur+']').hide();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        }
    }
};

AdminShopBoutiqueFournisseurController.load_TableFournisseurs = function() {
    if (Utils.exists('#container-table-fournisseurs')) {
        AjaxUtils.loadView(
            'admin/shop/boutique/fournisseur/views/table-fournisseurs/table-fournisseurs',
            '#container-table-fournisseurs',
            null,
            function() {
                control_AddFournisseur();
                control_EditFournisseur();
                control_DeleteFournisseur();
            },
            '#loader-page'
        );
    }

    function control_AddFournisseur() {
        $('#fournisseurs__bt-ajouter').unbind('click');
        $('#fournisseurs__bt-ajouter').click(function() {
            open_ModalSaveFournisseur(null);
        });
    }

    function control_EditFournisseur() {
        $('.icon-edit-fournisseur').unbind('click');
        $('.icon-edit-fournisseur').click(function() {
            var idFournisseur = $(this).attr('id-fournisseur');
            open_ModalSaveFournisseur(idFournisseur);
        });
    }

    function open_ModalSaveFournisseur(idFournisseur) {
        var params = null;
        if (idFournisseur != null) {
            params = {
                idFournisseur: idFournisseur
            };
        }
        AjaxUtils.loadView(
            'admin/shop/boutique/fournisseur/modals/save-fournisseur/modal-save-fournisseur-content',
            '#modal-save-fournisseur-content',
            params,
            function() {
                onClick_btSauvegarder(idFournisseur);
            },
            '#loader-page'
        );
        $('#modal-save-fournisseur').modal('show');

        function onClick_btSauvegarder(idFournisseur) {
            $('#modal-save-fournisseur__bt-sauvegarder').unbind('click');
            $('#modal-save-fournisseur__bt-sauvegarder').click(function() {
                FormUtils.hideFormErrors('#modal-save-fournisseur #form-save-fournisseur');
                ViewUtils.unsetHTMLAndHide('#modal-save-fournisseur .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('#modal-save-fournisseur__bt-sauvegarder', '#modal-save-fournisseur__bt-sauvegarder .loader');

                var formValidation = FormUtils.validateForm('#modal-save-fournisseur #form-save-fournisseur');
                
                if (formValidation) {
                    var inputNom = $.trim($('#form-save-fournisseur__input-nom').val());

                    saveFournisseur(idFournisseur, inputNom);
                } else {
                    ViewUtils.activeButtonAndHideLoader('#modal-save-fournisseur__bt-sauvegarder', '#modal-save-fournisseur__bt-sauvegarder .loader');
                }
            });

            function saveFournisseur(idFournisseur, inputNom) {
                AjaxUtils.callService(
                    'fournisseur-produit-boutique', 
                    'save', 
                    AjaxUtils.dataTypes.json, 
                    AjaxUtils.requestTypes.post, 
                    {
                        id: idFournisseur
                    }, 
                    {
                        nom: inputNom
                    }, 
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-save-fournisseur .form-result-message', 'Fournisseur sauvegard&eacute;', FormUtils.resultsTypes.ok);
                            $('#modal-save-fournisseur__bt-sauvegarder .loader').hide();
                            AdminShopBoutiqueFournisseurController.load_TableFournisseurs();
                        } else {
                            ViewUtils.activeButtonAndHideLoader('#modal-save-fournisseur__bt-sauvegarder', '#modal-save-fournisseur__bt-sauvegarder .loader');
                            FormUtils.displayFormResultMessage('#modal-save-fournisseur .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('#modal-save-fournisseur__bt-sauvegarder', '#modal-save-fournisseur__bt-sauvegarder .loader');
                        FormUtils.displayFormResultMessage('#modal-save-fournisseur .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            }
        }
    }

    function control_DeleteFournisseur() {
        $('.icon-delete-fournisseur').unbind('click');
        $('.icon-delete-fournisseur').click(function() {
            var idFournisseur = $(this).attr('id-fournisseur');
            
            ViewUtils.prompt(
                'Supprimer ce fournisseur ?', 
                {
                    btOkText: 'Oui',
                    btKoText: 'Non'
                },
                function() {
                    deleteFournisseur(idFournisseur);
                }, 
                null, 
                null
            );
        });

        function deleteFournisseur(idFournisseur) {
            $('.loader-delete[id-fournisseur='+idFournisseur+']').show();
            AjaxUtils.callService(
                'fournisseur-produit-boutique', 
                'delete', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idFournisseur
                }, 
                function(response) {
                    $('.loader-delete[id-fournisseur='+idFournisseur+']').hide();
                    if (response.success) {
                        AdminShopBoutiqueFournisseurController.load_TableFournisseurs();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la suppression du fournisseur', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-delete[id-fournisseur='+idFournisseur+']').hide();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        }
    }
};

initializePage = function() {
    AdminController.initializePage('fournisseur', 'boutique');

    AdminShopBoutiqueFournisseurController.load_TableCommandesFournisseur();
    AdminShopBoutiqueFournisseurController.load_TableLivraisonsFournisseur();
    AdminShopBoutiqueFournisseurController.load_TableFournisseurs();
};