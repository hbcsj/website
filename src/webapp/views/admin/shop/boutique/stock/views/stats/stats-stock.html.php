<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/shop/boutique/stock/views/stats/", 
    "stats-stock", 
    "StatsStockCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
    ?>
    <div class="container-stats__section">
        <div class="container-stats__section__title">Produits en stock :</div>
        <div class="container-stats__section__value"><?php echo $ctrl->getProduitsEnStock(); ?></div>
    </div>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>