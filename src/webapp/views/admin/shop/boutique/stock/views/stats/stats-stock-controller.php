<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/stock-produit-boutique-dao.php");

class StatsStockCtrl extends AbstractViewCtrl {

	private $produitBoutiqueDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_commercial()) {
			$this->stockProduitBoutiqueDAO = new StockProduitBoutiqueDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getProduitsEnStock() {
		return $this->stockProduitBoutiqueDAO->getStockTotal()[STOCK_PRODUIT_BOUTIQUE_STOCK_TOTAL];
	}
}

?>