<?php
require_once("core/php/resources/view.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/shop/boutique/stock/views/table/", 
    "table-stock", 
    "TableStockCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
    $produits = $ctrl->getProduits();
    ?>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Produit</th>
                <th>Cat&eacute;g.</th>
                <th>Marque</th>
                <th class="text-center">Tailles / Stock</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (sizeof($produits) > 0) {
                foreach ($produits as $produit) {
                    ?>
                    <tr>
                        <td>
                            <?php
                            echo $produit[PRODUIT_BOUTIQUE_NOM];
                            if ($produit[PRODUIT_BOUTIQUE_INDICATION] != "") {
                                echo "<br>".$produit[PRODUIT_BOUTIQUE_INDICATION];
                            }
                            ?>
                        </td>
                        <td><?php echo $ctrl->getNomCategorie($produit[PRODUIT_BOUTIQUE_CATEGORIE_PRODUIT_BOUTIQUE_ID]); ?></td>
                        <td><?php echo $ctrl->getNomMarque($produit[PRODUIT_BOUTIQUE_MARQUE_PRODUIT_BOUTIQUE_ID]); ?></td>
                        <td>
                            <?php
                            $tailles = $ctrl->getTaillesPossibles($produit[PRODUIT_BOUTIQUE_TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_ID]);

                            if (sizeof($tailles) > 0) {
                                $i = 1;
                                foreach ($tailles as $taille) {
                                    $stock = $ctrl->getStock($produit[PRODUIT_BOUTIQUE_ID], $taille);
                                    ?>
                                    <div class="form-horizontal" id-produit="<?php echo $produit[PRODUIT_BOUTIQUE_ID]; ?>" taille="<?php echo $taille; ?>">
                                        <div class="form-group">
                                            <label class="col-xs-2 control-label"><?php echo $taille; ?></label>
                                            <div class="col-xs-6 col-lg-3" for="input-stock-produit__<?php echo $produit[PRODUIT_BOUTIQUE_ID]."__".$i; ?>">
                                                <div class="input-group">
                                                    <span class="input-group-addon input-group-addon-minus-stock-produit clickable"
                                                        id-produit="<?php echo $produit[PRODUIT_BOUTIQUE_ID]; ?>" 
                                                        taille="<?php echo $taille; ?>"
                                                    >
                                                        <span class="glyphicon glyphicon-minus"></span>
                                                    </span>
                                                    <input type="text" class="form-control input-stock-produit" 
                                                        id="input-stock-produit__<?php echo $produit[PRODUIT_BOUTIQUE_ID]."__".$i; ?>""
                                                        id-produit="<?php echo $produit[PRODUIT_BOUTIQUE_ID]; ?>" 
                                                        taille="<?php echo $taille; ?>"
                                                        value="<?php echo $stock; ?>" 
                                                        pattern="^[0-9]+$" 
                                                        required
                                                    >
                                                    <span class="input-group-addon input-group-addon-plus-stock-produit clickable"
                                                        id-produit="<?php echo $produit[PRODUIT_BOUTIQUE_ID]; ?>" 
                                                        taille="<?php echo $taille; ?>"
                                                    >
                                                        <span class="glyphicon glyphicon-plus"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-xs-1">
                                                <button 
                                                    class="btn btn-default bt-table-admin bt-modifier-stock-produit" 
                                                    id-produit="<?php echo $produit[PRODUIT_BOUTIQUE_ID]; ?>" 
                                                    taille="<?php echo $taille; ?>"
                                                >
                                                    <div class="button__text">OK</div>
                                                    <div class="loader"></div>
                                                </button>
                                            </div>
                                            <div class="col-xs-1 form-icon-validation" for="input-stock-produit__<?php echo $produit[PRODUIT_BOUTIQUE_ID]."__".$i; ?>"></div>
                                            <div class="col-xs-2 form-result-message"></div>
                                        </div>
                                    </div>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>