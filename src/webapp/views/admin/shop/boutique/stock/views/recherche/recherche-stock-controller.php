<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/produit-boutique-dao.php");
require_once("common/php/dao/categorie-produit-boutique-dao.php");

class RechercheStockCtrl extends AbstractViewCtrl {

	private $produitBoutiqueDAO;
	private $categorieProduitBoutiqueDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_commercial()) {
			$this->produitBoutiqueDAO = new ProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->categorieProduitBoutiqueDAO = new CategorieProduitBoutiqueDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getProduits() {
		return $this->produitBoutiqueDAO->getAll(
			PRODUIT_BOUTIQUE_TABLE_NAME.".".PRODUIT_BOUTIQUE_NOM.", ".PRODUIT_BOUTIQUE_TABLE_NAME.".".PRODUIT_BOUTIQUE_INDICATION.", ".PRODUIT_BOUTIQUE_TABLE_NAME.".".PRODUIT_BOUTIQUE_CATEGORIE_PRODUIT_BOUTIQUE_ID
		);
	}

	public function getNomCategorie($categorieId) {
		$categorie = $this->categorieProduitBoutiqueDAO->getById($categorieId);
		return $categorie[CATEGORIE_PRODUIT_BOUTIQUE_NOM];
	}
}

?>