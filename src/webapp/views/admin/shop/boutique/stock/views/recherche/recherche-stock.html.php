<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/shop/boutique/stock/views/recherche/", 
    "recherche-stock", 
    "RechercheStockCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_commercial()) {
    $produits = $ctrl->getProduits();
    ?>
    <div class="container-recherche__title" title="Ouvrir le formulaire de recherche" for="form-recherche-stock">
        <div class="container-recherche__title__icon">
            <span class="glyphicon glyphicon-search"></span>
        </div>
        <div class="container-recherche__title__text">Recherche</div>
        <div class="container-recherche__title__arrow">
            <span class="glyphicon glyphicon-triangle-bottom"></span>
        </div>
    </div>
    <div class="form-horizontal" id="form-recherche-stock">
        <div class="form-group">
            <label for="form-recherche-stock__input-produit" class="col-xs-2 col-xs-offset-1 control-label">Produit :</label>
            <div class="col-xs-3 form-input" for="form-recherche-stock__input-produit">
                <select class="form-control" id="form-recherche-stock__input-produit">
                    <option value=""></option>
                    <?php
                    if (sizeof($produits) > 0) {
                        foreach ($produits as $produit) {
                            ?>
                            <option value="<?php echo $produit[PRODUIT_BOUTIQUE_ID]; ?>">
                                <?php 
                                echo $produit[PRODUIT_BOUTIQUE_NOM]." - ";
                                if ($produit[PRODUIT_BOUTIQUE_INDICATION] != "") {
                                    echo $produit[PRODUIT_BOUTIQUE_INDICATION]." - ";
                                }
                                echo $ctrl->getNomCategorie($produit[PRODUIT_BOUTIQUE_CATEGORIE_PRODUIT_BOUTIQUE_ID]); 
                                ?>
                            </option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
    <div class="container-recherche__footer" for="form-recherche-stock">
        <button id="recherche-stock__bt-rechercher" class="btn btn-default">
            <div class="button__icon">
                <span class="glyphicon glyphicon-search"></span>
            </div>
            <div class="button__text">Chercher</div>
        </button>
        <button id="recherche-stock__bt-reset" class="btn btn-default">
            <div class="button__icon">
                <span class="glyphicon glyphicon-erase"></span>
            </div>
            <div class="button__text">Reset</div>
        </button>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>