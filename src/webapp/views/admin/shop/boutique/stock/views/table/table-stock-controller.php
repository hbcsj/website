<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/produit-boutique-dao.php");
require_once("common/php/dao/stock-produit-boutique-dao.php");
require_once("common/php/dao/categorie-produit-boutique-dao.php");
require_once("common/php/dao/marque-produit-boutique-dao.php");
require_once("common/php/dao/tailles-possibles-produit-boutique-dao.php");
require_once("common/php/dao/commande-item-boutique-dao.php");

class TableStockCtrl extends AbstractViewCtrl {

	private $produitBoutiqueDAO;
	private $stockProduitBoutiqueDAO;
	private $categorieProduitBoutiqueDAO;
	private $marqueProduitBoutiqueDAO;
	private $taillesPossiblesProduitBoutiqueDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_commercial()) {
			$this->stockProduitBoutiqueDAO = new StockProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->produitBoutiqueDAO = new ProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->categorieProduitBoutiqueDAO = new CategorieProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->marqueProduitBoutiqueDAO = new MarqueProduitBoutiqueDAO($this->getDatabaseConnection());
			$this->taillesPossiblesProduitBoutiqueDAO = new TaillesPossiblesProduitBoutiqueDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getProduits() {
		$produits = array();
		if (HTTPUtils::paramExists(GET, "produitId")) {
			$produit = $this->produitBoutiqueDAO->getById($_GET["produitId"]);
			if ($produit != null) {
				$produits[] = $produit;
			}
		} else {
			$produits = $this->produitBoutiqueDAO->getAll(
				PRODUIT_BOUTIQUE_TABLE_NAME.".".PRODUIT_BOUTIQUE_NOM.", ".PRODUIT_BOUTIQUE_TABLE_NAME.".".PRODUIT_BOUTIQUE_INDICATION.", ".PRODUIT_BOUTIQUE_TABLE_NAME.".".PRODUIT_BOUTIQUE_CATEGORIE_PRODUIT_BOUTIQUE_ID
			);
		}
		return $produits;
	}

	public function getNomCategorie($categorieId) {
		$categorie = $this->categorieProduitBoutiqueDAO->getById($categorieId);
		return $categorie[CATEGORIE_PRODUIT_BOUTIQUE_NOM];
	}

	public function getNomMarque($marqueId) {
		$marque = $this->marqueProduitBoutiqueDAO->getById($marqueId);
		return $marque[MARQUE_PRODUIT_BOUTIQUE_NOM];
	}

	public function getTaillesPossibles($taillesPossiblesId) {
		$taillesPossibles = $this->taillesPossiblesProduitBoutiqueDAO->getById($taillesPossiblesId);
		return explode(TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TAILLES_SEPARATOR, $taillesPossibles[TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TAILLES_POSSIBLES]);
	}

	public function getStock($produitId, $taille) {
		$stockProduitBoutiqueId = array();
		$stockProduitBoutiqueId[STOCK_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID] = $produitId;
		$stockProduitBoutiqueId[STOCK_PRODUIT_BOUTIQUE_TAILLE] = $taille;

		$stock = $this->stockProduitBoutiqueDAO->getById($stockProduitBoutiqueId);

		$quantiteStock = 0;
		if ($stock != null) {
			$quantiteStock = $stock[STOCK_PRODUIT_BOUTIQUE_QUANTITE];
		}

		return $quantiteStock;
	}
}

?>