AdminShopBoutiqueStockController = {};

AdminShopBoutiqueStockController.load_StatsStocks = function() {
    if (Utils.exists('#container-stats-stock')) {
        AjaxUtils.loadView(
            'admin/shop/boutique/stock/views/stats/stats-stock',
            '#container-stats-stock',
            null,
            function() {},
            '#loader-page'
        );
    }
};

AdminShopBoutiqueStockController.load_RechercheAndTableStocks = function() {
    if (Utils.exists('#container-recherche-stock')) {
        AjaxUtils.loadView(
            'admin/shop/boutique/stock/views/recherche/recherche-stock',
            '#container-recherche-stock',
            null,
            function() {
                FormRechercheUtils.control_toggleFormRecherche();
                onClick_btRechercher();
                onClick_btReset();
                AdminShopBoutiqueStockController.load_TableStocks();
            },
            '#loader-page'
        );
    }

    function onClick_btRechercher() {
        $('#recherche-stock__bt-rechercher').unbind('click');
        $('#recherche-stock__bt-rechercher').click(function() {
            AdminShopBoutiqueStockController.load_TableStocks();
        });
    }

    function onClick_btReset() {
        $('#recherche-stock__bt-reset').unbind('click');
        $('#recherche-stock__bt-reset').click(function() {
            $('#form-recherche-stock__input-produit').val('');

            AdminShopBoutiqueStockController.load_TableStocks();
        });
    }
};

AdminShopBoutiqueStockController.load_TableStocks = function() {
    if (Utils.exists('#container-table-stock')) {
        var params = {
            produitId: $.trim($('#form-recherche-stock__input-produit').val())
        };
        AjaxUtils.loadView(
            'admin/shop/boutique/stock/views/table/table-stock',
            '#container-table-stock',
            params,
            function() {
                control_DiminuerStockProduit();
                control_AugmenterStockProduit();
                control_ModifierStockProduit();
            },
            '#loader-page'
        );

        function control_DiminuerStockProduit() {
            $('.input-group-addon-minus-stock-produit').unbind('click');
            $('.input-group-addon-minus-stock-produit').click(function() {
                var idProduit = $(this).attr('id-produit');
                var taille = $(this).attr('taille');
                var initialStock = parseInt($('.input-stock-produit[id-produit="'+idProduit+'"][taille="'+taille+'"]').val());
                
                if (initialStock != 0) {
                    saveStockProduit(idProduit, taille, initialStock - 1);
                }
            });
        }

        function control_AugmenterStockProduit() {
            $('.input-group-addon-plus-stock-produit').unbind('click');
            $('.input-group-addon-plus-stock-produit').click(function() {
                var idProduit = $(this).attr('id-produit');
                var taille = $(this).attr('taille');
                var initialStock = parseInt($('.input-stock-produit[id-produit="'+idProduit+'"][taille="'+taille+'"]').val());

                saveStockProduit(idProduit, taille, initialStock + 1);
            });
        }

        function control_ModifierStockProduit() {
            $('.bt-modifier-stock-produit').unbind('click');
            $('.bt-modifier-stock-produit').click(function() {
                var idProduit = $(this).attr('id-produit');
                var taille = $(this).attr('taille');

                saveStockProduit(idProduit, taille, null);
            });
        }

        function saveStockProduit(idProduit, taille, newStock) {
            $('.bt-modifier-stock-produit[id-produit="'+idProduit+'"][taille="'+taille+'"] .button__text').html('');
            ViewUtils.desactiveButtonAndShowLoader(
                '.bt-modifier-stock-produit[id-produit="'+idProduit+'"][taille="'+taille+'"]', 
                '.bt-modifier-stock-produit[id-produit="'+idProduit+'"][taille="'+taille+'"] .loader'
            );

            var formValidation = FormUtils.validateForm('.form-horizontal[id-produit="'+idProduit+'"][taille="'+taille+'"]');
                
            if (formValidation) {
                if (newStock != null) {
                    $('.input-stock-produit[id-produit="'+idProduit+'"][taille="'+taille+'"]').val(newStock);
                }

                FormUtils.hideFormErrors('.form-horizontal[id-produit="'+idProduit+'"][taille="'+taille+'"]');
                ViewUtils.unsetHTMLAndHide('.form-horizontal[id-produit="'+idProduit+'"][taille="'+taille+'"] .form-result-message');

                var quantiteStock = $('.input-stock-produit[id-produit="'+idProduit+'"][taille="'+taille+'"]').val();

                AjaxUtils.callService(
                    'stock-produit-boutique', 
                    'save', 
                    AjaxUtils.dataTypes.json, 
                    AjaxUtils.requestTypes.post, 
                    {
                        produitBoutiqueId: idProduit,
                        taille: taille
                    }, 
                    {
                        quantiteStock: quantiteStock
                    }, 
                    function(response) {
                        $('.bt-modifier-stock-produit[id-produit="'+idProduit+'"][taille="'+taille+'"] .button__text').html('OK');
                        ViewUtils.activeButtonAndHideLoader(
                            '.bt-modifier-stock-produit[id-produit="'+idProduit+'"][taille="'+taille+'"]', 
                            '.bt-modifier-stock-produit[id-produit="'+idProduit+'"][taille="'+taille+'"] .loader'
                        );
                        FormUtils.displayFormResultMessage(
                            '.form-horizontal[id-produit="'+idProduit+'"][taille="'+taille+'"] .form-result-message', 
                            'Stock sauvegard&eacute;', 
                            FormUtils.resultsTypes.ok
                        );
                        $('.input-stock-produit[id-produit="'+idProduit+'"][taille="'+taille+'"]').prop('readonly', false);
                    },
                    function(response) {
                        $('.bt-modifier-stock-produit[id-produit="'+idProduit+'"][taille="'+taille+'"] .button__text').html('OK');
                        ViewUtils.activeButtonAndHideLoader(
                            '.bt-modifier-stock-produit[id-produit="'+idProduit+'"][taille="'+taille+'"]', 
                            '.bt-modifier-stock-produit[id-produit="'+idProduit+'"][taille="'+taille+'"] .loader'
                        );
                        FormUtils.displayFormResultMessage(
                            '.form-horizontal[id-produit="'+idProduit+'"][taille="'+taille+'"] .form-result-message', 
                            'Service indisponible', 
                            FormUtils.resultsTypes.error
                        );
                        $('.input-stock-produit[id-produit="'+idProduit+'"][taille="'+taille+'"]').prop('readonly', false);
                    }
                );
            } else {
                $('.bt-modifier-stock-produit[id-produit="'+idProduit+'"][taille="'+taille+'"] .button__text').html('OK');
                ViewUtils.activeButtonAndHideLoader(
                    '.bt-modifier-stock-produit[id-produit="'+idProduit+'"][taille="'+taille+'"]', 
                    '.bt-modifier-stock-produit[id-produit="'+idProduit+'"][taille="'+taille+'"] .loader'
                );
            }
        }
    }
};

initializePage = function() {
    AdminController.initializePage('shop', 'boutique');

    AdminShopBoutiqueStockController.load_StatsStocks();
    AdminShopBoutiqueStockController.load_RechercheAndTableStocks();
};