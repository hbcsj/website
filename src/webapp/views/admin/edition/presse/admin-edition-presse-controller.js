AdminEditionPresseController = {};

AdminEditionPresseController.load_StatsArticlesPresse = function() {
    if (Utils.exists('#container-stats-articles-presse')) {
        AjaxUtils.loadView(
            'admin/edition/presse/views/stats/stats-articles-presse',
            '#container-stats-articles-presse',
            null,
            function() {},
            '#loader-page'
        );
    }
};

AdminEditionPresseController.load_RechercheAndTableArticlesPresse = function() {
    if (Utils.exists('#container-recherche-articles-presse')) {
        AjaxUtils.loadView(
            'admin/edition/presse/views/recherche/recherche-articles-presse',
            '#container-recherche-articles-presse',
            null,
            function() {
                FormRechercheUtils.control_toggleFormRecherche();
                onClick_btRechercher();
                onClick_btReset();
                AdminEditionPresseController.load_TableArticlesPresse();
            },
            '#loader-page'
        );
    }

    function onClick_btRechercher() {
        $('#recherche-articles-presse__bt-rechercher').unbind('click');
        $('#recherche-articles-presse__bt-rechercher').click(function() {
            AdminEditionPresseController.load_TableArticlesPresse();
        });
    }

    function onClick_btReset() {
        $('#recherche-articles-presse__bt-reset').unbind('click');
        $('#recherche-articles-presse__bt-reset').click(function() {
            $('#form-recherche-articles-presse__input-date-debut').val('');
            $('#form-recherche-articles-presse__input-date-fin').val('');
            $('#form-recherche-articles-presse__input-titre').val('');
            $('#form-recherche-articles-presse__input-journal').val('');

            AdminEditionPresseController.load_TableArticlesPresse();
        });
    }
};

AdminEditionPresseController.load_TableArticlesPresse = function() {
    if (Utils.exists('#container-table-articles-presse')) {
        var params = {
            dateDebut: $.trim($('#form-recherche-articles-presse__input-date-debut').val()),
            dateFin: $.trim($('#form-recherche-articles-presse__input-date-fin').val()),
            titre: $.trim($('#form-recherche-articles-presse__input-titre').val()),
            journalArticlePresseId: $('#form-recherche-articles-presse__input-journal').val()
        };
        AjaxUtils.loadView(
            'admin/edition/presse/views/table/table-articles-presse',
            '#container-table-articles-presse',
            params,
            function() {
                control_AddArticlePresse();
                control_EditArticlePresse();
                control_DeleteArticlePresse();
                control_ValidationArticlePresse();
            },
            '#loader-page'
        );
    }

    function control_AddArticlePresse() {
        $('#presse__bt-ajouter').unbind('click');
        $('#presse__bt-ajouter').click(function() {
            open_ModalSaveArticlePresse(null);
        });
    }

    function control_EditArticlePresse() {
        $('.glyphicon-pencil').unbind('click');
        $('.glyphicon-pencil').click(function() {
            var idArticlePresse = $(this).attr('id-article-presse');
            open_ModalSaveArticlePresse(idArticlePresse);
        });
    }

    function open_ModalSaveArticlePresse(idArticlePresse) {
        var params = null;
        if (idArticlePresse != null) {
            params = {
                idArticlePresse: idArticlePresse
            };
        }
        AjaxUtils.loadView(
            'admin/edition/presse/modals/save/modal-save-article-presse-content',
            '#modal-save-article-presse-content',
            params,
            function() {
                onClick_btSauvegarder();
            },
            '#loader-page'
        );
        $('#modal-save-article-presse').modal('show');
    }

    function onClick_btSauvegarder() {
        $('#modal-save-article-presse__bt-sauvegarder').unbind('click');
        $('#modal-save-article-presse__bt-sauvegarder').click(function() {
            FormUtils.hideFormErrors('#modal-save-article-presse #form-save-article-presse');
            ViewUtils.unsetHTMLAndHide('#modal-save-article-presse .form-result-message');
            ViewUtils.desactiveButtonAndShowLoader('#modal-save-article-presse__bt-sauvegarder', '#modal-save-article-presse__bt-sauvegarder .loader');

            var formValidation = FormUtils.validateForm('#modal-save-article-presse #form-save-article-presse');
            
            if (formValidation) {
                document.getElementById('form-save-article-presse').submit();
            } else {
                ViewUtils.activeButtonAndHideLoader('#modal-save-article-presse__bt-sauvegarder', '#modal-save-article-presse__bt-sauvegarder .loader');
            }
        });
    }

    function control_DeleteArticlePresse() {
        $('.glyphicon-trash').unbind('click');
        $('.glyphicon-trash').click(function() {
            var idArticlePresse = $(this).attr('id-article-presse');
            
            ViewUtils.prompt(
                'Supprimer cet article ?', 
                {
                    btOkText: 'Oui',
                    btKoText: 'Non'
                },
                function() {
                    deleteArticlePresse(idArticlePresse);
                }, 
                null, 
                null
            );
        });

        function deleteArticlePresse(idArticlePresse) {
            $('.loader-delete[id-article-presse='+idArticlePresse+']').show();
            AjaxUtils.callService(
                'article-presse', 
                'delete', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idArticlePresse
                }, 
                function(response) {
                    $('.loader-delete[id-article-presse='+idArticlePresse+']').hide();
                    if (response.success) {
                        AdminEditionPresseController.load_StatsArticlesPresse();
                        AdminEditionPresseController.load_TableArticlesPresse();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la suppression de l\'article', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-delete[id-article-presse='+idArticlePresse+']').hide();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        }
    }

    function control_ValidationArticlePresse() {
        $('.icon-validation').unbind('click');
        $('.icon-validation').click(function() {
            var idArticlePresse = $(this).attr('id-article-presse');
            
            $('.loader-validation[id-article-presse='+idArticlePresse+']').show();
            AjaxUtils.callService(
                'article-presse', 
                'toggleValidation', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idArticlePresse
                }, 
                function(response) {
                    $('.loader-validation[id-article-presse='+idArticlePresse+']').hide();
                    if (response.success) {
                        AdminEditionPresseController.load_TableArticlesPresse();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la validation / invalidation de l\'article', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-validation[id-article-presse='+idArticlePresse+']').hide();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        });
    }
};

initializePage = function() {
    AdminController.initializePage('edition', 'presse');

    AdminEditionPresseController.load_StatsArticlesPresse();
    AdminEditionPresseController.load_RechercheAndTableArticlesPresse();
};