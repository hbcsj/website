<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/article-presse-dao.php");
require_once("common/php/dao/journal-article-presse-dao.php");

class ModalSaveArticlePresseCtrl extends AbstractViewCtrl {
	
	private $articlePresse;

	private $articlePresseDAO;
	private $journalArticlePresseDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idArticlePresse" => $_GET["idArticlePresse"]
		), true);
		
		if (isAdminConnected_editeur()) {
			$this->articlePresseDAO = new ArticlePresseDAO($this->getDatabaseConnection());
			$this->journalArticlePresseDAO = new JournalArticlePresseDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdArticlePresse();
	}

	private function checkIdArticlePresse() {
		if (HTTPUtils::paramExists(GET, "idArticlePresse")) {
			$this->articlePresse = $this->articlePresseDAO->getById($_GET["idArticlePresse"]);
				
			if ($this->articlePresse == null) {
				$this->sendCheckError(
					HTTP_404, 
					"L'article de presse (idArticlePresse = '".$_GET["idArticlePresse"]."') n'existe pas", 
					"webapp/views/common/error/404/404.html.php"
				);
			}
		}
	}

	public function getArticlePresse() {
		return $this->articlePresse;
	}

	public function getTitle() {
		$title = "Nouvel article de presse";
		if ($this->articlePresse != null) {
			$title = $this->articlePresse[ARTICLE_PRESSE_TITRE]." - <span class=\"normal\">Edition</span>";
		}
		return $title;
	}

	public function getTitreArticlePresse() {
		if ($this->articlePresse != null) {
			return $this->articlePresse[ARTICLE_PRESSE_TITRE];
		}
		return "";
	}

	public function getUrlArticlePresse() {
		if ($this->articlePresse != null) {
			return $this->articlePresse[ARTICLE_PRESSE_URL];
		}
		return "";
	}

	public function getParamIdArticlePresse() {
		if ($this->articlePresse != null) {
			return "?id=".$this->articlePresse[ARTICLE_PRESSE_ID];
		}
		return "";
	}

	public function getOptionsJournaux() {
		$html = "";

		$journaux = $this->journalArticlePresseDAO->getAll();
		if (sizeof($journaux) > 0) {
			foreach ($journaux as $journal) {
				$selected = "";
				if ($this->articlePresse[ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID] == $journal[JOURNAL_ARTICLE_PRESSE_ID]) {
					$selected = " selected";
				}
				$html .= "<option value=\"".$journal[JOURNAL_ARTICLE_PRESSE_ID]."\"".$selected.">".$journal[JOURNAL_ARTICLE_PRESSE_NOM]."</option>";
			}
		}

		return $html;
	}

	public function getVisibleChecked() {
		$checked = "";
		if (
			($this->articlePresse == null && isAdminConnected_bureau()) || 
			($this->articlePresse != null && isAdminConnected_bureau() && $this->articlePresse[ARTICLE_PRESSE_VISIBLE_SUR_SITE])
		) {
			$checked = " checked=\"checked\"";
		}
        return $checked;
	}

	public function getEmailDernierEditeurArticlePresse() {
		if (isAdminConnected_superadmin()) {
			if ($this->articlePresse != null) {
				return $this->articlePresse[ARTICLE_PRESSE_EMAIL_DERNIER_EDITEUR];
			} else {
				return $GLOBALS[CONF][SUPERADMIN_EMAIL];
			}
		} 
		return "";
	}
}

?>