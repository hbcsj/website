<?php
require_once("core/php/resources/view.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/edition/presse/modals/save/", 
    "modal-save-article-presse", 
    "ModalSaveArticlePresseCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_editeur()) {
    $filesRequired = "";
    if ($ctrl->getArticlePresse() == null) {
        $filesRequired = " required";
    }
    ?>
    <div class="modal-header">
        <div class="modal-title"><?php echo $ctrl->getTitle(); ?></div>
    </div>
    <div class="modal-body">
        <form id="form-save-article-presse" method="post" action="<?php echo ROOT_PATH.UPLOADER_PATH; ?>/article-presse<?php echo $ctrl->getParamIdArticlePresse(); ?>" enctype="multipart/form-data" class="form-horizontal" target="uploader-article-presse">
            <div class="form-group">
                <label for="form-save-article-presse__input-titre" class="col-xs-4 control-label required">Titre :</label>
                <div class="col-xs-7 form-input" for="form-save-article-presse__input-titre">
                    <input type="text" class="form-control" id="form-save-article-presse__input-titre" name="titre" value="<?php echo $ctrl->getTitreArticlePresse(); ?>" required>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-article-presse__input-titre"></div>
            </div>
            <div class="form-group">
                <label for="form-save-article-presse__input-journal" class="col-xs-4 control-label required">Journal :</label>
                <div class="col-xs-7 form-input" for="form-save-article-presse__input-journal">
                    <select class="form-control" id="form-save-article-presse__input-journal" name="journal-article-presse-id" required>
                        <option value=""></option>
                        <?php echo $ctrl->getOptionsJournaux(); ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-article-presse__input-journal"></div>
            </div>
            <div class="form-group">
                <label for="form-save-article-presse__input-url" class="col-xs-4 control-label required">Lien :</label>
                <div class="col-xs-7 form-input" for="form-save-article-presse__input-url">
                    <input type="text" class="form-control" id="form-save-article-presse__input-url" name="url" value="<?php echo $ctrl->getUrlArticlePresse(); ?>" required>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-article-presse__input-url"></div>
            </div>
            <div class="form-group">
                <label for="form-save-article-presse__input-image" class="col-xs-4 control-label<?php echo $filesRequired; ?>">Image :</label>
                <div class="col-xs-7 form-input" for="form-save-article-presse__input-image">
                    <input type="file" class="form-control" id="form-save-article-presse__input-image" name="article-presse-image" pattern="^(.)+(\.<?php echo ARTICLES_PRESSE_EXTENSION; ?>)$" pattern-indication="L'extension doit &ecirc;tre '.<?php echo ARTICLES_PRESSE_EXTENSION; ?>'"<?php echo $filesRequired; ?>>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-article-presse__input-image"></div>
            </div>
            <div class="form-group">
                <label for="form-save-article-presse__input-image-medium" class="col-xs-4 control-label<?php echo $filesRequired; ?>">Image (largeur=600px) :</label>
                <div class="col-xs-7 form-input" for="form-save-article-presse__input-image-medium">
                    <input type="file" class="form-control" id="form-save-article-presse__input-image-medium" name="article-presse-image-medium" pattern="^(.)+(\.<?php echo ARTICLES_PRESSE_EXTENSION; ?>)$" pattern-indication="L'extension doit &ecirc;tre '.<?php echo ARTICLES_PRESSE_EXTENSION; ?>'"<?php echo $filesRequired; ?>>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-article-presse__input-image-medium"></div>
            </div>
            <div class="form-group">
                <label for="form-save-article-presse__input-image-small" class="col-xs-4 control-label<?php echo $filesRequired; ?>">Image (largeur=400px) :</label>
                <div class="col-xs-7 form-input" for="form-save-article-presse__input-image-small">
                    <input type="file" class="form-control" id="form-save-article-presse__input-image-small" name="article-presse-image-small" pattern="^(.)+(\.<?php echo ARTICLES_PRESSE_EXTENSION; ?>)$" pattern-indication="L'extension doit &ecirc;tre '.<?php echo ARTICLES_PRESSE_EXTENSION; ?>'"<?php echo $filesRequired; ?>>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-article-presse__input-image-small"></div>
            </div>
            <div class="form-group">
                <label for="form-save-article-presse__input-image-home" class="col-xs-4 control-label<?php echo $filesRequired; ?>">Image (accueil) :</label>
                <div class="col-xs-7 form-input" for="form-save-article-presse__input-image-home">
                    <input type="file" class="form-control" id="form-save-article-presse__input-image-home" name="article-presse-image-home" pattern="^(.)+(\.<?php echo ARTICLES_PRESSE_EXTENSION; ?>)$" pattern-indication="L'extension doit &ecirc;tre '.<?php echo ARTICLES_PRESSE_EXTENSION; ?>'"<?php echo $filesRequired; ?>>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-article-presse__input-image-home"></div>
            </div>
            <div class="form-group">
                <label for="form-save-article-presse__input-visible" class="col-xs-4 control-label">Visible ?</label>
                <div class="col-xs-1 form-input" for="form-save-article-presse__input-visible">
                    <?php
                    $disabledVisible = "";
                    if (!isAdminConnected_bureau()) {
                        $disabledVisible = " disabled";
                    }
                    ?>
                    <input type="checkbox" id="form-save-article-presse__input-visible" name="visible-sur-site"<?php echo $ctrl->getVisibleChecked(); ?><?php echo $disabledVisible; ?>>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-article-presse__input-visible"></div>
                <label for="form-save-article-presse__input-email-dernier-editeur" class="col-xs-1 control-label required">&nbsp;</label>
                <div class="col-xs-4 form-input" for="form-save-article-presse__input-email-dernier-editeur">
                    <?php
                    $disabledEmailDernierEditeur = "";
                    if (isAdminConnected_superadmin()) {
                        $disabledEmailDernierEditeur = " readonly";
                    }
                    ?>
                    <input type="text" class="form-control" id="form-save-article-presse__input-email-dernier-editeur" name="email-dernier-editeur" value="<?php echo $ctrl->getEmailDernierEditeurArticlePresse(); ?>" required <?php echo $disabledEmailDernierEditeur; ?> placeholder="Email dernier &eacute;diteur" pattern="^[A-Za-z0-9._-]+@[A-Za-z0-9.-]{2,}[.][A-Za-z]{2,3}$" pattern-indication="Le format de l'email est invalide">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-article-presse__input-email-dernier-editeur"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-required text-right">Les champs marqu&eacute;s d'une * sont obligatoires</div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-8">
                <div class="form-result-message"></div>
            </div>
            <div class="col-xs-4 text-right">
                <button id="modal-save-article-presse__bt-sauvegarder" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-ok"></span>
                    </div>
                    <div class="button__text">Sauvegarder</div>
                    <div class="loader"></div>
                </button>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>