<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/article-presse-dao.php");
require_once("common/php/dao/journal-article-presse-dao.php");
require_once("common/php/lib/date-utils.php");

class TableArticlesPresseCtrl extends AbstractViewCtrl {

	private $articlePresseDAO;
	private $journalArticlePresseDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_editeur()) {
			$this->articlePresseDAO = new ArticlePresseDAO($this->getDatabaseConnection());
			$this->journalArticlePresseDAO = new JournalArticlePresseDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getArticlesPresse() {
		return $this->articlePresseDAO->getAllByParams(
            DateUtils::convert_slashDate_to_sqlDate($_GET["dateDebut"]),
			DateUtils::convert_slashDate_to_sqlDate($_GET["dateFin"]),
			trim(strtolower($_GET["titre"])),
			$_GET["journalArticlePresseId"],
            ARTICLE_PRESSE_TABLE_NAME.".".ARTICLE_PRESSE_DATE." DESC, ".ARTICLE_PRESSE_TABLE_NAME.".".ARTICLE_PRESSE_ID." DESC"
        );
	}

	public function getNomJournalArticlePresse($journalArticlePresseId) {
		$journal = $this->journalArticlePresseDAO->getById($journalArticlePresseId);
		return $journal[JOURNAL_ARTICLE_PRESSE_NOM];
	}

	public function isArticlePresseVisible($actualite) {
		return (
			$actualite[ARTICLE_PRESSE_VISIBLE_SUR_SITE] && 
			$actualite[ARTICLE_PRESSE_EMAIL_DERNIER_EDITEUR] == $GLOBALS[CONF][SUPERADMIN_EMAIL]
		);
	}
}

?>