<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/path-utils.php");

$view = new View(
    "webapp/views/admin/edition/presse/views/table/", 
    "table-articles-presse", 
    "TableArticlesPresseCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_editeur()) {
    $articlesPresse = $ctrl->getArticlesPresse();
    ?>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Date</th>
                <th>Titre</th>
                <th>Journal</th>
                <th class="text-center">Lien</th>
                <th class="text-center">Article</th>
                <th class="text-center">Visible ?</th>
                <?php
                if (isAdminConnected_superadmin()) {
                    ?>
                    <th colspan="3">&nbsp;</th>
                    <?php
                } else if (isAdminConnected_bureau()) {
                    ?>
                    <th colspan="2">&nbsp;</th>
                    <?php
                } else {
                    ?>
                    <th>&nbsp;</th>
                    <?php
                }
                ?>
            </tr>
        </thead>
        <tbody>
            <?php
            if (sizeof($articlesPresse) > 0) {
                foreach ($articlesPresse as $articlePresse) {
                    $bg = "";
                    if ($articlePresse[ARTICLE_PRESSE_EMAIL_DERNIER_EDITEUR] != $GLOBALS[CONF][SUPERADMIN_EMAIL]) {
                        $bg = "bg-error";
                    } else if (!$articlePresse[ARTICLE_PRESSE_VISIBLE_SUR_SITE]) {
                        $bg = "bg-warning";
                    }
                    ?>
                    <tr>
                        <td class="<?php echo $bg; ?>"><?php echo DateUtils::convert_sqlDate_to_slashDate($articlePresse[ARTICLE_PRESSE_DATE]); ?></td>
                        <td class="<?php echo $bg; ?>"><?php echo $articlePresse[ARTICLE_PRESSE_TITRE]; ?></td>
                        <td class="<?php echo $bg; ?>"><?php echo $ctrl->getNomJournalArticlePresse($articlePresse[ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID]); ?></td>
                        <td class="text-center <?php echo $bg; ?>">
                            <a href="<?php echo $articlePresse[ARTICLE_PRESSE_URL]; ?>" target="_blank">Voir</a>
                        </td>
                        <td class="text-center <?php echo $bg; ?>">
                            <a href="<?php echo SRC_PATH.PathUtils::getArticlePresseImgFile($articlePresse[ARTICLE_PRESSE_ID]); ?>" target="_blank">Voir</a>
                        </td>
                        <td class="text-center <?php echo $bg; ?>"><?php echo ($articlePresse[ARTICLE_PRESSE_VISIBLE_SUR_SITE] == 1) ? "Oui" : "Non"; ?></td>
                        <td class="text-center <?php echo $bg; ?>">
                            <span class="glyphicon glyphicon-pencil clickable" title="Editer" id-article-presse="<?php echo $articlePresse[ARTICLE_PRESSE_ID]; ?>"></span>
                        </td>
                        <?php
                        if (isAdminConnected_bureau()) {
                            ?>
                            <td class="text-center <?php echo $bg; ?>">
                                <span class="glyphicon glyphicon-trash clickable" title="Supprimer" id-article-presse="<?php echo $articlePresse[ARTICLE_PRESSE_ID]; ?>"></span>
                                <div class="loader loader-delete" id-article-presse="<?php echo $articlePresse[ARTICLE_PRESSE_ID]; ?>"></div>
                            </td>
                            <?php
                        }
                        if (isAdminConnected_superadmin()) {
                            if (!$ctrl->isArticlePresseVisible($articlePresse)) {
                                ?>
                                <td class="text-center <?php echo $bg; ?>">
                                    <span class="glyphicon glyphicon-ok icon-validation clickable" title="Valider" id-article-presse="<?php echo $articlePresse[ARTICLE_PRESSE_ID]; ?>"></span>
                                    <div class="loader loader-validation" id-article-presse="<?php echo $articlePresse[ARTICLE_PRESSE_ID]; ?>"></div>
                                </td>
                                <?php
                            } else {
                                ?>
                                <td class="text-center <?php echo $bg; ?>">
                                    <span class="glyphicon glyphicon-remove icon-validation clickable" title="Invalider" id-article-presse="<?php echo $articlePresse[ARTICLE_PRESSE_ID]; ?>"></span>
                                    <div class="loader loader-validation" id-article-presse="<?php echo $articlePresse[ARTICLE_PRESSE_ID]; ?>"></div>
                                </td>
                                <?php
                            }
                        }
                        ?>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>