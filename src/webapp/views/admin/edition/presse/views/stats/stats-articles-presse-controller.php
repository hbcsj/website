<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/article-presse-dao.php");

class StatsArticlesPresseCtrl extends AbstractViewCtrl {

	private $articlePresseDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_editeur()) {
			$this->articlePresseDAO = new ArticlePresseDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getNbArticlesPresse() {
		return $this->articlePresseDAO->getNbTotalVisibles()[ARTICLE_PRESSE_NB_ARTICLES];
	}
}

?>