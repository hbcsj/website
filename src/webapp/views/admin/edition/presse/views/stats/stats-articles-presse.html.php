<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/edition/presse/views/stats/", 
    "stats-articles-presse", 
    "StatsArticlesPresseCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_editeur()) {
    ?>
    <div class="container-stats__section">
        <div class="container-stats__section__title">Nombre d'articles : </div>
        <div class="container-stats__section__value"><?php echo $ctrl->getNbArticlesPresse(); ?></div>
    </div>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>