<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/journal-article-presse-dao.php");

class RechercheArticlesPresseCtrl extends AbstractViewCtrl {

	private $journalArticlePresseDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_editeur()) {
            $this->journalArticlePresseDAO = new JournalArticlePresseDAO($this->getDatabaseConnection());
        } else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getJournauxArticlePresse() {
		return $this->journalArticlePresseDAO->getAll(JOURNAL_ARTICLE_PRESSE_TABLE_NAME.".".JOURNAL_ARTICLE_PRESSE_NOM);
	}
}

?>