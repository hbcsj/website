<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/edition/home/modals/save-home-message/", 
    "modal-save-home-message", 
    "ModalSaveHomeMessageCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_editeur()) {
    ?>
    <div class="modal-header">
        <div class="modal-title"><?php echo $ctrl->getTitle(); ?></div>
    </div>
    <div class="modal-body">
        <div class="form-horizontal" id="form-save-home-message">
            <div class="form-group">
                <label for="form-save-home-message__input-texte" class="col-xs-4 control-label required">Texte :</label>
                <div class="col-xs-7 form-input" for="form-save-home-message__input-texte">
                    <textarea class="form-control" rows="5" id="form-save-home-message__input-texte" required><?php echo $ctrl->getTexteHomeMessage(); ?></textarea>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-home-message__input-texte"></div>
            </div>
            <div class="form-group">
                <label for="form-save-home-message__input-criticite" class="col-xs-4 control-label required">Critic. :</label>
                <div class="col-xs-7 form-input" for="form-save-home-message__input-criticite">
                    <select class="form-control" id="form-save-home-message__input-criticite" required>
                        <option value=""></option>
                        <?php echo $ctrl->getOptionsCriticiteHomeMessage(); ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-home-message__input-criticite"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-8">
                <div class="form-result-message"></div>
            </div>
            <div class="col-xs-4 text-right">
                <button id="modal-save-home-message__bt-sauvegarder" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-ok"></span>
                    </div>
                    <div class="button__text">Sauvegarder</div>
                    <div class="loader"></div>
                </button>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>