<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/home-message-dao.php");
require_once("common/php/dao/criticite-dao.php");

class ModalSaveHomeMessageCtrl extends AbstractViewCtrl {
	
	private $homeMessage;

	private $homeMessageDAO;
	private $criticiteDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idHomeMessage" => $_GET["idHomeMessage"]
		), true);
		
		if (isAdminConnected_editeur()) {
			$this->homeMessageDAO = new HomeMessageDAO($this->getDatabaseConnection());
			$this->criticiteDAO = new CriticiteDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdHomeMessage();
	}

	private function checkIdHomeMessage() {
		if (HTTPUtils::paramExists(GET, "idHomeMessage")) {
			$this->homeMessage = $this->homeMessageDAO->getById($_GET["idHomeMessage"]);
				
			if ($this->homeMessage == null) {
				$this->sendCheckError(
					HTTP_404, 
					"Le home message (idHomeMessage = '".$_GET["idHomeMessage"]."') n'existe pas", 
					"webapp/views/common/error/404/404.html.php"
				);
			}
		}
	}

	public function getTitle() {
		$title = "Nouveau message";
		if ($this->homeMessage != null) {
			$title = "Message - <span class=\"normal\">Edition</span>";
		}
		return $title;
	}

	public function getHomeMessage() {
		return $this->homeMessage;
	}

	public function getTexteHomeMessage() {
		if ($this->homeMessage != null) {
			return $this->homeMessage[HOME_MESSAGE_TEXTE];
		}
		return "";
	}

	public function getOptionsCriticiteHomeMessage() {
		$html = "";

		$criticitex = $this->criticiteDAO->getAll();
		if (sizeof($criticitex) > 0) {
			foreach ($criticitex as $criticite) {
				$selected = "";
				if ($this->homeMessage[HOME_MESSAGE_CRITICITE_ID] == $criticite[CRITICITE_ID]) {
					$selected = " selected";
				}
				$html .= "<option value=\"".$criticite[CRITICITE_ID]."\"".$selected.">".$criticite[CRITICITE_LIBELLE]."</option>";
			}
		}

		return $html;
	}
}

?>