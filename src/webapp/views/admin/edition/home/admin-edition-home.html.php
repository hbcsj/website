<?php
require_once("core/php/resources/page.php");
require_once("common/php/lib/admin-utils.php");

$page = new Page(
    "webapp/views/admin/edition/home/", 
    "admin-edition-home", 
    "AdminEditionHomeCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/admin/common/nav/nav.html.php");
if (isAdminConnected_editeur()) {
    require_once("webapp/views/admin/edition/sub-menu/sub-menu-admin-edition.html.php");
}
?>

<div class="container-fluid animated fadeIn">
    <?php
    if (isAdminConnected_editeur()) {
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h1>Page d'accueil</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h2>
                    Messages
                    <button id="home-messages__bt-ajouter" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-plus"></span>
                        </div>
                        <div class="button__text">Ajouter un message</div>
                    </button>
                </h2>
            </div>
        </div>
        <div id="container-table-home-messages" class="container-table"></div>
        <?php
    } else {
        require_once("webapp/views/common/mdp-form/mdp-form.html.php");
    }
    ?>
</div>

<?php
require_once("webapp/views/common/modals/alert/modal-alert.html.php");
require_once("webapp/views/common/modals/prompt/modal-prompt.html.php");

if (isAdminConnected_editeur()) {
    require_once("webapp/views/admin/edition/home/modals/save-home-message/modal-save-home-message.html.php");
}

$page->finalizePage();
?>