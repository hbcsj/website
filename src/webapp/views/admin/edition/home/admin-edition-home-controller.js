AdminEditionHomeController = {};

AdminEditionHomeController.load_TableHomeMessages = function() {
    if (Utils.exists('#container-table-home-messages')) {
        AjaxUtils.loadView(
            'admin/edition/home/views/table-home-messages/table-home-messages',
            '#container-table-home-messages',
            null,
            function() {
                control_AddHomeMessage();
                control_EditHomeMessage();
                control_DeleteHomeMessage();
            },
            '#loader-page'
        );
    }

    function control_AddHomeMessage() {
        $('#home-messages__bt-ajouter').unbind('click');
        $('#home-messages__bt-ajouter').click(function() {
            open_ModalSaveHomeMessage(null);
        });
    }

    function control_EditHomeMessage() {
        $('.glyphicon-pencil').unbind('click');
        $('.glyphicon-pencil').click(function() {
            var idHomeMessage = $(this).attr('id-home-message');
            open_ModalSaveHomeMessage(idHomeMessage);
        });
    }

    function open_ModalSaveHomeMessage(idHomeMessage) {
        var params = null;
        if (idHomeMessage != null) {
            params = {
                idHomeMessage: idHomeMessage
            };
        }
        AjaxUtils.loadView(
            'admin/edition/home/modals/save-home-message/modal-save-home-message-content',
            '#modal-save-home-message-content',
            params,
            function() {
                onClick_btSauvegarder(idHomeMessage);
            },
            '#loader-page'
        );
        $('#modal-save-home-message').modal('show');

        function onClick_btSauvegarder(idHomeMessage) {
            $('#modal-save-home-message__bt-sauvegarder').unbind('click');
            $('#modal-save-home-message__bt-sauvegarder').click(function() {
                FormUtils.hideFormErrors('#modal-save-home-message #form-save-home-message');
                ViewUtils.unsetHTMLAndHide('#modal-save-home-message .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('#modal-save-home-message__bt-sauvegarder', '#modal-save-home-message__bt-sauvegarder .loader');

                var formValidation = FormUtils.validateForm('#modal-save-home-message #form-save-home-message');
                
                if (formValidation) {
                    var inputTexte = $.trim($('#form-save-home-message__input-texte').val());
                    var inputCriticite = $('#form-save-home-message__input-criticite').val();

                    saveHomeMessage(idHomeMessage, inputTexte, inputCriticite);
                } else {
                    ViewUtils.activeButtonAndHideLoader('#modal-save-home-message__bt-sauvegarder', '#modal-save-home-message__bt-sauvegarder .loader');
                }
            });

            function saveHomeMessage(idHomeMessage, inputTexte, inputCriticite) {
                AjaxUtils.callService(
                    'home-message', 
                    'save', 
                    AjaxUtils.dataTypes.json, 
                    AjaxUtils.requestTypes.post, 
                    {
                        id: idHomeMessage
                    }, 
                    {
                        texte: inputTexte,
                        criticiteId: inputCriticite
                    }, 
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-save-home-message .form-result-message', 'Message sauvegard&eacute;', FormUtils.resultsTypes.ok);
                            $('#modal-save-home-message__bt-sauvegarder .loader').hide();
                            AdminEditionHomeController.load_TableHomeMessages();
                        } else {
                            ViewUtils.activeButtonAndHideLoader('#modal-save-home-message__bt-sauvegarder', '#modal-save-home-message__bt-sauvegarder .loader');
                            FormUtils.displayFormResultMessage('#modal-save-home-message .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('#modal-save-home-message__bt-sauvegarder', '#modal-save-home-message__bt-sauvegarder .loader');
                        FormUtils.displayFormResultMessage('#modal-save-home-message .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            }
        }
    }

    function control_DeleteHomeMessage() {
        $('.glyphicon-trash').unbind('click');
        $('.glyphicon-trash').click(function() {
            var idHomeMessage = $(this).attr('id-home-message');
            
            ViewUtils.prompt(
                'Supprimer ce message ?', 
                {
                    btOkText: 'Oui',
                    btKoText: 'Non'
                },
                function() {
                    deleteHomeMessage(idHomeMessage);
                }, 
                null, 
                null
            );
        });

        function deleteHomeMessage(idHomeMessage) {
            $('.loader-delete[id-home-message='+idHomeMessage+']').show();
            AjaxUtils.callService(
                'home-message', 
                'delete', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idHomeMessage
                }, 
                function(response) {
                    $('.loader-delete[id-home-message='+idHomeMessage+']').hide();
                    if (response.success) {
                        AdminEditionHomeController.load_TableHomeMessages();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la suppression du message', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-delete[id-home-message='+idHomeMessage+']').hide();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        }
    }
};

initializePage = function() {
    AdminController.initializePage('edition', 'home');

    AdminEditionHomeController.load_TableHomeMessages();
};