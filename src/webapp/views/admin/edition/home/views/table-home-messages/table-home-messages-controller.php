<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/home-message-dao.php");
require_once("common/php/dao/criticite-dao.php");

class TableHomeMessagesCtrl extends AbstractViewCtrl {

	private $homeMessageDAO;
	private $criticiteDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_editeur()) {
			$this->homeMessageDAO = new HomeMessageDAO($this->getDatabaseConnection());
			$this->criticiteDAO = new CriticiteDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getHomeMessages() {
		return $this->homeMessageDAO->getAll(HOME_MESSAGE_TABLE_NAME.".".HOME_MESSAGE_ID." DESC");
	}

    public function getLibelleCriticite($criticiteId) {
        $criticite = $this->criticiteDAO->getById($criticiteId);
        return $criticite[CRITICITE_LIBELLE];
    }
}

?>