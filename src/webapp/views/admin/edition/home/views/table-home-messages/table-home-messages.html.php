<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/edition/home/views/table-home-messages/", 
    "table-home-messages", 
    "TableHomeMessagesCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_editeur()) {
    $homeMessages = $ctrl->getHomeMessages();
    ?>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Texte</th>
                <th class="text-center">Critic.</th>
                <th colspan="2">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (sizeof($homeMessages) > 0) {
                foreach ($homeMessages as $homeMessage) {
                    ?>
                    <tr>
                        <td><?php echo $homeMessage[HOME_MESSAGE_TEXTE]; ?></td>
                        <td class="text-center"><?php echo $ctrl->getLibelleCriticite($homeMessage[HOME_MESSAGE_CRITICITE_ID]); ?></td>
                        <td class="text-center">
                            <span class="glyphicon glyphicon-pencil clickable" title="Editer" id-home-message="<?php echo $homeMessage[HOME_MESSAGE_ID]; ?>"></span>
                        </td>
                        <td class="text-center">
                            <span class="glyphicon glyphicon-trash clickable" title="Supprimer" id-home-message="<?php echo $homeMessage[HOME_MESSAGE_ID]; ?>"></span>
                            <div class="loader loader-delete" id-home-message="<?php echo $homeMessage[HOME_MESSAGE_ID]; ?>"></div>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>