AdminEditionNewsController = {};

AdminEditionNewsController.load_StatsNews = function() {
    if (Utils.exists('#container-stats-news')) {
        AjaxUtils.loadView(
            'admin/edition/news/views/stats/stats-news',
            '#container-stats-news',
            null,
            function() {},
            '#loader-page'
        );
    }
};

AdminEditionNewsController.load_RechercheAndTableNews = function() {
    if (Utils.exists('#container-recherche-news')) {
        AjaxUtils.loadView(
            'admin/edition/news/views/recherche/recherche-news',
            '#container-recherche-news',
            null,
            function() {
                FormRechercheUtils.control_toggleFormRecherche();
                onClick_btRechercher();
                onClick_btReset();
                AdminEditionNewsController.load_TableNews();
            },
            '#loader-page'
        );
    }

    function onClick_btRechercher() {
        $('#recherche-news__bt-rechercher').unbind('click');
        $('#recherche-news__bt-rechercher').click(function() {
            AdminEditionNewsController.load_TableNews();
        });
    }

    function onClick_btReset() {
        $('#recherche-news__bt-reset').unbind('click');
        $('#recherche-news__bt-reset').click(function() {
            $('#form-recherche-news__input-date-debut').val('');
            $('#form-recherche-news__input-date-fin').val('');
            $('#form-recherche-news__input-titre').val('');

            AdminEditionNewsController.load_TableNews();
        });
    }
};

AdminEditionNewsController.load_TableNews = function() {
    if (Utils.exists('#container-table-news')) {
        var params = {
            dateDebut: $.trim($('#form-recherche-news__input-date-debut').val()),
            dateFin: $.trim($('#form-recherche-news__input-date-fin').val()),
            titre: $.trim($('#form-recherche-news__input-titre').val())
        };
        AjaxUtils.loadView(
            'admin/edition/news/views/table/table-news',
            '#container-table-news',
            params,
            function() {
                control_AddActu();
                control_EditActu();
                control_DeleteActu();
                control_ValidationActu();
            },
            '#loader-page'
        );
    }

    function control_AddActu() {
        $('#news__bt-ajouter').unbind('click');
        $('#news__bt-ajouter').click(function() {
            open_ModalSaveActu(null);
        });
    }

    function control_EditActu() {
        $('.glyphicon-pencil').unbind('click');
        $('.glyphicon-pencil').click(function() {
            var idActualite = $(this).attr('id-actualite');
            open_ModalSaveActu(idActualite);
        });
    }

    function open_ModalSaveActu(idActualite) {
        var params = null;
        if (idActualite != null) {
            params = {
                idActualite: idActualite
            };
        }
        AjaxUtils.loadView(
            'admin/edition/news/modals/save/modal-save-actualite-content',
            '#modal-save-actualite-content',
            params,
            function() {
                onClick_btSauvegarder();
            },
            '#loader-page'
        );
        $('#modal-save-actualite').modal('show');
    }

    function onClick_btSauvegarder() {
        $('#modal-save-actualite__bt-sauvegarder').unbind('click');
        $('#modal-save-actualite__bt-sauvegarder').click(function() {
            FormUtils.hideFormErrors('#modal-save-actualite #form-save-actualite');
            ViewUtils.unsetHTMLAndHide('#modal-save-actualite .form-result-message');
            ViewUtils.desactiveButtonAndShowLoader('#modal-save-actualite__bt-sauvegarder', '#modal-save-actualite__bt-sauvegarder .loader');

            var formValidation = FormUtils.validateForm('#modal-save-actualite #form-save-actualite');
            
            if (formValidation) {
                document.getElementById('form-save-actualite').submit();
            } else {
                ViewUtils.activeButtonAndHideLoader('#modal-save-actualite__bt-sauvegarder', '#modal-save-actualite__bt-sauvegarder .loader');
            }
        });
    }

    function control_DeleteActu() {
        $('.glyphicon-trash').unbind('click');
        $('.glyphicon-trash').click(function() {
            var idActualite = $(this).attr('id-actualite');
            
            ViewUtils.prompt(
                'Supprimer cette actualit&eacute; ?', 
                {
                    btOkText: 'Oui',
                    btKoText: 'Non'
                },
                function() {
                    deleteActu(idActualite);
                }, 
                null, 
                null
            );
        });

        function deleteActu(idActualite) {
            $('.loader-delete[id-actualite='+idActualite+']').show();
            AjaxUtils.callService(
                'actualite', 
                'delete', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idActualite
                }, 
                function(response) {
                    $('.loader-delete[id-actualite='+idActualite+']').hide();
                    if (response.success) {
                        AdminEditionNewsController.load_StatsNews();
                        AdminEditionNewsController.load_TableNews();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la suppression de l\'actualit&eacute;', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-delete[id-actualite='+idActualite+']').hide();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        }
    }

    function control_ValidationActu() {
        $('.icon-validation').unbind('click');
        $('.icon-validation').click(function() {
            var idActualite = $(this).attr('id-actualite');
            
            $('.loader-validation[id-actualite='+idActualite+']').show();
            AjaxUtils.callService(
                'actualite', 
                'toggleValidation', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idActualite
                }, 
                function(response) {
                    $('.loader-validation[id-actualite='+idActualite+']').hide();
                    if (response.success) {
                        AdminEditionNewsController.load_TableNews();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la validation / invalidation de l\'actualit&eacute;', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-validation[id-actualite='+idActualite+']').hide();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        });
    }
};

initializePage = function() {
    AdminController.initializePage('edition', 'news');

    AdminEditionNewsController.load_StatsNews();
    AdminEditionNewsController.load_RechercheAndTableNews();
};