<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/actualite-dao.php");

class StatsNewsCtrl extends AbstractViewCtrl {

	private $actualiteDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_editeur()) {
			$this->actualiteDAO = new ActualiteDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getNbNews() {
		return $this->actualiteDAO->getNbTotalVisibles()[ACTUALITE_NB_NEWS];
	}
}

?>