<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/edition/news/views/recherche/", 
    "recherche-news", 
    "RechercheNewsCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_editeur()) {
    ?>
    <div class="container-recherche__title" title="Ouvrir le formulaire de recherche" for="form-recherche-news">
        <div class="container-recherche__title__icon">
            <span class="glyphicon glyphicon-search"></span>
        </div>
        <div class="container-recherche__title__text">Recherche</div>
        <div class="container-recherche__title__arrow">
            <span class="glyphicon glyphicon-triangle-bottom"></span>
        </div>
    </div>
    <div class="form-horizontal" id="form-recherche-news">
        <div class="form-group">
            <label for="form-recherche-news__input-date-debut" class="col-xs-1 control-label">Date :</label>
            <div class="col-xs-2 form-input" for="form-recherche-news__input-date-debut">
                <div class="input-group">
                    <input type="text" class="form-control" id="form-recherche-news__input-date-debut" placeholder="D&eacute;but">
                    <span class="input-group-addon">
                        <img src="<?php echo SRC_PATH; ?>assets/img/icons/calendar.png">
                    </span>
                </div>
            </div>
            <label for="form-recherche-news__input-titre" class="col-xs-2 control-label">Titre :</label>
            <div class="col-xs-4 form-input" for="form-recherche-news__input-titre">
                <input type="text" class="form-control" id="form-recherche-news__input-titre">
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-2 col-xs-offset-1 form-input" for="form-recherche-news__input-date-fin">
                <div class="input-group">
                    <input type="text" class="form-control" id="form-recherche-news__input-date-fin" placeholder="Fin">
                    <span class="input-group-addon">
                        <img src="<?php echo SRC_PATH; ?>assets/img/icons/calendar.png">
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="container-recherche__footer" for="form-recherche-news">
        <button id="recherche-news__bt-rechercher" class="btn btn-default">
            <div class="button__icon">
                <span class="glyphicon glyphicon-search"></span>
            </div>
            <div class="button__text">Chercher</div>
        </button>
        <button id="recherche-news__bt-reset" class="btn btn-default">
            <div class="button__icon">
                <span class="glyphicon glyphicon-erase"></span>
            </div>
            <div class="button__text">Reset</div>
        </button>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>