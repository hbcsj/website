<?php
require_once("core/php/controllers/abstract-view-controller.php");

class RechercheNewsCtrl extends AbstractViewCtrl {

    public function __construct($viewName) {
		parent::__construct($viewName);

		if (!isAdminConnected_editeur()) {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}
}

?>