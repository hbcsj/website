<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/actualite-dao.php");
require_once("common/php/lib/date-utils.php");

class TableNewsCtrl extends AbstractViewCtrl {

	private $actualiteDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_editeur()) {
			$this->actualiteDAO = new ActualiteDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getNews() {
		return $this->actualiteDAO->getAllByParams(
            DateUtils::convert_slashDate_to_sqlDate($_GET["dateDebut"]),
			DateUtils::convert_slashDate_to_sqlDate($_GET["dateFin"]),
			trim(strtolower($_GET["titre"])),
            ACTUALITE_TABLE_NAME.".".ACTUALITE_DATE_HEURE." DESC"
        );
	}

	public function isActualiteVisible($actualite) {
		return (
			$actualite[ACTUALITE_VISIBLE_SUR_SITE] && 
			$actualite[ACTUALITE_EMAIL_DERNIER_EDITEUR] == $GLOBALS[CONF][SUPERADMIN_EMAIL]
		);
	}
}

?>