<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/edition/news/views/table/", 
    "table-news", 
    "TableNewsCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_editeur()) {
    $news = $ctrl->getNews();
    ?>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Date</th>
                <th>Titre</th>
                <th class="text-center">Visible ?</th>
                <?php
                if (isAdminConnected_superadmin()) {
                    ?>
                    <th colspan="3">&nbsp;</th>
                    <?php
                } else if (isAdminConnected_bureau()) {
                    ?>
                    <th colspan="2">&nbsp;</th>
                    <?php
                } else {
                    ?>
                    <th>&nbsp;</th>
                    <?php
                }
                ?>
            </tr>
        </thead>
        <tbody>
            <?php
            if (sizeof($news) > 0) {
                foreach ($news as $actu) {
                    $bg = "";
                    if ($actu[ACTUALITE_EMAIL_DERNIER_EDITEUR] != $GLOBALS[CONF][SUPERADMIN_EMAIL]) {
                        $bg = "bg-error";
                    } else if (!$actu[ACTUALITE_VISIBLE_SUR_SITE]) {
                        $bg = "bg-warning";
                    }
                    ?>
                    <tr>
                        <td class="<?php echo $bg; ?>"><?php echo str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDateTime_to_slashDateTime($actu[ACTUALITE_DATE_HEURE])); ?></td>
                        <td class="<?php echo $bg; ?>"><?php echo $actu[ACTUALITE_TITRE]; ?></td>
                        <td class="text-center <?php echo $bg; ?>"><?php echo ($actu[ACTUALITE_VISIBLE_SUR_SITE] == 1) ? "Oui" : "Non"; ?></td>
                        <td class="text-center <?php echo $bg; ?>">
                            <span class="glyphicon glyphicon-pencil clickable" title="Editer" id-actualite="<?php echo $actu[ACTUALITE_ID]; ?>"></span>
                        </td>
                        <?php
                        if (isAdminConnected_bureau()) {
                            ?>
                            <td class="text-center <?php echo $bg; ?>">
                                <span class="glyphicon glyphicon-trash clickable" title="Supprimer" id-actualite="<?php echo $actu[ACTUALITE_ID]; ?>"></span>
                                <div class="loader loader-delete" id-actualite="<?php echo $actu[ACTUALITE_ID]; ?>"></div>
                            </td>
                            <?php
                        }
                        if (isAdminConnected_superadmin()) {
                            if (!$ctrl->isActualiteVisible($actu)) {
                                ?>
                                <td class="text-center <?php echo $bg; ?>">
                                    <span class="glyphicon glyphicon-ok icon-validation clickable" title="Valider" id-actualite="<?php echo $actu[ACTUALITE_ID]; ?>"></span>
                                    <div class="loader loader-validation" id-actualite="<?php echo $actu[ACTUALITE_ID]; ?>"></div>
                                </td>
                                <?php
                            } else {
                                ?>
                                <td class="text-center <?php echo $bg; ?>">
                                    <span class="glyphicon glyphicon-remove icon-validation clickable" title="Invalider" id-actualite="<?php echo $actu[ACTUALITE_ID]; ?>"></span>
                                    <div class="loader loader-validation" id-actualite="<?php echo $actu[ACTUALITE_ID]; ?>"></div>
                                </td>
                                <?php
                            }
                        }
                        ?>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>