<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/actualite-dao.php");

class ModalSaveActualiteCtrl extends AbstractViewCtrl {
	
	private $actualite;

	private $actualiteDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idActualite" => $_GET["idActualite"]
		), true);
		
		if (isAdminConnected_editeur()) {
			$this->actualiteDAO = new ActualiteDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdActualite();
	}

	private function checkIdActualite() {
		if (HTTPUtils::paramExists(GET, "idActualite")) {
			$this->actualite = $this->actualiteDAO->getById($_GET["idActualite"]);
				
			if ($this->actualite == null) {
				$this->sendCheckError(
					HTTP_404, 
					"L'actualite (idActualite = '".$_GET["idActualite"]."') n'existe pas", 
					"webapp/views/common/error/404/404.html.php"
				);
			}
		}
	}

	public function getActualite() {
		return $this->actualite;
	}

	public function getTitle() {
		$title = "Nouvelle actualit&eacute;";
		if ($this->actualite != null) {
			$title = $this->actualite[ACTUALITE_TITRE]." - <span class=\"normal\">Edition</span>";
		}
		return $title;
	}

	public function getTitreActualite() {
		if ($this->actualite != null) {
			return $this->actualite[ACTUALITE_TITRE];
		}
		return "";
	}

	public function getParamIdActualite() {
		if ($this->actualite != null) {
			return "?id=".$this->actualite[ACTUALITE_ID];
		}
		return "";
	}

	public function getVisibleChecked() {
		$checked = "";
		if (
			($this->actualite == null && isAdminConnected_bureau()) || 
			($this->actualite != null && isAdminConnected_bureau() && $this->actualite[ACTUALITE_VISIBLE_SUR_SITE])
		) {
			$checked = " checked=\"checked\"";
		}
        return $checked;
	}

	public function getEmailDernierEditeurActualite() {
		if (isAdminConnected_superadmin()) {
			if ($this->actualite != null) {
				return $this->actualite[ACTUALITE_EMAIL_DERNIER_EDITEUR];
			} else {
				return $GLOBALS[CONF][SUPERADMIN_EMAIL];
			}
		} 
		return "";
	}
}

?>