<?php
require_once("core/php/resources/page.php");
require_once("common/php/lib/admin-utils.php");

$page = new Page(
    "webapp/views/admin/edition/news/", 
    "admin-edition-news", 
    "AdminEditionNewsCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/admin/common/nav/nav.html.php");
if (isAdminConnected_editeur()) {
    require_once("webapp/views/admin/edition/sub-menu/sub-menu-admin-edition.html.php");
}
?>

<div class="container-fluid animated fadeIn">
    <?php
    if (isAdminConnected_editeur()) {
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h1>
                    News
                    <button id="news__bt-ajouter" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-plus"></span>
                        </div>
                        <div class="button__text">Ajouter une actualit&eacute;</div>
                    </button>
                </h1>
            </div>
        </div>
        <div id="container-stats-news" class="container-stats"></div>
        <div id="container-recherche-news" class="container-recherche"></div>
        <div id="container-table-news" class="container-table"></div>
        <iframe name="uploader-actualite" class="iframe-uploader" src="#"></iframe>
        <?php
    } else {
        require_once("webapp/views/common/mdp-form/mdp-form.html.php");
    }
    ?>
</div>

<?php
require_once("webapp/views/common/modals/alert/modal-alert.html.php");
require_once("webapp/views/common/modals/prompt/modal-prompt.html.php");

if (isAdminConnected_editeur()) {
    require_once("webapp/views/admin/edition/news/modals/save/modal-save-actualite.html.php");
}

$page->finalizePage();
?>