<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/type-hhsj-dao.php");

class RechercheHHSJCtrl extends AbstractViewCtrl {

	private $typeHHSJDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_editeur()) {
            $this->typeHHSJDAO = new TypeHHSJDAO($this->getDatabaseConnection());
        } else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getTypesHHSJ() {
		return $this->typeHHSJDAO->getAll(TYPE_HHSJ_TABLE_NAME.".".TYPE_HHSJ_LIBELLE);
	}
}

?>