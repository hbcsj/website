<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/edition/hhsj/views/recherche/", 
    "recherche-hhsj", 
    "RechercheHHSJCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_editeur()) {
    $types = $ctrl->getTypesHHSJ();
    ?>
    <div class="container-recherche__title" title="Ouvrir le formulaire de recherche" for="form-recherche-hhsj">
        <div class="container-recherche__title__icon">
            <span class="glyphicon glyphicon-search"></span>
        </div>
        <div class="container-recherche__title__text">Recherche</div>
        <div class="container-recherche__title__arrow">
            <span class="glyphicon glyphicon-triangle-bottom"></span>
        </div>
    </div>
    <div class="form-horizontal" id="form-recherche-hhsj">
        <div class="form-group">
            <label for="form-recherche-hhsj__input-date-debut" class="col-xs-1 control-label">Date :</label>
            <div class="col-xs-2 form-input" for="form-recherche-hhsj__input-date-debut">
                <div class="input-group">
                    <input type="text" class="form-control" id="form-recherche-hhsj__input-date-debut" placeholder="D&eacute;but">
                    <span class="input-group-addon">
                        <img src="<?php echo SRC_PATH; ?>assets/img/icons/calendar.png">
                    </span>
                </div>
            </div>
            <label for="form-recherche-hhsj__input-titre" class="col-xs-2 control-label">Titre :</label>
            <div class="col-xs-4 form-input" for="form-recherche-hhsj__input-titre">
                <input type="text" class="form-control" id="form-recherche-hhsj__input-titre">
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-2 col-xs-offset-1 form-input" for="form-recherche-hhsj__input-date-fin">
                <div class="input-group">
                    <input type="text" class="form-control" id="form-recherche-hhsj__input-date-fin" placeholder="Fin">
                    <span class="input-group-addon">
                        <img src="<?php echo SRC_PATH; ?>assets/img/icons/calendar.png">
                    </span>
                </div>
            </div>
            <label for="form-recherche-hhsj__input-type" class="col-xs-2 control-label">Type :</label>
            <div class="col-xs-2 form-input" for="form-recherche-hhsj__input-type">
                <select class="form-control" id="form-recherche-hhsj__input-type">
                    <option value=""></option>
                    <?php
                    if (sizeof($types) > 0) {
                        foreach ($types as $type) {
                            ?>
                            <option value="<?php echo $type[TYPE_HHSJ_ID]; ?>"><?php echo $type[TYPE_HHSJ_LIBELLE]; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
    <div class="container-recherche__footer" for="form-recherche-hhsj">
        <button id="recherche-hhsj__bt-rechercher" class="btn btn-default">
            <div class="button__icon">
                <span class="glyphicon glyphicon-search"></span>
            </div>
            <div class="button__text">Chercher</div>
        </button>
        <button id="recherche-hhsj__bt-reset" class="btn btn-default">
            <div class="button__icon">
                <span class="glyphicon glyphicon-erase"></span>
            </div>
            <div class="button__text">Reset</div>
        </button>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>