<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/hhsj-dao.php");

class StatsHHSJCtrl extends AbstractViewCtrl {

	private $hhsjDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_editeur()) {
			$this->hhsjDAO = new HHSJDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getNbHHSJ() {
		return $this->hhsjDAO->getNbTotalVisibles()[HHSJ_NB_HHSJ];
	}
}

?>