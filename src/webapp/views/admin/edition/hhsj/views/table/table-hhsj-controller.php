<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/hhsj-dao.php");
require_once("common/php/dao/type-hhsj-dao.php");
require_once("common/php/lib/date-utils.php");

class TableHHSJCtrl extends AbstractViewCtrl {

	private $hhsjDAO;
	private $typeHHSJDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_editeur()) {
			$this->hhsjDAO = new HHSJDAO($this->getDatabaseConnection());
			$this->typeHHSJDAO = new TypeHHSJDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getHHSJs() {
		return $this->hhsjDAO->getAllByParams(
            DateUtils::convert_slashDate_to_sqlDate($_GET["dateDebut"]),
			DateUtils::convert_slashDate_to_sqlDate($_GET["dateFin"]),
			trim(strtolower($_GET["titre"])),
			$_GET["typeHHSJId"],
            HHSJ_TABLE_NAME.".".HHSJ_DATE." DESC, ".HHSJ_TABLE_NAME.".".HHSJ_ID." DESC"
        );
	}

	public function getNomTypeHHSJ($typeHHSJId) {
		$type = $this->typeHHSJDAO->getById($typeHHSJId);
		return $type[TYPE_HHSJ_LIBELLE];
	}

	public function isHHSJVisible($actualite) {
		return (
			$actualite[HHSJ_VISIBLE_SUR_SITE] && 
			$actualite[HHSJ_EMAIL_DERNIER_EDITEUR] == $GLOBALS[CONF][SUPERADMIN_EMAIL]
		);
	}
}

?>