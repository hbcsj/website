<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/path-utils.php");

$view = new View(
    "webapp/views/admin/edition/hhsj/views/table/", 
    "table-hhsj", 
    "TableHHSJCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_editeur()) {
    $hhsjs = $ctrl->getHHSJs();
    ?>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Date</th>
                <th>Titre</th>
                <th>Type</th>
                <th class="text-center">Hand-Hebdo</th>
                <th class="text-center">Visible ?</th>
                <?php
                if (isAdminConnected_superadmin()) {
                    ?>
                    <th colspan="3">&nbsp;</th>
                    <?php
                } else if (isAdminConnected_bureau()) {
                    ?>
                    <th colspan="2">&nbsp;</th>
                    <?php
                } else {
                    ?>
                    <th>&nbsp;</th>
                    <?php
                }
                ?>
            </tr>
        </thead>
        <tbody>
            <?php
            if (sizeof($hhsjs) > 0) {
                foreach ($hhsjs as $hhsj) {
                    $bg = "";
                    if ($hhsj[HHSJ_EMAIL_DERNIER_EDITEUR] != $GLOBALS[CONF][SUPERADMIN_EMAIL]) {
                        $bg = "bg-error";
                    } else if (!$hhsj[HHSJ_VISIBLE_SUR_SITE]) {
                        $bg = "bg-warning";
                    }
                    ?>
                    <tr>
                        <td class="<?php echo $bg; ?>"><?php echo DateUtils::convert_sqlDate_to_slashDate($hhsj[HHSJ_DATE]); ?></td>
                        <td class="<?php echo $bg; ?>"><?php echo $hhsj[HHSJ_TITRE]; ?></td>
                        <td class="<?php echo $bg; ?>"><?php echo $ctrl->getNomTypeHHSJ($hhsj[HHSJ_TYPE_HHSJ_ID]); ?></td>
                        <td class="text-center <?php echo $bg; ?>">
                            <?php
                            $hhsjLink = PathUtils::getHHSJFile($hhsj[HHSJ_ID]);
                            if ($hhsj[HHSJ_TYPE_HHSJ_ID] == HHSJ_UNE_DE_MAGAZINE_TYPE_ID) {
                                $hhsjLink = PathUtils::getHHSJImgFile($hhsj[HHSJ_ID]);
                            }
                            ?>
                            <a href="<?php echo SRC_PATH.$hhsjLink ?>" target="_blank">Voir</a>
                        </td>
                        <td class="text-center <?php echo $bg; ?>"><?php echo ($hhsj[HHSJ_VISIBLE_SUR_SITE] == 1) ? "Oui" : "Non"; ?></td>
                        <td class="text-center <?php echo $bg; ?>">
                            <span class="glyphicon glyphicon-pencil clickable" title="Editer" id-hhsj="<?php echo $hhsj[HHSJ_ID]; ?>"></span>
                        </td>
                        <?php
                        if (isAdminConnected_bureau()) {
                            ?>
                            <td class="text-center <?php echo $bg; ?>">
                                <span class="glyphicon glyphicon-trash clickable" title="Supprimer" id-hhsj="<?php echo $hhsj[HHSJ_ID]; ?>"></span>
                                <div class="loader loader-delete" id-hhsj="<?php echo $hhsj[HHSJ_ID]; ?>"></div>
                            </td>
                            <?php
                        }
                        if (isAdminConnected_superadmin()) {
                            if (!$ctrl->isHHSJVisible($hhsj)) {
                                ?>
                                <td class="text-center <?php echo $bg; ?>">
                                    <span class="glyphicon glyphicon-ok icon-validation clickable" title="Valider" id-hhsj="<?php echo $hhsj[HHSJ_ID]; ?>"></span>
                                    <div class="loader loader-validation" id-hhsj="<?php echo $hhsj[HHSJ_ID]; ?>"></div>
                                </td>
                                <?php
                            } else {
                                ?>
                                <td class="text-center <?php echo $bg; ?>">
                                    <span class="glyphicon glyphicon-remove icon-validation clickable" title="Invalider" id-hhsj="<?php echo $hhsj[HHSJ_ID]; ?>"></span>
                                    <div class="loader loader-validation" id-hhsj="<?php echo $hhsj[HHSJ_ID]; ?>"></div>
                                </td>
                                <?php
                            }
                        }
                        ?>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>