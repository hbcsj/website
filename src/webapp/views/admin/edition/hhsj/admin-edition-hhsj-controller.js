AdminEditionHHSJController = {};

AdminEditionHHSJController.idTypeHHSJUne = 1;

AdminEditionHHSJController.load_StatsHHSJ = function() {
    if (Utils.exists('#container-stats-hhsj')) {
        AjaxUtils.loadView(
            'admin/edition/hhsj/views/stats/stats-hhsj',
            '#container-stats-hhsj',
            null,
            function() {},
            '#loader-page'
        );
    }
};

AdminEditionHHSJController.load_RechercheAndTableHHSJ = function() {
    if (Utils.exists('#container-recherche-hhsj')) {
        AjaxUtils.loadView(
            'admin/edition/hhsj/views/recherche/recherche-hhsj',
            '#container-recherche-hhsj',
            null,
            function() {
                FormRechercheUtils.control_toggleFormRecherche();
                onClick_btRechercher();
                onClick_btReset();
                AdminEditionHHSJController.load_TableHHSJ();
            },
            '#loader-page'
        );
    }

    function onClick_btRechercher() {
        $('#recherche-hhsj__bt-rechercher').unbind('click');
        $('#recherche-hhsj__bt-rechercher').click(function() {
            AdminEditionHHSJController.load_TableHHSJ();
        });
    }

    function onClick_btReset() {
        $('#recherche-hhsj__bt-reset').unbind('click');
        $('#recherche-hhsj__bt-reset').click(function() {
            $('#form-recherche-hhsj__input-date-debut').val('');
            $('#form-recherche-hhsj__input-date-fin').val('');
            $('#form-recherche-hhsj__input-titre').val('');
            $('#form-recherche-hhsj__input-type').val('');

            AdminEditionHHSJController.load_TableHHSJ();
        });
    }
};

AdminEditionHHSJController.load_TableHHSJ = function() {
    if (Utils.exists('#container-table-hhsj')) {
        var params = {
            dateDebut: $.trim($('#form-recherche-hhsj__input-date-debut').val()),
            dateFin: $.trim($('#form-recherche-hhsj__input-date-fin').val()),
            titre: $.trim($('#form-recherche-hhsj__input-titre').val()),
            typeHHSJId: $('#form-recherche-hhsj__input-type').val()
        };
        AjaxUtils.loadView(
            'admin/edition/hhsj/views/table/table-hhsj',
            '#container-table-hhsj',
            params,
            function() {
                control_AddHHSJ();
                control_EditHHSJ();
                control_DeleteHHSJ();
                control_ValidationHHSJ();
            },
            '#loader-page'
        );
    }

    function control_AddHHSJ() {
        $('#hhsj__bt-ajouter').unbind('click');
        $('#hhsj__bt-ajouter').click(function() {
            open_ModalSaveHHSJ(null);
        });
    }

    function control_EditHHSJ() {
        $('.glyphicon-pencil').unbind('click');
        $('.glyphicon-pencil').click(function() {
            var idHHSJ = $(this).attr('id-hhsj');
            open_ModalSaveHHSJ(idHHSJ);
        });
    }

    function open_ModalSaveHHSJ(idHHSJ) {
        var params = null;
        if (idHHSJ != null) {
            params = {
                idHHSJ: idHHSJ
            };
        }
        AjaxUtils.loadView(
            'admin/edition/hhsj/modals/save/modal-save-hhsj-content',
            '#modal-save-hhsj-content',
            params,
            function() {
                control_TypeHHSJ(idHHSJ);
                onClick_btSauvegarder();
            },
            '#loader-page'
        );
        $('#modal-save-hhsj').modal('show');

        function control_TypeHHSJ(idHHSJ) {
            if (idHHSJ != null) {
                selectInputsByTypeHHSJ($('#form-save-hhsj__input-type').val());
            } else {
                selectInputsByTypeHHSJ(AdminEditionHHSJController.idTypeHHSJUne)
            }
            $('#form-save-hhsj__input-type').unbind('click');
            $('#form-save-hhsj__input-type').change(function() {
                FormUtils.hideFormErrors('#modal-save-hhsj #form-save-hhsj');
                var typeHHSJ = $(this).val();
                selectInputsByTypeHHSJ(typeHHSJ)
            });

            function selectInputsByTypeHHSJ(typeHHSJ) {
                if (typeHHSJ == AdminEditionHHSJController.idTypeHHSJUne) {
                    $('.form-group-fichier').hide();

                    $('label[for=\"form-save-hhsj__input-fichier\"]').removeClass('required');
                    $('#form-save-hhsj__input-fichier').prop('required', false);
                } else {
                    $('.form-group-fichier').show();

                    $('label[for=\"form-save-hhsj__input-fichier\"]').addClass('required');
                    $('#form-save-hhsj__input-fichier').prop('required', true);
                }
            }
        }
    }

    function onClick_btSauvegarder() {
        $('#modal-save-hhsj__bt-sauvegarder').unbind('click');
        $('#modal-save-hhsj__bt-sauvegarder').click(function() {
            FormUtils.hideFormErrors('#modal-save-hhsj #form-save-hhsj');
            ViewUtils.unsetHTMLAndHide('#modal-save-hhsj .form-result-message');
            ViewUtils.desactiveButtonAndShowLoader('#modal-save-hhsj__bt-sauvegarder', '#modal-save-hhsj__bt-sauvegarder .loader');

            var formValidation = FormUtils.validateForm('#modal-save-hhsj #form-save-hhsj');
            
            if (formValidation) {
                document.getElementById('form-save-hhsj').submit();
            } else {
                ViewUtils.activeButtonAndHideLoader('#modal-save-hhsj__bt-sauvegarder', '#modal-save-hhsj__bt-sauvegarder .loader');
            }
        });
    }

    function control_DeleteHHSJ() {
        $('.glyphicon-trash').unbind('click');
        $('.glyphicon-trash').click(function() {
            var idHHSJ = $(this).attr('id-hhsj');
            
            ViewUtils.prompt(
                'Supprimer ce Hand-Hebdo ?', 
                {
                    btOkText: 'Oui',
                    btKoText: 'Non'
                },
                function() {
                    deleteHHSJ(idHHSJ);
                }, 
                null, 
                null
            );
        });

        function deleteHHSJ(idHHSJ) {
            $('.loader-delete[id-hhsj='+idHHSJ+']').show();
            AjaxUtils.callService(
                'hhsj', 
                'delete', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idHHSJ
                }, 
                function(response) {
                    $('.loader-delete[id-hhsj='+idHHSJ+']').hide();
                    if (response.success) {
                        AdminEditionHHSJController.load_StatsHHSJ();
                        AdminEditionHHSJController.load_TableHHSJ();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la suppression de l\'Hand-Hebdo', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-delete[id-hhsj='+idHHSJ+']').hide();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        }
    }

    function control_ValidationHHSJ() {
        $('.icon-validation').unbind('click');
        $('.icon-validation').click(function() {
            var idHHSJ = $(this).attr('id-hhsj');
            
            $('.loader-validation[id-hhsj='+idHHSJ+']').show();
            AjaxUtils.callService(
                'hhsj', 
                'toggleValidation', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idHHSJ
                }, 
                function(response) {
                    $('.loader-validation[id-hhsj='+idHHSJ+']').hide();
                    if (response.success) {
                        AdminEditionHHSJController.load_TableHHSJ();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la validation / invalidation de l\'Hand-hebdo', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-validation[id-hhsj='+idHHSJ+']').hide();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        });
    }
};

initializePage = function() {
    AdminController.initializePage('edition', 'hhsj');

    AdminEditionHHSJController.load_StatsHHSJ();
    AdminEditionHHSJController.load_RechercheAndTableHHSJ();
};