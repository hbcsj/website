<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/hhsj-dao.php");
require_once("common/php/dao/type-hhsj-dao.php");

class ModalSaveHHSJCtrl extends AbstractViewCtrl {
	
	private $hhsj;

	private $hhsjDAO;
	private $typeHHSJDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idHHSJ" => $_GET["idHHSJ"]
		), true);
		
		if (isAdminConnected_editeur()) {
			$this->hhsjDAO = new HHSJDAO($this->getDatabaseConnection());
			$this->typeHHSJDAO = new TypeHHSJDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdHHSJ();
	}

	private function checkIdHHSJ() {
		if (HTTPUtils::paramExists(GET, "idHHSJ")) {
			$this->hhsj = $this->hhsjDAO->getById($_GET["idHHSJ"]);
				
			if ($this->hhsj == null) {
				$this->sendCheckError(
					HTTP_404, 
					"Le Hand-Hebdo (idHHSJ = '".$_GET["idHHSJ"]."') n'existe pas", 
					"webapp/views/common/error/404/404.html.php"
				);
			}
		}
	}

	public function getHHSJ() {
		return $this->hhsj;
	}

	public function getTitle() {
		$title = "Nouvel Hand-Hebdo";
		if ($this->hhsj != null) {
			$title = $this->hhsj[HHSJ_TITRE]." - <span class=\"normal\">Edition</span>";
		}
		return $title;
	}

	public function getTitreHHSJ() {
		if ($this->hhsj != null) {
			return $this->hhsj[HHSJ_TITRE];
		}
		return "";
	}

	public function getParamIdHHSJ() {
		if ($this->hhsj != null) {
			return "?id=".$this->hhsj[HHSJ_ID];
		}
		return "";
	}

	public function getOptionsTypes() {
		$html = "";

		$types = $this->typeHHSJDAO->getAll();
		if (sizeof($types) > 0) {
			foreach ($types as $type) {
				$selected = "";
				if ($this->hhsj[HHSJ_TYPE_HHSJ_ID] == $type[TYPE_HHSJ_ID]) {
					$selected = " selected";
				}
				$html .= "<option value=\"".$type[TYPE_HHSJ_ID]."\"".$selected.">".$type[TYPE_HHSJ_LIBELLE]."</option>";
			}
		}

		return $html;
	}

	public function getVisibleChecked() {
		$checked = "";
		if (
			($this->hhsj == null && isAdminConnected_bureau()) || 
			($this->hhsj != null && isAdminConnected_bureau() && $this->hhsj[HHSJ_VISIBLE_SUR_SITE])
		) {
			$checked = " checked=\"checked\"";
		}
        return $checked;
	}

	public function getEmailDernierEditeurHHSJ() {
		if (isAdminConnected_superadmin()) {
			if ($this->hhsj != null) {
				return $this->hhsj[HHSJ_EMAIL_DERNIER_EDITEUR];
			} else {
				return $GLOBALS[CONF][SUPERADMIN_EMAIL];
			}
		} 
		return "";
	}
}

?>