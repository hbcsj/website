<?php
require_once("core/php/resources/page.php");
require_once("common/php/lib/admin-utils.php");

$page = new Page(
    "webapp/views/admin/edition/photos-videos/", 
    "admin-edition-photos-videos", 
    "AdminEditionPhotosVideosCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/admin/common/nav/nav.html.php");
if (isAdminConnected_editeur()) {
    require_once("webapp/views/admin/edition/sub-menu/sub-menu-admin-edition.html.php");
}
?>

<div class="container-fluid animated fadeIn">
    <?php
    if (isAdminConnected_editeur()) {
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h1>Photos / Vid&eacute;os</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h2>
                    Albums photos
                    <button id="album-photo__bt-ajouter" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-plus"></span>
                        </div>
                        <div class="button__text">Ajouter un album photo</div>
                    </button>
                </h2>
            </div>
        </div>
        <div id="container-table-albums-photos" class="container-table"></div>
        <iframe name="uploader-album-photo" class="iframe-uploader" src="#"></iframe>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h2>
                   Vid&eacute;os
                    <button id="video__bt-ajouter" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-plus"></span>
                        </div>
                        <div class="button__text">Ajouter une vid&eacute;o</div>
                    </button>
                </h2>
            </div>
        </div>
        <div id="container-table-videos" class="container-table"></div>
        <iframe name="uploader-video" class="iframe-uploader" src="#"></iframe>
        <?php
    } else {
        require_once("webapp/views/common/mdp-form/mdp-form.html.php");
    }
    ?>
</div>

<?php
require_once("webapp/views/common/modals/alert/modal-alert.html.php");
require_once("webapp/views/common/modals/prompt/modal-prompt.html.php");

if (isAdminConnected_editeur()) {
    require_once("webapp/views/admin/edition/photos-videos/modals/save-album-photo/modal-save-album-photo.html.php");
    require_once("webapp/views/admin/edition/photos-videos/modals/save-video/modal-save-video.html.php");
}

$page->finalizePage();
?>