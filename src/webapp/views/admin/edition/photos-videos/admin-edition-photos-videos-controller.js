AdminEditionPhotosVideosController = {};

AdminEditionPhotosVideosController.load_TableAlbumsPhotos = function() {
    if (Utils.exists('#container-table-albums-photos')) {
        AjaxUtils.loadView(
            'admin/edition/photos-videos/views/table-albums-photos/table-albums-photos',
            '#container-table-albums-photos',
            null,
            function() {
                control_AddAlbumPhoto();
                control_EditAlbumPhoto();
                control_DeleteAlbumPhoto();
                control_ValidationAlbumPhoto();
            },
            '#loader-page'
        );
    }

    function control_AddAlbumPhoto() {
        $('#album-photo__bt-ajouter').unbind('click');
        $('#album-photo__bt-ajouter').click(function() {
            open_ModalSaveAlbumPhoto(null);
        });
    }

    function control_EditAlbumPhoto() {
        $('#container-table-albums-photos .glyphicon-pencil').unbind('click');
        $('#container-table-albums-photos .glyphicon-pencil').click(function() {
            var idAlbumPhoto = $(this).attr('id-album-photo');
            open_ModalSaveAlbumPhoto(idAlbumPhoto);
        });
    }

    function open_ModalSaveAlbumPhoto(idAlbumPhoto) {
        var params = null;
        if (idAlbumPhoto != null) {
            params = {
                idAlbumPhoto: idAlbumPhoto
            };
        }
        AjaxUtils.loadView(
            'admin/edition/photos-videos/modals/save-album-photo/modal-save-album-photo-content',
            '#modal-save-album-photo-content',
            params,
            function() {
                onClick_btSauvegarder();
            },
            '#loader-page'
        );
        $('#modal-save-album-photo').modal('show');
    }

    function onClick_btSauvegarder() {
        $('#modal-save-album-photo__bt-sauvegarder').unbind('click');
        $('#modal-save-album-photo__bt-sauvegarder').click(function() {
            FormUtils.hideFormErrors('#modal-save-album-photo #form-save-album-photo');
            ViewUtils.unsetHTMLAndHide('#modal-save-album-photo .form-result-message');
            ViewUtils.desactiveButtonAndShowLoader('#modal-save-album-photo__bt-sauvegarder', '#modal-save-album-photo__bt-sauvegarder .loader');

            var formValidation = FormUtils.validateForm('#modal-save-album-photo #form-save-album-photo');
            
            if (formValidation) {
                document.getElementById('form-save-album-photo').submit();
            } else {
                ViewUtils.activeButtonAndHideLoader('#modal-save-album-photo__bt-sauvegarder', '#modal-save-album-photo__bt-sauvegarder .loader');
            }
        });
    }

    function control_DeleteAlbumPhoto() {
        $('#container-table-albums-photos .glyphicon-trash').unbind('click');
        $('#container-table-albums-photos .glyphicon-trash').click(function() {
            var idAlbumPhoto = $(this).attr('id-album-photo');
            
            ViewUtils.prompt(
                'Supprimer cet album photo ?', 
                {
                    btOkText: 'Oui',
                    btKoText: 'Non'
                },
                function() {
                    deleteAlbumPhoto(idAlbumPhoto);
                }, 
                null, 
                null
            );
        });

        function deleteAlbumPhoto(idAlbumPhoto) {
            $('.loader-delete[id-album-photo='+idAlbumPhoto+']').show();
            AjaxUtils.callService(
                'album-photo', 
                'delete', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idAlbumPhoto
                }, 
                function(response) {
                    $('.loader-delete[id-album-photo='+idAlbumPhoto+']').hide();
                    if (response.success) {
                        AdminEditionPhotosVideosController.load_TableAlbumsPhotos();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la suppression de l\'album photo', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-delete[id-album-photo='+idAlbumPhoto+']').hide();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        }
    }

    function control_ValidationAlbumPhoto() {
        $('#container-table-albums-photos .icon-validation').unbind('click');
        $('#container-table-albums-photos .icon-validation').click(function() {
            var idAlbumPhoto = $(this).attr('id-album-photo');
            
            $('.loader-validation[id-album-photo='+idAlbumPhoto+']').show();
            AjaxUtils.callService(
                'album-photo', 
                'toggleValidation', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idAlbumPhoto
                }, 
                function(response) {
                    $('.loader-validation[id-album-photo='+idAlbumPhoto+']').hide();
                    if (response.success) {
                        AdminEditionPhotosVideosController.load_TableAlbumsPhotos();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la validation / invalidation de l\'album photo', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-validation[id-album-photo='+idAlbumPhoto+']').hide();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        });
    }
};

AdminEditionPhotosVideosController.load_TableVideos = function() {
    if (Utils.exists('#container-table-videos')) {
        AjaxUtils.loadView(
            'admin/edition/photos-videos/views/table-videos/table-videos',
            '#container-table-videos',
            null,
            function() {
                control_AddVideo();
                control_EditVideo();
                control_DeleteVideo();
                control_ValidationVideo();
            },
            '#loader-page'
        );
    }

    function control_AddVideo() {
        $('#video__bt-ajouter').unbind('click');
        $('#video__bt-ajouter').click(function() {
            open_ModalSaveVideo(null);
        });
    }

    function control_EditVideo() {
        $('#container-table-videos .glyphicon-pencil').unbind('click');
        $('#container-table-videos .glyphicon-pencil').click(function() {
            var idVideo = $(this).attr('id-video');
            open_ModalSaveVideo(idVideo);
        });
    }

    function open_ModalSaveVideo(idVideo) {
        var params = null;
        if (idVideo != null) {
            params = {
                idVideo: idVideo
            };
        }
        AjaxUtils.loadView(
            'admin/edition/photos-videos/modals/save-video/modal-save-video-content',
            '#modal-save-video-content',
            params,
            function() {
                onClick_btSauvegarder();
            },
            '#loader-page'
        );
        $('#modal-save-video').modal('show');
    }

    function onClick_btSauvegarder() {
        $('#modal-save-video__bt-sauvegarder').unbind('click');
        $('#modal-save-video__bt-sauvegarder').click(function() {
            FormUtils.hideFormErrors('#modal-save-video #form-save-video');
            ViewUtils.unsetHTMLAndHide('#modal-save-video .form-result-message');
            ViewUtils.desactiveButtonAndShowLoader('#modal-save-video__bt-sauvegarder', '#modal-save-video__bt-sauvegarder .loader');

            var formValidation = FormUtils.validateForm('#modal-save-video #form-save-video');
            
            if (formValidation) {
                document.getElementById('form-save-video').submit();
            } else {
                ViewUtils.activeButtonAndHideLoader('#modal-save-video__bt-sauvegarder', '#modal-save-video__bt-sauvegarder .loader');
            }
        });
    }

    function control_DeleteVideo() {
        $('#container-table-videos .glyphicon-trash').unbind('click');
        $('#container-table-videos .glyphicon-trash').click(function() {
            var idVideo = $(this).attr('id-video');
            
            ViewUtils.prompt(
                'Supprimer cette vid&eacute;o ?', 
                {
                    btOkText: 'Oui',
                    btKoText: 'Non'
                },
                function() {
                    deleteVideo(idVideo);
                }, 
                null, 
                null
            );
        });

        function deleteVideo(idVideo) {
            $('.loader-delete[id-video='+idVideo+']').show();
            AjaxUtils.callService(
                'video', 
                'delete', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idVideo
                }, 
                function(response) {
                    $('.loader-delete[id-video='+idVideo+']').hide();
                    if (response.success) {
                        AdminEditionPhotosVideosController.load_TableVideos();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la suppression de la vid&eacute;o', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-delete[id-video='+idVideo+']').hide();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        }
    }

    function control_ValidationVideo() {
        $('#container-table-videos .icon-validation').unbind('click');
        $('#container-table-videos .icon-validation').click(function() {
            var idVideo = $(this).attr('id-video');
            
            $('.loader-validation[id-video='+idVideo+']').show();
            AjaxUtils.callService(
                'video', 
                'toggleValidation', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idVideo
                }, 
                function(response) {
                    $('.loader-validation[id-album-photo='+idVideo+']').hide();
                    if (response.success) {
                        AdminEditionPhotosVideosController.load_TableVideos();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la validation / invalidation de la vid&eacute;o', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-validation[id-album-photo='+idVideo+']').hide();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        });
    }
};

initializePage = function() {
    AdminController.initializePage('edition', 'photos-videos');

    AdminEditionPhotosVideosController.load_TableAlbumsPhotos();
    AdminEditionPhotosVideosController.load_TableVideos();
};