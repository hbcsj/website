<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/video-dao.php");

class ModalSaveVideoCtrl extends AbstractViewCtrl {
	
	private $video;

	private $videoDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idVideo" => $_GET["idVideo"]
		), true);
		
		if (isAdminConnected_editeur()) {
			$this->videoDAO = new VideoDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdVideo();
	}

	private function checkIdVideo() {
		if (HTTPUtils::paramExists(GET, "idVideo")) {
			$this->video = $this->videoDAO->getById($_GET["idVideo"]);
				
			if ($this->video == null) {
				$this->sendCheckError(
					HTTP_404, 
					"La vid&eacute;o (idVideo = '".$_GET["idVideo"]."') n'existe pas", 
					"webapp/views/common/error/404/404.html.php"
				);
			}
		}
	}

	public function getVideo() {
		return $this->video;
	}

	public function getTitle() {
		$title = "Nouvelle vid&eacute;o";
		if ($this->video != null) {
			$title = $this->video[VIDEO_NOM]." - <span class=\"normal\">Edition</span>";
		}
		return $title;
	}

	public function getNomVideo() {
		if ($this->video != null) {
			return $this->video[VIDEO_NOM];
		}
		return "";
	}

	public function getUrlVideo() {
		if ($this->video != null) {
			return $this->video[VIDEO_URL];
		}
		return "";
	}

	public function getParamIdVideo() {
		if ($this->video != null) {
			return "?id=".$this->video[VIDEO_ID];
		}
		return "";
	}

	public function getVisibleChecked() {
		$checked = "";
		if (
			($this->video == null && isAdminConnected_bureau()) || 
			($this->video != null && isAdminConnected_bureau() && $this->video[VIDEO_VISIBLE_SUR_SITE])
		) {
			$checked = " checked=\"checked\"";
		}
        return $checked;
	}

	public function getEmailDernierEditeurVideo() {
		if (isAdminConnected_superadmin()) {
			if ($this->video != null) {
				return $this->video[VIDEO_EMAIL_DERNIER_EDITEUR];
			} else {
				return $GLOBALS[CONF][SUPERADMIN_EMAIL];
			}
		} 
		return "";
	}
}

?>