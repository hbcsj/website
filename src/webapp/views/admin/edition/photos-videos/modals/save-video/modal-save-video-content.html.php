<?php
require_once("core/php/resources/view.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/edition/photos-videos/modals/save-video/", 
    "modal-save-video", 
    "ModalSaveVideoCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_editeur()) {
    $filesRequired = "";
    if ($ctrl->getVideo() == null) {
        $filesRequired = " required";
    }
    ?>
    <div class="modal-header">
        <div class="modal-title"><?php echo $ctrl->getTitle(); ?></div>
    </div>
    <div class="modal-body">
        <form id="form-save-video" method="post" action="<?php echo ROOT_PATH.UPLOADER_PATH; ?>/video<?php echo $ctrl->getParamIdVideo(); ?>" enctype="multipart/form-data" class="form-horizontal" target="uploader-video">
            <div class="form-group">
                <label for="form-save-video__input-nom" class="col-xs-4 control-label required">Nom :</label>
                <div class="col-xs-7 form-input" for="form-save-video__input-nom">
                    <input type="text" class="form-control" id="form-save-video__input-nom" name="nom" value="<?php echo $ctrl->getNomVideo(); ?>" required>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-video__input-nom"></div>
            </div>
            <div class="form-group">
                <label for="form-save-video__input-url" class="col-xs-4 control-label required">Lien :</label>
                <div class="col-xs-7 form-input" for="form-save-video__input-url">
                    <input type="text" class="form-control" id="form-save-video__input-url" name="url" value="<?php echo $ctrl->getUrlVideo(); ?>" required>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-video__input-url"></div>
            </div>
            <div class="form-group">
                <label for="form-save-video__input-image" class="col-xs-4 control-label<?php echo $filesRequired; ?>">Image :</label>
                <div class="col-xs-7 form-input" for="form-save-video__input-image">
                    <input type="file" class="form-control" id="form-save-video__input-image" name="video-image" pattern="^(.)+(\.<?php echo IMG_VIDEOS_EXTENSION; ?>)$" pattern-indication="L'extension doit &ecirc;tre '.<?php echo IMG_VIDEOS_EXTENSION; ?>'"<?php echo $filesRequired; ?>>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-video__input-image"></div>
            </div>
            <div class="form-group">
                <label for="form-save-video__input-image-small" class="col-xs-4 control-label<?php echo $filesRequired; ?>">Image (largeur=400px) :</label>
                <div class="col-xs-7 form-input" for="form-save-video__input-image-small">
                    <input type="file" class="form-control" id="form-save-video__input-image-small" name="video-image-small" pattern="^(.)+(\.<?php echo IMG_VIDEOS_EXTENSION; ?>)$" pattern-indication="L'extension doit &ecirc;tre '.<?php echo IMG_VIDEOS_EXTENSION; ?>'"<?php echo $filesRequired; ?>>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-video__input-image-small"></div>
            </div>
            <div class="form-group">
                <label for="form-save-video__input-visible" class="col-xs-4 control-label">Visible ?</label>
                <div class="col-xs-1 form-input" for="form-save-video__input-visible">
                    <?php
                    $disabledVisible = "";
                    if (!isAdminConnected_bureau()) {
                        $disabledVisible = " disabled";
                    }
                    ?>
                    <input type="checkbox" id="form-save-video__input-visible" name="visible-sur-site"<?php echo $ctrl->getVisibleChecked(); ?><?php echo $disabledVisible; ?>>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-video__input-visible"></div>
                <label for="form-save-video__input-email-dernier-editeur" class="col-xs-1 control-label required">&nbsp;</label>
                <div class="col-xs-4 form-input" for="form-save-video__input-email-dernier-editeur">
                    <?php
                    $disabledEmailDernierEditeur = "";
                    if (isAdminConnected_superadmin()) {
                        $disabledEmailDernierEditeur = " readonly";
                    }
                    ?>
                    <input type="text" class="form-control" id="form-save-video__input-email-dernier-editeur" name="email-dernier-editeur" value="<?php echo $ctrl->getEmailDernierEditeurVideo(); ?>" required <?php echo $disabledEmailDernierEditeur; ?> placeholder="Email dernier &eacute;diteur" pattern="^[A-Za-z0-9._-]+@[A-Za-z0-9.-]{2,}[.][A-Za-z]{2,3}$" pattern-indication="Le format de l'email est invalide">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-video__input-email-dernier-editeur"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-required text-right">Les champs marqu&eacute;s d'une * sont obligatoires</div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-8">
                <div class="form-result-message"></div>
            </div>
            <div class="col-xs-4 text-right">
                <button id="modal-save-video__bt-sauvegarder" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-ok"></span>
                    </div>
                    <div class="button__text">Sauvegarder</div>
                    <div class="loader"></div>
                </button>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>