<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/album-photo-dao.php");

class ModalSaveAlbumPhotoCtrl extends AbstractViewCtrl {
	
	private $albumPhoto;

	private $albumPhotoDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idAlbumPhoto" => $_GET["idAlbumPhoto"]
		), true);
		
		if (isAdminConnected_editeur()) {
			$this->albumPhotoDAO = new AlbumPhotoDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdAlbumPhoto();
	}

	private function checkIdAlbumPhoto() {
		if (HTTPUtils::paramExists(GET, "idAlbumPhoto")) {
			$this->albumPhoto = $this->albumPhotoDAO->getById($_GET["idAlbumPhoto"]);
				
			if ($this->albumPhoto == null) {
				$this->sendCheckError(
					HTTP_404, 
					"L'album photo (idAlbumPhoto = '".$_GET["idAlbumPhoto"]."') n'existe pas", 
					"webapp/views/common/error/404/404.html.php"
				);
			}
		}
	}

	public function getAlbumPhoto() {
		return $this->albumPhoto;
	}

	public function getTitle() {
		$title = "Nouvel album photo";
		if ($this->albumPhoto != null) {
			$title = $this->albumPhoto[ALBUM_PHOTO_NOM]." - <span class=\"normal\">Edition</span>";
		}
		return $title;
	}

	public function getNomAlbumPhoto() {
		if ($this->albumPhoto != null) {
			return $this->albumPhoto[ALBUM_PHOTO_NOM];
		}
		return "";
	}

	public function getUrlAlbumPhoto() {
		if ($this->albumPhoto != null) {
			return $this->albumPhoto[ALBUM_PHOTO_URL];
		}
		return "";
	}

	public function getParamIdAlbumPhoto() {
		if ($this->albumPhoto != null) {
			return "?id=".$this->albumPhoto[ALBUM_PHOTO_ID];
		}
		return "";
	}

	public function getVisibleChecked() {
		$checked = "";
		if (
			($this->albumPhoto == null && isAdminConnected_bureau()) || 
			($this->albumPhoto != null && isAdminConnected_bureau() && $this->albumPhoto[ALBUM_PHOTO_VISIBLE_SUR_SITE])
		) {
			$checked = " checked=\"checked\"";
		}
        return $checked;
	}

	public function getEmailDernierEditeurAlbumPhoto() {
		if (isAdminConnected_superadmin()) {
			if ($this->albumPhoto != null) {
				return $this->albumPhoto[ALBUM_PHOTO_EMAIL_DERNIER_EDITEUR];
			} else {
				return $GLOBALS[CONF][SUPERADMIN_EMAIL];
			}
		} 
		return "";
	}
}

?>