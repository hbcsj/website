<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/path-utils.php");

$view = new View(
    "webapp/views/admin/edition/photos-videos/views/table-albums-photos/", 
    "table-albums-photos", 
    "TableAlbumsPhotosCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_editeur()) {
    $albumsPhotos = $ctrl->getAlbumsPhotos();
    ?>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Nom</th>
                <th class="text-center">Lien</th>
                <th class="text-center">Visible ?</th>
                <?php
                if (isAdminConnected_superadmin()) {
                    ?>
                    <th colspan="3">&nbsp;</th>
                    <?php
                } else if (isAdminConnected_bureau()) {
                    ?>
                    <th colspan="2">&nbsp;</th>
                    <?php
                } else {
                    ?>
                    <th>&nbsp;</th>
                    <?php
                }
                ?>
            </tr>
        </thead>
        <tbody>
            <?php
            if (sizeof($albumsPhotos) > 0) {
                foreach ($albumsPhotos as $albumPhoto) {
                    $bg = "";
                    if ($albumPhoto[ALBUM_PHOTO_EMAIL_DERNIER_EDITEUR] != $GLOBALS[CONF][SUPERADMIN_EMAIL]) {
                        $bg = "bg-error";
                    } else if (!$albumPhoto[ALBUM_PHOTO_VISIBLE_SUR_SITE]) {
                        $bg = "bg-warning";
                    }
                    ?>
                    <tr>
                        <td class="<?php echo $bg; ?>"><?php echo $albumPhoto[ALBUM_PHOTO_NOM]; ?></td>
                        <td class="text-center <?php echo $bg; ?>">
                            <a href="<?php echo $albumPhoto[ALBUM_PHOTO_URL]; ?>" target="_blank">Voir</a>
                        </td>
                        <td class="text-center <?php echo $bg; ?>"><?php echo ($albumPhoto[ALBUM_PHOTO_VISIBLE_SUR_SITE] == 1) ? "Oui" : "Non"; ?></td>
                        <td class="text-center <?php echo $bg; ?>">
                            <span class="glyphicon glyphicon-pencil clickable" title="Editer" id-album-photo="<?php echo $albumPhoto[ALBUM_PHOTO_ID]; ?>"></span>
                        </td>
                        <?php
                        if (isAdminConnected_bureau()) {
                            ?>
                            <td class="text-center <?php echo $bg; ?>">
                                <span class="glyphicon glyphicon-trash clickable" title="Supprimer" id-album-photo="<?php echo $albumPhoto[ALBUM_PHOTO_ID]; ?>"></span>
                                <div class="loader loader-delete" id-album-photo="<?php echo $albumPhoto[ALBUM_PHOTO_ID]; ?>"></div>
                            </td>
                            <?php
                        }
                        if (isAdminConnected_superadmin()) {
                            if (!$ctrl->isAlbumPhotoVisible($albumPhoto)) {
                                ?>
                                <td class="text-center <?php echo $bg; ?>">
                                    <span class="glyphicon glyphicon-ok icon-validation clickable" title="Valider" id-album-photo="<?php echo $albumPhoto[ALBUM_PHOTO_ID]; ?>"></span>
                                    <div class="loader loader-validation" id-album-photo="<?php echo $albumPhoto[ALBUM_PHOTO_ID]; ?>"></div>
                                </td>
                                <?php
                            } else {
                                ?>
                                <td class="text-center <?php echo $bg; ?>">
                                    <span class="glyphicon glyphicon-remove icon-validation clickable" title="Invalider" id-album-photo="<?php echo $albumPhoto[ALBUM_PHOTO_ID]; ?>"></span>
                                    <div class="loader loader-validation" id-album-photo="<?php echo $albumPhoto[ALBUM_PHOTO_ID]; ?>"></div>
                                </td>
                                <?php
                            }
                        }
                        ?>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>