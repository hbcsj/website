<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/album-photo-dao.php");
require_once("common/php/lib/date-utils.php");

class TableAlbumsPhotosCtrl extends AbstractViewCtrl {

	private $albumPhotoDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_editeur()) {
			$this->albumPhotoDAO = new AlbumPhotoDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getAlbumsPhotos() {
		return $this->albumPhotoDAO->getAll(ALBUM_PHOTO_TABLE_NAME.".".ALBUM_PHOTO_ID." DESC");
	}

	public function isAlbumPhotoVisible($actualite) {
		return (
			$actualite[ALBUM_PHOTO_VISIBLE_SUR_SITE] && 
			$actualite[ALBUM_PHOTO_EMAIL_DERNIER_EDITEUR] == $GLOBALS[CONF][SUPERADMIN_EMAIL]
		);
	}
}

?>