<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/video-dao.php");
require_once("common/php/lib/date-utils.php");

class TableVideosCtrl extends AbstractViewCtrl {

	private $videoDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_editeur()) {
			$this->videoDAO = new VideoDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getVideos() {
		return $this->videoDAO->getAll(VIDEO_TABLE_NAME.".".VIDEO_ID." DESC");
	}

	public function isVideoVisible($actualite) {
		return (
			$actualite[VIDEO_VISIBLE_SUR_SITE] && 
			$actualite[VIDEO_EMAIL_DERNIER_EDITEUR] == $GLOBALS[CONF][SUPERADMIN_EMAIL]
		);
	}
}

?>