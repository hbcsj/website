<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/path-utils.php");

$view = new View(
    "webapp/views/admin/edition/photos-videos/views/table-videos/", 
    "table-videos", 
    "TableVideosCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_editeur()) {
    $videos = $ctrl->getVideos();
    ?>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Nom</th>
                <th class="text-center">Lien</th>
                <th class="text-center">Visible ?</th>
                <?php
                if (isAdminConnected_superadmin()) {
                    ?>
                    <th colspan="3">&nbsp;</th>
                    <?php
                } else if (isAdminConnected_bureau()) {
                    ?>
                    <th colspan="2">&nbsp;</th>
                    <?php
                } else {
                    ?>
                    <th>&nbsp;</th>
                    <?php
                }
                ?>
            </tr>
        </thead>
        <tbody>
            <?php
            if (sizeof($videos) > 0) {
                foreach ($videos as $video) {
                    $bg = "";
                    if ($video[VIDEO_EMAIL_DERNIER_EDITEUR] != $GLOBALS[CONF][SUPERADMIN_EMAIL]) {
                        $bg = "bg-error";
                    } else if (!$video[VIDEO_VISIBLE_SUR_SITE]) {
                        $bg = "bg-warning";
                    }
                    ?>
                    <tr>
                        <td class="<?php echo $bg; ?>"><?php echo $video[VIDEO_NOM]; ?></td>
                        <td class="text-center <?php echo $bg; ?>">
                            <a href="<?php echo $video[VIDEO_URL]; ?>" target="_blank">Voir</a>
                        </td>
                        <td class="text-center <?php echo $bg; ?>"><?php echo ($video[VIDEO_VISIBLE_SUR_SITE] == 1) ? "Oui" : "Non"; ?></td>
                        <td class="text-center <?php echo $bg; ?>">
                            <span class="glyphicon glyphicon-pencil clickable" title="Editer" id-video="<?php echo $video[VIDEO_ID]; ?>"></span>
                        </td>
                        <?php
                        if (isAdminConnected_bureau()) {
                            ?>
                            <td class="text-center <?php echo $bg; ?>">
                                <span class="glyphicon glyphicon-trash clickable" title="Supprimer" id-video="<?php echo $video[VIDEO_ID]; ?>"></span>
                                <div class="loader loader-delete" id-video="<?php echo $video[VIDEO_ID]; ?>"></div>
                            </td>
                            <?php
                        }
                        if (isAdminConnected_superadmin()) {
                            if (!$ctrl->isVideoVisible($video)) {
                                ?>
                                <td class="text-center <?php echo $bg; ?>">
                                    <span class="glyphicon glyphicon-ok icon-validation clickable" title="Valider" id-video="<?php echo $video[VIDEO_ID]; ?>"></span>
                                    <div class="loader loader-validation" id-video="<?php echo $video[VIDEO_ID]; ?>"></div>
                                </td>
                                <?php
                            } else {
                                ?>
                                <td class="text-center <?php echo $bg; ?>">
                                    <span class="glyphicon glyphicon-remove icon-validation clickable" title="Invalider" id-video="<?php echo $video[VIDEO_ID]; ?>"></span>
                                    <div class="loader loader-validation" id-video="<?php echo $video[VIDEO_ID]; ?>"></div>
                                </td>
                                <?php
                            }
                        }
                        ?>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>