<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/evenements/evenements/views/recherche/", 
    "recherche-evenements", 
    "RechercheEvenementsCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
    $categories = $ctrl->getCategories();
    $numEquipeMax = $ctrl->getNumEquipeMax();
    $typesEvenements = $ctrl->getTypesEvenements();
    ?>
    <div class="container-recherche__title" title="Ouvrir le formulaire de recherche" for="form-recherche-evenements">
        <div class="container-recherche__title__icon">
            <span class="glyphicon glyphicon-search"></span>
        </div>
        <div class="container-recherche__title__text">Recherche</div>
        <div class="container-recherche__title__arrow">
            <span class="glyphicon glyphicon-triangle-bottom"></span>
        </div>
    </div>
    <div class="form-horizontal" id="form-recherche-evenements">
        <div class="form-group">
            <label for="form-recherche-evenements__input-date-debut" class="col-xs-1 control-label">Date :</label>
            <div class="col-xs-2 form-input" for="form-recherche-evenements__input-date-debut">
                <div class="input-group">
                    <input type="text" class="form-control" id="form-recherche-evenements__input-date-debut" placeholder="D&eacute;but">
                    <span class="input-group-addon">
                        <img src="<?php echo SRC_PATH; ?>assets/img/icons/calendar.png">
                    </span>
                </div>
            </div>
            <label for="form-recherche-evenements__input-categorie" class="col-xs-2 control-label">Cat&eacute;gorie :</label>
            <div class="col-xs-2 form-input" for="form-recherche-evenements__input-categorie">
                <select class="form-control" id="form-recherche-evenements__input-categorie">
                    <option value=""></option>
                    <?php
                    if (sizeof($categories) > 0) {
                        foreach ($categories as $categorie) {
                            ?>
                            <option value="<?php echo $categorie[CATEGORIE_ID]; ?>"><?php echo $categorie[CATEGORIE_NOM]; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <label for="form-recherche-evenements__input-num-equipe" class="col-xs-2 control-label">Num. &eacute;quipe :</label>
            <div class="col-xs-1 form-input" for="form-recherche-evenements__input-num-equipe">
                <select class="form-control" id="form-recherche-evenements__input-num-equipe">
                    <option value=""></option>
                    <?php
                    for ($numEquipe = 1 ; $numEquipe <= $numEquipeMax ; $numEquipe++) {
                        ?>
                        <option value="<?php echo $numEquipe; ?>"><?php echo $numEquipe; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-2 col-xs-offset-1 form-input" for="form-recherche-evenements__input-date-fin">
                <div class="input-group">
                    <input type="text" class="form-control" id="form-recherche-evenements__input-date-fin" placeholder="Fin">
                    <span class="input-group-addon">
                        <img src="<?php echo SRC_PATH; ?>assets/img/icons/calendar.png">
                    </span>
                </div>
            </div>
            <label for="form-recherche-evenements__input-type" class="col-xs-2 control-label">Type :</label>
            <div class="col-xs-2 form-input" for="form-recherche-evenements__input-type">
                <select class="form-control" id="form-recherche-evenements__input-type">
                    <option value=""></option>
                    <?php
                    if (sizeof($typesEvenements) > 0) {
                        foreach ($typesEvenements as $typeEvenement) {
                            ?>
                            <option value="<?php echo $typeEvenement[TYPE_EVENEMENT_ID]; ?>"><?php echo $typeEvenement[TYPE_EVENEMENT_LIBELLE]; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <label for="form-recherche-evenements__input-competition" class="col-xs-2 control-label">Comp&eacute;t. :</label>
            <div class="col-xs-2 form-input" for="form-recherche-evenements__input-competition">
                <select class="form-control" id="form-recherche-evenements__input-competition">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="form-recherche-evenements__input-adversaire" class="col-xs-1 control-label">Advers. :</label>
            <div class="col-xs-2 form-input" for="form-recherche-evenements__input-adversaire">
                <input type="text" class="form-control" id="form-recherche-evenements__input-adversaire">
            </div>
            <label for="form-recherche-evenements__input-ville" class="col-xs-2 control-label">Ville :</label>
            <div class="col-xs-2 form-input" for="form-recherche-evenements__input-ville">
                <input type="text" class="form-control" id="form-recherche-evenements__input-ville">
            </div>
            <label for="form-recherche-evenements__input-is-a-domicile" class="col-xs-2 control-label">A domic. ?</label>
            <div class="col-xs-1 form-input" for="form-recherche-evenements__input-is-a-domicile">
                <select class="form-control" id="form-recherche-evenements__input-is-a-domicile">
                    <option value=""></option>
                    <option value="1">Oui</option>
                    <option value="0">Non</option>
                </select>
            </div>
        </div>
    </div>
    <div class="container-recherche__footer" for="form-recherche-evenements">
        <button id="recherche-evenements__bt-rechercher" class="btn btn-default">
            <div class="button__icon">
                <span class="glyphicon glyphicon-search"></span>
            </div>
            <div class="button__text">Chercher</div>
        </button>
        <button id="recherche-evenements__bt-reset" class="btn btn-default">
            <div class="button__icon">
                <span class="glyphicon glyphicon-erase"></span>
            </div>
            <div class="button__text">Reset</div>
        </button>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>