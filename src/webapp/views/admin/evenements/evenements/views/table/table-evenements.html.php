<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");

$view = new View(
    "webapp/views/admin/evenements/evenements/views/table/", 
    "table-evenements", 
    "TableEvenementsCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
    $evenements = $ctrl->getEvenements();
    ?>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Date</th>
                <th>Code rencontre</th>
                <th>Titre</th>
                <th>R&eacute;sultat</th>
                <th class="text-center">FDM</th>
                <th>Ville</th>
                <th class="text-center">&nbsp;</th>
                <th class="text-center">&nbsp;</th>
                <th class="text-center">Visible ?</th>
                <?php
                if (isAdminConnected_superadmin()) {
                    ?>
                    <th colspan="3">&nbsp;</th>
                    <?php
                } else if (isAdminConnected_bureau()) {
                    ?>
                    <th colspan="2">&nbsp;</th>
                    <?php
                } else {
                    ?>
                    <th>&nbsp;</th>
                    <?php
                }
                ?>
            </tr>
        </thead>
        <tbody>
            <?php
            if (sizeof($evenements) > 0) {
                foreach ($evenements as $evenement) {
                    $bg = "";
                    if ($evenement[EVENEMENT_EMAIL_DERNIER_EDITEUR] != $GLOBALS[CONF][SUPERADMIN_EMAIL]) {
                        $bg = "bg-error";
                    } else if (!$evenement[EVENEMENT_VISIBLE_SUR_SITE]) {
                        $bg = "bg-warning";
                    }

                    $evenementProps = $ctrl->getRecapEvenementProps($evenement);
                    $nbActionsClub = $ctrl->getNbActionsClub($evenement[EVENEMENT_ID]);
                    $nbArbitrages = $ctrl->getNbArbitrages($evenement[EVENEMENT_ID]);
                    ?>
                    <tr>
                        <td class="<?php echo $bg; ?>">
                            <?php
                            echo $evenementProps[EVENEMENT_JOUR]."<br>".str_replace(SLASH_DATE_TIME_SEPARATOR, "<br>", DateUtils::convert_sqlDateTime_to_slashDateTime($evenement[EVENEMENT_DATE_HEURE]));
                            ?>
                        </td>
                        <td class="<?php echo $bg; ?>">
                            <?php
                            echo $evenement[EVENEMENT_CODE];
                            ?>
                        </td>
                        <td class="<?php echo $bg; ?>">
                            <?php
                            if ($evenement[EVENEMENT_TYPE_EVENEMENT_ID] == EVENEMENT_MATCH_TYPE_ID) {
                                echo $evenementProps[MATCH_EQUIPE_DOMICILE]." VS ".$evenementProps[MATCH_EQUIPE_EXTERIEUR];
                                ?>
                                <br><br>
                                <?php
                                if ($evenementProps[MATCH_COMPETITION] != null) {
                                    if ($evenementProps[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB] != "") {
                                        ?>
                                        <a href="<?php echo $evenementProps[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB]; ?>" target="_blank">
                                        <?php
                                    }
                                    echo $evenementProps[MATCH_COMPETITION][COMPETITION_NOM];
                                    if ($evenementProps[MATCH_COMPETITION][COMPETITION_URL_SITE_FFHB] != "") {
                                        echo "</a>";
                                    }
                                }
                            } else {
                                if (sizeof($evenementProps[EVENEMENT_CATEGORIES]) > 0) {
                                    if (!$ctrl->toutesLesCategoriesParticipentAEvenement($evenementProps[EVENEMENT_CATEGORIES])) {
                                        $nomsCategories = array();
                                        foreach ($evenementProps[EVENEMENT_CATEGORIES] as $evenementCategorie) {
                                            $nomsCategories[] = $evenementCategorie[CATEGORIE_NOM];
                                        }
                                        echo implode(", ", $nomsCategories)." - ";
                                    }
                                }
                                echo $evenementProps[EVENEMENT_TYPE];
                                
                                echo "<br><br>";
                                echo $evenement[EVENEMENT_NOM];
                            }
                            ?>
                        </td>
                        <td class="<?php echo $bg; ?><?php echo ($evenement[EVENEMENT_TYPE_EVENEMENT_ID] == EVENEMENT_MATCH_TYPE_ID) ? " resultat-".$evenementProps[MATCH_CLASS_RESULTAT] : ""; ?>">
                            <?php
                            if ($evenement[EVENEMENT_TYPE_EVENEMENT_ID] == EVENEMENT_MATCH_TYPE_ID && 
                                (
                                    $evenementProps[MATCH_SCORE_DOMICILE] != "" || 
                                    $evenementProps[MATCH_SCORE_EXTERIEUR] != ""
                                )
                            ) {
                                echo $evenementProps[MATCH_SCORE_DOMICILE]." - ".$evenementProps[MATCH_SCORE_EXTERIEUR];
                            }
                            ?>
                        </td>
                        <td class="text-center <?php echo $bg; ?>">
                            <?php
                            if ($evenement[EVENEMENT_TYPE_EVENEMENT_ID] == EVENEMENT_MATCH_TYPE_ID) {
                                if ($evenementProps[MATCH_FDM] != null) {
                                    ?>
                                    <a href="<?php echo SRC_PATH.$evenementProps[MATCH_FDM]; ?>" target="_blank" class="link-with-img">
                                        <span class="glyphicon glyphicon-open-file"></span>
                                    </a>
                                    <span class="glyphicon glyphicon-remove-circle clickable" id-evenement="<?php echo $evenement[EVENEMENT_ID]; ?>"></span>
                                    <?php
                                }
                                ?>
                                <span class="glyphicon glyphicon-upload clickable" id-evenement="<?php echo $evenement[EVENEMENT_ID]; ?>"></span>
                                <?php
                            }
                            ?>
                        </td>
                        <td class="<?php echo $bg; ?>">
                            <?php
                            if ($evenementProps[EVENEMENT_GOOGLE_MAP_URL] != "") {
                                echo "<a href=\"".$evenementProps[EVENEMENT_GOOGLE_MAP_URL]."\" target=\"blank\">".$evenement[EVENEMENT_VILLE]."</a>"; 
                            } else {
                                echo "&nbsp;";
                            }
                            ?>
                        </td>
                        <td class="text-center <?php echo $bg; ?>">
                            <button class="btn btn-default bt-table-admin bt-evenement-historique-dispos" id-evenement="<?php echo $evenement[EVENEMENT_ID]; ?>">
                                <div class="button__text">Historique<br>disponibilit&eacute;s</div>
                            </button>
                        </td>
                        <td class="text-center <?php echo $bg; ?>">
                            <button class="btn btn-default bt-table-admin bt-evenement-actions-club" id-evenement="<?php echo $evenement[EVENEMENT_ID]; ?>">
                                <div class="button__text"><?php echo $nbActionsClub; ?> action(s) club</div>
                            </button>
                        </td>
                        <td class="text-center <?php echo $bg; ?>">
                            <button class="btn btn-default bt-table-admin bt-evenement-arbitrages" id-evenement="<?php echo $evenement[EVENEMENT_ID]; ?>">
                                <div class="button__text"><?php echo $nbArbitrages; ?> arbitrage(s)</div>
                            </button>
                        </td>
                        <td class="text-center <?php echo $bg; ?>"><?php echo ($evenement[EVENEMENT_VISIBLE_SUR_SITE] == 1) ? "Oui" : "Non"; ?></td>
                        <td class="text-center <?php echo $bg; ?>">
                            <span class="glyphicon glyphicon-pencil clickable" title="Editer" id-evenement="<?php echo $evenement[EVENEMENT_ID]; ?>"></span>
                        </td>
                        <?php
                        if (isAdminConnected_bureau()) {
                            ?>
                            <td class="text-center <?php echo $bg; ?>">
                                <span class="glyphicon glyphicon-trash clickable" title="Supprimer" id-evenement="<?php echo $evenement[EVENEMENT_ID]; ?>"></span>
                                <div class="loader loader-delete" id-evenement="<?php echo $evenement[EVENEMENT_ID]; ?>"></div>
                            </td>
                            <?php
                        }
                        if (isAdminConnected_superadmin()) {
                            if (!$ctrl->isEvenementVisible($evenement)) {
                                ?>
                                <td class="text-center <?php echo $bg; ?>">
                                    <span class="glyphicon glyphicon-ok icon-validation clickable" title="Valider" id-evenement="<?php echo $evenement[EVENEMENT_ID]; ?>"></span>
                                    <div class="loader loader-validation" id-evenement="<?php echo $evenement[EVENEMENT_ID]; ?>"></div>
                                </td>
                                <?php
                            } else {
                                ?>
                                <td class="text-center <?php echo $bg; ?>">
                                    <span class="glyphicon glyphicon-remove icon-validation clickable" title="Invalider" id-evenement="<?php echo $evenement[EVENEMENT_ID]; ?>"></span>
                                    <div class="loader loader-validation" id-evenement="<?php echo $evenement[EVENEMENT_ID]; ?>"></div>
                                </td>
                                <?php
                            }
                        }
                        ?>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>