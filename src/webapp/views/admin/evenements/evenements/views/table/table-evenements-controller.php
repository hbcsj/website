<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/evenement-dao.php");
require_once("common/php/dao/categorie-dao.php");
require_once("common/php/dao/action-club-dao.php");
require_once("common/php/dao/arbitrage-dao.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/managers/evenement-manager.php");

class TableEvenementsCtrl extends AbstractViewCtrl {

	private $evenementDAO;
	private $categorieDAO;
	private $arbitrageDAO;

	private $evenementManager;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_coach()) {
			$this->evenementDAO = new EvenementDAO($this->getDatabaseConnection());
			$this->categorieDAO = new CategorieDAO($this->getDatabaseConnection());
			$this->actionClubDAO = new ActionClubDAO($this->getDatabaseConnection());
			$this->arbitrageDAO = new ArbitrageDAO($this->getDatabaseConnection());

			$this->evenementManager = new EvenementManager($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getEvenements() {
		$orderBy = EVENEMENT_TABLE_NAME.".".EVENEMENT_DATE_HEURE;
		if ($_GET["categorieId"] != null && $_GET["categorieId"] != "") {
			$orderBy .= ", ".CATEGORIE_PARTICIPE_A_EVENEMENT_TABLE_NAME.".".CATEGORIE_PARTICIPE_A_EVENEMENT_CATEGORIE_ID;
		}
		return $this->evenementDAO->getAllByParams(
			DateUtils::convert_slashDate_to_sqlDate($_GET["dateDebut"]),
			DateUtils::convert_slashDate_to_sqlDate($_GET["dateFin"]),
			$_GET["categorieId"],
			$_GET["numEquipe"],
			$_GET["type"],
			$_GET["competitionId"],
			trim(strtolower($_GET["adversaire"])),
			trim(strtolower($_GET["ville"])),
			$_GET["aDomicile"],
			null,
			$orderBy
		);
	}

    public function getRecapEvenementProps($evenement) {
        return $this->evenementManager->getRecapEvenementProps($evenement);
	}

	public function toutesLesCategoriesParticipentAEvenement($categoriesEvenement) {
		return (sizeof($categoriesEvenement) == sizeof($this->categorieDAO->getAll()));
	}

	public function getNbActionsClub($evenementId) {
		return sizeof($this->actionClubDAO->getByEvenementId($evenementId));
	}

	public function getNbArbitrages($evenementId) {
		return sizeof($this->arbitrageDAO->getByEvenementId($evenementId));
	}

	public function isEvenementVisible($evenement) {
		return (
			$evenement[EVENEMENT_VISIBLE_SUR_SITE] && 
			$evenement[EVENEMENT_EMAIL_DERNIER_EDITEUR] == $GLOBALS[CONF][SUPERADMIN_EMAIL]
		);
	}
}

?>