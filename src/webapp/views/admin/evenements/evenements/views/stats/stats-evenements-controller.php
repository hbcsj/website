<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/evenement-dao.php");

class StatsEvenementsCtrl extends AbstractViewCtrl {

	private $evenementDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_coach()) {
			$this->evenementDAO = new EvenementDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getNbEvenements() {
		return $this->evenementDAO->getNbTotal()[EVENEMENT_NB_EVENEMENTS];
	}

	public function getNbEvenementsAVenir() {
		return $this->evenementDAO->getNbAVenir()[EVENEMENT_NB_EVENEMENTS];
	}

	public function getNbEvenementsVisibles() {
		return $this->evenementDAO->getNbVisibles()[EVENEMENT_NB_EVENEMENTS];
	}

	public function getNbEvenementsAValider() {
		return $this->evenementDAO->getNbAValider()[EVENEMENT_NB_EVENEMENTS];
	}
}

?>