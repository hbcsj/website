<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/evenements/evenements/views/stats/", 
    "stats-evenements", 
    "StatsEvenementsCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
    ?>
    <div class="container-stats__section">
        <div class="container-stats__section__title">Nombre d'&eacute;v&eacute;nements : </div>
        <div class="container-stats__section__value"><?php echo $ctrl->getNbEvenements(); ?></div>
    </div>
    <div class="container-stats__section">
        <div class="container-stats__section__title">
            Dont &agrave; venir :
            <br>
            Dont visibles :
        </div>
        <div class="container-stats__section__value">
            <?php echo $ctrl->getNbEvenementsAVenir(); ?>
            <br>
            <?php echo $ctrl->getNbEvenementsVisibles(); ?>
        </div>
    </div>
    <div class="container-stats__section">
        <div class="container-stats__section__title">A valider : </div>
        <div class="container-stats__section__value"><?php echo $ctrl->getNbEvenementsAValider(); ?></div>
    </div>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>