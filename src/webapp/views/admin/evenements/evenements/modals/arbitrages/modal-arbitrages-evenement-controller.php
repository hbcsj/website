<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/evenement-dao.php");
require_once("common/php/dao/type-evenement-dao.php");
require_once("common/php/dao/licencie-dao.php");
require_once("common/php/dao/arbitrage-dao.php");

class ModalArbitragesEvenementCtrl extends AbstractViewCtrl {
	
	private $evenement;

	private $licencieDAO;
	private $evenementDAO;
	private $typeEvenementDAO;
	private $arbitrageDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idEvenement" => $_GET["idEvenement"]
		), true);
		
		if (isAdminConnected_coach()) {
			$this->licencieDAO = new LicencieDAO($this->getDatabaseConnection());
			$this->evenementDAO = new EvenementDAO($this->getDatabaseConnection());
			$this->typeEvenementDAO = new TypeEvenementDAO($this->getDatabaseConnection());
			$this->arbitrageDAO = new ArbitrageDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$checkParams = $this->checkParams(array(
			array(GET, "idEvenement")
		));
		if ($checkParams) {
			$this->checkIdEvenement();
		}
	}

	private function checkIdEvenement() {
		$this->evenement = $this->evenementDAO->getById($_GET["idEvenement"]);
			
		if ($this->evenement == null) {
			$this->sendCheckError(
				HTTP_404, 
				"L'evenement (idEvenement = '".$_GET["idEvenement"]."') n'existe pas", 
				"webapp/views/common/error/404/404.html.php"
			);
		}
	}

	public function getEvenement() {
		return $this->evenement;
	}

	public function getLicencies($evenementId) {
		return $this->licencieDAO->getNotArbitresByEvenementId(
			$evenementId, 
			LICENCIE_TABLE_NAME.".".LICENCIE_NOM.", ".LICENCIE_TABLE_NAME.".".LICENCIE_PRENOM
		);
	}

	public function getLicencie($licencieId) {
		return $this->licencieDAO->getById($licencieId);
	}

	public function getArbitrages() {
		return $this->arbitrageDAO->getByEvenementId($this->evenement[EVENEMENT_ID]);
	}

	public function getLibelleTypeEvenement($typeEvenementId) {
		$typeEvenement = $this->typeEvenementDAO->getById($typeEvenementId);
		return $typeEvenement[TYPE_EVENEMENT_LIBELLE];
	}
}

?>