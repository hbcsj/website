<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/evenements/evenements/modals/arbitrages/", 
    "modal-arbitrages-evenement", 
    "ModalArbitragesEvenementCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
    $arbitrages = $ctrl->getArbitrages();
    $evenement = $ctrl->getEvenement();
    $licencies = $ctrl->getLicencies($evenement[EVENEMENT_ID]);
    ?>
    <div class="modal-header">
        <div class="modal-title">
            <?php
            if ($evenement[EVENEMENT_TYPE_EVENEMENT_ID] == EVENEMENT_MATCH_TYPE_ID) {
				echo "Match contre ".$evenement[EVENEMENT_ADVERSAIRE];
			} else {
				echo $ctrl->getLibelleTypeEvenement($evenement[EVENEMENT_TYPE_EVENEMENT_ID])." '".$evenement[EVENEMENT_NOM]."' &agrave; ".$evenement[EVENEMENT_VILLE];
			}
            ?> - 
            <span class="normal">Arbitrages</span>
        </div>
        <div class="loader loader-modal" for="form-arbitrages-evenement"></div>
    </div>
    <div class="modal-body">
        <div class="form-horizontal" id="form-arbitrages-evenement">
            <?php
            if (sizeof($arbitrages) > 0) {
                foreach ($arbitrages as $arbitrage) {
                    $licencieArbitrage = $ctrl->getLicencie($arbitrage[ARBITRAGE_LICENCIE_ID]);
                    ?>
                    <div class="form-group">
                        <label for="form-arbitrages-evenement__input-licencie-<?php echo $licencieArbitrage[LICENCIE_ID]; ?>" class="col-xs-2 control-label">Licenci&eacute; :</label>
                        <div class="col-xs-5 form-input" for="form-arbitrages-evenement__input-licencie-<?php echo $licencieArbitrage[LICENCIE_ID]; ?>">
                            <input type="text" class="form-control" value="<?php echo $licencieArbitrage[LICENCIE_NOM]." ".$licencieArbitrage[LICENCIE_PRENOM]; ?>" disabled>
                        </div>
                        <div class="col-xs-2 col-xs-offset-3 text-right">
                            <button class="btn btn-default bt-supprimer-arbitrage" id-licencie="<?php echo $licencieArbitrage[LICENCIE_ID]; ?>">
                                <div class="button__icon">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </div>
                                <div class="button__text">Sup.</div>
                            </button>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <div class="form-horizontal" id="form-add-arbitrage-evenement">
            <div class="form-group">
                <label for="form-add-arbitrage-evenement__input-licencie" class="col-xs-2 control-label required">Licenci&eacute; :</label>
                <div class="col-xs-5 form-input" for="form-add-arbitrage-evenement__input-licencie">
                    <select class="form-control" id="form-add-arbitrage-evenement__input-licencie" required>
                        <option value=""></option>
                        <?php
                        if (sizeof($licencies)) {
                            foreach ($licencies as $licencie) {
                                ?>
                                <option value="<?php echo $licencie[LICENCIE_ID]; ?>"><?php echo $licencie[LICENCIE_NOM]." ".$licencie[LICENCIE_PRENOM]; ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-add-arbitrage-evenement__input-licencie"></div>
                <div class="col-xs-2 col-xs-offset-3 text-right">
                    <button id="modal-arbitrages-evenement__bt-ajouter" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-plus"></span>
                        </div>
                        <div class="button__text">OK</div>
                    </button>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-required text-right">Les champs marqu&eacute;s d'une * sont obligatoires</div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-12">
                <div class="form-result-message"></div>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>