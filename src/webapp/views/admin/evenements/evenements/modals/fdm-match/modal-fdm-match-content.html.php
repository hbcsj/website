<?php
require_once("core/php/resources/view.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/evenements/evenements/modals/fdm-match/", 
    "modal-fdm-match", 
    "ModalFDMMatchCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
    $evenement = $ctrl->getEvenement();
    ?>
    <div class="modal-header">
        <div class="modal-title">Match contre <?php echo $evenement[EVENEMENT_ADVERSAIRE]; ?> - Feuille de match</div>
    </div>
    <div class="modal-body">
        <form id="form-fdm-match" method="post" action="<?php echo ROOT_PATH.UPLOADER_PATH; ?>/match-fdm?idEvenement=<?php echo $evenement[EVENEMENT_ID]; ?>" enctype="multipart/form-data" class="form-horizontal" target="uploader-fdm-match">
            <div class="form-group">
                <label for="form-fdm-match__input-fdm" class="col-xs-4 control-label required">Feuille de match :</label>
                <div class="col-xs-7 form-input" for="form-fdm-match__input-fdm">
                    <input type="file" class="form-control" id="form-fdm-match__input-fdm" name="fdm-file" pattern="^(.)+(\.<?php echo FDM_EXTENSION; ?>)$" pattern-indication="L'extension doit &ecirc;tre '.<?php echo FDM_EXTENSION; ?>'" required>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-fdm-match__input-fdm"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-required text-right">Les champs marqu&eacute;s d'une * sont obligatoires</div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-8">
                <div class="form-result-message"></div>
            </div>
            <div class="col-xs-4 text-right">
                <button id="modal-fdm-match__bt-sauvegarder" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-ok"></span>
                    </div>
                    <div class="button__text">Sauvegarder</div>
                    <div class="loader"></div>
                </button>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>