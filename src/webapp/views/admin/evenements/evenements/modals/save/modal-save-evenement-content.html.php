<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/evenements/evenements/modals/save/", 
    "modal-save-evenement", 
    "ModalSaveEvenementCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
?>
    <div class="modal-header">
        <div class="modal-title"><?php echo $ctrl->getTitle(); ?></div>
        <div class="loader loader-modal" for="form-save-evenement"></div>
    </div>
    <div class="modal-body">
        <div class="form-horizontal" id="form-save-evenement">
            <div class="form-group">
                <label for="form-save-evenement__input-type" class="col-xs-4 control-label required">Type :</label>
                <div class="col-xs-7 form-input" for="form-save-evenement__input-type">
                    <select class="form-control" id="form-save-evenement__input-type" required>
                        <?php echo $ctrl->getOptionsTypeEvenement(); ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-licencie__input-categories"></div>
            </div>
            <div class="form-group form-group-no-match">
                <label for="form-save-evenement__input-nom" class="col-xs-4 control-label">Nom :</label>
                <div class="col-xs-7 form-input" for="form-save-evenement__input-nom">
                    <input type="text" class="form-control" id="form-save-evenement__input-nom" value="<?php echo $ctrl->getNomEvenement(); ?>">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-evenement__input-nom"></div>
            </div>
            <div class="form-group">
                <label for="form-save-evenement__input-code-rencontre" class="col-xs-4 control-label">Code rencontre :</label>
                <div class="col-xs-7 form-input" for="form-save-evenement__input-code-rencontre">
                    <input type="text" class="form-control" id="form-save-evenement__input-code-rencontre" value="<?php echo $ctrl->getCodeEvenement(); ?>">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-evenement__input-code-rencontre"></div>
            </div>
            <div class="form-group">
                <label for="form-save-evenement__input-date" class="col-xs-4 control-label required">Date / Heure :</label>
                <div class="col-xs-3 form-input" for="form-save-evenement__input-date">
                    <div class="input-group">
                        <input type="text" class="form-control" id="form-save-evenement__input-date" value="<?php echo $ctrl->getDateEvenement(); ?>" pattern="^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$" pattern-indication="Le format doit &ecirc;tre 'jj/mm/aaaa'" required>
                        <span class="input-group-addon">
                            <img src="<?php echo SRC_PATH; ?>assets/img/icons/calendar.png">
                        </span>
                    </div>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-evenement__input-date"></div>
                <div class="col-xs-3 form-input" for="form-save-evenement__input-heure">
                    <select class="form-control" id="form-save-evenement__input-heure" required>
                        <option value=""></option>
                        <?php echo $ctrl->getOptionsHeureEvenement(); ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-evenement__input-heure"></div>
            </div>
            <div class="form-group form-group-match">
                <label for="form-save-evenement__input-categorie" class="col-xs-4 control-label">Cat&eacute;gorie :</label>
                <div class="col-xs-7 form-input" for="form-save-evenement__input-categorie">
                    <select class="form-control" id="form-save-evenement__input-categorie">
                        <option value=""></option>
                        <?php echo $ctrl->getOptionsCategorieEvenement(); ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-evenement__input-categorie"></div>
            </div>
            <div class="form-group form-group-match">
                <label for="form-save-evenement__input-num-equipe" class="col-xs-4 control-label">Num&eacute;ro d'&eacute;quipe :</label>
                <div class="col-xs-7 form-input" for="form-save-evenement__input-num-equipe">
                    <input type="hidden" id="form-save-evenement__input-hidden-num-equipe" value="<?php echo $ctrl->getNumEquipeCompetition(); ?>">
                    <select class="form-control" id="form-save-evenement__input-num-equipe">
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-evenement__input-num-equipe"></div>
            </div>
            <div class="form-group form-group-match">
                <label for="form-save-evenement__input-adversaire" class="col-xs-4 control-label">Adversaire :</label>
                <div class="col-xs-7 form-input" for="form-save-evenement__input-adversaire">
                    <input type="text" class="form-control" id="form-save-evenement__input-adversaire" value="<?php echo $ctrl->getAdversaireEvenement(); ?>">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-evenement__input-adversaire"></div>
            </div>
            <div class="form-group form-group-match">
                <label for="form-save-evenement__input-competition" class="col-xs-4 control-label">Comp&eacute;tition :</label>
                <div class="col-xs-7 form-input" for="form-save-evenement__input-competition">
                    <input type="hidden" id="form-save-evenement__input-hidden-competition" value="<?php echo $ctrl->getCompetitionEvenement(); ?>">
                    <select class="form-control" id="form-save-evenement__input-competition">
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-evenement__input-competition"></div>
            </div>
            <div class="form-group form-group-match">
                <label for="form-save-evenement__input-score-hbcsj" class="col-xs-4 control-label">Score :</label>
                <div class="col-xs-2 form-input" for="form-save-evenement__input-score-hbcsj">
                    <input type="text" class="form-control" id="form-save-evenement__input-score-hbcsj" value="<?php echo $ctrl->getScoreHBCSJEvenement(); ?>" placeholder="HBCSJ" pattern="^[0-9]{1}[0-9]*$" pattern-indication="Le score doit &ecirc;tre un nombre">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-evenement__input-score-hbcsj"></div>
                <div class="col-xs-2 form-input" for="form-save-evenement__input-score-adversaire">
                    <input type="text" class="form-control" id="form-save-evenement__input-score-adversaire" value="<?php echo $ctrl->getScoreAdversaireEvenement(); ?>" placeholder="Adversaire" pattern="^[0-9]{1}[0-9]*$" pattern-indication="Le score doit &ecirc;tre un nombre">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-evenement__input-score-adversaire"></div>
            </div>
            <div class="form-group form-group-no-match">
                <label for="form-save-evenement__input-categories" class="col-xs-4 control-label">Cat&eacute;gorie(s) :</label>
                <div class="col-xs-4 form-input" for="form-save-evenement__input-categories">
                    <select class="form-control" id="form-save-evenement__input-categories" multiple>
                        <?php echo $ctrl->getOptionsCategoriesEvenement(); ?>
                    </select>
                </div>
                <div class="col-xs-3 form-input">
                    <button id="modal-save-evenement__bt-select-all-categories" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-list-alt"></span>
                        </div>
                        <div class="button__text">Tout s&eacute;lectionner</div>
                        <div class="loader"></div>
                    </button>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-evenement__input-categories"></div>
            </div>
            <div class="form-group">
                <label for="form-save-evenement__input-a-domicile" class="col-xs-4 control-label">A domicile ?</label>
                <div class="col-xs-1 form-input" for="form-save-evenement__input-a-domicile">
                    <input type="checkbox" id="form-save-evenement__input-a-domicile"<?php echo $ctrl->getADomicileChecked(); ?>>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-evenement__input-a-domicile"></div>
                <label for="form-save-evenement__input-gymnase" class="col-xs-2 control-label">Gymnase :</label>
                <div class="col-xs-3 form-input" for="form-save-evenement__input-gymnase">
                    <select class="form-control" id="form-save-evenement__input-gymnase">
                        <option value=""></option>
                        <?php echo $ctrl->getOptionsGymnaseEvenement(); ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-evenement__input-gymnase"></div>
            </div>
            <div class="form-group">
                <label for="form-save-evenement__input-adresse" class="col-xs-4 control-label">Adresse :</label>
                <div class="col-xs-7 form-input" for="form-save-evenement__input-adresse">
                    <input type="text" class="form-control" id="form-save-evenement__input-adresse" value="<?php echo $ctrl->getAdresseEvenement(); ?>" placeholder="Adresse">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-evenement__input-adresse"></div>
            </div>
            <div class="form-group">
                <div class="col-xs-2 col-xs-offset-4 form-input" for="form-save-evenement__input-code-postal">
                    <input type="text" class="form-control" id="form-save-evenement__input-code-postal" value="<?php echo $ctrl->getCodePostalEvenement(); ?>" placeholder="Code postal">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-evenement__input-code-postal"></div>
                <div class="col-xs-4 form-input" for="form-save-evenement__input-ville">
                    <input type="text" class="form-control" id="form-save-evenement__input-ville" value="<?php echo $ctrl->getVilleEvenement(); ?>" placeholder="Ville">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-evenement__input-ville"></div>
            </div>
            <div class="form-group">
                <label for="form-save-evenement__input-latitude" class="col-xs-4 control-label">Latitude :</label>
                <div class="col-xs-7 form-input" for="form-save-evenement__input-latitude">
                    <input type="text" class="form-control" id="form-save-evenement__input-latitude" value="<?php echo $ctrl->getLatitudeEvenement(); ?>" pattern="^(-)?[0-9]+(\.)?[0-9]*$" pattern-indication="La latitude doit &ecirc;tre un nombre d&eacute;cimal">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-evenement__input-latitude"></div>
            </div>
            <div class="form-group">
                <label for="form-save-evenement__input-longitude" class="col-xs-4 control-label">Longitude :</label>
                <div class="col-xs-7 form-input" for="form-save-evenement__input-longitude">
                    <input type="text" class="form-control" id="form-save-evenement__input-longitude" value="<?php echo $ctrl->getLongitudeEvenement(); ?>" pattern="^(-)?[0-9]+(\.)?[0-9]*$" pattern-indication="La longitude doit &ecirc;tre un nombre d&eacute;cimal">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-evenement__input-longitude"></div>
            </div>
            <div class="form-group">
                <label for="form-save-evenement__input-visible" class="col-xs-4 control-label">Visible ?</label>
                <div class="col-xs-1 form-input" for="form-save-evenement__input-visible">
                    <?php
                    $disabledVisible = "";
                    if (!isAdminConnected_bureau() && !isAdminConnected_coach()) {
                        $disabledVisible = " disabled";
                    }
                    ?>
                    <input type="checkbox" id="form-save-evenement__input-visible"<?php echo $ctrl->getVisibleChecked(); ?><?php echo $disabledVisible; ?>>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-evenement__input-visible"></div>
                <label for="form-save-evenement__input-email-dernier-editeur" class="col-xs-1 control-label required">&nbsp;</label>
                <div class="col-xs-4 form-input" for="form-save-evenement__input-email-dernier-editeur">
                    <?php
                    $disabledEmailDernierEditeur = "";
                    if (isAdminConnected_superadmin()) {
                        $disabledEmailDernierEditeur = " disabled";
                    }
                    ?>
                    <input type="text" class="form-control" id="form-save-evenement__input-email-dernier-editeur" value="<?php echo $ctrl->getEmailDernierEditeurEvenement(); ?>" required <?php echo $disabledEmailDernierEditeur; ?> placeholder="Email dernier &eacute;diteur" pattern="^[A-Za-z0-9._-]+@[A-Za-z0-9.-]{2,}[.][A-Za-z]{2,3}$" pattern-indication="Le format de l'email est invalide">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-evenement__input-email-dernier-editeur"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-required text-right">Les champs marqu&eacute;s d'une * sont obligatoires</div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-8">
                <div class="form-result-message"></div>
            </div>
            <div class="col-xs-4 text-right">
                <button id="modal-save-evenement__bt-sauvegarder" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-ok"></span>
                    </div>
                    <div class="button__text">Sauvegarder</div>
                    <div class="loader"></div>
                </button>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>