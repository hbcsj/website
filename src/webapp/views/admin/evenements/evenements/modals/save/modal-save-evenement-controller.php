<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/dao/evenement-dao.php");
require_once("common/php/dao/type-evenement-dao.php");
require_once("common/php/dao/categorie-dao.php");
require_once("common/php/dao/gymnase-dao.php");
require_once("common/php/managers/evenement-manager.php");

class ModalSaveEvenementCtrl extends AbstractViewCtrl {
	
	private $evenement;

	private $evenementDAO;
	private $typeEvenementDAO;
	private $categorieDAO;
	private $gymnaseDAO;

	private $evenementManager;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idEvenement" => $_GET["idEvenement"]
		), true);
		
		if (isAdminConnected()) {
			$this->evenementDAO = new EvenementDAO($this->getDatabaseConnection());
			$this->typeEvenementDAO = new TypeEvenementDAO($this->getDatabaseConnection());
			$this->categorieDAO = new CategorieDAO($this->getDatabaseConnection());
			$this->gymnaseDAO = new GymnaseDAO($this->getDatabaseConnection());

			$this->evenementManager = new EvenementManager($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdEvenement();
	}

	private function checkIdEvenement() {
		if (HTTPUtils::paramExists(GET, "idEvenement")) {
			$this->evenement = $this->evenementDAO->getById($_GET["idEvenement"]);
				
			if ($this->evenement == null) {
				$this->sendCheckError(
					HTTP_404, 
					"L'evenement (idEvenement = '".$_GET["idEvenement"]."') n'existe pas", 
					"webapp/views/common/error/404/404.html.php"
				);
			}
		}
	}

	public function getTitle() {
		$title = "Nouvel &eacute;v&eacute;nement";
		if ($this->evenement != null) {
			if ($this->evenement[EVENEMENT_TYPE_EVENEMENT_ID] == EVENEMENT_MATCH_TYPE_ID) {
				$title = "Match contre ".$this->evenement[EVENEMENT_ADVERSAIRE]." &agrave; ".$this->evenement[EVENEMENT_VILLE];
			} else {
				$type = $this->typeEvenementDAO->getById($this->evenement[EVENEMENT_TYPE_EVENEMENT_ID]);
				$title = $type[TYPE_EVENEMENT_LIBELLE]." '".$this->evenement[EVENEMENT_NOM]."' &agrave; ".$this->evenement[EVENEMENT_VILLE];
			}
		}
		return $title;
	}

	public function getOptionsTypeEvenement() {
		$html = "";

		$types = $this->typeEvenementDAO->getAll(TYPE_EVENEMENT_TABLE_NAME.".".TYPE_EVENEMENT_ID);
		if (sizeof($types) > 0) {
			foreach ($types as $type) {
				$selected = "";
				if ($this->evenement[EVENEMENT_TYPE_EVENEMENT_ID] == $type[TYPE_EVENEMENT_ID]) {
					$selected = " selected";
				}
				$html .= "<option value=\"".$type[TYPE_EVENEMENT_ID]."\"".$selected.">".$type[TYPE_EVENEMENT_LIBELLE]."</option>";
			}
		}

		return $html;
	}

	public function getCodeEvenement() {
		if ($this->evenement != null) {
			return $this->evenement[EVENEMENT_CODE];
		}
		return "";
	}
	
	public function getNomEvenement() {
		if ($this->evenement != null) {
			return $this->evenement[EVENEMENT_NOM];
		}
		return "";
	}

	public function getDateEvenement() {
		if ($this->evenement != null) {
			return date(SLASH_DATE_FORMAT, DateUtils::get_sqlDateTime_timestamp($this->evenement[EVENEMENT_DATE_HEURE]));
		}
		return "";
	}

	public function getOptionsHeureEvenement() {
		$html = "";

		for ($h = 0 ; $h <= 23 ; $h++) {
			$heure = $h;
			if ($heure < 10) {
				$heure = "0".$heure;
			}

			for ($m = 0 ; $m <= 59 ; $m += 15) {
				$minutes = $m;
				if ($minutes < 10) {
					$minutes = "0".$minutes;
				}

				$selected = "";
				if ($this->evenement != null && date(SLASH_TIME_FORMAT, DateUtils::get_sqlDateTime_timestamp($this->evenement[EVENEMENT_DATE_HEURE])) == $heure.SLASH_TIME_SEPARATOR.$minutes) {
					$selected = " selected";
				}
				$html .= "<option value=\"".$heure.SLASH_TIME_SEPARATOR.$minutes."\"".$selected.">".$heure.SLASH_TIME_SEPARATOR.$minutes."</option>";
			}
		}

		return $html;
	}

	public function getOptionsCategorieEvenement() {
		$html = "";

		$categorieEvenement = $this->evenementManager->getCategorieMatch($this->evenement[EVENEMENT_ID]);

		$categories = $this->categorieDAO->getAll(CATEGORIE_TABLE_NAME.".".CATEGORIE_POSITION_AFFICHAGE);
		if (sizeof($categories) > 0) {
			foreach ($categories as $categorie) {
				$selected = "";
				if ($categorie[CATEGORIE_ID] == $categorieEvenement[CATEGORIE_ID]) {
					$selected = " selected";
				}
				$html .= "<option value=\"".$categorie[CATEGORIE_ID]."\"".$selected.">".$categorie[CATEGORIE_NOM]."</option>";
			}
		}

		return $html;
	}

	public function getNumEquipeCompetition() {
		if ($this->evenement != null) {
			return $this->evenement[EVENEMENT_NUM_EQUIPE];
		}
		return "";
	}

	public function getAdversaireEvenement() {
		if ($this->evenement != null) {
			return $this->evenement[EVENEMENT_ADVERSAIRE];
		}
		return "";
	}

	public function getScoreHBCSJEvenement() {
		if ($this->evenement != null) {
			return $this->evenement[EVENEMENT_SCORE_HBCSJ];
		}
		return "";
	}

	public function getScoreAdversaireEvenement() {
		if ($this->evenement != null) {
			return $this->evenement[EVENEMENT_SCORE_ADVERSAIRE];
		}
		return "";
	}

	public function getCompetitionEvenement() {
		if ($this->evenement != null) {
			return $this->evenement[EVENEMENT_COMPETITION_ID];
		}
		return "";
	}

	public function getOptionsCategoriesEvenement() {
		$html = "";

		$categoriesEvenement = $this->evenementManager->getCategoriesEvenement($this->evenement[EVENEMENT_ID]);
		$categorieIdsEvenement = array();
		if (sizeof($categoriesEvenement) > 0) {
			foreach ($categoriesEvenement as $categorieEvenement) {
				$categorieIdsEvenement[] = $categorieEvenement[CATEGORIE_ID];
			}
		}

		$categories = $this->categorieDAO->getAll(CATEGORIE_TABLE_NAME.".".CATEGORIE_POSITION_AFFICHAGE);
		if (sizeof($categories) > 0) {
			foreach ($categories as $categorie) {
				$selected = "";
				if (in_array($categorie[CATEGORIE_ID], $categorieIdsEvenement)) {
					$selected = " selected";
				}
				$html .= "<option value=\"".$categorie[CATEGORIE_ID]."\"".$selected.">".$categorie[CATEGORIE_NOM]."</option>";
			}
		}

		return $html;
	}

	public function getOptionsGymnaseEvenement() {
		$html = "";

		$gymnases = $this->gymnaseDAO->getAll(GYMNASE_TABLE_NAME.".".GYMNASE_VILLE);
		if (sizeof($gymnases) > 0) {
			foreach ($gymnases as $gymnase) {
				$selected = "";
				if ($this->evenement[EVENEMENT_GYMNASE_ID] == $gymnase[GYMNASE_ID]) {
					$selected = " selected";
				}
				$html .= "<option value=\"".$gymnase[GYMNASE_ID]."\"".$selected." a-domicile=\"".$gymnase[GYMNASE_A_DOMICILE]."\">".$gymnase[GYMNASE_VILLE]." - ".$gymnase[GYMNASE_NOM]."</option>";
			}
		}

		return $html;
	}

	public function getAdresseEvenement() {
		if ($this->evenement != null) {
			return $this->evenement[EVENEMENT_ADRESSE];
		}
		return "";
	}

	public function getCodePostalEvenement() {
		if ($this->evenement != null) {
			return $this->evenement[EVENEMENT_CODE_POSTAL];
		}
		return "";
	}

	public function getVilleEvenement() {
		if ($this->evenement != null) {
			return $this->evenement[EVENEMENT_VILLE];
		}
		return "";
	}

	public function getLatitudeEvenement() {
		if ($this->evenement != null) {
			return $this->evenement[EVENEMENT_LATITUDE];
		}
		return "";
	}

	public function getLongitudeEvenement() {
		if ($this->evenement != null) {
			return $this->evenement[EVENEMENT_LONGITUDE];
		}
		return "";
	}

	public function getADomicileChecked() {
		$checked = "";
        if ($this->evenement != null && $this->evenement[EVENEMENT_A_DOMICILE]) {
            $checked = " checked=\"checked\"";
        }
        return $checked;
	}

	public function getVisibleChecked() {
		$checked = "";
		if (
			($this->evenement == null && isAdminConnected_bureau()) || 
			($this->evenement != null && isAdminConnected_bureau() && $this->evenement[EVENEMENT_VISIBLE_SUR_SITE])
		) {
			$checked = " checked=\"checked\"";
		}
        return $checked;
	}

	public function getEmailDernierEditeurEvenement() {
		if (isAdminConnected_superadmin()) {
			if ($this->evenement != null) {
				return $this->evenement[EVENEMENT_EMAIL_DERNIER_EDITEUR];
			} else {
				return $GLOBALS[CONF][SUPERADMIN_EMAIL];
			}
		} 
		return "";
	}
}

?>