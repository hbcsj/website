<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/evenement-dao.php");
require_once("common/php/dao/type-evenement-dao.php");
require_once("common/php/dao/licencie-dao.php");
require_once("common/php/dao/action-club-dao.php");
require_once("common/php/dao/type-action-club-dao.php");

class ModalActionsClubEvenementCtrl extends AbstractViewCtrl {
	
	private $evenement;

	private $licencieDAO;
	private $evenementDAO;
	private $typeEvenementDAO;
	private $actionClubDAO;
	private $typeActionClubDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idEvenement" => $_GET["idEvenement"]
		), true);
		
		if (isAdminConnected_coach()) {
			$this->licencieDAO = new LicencieDAO($this->getDatabaseConnection());
			$this->evenementDAO = new EvenementDAO($this->getDatabaseConnection());
			$this->typeEvenementDAO = new TypeEvenementDAO($this->getDatabaseConnection());
			$this->actionClubDAO = new ActionClubDAO($this->getDatabaseConnection());
			$this->typeActionClubDAO = new TypeActionClubDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$checkParams = $this->checkParams(array(
			array(GET, "idEvenement")
		));
		if ($checkParams) {
			$this->checkIdEvenement();
		}
	}

	private function checkIdEvenement() {
		$this->evenement = $this->evenementDAO->getById($_GET["idEvenement"]);
			
		if ($this->evenement == null) {
			$this->sendCheckError(
				HTTP_404, 
				"L'evenement (idEvenement = '".$_GET["idEvenement"]."') n'existe pas", 
				"webapp/views/common/error/404/404.html.php"
			);
		}
	}

	public function getEvenement() {
		return $this->evenement;
	}

	public function getLicencies() {
		return $this->licencieDAO->getAllByParams(
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			1,
			null,
			LICENCIE_TABLE_NAME.".".LICENCIE_NOM.", ".LICENCIE_TABLE_NAME.".".LICENCIE_PRENOM
		);
	}

	public function getLicencie($licencieId) {
		return $this->licencieDAO->getById($licencieId);
	}

	public function getActionsClub() {
		return $this->actionClubDAO->getByEvenementId($this->evenement[EVENEMENT_ID]);
	}

	public function getLibelleTypeEvenement($typeEvenementId) {
		$typeEvenement = $this->typeEvenementDAO->getById($typeEvenementId);
		return $typeEvenement[TYPE_EVENEMENT_LIBELLE];
	}

	public function getTypesActionsClub() {
		return $this->typeActionClubDAO->getAll();
	}

	public function getTypeActionClub($typeActionClubId) {
		return $this->typeActionClubDAO->getById($typeActionClubId);
	}
}

?>