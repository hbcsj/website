<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/lib/date-utils.php");

$view = new View(
    "webapp/views/admin/evenements/evenements/modals/historique-dispos/", 
    "modal-historique-dispos-evenement", 
    "ModalHistoriqueDisposEvenementCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
    $historiqueDispos = $ctrl->getHistoriqueDispos();
    $evenement = $ctrl->getEvenement();
    ?>
    <div class="modal-header">
        <div class="modal-title">
            <?php
            if ($evenement[EVENEMENT_TYPE_EVENEMENT_ID] == EVENEMENT_MATCH_TYPE_ID) {
				echo "Match contre ".$evenement[EVENEMENT_ADVERSAIRE];
			} else {
				echo $ctrl->getLibelleTypeEvenement($evenement[EVENEMENT_TYPE_EVENEMENT_ID])." '".$evenement[EVENEMENT_NOM]."' &agrave; ".$evenement[EVENEMENT_VILLE];
			}
            ?> - 
            <span class="normal">Historique des dispos</span>
        </div>
    </div>
    <div class="modal-body">
        <table class="table table-condensed">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Nom</th>
                    <th>Pr&eacute;nom</th>
                    <th class="text-center">R&eacute;ponse</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (sizeof($historiqueDispos) > 0) {
                    foreach ($historiqueDispos as $saisieDispo) {
                        $licencie = $ctrl->getLicencie($saisieDispo[HISTO_DISPO_LICENCIE_ID]);
                        $reponse = $ctrl->getLibelleReponse($saisieDispo[HISTO_DISPO_REPONSE_DISPO_ID]);
                        ?>
                        <tr>
                            <td><?php echo str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDateTime_to_slashDateTime($saisieDispo[HISTO_DISPO_DATE_HEURE])); ?></td>
                            <td><?php echo $licencie[LICENCIE_NOM]; ?></td>
                            <td><?php echo $licencie[LICENCIE_PRENOM]; ?></td>
                            <td class="text-center"><?php echo $reponse; ?></td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="modal-footer"></div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>