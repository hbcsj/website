<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/evenement-dao.php");
require_once("common/php/dao/dispo-dao.php");
require_once("common/php/dao/histo-dispo-dao.php");
require_once("common/php/dao/licencie-dao.php");
require_once("common/php/dao/type-evenement-dao.php");

class ModalHistoriqueDisposEvenementCtrl extends AbstractViewCtrl {
	
	private $evenement;

	private $licencieDAO;
	private $evenementDAO;
	private $histoDispoDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idEvenement" => $_GET["idEvenement"]
		), true);
		
		if (isAdminConnected_coach()) {
			$this->licencieDAO = new LicencieDAO($this->getDatabaseConnection());
			$this->evenementDAO = new EvenementDAO($this->getDatabaseConnection());
			$this->histoDispoDAO = new HistoDispoDAO($this->getDatabaseConnection());
			$this->typeEvenementDAO = new TypeEvenementDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$checkParams = $this->checkParams(array(
			array(GET, "idEvenement")
		));
		if ($checkParams) {
			$this->checkIdEvenement();
		}
	}

	private function checkIdEvenement() {
		$this->evenement = $this->evenementDAO->getById($_GET["idEvenement"]);
			
		if ($this->evenement == null) {
			$this->sendCheckError(
				HTTP_404, 
				"L'evenement (idEvenement = '".$_GET["idEvenement"]."') n'existe pas", 
				"webapp/views/common/error/404/404.html.php"
			);
		}
	}

	public function getEvenement() {
		return $this->evenement;
	}

	public function getLicencie($licencieId) {
		return $this->licencieDAO->getById($licencieId);
	}

	public function getHistoriqueDispos() {
		return $this->histoDispoDAO->getByEvenementIdAndTypeDispoId($this->evenement[EVENEMENT_ID], DISPO_PARTICIPATION_TYPE_ID);
	}

	public function getLibelleTypeEvenement($typeEvenementId) {
		$typeEvenement = $this->typeEvenementDAO->getById($typeEvenementId);
		return $typeEvenement[TYPE_EVENEMENT_LIBELLE];
    }
    
    public function getLibelleReponse($reponseDispoId) {
        $libelle = "";

        if ($reponseDispoId == DISPO_DISPONIBLE_REPONSE_ID) {
            $libelle = "<span class=\"badge badge-dispo badge-disponible\">Disponible</span>";
        } else if ($reponseDispoId == DISPO_SI_NECESSAIRE_REPONSE_ID) {
            $libelle = "<span class=\"badge badge-dispo badge-si-necessaire\">Si n&eacute;cessaire";
        } else if ($reponseDispoId == DISPO_ABSENT_REPONSE_ID) {
            $libelle = "<span class=\"badge badge-dispo badge-absent\">Absent";
        } else {
            $libelle = "<span class=\"badge badge-dispo badge-pas-de-reponse\">Pas de r&eacute;ponse";
        }

        return $libelle;
    }
}

?>