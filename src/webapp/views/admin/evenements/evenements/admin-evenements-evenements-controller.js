AdminEvenementsEvenementsController = {};

AdminEvenementsEvenementsController.idTypeEvenementMatch = 1;

AdminEvenementsEvenementsController.dateDebutRecherche = null;

AdminEvenementsEvenementsController.load_StatsEvenements = function() {
    if (Utils.exists('#container-stats-evenements')) {
        AjaxUtils.loadView(
            'admin/evenements/evenements/views/stats/stats-evenements',
            '#container-stats-evenements',
            null,
            function() {},
            '#loader-page'
        );
    }
};

AdminEvenementsEvenementsController.load_RechercheAndTableEvenements = function() {
    if (Utils.exists('#container-recherche-evenements')) {
        AjaxUtils.loadView(
            'admin/evenements/evenements/views/recherche/recherche-evenements',
            '#container-recherche-evenements',
            null,
            function() {
                $('#form-recherche-evenements__input-date-debut').val(AdminEvenementsEvenementsController.dateDebutRecherche);

                FormRechercheUtils.control_toggleFormRecherche();
                AdminEvenementsEvenementsController.setCompetitionsInSelect(
                    '',
                    'form-recherche-evenements__input-competition',
                    null,
                    '#loader-page'
                );
                control_Categorie();
                onClick_btRechercher();
                onClick_btReset();
                AdminEvenementsEvenementsController.load_TableEvenements();
            },
            '#loader-page'
        );
    }

    function control_Categorie() {
        $('#form-recherche-evenements__input-categorie').unbind('change');
        $('#form-recherche-evenements__input-categorie').change(function() {
            var idCategorie = $(this).val();
            AdminEvenementsEvenementsController.setCompetitionsInSelect(
                idCategorie,
                'form-recherche-evenements__input-competition',
                null,
                '#loader-page'
            );
        });
    }

    function onClick_btRechercher() {
        $('#recherche-evenements__bt-rechercher').unbind('click');
        $('#recherche-evenements__bt-rechercher').click(function() {
            AdminEvenementsEvenementsController.load_TableEvenements();
        });
    }

    function onClick_btReset() {
        $('#recherche-evenements__bt-reset').unbind('click');
        $('#recherche-evenements__bt-reset').click(function() {
            $('#form-recherche-evenements__input-date-debut').val(AdminEvenementsEvenementsController.dateDebutRecherche);
            $('#form-recherche-evenements__input-date-fin').val('');
            $('#form-recherche-evenements__input-categorie').val('');
            $('#form-recherche-evenements__input-num-equipe').val('');
            $('#form-recherche-evenements__input-type').val('');
            AdminEvenementsEvenementsController.setCompetitionsInSelect(
                '',
                'form-recherche-evenements__input-competition',
                null,
                '#loader-page'
            );
            $('#form-recherche-evenements__input-adversaire').val('');
            $('#form-recherche-evenements__input-ville').val('');
            $('#form-recherche-evenements__input-is-a-domicile').val('');

            AdminEvenementsEvenementsController.load_TableEvenements();
        });
    }
};

AdminEvenementsEvenementsController.load_TableEvenements = function() {
    if (Utils.exists('#container-table-evenements')) {
        var params = {
            dateDebut: $.trim($('#form-recherche-evenements__input-date-debut').val()),
            dateFin: $.trim($('#form-recherche-evenements__input-date-fin').val()),
            categorieId: $('#form-recherche-evenements__input-categorie').val(),
            numEquipe: $('#form-recherche-evenements__input-num-equipe').val(),
            type: $('#form-recherche-evenements__input-type').val(),
            competitionId: $('#form-recherche-evenements__input-competition').val(),
            adversaire: $.trim($('#form-recherche-evenements__input-adversaire').val()),
            ville: $.trim($('#form-recherche-evenements__input-ville').val()),
            aDomicile: $('#form-recherche-evenements__input-is-a-domicile').val()
        };
        AjaxUtils.loadView(
            'admin/evenements/evenements/views/table/table-evenements',
            '#container-table-evenements',
            params,
            function() {
                control_FDM();
                control_DeleteFDM();
                control_HistoriqueDispos();
                control_ActionsClub();
                control_Arbitrages();
                control_AddEvenement();
                control_EditEvenement();
                control_DeleteEvenement();
                control_ValidationEvenement();
            },
            '#loader-page'
        );
    }

    function control_FDM() {
        $('.glyphicon-upload').unbind('click');
        $('.glyphicon-upload').click(function() {
            var idEvenement = $(this).attr('id-evenement');
            open_ModalFDM(idEvenement);
        });

        function open_ModalFDM(idEvenement) {
            var params = {
                idEvenement: idEvenement
            };
            AjaxUtils.loadView(
                'admin/evenements/evenements/modals/fdm-match/modal-fdm-match-content',
                '#modal-fdm-match-content',
                params,
                function() {
                    onClick_btSauvegarder(idEvenement);
                },
                '#loader-page'
            );
            $('#modal-fdm-match').modal('show');

            function onClick_btSauvegarder(idEvenement) {
                $('#modal-fdm-match__bt-sauvegarder').unbind('click');
                $('#modal-fdm-match__bt-sauvegarder').click(function() {
                    FormUtils.hideFormErrors('#modal-fdm-match #form-fdm-match');
                    ViewUtils.unsetHTMLAndHide('#modal-fdm-match .form-result-message');
                    ViewUtils.desactiveButtonAndShowLoader('#modal-fdm-match__bt-sauvegarder', '#modal-fdm-match__bt-sauvegarder .loader');

                    var formValidation = FormUtils.validateForm('#modal-fdm-match #form-fdm-match');

                    if (formValidation) {
                        document.getElementById('form-fdm-match').submit();
                    } else {
                        ViewUtils.activeButtonAndHideLoader('#modal-fdm-match__bt-sauvegarder', '#modal-fdm-match__bt-sauvegarder .loader');
                    }
                });
            }
        }
    }

    function control_DeleteFDM() {
        $('.glyphicon-remove-circle').unbind('click');
        $('.glyphicon-remove-circle').click(function() {
            var idEvenement = $(this).attr('id-evenement');

            $('.loader-delete[id-evenement=' + idEvenement + ']').show();
            AjaxUtils.callService(
                'evenement',
                'deleteFDM',
                AjaxUtils.dataTypes.json,
                AjaxUtils.requestTypes.post,
                null, {
                    idEvenement: idEvenement
                },
                function(response) {
                    $('.loader-delete[id-evenement=' + idEvenement + ']').hide();
                    if (response.success) {
                        AdminEvenementsEvenementsController.load_TableEvenements();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la suppression de l\'&eacute;v&eacute;nement',
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-delete[id-evenement=' + idEvenement + ']').hide();
                    ViewUtils.alert(
                        'Service indisponible',
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        });
    }

    function control_HistoriqueDispos() {
        $('.bt-evenement-historique-dispos').unbind('click');
        $('.bt-evenement-historique-dispos').click(function() {
            var idEvenement = $(this).attr('id-evenement');
            open_ModalHistoriqueDispos(idEvenement);
        });

        function open_ModalHistoriqueDispos(idEvenement) {
            load_ModalHistoriqueDispos(idEvenement);
            $('#modal-historique-dispos-evenement').modal('show');
        }

        function load_ModalHistoriqueDispos(idEvenement) {
            var params = {
                idEvenement: idEvenement
            };
            AjaxUtils.loadView(
                'admin/evenements/evenements/modals/historique-dispos/modal-historique-dispos-evenement-content',
                '#modal-historique-dispos-evenement-content',
                params,
                function() {},
                '#loader-page'
            );
        }
    }

    function control_ActionsClub() {
        $('.bt-evenement-actions-club').unbind('click');
        $('.bt-evenement-actions-club').click(function() {
            var idEvenement = $(this).attr('id-evenement');
            open_ModalActionsClub(idEvenement);
        });

        function open_ModalActionsClub(idEvenement) {
            load_ModalActionsClub(idEvenement);
            $('#modal-actions-club-evenement').modal('show');
        }

        function load_ModalActionsClub(idEvenement) {
            var params = {
                idEvenement: idEvenement
            };
            AjaxUtils.loadView(
                'admin/evenements/evenements/modals/actions-club/modal-actions-club-evenement-content',
                '#modal-actions-club-evenement-content',
                params,
                function() {
                    onClick_btSupprimer(idEvenement);
                    onClick_btAjouter(idEvenement);
                },
                '#loader-page'
            );
        }

        function onClick_btSupprimer(idEvenement) {
            $('.bt-supprimer-action-club').unbind('click');
            $('.bt-supprimer-action-club').click(function() {
                var idLicencie = $(this).attr('id-licencie');
                var idTypeActionClub = $(this).attr('id-type-action-club');

                FormUtils.hideFormErrors('#modal-actions-club-evenement #form-actions-club-evenement');
                ViewUtils.unsetHTMLAndHide('#modal-actions-club-evenement .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('.bt-supprimer-action-club[id-licencie=' + idLicencie + ']', '.loader-modal[for=form-actions-club-evenement]');
                AjaxUtils.callService(
                    'evenement',
                    'deleteActionClub',
                    AjaxUtils.dataTypes.json,
                    AjaxUtils.requestTypes.post,
                    null, {
                        evenementId: idEvenement,
                        licencieId: idLicencie,
                        typeActionClubId: idTypeActionClub
                    },
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-actions-club-evenement .form-result-message', 'Action club supprim&eacute;e', FormUtils.resultsTypes.ok);
                            $('.loader-modal[for=form-actions-club-evenement]').hide();
                            AdminEvenementsEvenementsController.load_StatsEvenements();
                            AdminEvenementsEvenementsController.load_TableEvenements();
                            load_ModalActionsClub(idEvenement);
                        } else {
                            ViewUtils.activeButtonAndHideLoader('#.bt-supprimer-action-club[id-licencie=' + idLicencie + ']', '.loader-modal[for=form-actions-club-evenement]');
                            FormUtils.displayFormResultMessage('#modal-actions-club-evenement .form-result-message', 'Erreur durant la suppression', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('.bt-supprimer-action-club[id-licencie=' + idLicencie + ']', '.loader-modal[for=form-actions-club-evenement]');
                        FormUtils.displayFormResultMessage('#modal-actions-club-evenement .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            });
        }

        function onClick_btAjouter(idEvenement) {
            $('#modal-actions-club-evenement__bt-ajouter').unbind('click');
            $('#modal-actions-club-evenement__bt-ajouter').click(function() {
                FormUtils.hideFormErrors('#modal-actions-club-evenement #form-add-actions-club-evenement');
                ViewUtils.unsetHTMLAndHide('#modal-actions-club-evenement .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('#modal-actions-club-evenement__bt-ajouter', '.loader-modal[for=form-actions-club-evenement]');

                var formValidation = FormUtils.validateForm('#modal-actions-club-evenement #form-add-action-club-evenement');

                if (formValidation) {
                    var inputLicencie = $.trim($('#form-add-action-club-evenement__input-licencie').val());
                    var inputType = $.trim($('#form-add-action-club-evenement__input-type').val());

                    AjaxUtils.callService(
                        'evenement',
                        'actionClubExists',
                        AjaxUtils.dataTypes.json,
                        AjaxUtils.requestTypes.get, {
                            evenementId: idEvenement,
                            licencieId: inputLicencie,
                            typeActionClubId: inputType
                        },
                        null,
                        function(response) {
                            if (response.actionClubExists) {
                                ViewUtils.activeButtonAndHideLoader('#modal-actions-club-evenement__bt-ajouter', '.loader-modal[for=form-actions-club-evenement]');
                                FormUtils.displayFormResultMessage('#modal-actions-club-evenement .form-result-message', 'Cette action club existe d&eacute;j&agrave;', FormUtils.resultsTypes.warning);
                            } else {
                                addActionClub(idEvenement, inputLicencie, inputType);
                            }
                        },
                        function(response) {
                            ViewUtils.activeButtonAndHideLoader('#modal-actions-club-evenement__bt-ajouter', '.loader-modal[for=form-actions-club-evenement]');
                            FormUtils.displayFormResultMessage('#modal-actions-club-evenement .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                        }
                    );
                } else {
                    ViewUtils.activeButtonAndHideLoader('#modal-actions-club-evenement__bt-ajouter', '.loader-modal[for=form-actions-club-evenement]');
                }
            });

            function addActionClub(idEvenement, inputLicencie, inputType) {
                AjaxUtils.callService(
                    'evenement',
                    'addActionClub',
                    AjaxUtils.dataTypes.json,
                    AjaxUtils.requestTypes.post, {
                        evenementId: idEvenement
                    }, {
                        licencieId: inputLicencie,
                        typeActionClubId: inputType
                    },
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-actions-club-evenement .form-result-message', 'Action club ajout&eacute;e', FormUtils.resultsTypes.ok);
                            $('.loader-modal[for=form-actions-club-evenement]').hide();
                            AdminEvenementsEvenementsController.load_StatsEvenements();
                            AdminEvenementsEvenementsController.load_TableEvenements();
                            load_ModalActionsClub(idEvenement);
                        } else {
                            ViewUtils.activeButtonAndHideLoader('#modal-actions-club-evenement__bt-ajouter', '.loader-modal[for=form-actions-club-evenement]');
                            FormUtils.displayFormResultMessage('#modal-actions-club-evenement .form-result-message', 'Erreur durant l\'ajout', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('#modal-actions-club-evenement__bt-ajouter', '.loader-modal[for=form-actions-club-evenement]');
                        FormUtils.displayFormResultMessage('#modal-actions-club-evenement .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            }
        }
    }

    function control_Arbitrages() {
        $('.bt-evenement-arbitrages').unbind('click');
        $('.bt-evenement-arbitrages').click(function() {
            var idEvenement = $(this).attr('id-evenement');
            open_ModalArbitrages(idEvenement)
        });

        function open_ModalArbitrages(idEvenement) {
            load_ModalArbitrages(idEvenement);
            $('#modal-arbitrages-evenement').modal('show');
        }

        function load_ModalArbitrages(idEvenement) {
            var params = {
                idEvenement: idEvenement
            };
            AjaxUtils.loadView(
                'admin/evenements/evenements/modals/arbitrages/modal-arbitrages-evenement-content',
                '#modal-arbitrages-evenement-content',
                params,
                function() {
                    onClick_btSupprimer(idEvenement);
                    onClick_btAjouter(idEvenement);
                },
                '#loader-page'
            );
        }

        function onClick_btSupprimer(idEvenement) {
            $('.bt-supprimer-arbitrage').unbind('click');
            $('.bt-supprimer-arbitrage').click(function() {
                var idLicencie = $(this).attr('id-licencie');

                FormUtils.hideFormErrors('#modal-arbitrages-evenement #form-arbitrages-evenement');
                ViewUtils.unsetHTMLAndHide('#modal-arbitrages-evenement .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('.bt-supprimer-arbitrage[id-licencie=' + idLicencie + ']', '.loader-modal[for=form-arbitrages-evenement]');
                AjaxUtils.callService(
                    'evenement',
                    'deleteArbitrage',
                    AjaxUtils.dataTypes.json,
                    AjaxUtils.requestTypes.post,
                    null, {
                        evenementId: idEvenement,
                        licencieId: idLicencie
                    },
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-arbitrage-evenement .form-result-message', 'Arbitrage supprim&eacute;', FormUtils.resultsTypes.ok);
                            $('.loader-modal[for=form-arbitrages-evenement]').hide();
                            AdminEvenementsEvenementsController.load_StatsEvenements();
                            AdminEvenementsEvenementsController.load_TableEvenements();
                            load_ModalArbitrages(idEvenement);
                        } else {
                            ViewUtils.activeButtonAndHideLoader('#.bt-supprimer-arbitrage[id-licencie=' + idLicencie + ']', '.loader-modal[for=form-arbitrages-evenement]');
                            FormUtils.displayFormResultMessage('#modal-arbitrages-evenement .form-result-message', 'Erreur durant la suppression', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('.bt-supprimer-arbitrage[id-licencie=' + idLicencie + ']', '.loader-modal[for=form-arbitrages-evenement]');
                        FormUtils.displayFormResultMessage('#modal-arbitrages-evenement .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            });
        }

        function onClick_btAjouter(idEvenement) {
            $('#modal-arbitrages-evenement__bt-ajouter').unbind('click');
            $('#modal-arbitrages-evenement__bt-ajouter').click(function() {
                FormUtils.hideFormErrors('#modal-arbitrages-evenement #form-add-arbitrages-evenement');
                ViewUtils.unsetHTMLAndHide('#modal-arbitrages-evenement .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('#modal-arbitrage-evenement__bt-ajouter', '.loader-modal[for=form-arbitrages-evenement]');

                var formValidation = FormUtils.validateForm('#modal-arbitrages-evenement #form-add-arbitrage-evenement');

                if (formValidation) {
                    var inputLicencie = $.trim($('#form-add-arbitrage-evenement__input-licencie').val());

                    addArbitrage(idEvenement, inputLicencie);
                } else {
                    ViewUtils.activeButtonAndHideLoader('#modal-arbitrages-evenement__bt-ajouter', '.loader-modal[for=form-arbitrages-evenement]');
                }
            });

            function addArbitrage(idEvenement, inputLicencie) {
                AjaxUtils.callService(
                    'evenement',
                    'addArbitrage',
                    AjaxUtils.dataTypes.json,
                    AjaxUtils.requestTypes.post, {
                        evenementId: idEvenement
                    }, {
                        licencieId: inputLicencie
                    },
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-arbitrages-evenement .form-result-message', 'Arbitrage ajout&eacute;', FormUtils.resultsTypes.ok);
                            $('.loader-modal[for=form-arbitrages-evenement]').hide();
                            AdminEvenementsEvenementsController.load_StatsEvenements();
                            AdminEvenementsEvenementsController.load_TableEvenements();
                            load_ModalArbitrages(idEvenement);
                        } else {
                            ViewUtils.activeButtonAndHideLoader('#modal-arbitrages-evenement__bt-ajouter', '.loader-modal[for=form-arbitrages-evenement]');
                            FormUtils.displayFormResultMessage('#modal-arbitrages-evenement .form-result-message', 'Erreur durant l\'ajout', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('#modal-arbitrages-evenement__bt-ajouter', '.loader-modal[for=form-arbitrages-evenement]');
                        FormUtils.displayFormResultMessage('#modal-arbitrages-evenement .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            }
        }
    }

    function control_AddEvenement() {
        $('#evenements__bt-ajouter').unbind('click');
        $('#evenements__bt-ajouter').click(function() {
            open_ModalSaveEvenement(null);
        });
    }

    function control_EditEvenement() {
        $('.glyphicon-pencil').unbind('click');
        $('.glyphicon-pencil').click(function() {
            var idEvenement = $(this).attr('id-evenement');
            open_ModalSaveEvenement(idEvenement);
        });
    }

    function open_ModalSaveEvenement(idEvenement) {
        var params = null;
        if (idEvenement != null) {
            params = {
                idEvenement: idEvenement
            };
        }
        AjaxUtils.loadView(
            'admin/evenements/evenements/modals/save/modal-save-evenement-content',
            '#modal-save-evenement-content',
            params,
            function() {
                control_TypeEvenement(idEvenement);
                control_ADomicile();
                control_Gymnase();
                onClick_btSauvegarder(idEvenement);
            },
            '#loader-page'
        );
        $('#modal-save-evenement').modal('show');

        function control_TypeEvenement(idEvenement) {
            if (idEvenement != null) {
                selectInputsByTypeEvenement($('#form-save-evenement__input-type').val());
            } else {
                selectInputsByTypeEvenement(AdminEvenementsEvenementsController.idTypeEvenementMatch)
            }
            $('#form-save-evenement__input-type').unbind('click');
            $('#form-save-evenement__input-type').change(function() {
                FormUtils.hideFormErrors('#modal-save-evenement #form-save-evenement');
                var typeEvenement = $(this).val();
                selectInputsByTypeEvenement(typeEvenement)
            });

            function selectInputsByTypeEvenement(typeEvenement) {
                if (typeEvenement == AdminEvenementsEvenementsController.idTypeEvenementMatch) {
                    $('.form-group-no-match').hide();

                    $('label[for=\"form-save-evenement__input-categories\"]').removeClass('required');
                    $('#form-save-evenement__input-categories').prop('required', false);
                    $('label[for=\"form-save-evenement__input-categorie\"]').addClass('required');
                    $('#form-save-evenement__input-categorie').prop('required', true);
                    $('label[for=\"form-save-evenement__input-adversaire\"]').addClass('required');
                    $('#form-save-evenement__input-adversaire').prop('required', true);

                    $('#form-save-evenement__input-nom').val('');
                    $('#form-save-evenement__input-categories').val('');

                    $('.form-group-match').show();

                    AdminController.setNumsEquipeInSelect(
                        $('#form-save-evenement__input-categorie').val(),
                        'form-save-evenement__input-num-equipe',
                        'form-save-evenement__input-hidden-num-equipe',
                        '.loader-modal[for=form-save-evenement]'
                    );
                    control_Categorie();
                    AdminEvenementsEvenementsController.setCompetitionsInSelect(
                        '',
                        'form-save-evenement__input-competition',
                        'form-save-evenement__input-hidden-competition',
                        '.loader-modal[for=form-save-evenement]'
                    );
                } else {
                    $('.form-group-match').hide();

                    $('label[for=\"form-save-evenement__input-categories\"]').addClass('required');
                    $('#form-save-evenement__input-categories').prop('required', true);
                    $('label[for=\"form-save-evenement__input-categorie\"]').removeClass('required');
                    $('#form-save-evenement__input-categorie').prop('required', false);
                    $('label[for=\"form-save-evenement__input-adversaire\"]').removeClass('required');
                    $('#form-save-evenement__input-adversaire').prop('required', false);

                    $('#form-save-evenement__input-categorie').val('');
                    $('#form-save-evenement__input-num-equipe').html('');
                    $('#form-save-evenement__input-adversaire').val('');
                    $('#form-save-evenement__input-competition').val('');
                    $('#form-save-evenement__input-score-hbcsj').val('');
                    $('#form-save-evenement__input-score-adversaire').val('');

                    $('.form-group-no-match').show();

                    onClick_btSelectAllCategories();
                }

                function onClick_btSelectAllCategories() {
                    $('#modal-save-evenement__bt-select-all-categories').unbind('click');
                    $('#modal-save-evenement__bt-select-all-categories').click(function() {
                        $('#form-save-evenement__input-categories option').prop('selected', true);
                    });
                }
            }
        }

        function control_Categorie() {
            $('#form-save-evenement__input-categorie').unbind('change');
            $('#form-save-evenement__input-categorie').change(function() {
                var idCategorie = $(this).val();
                AdminController.setNumsEquipeInSelect(
                    idCategorie,
                    'form-save-evenement__input-num-equipe',
                    'form-save-evenement__input-hidden-num-equipe',
                    '.loader-modal[for=form-save-evenement]'
                );
                AdminEvenementsEvenementsController.setCompetitionsInSelect(
                    idCategorie,
                    'form-save-evenement__input-competition',
                    'form-save-evenement__input-hidden-competition',
                    '.loader-modal[for=form-save-evenement]'
                );
            });
        }

        function control_ADomicile() {
            $('#form-save-evenement__input-a-domicile').unbind('change');
            $('#form-save-evenement__input-a-domicile').change(function() {
                if ($(this).prop('checked')) {
                    var idGymnaseADomicile = $('#form-save-evenement__input-gymnase option[a-domicile=1]').first().attr('value');
                    $('#form-save-evenement__input-gymnase').val(idGymnaseADomicile);
                    setGymnaseValues(idGymnaseADomicile);
                }
            });
        }

        function setGymnaseValues(idGymnase) {
            if (idGymnase != '') {
                $('.loader-modal[for=form-save-evenement]').show();
                AjaxUtils.callService(
                    'gymnase',
                    'getGymnaseById',
                    AjaxUtils.dataTypes.json,
                    AjaxUtils.requestTypes.get, {
                        id: idGymnase
                    },
                    null,
                    function(response) {
                        $('.loader-modal[for=form-save-evenement]').hide();

                        $('#form-save-evenement__input-adresse').val(response.adresse);
                        $('#form-save-evenement__input-code-postal').val(response.codePostal);
                        $('#form-save-evenement__input-ville').val(response.ville);
                        $('#form-save-evenement__input-latitude').val(response.latitude);
                        $('#form-save-evenement__input-longitude').val(response.longitude);
                    },
                    function(response) {
                        $('.loader-modal[for=form-save-evenement]').hide();

                        var errorGymnase = {
                            errorType: FormUtils.errorTypes.unavailableValue,
                            message: 'Service indisponible'
                        };
                        FormUtils.hideErrors('form-save-evenement__input-gymnase');
                        FormUtils.displayError('form-save-evenement__input-gymnase', errorGymnase);
                    }
                );
            } else {
                $('#form-save-evenement__input-adresse').val('');
                $('#form-save-evenement__input-code-postal').val('');
                $('#form-save-evenement__input-ville').val('');
                $('#form-save-evenement__input-latitude').val('');
                $('#form-save-evenement__input-longitude').val('');
            }
        }

        function control_Gymnase() {
            $('#form-save-evenement__input-gymnase').unbind('change');
            $('#form-save-evenement__input-gymnase').change(function() {
                var idGymnase = $(this).val();
                setGymnaseValues(idGymnase);
            });
        }

        function onClick_btSauvegarder(idEvenement) {
            $('#modal-save-evenement__bt-sauvegarder').unbind('click');
            $('#modal-save-evenement__bt-sauvegarder').click(function() {
                FormUtils.hideFormErrors('#modal-save-evenement #form-save-evenement');
                ViewUtils.unsetHTMLAndHide('#modal-save-evenement .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('#modal-save-evenement__bt-sauvegarder', '#modal-save-evenement__bt-sauvegarder .loader');

                var formValidation = FormUtils.validateForm('#modal-save-evenement #form-save-evenement');

                if (formValidation) {
                    var inputTypeEvenement = $('#form-save-evenement__input-type').val();
                    var inputCodeRencontre = $.trim($('#form-save-evenement__input-code-rencontre').val());
                    var inputNom = $.trim($('#form-save-evenement__input-nom').val());
                    var inputDate = $.trim($('#form-save-evenement__input-date').val());
                    var inputHeure = $('#form-save-evenement__input-heure').val();

                    var categorie = $('#form-save-evenement__input-categorie').val();
                    var inputCategories = [];
                    if (categorie != '') {
                        inputCategories.push(categorie);
                    } else {
                        inputCategories = $('#form-save-evenement__input-categories').val();
                    }

                    var inputNumEquipe = $('#form-save-evenement__input-num-equipe').val();
                    var inputAdversaire = $.trim($('#form-save-evenement__input-adversaire').val());
                    var inputCompetition = $('#form-save-evenement__input-competition').val();
                    var inputGymnase = $('#form-save-evenement__input-gymnase').val();
                    var inputAdresse = $.trim($('#form-save-evenement__input-adresse').val());
                    var inputCodePostal = $.trim($('#form-save-evenement__input-code-postal').val());
                    var inputVille = $.trim($('#form-save-evenement__input-ville').val());
                    var inputLatitude = $.trim($('#form-save-evenement__input-latitude').val());
                    var inputLongitude = $.trim($('#form-save-evenement__input-longitude').val());
                    var inputADomicile = ($('#form-save-evenement__input-a-domicile').prop('checked')) ? '1' : '0';
                    var inputScoreHBCSJ = $.trim($('#form-save-evenement__input-score-hbcsj').val());
                    var inputScoreAdversaire = $.trim($('#form-save-evenement__input-score-adversaire').val());
                    var inputVisible = ($('#form-save-evenement__input-visible').prop('checked')) ? '1' : '0';
                    var inputEmailDernierEditeur = $.trim($('#form-save-evenement__input-email-dernier-editeur').val());

                    saveEvenement(idEvenement, inputTypeEvenement, inputCodeRencontre, inputNom, inputDate, inputHeure, inputCategories, inputNumEquipe,
                        inputAdversaire, inputCompetition, inputGymnase, inputAdresse, inputCodePostal, inputVille,
                        inputLatitude, inputLongitude, inputADomicile, inputScoreHBCSJ, inputScoreAdversaire,
                        inputVisible, inputEmailDernierEditeur);
                } else {
                    ViewUtils.activeButtonAndHideLoader('#modal-save-evenement__bt-sauvegarder', '#modal-save-evenement__bt-sauvegarder .loader');
                }
            });

            function saveEvenement(idEvenement, inputTypeEvenement, inputCodeRencontre, inputNom, inputDate, inputHeure, inputCategories, inputNumEquipe,
                inputAdversaire, inputCompetition, inputGymnase, inputAdresse, inputCodePostal, inputVille,
                inputLatitude, inputLongitude, inputADomicile, inputScoreHBCSJ, inputScoreAdversaire,
                inputVisible, inputEmailDernierEditeur) {
                AjaxUtils.callService(
                    'evenement',
                    'save',
                    AjaxUtils.dataTypes.json,
                    AjaxUtils.requestTypes.post, {
                        id: idEvenement
                    }, {
                        typeEvenementId: inputTypeEvenement,

                        codeRencontre: inputCodeRencontre,
                        nom: inputNom,
                        date: inputDate,
                        heure: inputHeure,
                        categorieIds: inputCategories,
                        numEquipe: inputNumEquipe,
                        adversaire: inputAdversaire,
                        competitionId: inputCompetition,
                        gymnaseId: inputGymnase,
                        adresse: inputAdresse,
                        codePostal: inputCodePostal,
                        ville: inputVille,
                        latitude: inputLatitude,
                        longitude: inputLongitude,
                        aDomicile: inputADomicile,
                        scoreHBCSJ: inputScoreHBCSJ,
                        scoreAdversaire: inputScoreAdversaire,
                        visibleSurSite: inputVisible,
                        emailDernierEditeur: inputEmailDernierEditeur
                    },
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-save-evenement .form-result-message', 'Ev&eacute;nement sauvegard&eacute;', FormUtils.resultsTypes.ok);
                            $('#modal-save-evenement__bt-sauvegarder .loader').hide();
                            AdminEvenementsEvenementsController.load_StatsEvenements();
                            AdminEvenementsEvenementsController.load_TableEvenements();
                        } else {
                            ViewUtils.activeButtonAndHideLoader('#modal-save-evenement__bt-sauvegarder', '#modal-save-evenement__bt-sauvegarder .loader');
                            FormUtils.displayFormResultMessage('#modal-save-evenement .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('#modal-save-evenement__bt-sauvegarder', '#modal-save-evenement__bt-sauvegarder .loader');
                        FormUtils.displayFormResultMessage('#modal-save-evenement .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            }
        }
    }

    function control_DeleteEvenement() {
        $('.glyphicon-trash').unbind('click');
        $('.glyphicon-trash').click(function() {
            var idEvenement = $(this).attr('id-evenement');

            ViewUtils.prompt(
                'Supprimer cet &eacute;v&eacute;nement ?', {
                    btOkText: 'Oui',
                    btKoText: 'Non'
                },
                function() {
                    deleteEvenement(idEvenement);
                },
                null,
                null
            );
        });

        function deleteEvenement(idEvenement) {
            $('.loader-delete[id-evenement=' + idEvenement + ']').show();
            AjaxUtils.callService(
                'evenement',
                'delete',
                AjaxUtils.dataTypes.json,
                AjaxUtils.requestTypes.post,
                null, {
                    id: idEvenement
                },
                function(response) {
                    $('.loader-delete[id-evenement=' + idEvenement + ']').hide();
                    if (response.success) {
                        AdminEvenementsEvenementsController.load_StatsEvenements();
                        AdminEvenementsEvenementsController.load_TableEvenements();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la suppression de l\'&eacute;v&eacute;nement',
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-delete[id-evenement=' + idEvenement + ']').hide();
                    ViewUtils.alert(
                        'Service indisponible',
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        }
    }

    function control_ValidationEvenement() {
        $('.icon-validation').unbind('click');
        $('.icon-validation').click(function() {
            var idEvenement = $(this).attr('id-evenement');

            $('.loader-validation[id-evenement=' + idEvenement + ']').show();
            AjaxUtils.callService(
                'evenement',
                'toggleValidation',
                AjaxUtils.dataTypes.json,
                AjaxUtils.requestTypes.post,
                null, {
                    id: idEvenement
                },
                function(response) {
                    $('.loader-validation[id-evenement=' + idEvenement + ']').hide();
                    if (response.success) {
                        AdminEvenementsEvenementsController.load_StatsEvenements();
                        AdminEvenementsEvenementsController.load_TableEvenements();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la validation / invalidation de l\'&eacute;v&eacute;nement',
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-validation[id-evenement=' + idEvenement + ']').hide();
                    ViewUtils.alert(
                        'Service indisponible',
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        });
    }
};

AdminEvenementsEvenementsController.setCompetitionsInSelect = function(idCategorie, input, inputHidden, loaderSelector) {
    FormUtils.hideErrors(input);
    $(loaderSelector).show();
    if (idCategorie != '') {
        AjaxUtils.callService(
            'competition',
            'getCompetitionsByCategorie',
            AjaxUtils.dataTypes.json,
            AjaxUtils.requestTypes.get, {
                idCategorie: idCategorie
            },
            null,
            function(response) {
                setHTMLCompetitionsInSelect(response, input, inputHidden, loaderSelector);
            },
            function(response) {
                displayErrorForCompetitionsSelect(input, loaderSelector);
            }
        );
    } else {
        AjaxUtils.callService(
            'competition',
            'getCompetitions',
            AjaxUtils.dataTypes.json,
            AjaxUtils.requestTypes.get,
            null,
            null,
            function(response) {
                setHTMLCompetitionsInSelect(response, input, inputHidden, loaderSelector);
            },
            function(response) {
                displayErrorForCompetitionsSelect(input, loaderSelector);
            }
        );
    }

    function setHTMLCompetitionsInSelect(response, input, inputHidden, loaderSelector) {
        $(loaderSelector).hide();

        var htmlCompetitions = '';

        htmlCompetitions += '<option value=""></option>';
        for (var i = 0; i < response.competitions.length; i++) {
            htmlCompetitions += '<option value="' + response.competitions[i].id + '">' + response.competitions[i].nom + '</option>';
        }

        $('#' + input).html(htmlCompetitions);
        if ('#' + inputHidden != null && $('#' + inputHidden).val() != '') {
            $('#' + input).val($('#' + inputHidden).val());
        }
    }

    function displayErrorForCompetitionsSelect(input, loaderSelector) {
        $(loaderSelector).hide();

        var errorCompetitions = {
            errorType: FormUtils.errorTypes.unavailableValue,
            message: 'Service indisponible'
        };
        FormUtils.hideErrors(input);
        FormUtils.displayError(input, errorCompetitions);
    }
};

initializePage = function() {
    AdminController.initializePage('evenements', 'evenements');

    AjaxUtils.callService(
        'date',
        'getSlashDateAvecAjout',
        AjaxUtils.dataTypes.json,
        AjaxUtils.requestTypes.get, {
            ajout: -7
        },
        null,
        function(response) {
            AdminEvenementsEvenementsController.dateDebutRecherche = response.date;

            AdminEvenementsEvenementsController.load_StatsEvenements();
            AdminEvenementsEvenementsController.load_RechercheAndTableEvenements();
        },
        function(response) {}
    );
};