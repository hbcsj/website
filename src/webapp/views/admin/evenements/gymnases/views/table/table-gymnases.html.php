<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/evenements/gymnases/views/table/", 
    "table-gymnases", 
    "TableGymnasesCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
    $gymnases = $ctrl->getGymnases();
    $ressourcesExternes = $ctrl->getRessourcesExternes();
    ?>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Nom</th>
                <th>Adresse</th>
                <th>Code postal</th>
                <th>Ville</th>
                <th class="text-center">A domicile ?</th>
                <th class="text-center">Localiser</th>
                <?php
                if (isAdminConnected_coach()) {
                    ?>
                    <th colspan="2">&nbsp;</th>
                    <?php
                }
                ?>
            </tr>
        </thead>
        <tbody>
            <?php
            if (sizeof($gymnases) > 0) {
                foreach ($gymnases as $gymnase) {
                    ?>
                    <tr>
                        <td><?php echo $gymnase[GYMNASE_NOM]; ?></td>
                        <td><?php echo $gymnase[GYMNASE_ADRESSE]; ?></td>
                        <td><?php echo $gymnase[GYMNASE_CODE_POSTAL]; ?></td>
                        <td><?php echo $gymnase[GYMNASE_VILLE]; ?></td>
                        <td class="text-center"><?php echo ($gymnase[GYMNASE_A_DOMICILE] == 1) ? "Oui" : "Non"; ?></td>
                        <td class="text-center">
                            <a class="link-with-img" href="<?php echo $ressourcesExternes[RESSOURCES_EXTERNES_GOOGLE_MAP_URL].$gymnase[GYMNASE_LATITUDE].",".$gymnase[GYMNASE_LONGITUDE]; ?>" target="_blank" title="Visualiser sur Google Map">
                                <span class="glyphicon glyphicon-screenshot"></span>
                            </a>
                        </td>
                        <?php
                        if (isAdminConnected_coach()) {
                            ?>
                            <td class="text-center">
                                <span class="glyphicon glyphicon-pencil clickable" title="Editer" id-gymnase="<?php echo $gymnase[GYMNASE_ID]; ?>"></span>
                            </td>
                            <td class="text-center">
                                <span class="glyphicon glyphicon-trash clickable" title="Supprimer" id-gymnase="<?php echo $gymnase[GYMNASE_ID]; ?>"></span>
                                <div class="loader loader-delete" id-gymnase="<?php echo $gymnase[GYMNASE_ID]; ?>"></div>
                            </td>
                            <?php
                        }
                        ?>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>