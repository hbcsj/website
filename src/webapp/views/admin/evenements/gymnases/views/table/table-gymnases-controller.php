<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/gymnase-dao.php");
require_once("common/php/dao/ressources-externes-dao.php");

class TableGymnasesCtrl extends AbstractViewCtrl {

	private $gymnaseDAO;

    private $ressourcesExternesDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_coach()) {
			$this->gymnaseDAO = new GymnaseDAO($this->getDatabaseConnection());
			$this->ressourcesExternesDAO = new RessourcesExternesDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getGymnases() {
		return $this->gymnaseDAO->getAll(GYMNASE_TABLE_NAME.".".GYMNASE_VILLE.", ".GYMNASE_TABLE_NAME.".".GYMNASE_NOM);
	}

    public function getRessourcesExternes() {
        return $this->ressourcesExternesDAO->getSingleton();
    }
}

?>