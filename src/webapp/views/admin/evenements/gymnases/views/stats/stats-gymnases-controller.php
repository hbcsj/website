<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/gymnase-dao.php");

class StatsGymnasesCtrl extends AbstractViewCtrl {

	private $gymnaseDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_coach()) {
			$this->gymnaseDAO = new GymnaseDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getNbGymnases() {
		return $this->gymnaseDAO->getNbTotal()[GYMNASE_NB_GYMNASES];
	}
}

?>