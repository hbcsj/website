<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/gymnase-dao.php");

class ModalSaveGymnaseCtrl extends AbstractViewCtrl {
	
	private $gymnase;

	private $gymnaseDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idGymnase" => $_GET["idGymnase"]
		), true);
		
		if (isAdminConnected_coach()) {
			$this->gymnaseDAO = new GymnaseDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdGymnase();
	}

	private function checkIdGymnase() {
		if (HTTPUtils::paramExists(GET, "idGymnase")) {
			$this->gymnase = $this->gymnaseDAO->getById($_GET["idGymnase"]);
				
			if ($this->gymnase == null) {
				$this->sendCheckError(
					HTTP_404, 
					"Le gymnase (idGymnase = '".$_GET["idGymnase"]."') n'existe pas", 
					"webapp/views/common/error/404/404.html.php"
				);
			}
		}
	}

	public function getTitle() {
		$title = "Nouveau gymnase";
		if ($this->gymnase != null) {
			$title = $this->gymnase[GYMNASE_NOM]." - ".$this->gymnase[GYMNASE_VILLE]." - <span class=\"normal\">Edition</span>";
		}
		return $title;
	}

	public function getNomGymnase() {
		if ($this->gymnase != null) {
			return $this->gymnase[GYMNASE_NOM];
		}
		return "";
	}

	public function getAdresseGymnase() {
		if ($this->gymnase != null) {
			return $this->gymnase[GYMNASE_ADRESSE];
		}
		return "";
	}

	public function getCodePostalGymnase() {
		if ($this->gymnase != null) {
			return $this->gymnase[GYMNASE_CODE_POSTAL];
		}
		return "";
	}

	public function getVilleGymnase() {
		if ($this->gymnase != null) {
			return $this->gymnase[GYMNASE_VILLE];
		}
		return "";
	}

	public function getLatitudeGymnase() {
		if ($this->gymnase != null) {
			return $this->gymnase[GYMNASE_LATITUDE];
		}
		return "";
	}

	public function getLongitudeGymnase() {
		if ($this->gymnase != null) {
			return $this->gymnase[GYMNASE_LONGITUDE];
		}
		return "";
	}

	public function getADomicileChecked() {
		$checked = "";
        if ($this->gymnase != null && $this->gymnase[GYMNASE_A_DOMICILE]) {
            $checked = " checked=\"checked\"";
        }
        return $checked;
	}
}

?>