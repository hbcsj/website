<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/evenements/gymnases/modals/save/", 
    "modal-save-gymnase", 
    "ModalSaveGymnaseCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
?>
    <div class="modal-header">
        <div class="modal-title"><?php echo $ctrl->getTitle(); ?></div>
    </div>
    <div class="modal-body">
        <div class="form-horizontal" id="form-save-gymnase">
            <div class="form-group">
                <label for="form-save-gymnase__input-nom" class="col-xs-4 control-label required">Nom :</label>
                <div class="col-xs-7 form-input" for="form-save-gymnase__input-nom">
                    <input type="text" class="form-control" id="form-save-gymnase__input-nom" value="<?php echo $ctrl->getNomGymnase(); ?>" required>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-gymnase__input-nom"></div>
            </div>
            <div class="form-group">
                <label for="form-save-gymnase__input-adresse" class="col-xs-4 control-label">Adresse :</label>
                <div class="col-xs-7 form-input" for="form-save-gymnase__input-adresse">
                    <input type="text" class="form-control" id="form-save-gymnase__input-adresse" value="<?php echo $ctrl->getAdresseGymnase(); ?>" placeholder="Adresse">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-gymnase__input-adresse"></div>
            </div>
            <div class="form-group">
                <div class="col-xs-7 col-xs-offset-4 form-input" for="form-save-gymnase__input-code-postal">
                    <input type="text" class="form-control" id="form-save-gymnase__input-code-postal" value="<?php echo $ctrl->getCodePostalGymnase(); ?>" placeholder="Code postal">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-gymnase__input-code-postal"></div>
            </div>
            <div class="form-group">
                <label for="form-save-gymnase__input-ville" class="col-xs-4 control-label required">&nbsp;</label>
                <div class="col-xs-7 form-input" for="form-save-gymnase__input-ville">
                    <input type="text" class="form-control" id="form-save-gymnase__input-ville" value="<?php echo $ctrl->getVilleGymnase(); ?>" placeholder="Ville" required>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-gymnase__input-ville"></div>
            </div>
            <div class="form-group">
                <label for="form-save-gymnase__input-latitude" class="col-xs-4 control-label required">Latitude :</label>
                <div class="col-xs-7 form-input" for="form-save-gymnase__input-latitude">
                    <input type="text" class="form-control" id="form-save-gymnase__input-latitude" value="<?php echo $ctrl->getLatitudeGymnase(); ?>" pattern="^(-)?[0-9]+(\.)?[0-9]*$" pattern-indication="La latitude doit &ecirc;tre un nombre d&eacute;cimal" required>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-gymnase__input-latitude"></div>
            </div>
            <div class="form-group">
                <label for="form-save-gymnase__input-longitude" class="col-xs-4 control-label required">Longitude :</label>
                <div class="col-xs-7 form-input" for="form-save-gymnase__input-longitude">
                    <input type="text" class="form-control" id="form-save-gymnase__input-longitude" value="<?php echo $ctrl->getLongitudeGymnase(); ?>" pattern="^(-)?[0-9]+(\.)?[0-9]*$" pattern-indication="La longitude doit &ecirc;tre un nombre d&eacute;cimal" required>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-gymnase__input-longitude"></div>
            </div>
            <div class="form-group">
                <label for="form-save-gymnase__input-a-domicile" class="col-xs-4 control-label">A domicile ?</label>
                <div class="col-xs-7 form-input" for="form-save-gymnase__input-a-domicile">
                    <input type="checkbox" id="form-save-gymnase__input-a-domicile"<?php echo $ctrl->getADomicileChecked(); ?>>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-gymnase__input-a-domicile"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-required text-right">Les champs marqu&eacute;s d'une * sont obligatoires</div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-8">
                <div class="form-result-message"></div>
            </div>
            <div class="col-xs-4 text-right">
                <button id="modal-save-gymnase__bt-sauvegarder" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-ok"></span>
                    </div>
                    <div class="button__text">Sauvegarder</div>
                    <div class="loader"></div>
                </button>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>