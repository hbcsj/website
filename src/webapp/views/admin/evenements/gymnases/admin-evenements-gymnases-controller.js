AdminEvenementsGymnasesController = {};

AdminEvenementsGymnasesController.load_StatsGymnases = function() {
    if (Utils.exists('#container-stats-gymnases')) {
        AjaxUtils.loadView(
            'admin/evenements/gymnases/views/stats/stats-gymnases',
            '#container-stats-gymnases',
            null,
            function() {},
            '#loader-page'
        );
    }
};

AdminEvenementsGymnasesController.load_TableGymnases = function() {
    if (Utils.exists('#container-table-gymnases')) {
        AjaxUtils.loadView(
            'admin/evenements/gymnases/views/table/table-gymnases',
            '#container-table-gymnases',
            null,
            function() {
                control_AddGymnase();
                control_EditGymnase();
                control_DeleteGymnase();
            },
            '#loader-page'
        );
    }

    function control_AddGymnase() {
        $('#gymnases__bt-ajouter').unbind('click');
        $('#gymnases__bt-ajouter').click(function() {
            open_ModalSaveGymnase(null);
        });
    }

    function control_EditGymnase() {
        $('.glyphicon-pencil').unbind('click');
        $('.glyphicon-pencil').click(function() {
            var idGymnase = $(this).attr('id-gymnase');
            open_ModalSaveGymnase(idGymnase);
        });
    }

    function open_ModalSaveGymnase(idGymnase) {
        var params = null;
        if (idGymnase != null) {
            params = {
                idGymnase: idGymnase
            };
        }
        AjaxUtils.loadView(
            'admin/evenements/gymnases/modals/save/modal-save-gymnase-content',
            '#modal-save-gymnase-content',
            params,
            function() {
                onClick_btSauvegarder(idGymnase);
            },
            '#loader-page'
        );
        $('#modal-save-gymnase').modal('show');

        function onClick_btSauvegarder(idGymnase) {
            $('#modal-save-gymnase__bt-sauvegarder').unbind('click');
            $('#modal-save-gymnase__bt-sauvegarder').click(function() {
                FormUtils.hideFormErrors('#modal-save-gymnase #form-save-gymnase');
                ViewUtils.unsetHTMLAndHide('#modal-save-gymnase .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('#modal-save-gymnase__bt-sauvegarder', '#modal-save-gymnase__bt-sauvegarder .loader');

                var formValidation = FormUtils.validateForm('#modal-save-gymnase #form-save-gymnase');
                
                if (formValidation) {
                    var inputNom = $.trim($('#form-save-gymnase__input-nom').val());
                    var inputAdresse = $.trim($('#form-save-gymnase__input-adresse').val());
                    var inputCodePostal = $.trim($('#form-save-gymnase__input-code-postal').val());
                    var inputVille = $.trim($('#form-save-gymnase__input-ville').val());
                    var inputLatitude = $.trim($('#form-save-gymnase__input-latitude').val());
                    var inputLongitude = $.trim($('#form-save-gymnase__input-longitude').val());
                    var inputADomicile = ($('#form-save-gymnase__input-a-domicile').prop('checked')) ? '1' : '0';

                    saveGymnase(idGymnase, inputNom, inputAdresse, inputCodePostal, inputVille, inputLatitude, inputLongitude, inputADomicile);
                } else {
                    ViewUtils.activeButtonAndHideLoader('#modal-save-gymnase__bt-sauvegarder', '#modal-save-gymnase__bt-sauvegarder .loader');
                }
            });

            function saveGymnase(idGymnase, inputNom, inputAdresse, inputCodePostal, inputVille, inputLatitude, inputLongitude, inputADomicile) {
                AjaxUtils.callService(
                    'gymnase', 
                    'save', 
                    AjaxUtils.dataTypes.json, 
                    AjaxUtils.requestTypes.post, 
                    {
                        id: idGymnase
                    }, 
                    {
                        nom: inputNom,
                        adresse: inputAdresse,
                        codePostal: inputCodePostal,
                        ville: inputVille,
                        latitude: inputLatitude,
                        longitude: inputLongitude,
                        aDomicile: inputADomicile
                    }, 
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-save-gymnase .form-result-message', 'Gymnase sauvegard&eacute;', FormUtils.resultsTypes.ok);
                            $('#modal-save-gymnase__bt-sauvegarder .loader').hide();
                            AdminEvenementsGymnasesController.load_StatsGymnases();
                            AdminEvenementsGymnasesController.load_TableGymnases();
                        } else {
                            ViewUtils.activeButtonAndHideLoader('#modal-save-gymnase__bt-sauvegarder', '#modal-save-gymnase__bt-sauvegarder .loader');
                            FormUtils.displayFormResultMessage('#modal-save-gymnase .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('#modal-save-gymnase__bt-sauvegarder', '#modal-save-gymnase__bt-sauvegarder .loader');
                        FormUtils.displayFormResultMessage('#modal-save-gymnase .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            }
        }
    }

    function control_DeleteGymnase() {
        $('.glyphicon-trash').unbind('click');
        $('.glyphicon-trash').click(function() {
            var idGymnase = $(this).attr('id-gymnase');
            
            ViewUtils.prompt(
                'Supprimer ce gymnase ?', 
                {
                    btOkText: 'Oui',
                    btKoText: 'Non'
                },
                function() {
                    deleteGymnase(idGymnase);
                }, 
                null, 
                null
            );
        });

        function deleteGymnase(idGymnase) {
            $('.loader-delete[id-gymnase='+idGymnase+']').show();
            AjaxUtils.callService(
                'gymnase', 
                'delete', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    id: idGymnase
                }, 
                function(response) {
                    $('.loader-delete[id-gymnase='+idGymnase+']').hide();
                    if (response.success) {
                        AdminEvenementsGymnasesController.load_StatsGymnases();
                        AdminEvenementsGymnasesController.load_TableGymnases();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la suppression du gymnase', 
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-delete[id-gymnase='+idGymnase+']').hide();
                    ViewUtils.alert(
                        'Service indisponible', 
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        }
    }
};

initializePage = function() {
    AdminController.initializePage('evenements', 'gymnases');

    AdminEvenementsGymnasesController.load_StatsGymnases();
    AdminEvenementsGymnasesController.load_TableGymnases();
};