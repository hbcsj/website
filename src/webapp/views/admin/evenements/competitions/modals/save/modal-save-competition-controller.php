<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/lib/admin-utils.php");
require_once("common/php/dao/competition-dao.php");
require_once("common/php/dao/categorie-dao.php");
require_once("common/php/dao/saison-dao.php");

class ModalSaveCompetitionCtrl extends AbstractViewCtrl {
	
	private $competition;

	private $competitionDAO;
	private $categorieDAO;
	private $saisonDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, array(
            "idCompetition" => $_GET["idCompetition"]
		), true);
		
		if (isAdminConnected_coach()) {
			$this->competitionDAO = new CompetitionDAO($this->getDatabaseConnection());
			$this->categorieDAO = new CategorieDAO($this->getDatabaseConnection());
			$this->saisonDAO = new SaisonDAO($this->getDatabaseConnection());

			$this->checkRequest();
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	private function checkRequest() {
		$this->checkIdCompetition();
	}

	private function checkIdCompetition() {
		if (HTTPUtils::paramExists(GET, "idCompetition")) {
			$this->competition = $this->competitionDAO->getById($_GET["idCompetition"]);
				
			if ($this->competition == null) {
				$this->sendCheckError(
					HTTP_404, 
					"La personne (idCompetition = '".$_GET["idCompetition"]."') n'existe pas", 
					"webapp/views/common/error/404/404.html.php"
				);
			}
		}
	}

	public function getTitle() {
		$title = "Nouvelle comp&eacute;tition";
		if ($this->competition != null) {
			$title = $this->competition[COMPETITION_NOM]." - <span class=\"normal\">Edition</span>";
		}
		return $title;
	}

	public function getNomCompetition() {
		if ($this->competition != null) {
			return $this->competition[COMPETITION_NOM];
		}
		return "";
	}

	public function getCodeCompetition() {
		if ($this->competition != null) {
			return $this->competition[COMPETITION_CODE];
		}
		return "";
	}

	public function getOptionsCategorieCompetition() {
		$html = "";

		$categories = $this->categorieDAO->getAll(CATEGORIE_TABLE_NAME.".".CATEGORIE_POSITION_AFFICHAGE);
		if (sizeof($categories) > 0) {
			foreach ($categories as $categorie) {
				$selected = "";
				if ($this->competition[COMPETITION_CATEGORIE_ID] == $categorie[CATEGORIE_ID]) {
					$selected = " selected";
				}
				$html .= "<option value=\"".$categorie[CATEGORIE_ID]."\"".$selected.">".$categorie[CATEGORIE_NOM]."</option>";
			}
		}

		return $html;
	}

	public function getNumEquipeCompetition() {
		if ($this->competition != null) {
			return $this->competition[COMPETITION_NUM_EQUIPE];
		}
		return "";
	}

	public function getOptionsSaisonCompetition() {
		$html = "";

		$saisons = $this->saisonDAO->getAll();
		if (sizeof($saisons) > 0) {
			foreach ($saisons as $saison) {
				$selected = "";
				if ($this->competition[COMPETITION_SAISON_ID] == $saison[SAISON_ID]) {
					$selected = " selected";
				}
				$html .= "<option value=\"".$saison[SAISON_ID]."\"".$selected.">".$saison[SAISON_LIBELLE]."</option>";
			}
		}

		return $html;
	}

	public function getUrlSiteFFHBCompetition() {
		if ($this->competition != null) {
			return $this->competition[COMPETITION_URL_SITE_FFHB];
		}
		return "";
	}

	public function getUrlSiteLigueCompetition() {
		if ($this->competition != null) {
			return $this->competition[COMPETITION_URL_SITE_LIGUE];
		}
		return "";
	}

	public function getEnCoursChecked() {
		$checked = "";
        if ($this->competition == null || ($this->competition != null && $this->competition[COMPETITION_EN_COURS])) {
            $checked = " checked=\"checked\"";
        }
        return $checked;
	}

	public function getVisibleChecked() {
		$checked = "";
		if (
			($this->competition == null && isAdminConnected_bureau()) || 
			($this->competition != null && isAdminConnected_bureau() && $this->competition[COMPETITION_VISIBLE_SUR_SITE])
		) {
			$checked = " checked=\"checked\"";
		}
        return $checked;
	}

	public function getEmailDernierEditeurCompetition() {
		if (isAdminConnected_superadmin()) {
			if ($this->competition != null) {
				return $this->competition[COMPETITION_EMAIL_DERNIER_EDITEUR];
			} else {
				return $GLOBALS[CONF][SUPERADMIN_EMAIL];
			}
		} 
		return "";
	}
}

?>