<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/evenements/competitions/modals/save/", 
    "modal-save-competition", 
    "ModalSaveCompetitionCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
?>
    <div class="modal-header">
        <div class="modal-title"><?php echo $ctrl->getTitle(); ?></div>
        <div class="loader loader-modal" for="form-save-competition"></div>
    </div>
    <div class="modal-body">
        <div class="form-horizontal" id="form-save-competition">
            <div class="form-group">
                <label for="form-save-competition__input-nom" class="col-xs-4 control-label required">Nom :</label>
                <div class="col-xs-7 form-input" for="form-save-competition__input-nom">
                    <input type="text" class="form-control" id="form-save-competition__input-nom" value="<?php echo $ctrl->getNomCompetition(); ?>" required>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-competition__input-nom"></div>
            </div>
            <div class="form-group">
                <label for="form-save-competition__input-code-competition" class="col-xs-4 control-label">Code competition :</label>
                <div class="col-xs-7 form-input" for="form-save-competition__input-code-competition">
                    <input type="text" class="form-control" id="form-save-competition__input-code-competition" value="<?php echo $ctrl->getCodeCompetition(); ?>">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-competition__input-code-competition"></div>
            </div>
            <div class="form-group">
                <label for="form-save-competition__input-categorie" class="col-xs-4 control-label">Cat&eacute;gorie :</label>
                <div class="col-xs-7 form-input" for="form-save-competition__input-categorie">
                    <select class="form-control" id="form-save-competition__input-categorie">
                        <option value=""></option>
                        <?php echo $ctrl->getOptionsCategorieCompetition(); ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-competition__input-categorie"></div>
            </div>
            <div class="form-group">
                <label for="form-save-competition__input-num-equipe" class="col-xs-4 control-label">Num&eacute;ro d'&eacute;quipe :</label>
                <div class="col-xs-7 form-input" for="form-save-competition__input-num-equipe">
                    <input type="hidden" id="form-save-competition__input-hidden-num-equipe" value="<?php echo $ctrl->getNumEquipeCompetition(); ?>">
                    <select class="form-control" id="form-save-competition__input-num-equipe">
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-competition__input-num-equipe"></div>
            </div>
            <div class="form-group">
                <label for="form-save-competition__input-saison" class="col-xs-4 control-label required">Saison :</label>
                <div class="col-xs-7 form-input" for="form-save-competition__input-saison">
                    <select class="form-control" id="form-save-competition__input-saison" required>
                        <option value=""></option>
                        <?php echo $ctrl->getOptionsSaisonCompetition(); ?>
                    </select>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-competition__input-saison"></div>
            </div>
            <div class="form-group">
                <label for="form-save-competition__input-url-site-ffhb" class="col-xs-4 control-label">Site FFHB :</label>
                <div class="col-xs-7 form-input" for="form-save-competition__input-url-site-ffhb">
                    <input type="text" class="form-control" id="form-save-competition__input-url-site-ffhb" value="<?php echo $ctrl->getUrlSiteFFHBCompetition(); ?>">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-competition__input-url-site-ffhb"></div>
            </div>
            <div class="form-group">
                <label for="form-save-competition__input-url-site-ligue" class="col-xs-4 control-label">Site Ligue :</label>
                <div class="col-xs-7 form-input" for="form-save-competition__input-url-site-ligue">
                    <input type="text" class="form-control" id="form-save-competition__input-url-site-ligue" value="<?php echo $ctrl->getUrlSiteLigueCompetition(); ?>">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-competition__input-url-site-ligue"></div>
            </div>
            <div class="form-group">
                <label for="form-save-competition__input-en-cours" class="col-xs-4 control-label">En cours ?</label>
                <div class="col-xs-7 form-input" for="form-save-competition__input-en-cours">
                    <input type="checkbox" id="form-save-competition__input-en-cours"<?php echo $ctrl->getEnCoursChecked(); ?>>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-competition__input-en-cours"></div>
            </div>
            <div class="form-group">
                <label for="form-save-competition__input-visible" class="col-xs-4 control-label">Visible ?</label>
                <div class="col-xs-1 form-input" for="form-save-competition__input-visible">
                    <?php
                    $disabledVisible = "";
                    if (!isAdminConnected_bureau() && !isAdminConnected_coach()) {
                        $disabledVisible = " disabled";
                    }
                    ?>
                    <input type="checkbox" id="form-save-competition__input-visible"<?php echo $ctrl->getVisibleChecked(); ?><?php echo $disabledVisible; ?>>
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-competition__input-visible"></div>
                <label for="form-save-competition__input-email-dernier-editeur" class="col-xs-1 control-label required">&nbsp;</label>
                <div class="col-xs-4 form-input" for="form-save-competition__input-email-dernier-editeur">
                    <?php
                    $disabledEmailDernierEditeur = "";
                    if (isAdminConnected_superadmin()) {
                        $disabledEmailDernierEditeur = " disabled";
                    }
                    ?>
                    <input type="text" class="form-control" id="form-save-competition__input-email-dernier-editeur" value="<?php echo $ctrl->getEmailDernierEditeurCompetition(); ?>" required <?php echo $disabledEmailDernierEditeur; ?> placeholder="Email dernier &eacute;diteur" pattern="^[A-Za-z0-9._-]+@[A-Za-z0-9.-]{2,}[.][A-Za-z]{2,3}$" pattern-indication="Le format de l'email est invalide">
                </div>
                <div class="col-xs-1 form-icon-validation" for="form-save-competition__input-email-dernier-editeur"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-required text-right">Les champs marqu&eacute;s d'une * sont obligatoires</div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-8">
                <div class="form-result-message"></div>
            </div>
            <div class="col-xs-4 text-right">
                <button id="modal-save-competition__bt-sauvegarder" class="btn btn-default">
                    <div class="button__icon">
                        <span class="glyphicon glyphicon-ok"></span>
                    </div>
                    <div class="button__text">Sauvegarder</div>
                    <div class="loader"></div>
                </button>
            </div>
        </div>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>