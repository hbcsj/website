<?php
require_once("core/php/resources/page.php");
require_once("common/php/lib/admin-utils.php");

$page = new Page(
    "webapp/views/admin/evenements/competitions/", 
    "admin-evenements-competitions", 
    "AdminEvenementsCompetitionsCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/admin/common/nav/nav.html.php");
if (isAdminConnected_coach()) {
    require_once("webapp/views/admin/evenements/sub-menu/sub-menu-admin-evenements.html.php");
}
?>

<div class="container-fluid animated fadeIn">
    <?php
    if (isAdminConnected_coach()) {
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h1>
                    Comp&eacute;titions
                    <button id="competitions__bt-ajouter" class="btn btn-default">
                        <div class="button__icon">
                            <span class="glyphicon glyphicon-plus"></span>
                        </div>
                        <div class="button__text">Ajouter une comp&eacute;tition</div>
                    </button>
                </h1>
            </div>
        </div>
        <div id="container-stats-competitions" class="container-stats"></div>
        <div id="container-recherche-competitions" class="container-recherche"></div>
        <div id="container-table-competitions" class="container-table"></div>
        <?php
    } else {
        require_once("webapp/views/common/mdp-form/mdp-form.html.php");
    }
    ?>
</div>

<?php
require_once("webapp/views/common/modals/alert/modal-alert.html.php");
require_once("webapp/views/common/modals/prompt/modal-prompt.html.php");

if (isAdminConnected_coach()) {
    require_once("webapp/views/admin/evenements/competitions/modals/save/modal-save-competition.html.php");
}

$page->finalizePage();
?>