AdminEvenementsCompetitionsController = {};

AdminEvenementsCompetitionsController.load_StatsCompetitions = function() {
    if (Utils.exists('#container-stats-competitions')) {
        AjaxUtils.loadView(
            'admin/evenements/competitions/views/stats/stats-competitions',
            '#container-stats-competitions',
            null,
            function() {},
            '#loader-page'
        );
    }
};

AdminEvenementsCompetitionsController.load_RechercheAndTableCompetitions = function() {
    if (Utils.exists('#container-recherche-competitions')) {
        AjaxUtils.loadView(
            'admin/evenements/competitions/views/recherche/recherche-competitions',
            '#container-recherche-competitions',
            null,
            function() {
                $('#form-recherche-competitions__input-is-en-cours').val(1);

                FormRechercheUtils.control_toggleFormRecherche();
                onClick_btRechercher();
                onClick_btReset();
                AdminEvenementsCompetitionsController.load_TableCompetitions();
            },
            '#loader-page'
        );
    }

    function onClick_btRechercher() {
        $('#recherche-competitions__bt-rechercher').unbind('click');
        $('#recherche-competitions__bt-rechercher').click(function() {
            AdminEvenementsCompetitionsController.load_TableCompetitions();
        });
    }

    function onClick_btReset() {
        $('#recherche-competitions__bt-reset').unbind('click');
        $('#recherche-competitions__bt-reset').click(function() {
            $('#form-recherche-competitions__input-code-competition').val('');
            $('#form-recherche-competitions__input-nom').val('');
            $('#form-recherche-competitions__input-categorie').val('');
            $('#form-recherche-competitions__input-saison').val('');
            $('#form-recherche-competitions__input-num-equipe').val('');
            $('#form-recherche-competitions__input-is-en-cours').val(1);
            $('#form-recherche-competitions__input-is-visible').val('');

            AdminEvenementsCompetitionsController.load_TableCompetitions();
        });
    }
};

AdminEvenementsCompetitionsController.load_TableCompetitions = function() {
    if (Utils.exists('#container-table-competitions')) {
        var params = {
            codeCompetition: $.trim($('#form-recherche-competitions__input-code-competition').val()),
            nom: $.trim($('#form-recherche-competitions__input-nom').val()),
            categorieId: $('#form-recherche-competitions__input-categorie').val(),
            saisonId: $('#form-recherche-competitions__input-saison').val(),
            numEquipe: $('#form-recherche-competitions__input-num-equipe').val(),
            enCours: $.trim($('#form-recherche-competitions__input-is-en-cours').val()),
            visible: $.trim($('#form-recherche-competitions__input-is-visible').val())
        };
        AjaxUtils.loadView(
            'admin/evenements/competitions/views/table/table-competitions',
            '#container-table-competitions',
            params,
            function() {
                control_AddCompetition();
                control_EditCompetition();
                control_DeleteCompetition();
                control_ValidationCompetition();
            },
            '#loader-page'
        );
    }

    function control_AddCompetition() {
        $('#competitions__bt-ajouter').unbind('click');
        $('#competitions__bt-ajouter').click(function() {
            open_ModalSaveCompetition(null);
        });
    }

    function control_EditCompetition() {
        $('.glyphicon-pencil').unbind('click');
        $('.glyphicon-pencil').click(function() {
            var idCompetition = $(this).attr('id-competition');
            open_ModalSaveCompetition(idCompetition);
        });
    }

    function open_ModalSaveCompetition(idCompetition) {
        var params = null;
        if (idCompetition != null) {
            params = {
                idCompetition: idCompetition
            };
        }
        AjaxUtils.loadView(
            'admin/evenements/competitions/modals/save/modal-save-competition-content',
            '#modal-save-competition-content',
            params,
            function() {
                AdminController.setNumsEquipeInSelect(
                    $('#form-save-competition__input-categorie').val(),
                    'form-save-competition__input-num-equipe',
                    'form-save-competition__input-hidden-num-equipe',
                    '.loader-modal[for=form-save-competition]'
                );
                control_Categorie();
                onClick_btSauvegarder(idCompetition);
            },
            '#loader-page'
        );
        $('#modal-save-competition').modal('show');

        function control_Categorie() {
            $('#form-save-competition__input-categorie').unbind('change');
            $('#form-save-competition__input-categorie').change(function() {
                var idCategorie = $(this).val();
                AdminController.setNumsEquipeInSelect(
                    idCategorie,
                    'form-save-competition__input-num-equipe',
                    'form-save-competition__input-hidden-num-equipe',
                    '.loader-modal[for=form-save-competition]'
                );
            });
        }

        function onClick_btSauvegarder(idCompetition) {
            $('#modal-save-competition__bt-sauvegarder').unbind('click');
            $('#modal-save-competition__bt-sauvegarder').click(function() {
                FormUtils.hideFormErrors('#modal-save-competition #form-save-competition');
                ViewUtils.unsetHTMLAndHide('#modal-save-competition .form-result-message');
                ViewUtils.desactiveButtonAndShowLoader('#modal-save-competition__bt-sauvegarder', '#modal-save-competition__bt-sauvegarder .loader');

                var formValidation = FormUtils.validateForm('#modal-save-competition #form-save-competition');

                if (formValidation) {
                    var inputCodeCompetition = $.trim($('#form-save-competition__input-code-competition').val());
                    var inputNom = $.trim($('#form-save-competition__input-nom').val());
                    var inputNumEquipe = $.trim($('#form-save-competition__input-num-equipe').val());
                    var inputUrlSiteFFHB = $.trim($('#form-save-competition__input-url-site-ffhb').val());
                    var inputUrlSiteLigue = $('#form-save-competition__input-url-site-ligue').val();
                    var inputVisible = ($('#form-save-competition__input-visible').prop('checked')) ? '1' : '0';
                    var inputEnCours = ($('#form-save-competition__input-en-cours').prop('checked')) ? '1' : '0';
                    var inputEmailDernierEditeur = $.trim($('#form-save-competition__input-email-dernier-editeur').val());
                    var inputCategorie = $.trim($('#form-save-competition__input-categorie').val());
                    var inputSaison = $.trim($('#form-save-competition__input-saison').val());

                    saveCompetition(idCompetition, inputCodeCompetition, inputNom, inputNumEquipe, inputUrlSiteFFHB, inputUrlSiteLigue, inputVisible, inputEnCours, inputEmailDernierEditeur, inputCategorie, inputSaison);
                } else {
                    ViewUtils.activeButtonAndHideLoader('#modal-save-competition__bt-sauvegarder', '#modal-save-competition__bt-sauvegarder .loader');
                }
            });

            function saveCompetition(idCompetition, inputCodeCompetition, inputNom, inputNumEquipe, inputUrlSiteFFHB, inputUrlSiteLigue, inputVisible, inputEnCours, inputEmailDernierEditeur, inputCategorie, inputSaison) {
                AjaxUtils.callService(
                    'competition',
                    'save',
                    AjaxUtils.dataTypes.json,
                    AjaxUtils.requestTypes.post, {
                        id: idCompetition
                    }, {
                        codeCompetition: inputCodeCompetition,
                        nom: inputNom,
                        numEquipe: inputNumEquipe,
                        urlSiteFFHB: inputUrlSiteFFHB,
                        urlSiteLigue: inputUrlSiteLigue,
                        enCours: inputEnCours,
                        visibleSurSite: inputVisible,
                        emailDernierEditeur: inputEmailDernierEditeur,
                        categorieId: inputCategorie,
                        saisonId: inputSaison
                    },
                    function(response) {
                        if (response.success) {
                            FormUtils.displayFormResultMessage('#modal-save-competition .form-result-message', 'Comp&eacute;tition sauvegard&eacute;e', FormUtils.resultsTypes.ok);
                            $('#modal-save-competition__bt-sauvegarder .loader').hide();
                            AdminEvenementsCompetitionsController.load_StatsCompetitions();
                            AdminEvenementsCompetitionsController.load_TableCompetitions();
                        } else {
                            ViewUtils.activeButtonAndHideLoader('#modal-save-competition__bt-sauvegarder', '#modal-save-competition__bt-sauvegarder .loader');
                            FormUtils.displayFormResultMessage('#modal-save-competition .form-result-message', 'Erreur durant la sauvegarde', FormUtils.resultsTypes.error);
                        }
                    },
                    function(response) {
                        ViewUtils.activeButtonAndHideLoader('#modal-save-competition__bt-sauvegarder', '#modal-save-competition__bt-sauvegarder .loader');
                        FormUtils.displayFormResultMessage('#modal-save-competition .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                    }
                );
            }
        }
    }

    function control_DeleteCompetition() {
        $('.glyphicon-trash').unbind('click');
        $('.glyphicon-trash').click(function() {
            var idCompetition = $(this).attr('id-competition');

            ViewUtils.prompt(
                'Supprimer cette comp&eacute;tition ?', {
                    btOkText: 'Oui',
                    btKoText: 'Non'
                },
                function() {
                    deleteCompetition(idCompetition);
                },
                null,
                null
            );
        });

        function deleteCompetition(idCompetition) {
            $('.loader-delete[id-competition=' + idCompetition + ']').show();
            AjaxUtils.callService(
                'competition',
                'delete',
                AjaxUtils.dataTypes.json,
                AjaxUtils.requestTypes.post,
                null, {
                    id: idCompetition
                },
                function(response) {
                    $('.loader-delete[id-competition=' + idCompetition + ']').hide();
                    if (response.success) {
                        AdminEvenementsCompetitionsController.load_StatsCompetitions();
                        AdminEvenementsCompetitionsController.load_TableCompetitions();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la suppression de la comp&eacute;tition',
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-delete[id-competition=' + idCompetition + ']').hide();
                    ViewUtils.alert(
                        'Service indisponible',
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        }
    }

    function control_ValidationCompetition() {
        $('.icon-validation').unbind('click');
        $('.icon-validation').click(function() {
            var idCompetition = $(this).attr('id-competition');

            $('.loader-validation[id-competition=' + idCompetition + ']').show();
            AjaxUtils.callService(
                'competition',
                'toggleValidation',
                AjaxUtils.dataTypes.json,
                AjaxUtils.requestTypes.post,
                null, {
                    id: idCompetition
                },
                function(response) {
                    $('.loader-validation[id-competition=' + idCompetition + ']').hide();
                    if (response.success) {
                        AdminEvenementsCompetitionsController.load_StatsCompetitions();
                        AdminEvenementsCompetitionsController.load_TableCompetitions();
                    } else {
                        ViewUtils.alert(
                            'Erreur durant la validation / invalidation de la comp&eacute;tition',
                            ViewUtils.alertTypes.error,
                            null
                        );
                    }
                },
                function(response) {
                    $('.loader-validation[id-competition=' + idCompetition + ']').hide();
                    ViewUtils.alert(
                        'Service indisponible',
                        ViewUtils.alertTypes.error,
                        null
                    );
                }
            );
        });
    }
};

initializePage = function() {
    AdminController.initializePage('evenements', 'competitions');

    AdminEvenementsCompetitionsController.load_StatsCompetitions();
    AdminEvenementsCompetitionsController.load_RechercheAndTableCompetitions();
};