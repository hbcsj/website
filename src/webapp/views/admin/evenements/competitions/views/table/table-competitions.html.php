<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/evenements/competitions/views/table/", 
    "table-competitions", 
    "TableCompetitionsCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
    $competitions = $ctrl->getCompetitions();
    ?>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Code compétition</th>
                <th>Nom</th>
                <th>Saison</th>
                <th>Equipe</th>
                <th class="text-center">Site FFHB</th>
                <th class="text-center">Site ligue</th>
                <th class="text-center">En cours ?</th>
                <th class="text-center">Visible ?</th>
                <?php
                if (isAdminConnected_superadmin()) {
                    ?>
                    <th colspan="3">&nbsp;</th>
                    <?php
                } else if (isAdminConnected_bureau()) {
                    ?>
                    <th colspan="2">&nbsp;</th>
                    <?php
                } else {
                    ?>
                    <th>&nbsp;</th>
                    <?php
                }
                ?>
            </tr>
        </thead>
        <tbody>
            <?php
            if (sizeof($competitions) > 0) {
                foreach ($competitions as $competition) {
                    $bg = "";
                    if ($competition[COMPETITION_EMAIL_DERNIER_EDITEUR] != $GLOBALS[CONF][SUPERADMIN_EMAIL]) {
                        $bg = "bg-error";
                    } else if (!$competition[COMPETITION_VISIBLE_SUR_SITE]) {
                        $bg = "bg-warning";
                    }
                    ?>
                    <tr>
                        <td class="<?php echo $bg; ?>"><?php echo $competition[COMPETITION_CODE]; ?></td>
                        <td class="<?php echo $bg; ?>"><?php echo $competition[COMPETITION_NOM]; ?></td>
                        <td class="<?php echo $bg; ?>"><?php echo $ctrl->getLibelleSaison($competition[COMPETITION_SAISON_ID]); ?></td>
                        <td class="<?php echo $bg; ?>"><?php echo $ctrl->getNomEquipe($competition); ?></td>
                        <td class="text-center <?php echo $bg; ?>">
                            <?php
                            if ($competition[COMPETITION_URL_SITE_FFHB] != "") {
                                ?>
                                <a href="<?php echo $competition[COMPETITION_URL_SITE_FFHB]; ?>" target="_blank">Voir</a>
                                <?php
                            }
                            ?>
                        </td>
                        <td class="text-center <?php echo $bg; ?>">
                            <?php
                            if ($competition[COMPETITION_URL_SITE_LIGUE] != "") {
                                ?>
                                <a href="<?php echo $competition[COMPETITION_URL_SITE_LIGUE]; ?>" target="_blank">Voir</a>
                                <?php
                            }
                            ?>
                        </td>
                        <td class="text-center <?php echo $bg; ?>"><?php echo ($competition[COMPETITION_EN_COURS] == 1) ? "Oui" : "Non"; ?></td>
                        <td class="text-center <?php echo $bg; ?>"><?php echo ($competition[COMPETITION_VISIBLE_SUR_SITE] == 1) ? "Oui" : "Non"; ?></td>
                        <td class="text-center <?php echo $bg; ?>">
                            <span class="glyphicon glyphicon-pencil clickable" title="Editer" id-competition="<?php echo $competition[COMPETITION_ID]; ?>"></span>
                        </td>
                        <?php
                        if (isAdminConnected_bureau()) {
                            ?>
                            <td class="text-center <?php echo $bg; ?>">
                                <span class="glyphicon glyphicon-trash clickable" title="Supprimer" id-competition="<?php echo $competition[COMPETITION_ID]; ?>"></span>
                                <div class="loader loader-delete" id-competition="<?php echo $competition[COMPETITION_ID]; ?>"></div>
                            </td>
                            <?php
                        }
                        if (isAdminConnected_superadmin()) {
                            if (!$ctrl->isCompetitionVisible($competition)) {
                                ?>
                                <td class="text-center <?php echo $bg; ?>">
                                    <span class="glyphicon glyphicon-ok icon-validation clickable" title="Valider" id-competition="<?php echo $competition[COMPETITION_ID]; ?>"></span>
                                    <div class="loader loader-validation" id-competition="<?php echo $competition[COMPETITION_ID]; ?>"></div>
                                </td>
                                <?php
                            } else {
                                ?>
                                <td class="text-center <?php echo $bg; ?>">
                                    <span class="glyphicon glyphicon-remove icon-validation clickable" title="Invalider" id-competition="<?php echo $competition[COMPETITION_ID]; ?>"></span>
                                    <div class="loader loader-validation" id-competition="<?php echo $competition[COMPETITION_ID]; ?>"></div>
                                </td>
                                <?php
                            }
                        }
                        ?>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>