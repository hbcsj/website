<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/competition-dao.php");
require_once("common/php/dao/categorie-dao.php");
require_once("common/php/dao/saison-dao.php");

class TableCompetitionsCtrl extends AbstractViewCtrl {

	private $competitionDAO;
	private $categorieDAO;
	private $saisonDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_coach()) {
			$this->competitionDAO = new CompetitionDAO($this->getDatabaseConnection());
			$this->categorieDAO = new CategorieDAO($this->getDatabaseConnection());
			$this->saisonDAO = new SaisonDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getCompetitions() {
		return $this->competitionDAO->getAllByParams(
			trim(strtolower($_GET["nom"])),
			$_GET["categorieId"],
			$_GET["saisonId"],
			$_GET["numEquipe"],
			$_GET["enCours"],
			$_GET["visible"],
			COMPETITION_TABLE_NAME.".".COMPETITION_CATEGORIE_ID.", ".COMPETITION_TABLE_NAME.".".COMPETITION_NUM_EQUIPE
		);
	}

	public function getLibelleSaison($saisonId) {
		$saison = $this->saisonDAO->getById($saisonId);
		return $saison[SAISON_LIBELLE];
	}

	public function getNomEquipe($competition) {
		if ($competition[COMPETITION_CATEGORIE_ID] != null) {
			$categorie = $this->categorieDAO->getById($competition[COMPETITION_CATEGORIE_ID]);
			return trim($categorie[CATEGORIE_NOM]." ".$competition[COMPETITION_NUM_EQUIPE]);
		}
		return "";
	}

	public function isCompetitionVisible($competition) {
		return (
			$competition[COMPETITION_VISIBLE_SUR_SITE] && 
			$competition[COMPETITION_EMAIL_DERNIER_EDITEUR] == $GLOBALS[CONF][SUPERADMIN_EMAIL]
		);
	}
}

?>