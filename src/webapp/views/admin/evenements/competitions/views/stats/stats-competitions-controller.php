<?php
require_once("core/php/controllers/abstract-view-controller.php");
require_once("common/php/dao/competition-dao.php");

class StatsCompetitionsCtrl extends AbstractViewCtrl {

	private $competitionDAO;

    public function __construct($viewName) {
		parent::__construct($viewName, null, true);

		if (isAdminConnected_coach()) {
			$this->competitionDAO = new CompetitionDAO($this->getDatabaseConnection());
		} else {
			$this->sendCheckError(
				HTTP_401, 
				"Vous n'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette vue", 
				"webapp/views/common/error/401/401.html.php"
			);
		}
	}

	public function getNbCompetitions() {
		return $this->competitionDAO->getNbTotal()[COMPETITION_NB_COMPETITIONS];
	}

	public function getNbCompetitionsEnCours() {
		return $this->competitionDAO->getNbEnCours()[COMPETITION_NB_COMPETITIONS];
	}

	public function getNbCompetitionsVisibles() {
		return $this->competitionDAO->getNbVisibles()[COMPETITION_NB_COMPETITIONS];
	}

	public function getNbCompetitionsAValider() {
		return $this->competitionDAO->getNbAValider()[COMPETITION_NB_COMPETITIONS];
	}
}

?>