<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/evenements/competitions/views/stats/", 
    "stats-competitions", 
    "StatsCompetitionsCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
    ?>
    <div class="container-stats__section">
        <div class="container-stats__section__title">Nombre de comp&eacute;titions : </div>
        <div class="container-stats__section__value"><?php echo $ctrl->getNbCompetitions(); ?></div>
    </div>
    <div class="container-stats__section">
        <div class="container-stats__section__title">
            Dont en cours :
            <br>
            Dont visibles :
        </div>
        <div class="container-stats__section__value">
            <?php echo $ctrl->getNbCompetitionsEnCours(); ?>
            <br>
            <?php echo $ctrl->getNbCompetitionsVisibles(); ?>
        </div>
    </div>
    <div class="container-stats__section">
        <div class="container-stats__section__title">A valider : </div>
        <div class="container-stats__section__value"><?php echo $ctrl->getNbCompetitionsAValider(); ?></div>
    </div>
    <?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>