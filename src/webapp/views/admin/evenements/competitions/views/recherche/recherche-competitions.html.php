<?php
require_once("core/php/resources/view.php");
require_once("common/php/lib/admin-utils.php");

$view = new View(
    "webapp/views/admin/evenements/competitions/views/recherche/", 
    "recherche-competitions", 
    "RechercheCompetitionsCtrl"
);
$ctrl = $view->getController();

if (isAdminConnected_coach()) {
    $categories = $ctrl->getCategories();
    $saisons = $ctrl->getSaisons();
    $numEquipeMax = $ctrl->getNumEquipeMax();
    ?>
    <div class="container-recherche__title" title="Ouvrir le formulaire de recherche" for="form-recherche-competitions">
        <div class="container-recherche__title__icon">
            <span class="glyphicon glyphicon-search"></span>
        </div>
        <div class="container-recherche__title__text">Recherche</div>
        <div class="container-recherche__title__arrow">
            <span class="glyphicon glyphicon-triangle-bottom"></span>
        </div>
    </div>
    <div class="form-horizontal" id="form-recherche-competitions">
        <div class="form-group">
            <label for="form-recherche-competitions__input-nom" class="col-xs-2 control-label">Nom :</label>
            <div class="col-xs-2 form-input" for="form-recherche-competitions__input-nom">
                <input type="text" class="form-control" id="form-recherche-competitions__input-nom">
            </div>
            <label for="form-recherche-competitions__input-categorie" class="col-xs-2 control-label">Cat&eacute;gorie</label>
            <div class="col-xs-2 form-input" for="form-recherche-competitions__input-categorie">
                <select class="form-control" id="form-recherche-competitions__input-categorie">
                    <option value=""></option>
                    <?php
                    if (sizeof($categories) > 0) {
                        foreach ($categories as $categorie) {
                            ?>
                            <option value="<?php echo $categorie[CATEGORIE_ID]; ?>"><?php echo $categorie[CATEGORIE_NOM]; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <label for="form-recherche-competitions__input-num-equipe" class="col-xs-2 control-label">Num. &eacute;quipe :</label>
            <div class="col-xs-1 form-input" for="form-recherche-competitions__input-num-equipe">
                <select class="form-control" id="form-recherche-competitions__input-num-equipe">
                    <option value=""></option>
                    <?php
                    for ($numEquipe = 1 ; $numEquipe <= $numEquipeMax ; $numEquipe++) {
                        ?>
                        <option value="<?php echo $numEquipe; ?>"><?php echo $numEquipe; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="form-recherche-competitions__input-saison" class="col-xs-2 control-label">Saison :</label>
            <div class="col-xs-2 form-input" for="form-recherche-competitions__input-saison">
                <select class="form-control" id="form-recherche-competitions__input-saison">
                    <option value=""></option>
                    <?php
                    if (sizeof($saisons) > 0) {
                        foreach ($saisons as $saison) {
                            ?>
                            <option value="<?php echo $saison[SAISON_ID]; ?>"><?php echo $saison[SAISON_LIBELLE]; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <label for="form-recherche-competitions__input-is-en-cours" class="col-xs-2 control-label">En cours ?</label>
            <div class="col-xs-1 form-input" for="form-recherche-competitions__input-is-en-cours">
                <select class="form-control" id="form-recherche-competitions__input-is-en-cours">
                    <option value=""></option>
                    <option value="1">Oui</option>
                    <option value="0">Non</option>
                </select>
            </div>
            <label for="form-recherche-competitions__input-is-visible" class="col-xs-2 col-xs-offset-1 control-label">Visible ?</label>
            <div class="col-xs-1 form-input" for="form-recherche-competitions__input-is-visible">
                <select class="form-control" id="form-recherche-competitions__input-is-visible">
                    <option value=""></option>
                    <option value="1">Oui</option>
                    <option value="0">Non</option>
                </select>
            </div>
        </div>
    </div>
    <div class="container-recherche__footer" for="form-recherche-competitions">
        <button id="recherche-competitions__bt-rechercher" class="btn btn-default">
            <div class="button__icon">
                <span class="glyphicon glyphicon-search"></span>
            </div>
            <div class="button__text">Chercher</div>
        </button>
        <button id="recherche-competitions__bt-reset" class="btn btn-default">
            <div class="button__icon">
                <span class="glyphicon glyphicon-erase"></span>
            </div>
            <div class="button__text">Reset</div>
        </button>
    </div>
<?php
} else {
    $ctrl->sendCheckError(HTTP_401, null, "webapp/views/common/error/401/401.html.php");
}
?>