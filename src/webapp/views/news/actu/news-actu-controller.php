<?php
require_once("core/php/controllers/abstract-page-controller.php");
require_once("common/php/dao/actualite-dao.php");

class NewsActuCtrl extends AbstractPageCtrl {
    
    private $actualite;

    private $actualiteDAO;

    public function __construct($pageName) {
		parent::__construct($pageName, array(
            "id" => $_GET["id"]
		), true);

        $this->actualiteDAO = new ActualiteDAO($this->getDatabaseConnection());

        $this->checkRequest();
	}
    
    private function checkRequest() {
		$checkParams = $this->checkParams(array(
			array(GET, "id")
		));
		if ($checkParams) {
			$this->checkIdActualite();
		}
	}

	private function checkIdActualite() {
		$this->actualite = $this->actualiteDAO->getById($_GET["id"]);
			
		if ($this->actualite == null) {
			$this->sendCheckError(
				HTTP_404, 
				"L'actualite (idActualite = '".$_GET["id"]."') n'existe pas", 
				"404"
			);
		}
    }
    
    public function getActualite() {
        return $this->actualite;
    }
}

?>