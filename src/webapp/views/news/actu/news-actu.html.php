<?php
require_once("core/php/resources/page.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/path-utils.php");

$page = new Page(
    "webapp/views/news/actu/", 
    "news-actu", 
    "NewsActuCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/common/header/header.html.php");
require_once("webapp/views/common/nav/nav.html.php");

$actualite = $ctrl->getActualite();
?>

<div class="container-fluid animated fadeIn">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
            <h1>
                <?php echo $actualite[ACTUALITE_TITRE]; ?>
                <span class="date-publication">
                    Publi&eacute; le <?php echo str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDateTime_to_slashDateTime($actualite[ACTUALITE_DATE_HEURE])); ?>
                </span>
            </h1>
            <div class="div-img-share-social-actu">
                <img src="<?php echo SRC_PATH.PathUtils::getActualiteImgFile($actualite[ACTUALITE_ID]); ?>">
            </div>
            <?php echo FileUtils::getFileContent(PathUtils::getActualiteFile($actualite[ACTUALITE_ID])); ?>
        </div>
    </div>
</div>

<?php
require_once("webapp/views/common/footer/footer.html.php");

require_once("webapp/views/common/modals/alert/modal-alert.html.php");
require_once("webapp/views/common/modals/img/modal-img.html.php");
?>

<?php
$page->finalizePage();
?>