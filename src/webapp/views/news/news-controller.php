<?php
require_once("core/php/controllers/abstract-page-controller.php");
require_once("common/php/dao/actualite-dao.php");

class NewsCtrl extends AbstractPageCtrl {
    
    private $actualiteDAO;

    public function __construct($pageName) {
		parent::__construct($pageName, null, true);

        $this->actualiteDAO = new ActualiteDAO($this->getDatabaseConnection());
	}
    
    public function getActualites() {
        return $this->actualiteDAO->getVisibles(
			ACTUALITE_TABLE_NAME.".".ACTUALITE_DATE_HEURE." DESC"
		);
    }
}

?>