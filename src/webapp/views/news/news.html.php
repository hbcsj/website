<?php
require_once("core/php/resources/page.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/path-utils.php");
require_once("lib/php/mobile-detect.php");

$page = new Page(
    "webapp/views/news/", 
    "news", 
    "NewsCtrl"
);
$ctrl = $page->getController();

require_once("webapp/views/common/header/header.html.php");
require_once("webapp/views/common/nav/nav.html.php");

$mobileDetect = new Mobile_Detect();
$news = $ctrl->getActualites();
?>

<div class="container-fluid animated fadeIn">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
            <h1>Actualit&eacute;s</h1>
        </div>
    </div>
    <?php
    if ($mobileDetect->isMobile() || $mobileDetect->isTablet()) {
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <?php
                if (sizeof($news) > 0) {
                    $i = 0;
                    foreach ($news as $actu) {
                        $classBordered = "";
                        if ($i < (sizeof($news) - 1)) {
                            $classBordered = " row-actu-bordered";
                        }
                        ?>
                        <div class="row row-actu<?php echo $classBordered; ?>">
                            <div class="col-xs-5">
                                <a class="link-with-img" href="<?php echo ROOT_PATH; ?>news/actu?id=<?php echo $actu[ACTUALITE_ID]; ?>" title="Lire l'article complet">
                                    <img src="<?php echo SRC_PATH.PathUtils::getActualiteImgFile($actu[ACTUALITE_ID]); ?>" class="img-actu">
                                </a>
                                <div class="div-link-bottom-image">
                                    <a href="<?php echo ROOT_PATH; ?>news/actu?id=<?php echo $actu[ACTUALITE_ID]; ?>">Lire l'article complet</a>
                                </div>
                            </div>
                            <div class="col-xs-6 col-xs-offset-1">
                                <div class="date-publication">
                                    Publi&eacute; le <?php echo str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDateTime_to_slashDateTime($actu[ACTUALITE_DATE_HEURE])); ?>
                                </div>
                                <div class="titre-actu"><?php echo $actu[ACTUALITE_TITRE]; ?></div>
                                <div class="resume-actu">
                                    <?php echo nl2br(FileUtils::getFileContent(PathUtils::getActualiteHomeFile($actu[ACTUALITE_ID]))); ?>
                                </div>
                            </div>
                        </div>
                        <?php
                        $i++;
                    }
                }
                ?>
            </div>
        </div>
        <?php
    } else {
        ?>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <?php
                if (sizeof($news) > 0) {
                    $i = 0;
                    foreach ($news as $actu) {
                        $classBordered = "";
                        if ($i < (sizeof($news) - 1)) {
                            $classBordered = " row-actu-bordered";
                        }
                        ?>
                        <div class="row row-actu<?php echo $classBordered; ?>">
                            <div class="col-xs-4">
                                <a class="link-with-img" href="<?php echo ROOT_PATH; ?>news/actu?id=<?php echo $actu[ACTUALITE_ID]; ?>" title="Lire l'article complet">
                                    <img src="<?php echo SRC_PATH.PathUtils::getActualiteImgFile($actu[ACTUALITE_ID]); ?>" class="img-actu">
                                </a>
                                <div class="div-link-bottom-image">
                                    <a href="<?php echo ROOT_PATH; ?>news/actu?id=<?php echo $actu[ACTUALITE_ID]; ?>">Lire l'article complet</a>
                                </div>
                            </div>
                            <div class="col-xs-7 col-xs-offset-1">
                                <div class="date-publication">
                                    Publi&eacute; le <?php echo str_replace(SLASH_DATE_TIME_SEPARATOR, " &agrave; ", DateUtils::convert_sqlDateTime_to_slashDateTime($actu[ACTUALITE_DATE_HEURE])); ?>
                                </div>
                                <div class="titre-actu"><?php echo $actu[ACTUALITE_TITRE]; ?></div>
                                <div class="resume-actu">
                                    <?php echo nl2br(FileUtils::getFileContent(PathUtils::getActualiteHomeFile($actu[ACTUALITE_ID]))); ?>
                                </div>
                            </div>
                        </div>
                        <?php
                        $i++;
                    }
                }
                ?>
            </div>
        </div>
        <?php
    }
?>
</div>

<?php
require_once("webapp/views/common/footer/footer.html.php");

require_once("webapp/views/common/modals/alert/modal-alert.html.php");
?>

<?php
$page->finalizePage();
?>