AjaxUtils = {};

AjaxUtils.servicePath = '_service_';
AjaxUtils.viewPath = '_view_';

AjaxUtils.methodParamName = 'method';

AjaxUtils.dataTypes = {};
AjaxUtils.dataTypes.text = 'text';
AjaxUtils.dataTypes.html = 'html';
AjaxUtils.dataTypes.json = 'json';

AjaxUtils.requestTypes = {};
AjaxUtils.requestTypes.get = 'get';
AjaxUtils.requestTypes.post = 'post';

AjaxUtils.callService = function(serviceName, method, dataType, httpMethod, getParameters, postParameters, callback, callbackError) {
    var stringParameters = '';
    if (getParameters != null) {
        stringParameters = '&'+$.param(getParameters);
    }
    var url = rootPath+AjaxUtils.servicePath+'/'+serviceName+'?'+AjaxUtils.methodParamName+'='+method+stringParameters;
    var response = null;
	var request = $.ajax({
        async: true, 
        dataType: dataType, 
        type: httpMethod,
        url: url,
        data: postParameters,
        error: function(xhr, msg, e) {
            response = {};
            response.status = xhr.status;
            response.message = e;
        }, 
        success: function(requestResponse) {
            response = requestResponse;
        }
    });
    $.when(request).then(
        function() { 
            if (callback != null) {
                callback(response);
            }
        }, 
        function() {
            if (callbackError != null) {
                callbackError(response);
            }
        }
    );
};

AjaxUtils.loadView = function(viewPath, elemSelector, getParameters, callback, loaderSelector) {
    if (loaderSelector != undefined && loaderSelector != null) {
        $(loaderSelector).show();
    }

    var stringParameters = '';
    if (getParameters != null) {
        stringParameters = $.param(getParameters);
    }
    var url = rootPath+AjaxUtils.viewPath+'/'+viewPath+'?'+stringParameters;
    var request = $.ajax({
        async: true, 
        dataType: AjaxUtils.dataTypes.html, 
        type: AjaxUtils.requestTypes.get,
        url: url,
        data: {},
        error: function(xhr, msg, e) {
            response = {};
            response.status = xhr.status;
            response.message = e;
        }, 
        success: function(requestResponse) {
            response = requestResponse;
        }
    });
    $.when(request).then(
        function() { 
            $(elemSelector).html(response);

            if (loaderSelector != undefined && loaderSelector != null) {
                $(loaderSelector).hide();
            }
            
            if (callback != null) {
                callback();
            }
        }, 
        function() {
            if (loaderSelector != undefined && loaderSelector != null) {
                $(loaderSelector).hide();
            }

            $(elemSelector).html('<div class="container-error"><h1>Error '+response.status+'</h1><h2>'+response.message+'</h2></div>');
        }
    );
};