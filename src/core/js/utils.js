Utils = {};

Utils.exists = function(elemSelector) {
    return $(elemSelector).length > 0;
};

Utils.getUrlParams = function() {
    var params = {};
    var url = window.location.href;
    if (url.indexOf("?") != -1) {
        var urlSplitted = url.split('?');
        var paramsString = urlSplitted[1].split('&');
        for (var i = 0 ; i < paramsString.length ; i++) {
            var paramsStringSplitted = paramsString[i].split('=');
            params[paramsStringSplitted[0]] = paramsStringSplitted[1];
        }
    }
    return params;
};