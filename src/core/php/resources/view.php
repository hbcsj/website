<?php

class View {

    private $controller;

    public function __construct($viewPath, $viewName, $controller) {
        IncludeUtils::callViewController($viewPath, $viewName);
        $this->controller = new $controller($viewName);
    }

    public function getController() {
        return $this->controller;
    }
}

?>