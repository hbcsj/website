<?php

class Page {

    const LESS_FILE = "less.".JS_EXTENSION;
    const INIT_PAGE_FILE = "init-page.".JS_EXTENSION;

    private $controller;

    public function __construct($pagePath, $pageName, $controller) {
        $this->initPage($pagePath, $pageName);
        $this->buildPageController($pagePath, $pageName, $controller);
    }

    private function buildPageController($pagePath, $pageName, $controller) {
        IncludeUtils::callPageController($pagePath, $pageName);
        $this->controller = new $controller($pageName);
    }

    private function initPage($pagePath, $pageName) {
        $srcPath = ROOT_PATH;
        if ($GLOBALS[CONF][IS_DEV]) {
            $srcPath .= SRC_FOLDER;
        }

        $pageConfig = $this->getPageConfig($pagePath, $pageName);

        $html = "<html>\n";
        $html .= "<head>\n";
        $html .= "<meta charset=\"UTF-8\">\n";
        $html .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n";
        $html .= "<link rel=\"shortcut icon\" type=\"image/ico\" href=\"/favicon.ico\"/>\n";

        if ($pageConfig->metas != null) {
            if ($pageConfig->metas->keywords != null) {
                $html .= "<meta name=\"keywords\" content=\"".$pageConfig->metas->keywords."\">\n";
            }
            if ($pageConfig->metas->description != null) {
                $html .= "<meta name=\"description\" content=\"".$pageConfig->metas->description."\">\n";
            }
        }

        $html .= $this->getCSSFiles($pagePath, $pageName, $pageConfig, $srcPath);
        $html .= $this->getJSFiles($pagePath, $pageName, $pageConfig, $srcPath);
        
        if ($pageConfig->title != null) {
            $html .= "<title>".$pageConfig->title."</title>\n";
        }
        $html .= "</head>\n";
        $html .= "<body root=\"".ROOT_PATH."\">\n";
        
        echo $html;
    }

    public function finalizePage() {
        $html = "</body>\n";
        $html .= "</html>\n";
        echo $html;
    }

    private function getPageConfig($pagePath, $pageName) {
        $pageConfigFile = $pagePath.$pageName.PAGE_CONFIG_SUFFIX.PAGE_CONFIG_EXTENSION;

        $config = null;
        if (file_exists($pageConfigFile)) {
            $fp = fopen($pageConfigFile, "r");
            $pageConfigFileContent = fread($fp, filesize($pageConfigFile));
            fclose($fp);
            $config = json_decode($pageConfigFileContent);
        } else {
            $config = $this->getDefaultPageConfig();
        }

        return $config;
    }

    private function getDefaultPageConfig() {
        $config = new stdClass;
        $config->title = "";
        $config->js = array();
        $config->css = array();
        return $config;
    }

    private function getPagePath($pageName) {
        $pageNameSplitted = explode(RESOURCE_NAME_SEPARATOR, $pageName);

        $pagePath = VIEWS_FOLDER;

        foreach ($pageNameSplitted as $pageNamePart) {
            $pagePath .= $pageNamePart."/";
        }

        return $pagePath;
    }

    private function getCSSFiles($pagePath, $pageName, $pageConfig, $srcPath) {
        $html = "";

        if ($pageConfig->css != null && sizeof($pageConfig->css) > 0) {
            foreach ($pageConfig->css as $cssLib) {
                $cssLibFilePath = $cssLib.".".CSS_EXTENSION;
                if (file_exists($cssLibFilePath)) {
                    $html .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"".$srcPath.$cssLibFilePath."?".time()."\" />\n";
                }
            }
        }

        $cssExtension = CSS_EXTENSION;
        $stylesheetSuffix = "";
        if ($GLOBALS[CONF][IS_DEV]) {
            $cssExtension = LESS_EXTENSION;
            $stylesheetSuffix = "/less";
        }
        $cssPageFilePath = $pagePath.$pageName.".".$cssExtension;
        if (file_exists($cssPageFilePath)) {
            $html .= "<link rel=\"stylesheet".$stylesheetSuffix."\" type=\"text/css\" href=\"".$srcPath.$cssPageFilePath."?".time()."\" />\n";
        }

        return $html;
    }

    private function getJSFiles($pagePath, $pageName, $pageConfig, $srcPath) {
        $html = "";

        if ($GLOBALS[CONF][IS_DEV]) {
            $html .= "<script type=\"text/javascript\" src=\"".$srcPath.LIB_FOLDER.JS_FOLDER.self::LESS_FILE."?".time()."\"></script>\n";
        }
        
        if ($pageConfig->js != null && sizeof($pageConfig->js) > 0) {
            foreach ($pageConfig->js as $jsLib) {
                $jsLibFilePath = $jsLib.".".JS_EXTENSION;
                if (file_exists($jsLibFilePath)) {
                    $html .= "<script type=\"text/javascript\" src=\"".$srcPath.$jsLibFilePath."?".time()."\"></script>\n";
                }
            }
        }

        $html .= "<script type=\"text/javascript\" src=\"".$srcPath.CORE_FOLDER.JS_FOLDER.self::INIT_PAGE_FILE."?".time()."\"></script>\n";

        $jsPageFilePath = $pagePath.$pageName.CONTROLLER_SUFFIX.".".JS_EXTENSION;
        if (file_exists($jsPageFilePath)) {
            $html .= "<script type=\"text/javascript\" src=\"".$srcPath.$jsPageFilePath."?".time()."\"></script>\n";
        }

        return $html;
    }

    public function getController() {
        return $this->controller;
    }
}

?>