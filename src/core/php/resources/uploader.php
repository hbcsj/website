<?php

class Uploader {

    const INIT_UPLOADER_FILE = "init-uploader.".JS_EXTENSION;

    private $controller;

    public function __construct($uploaderPath, $uploaderName, $controller) {
        $this->initUploader($uploaderPath, $uploaderName);
        $this->buildUploaderController($uploaderPath, $uploaderName, $controller);
    }

    private function buildUploaderController($uploaderPath, $uploaderName, $controller) {
        IncludeUtils::callUploaderController($uploaderPath, $uploaderName);
        $this->controller = new $controller($uploaderName);
    }

    private function initUploader($uploaderPath, $uploaderName) {
        $nbDotDotSlashes = sizeof(explode("/", $uploaderPath)) - 3;
        $dotDotSlashes = "";
        for ($i = 0 ; $i < $nbDotDotSlashes ; $i++) {
            $dotDotSlashes .= "../";
        }
        $srcPath = $dotDotSlashes;
        if ($GLOBALS[CONF][IS_DEV]) {
            $srcPath .= SRC_FOLDER;
        }

        $uploaderConfig = self::getUploaderConfig($uploaderPath, $uploaderName);

        $html = "<html>\n";
        $html .= "<head>\n";
        $html .= "<meta charset=\"UTF-8\">\n";
        $html .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n";
        $html .= self::getJSFiles($uploaderPath, $uploaderName, $uploaderConfig, $srcPath);
        $html .= "</head>\n";
        $html .= "<body root=\"".ROOT_PATH."\">\n";
        
        echo $html;
    }

    public function finalizeUploader() {
        $html = "</body>\n";
        $html .= "</html>\n";
        echo $html;
    }

    private function getUploaderConfig($uploaderPath, $uploaderName) {
        $uploaderConfigFile = $uploaderPath.$uploaderName.UPLOADER_CONFIG_SUFFIX.UPLOADER_CONFIG_EXTENSION;

        $config = null;
        if (file_exists($uploaderConfigFile)) {
            $fp = fopen($uploaderConfigFile, "r");
            $uploaderConfigFileContent = fread($fp, filesize($uploaderConfigFile));
            fclose($fp);
            $config = json_decode($uploaderConfigFileContent);
        } else {
            $config = $this->getDefaultUploaderConfig();
        }

        return $config;
    }

    private function getDefaultUploaderConfig() {
        $config = new stdClass;
        $config->title = "";
        $config->js = array();
        return $config;
    }

    private function getUploaderPath($uploaderName) {
        $uploaderNameSplitted = explode(RESOURCE_NAME_SEPARATOR, $uploaderName);

        $uploaderPath = UPLOADER_FOLDER;

        foreach ($uploaderNameSplitted as $uploaderNamePart) {
            $uploaderPath .= $uploaderNamePart."/";
        }

        return $uploaderPath;
    }

    private function getJSFiles($uploaderPath, $uploaderName, $uploaderConfig, $srcPath) {
        $html = "";

        if ($uploaderConfig->js != null && sizeof($uploaderConfig->js) > 0) {
            foreach ($uploaderConfig->js as $jsLib) {
                $jsLibFilePath = $jsLib.".".JS_EXTENSION;
                if (file_exists($jsLibFilePath)) {
                    $html .= "<script type=\"text/javascript\" src=\"".$srcPath.$jsLibFilePath."?".time()."\"></script>\n";
                }
            }
        }

        $html .= "<script type=\"text/javascript\" src=\"".$srcPath.CORE_FOLDER.JS_FOLDER.self::INIT_UPLOADER_FILE."?".time()."\"></script>\n";

        $jsUploaderFilePath = $uploaderPath.$uploaderName.CONTROLLER_SUFFIX.".".JS_EXTENSION;
        if (file_exists($jsUploaderFilePath)) {
            $html .= "<script type=\"text/javascript\" src=\"".$srcPath.$jsUploaderFilePath."?".time()."\"></script>\n";
        }

        return $html;
    }

    public function getController() {
        return $this->controller;
    }
}

?>