<?php

class Service {

    public function __construct($servicePath, $serviceName, $controller) {
        IncludeUtils::callServiceController($servicePath, $serviceName);
        new $controller($serviceName);
    }
}

?>