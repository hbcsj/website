<?php

define(URL_GET_PARAM, "url");

define(RESOURCE_NAME_SEPARATOR, "-");

define(SRC_FOLDER, "src/");
define(JS_FOLDER, "js/");
define(LIB_FOLDER, "lib/");
define(CORE_FOLDER, "core/");
define(WEBAPP_FOLDER, "webapp/");
define(VIEWS_FOLDER, WEBAPP_FOLDER."views/");
define(CONTROLLER_SUFFIX, "-controller");
define(PAGE_CONFIG_SUFFIX, "-config");
define(PAGE_CONFIG_EXTENSION, ".json");
define(UPLOADER_CONFIG_SUFFIX, "-config");
define(UPLOADER_CONFIG_EXTENSION, ".json");
define(VIEW_EXTENSION, ".html.php");
define(SERVICES_FOLDER, WEBAPP_FOLDER."services/");
define(SERVICES_EXTENSION, ".service.php");
define(UPLOADERS_FOLDER, WEBAPP_FOLDER."uploaders/");
define(UPLOADERS_EXTENSION, ".uploader.php");
define(VIEW_404, "404");

define(JS_EXTENSION, "js");
define(LESS_EXTENSION, "less");
define(CSS_EXTENSION, "css");
define(PHP_EXTENSION, "php");

?>