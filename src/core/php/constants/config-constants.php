<?php

define(CONF_FILE, "conf/config.ini");
define(CONF, "conf");

define(DEFAULT_PAGE, "default_page");
define(LOGS_FOLDER, "logs_folder");
define(IS_DEV, "is_dev");
define(MAILS_IN_TEST, "mails_in_test");

define(DATABASE_SYSTEM, "database_system");
define(DATABASE_HOSTNAME, "database_hostname");
define(DATABASE_LOGIN, "database_login");
define(DATABASE_PASSWORD, "database_password");
define(DATABASE_NAME, "database_name");

define(SUPERADMIN_EMAIL, "superadmin_email");

define(DEFAULT_DEFAULT_PAGE, "home");
define(DEFAULT_LOGS_FOLDER, "_logs");
define(DEFAULT_IS_DEV, false);
define(DEFAULT_MAILS_IN_TEST, false);

define(DEFAULT_DATABASE_SYSTEM, "");
define(DEFAULT_DATABASE_HOSTNAME, "");
define(DEFAULT_DATABASE_LOGIN, "");
define(DEFAULT_DATABASE_PASSWORD, "");
define(DEFAULT_DATABASE_NAME, "");

define(DEFAULT_SUPERADMIN_EMAIL, "");

?>