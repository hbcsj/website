<?php

define(HTTP_400, "HTTP/1.0 400 Bad Request");
define(HTTP_401, "HTTP/1.0 401 Unauthorized");
define(HTTP_404, "HTTP/1.0 404 Not Found");

define(GET, "GET");
define(POST, "POST");
define(FILES, "FILES");
define(SESSION, "SESSION");

define(CONTENT_TYPE_TEXT, "text/plain");
define(CONTENT_TYPE_HTML, "text/html");
define(CONTENT_TYPE_JSON, "application/json");

?>