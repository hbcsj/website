<?php

class Mailer {

    const MAILER_FILE = "mailer.php";

    private $senderMail;
    private $recipientMails;
    private $mailsCc;
    private $mailsCci;
    private $subject;
    private $message;
    private $style;
    private $attachedFile;

    public function __construct($senderMail, $recipientMails, $subject, $message) {
		$this->senderMail = $senderMail;
        $this->recipientMails = $recipientMails;
        $this->mailsCc = null;
        $this->mailsCci = null;
		$this->subject = $subject;
        $this->message = $message;
        $this->attachedFile = null;
	}
    
    public function send() {
        if ($GLOBALS[CONF][MAILS_IN_TEST]) {
            $this->recipientMails = $GLOBALS[CONF][SUPERADMIN_EMAIL];
        }

        if ($this->attachedFile != null) {
            return $this->sendWithFile();
        } else {
            return $this->sendWithoutFile();
        }
    }

    private function getHeaders($contentType) {
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: ".$contentType."; charset=UTF-8\r\n";
        $headers .= "To: ".$this->recipientMails."\r\n";
        $headers .= "From: ".$this->senderMail."\r\n";
        if (!$GLOBALS[CONF][MAILS_IN_TEST]) {
            if ($this->mailsCc != null) {
                $headers .= "Cc: ".$this->mailsCc."\r\n";
            }
            if ($this->mailsCci != null) {
                $headers .= "Bcc: ".$this->mailsCci."\r\n";
            }
        }

        return $headers;
    }

    public function getHTMLMessage() {
        $styleCode = "";
        if ($this->style != "") {
            $styleCode = "<style type=\"text/css\">";
            $styleCode .= $this->style;
            $styleCode .= "</style>";
        }
        return "<html><head>".$styleCode."<title>".$this->subject."</title></head><body>".$this->message."</body></html>";
    }

    private function sendWithoutFile() {
        $headers = $this->getHeaders("text/html");
        
        $message = $this->getHTMLMessage();
        
        $resMail = true;
        if (!$GLOBALS[CONF][IS_DEV]) {
            $resMail = mail($this->recipientMails, $this->subject, $message, $headers);
        }
        
        if (!$resMail) {
            Logger::logError(MAILER_FILE, "Error during mail sending by '".$this->senderMail."' for '".$this->recipientMails."' (".$this->subject.")");
        }
        
        return $resMail;
    }

    private function sendWithFile() {
        $boundary = "-----=".md5(rand());
        $boundaryAlt = "-----=".md5(rand());

        $headers = $this->getHeaders("multipart/mixed");
        $headers .= " boundary=\"".$boundary."\"\r\n";

        $message = "\r\n--".$boundary."\r\n";
        $message .= "Content-Type: multipart/alternative;\r\n boundary=\"".$boundaryAlt."\"\r\n";
        $message .= "\r\n--".$boundaryAlt."\r\n";

        $message .= "Content-type: text/plain; charset=UTF-8\r\n";
        $message .= "Content-Transfer-Encoding: 8bit\r\n";
        $message .= "\r\nTEST\r\n";
        $message .= "\r\n--".$boundaryAlt."\r\n";

        $message .= "Content-type: text/html; charset=UTF-8\r\n";
        $message .= "Content-Transfer-Encoding: 8bit\r\n";
        $message .= "\r\n".$this->getHTMLMessage()."\r\n";
        $message .= "\r\n--".$boundaryAlt."--\r\n";
        $message .= "\r\n--".$boundary."--\r\n";
        
        $fp = fopen($this->attachedFile["path"], "r");
        $attachment = fread($fp, filesize($this->attachedFile["path"]));
        $attachment = chunk_split(base64_encode($attachment));
        fclose($fp);

        $message .= "Content-Type: ".$this->attachedFile["content-type"]."; name=\"".$this->attachedFile["name"]."\"\r\n";
        $message .= "Content-Transfer-Encoding: base64\r\n";
        $message .= "Content-Disposition: attachment; filename=\"".$this->attachedFile["name"]."\"\r\n";
        $message .= "\r\n".$attachment."\r\n\r\n";
        $message .= "\r\n--".$boundary."--\r\n";

        $resMail = true;
        if (!$GLOBALS[CONF][IS_DEV]) {
            $resMail = mail($this->recipientMails, $this->subject, $message, $headers);
        }
    
        if (!$resMail) {
            Logger::logError(MAILER_FILE, "Error during mail with file sending by de '".$this->senderMail."' for '".$this->recipientMails."' (".$this->subject.")");
        }
        
        return $resMail;
    }
    
    public function setMailsCc($mailsCc) {
        $this->mailsCc = $mailsCc;
    }
    
    public function setMailsCci($mailsCci) {
        $this->mailsCci = $mailsCci;
    }
    
    public function setStyle($style) {
        $this->style = $style;
    }

    public function setAttachedFile($attachedFile) {
        $this->attachedFile = $attachedFile;
    }
}

?>