<?php

class IncludeUtils {

    public static function callPageController($pagePath, $pageName) {
        require_once($pagePath.$pageName.CONTROLLER_SUFFIX.".".PHP_EXTENSION);
    }

    public static function callViewController($viewPath, $viewName) {
        require_once($viewPath.$viewName.CONTROLLER_SUFFIX.".".PHP_EXTENSION);
    }

    public static function callServiceController($servicePath, $serviceName) {
        require_once($servicePath.$serviceName.CONTROLLER_SUFFIX.".".PHP_EXTENSION);
    }

    public static function callUploaderController($uploaderPath, $uploaderName) {
        require_once($uploaderPath.$uploaderName.CONTROLLER_SUFFIX.".".PHP_EXTENSION);
    }
}