<?php

class Logger {

    const LOG_SEPARATOR = "\t";
    const DATE_FORMAT = "d/m/Y H:i:s";

    const DEBUG_FILENAME_PREFIX = "debug";
    const NAVIGATION_FILENAME_PREFIX = "navigation";
    const SQL_FILENAME_PREFIX = "sql";
    const LOGS_FILENAME_PREFIX = "logs";

    public static function logDebug($txt) {
        $debugFilename = self::DEBUG_FILENAME_PREFIX."-".date("Ymd").".txt";
        $fp = fopen($GLOBALS[CONF][LOGS_FOLDER]."/".$debugFilename, "a");
        fputs($fp, date(self::DATE_FORMAT).self::LOG_SEPARATOR.$txt."\n");
        fclose($fp);
    }

    public static function logNavigation($resourceName, $params) {
        $navigationFilename = self::NAVIGATION_FILENAME_PREFIX."-".date("Ymd").".txt";
        $fp = fopen($GLOBALS[CONF][LOGS_FOLDER]."/".$navigationFilename, "a");
                
        $txtParams = "";
        if ($params != null) {
            foreach ($params as $paramName => $paramValue) {
                $txtParams .= " / ".$paramName."=".$paramValue;
            }
        }
        
        fputs($fp, date(self::DATE_FORMAT).self::LOG_SEPARATOR.$resourceName.$txtParams.self::LOG_SEPARATOR.$_SERVER["REMOTE_ADDR"].self::LOG_SEPARATOR.$_SERVER["HTTP_USER_AGENT"]."\n");
        fclose($fp);
    }

    public static function logSQL($location, $query, $params) {
        if ($GLOBALS[CONF][IS_DEV]) {
            $sqlFilename = self::SQL_FILENAME_PREFIX."-".date("Ymd").".txt";
            $fp = fopen($GLOBALS[CONF][LOGS_FOLDER]."/".$sqlFilename, "a");

            $txtParams = "";
            if ($params != null) {
                $i = 0;
                foreach ($params as $paramName => $paramValue) {
                    if ($i != 0) {
                        $txtParams .= " / ";
                    }
                    $txtParams .= $paramName."=".$paramValue;
                    $i++;
                }
            }
            
            fputs($fp, date(self::DATE_FORMAT).self::LOG_SEPARATOR.$location.self::LOG_SEPARATOR.$query.self::LOG_SEPARATOR.$txtParams.self::LOG_SEPARATOR.$_SERVER["REMOTE_ADDR"].self::LOG_SEPARATOR.$_SERVER["HTTP_USER_AGENT"]."\n");
            fclose($fp);
        }
    }

    public static function logError($location, $errorText) {
        $logFilename = self::LOGS_FILENAME_PREFIX."-".date("Ymd").".txt";
        $fp = fopen($GLOBALS[CONF][LOGS_FOLDER]."/".$logFilename, "a");
        fputs($fp, date(self::DATE_FORMAT).self::LOG_SEPARATOR.$location.self::LOG_SEPARATOR.$errorText.self::LOG_SEPARATOR.$_SERVER["REMOTE_ADDR"].self::LOG_SEPARATOR.$_SERVER["HTTP_USER_AGENT"]."\n");
        fclose($fp);
    }
}

?>