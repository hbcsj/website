<?php

class StringUtils {

    public static function convertAccentsToHtmlSpecialChars($str) {
        $str = str_replace("à", "&agrave;", $str);
        $str = str_replace(strtoupper("à"), "&Agrave;", $str);
        $str = str_replace("â", "&acirc;", $str);
        $str = str_replace(strtoupper("â"), "&Acirc;", $str);
        $str = str_replace("ä", "&auml;", $str);
        $str = str_replace(strtoupper("ä"), "&Aurl;", $str);
        $str = str_replace("ç", "&ccedil;", $str);
        $str = str_replace(strtoupper("ç"), "&Ccedil;", $str);
        $str = str_replace("é", "&eacute;", $str);
        $str = str_replace(strtoupper("é"), "&Eacute;", $str);
        $str = str_replace("è", "&egrave;", $str);
        $str = str_replace(strtoupper("è"), "&Egrave;", $str);
        $str = str_replace("ê", "&ecirc;", $str);
        $str = str_replace(strtoupper("ê"), "&Ecirc;", $str);
        $str = str_replace("ë", "&euml;", $str);
        $str = str_replace(strtoupper("ë"), "&Euml;", $str);
        $str = str_replace("ì", "&igrave;", $str);
        $str = str_replace(strtoupper("ì"), "&Igrave;", $str);
        $str = str_replace("î", "&icirc;", $str);
        $str = str_replace(strtoupper("î"), "&Icirc;", $str);
        $str = str_replace("ï", "&iuml;", $str);
        $str = str_replace(strtoupper("ï"), "&Iuml;", $str);
        $str = str_replace("ò", "&ograve;", $str);
        $str = str_replace(strtoupper("ò"), "&Ograve;", $str);
        $str = str_replace("ô", "&ocirc;", $str);
        $str = str_replace(strtoupper("ô"), "&Ocirc;", $str);
        $str = str_replace("ö", "&ouml;", $str);
        $str = str_replace(strtoupper("ö"), "&Ouml;", $str);
        $str = str_replace("ù", "&ugrave;", $str);
        $str = str_replace(strtoupper("ù"), "&Ugrave;", $str);
        $str = str_replace("û", "&ucirc;", $str);
        $str = str_replace(strtoupper("û"), "&Ucirc;", $str);
        $str = str_replace("ü", "&uuml;", $str);
        $str = str_replace(strtoupper("ü"), "&Uuml;", $str);
        $str = str_replace("ÿ", "&yuml;", $str);
        $str = str_replace(strtoupper("ÿ"), "&Yuml;", $str);
        $str = str_replace("²", "&sup2;", $str);
        $str = str_replace("°", "&deg;", $str);
        $str = str_replace("€", "&euro;", $str);
        
        return $str;
    }
    
    public static function convertHtmlSpecialCharsToAccents($str) {
        $str = str_replace("&agrave;", "à", $str);
        $str = str_replace("&Agrave;", strtoupper("à"), $str);
        $str = str_replace("&acirc;", "â", $str);
        $str = str_replace("&Acirc;", strtoupper("â"), $str);
        $str = str_replace("&auml;", "ä", $str);
        $str = str_replace("&Auml;", strtoupper("ä"), $str);
        $str = str_replace("&ccedil;", "ç", $str);
        $str = str_replace("&Ccedil;", strtoupper("ç"), $str);
        $str = str_replace("&eacute;", "é", $str);
        $str = str_replace("&Eacute;", strtoupper("é"), $str);
        $str = str_replace("&egrave;", "è", $str);
        $str = str_replace("&Egrave;", strtoupper("è"), $str);
        $str = str_replace("&ecirc;", "ê", $str);
        $str = str_replace("&Ecirc;", strtoupper("ê"), $str);
        $str = str_replace("&euml;", "ë", $str);
        $str = str_replace("&Euml;", strtoupper("ë"), $str);
        $str = str_replace("&igrave;", "ì", $str);
        $str = str_replace("&Igrave;", strtoupper("ì"), $str);
        $str = str_replace("&icirc;", "î", $str);
        $str = str_replace("&Icirc;", strtoupper("î"), $str);
        $str = str_replace("&iuml;", "ï", $str);
        $str = str_replace("&Iuml;", strtoupper("ï"), $str);
        $str = str_replace("&ograve;", "ò", $str);
        $str = str_replace("&Ograve;", strtoupper("ò"), $str);
        $str = str_replace("&ocirc;", "ô", $str);
        $str = str_replace("&Ocirc;", strtoupper("ô"), $str);
        $str = str_replace("&ouml;", "ö", $str);
        $str = str_replace("&Ouml;", strtoupper("ö"), $str);
        $str = str_replace("&ugrave;", "ù", $str);
        $str = str_replace("&Ugrave;", strtoupper("ù"), $str);
        $str = str_replace("&ucirc;", "û", $str);
        $str = str_replace("&Ucirc;", strtoupper("û"), $str);
        $str = str_replace("&uuml;", "ü", $str);
        $str = str_replace("&Uuml;", strtoupper("ü"), $str);
        $str = str_replace("&yuml;", "ÿ", $str);
        $str = str_replace("&Yuml;", strtoupper("ÿ"), $str);
        $str = str_replace("&sup2;", "²", $str);
        $str = str_replace("&deg;", "°", $str);
        $str = str_replace("&euro;", "€", $str);
        
        return $str;
    }
    
    public static function removeHtmlSpecialChars($str) {
        $str = str_replace("&agrave;", "a", $str);
        $str = str_replace("&Agrave;", "A", $str);
        $str = str_replace("&acirc;", "a", $str);
        $str = str_replace("&Acirc;", "A", $str);
        $str = str_replace("&auml;", "a", $str);
        $str = str_replace("&Auml;", "A", $str);
        $str = str_replace("&ccedil;", "c", $str);
        $str = str_replace("&Ccedil;", "C", $str);
        $str = str_replace("&eacute;", "e", $str);
        $str = str_replace("&Eacute;", "E", $str);
        $str = str_replace("&egrave;", "e", $str);
        $str = str_replace("&Egrave;", "E", $str);
        $str = str_replace("&ecirc;", "e", $str);
        $str = str_replace("&Ecirc;", "E", $str);
        $str = str_replace("&euml;", "e", $str);
        $str = str_replace("&Euml;", "E", $str);
        $str = str_replace("&igrave;", "i", $str);
        $str = str_replace("&Igrave;", "I", $str);
        $str = str_replace("&icirc;", "i", $str);
        $str = str_replace("&Icirc;", "I", $str);
        $str = str_replace("&iuml;", "i", $str);
        $str = str_replace("&Iuml;", "I", $str);
        $str = str_replace("&ograve;", "o", $str);
        $str = str_replace("&Ograve;", "O", $str);
        $str = str_replace("&ocirc;", "o", $str);
        $str = str_replace("&Ocirc;", "O", $str);
        $str = str_replace("&ouml;", "o", $str);
        $str = str_replace("&Ouml;", "O", $str);
        $str = str_replace("&ugrave;", "u", $str);
        $str = str_replace("&Ugrave;", "U", $str);
        $str = str_replace("&ucirc;", "u", $str);
        $str = str_replace("&Ucirc;", "U", $str);
        $str = str_replace("&uuml;", "u", $str);
        $str = str_replace("&Uuml;", "U", $str);
        $str = str_replace("&yuml;", "y", $str);
        $str = str_replace("&Yuml;", "Y", $str);
        
        return $str;
    }

    public static function convertEmptyStringToNull($str) {
        if (trim($str) == "") {
            return null;
        }
        return $str;
    }

    public static function truncate($str, $nbChars) {
        if (strlen($str) <= $nbChars) {
            return $str;
        }
        return substr($str, 0, $nbChars)."...";
    }
}

?>