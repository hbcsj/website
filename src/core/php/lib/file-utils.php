<?php

class FileUtils {
    
    public static function getFolderContent($folderPath) {
        $folderPath = str_replace("\\", "/", $folderPath);
        $files = array();
        if (file_exists($folderPath)) {
            if ($handle = opendir($folderPath)) {
                while ($file = readdir($handle)) {
                    if ($file != "." && $file != "..") {
                        $files[] = $file;
                    }
                }
            }
        }
        return $files;
    }

    public static function getFileExtension($file) {
        $fileSplitted = explode(".", $file);
        return $fileSplitted[sizeof($fileSplitted) - 1];
    }
    
    public static function getFileContent($filePath) {
        $content = "";
        
        $fp = fopen($filePath, "r");
        $content = fread($fp, filesize($filePath));
        fclose($fp);
        
        return trim($content);
    }

    public static function download($filePath, $filenameToDownload) {
        header("Cache-Control: no-cache, must-revalidate");
        header("Cache-Control: post-check=0,pre-check=0");
        header("Cache-Control: max-age=0");
        header("Pragma: no-cache");
        header("Expires: 0");
        header("Content-Type: application/force-download");
        header('Content-Disposition: attachment; filename="'.$filenameToDownload.'"');
        header("Content-Length: ".filesize($filePath));
        readfile($filePath);
    }
}

?>