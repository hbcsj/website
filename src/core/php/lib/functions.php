<?php

function desc($myvar, $output = false) {
	$debug = "<span style=\"color= #000000;\"><pre>".str_replace(array(
		"\n",
		" "
	), array(
		"<br>",
		" "
	), htmlentities(print_r($myvar, true))) . "</pre></span>";
	$debug = str_replace(" Object", ":<span style=\"font-weight: bold; color: #FF0000;\">Object</span>", $debug);
	$debug = str_replace("Array", "<span style=\"color: #FFAA00; font-weight: bold;\">Array</span>", $debug);
	$debug = str_replace("[", "[<span style=\"color: #0000FF;\">", $debug);
	$debug = str_replace("]", "</span>]", $debug);
	$debug = str_replace(":", " : ", $debug);
	$debug = str_replace("private", "<span style=\"font-style: italic; color: #BBBBBB;\">private</span>", $debug);
	$debug = str_replace("public", "<span style=\"font-style: italic; color: #BBBBBB;\">public</span>", $debug);
	if ($output) {
		return $debug;
	} else {
		echo "<div style=\"padding: 5px; font-size: 12px;\">".$debug."</div>";
	}
}

?>