<?php 

abstract class AbstractManager {

    const FILE_NAME = "AbstractManager";

	private $databaseConnection;

	public function __construct($databaseConnection = null) {
		$this->databaseConnection = $databaseConnection;

		if ($this->databaseConnection == null) {
			try {
				$this->databaseConnection = new PDO(
					$GLOBALS[CONF][DATABASE_SYSTEM].":host=".$GLOBALS[CONF][DATABASE_HOSTNAME].";dbname=".$GLOBALS[CONF][DATABASE_NAME].";charset=utf8", 
					$GLOBALS[CONF][DATABASE_LOGIN], 
					$GLOBALS[CONF][DATABASE_PASSWORD]
				);
			} catch (Exception $e) {
				Logger::logError(self::FILE_NAME, $e->getMessage());
			}
		}
	}

    public function getDatabaseConnection() {
        return $this->databaseConnection;
    }
}

?>