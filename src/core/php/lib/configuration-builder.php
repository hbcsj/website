<?php

class ConfigurationBuilder {

    public static function buildConfig() {
        $GLOBALS[CONF] = parse_ini_file(CONF_FILE);

        if (!isset($GLOBALS[CONF][DEFAULT_PAGE]) || 
            $GLOBALS[CONF][DEFAULT_PAGE] == null || 
            $GLOBALS[CONF][DEFAULT_PAGE] == "") {
            $GLOBALS[CONF][DEFAULT_PAGE] = DEFAULT_DEFAULT_PAGE;
        }
        
        if (!isset($GLOBALS[CONF][LOGS_FOLDER]) || 
            $GLOBALS[CONF][LOGS_FOLDER] == null || 
            $GLOBALS[CONF][LOGS_FOLDER] == "") {
            $GLOBALS[CONF][LOGS_FOLDER] = DEFAULT_LOGS_FOLDER;
        }

        if (!isset($GLOBALS[CONF][IS_DEV]) || 
            $GLOBALS[CONF][IS_DEV] == null || 
            $GLOBALS[CONF][IS_DEV] == "") {
            $GLOBALS[CONF][IS_DEV] = DEFAULT_IS_DEV;
        } else {
            $GLOBALS[CONF][IS_DEV] = ($GLOBALS[CONF][IS_DEV] == 1);
        }

        if (!isset($GLOBALS[CONF][MAILS_IN_TEST]) || 
            $GLOBALS[CONF][MAILS_IN_TEST] == null || 
            $GLOBALS[CONF][MAILS_IN_TEST] == "") {
            $GLOBALS[CONF][MAILS_IN_TEST] = DEFAULT_MAILS_IN_TEST;
        } else {
            $GLOBALS[CONF][MAILS_IN_TEST] = ($GLOBALS[CONF][MAILS_IN_TEST] == 1);
        }
        
        if (!isset($GLOBALS[CONF][DATABASE_SYSTEM]) || 
            $GLOBALS[CONF][DATABASE_SYSTEM] == null || 
            $GLOBALS[CONF][DATABASE_SYSTEM] == "") {
            $GLOBALS[CONF][DATABASE_SYSTEM] = DEFAULT_DATABASE_SYSTEM;
        }
        
        if (!isset($GLOBALS[CONF][DATABASE_HOSTNAME]) || 
            $GLOBALS[CONF][DATABASE_HOSTNAME] == null || 
            $GLOBALS[CONF][DATABASE_HOSTNAME] == "") {
            $GLOBALS[CONF][DATABASE_HOSTNAME] = DEFAULT_DATABASE_HOSTNAME;
        }
        
        if (!isset($GLOBALS[CONF][DATABASE_LOGIN]) || 
            $GLOBALS[CONF][DATABASE_LOGIN] == null || 
            $GLOBALS[CONF][DATABASE_LOGIN] == "") {
            $GLOBALS[CONF][DATABASE_LOGIN] = DEFAULT_DATABASE_LOGIN;
        }
        
        if (!isset($GLOBALS[CONF][DATABASE_PASSWORD]) || 
            $GLOBALS[CONF][DATABASE_PASSWORD] == null || 
            $GLOBALS[CONF][DATABASE_PASSWORD] == "") {
            $GLOBALS[CONF][DATABASE_PASSWORD] = DEFAULT_DATABASE_PASSWORD;
        }
        
        if (!isset($GLOBALS[CONF][DATABASE_NAME]) || 
            $GLOBALS[CONF][DATABASE_NAME] == null || 
            $GLOBALS[CONF][DATABASE_NAME] == "") {
            $GLOBALS[CONF][DATABASE_NAME] = DEFAULT_DATABASE_NAME;
        }
        
        if (!isset($GLOBALS[CONF][SUPERADMIN_EMAIL]) || 
            $GLOBALS[CONF][SUPERADMIN_EMAIL] == null || 
            $GLOBALS[CONF][SUPERADMIN_EMAIL] == "") {
            $GLOBALS[CONF][SUPERADMIN_EMAIL] = DEFAULT_SUPERADMIN_EMAIL;
        }
    }
}

?>