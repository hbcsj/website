<?php

define(SERVICE_PATH, "_service_");
define(VIEW_PATH, "_view_");
define(UPLOADER_PATH, "_uploader_");

class Router {

    private $path;
    private $httpCode;
    private $rootPath;
    private $isUrlCorrected;

    public function __construct() {
        $this->path = "";
        $this->rootPath = "";
        $this->httpCode = null;
        $this->isUrlCorrected = false;
    }

    public function buildRoute($url) {
        $url = $this->correctUrl($url);

        if (is_int(strpos($url, SERVICE_PATH))) {
            $this->buildServicePath($url);
        } else if (is_int(strpos($url, VIEW_PATH))) {
            $this->buildViewPath($url);
        } else if (is_int(strpos($url, UPLOADER_PATH))) {
            $this->buildUploaderPath($url);
        } else {
            $this->buildPagePath($url);
        }
    }

    private function correctUrl($url) {
        $urlCorrected = $url;
        if (substr($url, strlen($url) - 1, 1) == "/") {
            $urlCorrected = substr($url, 0, strlen($url) - 1);
            $this->isUrlCorrected = true;
        }
        return $urlCorrected;
    }

    private function buildServicePath($url) {
        $this->path = str_replace(
            substr($url, 0, strpos($url, SERVICE_PATH)).SERVICE_PATH, 
            substr(SERVICES_FOLDER, 0, strlen(SERVICES_FOLDER) - 1), 
            $url
        );
        $resource = str_replace("/", "", str_replace(substr(SERVICES_FOLDER, 0, strlen(SERVICES_FOLDER)), "", $this->path));

        $this->path = $this->path."/".$resource.SERVICES_EXTENSION;

        if (!file_exists($this->path)) {
            Logger::logError("Service '".$this->path."'", "Not found");
            $this->httpCode = HTTP_404;
        }
    }

    private function buildViewPath($url) {
        $this->path = str_replace(VIEW_PATH, substr(VIEWS_FOLDER, 0, strlen(VIEWS_FOLDER) - 1), $url);
        $this->path = substr($this->path, 0, strlen($this->path));
        $this->path .= VIEW_EXTENSION;

        $this->setRootPath(6);

        if (!file_exists($this->path)) {
            Logger::logError("View '".$this->path."'", "Not found");
            $this->httpCode = HTTP_404;
        }
    }

    private function buildUploaderPath($url) {
        $this->path = str_replace(
            substr($url, 0, strpos($url, UPLOADER_PATH)).UPLOADER_PATH, 
            substr(UPLOADERS_FOLDER, 0, strlen(UPLOADERS_FOLDER) - 1), 
            $url
        );
        $resource = str_replace("/", "", str_replace(substr(UPLOADERS_FOLDER, 0, strlen(UPLOADERS_FOLDER)), "", $this->path));
        
        $this->path = $this->path."/".$resource.UPLOADERS_EXTENSION;
        
        if (!file_exists($this->path)) {
            Logger::logError("Uploader '".$this->path."'", "Not found");
            $this->httpCode = HTTP_404;
        }
    }

    private function buildPagePath($url) {
        if (substr($url, strlen($url) - 1, 1) != "/") {
            $url .= "/";
        }
        
        $resource = "";
        if ($url == "/") {
            $url = $GLOBALS[CONF][DEFAULT_PAGE]."/";
            $resource = $GLOBALS[CONF][DEFAULT_PAGE];
        } else {
            $resource = str_replace("/", RESOURCE_NAME_SEPARATOR, $url);
            $resource = substr($resource, 0, strlen($resource) - 1);
        }

        $this->path = VIEWS_FOLDER.$url.$resource.VIEW_EXTENSION;

        $this->setRootPath(4);

        if (!file_exists($this->path)) {
            Logger::logError("Page '".$resource."'", "Not found");
            header("Location: ".$this->rootPath."error/404");
            exit();
        }
    }

    public function getPath() {
        return $this->path;
    }

    public function getHTTPCode() {
        return $this->httpCode;
    }

    public function getRootPath() {
        return $this->rootPath;
    }

    public function setRootPath($levelsToRemove) {
        $nbDotDotSlashes = sizeof(explode("/", $this->path)) - $levelsToRemove;
        for ($i = 0 ; $i < $nbDotDotSlashes ; $i++) {
            $this->rootPath .= "../";
        }
        if ($this->isUrlCorrected) {
            $this->rootPath .= "../";
        }
    }
}

?>