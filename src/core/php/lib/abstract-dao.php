<?php 

abstract class AbstractDAO {

    const FILE_NAME = "AbstractDAO";
    
    private $tableName;
	private $databaseConnection;

	public function __construct($tableName, $databaseConnection) {
        $this->tableName = $tableName;
		$this->databaseConnection = $databaseConnection;
	}
	
	abstract public function getById($id);
	
	abstract public function getAll($orderBy = null);

	protected function executeGetUniqueRequest($query, $params, $columns) {
		$object = null;

		$request = null;
		if ($params != null) {
			$request = $this->getDatabaseConnection()->prepare($query);
			$request->execute($params);
		} else {
			$request = $this->getDatabaseConnection()->query($query);
		}
        Logger::logSQL(self::FILE_NAME, $query, $params);
        while ($row = $request->fetch()) {
			$object = array();
			if (sizeof($columns) > 0) {
				foreach ($columns as $column) {
					$object[$column] = $row[$column];
				}
			}
            break;
        }
        $request->closeCursor();
		
        return $object;
	}

	protected function executeGetRequest($query, $params, $columns) {
		$list = array();

		$request = null;
		if ($params != null) {
			$request = $this->getDatabaseConnection()->prepare($query);
			$request->execute($params);
		} else {
			$request = $this->getDatabaseConnection()->query($query);
		}
        Logger::logSQL(self::FILE_NAME, $query, $params);
        while ($row = $request->fetch()) {
			$object = array();
			if (sizeof($columns) > 0) {
				foreach ($columns as $column) {
					$object[$column] = $row[$column];
				}
			}
            $list[] = $object;
        }
        $request->closeCursor();
		
        return $list;
	}
	
	abstract public function create($object);

	protected function executeCreateRequest($query, $params) {
		$idCreated = null;
        try {
            $this->getDatabaseConnection()->beginTransaction();
            $request = $this->getDatabaseConnection()->prepare($query);
            $request->execute($params);
            $idCreated = $this->getDatabaseConnection()->lastInsertId();
            $this->getDatabaseConnection()->commit();
            Logger::logSQL(self::FILE_NAME, $query, $params);
        } catch (Exception $e) {
            $this->getDatabaseConnection()->rollback();
            Logger::logError(self::FILE_NAME, $e->getMessage());
        } finally {
            return $idCreated;
        }
	}
	
	abstract public function update($object);
	
	abstract public function delete($id);

	protected function executeUpdateOrDeleteRequest($query, $params) {
		try {
            $this->getDatabaseConnection()->beginTransaction();
            $request = $this->getDatabaseConnection()->prepare($query);
            $request->execute($params);
            $this->getDatabaseConnection()->commit();
            Logger::logSQL(self::FILE_NAME, $query, $params);
        } catch (Exception $e) {
            $this->getDatabaseConnection()->rollback();
            Logger::logError(self::FILE_NAME, $e->getMessage());
        }
	}
    
    public function getTableName() {
        return $this->tableName;
    }

    public function getDatabaseConnection() {
        return $this->databaseConnection;
    }
}

?>