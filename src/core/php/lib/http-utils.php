<?php

class HTTPUtils {

    public static function paramExists($method, $paramName) {
        $res = false;
        switch ($method) {
            case GET:
                $res = (isset($_GET[$paramName]) && $_GET[$paramName] != null && $_GET[$paramName] != "");
                break;
            case POST:
                $res = (isset($_POST[$paramName]) && $_POST[$paramName] != null && $_POST[$paramName] != "");
                break;
            case FILES:
                $res = (isset($_FILES[$paramName]) && $_FILES[$paramName] != null && $_FILES[$paramName] != "");
                break;
            case SESSION:
                $res = (isset($_SESSION[$paramName]) && $_SESSION[$paramName] != null && $_SESSION[$paramName] != "");
                break;
            default:
                break;
        }
        return $res;
    }

    public static function getContentType($contentType, $charset = "UTF-8") {
        return "Content-Type: ".$contentType."; charset=".$charset;
    }
}

?>