<?php
require_once("core/php/controllers/abstract-controller.php");

abstract class AbstractUploaderCtrl extends AbstractCtrl {
    
    const UPLOADER = "Uploader";
    const SUCCESS_TRUE = "true";
    const SUCCESS_FALSE = "false";

    private $success;

	public function __construct($uploaderName, $params = null, $needsDatabaseConnection = false) {
        parent::__construct($uploaderName, $needsDatabaseConnection);
        $this->success = true;
        Logger::logNavigation($this->getResourceType()." '".$this->getResourceName()."'", $params);
	}
    
    protected function checkParams($arrayParams) {
        $res = true;
        for ($i = 0 ; $i < sizeof($arrayParams) ; $i++) {
            if (!HTTPUtils::paramExists($arrayParams[$i][0], $arrayParams[$i][1])) {
                $res = false;
                Logger::logError($this->getResourceType()." '".$this->getResourceName()."'", "Param '".$arrayParams[$i][1]."' (".$arrayParams[$i][0].") missing");
            }
        }
        if (!$res) {
            $this->sendCheckError(HTTP_400);
        }
        $this->success = $res;
        return $res;
    }

    public function sendCheckError($errorCode, $errorMessage = null) {
        if ($errorMessage != null) {
            Logger::logError($this->getResourceType()." '".$this->getResourceName()."'", $errorMessage);
        }
        header($errorCode);
        exit();
    }
    
    protected function getResourceType() {
        return self::UPLOADER;
    }

    public function getSuccess() {
        return ($this->success) ? self::SUCCESS_TRUE : self::SUCCESS_FALSE;
    }

    public function setSuccess($success) {
        $this->success = $success;
    }
}

?>