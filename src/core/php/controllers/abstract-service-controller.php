<?php
require_once("core/php/controllers/abstract-controller.php");

abstract class AbstractServiceCtrl extends AbstractCtrl {
    
    const SERVICE = "Service";

    public function __construct($serviceName, $needsDatabaseConnection = false) {
        parent::__construct($serviceName, $needsDatabaseConnection);
        if (HTTPUtils::paramExists(GET, "method")) {
            $method = $_GET["method"];
            $this->$method();
        }
	}
    
    protected function checkParams($arrayParams) {
        $res = true;
        for ($i = 0 ; $i < sizeof($arrayParams) ; $i++) {
            if (!HTTPUtils::paramExists($arrayParams[$i][0], $arrayParams[$i][1])) {
                $res = false;
                Logger::logError($this->getResourceType()." '".$this->getResourceName()."'", "Param '".$arrayParams[$i][1]."' (".$arrayParams[$i][0].") missing");
            }
        }
        if (!$res) {
            header(HTTP_400);
        }
        return $res;
    }

    protected function logNavigation($method, $params) {
        Logger::logNavigation($this->getResourceType()." '".$this->getResourceName()."' / ".$method, $params);
    }
    
    protected function getResourceType() {
        return self::SERVICE;
    }

    public function sendCheckError($errorCode, $errorMessage = null) {
        if ($errorMessage != null) {
            Logger::logError($this->getResourceType()." '".$this->getResourceName()."'", $errorMessage);
        }
        header($errorCode);
    }

    public function generateResponse($response, $contentType) {
        header(HTTPUtils::getContentType($contentType));
        echo json_encode($response);
    }
}

?>