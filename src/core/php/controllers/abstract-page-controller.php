<?php
require_once("core/php/controllers/abstract-controller.php");

abstract class AbstractPageCtrl extends AbstractCtrl {
    
    const PAGE = "Page";

	public function __construct($pageName, $params = null, $needsDatabaseConnection = false) {
		parent::__construct($pageName, $needsDatabaseConnection);
        Logger::logNavigation($this->getResourceType()." '".$this->getResourceName()."'", $params);
	}
    
    protected function checkParams($arrayParams) {
        $res = true;
        for ($i = 0 ; $i < sizeof($arrayParams) ; $i++) {
            if (!HTTPUtils::paramExists($arrayParams[$i][0], $arrayParams[$i][1])) {
                $res = false;
                Logger::logError($this->getResourceType()." '".$this->getResourceName()."'", "Param '".$arrayParams[$i][1]."' (".$arrayParams[$i][0].") missing");
            }
        }
        if (!$res) {
            $this->sendCheckError(
                HTTP_400,
                null,
                "400"
            );
        }
        return $res;
    }

    public function sendCheckError($errorCode, $errorMessage = null, $errorTemplate = null) {
        if ($errorMessage != null) {
            Logger::logError($this->getResourceType()." '".$this->getResourceName()."'", $errorMessage);
        }
        header($errorCode);
        if ($errorTemplate != null) {
            header("Location: ".ROOT_PATH."error/".$errorTemplate);
        }
        exit();
    }
    
    protected function getResourceType() {
        return self::PAGE;
    }
}

?>