<?php

abstract class AbstractCtrl {
	
    private $resourceName;
    private $databaseConnection;
    
	public function __construct($resourceName, $needsDatabaseConnection = false) {
        $this->resourceName = $resourceName;
        $this->createMySQLConnection($needsDatabaseConnection);
	}

    private function createMySQLConnection($needsDatabaseConnection) {
        if ($needsDatabaseConnection) {
            try {
                $this->databaseConnection = new PDO(
                    $GLOBALS[CONF][DATABASE_SYSTEM].":host=".$GLOBALS[CONF][DATABASE_HOSTNAME].";dbname=".$GLOBALS[CONF][DATABASE_NAME].";charset=utf8", 
                    $GLOBALS[CONF][DATABASE_LOGIN], 
                    $GLOBALS[CONF][DATABASE_PASSWORD]
                );
            } catch (Exception $e) {
                Logger::logError($this->getResourceType()." '".$this->getResourceName()."'", $e->getMessage());
            }
        }
    }
    
    protected function getResourceName() {
        return $this->resourceName;
    }

    abstract protected function checkParams($arrayParams);
    
    abstract protected function getResourceType();

    public function getDatabaseConnection() {
        return $this->databaseConnection;
    }
}

?>