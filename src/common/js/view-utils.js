ViewUtils = {};

ViewUtils.alertTypes = {};
ViewUtils.alertTypes.info = 'info';
ViewUtils.alertTypes.ok = 'ok';
ViewUtils.alertTypes.warning = 'warning';
ViewUtils.alertTypes.error = 'error';

ViewUtils.setHTMLAndShow = function(elemSelector, html) {
    $(elemSelector).html(html);
    $(elemSelector).show();
};

ViewUtils.unsetHTMLAndHide = function(elemSelector) {
    $(elemSelector).hide();
    $(elemSelector).html('');
};

ViewUtils.desactiveButtonAndShowLoader = function(buttonSelector, loaderSelector) {
    $(buttonSelector).prop('disabled', true);
    $(loaderSelector).show();
};

ViewUtils.activeButtonAndHideLoader = function(buttonSelector, loaderSelector) {
    $(loaderSelector).hide();
    $(buttonSelector).prop('disabled', false);
};

ViewUtils.alert = function(html, type, callbackHide) {
    $('#modal-alert__message').html(html);

    var alertClass = ViewUtils.alertTypes.info;
    var alertIcon = 'info';
    if (type != null) {
        if (type == ViewUtils.alertTypes.ok) {
            alertClass = ViewUtils.alertTypes.ok;
            alertIcon = 'ok';
        } else if (type == ViewUtils.alertTypes.warning) {
            alertClass = ViewUtils.alertTypes.warning;
            alertIcon = 'warning';
        } else if (type == ViewUtils.alertTypes.error) {
            alertClass = ViewUtils.alertTypes.error;
            alertIcon = 'remove';
        }
    }
    $('#modal-alert').removeClass('modal-alert-'+ViewUtils.alertTypes.info);
    $('#modal-alert').removeClass('modal-alert-'+ViewUtils.alertTypes.ok);
    $('#modal-alert').removeClass('modal-alert-'+ViewUtils.alertTypes.warning);
    $('#modal-alert').removeClass('modal-alert-'+ViewUtils.alertTypes.error);
    $('#modal-alert').addClass('modal-alert-'+alertClass);
    $('#modal-alert__icon span.glyphicon').removeClass('glyphicon-info-sign');
    $('#modal-alert__icon span.glyphicon').removeClass('glyphicon-ok-sign');
    $('#modal-alert__icon span.glyphicon').removeClass('glyphicon-warning-sign');
    $('#modal-alert__icon span.glyphicon').removeClass('glyphicon-remove-sign');
    $('#modal-alert__icon span.glyphicon').addClass('glyphicon-'+alertIcon+'-sign');

    $('#modal-alert').modal('show');

    $('#modal-alert').off('hidden.bs.modal');
    $('#modal-alert').on('hidden.bs.modal', function(event) {
        if (callbackHide != null) {
            callbackHide();
        }
    });
};

ViewUtils.prompt = function(html, settings, callbackOK, callbackKO, callbackHide) {
    $('#modal-prompt__message').html(html);

    if (settings != null) {
        if (settings.btOkText != undefined) {
            $('#modal-prompt__bt-ok').html(settings.btOkText);
        }
        if (settings.btKoText != undefined) {
            $('#modal-prompt__bt-ko').html(settings.btKoText);
        }
    }

    $('#modal-prompt').modal('show');

    $('#modal-prompt__bt-ok').unbind('click');
    $('#modal-prompt__bt-ok').click(function() {
        $('#modal-prompt').modal('hide');
        if (callbackOK != null) {
            callbackOK();
        }
    });

    $('#modal-prompt__bt-ko').unbind('click');
    $('#modal-prompt__bt-ko').click(function() {
        $('#modal-prompt').modal('hide');
        if (callbackKO != null) {
            callbackKO();
        }
    });

    $('#modal-prompt').off('hidden.bs.modal');
    $('#modal-prompt').on('hidden.bs.modal', function(event) {
        if (callbackHide != null) {
            callbackHide();
        }
    });
};