ConnexionController = {};

ConnexionController.admin = 'Admin';
ConnexionController.dispos = 'Dispos';

ConnexionController.adminSuperadmin = 'superadmin';
ConnexionController.adminBureau = 'bureau';
ConnexionController.adminCoach = 'coach';
ConnexionController.adminEditeur = 'editeur';
ConnexionController.adminCommercial = 'commercial';

ConnexionController.onClick_btConnexion = function(site) {
    $('#mdp-form__bt-connexion').unbind('click');
    $('#mdp-form__bt-connexion').click(function() {
        ViewUtils.unsetHTMLAndHide('#mdp-form__error');
        ViewUtils.desactiveButtonAndShowLoader('#mdp-form__bt-connexion', '#mdp-form__bt-connexion .loader');
        AjaxUtils.callService(
            'connexion', 
            'check'+site, 
            AjaxUtils.dataTypes.json, 
            AjaxUtils.requestTypes.post, 
            null, 
            {
                mdp: $('#mdp-form__input-password').val()
            }, 
            function(response) {
                if (response.isMdpValid) {
                    if (site == ConnexionController.admin) {
                        var urlSplitted = window.location.href.split('/');
                        var nbDotDotSlashes = '';
                        var i = (urlSplitted.length - 1);
                        while (urlSplitted[i] != 'admin') {
                            if (i != (urlSplitted.length - 2)) {
                                nbDotDotSlashes += '../';
                            }
                            i--;
                        }

                        if (response.adminProfile == ConnexionController.adminSuperadmin) {
                            window.location.href = nbDotDotSlashes+'superadmin';
                        } else if (response.adminProfile == ConnexionController.adminBureau) {
                            window.location.href = nbDotDotSlashes+'coaching';
                        } else if (response.adminProfile == ConnexionController.adminCoach) {
                            window.location.href = nbDotDotSlashes+'coaching';
                        } else if (response.adminProfile == ConnexionController.adminEditeur) {
                            window.location.href = nbDotDotSlashes+'edition';
                        } else if (response.adminProfile == ConnexionController.adminCommercial) {
                            window.location.href = nbDotDotSlashes+'shop';
                        }
                    } else if (site == ConnexionController.dispos) {
                        window.location.reload();
                    }
                } else {
                    display_ErreurConnexion('Mot de passe invalide');
                }
            },
            function(response) {
                display_ErreurConnexion('Service indisponible');
            }
        );
    });

    function display_ErreurConnexion(text) {
        ViewUtils.setHTMLAndShow('#mdp-form__error', text);
        ViewUtils.activeButtonAndHideLoader('#mdp-form__bt-connexion', '#mdp-form__bt-connexion .loader');
        $('#mdp-form__input-password').val('');
    }
};

ConnexionController.onClick_linkLogout = function() {
    $('#link-logout').unbind('click');
    $('#link-logout').click(function() {
        AjaxUtils.callService(
            'connexion', 
            'logout', 
            AjaxUtils.dataTypes.json, 
            AjaxUtils.requestTypes.post, 
            null, 
            {}, 
            function(response) {
                window.location.reload();
            },
            function(response) {}
        );
    });
};