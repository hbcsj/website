MainWebsiteController = {};

MainWebsiteController.initializePage = function(navLink, subMenuLink) {
    GlobalController.initializePage(navLink, subMenuLink);
    MainWebsiteController.control_Scroll();
};

MainWebsiteController.control_Scroll = function() {
    var windowHeight = window.innerHeight;
    var fullHeight = Math.max(
        document.body.scrollHeight, 
        document.documentElement.scrollHeight, 
        document.body.offsetHeight, 
        document.documentElement.offsetHeight, 
        document.body.clientHeight, 
        document.documentElement.clientHeight
    );
    if ((fullHeight - windowHeight) > 110) {
        var $window = $(window);
        $window.unbind('scroll');
        $window.scroll(function() {
            if ($window.scrollTop() > 0) {
                $('header').hide();
                $('nav').addClass('header-hidden');
                $('.sub-menu').addClass('header-hidden');
                $('#nav-logo').show();
                $('.container-fluid').addClass('header-hidden');
            } else {
                $('header').show();
                $('nav').removeClass('header-hidden');
                $('.sub-menu').removeClass('header-hidden');
                $('#nav-logo').hide();
                $('.container-fluid').removeClass('header-hidden');
            }
        });
    }
};