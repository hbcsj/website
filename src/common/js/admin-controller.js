AdminController = {};

AdminController.adminSite = 'Admin';

AdminController.initializePage = function(navLink, subMenuLink) {
    GlobalController.initializePage(navLink, subMenuLink);

    ConnexionController.onClick_btConnexion(AdminController.adminSite);
    ConnexionController.onClick_linkLogout();
};

AdminController.control_checkAllCheckboxes = function(allCheckboxesIconSelector, checkboxesSelector) {
    $(allCheckboxesIconSelector).unbind('click');
    $(allCheckboxesIconSelector).click(function() {
        if ($(allCheckboxesIconSelector).hasClass('glyphicon-check')) {
            $(allCheckboxesIconSelector).removeClass('glyphicon-check');
            $(allCheckboxesIconSelector).addClass('glyphicon-unchecked');
            $(checkboxesSelector+' input[type=checkbox]').prop('checked', false);
        } else {
            $(allCheckboxesIconSelector).removeClass('glyphicon-unchecked');
            $(allCheckboxesIconSelector).addClass('glyphicon-check');
            $(checkboxesSelector+' input[type=checkbox]').prop('checked', true);
        }
    });
};

AdminController.setNumsEquipeInSelect = function(idCategorie, inputNumEquipe, inputHiddenNumEquipe, loaderSelector) {
    if (idCategorie != '') {
        FormUtils.hideErrors(inputNumEquipe);
        if (loaderSelector != null) {
            $(loaderSelector).show();
        }
        AjaxUtils.callService(
            'categorie', 
            'getNbEquipes', 
            AjaxUtils.dataTypes.json, 
            AjaxUtils.requestTypes.get, 
            {
                id: idCategorie
            }, 
            null, 
            function(response) {
                if (loaderSelector != null) {
                    $(loaderSelector).hide();
                }

                var htmlNbEquipes = '';

                if (response.nbEquipes > 1) {
                    htmlNbEquipes += '<option value=""></option>';
                    for (var i = 1 ; i <= response.nbEquipes ; i++) {
                        htmlNbEquipes += '<option value="'+i+'">'+i+'</option>';
                    }
                } else {
                    htmlNbEquipes += '<option value="">1</option>';
                }
                
                $('#'+inputNumEquipe).html(htmlNbEquipes);
                if ($('#'+inputHiddenNumEquipe).val() != '') {
                    $('#'+inputNumEquipe).val($('#'+inputHiddenNumEquipe).val());
                }
            },
            function(response) {
                if (loaderSelector != null) {
                    $(loaderSelector).hide();
                }
                var errorNbEquipes = {
                    errorType: FormUtils.errorTypes.unavailableValue,
                    message: 'Service indisponible'
                };
                FormUtils.hideErrors(inputNumEquipe);
                FormUtils.displayError(inputNumEquipe, errorNbEquipes);
            }
        );
    }
};