GlobalController = {};

GlobalController.initializePage = function(navLink, subMenuLink) {
    GlobalController.selectNavLink(navLink);
    GlobalController.selectSubMenuLink(subMenuLink);
    GlobalController.control_AlertLinks();
    GlobalController.control_ImgModals();
};

GlobalController.selectNavLink = function(page) {
    $('nav .nav-links a').removeClass('selected-link');
    $('nav .nav-links a[page='+page+']').addClass('selected-link');
};

GlobalController.selectSubMenuLink = function(page) {
    $('.sub-menu a').removeClass('selected-link');
    $('.sub-menu a[page='+page+']').addClass('selected-link');
};

GlobalController.control_AlertLinks = function() {
    $('*[alert-message]').unbind('click');
    $('*[alert-message]').click(function() {
        var alertMessage = $(this).attr('alert-message');
        ViewUtils.alert(
            alertMessage, 
            ViewUtils.alertTypes.info,
            null
        );
    });
};

GlobalController.control_ImgModals = function() {
    $('*[img-modal]').unbind('click');
    $('*[img-modal]').click(function() {
        var imgSrc = $(this).attr('img-modal');
        $('#modal-img .modal-dialog .modal-content').html('<img src='+imgSrc+'>');
        $('#modal-img').modal('show');
    });
};