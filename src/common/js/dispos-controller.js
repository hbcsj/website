DisposController = {};

DisposController.disposSite = 'Dispos';

DisposController.initializePage = function(navLink) {
    GlobalController.initializePage(navLink, null);

    control_btBack();
    onClick_btReload();

    ConnexionController.onClick_btConnexion(DisposController.disposSite);
    ConnexionController.onClick_linkLogout();

    function control_btBack() {
        if (window.history.length <= 1) {
            $('#dispos__bt-back').prop('disabled', true);
        }
        
        $('#dispos__bt-back').unbind('click');
        $('#dispos__bt-back').click(function() {
            if (window.history.length > 1) {
                window.history.back();
            }
        });
    }

    function onClick_btReload() {
        $('#dispos__bt-reload').unbind('click');
        $('#dispos__bt-reload').click(function() {
            window.location.reload();
        });
    }
};

DisposController.control_MatchsUniquement = function(showOnlyMatchsOnInit, calculDisposCallback) {
    if (showOnlyMatchsOnInit) {
        $('.row.row-evenement .text-evenement .row[is-match="0"]').hide();
        $('.row.row-evenement[is-match="0"]').hide();
        $('.row.row-evenement[all-no-matchs="1"]').hide();
    }
    
    if (calculDisposCallback != null) {
        calculDisposCallback(true);
    }

    $('#dispos__bt-matchs-uniquement').unbind('click');
    $('#dispos__bt-matchs-uniquement').click(function() {
        if ($('#dispos__bt-matchs-uniquement .button__icon span.glyphicon').hasClass('glyphicon-check')) {
            $('#dispos__bt-matchs-uniquement .button__icon span.glyphicon').removeClass('glyphicon-check');
            $('#dispos__bt-matchs-uniquement .button__icon span.glyphicon').addClass('glyphicon-unchecked');
            $('.row.row-evenement .text-evenement .row[is-match="0"]').show();
            $('.row.row-evenement[is-match="0"]').show();
            $('.row.row-evenement[all-no-matchs="1"]').show();

            if (calculDisposCallback != null) {
                calculDisposCallback(false);
            }
        } else {
            $('#dispos__bt-matchs-uniquement .button__icon span.glyphicon').removeClass('glyphicon-unchecked');
            $('#dispos__bt-matchs-uniquement .button__icon span.glyphicon').addClass('glyphicon-check');
            $('.row.row-evenement .text-evenement .row[is-match="0"]').hide();
            $('.row.row-evenement[is-match="0"]').hide();
            $('.row.row-evenement[all-no-matchs="1"]').hide();

            if (calculDisposCallback != null) {
                calculDisposCallback(true);
            }
        }
    });
};