FormUtils = {};

FormUtils.resultsTypes = {};
FormUtils.resultsTypes.ok = 'ok';
FormUtils.resultsTypes.warning = 'warning';
FormUtils.resultsTypes.error = 'error';

FormUtils.errorTypes = {};
FormUtils.errorTypes.required = {};
FormUtils.errorTypes.required.label = 'required';
FormUtils.errorTypes.required.cssClass = 'error';
FormUtils.errorTypes.required.glyphicon = 'remove-sign';
FormUtils.errorTypes.unavailableValue = {};
FormUtils.errorTypes.unavailableValue.label = 'unavailable-value';
FormUtils.errorTypes.unavailableValue.cssClass = 'error';
FormUtils.errorTypes.unavailableValue.glyphicon = 'exclamation-sign';
FormUtils.errorTypes.invalidFormat = {};
FormUtils.errorTypes.invalidFormat.label = 'invalid-format';
FormUtils.errorTypes.invalidFormat.cssClass = 'warning';
FormUtils.errorTypes.invalidFormat.glyphicon = 'warning-sign';

FormUtils.validateForm = function(formSelector) {
    var formInputs = FormUtils.getFormInputs(formSelector);

    var errors = {};
    for (var i in formInputs) {
        FormUtils.validateInput(formInputs[i], errors);
    }
    
    for (var inputId in errors) {
        FormUtils.displayError(inputId, errors[inputId]);
    }
    
    return Object.keys(errors).length == 0;
};

FormUtils.getFormInputs = function(formSelector) {
    var formInputs = [];
    $(formSelector).find('input').each(function() {
        formInputs.push($(this));
    });
    $(formSelector).find('textarea').each(function() {
        formInputs.push($(this));
    });
    $(formSelector).find('select').each(function() {
        formInputs.push($(this));
    });

    return formInputs;
};

FormUtils.validateInput = function(input, errors) {
    var inputId = input.attr('id');
    if (inputId != undefined) {
        var inputError = null;
        var inputValue = $.trim(input.val());

        if (input.attr('required') != undefined && input.attr('required') == 'required') {
            if (inputValue == '') {
                inputError = {
                    errorType: FormUtils.errorTypes.required,
                    message: 'Ce champ doit &ecirc;tre renseign&eacute;'
                };
            }
        }

        var inputPattern = input.attr('pattern');
        var patternIndication = input.attr('pattern-indication');
        if (inputPattern != undefined) {
            if (patternIndication == undefined) {
                patternIndication = null;
            }
            if (inputValue != '' && new RegExp(inputPattern).exec(inputValue) == null) {
                inputError = {
                    errorType: FormUtils.errorTypes.invalidFormat,
                    message: 'La valeur du champ est invalide',
                    patternIndication: patternIndication
                };
            }
        }

        if (inputError != null) {
            errors[inputId] = inputError;
        }
    }
};

FormUtils.hideFormErrors = function(formSelector) {
    var formInputs = FormUtils.getFormInputs(formSelector);

    for (var i in formInputs) {
        var inputId = formInputs[i].attr('id');
        FormUtils.hideErrors(inputId);
    }
};

FormUtils.hideErrors = function(inputId) {
    $('#'+inputId).removeClass('border-'+FormUtils.resultsTypes.error);
    $('#'+inputId).removeClass('border-'+FormUtils.resultsTypes.warning);
    $('#'+inputId).removeClass('border-'+FormUtils.resultsTypes.ok);

    $('label[for='+inputId+']').removeClass('text-'+FormUtils.resultsTypes.error);
    $('label[for='+inputId+']').removeClass('text-'+FormUtils.resultsTypes.warning);
    $('label[for='+inputId+']').removeClass('text-'+FormUtils.resultsTypes.ok);

    $('.form-icon-validation[for='+inputId+'] span.glyphicon').hide();
    $('.form-icon-validation[for='+inputId+']').removeClass('text-'+FormUtils.resultsTypes.error);
    $('.form-icon-validation[for='+inputId+']').removeClass('text-'+FormUtils.resultsTypes.warning);
    $('.form-icon-validation[for='+inputId+']').removeClass('text-'+FormUtils.resultsTypes.ok);

    if (Utils.exists('.form-input[for='+inputId+'] .form-input-validation-message')) {
        $('.form-input[for='+inputId+'] .form-input-validation-message').hide();
    }

    $('.form-input[for='+inputId+'] .form-input-validation-message').removeClass('text-'+FormUtils.resultsTypes.error);
    $('.form-input[for='+inputId+'] .form-input-validation-message').removeClass('text-'+FormUtils.resultsTypes.warning);
    $('.form-input[for='+inputId+'] .form-input-validation-message').removeClass('text-'+FormUtils.resultsTypes.ok);
};

FormUtils.displayError = function(inputId, error) {
    FormUtils.hideErrors(inputId);

    $('#'+inputId).addClass('border-'+error.errorType.cssClass);
    $('label[for='+inputId+']').addClass('text-'+error.errorType.cssClass);

    $('.form-icon-validation[for='+inputId+']').html('<span class="glyphicon glyphicon-'+error.errorType.glyphicon+' animated fadeIn"></span>');
    $('.form-icon-validation[for='+inputId+']').addClass('text-'+error.errorType.cssClass);
    $('.form-icon-validation[for='+inputId+'] span.glyphicon').show();

    if ($('.form-input[for='+inputId+'] .form-input-validation-message').length == 0) {
        $('.form-input[for='+inputId+']').append('<div class="form-input-validation-message animated fadeIn"></div>');
    }
    
    var errorHTML = error.message;
    if (error.patternIndication != null) {
        errorHTML += '<br>'+error.patternIndication;
    }
    $('.form-input[for='+inputId+'] .form-input-validation-message').html(errorHTML);
    $('.form-input[for='+inputId+'] .form-input-validation-message').addClass('text-'+error.errorType.cssClass);
    $('.form-input[for='+inputId+'] .form-input-validation-message').show();
};

FormUtils.displayFormResultMessage = function(elemSelector, html, textClass) {
    $(elemSelector).removeClass('bg-'+FormUtils.resultsTypes.error);
    $(elemSelector).removeClass('bg-'+FormUtils.resultsTypes.warning);
    $(elemSelector).removeClass('bg-'+FormUtils.resultsTypes.ok);

    ViewUtils.setHTMLAndShow(elemSelector, html);
    $(elemSelector).addClass('bg-'+textClass);
};