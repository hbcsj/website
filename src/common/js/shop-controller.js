ShopController = {};

ShopController.initializePage = function(navLink) {
    GlobalController.initializePage(navLink, null);
    
    ShopController.load_MontantPanier();
    ShopController.onClick_btPanier();
};

ShopController.load_MontantPanier = function() {
    AjaxUtils.callService(
        'shop-panier', 
        'getMontantTotal', 
        AjaxUtils.dataTypes.json, 
        AjaxUtils.requestTypes.get, 
        null, 
        null, 
        function(response) {
            $('#bt-panier .button__text .badge').html(response.montant+' &euro;');
        },
        function(response) {}
    );
};

ShopController.onClick_btPanier = function() {
    $('#bt-panier').unbind('click');
    $('#bt-panier').click(function() {
        AjaxUtils.callService(
            'shop-panier', 
            'getNbArticles', 
            AjaxUtils.dataTypes.json, 
            AjaxUtils.requestTypes.get, 
            null, 
            null, 
            function(response) {
                if (response.nbArticles > 0) {
                    ShopController.load_ModalPanier();
                    $('#modal-panier').modal('show');
                } else {
                    ViewUtils.alert(
                        'Votre panier est vide !', 
                        ViewUtils.alertTypes.info,
                        null
                    );
                }
            },
            function(response) {}
        );
    });  
};

ShopController.load_ModalPanier = function() {
    AjaxUtils.loadView(
        'shop/common/modals/panier/modal-panier-content',
        '#modal-panier-content',
        null,
        function() {
            onClick_btCommander();
            control_DeleteArticlePanier();
        },
        '#loader-page'
    );

    function onClick_btCommander() {
        $('#modal-panier__bt-commander').unbind('click');
        $('#modal-panier__bt-commander').click(function() {
            FormUtils.hideFormErrors('#modal-panier #form-panier');
            ViewUtils.unsetHTMLAndHide('#modal-panier .form-result-message');
            ViewUtils.desactiveButtonAndShowLoader('#modal-panier__bt-commander', '#modal-panier__bt-commander .loader');

            var formValidation = FormUtils.validateForm('#modal-panier #form-panier');
            
            if (formValidation) {
                var inputNom = $.trim($('#form-panier__input-nom').val());
                var inputPrenom = $.trim($('#form-panier__input-prenom').val());
                var inputTelephone = $.trim($('#form-panier__input-telephone').val());
                var inputEmail = $.trim($('#form-panier__input-email').val());
                var inputMessage = $.trim($('#form-panier__input-message').val());

                sendCommandePanier(inputNom, inputPrenom, inputTelephone, inputEmail, inputMessage);
            } else {
                ViewUtils.activeButtonAndHideLoader('#modal-panier__bt-commander', '#modal-panier__bt-commander .loader');
            }
        });

        function sendCommandePanier(inputNom, inputPrenom, inputTelephone, inputEmail, inputMessage) {
            AjaxUtils.callService(
                'shop-panier', 
                'sendCommande', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    nom: inputNom,
                    prenom: inputPrenom,
                    telephone: inputTelephone,
                    email: inputEmail,
                    message: inputMessage
                }, 
                function(response) {
                    if (response.success) {
                        FormUtils.displayFormResultMessage('#modal-panier .form-result-message', 'Commande envoy&eacute;e', FormUtils.resultsTypes.ok);
                        $('#modal-panier__bt-commander .loader').hide();
                        ShopController.load_MontantPanier();
                    } else {
                        ViewUtils.activeButtonAndHideLoader('#modal-panier__bt-commander', '#modal-panier__bt-commander .loader');
                        FormUtils.displayFormResultMessage('#modal-panier .form-result-message', 'Erreur durant la commande', FormUtils.resultsTypes.error);
                    }
                },
                function(response) {
                    ViewUtils.activeButtonAndHideLoader('#modal-panier__bt-commander', '#modal-panier__bt-commander .loader');
                    FormUtils.displayFormResultMessage('#modal-panier .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                }
            );
        }
    }

    function control_DeleteArticlePanier() {
        $('.glyphicon-trash').unbind('click');
        $('.glyphicon-trash').click(function() {
            ViewUtils.unsetHTMLAndHide('#modal-panier .form-result-message');
    
            var idArticlePanier = $(this).attr('id-article-panier');
            
            $('.loader-delete[id-article-panier='+idArticlePanier+']').show();
            AjaxUtils.callService(
                'shop-panier', 
                'deleteArticle', 
                AjaxUtils.dataTypes.json, 
                AjaxUtils.requestTypes.post, 
                null, 
                {
                    idArticlePanier: idArticlePanier
                }, 
                function(response) {
                    $('.loader-delete[id-article-panier='+idArticlePanier+']').hide();
                    if (response.success) {
                        ShopController.load_ModalPanier();
                        ShopController.load_MontantPanier();
                    } else {
                        FormUtils.displayFormResultMessage('#modal-panier .form-result-message', 'Erreur durant la suppression', FormUtils.resultsTypes.error);
                    }
                },
                function(response) {
                    $('.loader-delete[id-article-panier='+idArticlePanier+']').hide();
                    FormUtils.displayFormResultMessage('#modal-panier .form-result-message', 'Service indisponible', FormUtils.resultsTypes.error);
                }
            );
        });
    }
};