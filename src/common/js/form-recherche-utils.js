FormRechercheUtils = {};

FormRechercheUtils.control_toggleFormRecherche = function() {
    $('.container-recherche__title').click(function() {
        var idFormToToggle = $(this).attr('for');
        $('#'+idFormToToggle).toggle();
        $('.container-recherche__footer[for='+idFormToToggle+']').toggle();

        var arrowGlyphicon = $(this).find('.container-recherche__title__arrow span.glyphicon');
        if (arrowGlyphicon.length > 0) {
            var arrowClass = arrowGlyphicon.attr('class');
            var oldArrow = 'top';
            var newArrow = 'bottom';
            if (arrowClass.indexOf('bottom') != -1) {
                var oldArrow = 'bottom';
                var newArrow = 'top';
            }
            arrowGlyphicon.removeClass('glyphicon-triangle-'+oldArrow);
            arrowGlyphicon.addClass('glyphicon-triangle-'+newArrow);
        }
    });
};