<?php

define(NB_MAX_ACTUS_HOME, "nb_max_actus_home");
define(NB_MAX_ACTUS_NEWSLETTER, "nb_max_actus_newsletter");
define(NB_MAX_ARTICLES_PRESSE_HOME, "nb_max_articles_presse_home");
define(NB_MAX_ARTICLES_PRESSE_NEWSLETTER, "nb_max_articles_presse_newsletter");
define(NB_MAX_RENCONTRES_A_AFFICHER_CATEGORIE, "nb_max_rencontres_a_afficher_categorie");
define(NB_MAX_PLACES_COMMANDE_SHOP_BILLETTERIE, "nb_max_places_commande_shop_billetterie");
define(NB_MAX_ARTICLES_COMMANDE_SHOP_BOUTIQUE, "nb_max_articles_commande_shop_boutique");
define(NB_MAX_CARACTERES_FLOCAGE_NOM, "nb_max_caracteres_flocage_nom");
define(NB_ACTIONS_CLUB_A_FAIRE, "nb_actions_club_a_faire");
define(NEWSLETTER_MESSAGE_DEFAUT, "newsletter_message_defaut");

define(DATA_FOLDER, "data_folder");
define(TEST_FOLDER, "test_folder");
define(WEBAPP_URL, "webapp_url");

define(NOREPLY_EMAIL, "noreply_email");

?>