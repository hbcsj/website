<?php

define(SESSION_HBCSJ, "hbcsj");

define(IS_DISPOS_CONNECTED, "isDisposConnected");
define(IS_ADMIN_CONNECTED, "isAdminConnected");
define(ADMIN_SUPERADMIN, "superadmin");
define(ADMIN_BUREAU, "bureau");
define(ADMIN_COACH, "coach");
define(ADMIN_EDITEUR, "editeur");
define(ADMIN_COMMERCIAL, "commercial");

define(SHOP_PANIER_ARTICLES, "shopPanierArticles");

?>