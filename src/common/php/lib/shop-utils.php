<?php
require_once("common/php/constants/session-constants.php");

define(SHOP_PANIER_TYPE_BOUTIQUE, "Boutique");
define(SHOP_PANIER_TYPE_BILLETTERIE, "Billetterie");

define(SHOP_PANIER_TYPE, "type");
define(SHOP_PANIER_PRODUIT_ID, "produit_id");
define(SHOP_PANIER_QUANTITE, "quantite");
define(SHOP_PANIER_TAILLE, "taille");
define(SHOP_PANIER_COULEUR, "couleur");
define(SHOP_PANIER_FLOCAGE_NOM, "flocage_nom");
define(SHOP_PANIER_FLOCAGE_NUMERO_AVANT, "flocage_numero_avant");
define(SHOP_PANIER_FLOCAGE_NUMERO_ARRIERE, "flocage_numero_arriere");
define(SHOP_PANIER_FLOCAGE_INITIALES, "flocage_initiales");
define(SHOP_PANIER_PRIX, "prix");
define(SHOP_PANIER_PRIX_FOURNISSEUR, "prix_fournisseur");

function shopPanierExists() {
    return (
        isset($_SESSION[SESSION_HBCSJ]) &&
        $_SESSION[SESSION_HBCSJ] != null &&
        isset($_SESSION[SESSION_HBCSJ][SHOP_PANIER_ARTICLES]) &&
        $_SESSION[SESSION_HBCSJ][SHOP_PANIER_ARTICLES] != null
    );
}

function checkShopPanierInitialized() {
    if (!shopPanierExists()) {
        $_SESSION[SESSION_HBCSJ][SHOP_PANIER_ARTICLES] = array();
    }
}

?>