<?php
require_once("common/php/constants/data-constants.php");

class PathUtils {
    
    public static function getPhotoCategorie($categorieAbreviation) {
		$photoPath = EQUIPES_FOLDER."/".MEDIUM_FOLDER."/".strtolower($categorieAbreviation).".".IMG_EQUIPES_EXTENSION;
        if (file_exists($photoPath)) {
            return $photoPath;
        }
        return PHOTO_EQUIPE_NOT_AVAILABLE_FILE;
    }

    public static function getActualiteHomeFile($actualiteId) {
        return NEWS_FOLDER."/".$actualiteId.NEWS_HOME_SUFFIXE.".".NEWS_EXTENSION;
    }

    public static function getActualiteFile($actualiteId) {
        return NEWS_FOLDER."/".$actualiteId.".".NEWS_EXTENSION;
    }

    public static function getActualiteImgFile($actualiteId) {
        return NEWS_FOLDER."/".$actualiteId.".".IMG_NEWS_EXTENSION;
    }

    public static function getArticlePresseImgHomeFile($articlePresseId) {
        return PRESSE_FOLDER."/".$articlePresseId.IMG_HOME_SUFFIXE.".".ARTICLES_PRESSE_EXTENSION;
    }

    public static function getArticlePresseImgFile($articlePresseId) {
        return PRESSE_FOLDER."/".$articlePresseId.".".ARTICLES_PRESSE_EXTENSION;
    }

    public static function getArticlePresseImgMediumFile($articlePresseId) {
        return PRESSE_FOLDER."/".MEDIUM_FOLDER."/".$articlePresseId.".".ARTICLES_PRESSE_EXTENSION;
    }

    public static function getArticlePresseImgSmallFile($articlePresseId) {
        return PRESSE_FOLDER."/".SMALL_FOLDER."/".$articlePresseId.".".ARTICLES_PRESSE_EXTENSION;
    }

    public static function getHHSJImgHomeFile($hhsjId) {
        return HAND_HEBDOS_SAINT_JEANNAIS_FOLDER."/".$hhsjId.IMG_HOME_SUFFIXE.".".HAND_HEBDOS_SAINT_JEANNAIS_UNES_EXTENSION;
    }

    public static function getHHSJImgFile($hhsjId) {
        return HAND_HEBDOS_SAINT_JEANNAIS_FOLDER."/".$hhsjId.".".HAND_HEBDOS_SAINT_JEANNAIS_UNES_EXTENSION;
    }

    public static function getHHSJImgMediumFile($hhsjId) {
        return HAND_HEBDOS_SAINT_JEANNAIS_FOLDER."/".MEDIUM_FOLDER."/".$hhsjId.".".HAND_HEBDOS_SAINT_JEANNAIS_UNES_EXTENSION;
    }

    public static function getHHSJImgSmallFile($hhsjId) {
        return HAND_HEBDOS_SAINT_JEANNAIS_FOLDER."/".SMALL_FOLDER."/".$hhsjId.".".HAND_HEBDOS_SAINT_JEANNAIS_UNES_EXTENSION;
    }

    public static function getHHSJFile($hhsjId) {
        return HAND_HEBDOS_SAINT_JEANNAIS_FOLDER."/".$hhsjId.".".HAND_HEBDOS_SAINT_JEANNAIS_COMPLET_EXTENSION;
    }

    public static function getAlbumPhotoImgFile($albumPhotoId) {
        return ALBUMS_PHOTOS_FOLDER."/".$albumPhotoId.".".IMG_ALBUMS_PHOTOS_EXTENSION;
    }

    public static function getAlbumPhotoImgSmallFile($albumPhotoId) {
        return ALBUMS_PHOTOS_FOLDER."/".SMALL_FOLDER."/".$albumPhotoId.".".IMG_ALBUMS_PHOTOS_EXTENSION;
    }

    public static function getVideoImgFile($videoId) {
        return VIDEOS_FOLDER."/".$videoId.".".IMG_VIDEOS_EXTENSION;
    }

    public static function getVideoImgSmallFile($videoId) {
        return VIDEOS_FOLDER."/".SMALL_FOLDER."/".$videoId.".".IMG_VIDEOS_EXTENSION;
    }

    public static function getProduitBoutiqueImgFile($produitBoutiqueId) {
        return PRODUITS_BOUTIQUE_FOLDER."/".$produitBoutiqueId.".".IMG_PRODUIT_BOUTIQUE_EXTENSION;
    }

    public static function getProduitBoutiqueDescriptionFile($produitBoutiqueId) {
        return PRODUITS_BOUTIQUE_FOLDER."/".$produitBoutiqueId.".".DESC_PRODUIT_BOUTIQUE_EXTENSION;
    }

    public static function getMarqueProduitBoutiqueImgFile($produitBoutiqueId) {
        return MARQUES_BOUTIQUE_FOLDER."/".$produitBoutiqueId.".".IMG_MARQUES_BOUTIQUE_EXTENSION;
    }

    public static function getProduitBoutiqueTaillesFile($taillesProduitBoutiqueId) {
        return TAILLES_BOUTIQUE_FOLDER."/".$taillesProduitBoutiqueId.".".DESC_PRODUIT_BOUTIQUE_EXTENSION;
    }

    public static function getAdversaireImgFile($adversaireBilletterieId) {
        return ADVERSAIRES_FENIX_BILLETTERIE_FOLDER."/".$adversaireBilletterieId.".".IMG_ADVERSAIRES_FENIX_BILLETTERIE_EXTENSION;
    }

    public static function getFDMMatchFile($matchId) {
        return FDM_FOLDER."/".$matchId.".".FDM_EXTENSION;
    }

    public static function getDocumentCoachingFile($nomFichier, $idDocumentCoaching) {
        return DOCUMENT_COACHING_FOLDER."/".$idDocumentCoaching."_".$nomFichier;
    }
}

?>