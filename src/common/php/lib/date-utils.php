<?php

define(SLASH_DATE_SEPARATOR, "/");
define(SLASH_DATE_TIME_SEPARATOR, " - ");
define(SLASH_TIME_SEPARATOR, "h");
define(SLASH_DATE_FORMAT, "d/m/Y");
define(SLASH_TIME_FORMAT, "H\hi");
define(SLASH_DATE_TIME_FORMAT, SLASH_DATE_FORMAT.SLASH_DATE_TIME_SEPARATOR.SLASH_TIME_FORMAT);

define(SQL_DATE_SEPARATOR, "-");
define(SQL_TIME_SEPARATOR, ":");
define(SQL_DATE_TIME_SEPARATOR, " ");
define(SQL_DATE_FORMAT, "Y-m-d");
define(SQL_TIME_FORMAT, "H:i:s");
define(SQL_DATE_TIME_FORMAT, SQL_DATE_FORMAT.SQL_DATE_TIME_SEPARATOR.SQL_TIME_FORMAT);

class DateUtils {

	public static function getNomJour($numJour) {
		$nomsJour = array(
			"Dimanche", 
			"Lundi", 
			"Mardi", 
			"Mercredi", 
			"Jeudi", 
			"Vendredi", 
			"Samedi", 
			"Dimanche"
		);
		return $nomsJour[$numJour];
	}
	
	public static function nbJoursDansMois($mois, $annee) {
		if ($mois == "04" || $mois == "06" || $mois == "09" || $mois == "11") { 
			return 30; 
		} else if ($mois == "02") {
			if (($annee % 400) == 0 || (($annee % 4) == 0 && ($annee % 100) != 0 )) { 
				return 29; 
			} else { 
				return 28; 
			}
		}
		else { 
			return 31; 
		}
	}

    public static function getMois() {
        return array(
			"01" => "Janvier",
			"02" => "F&eacute;vrier",
			"03" => "Mars",
			"04" => "Avril",
			"05" => "Mai",
			"06" => "Juin",
			"07" => "Juillet",
			"08" => "Ao&ucirc;t",
			"09" => "Septembre",
			"10" => "Octobre",
			"11" => "Novembre",
			"12" => "D&eacute;cembre",
		);
	}
	
	public static function convert_slashDate_to_sqlDate($date) {
		if ($date != "") {
			$dateSplitted = explode(SLASH_DATE_SEPARATOR, $date);
			return $dateSplitted[2].SQL_DATE_SEPARATOR.$dateSplitted[1].SQL_DATE_SEPARATOR.$dateSplitted[0];
		}
		return null;
	}

	public static function convert_sqlDate_to_slashDate($date) {
		if ($date != null) {
			$dateSplitted = explode(SQL_DATE_SEPARATOR, $date);
			return $dateSplitted[2].SLASH_DATE_SEPARATOR.$dateSplitted[1].SLASH_DATE_SEPARATOR.$dateSplitted[0];
		}
		return "";
	}

	public static function convert_sqlDateWithoutYear_to_slashDateWithoutYear($date) {
		if ($date != null) {
			$dateSplitted = explode(SQL_DATE_SEPARATOR, $date);
			return $dateSplitted[1].SLASH_DATE_SEPARATOR.$dateSplitted[0];
		}
		return "";
	}

	public static function convert_slashDateTime_to_sqlDateTime($dateTime) {
		if ($dateTime != null) {
			$dateTimeSplitted = explode(SLASH_DATE_TIME_SEPARATOR, $dateTime);
			$dateSplitted = explode(SLASH_DATE_SEPARATOR, $dateTimeSplitted[0]);
			$timeSplitted = explode(SLASH_TIME_SEPARATOR, $dateTimeSplitted[1]);
			return $dateSplitted[2].SQL_DATE_SEPARATOR.$dateSplitted[1].SQL_DATE_SEPARATOR.$dateSplitted[0].SQL_DATE_TIME_SEPARATOR.$timeSplitted[0].SQL_TIME_SEPARATOR.$timeSplitted[1].SQL_TIME_SEPARATOR."00";
		}
		return "";
	}

	public static function convert_sqlDateTime_to_slashDateTime($dateTime) {
		if ($dateTime != null) {
			$dateTimeSplitted = explode(SQL_DATE_TIME_SEPARATOR, $dateTime);
			$dateSplitted = explode(SQL_DATE_SEPARATOR, $dateTimeSplitted[0]);
			$timeSplitted = explode(SQL_TIME_SEPARATOR, $dateTimeSplitted[1]);
			return $dateSplitted[2].SLASH_DATE_SEPARATOR.$dateSplitted[1].SLASH_DATE_SEPARATOR.$dateSplitted[0].SLASH_DATE_TIME_SEPARATOR.$timeSplitted[0].SLASH_TIME_SEPARATOR.$timeSplitted[1];
		}
		return "";
	}

	public static function convert_sqlTime_to_timeWithoutSeconds($time) {
		if ($time != null) {
			$timeSplitted = explode(SQL_TIME_SEPARATOR, $time);
			return $timeSplitted[0].SLASH_TIME_SEPARATOR.$timeSplitted[1];
		}
		return "";
	}

	public static function convert_timeWithoutSeconds_to_sqlTime($time) {
		if ($time != null) {
			$timeSplitted = explode(SLASH_TIME_SEPARATOR, $time);
			return $timeSplitted[0].SQL_TIME_SEPARATOR.$timeSplitted[1].":00";
		}
		return "";
	}

	public static function get_sqlDateTime_timestamp($dateTime) {
		$dateTimeSplitted = explode(SQL_DATE_TIME_SEPARATOR, $dateTime);
		$dateSplitted = explode(SQL_DATE_SEPARATOR, $dateTimeSplitted[0]);
		$timeSplitted = explode(SQL_TIME_SEPARATOR, $dateTimeSplitted[1]);
		return mktime(
			$timeSplitted[0], 
			$timeSplitted[1], 
			$timeSplitted[2], 
			$dateSplitted[1], 
			$dateSplitted[2], 
			$dateSplitted[0]
		);
	}

	public static function get_sqlDate_timestamp($date) {
		$dateSplitted = explode(SQL_DATE_SEPARATOR, $date);
		return mktime(
			0, 
			0, 
			0, 
			$dateSplitted[1], 
			$dateSplitted[2], 
			$dateSplitted[0]
		);
	}

	public static function get_slashDateTime_timestamp($dateTime) {
		$dateTimeSplitted = explode(SLASH_DATE_TIME_SEPARATOR, $dateTime);
		$dateSplitted = explode(SLASH_DATE_SEPARATOR, $dateTimeSplitted[0]);
		$timeSplitted = explode(SLASH_TIME_SEPARATOR, $dateTimeSplitted[1]);
		return mktime(
			$timeSplitted[0], 
			$timeSplitted[1], 
			$timeSplitted[2], 
			$dateSplitted[1], 
			$dateSplitted[0], 
			$dateSplitted[2]
		);
	}

	public static function get_slashDate_timestamp($date) {
		$dateSplitted = explode(SLASH_DATE_SEPARATOR, $date);
		return mktime(
			0, 
			0, 
			0, 
			$dateSplitted[1], 
			$dateSplitted[0], 
			$dateSplitted[2]
		);
	}

	public static function get_annee_debutSaison() {
		if (date("m") == "09" || date("m") == "10" || date("m") == "11" || date("m") == "12") {
			return date("Y");
		} else {
			return (date("Y") - 1);
		}
	}

	public static function get_slashDate_debutSaison() {
		if (date("m") == "09" || date("m") == "10" || date("m") == "11" || date("m") == "12") {
			return "01".SLASH_DATE_SEPARATOR."09".SLASH_DATE_SEPARATOR.date("Y");
		} else {
			return "01".SLASH_DATE_SEPARATOR."09".SLASH_DATE_SEPARATOR.(date("Y") - 1);
		}
	}

	public static function get_sqlDate_debutSaison() {
		return self::convert_slashDate_to_sqlDate(self::get_slashDate_debutSaison());
	}

	public static function ajouterJoursADateDuJour($daysToAdd, $format) {
		return date(
            $format, 
            mktime(0, 0, 0, date("m"), date("d") + $daysToAdd, date("Y"))
        );
	}
}

?>