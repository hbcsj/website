<?php
require_once("core/php/lib/mailer.php");
require_once("common/php/constants/config-constants.php");

define(ACTION_TYPE_CREATION, "Creation");
define(ACTION_TYPE_MODIFICATION, "Modification");
define(ACTION_TYPE_SUPPRESSION, "Suppression");

define(OBJET_ASSOCIE_TYPE, "type");
define(OBJET_ASSOCIE_ID, "id");
define(OBJET_ASSOCIE_LABEL, "label");

class MailUtils {

    public static function notifySuperadmin($elementType, $actionType, $objetsAssocies, $databaseConnection) {
        require_once("common/php/dao/infos-club-dao.php");
        $infosClubDAO = new InfosClubDAO($databaseConnection);
        $infosClub = $infosClubDAO->getSingleton();

        $sujet = "[Site du ".$infosClub[INFOS_CLUB_NOM_XS]." - Admin] - ".$actionType." d'un element - ".$elementType;
        
        $message = "";
        if (sizeof($objetsAssocies) > 0) {
            foreach ($objetsAssocies as $objetAssocie) {
                $message .= "<br>".$objetAssocie[OBJET_ASSOCIE_TYPE].":<br>";
                $message .= OBJET_ASSOCIE_ID." = ".$objetAssocie[OBJET_ASSOCIE_ID]."<br>";
                $message .= OBJET_ASSOCIE_LABEL." = ".$objetAssocie[OBJET_ASSOCIE_LABEL]."<br>";
            }
        }
        
        $mailer = new Mailer($GLOBALS[CONF][NOREPLY_EMAIL], $GLOBALS[CONF][SUPERADMIN_EMAIL], $sujet, $message);
        $mailer->send();
    }

    public static function test($mailFilename, $mail) {
        if ($GLOBALS[CONF][IS_DEV]) {
            $fp = fopen($GLOBALS[CONF][TEST_FOLDER]."/".$mailFilename, "w");
            fputs($fp, "<html><head></head><body>".$mail."</body></html>");
            fclose($fp);
        }
    }
}

?>