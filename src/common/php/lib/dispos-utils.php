<?php
require_once("common/php/constants/session-constants.php");

function isDisposConnected() {
    return (
        isset($_SESSION[SESSION_HBCSJ]) &&
        $_SESSION[SESSION_HBCSJ] != null &&
        isset($_SESSION[SESSION_HBCSJ][IS_DISPOS_CONNECTED]) &&
        $_SESSION[SESSION_HBCSJ][IS_DISPOS_CONNECTED] != null &&
        $_SESSION[SESSION_HBCSJ][IS_DISPOS_CONNECTED]
    );
}

?>