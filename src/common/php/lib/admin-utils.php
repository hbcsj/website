<?php
require_once("common/php/constants/session-constants.php");

function isAdminConnected() {
    return (
        isset($_SESSION[SESSION_HBCSJ]) &&
        $_SESSION[SESSION_HBCSJ] != null &&
        isset($_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED]) &&
        $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED] != null &&
        $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED] && 
        (
            (
                isset($_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_SUPERADMIN]) &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_SUPERADMIN] != null &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_SUPERADMIN]
            ) || (
                isset($_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_BUREAU]) &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_BUREAU] != null &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_BUREAU]
            ) || (
                isset($_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_COACH]) &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_COACH] != null &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_COACH]
            ) || (
                isset($_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_EDITEUR]) &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_EDITEUR] != null &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_EDITEUR]
            ) || (
                isset($_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_COMMERCIAL]) &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_COMMERCIAL] != null &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_COMMERCIAL]
            )
        )
    );
}

function isAdminConnected_superadmin() {
    return (
        isset($_SESSION[SESSION_HBCSJ]) &&
        $_SESSION[SESSION_HBCSJ] != null &&
        isset($_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED]) &&
        $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED] != null &&
        $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED] && 
        isset($_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_SUPERADMIN]) &&
        $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_SUPERADMIN] != null &&
        $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_SUPERADMIN]
    );
}

function isAdminConnected_bureau() {
    return (
        isset($_SESSION[SESSION_HBCSJ]) &&
        $_SESSION[SESSION_HBCSJ] != null &&
        isset($_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED]) &&
        $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED] != null &&
        $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED] && 
        (
            (
                isset($_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_SUPERADMIN]) &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_SUPERADMIN] != null &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_SUPERADMIN]
            ) || (
                isset($_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_BUREAU]) &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_BUREAU] != null &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_BUREAU]
            )
        )
    );
}

function isAdminConnected_coach() {
    return (
        isset($_SESSION[SESSION_HBCSJ]) &&
        $_SESSION[SESSION_HBCSJ] != null &&
        isset($_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED]) &&
        $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED] != null &&
        $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED] && 
        (
            (
                isset($_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_SUPERADMIN]) &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_SUPERADMIN] != null &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_SUPERADMIN]
            ) || (
                isset($_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_BUREAU]) &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_BUREAU] != null &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_BUREAU]
            ) || (
                isset($_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_COACH]) &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_COACH] != null &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_COACH]
            )
        )
    );
}

function isAdminConnected_editeur() {
    return (
        isset($_SESSION[SESSION_HBCSJ]) &&
        $_SESSION[SESSION_HBCSJ] != null &&
        isset($_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED]) &&
        $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED] != null &&
        $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED] && 
        (
            (
                isset($_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_SUPERADMIN]) &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_SUPERADMIN] != null &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_SUPERADMIN]
            ) || (
                isset($_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_BUREAU]) &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_BUREAU] != null &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_BUREAU]
            ) || (
                isset($_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_EDITEUR]) &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_EDITEUR] != null &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_EDITEUR]
            )
        )
    );
}

function isAdminConnected_commercial() {
    return (
        isset($_SESSION[SESSION_HBCSJ]) &&
        $_SESSION[SESSION_HBCSJ] != null &&
        isset($_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED]) &&
        $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED] != null &&
        $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED] && 
        (
            (
                isset($_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_SUPERADMIN]) &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_SUPERADMIN] != null &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_SUPERADMIN]
            ) || (
                isset($_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_BUREAU]) &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_BUREAU] != null &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_BUREAU]
            ) || (
                isset($_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_COMMERCIAL]) &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_COMMERCIAL] != null &&
                $_SESSION[SESSION_HBCSJ][IS_ADMIN_CONNECTED][ADMIN_COMMERCIAL]
            )
        )
    );
}

?>