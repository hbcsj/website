<?php
require_once("core/php/lib/abstract-manager.php");
require_once("common/php/constants/data-constants.php");
require_once("common/php/dao/evenement-dao.php");
require_once("common/php/dao/categorie-dao.php");
require_once("common/php/dao/categorie-participe-a-evenement-dao.php");
require_once("common/php/dao/ressources-externes-dao.php");
require_once("common/php/dao/competition-dao.php");
require_once("common/php/dao/type-evenement-dao.php");
require_once("common/php/dao/gymnase-dao.php");
require_once("common/php/lib/date-utils.php");
require_once("common/php/lib/path-utils.php");

define(EVENEMENT_TYPE, "type");
define(EVENEMENT_CATEGORIES, "categories");
define(EVENEMENT_JOUR, "jour");
define(EVENEMENT_GOOGLE_MAP_URL, "google_map_url");
define(EVENEMENT_GYMNASE, "gymnase");

define(MATCH_EQUIPE_DOMICILE, "equipe_domicile");
define(MATCH_SCORE_DOMICILE, "score_domicile");
define(MATCH_EQUIPE_EXTERIEUR, "equipe_exterieur");
define(MATCH_SCORE_EXTERIEUR, "score_exterieur");
define(MATCH_CLASS_RESULTAT, "class_resultat");
define(MATCH_TEXT_RESULTAT, "text_resultat");
define(MATCH_COMPETITION, "competition");
define(MATCH_FDM, "fdm");

class EvenementManager extends AbstractManager {

    private $evenementDAO;
    private $categorieDAO;
    private $categorieParticipeAEvenementDAO;
    private $competitionDAO;
    private $ressourcesExternesDAO;
	private $typeEvenementDAO;
	private $gymnaseDAO;

    public function __construct($databaseConnection = null) {
        parent::__construct($databaseConnection);

        $this->ressourcesExternesDAO = new RessourcesExternesDAO($this->getDatabaseConnection());
        $this->evenementDAO = new EvenementDAO($this->getDatabaseConnection());
        $this->categorieDAO = new CategorieDAO($this->getDatabaseConnection());
        $this->categorieParticipeAEvenementDAO = new CategorieParticipeAEvenementDAO($this->getDatabaseConnection());
        $this->competitionDAO = new CompetitionDAO($this->getDatabaseConnection());
        $this->typeEvenementDAO = new TypeEvenementDAO($this->getDatabaseConnection());
        $this->gymnaseDAO = new GymnaseDAO($this->getDatabaseConnection());
    }

    public function getRecapMatchProps($evenement) {
        $props = null;
        
        if ($evenement[EVENEMENT_TYPE_EVENEMENT_ID] == EVENEMENT_MATCH_TYPE_ID) {
            $categorie = $this->getCategorieMatch($evenement[EVENEMENT_ID]);
            
            if ($categorie != null) {
                $competition = $this->competitionDAO->getById($evenement[EVENEMENT_COMPETITION_ID]);
    
                $equipeDomicile = "";
                $scoreDomicile = "";
                $equipeExterieur = "";
                $scoreExterieur = "";
                $classResultat = "";
                $textResultat = "";
                $jour = "";
                $dateHeure = "";
                $googleMapURL = "";
                $nomGymnase = "";

                if ($evenement[EVENEMENT_A_DOMICILE]) {
                    $equipeDomicile = "";
                    if (is_numeric(substr($categorie[CATEGORIE_ABREVIATION], 0, 1))) {
                        $equipeDomicile .= "-";
                    }
                    $equipeDomicile .= $categorie[CATEGORIE_ABREVIATION].$evenement[EVENEMENT_NUM_EQUIPE];
                    $scoreDomicile = $evenement[EVENEMENT_SCORE_HBCSJ];
                    $equipeExterieur = $evenement[EVENEMENT_ADVERSAIRE];
                    $scoreExterieur = $evenement[EVENEMENT_SCORE_ADVERSAIRE];
                } else {
                    $equipeExterieur = "";
                    if (is_numeric(substr($categorie[CATEGORIE_ABREVIATION], 0, 1))) {
                        $equipeExterieur .= "-";
                    }
                    $equipeExterieur .= $categorie[CATEGORIE_ABREVIATION].$evenement[EVENEMENT_NUM_EQUIPE];
                    $scoreExterieur = $evenement[EVENEMENT_SCORE_HBCSJ];
                    $equipeDomicile = $evenement[EVENEMENT_ADVERSAIRE];
                    $scoreDomicile = $evenement[EVENEMENT_SCORE_ADVERSAIRE];
                }
    
                if ($evenement[EVENEMENT_SCORE_HBCSJ] > $evenement[EVENEMENT_SCORE_ADVERSAIRE]) {
                    $classResultat = "victoire";
                    $textResultat = "Victoire";
                } else if ($evenement[EVENEMENT_SCORE_HBCSJ] < $evenement[EVENEMENT_SCORE_ADVERSAIRE]) {
                    $classResultat = "defaite";
                    $textResultat = "D&eacute;faite";
                } else if ($evenement[EVENEMENT_SCORE_HBCSJ] == $evenement[EVENEMENT_SCORE_ADVERSAIRE]) {
                    $classResultat = "nul";
                    $textResultat = "Match nul";
                }
                
                $fdm = PathUtils::getFDMMatchFile($evenement[EVENEMENT_ID]);
                if (!file_exists($fdm)) {
                    $fdm = "";
                }
                
                $gymnase = $this->gymnaseDAO->getById($evenement[EVENEMENT_GYMNASE_ID]);
                if ($gymnase != null) {
                    $nomGymnase = $gymnase[GYMNASE_NOM];
                }
                
                $props = array();
                $props[MATCH_EQUIPE_DOMICILE] = $equipeDomicile;
                $props[MATCH_SCORE_DOMICILE] = $scoreDomicile;
                $props[MATCH_EQUIPE_EXTERIEUR] = $equipeExterieur;
                $props[MATCH_SCORE_EXTERIEUR] = $scoreExterieur;
                $props[MATCH_CLASS_RESULTAT] = $classResultat;
                $props[MATCH_TEXT_RESULTAT] = $textResultat;
                $props[MATCH_COMPETITION] = $competition;
                $props[EVENEMENT_JOUR] = $this->getJourEvenement($evenement);
                $props[MATCH_FDM] = $fdm;
                $props[EVENEMENT_GOOGLE_MAP_URL] = $this->getGoogleMapURL($evenement);
                $props[EVENEMENT_GYMNASE] = $nomGymnase;
            }
        }  

        return $props;
    }

    public function getRecapEvenementProps($evenement) {
        $props = null;
        
        if ($evenement[EVENEMENT_TYPE_EVENEMENT_ID] == EVENEMENT_MATCH_TYPE_ID) {
            return $this->getRecapMatchProps($evenement);
        } else {
            $type = "";
            $categories = array();
            $jour = "";
            $dateHeure = "";
            $googleMapURL = "";
            $nomGymnase = "";

            $categories = $this->getCategoriesEvenement($evenement[EVENEMENT_ID]);

            $typeEvenement = $this->typeEvenementDAO->getById($evenement[EVENEMENT_TYPE_EVENEMENT_ID]);
            if ($typeEvenement != null) {
                $type = $typeEvenement[TYPE_EVENEMENT_LIBELLE];
            }

            $gymnase = $this->gymnaseDAO->getById($evenement[EVENEMENT_GYMNASE_ID]);
            if ($gymnase != null) {
                $nomGymnase = $gymnase[GYMNASE_NOM];
            }

            $props = array();
            $props[EVENEMENT_TYPE] = $type;
            $props[EVENEMENT_CATEGORIES] = $categories;
            $props[EVENEMENT_JOUR] = $this->getJourEvenement($evenement);
            $props[EVENEMENT_GOOGLE_MAP_URL] = $this->getGoogleMapURL($evenement);
            $props[EVENEMENT_GYMNASE] = $nomGymnase;
        }

        return $props;
    }

    private function getJourEvenement($evenement) {
        return DateUtils::getNomJour(
            date("w", DateUtils::get_sqlDateTime_timestamp($evenement[EVENEMENT_DATE_HEURE]))
        );
    }

    private function getGoogleMapURL($evenement) {
        $googleMapURL = "";

        if ($evenement[EVENEMENT_VILLE] != "") {
            $googleMapBaseURL = $this->ressourcesExternesDAO->getSingleton()[RESSOURCES_EXTERNES_GOOGLE_MAP_URL];
            if ($evenement[EVENEMENT_LATITUDE] != "" && $evenement[EVENEMENT_LONGITUDE] != "") {
                $googleMapURL = $googleMapBaseURL.$evenement[EVENEMENT_LATITUDE].",".$evenement[EVENEMENT_LONGITUDE]; 
            } else {
                $googleMapURL = $googleMapBaseURL.trim($evenement[EVENEMENT_ADRESSE]." ".$evenement[EVENEMENT_CODE_POSTAL]." ".$evenement[EVENEMENT_VILLE]); 
            }
        }

        return $googleMapURL;
    }

    public function getCategorieMatch($idEvenement) {
        $categoriesMatch = $this->getCategoriesEvenement($idEvenement);
        if (sizeof($categoriesMatch) >= 1) {
            return $categoriesMatch[0];
        }
        return null;
    }

    public function getCategoriesEvenement($idEvenement) {
        $categoriesParticipentEvenement = $this->categorieParticipeAEvenementDAO->getByEvenementId($idEvenement);

        $categories = array();
        if (sizeof($categoriesParticipentEvenement) > 0) {
            foreach ($categoriesParticipentEvenement as $categorieParticipeEvenement) {
                $categorie = $this->categorieDAO->getById($categorieParticipeEvenement[CATEGORIE_PARTICIPE_A_EVENEMENT_CATEGORIE_ID]);
                $categories[] = $categorie;
            }
        }

        return $categories;
    }

    public function getEvenementsForDispos($categorie) {
        $evenements = $this->evenementDAO->getAllByParams(
            date(SQL_DATE_FORMAT), 
            null, 
            $categorie[CATEGORIE_ID], 
            null, 
            null, 
            null, 
            null, 
            null, 
            null, 
            null, 
            EVENEMENT_TABLE_NAME.".".EVENEMENT_DATE_HEURE
        );
    
        $evenementsForDispos = array();

        if (sizeof($evenements) > 0) {
            if ($categorie[CATEGORIE_REGROUPEMENT_MATCHS] == 1) {
                foreach ($evenements as $evenement) {
                    $evenementDateTimeSplitted = explode(SQL_DATE_TIME_SEPARATOR, $evenement[EVENEMENT_DATE_HEURE]);
                    $evenementDateGroup = $evenementDateTimeSplitted[0];
                    if (!array_key_exists($evenementDateGroup, $evenementsForDispos)) {
                        $evenementsForDispos[$evenementDateGroup] = array();
                    }
                    $evenementsForDispos[$evenementDateGroup][] = $evenement;
                }
            } else {
                foreach ($evenements as $evenement) {
                    $evenementsForDispos[$evenement[EVENEMENT_ID]] = array();
                    $evenementsForDispos[$evenement[EVENEMENT_ID]][0] = $evenement;
                }
            }
        }

        return $evenementsForDispos;
    }

    public function getEvenementsGroupedForDisposByKey($key, $categorie) {
        $evenementsForDispos = $this->getEvenementsForDispos($categorie);
        return $evenementsForDispos[$key];
    }

    public function getEvenementsForActionsClubAndArbitrages() {
        return $this->evenementDAO->getAllByParams(
            date(SQL_DATE_FORMAT), 
            null, 
            null, 
            null, 
            null, 
            null, 
            null, 
            null, 
            null, 
            1, 
            EVENEMENT_TABLE_NAME.".".EVENEMENT_DATE_HEURE
        );
    }
}

?>