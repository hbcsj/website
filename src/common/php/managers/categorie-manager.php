<?php
require_once("core/php/lib/abstract-manager.php");
require_once("common/php/dao/licencie-dao.php");
require_once("common/php/dao/licencie-coache-categorie-dao.php");

define(COACH_EN_SOUTIEN, "coach_en_soutien");

class CategorieManager extends AbstractManager {

    private $licencieDAO;
    private $licencieCoacheCategorieDAO;

    public function __construct($databaseConnection = null) {
        parent::__construct($databaseConnection);

        $this->licencieDAO = new LicencieDAO($this->getDatabaseConnection());
        $this->licencieCoacheCategorieDAO = new LicencieCoacheCategorieDAO($this->getDatabaseConnection());
    }

    public function getCoachsCategorie($categorieId) {
        $licencies = array();
        $licenciesCoachsCategorie = $this->licencieCoacheCategorieDAO->getByCategorieId(
            $categorieId, 
            LICENCIE_COACHE_CATEGORIE_TABLE_NAME.".".LICENCIE_COACHE_CATEGORIE_EN_SOUTIEN
        );

        if (sizeof($licenciesCoachsCategorie) > 0) {
            foreach ($licenciesCoachsCategorie as $licencieCoachCategorie) {
                $licencie = $this->licencieDAO->getById($licencieCoachCategorie[LICENCIE_COACHE_CATEGORIE_LICENCIE_ID]);
                $licencie[COACH_EN_SOUTIEN] = ($licencieCoachCategorie[LICENCIE_COACHE_CATEGORIE_EN_SOUTIEN] == 1) ? true : false;
                $licencies[] = $licencie;
            }
        }

        return $licencies;
    }

    public function getLicenciesCategorie($categorieId, $withCoachs = false) {
        if ($withCoachs) {
            return $this->licencieDAO->getJoueursAndCoachsByCategorieId($categorieId, LICENCIE_TABLE_NAME.".".LICENCIE_NOM.", ".LICENCIE_TABLE_NAME.".".LICENCIE_PRENOM);
        }
        return $this->licencieDAO->getAllByParams(null, null, $categorieId, null, null, null, null, LICENCIE_TABLE_NAME.".".LICENCIE_NOM.", ".LICENCIE_TABLE_NAME.".".LICENCIE_PRENOM);
    }
}

?>