<?php
require_once("core/php/lib/abstract-manager.php");
require_once("common/php/dao/article-presse-dao.php");
require_once("common/php/dao/hhsj-dao.php");

define(TYPE_HHSJ_UNE_DE_MAGAZINE_ID, 1);
define(TYPE_HHSJ_MAGAZINE_COMPLET_ID, 2);
define(TYPE_HHSJ_PETIT_SAINT_JEANNAIS_ID, 3);

define(HHSJ_ID_PREFIX, "HHSJ");
define(ARTICLE_PRESSE_ID_PREFIX, "AP");

class PresseManager extends AbstractManager {

    private $articlePresseDAO;
    private $hhsjDAO;

    public function __construct($databaseConnection = null) {
        parent::__construct($databaseConnection);

        $this->articlePresseDAO = new ArticlePresseDAO($this->getDatabaseConnection());
        $this->hhsjDAO = new HHSJDAO($this->getDatabaseConnection());
    }

    public function getDerniersArticlesPresse($nbMaxArticlesPresse) {
        $articlesPresse = $this->articlePresseDAO->getVisibles(ARTICLE_PRESSE_TABLE_NAME.".".ARTICLE_PRESSE_DATE." DESC");
        $hhsjs = $this->hhsjDAO->getVisibles(HHSJ_TABLE_NAME.".".HHSJ_DATE." DESC");

        $allArticles = array_merge($articlesPresse, $hhsjs);
        foreach ($allArticles as $key => $row) {
            $date[$key] = $row["date"];
            $id[$key] = $row["id"];
        }
        array_multisort($date, $id, $allArticles);
        $allArticles = array_reverse($allArticles);

        $derniersArticles = array();
        for ($i = 0 ; $i < sizeof($allArticles) && $i < $nbMaxArticlesPresse ; $i++) {
            $derniersArticles[] = $allArticles[$i];
        }

        return $derniersArticles;
    }

    public function getArticlesPresseApresDate($date) {
        $articlesPresse = $this->articlePresseDAO->getVisiblesApresDate($date, ARTICLE_PRESSE_TABLE_NAME.".".ARTICLE_PRESSE_DATE." DESC");
        $hhsjs = $this->hhsjDAO->getVisiblesApresDate($date, HHSJ_TABLE_NAME.".".HHSJ_DATE." DESC");

        return array_merge($articlesPresse, $hhsjs);
    }
}

?>