<?php
require_once("core/php/lib/abstract-manager.php");
require_once("common/php/dao/gymnase-dao.php");

class GymnaseManager extends AbstractManager {

    private $gymnaseDAO;

    public function __construct($databaseConnection = null) {
        parent::__construct($databaseConnection);

        $this->gymnaseDAO = new GymnaseDAO($this->getDatabaseConnection());
    }

    public function getGymnasePrincipal() {
        $gymnasePrincipal = null;

        $gymnasesADomicile = $this->gymnaseDAO->getADomicile();

        if (sizeof($gymnasesADomicile) > 0) {
            $gymnasePrincipal = $gymnasesADomicile[0];
        }

        return $gymnasePrincipal;
    }
}

?>