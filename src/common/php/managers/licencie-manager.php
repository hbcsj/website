<?php
require_once("core/php/lib/abstract-manager.php");
require_once("common/php/dao/categorie-dao.php");
require_once("common/php/dao/licencie-coache-categorie-dao.php");
require_once("common/php/dao/licencie-joue-dans-categorie-dao.php");

define(COACH_EN_SOUTIEN, "coach_en_soutien");

class LicencieManager extends AbstractManager {

    private $categorieDAO;
    private $licencieCoacheCategorieDAO;
    private $licencieJoueDansCategorieDAO;

    public function __construct($databaseConnection = null) {
        parent::__construct($databaseConnection);

        $this->categorieDAO = new CategorieDAO($this->getDatabaseConnection());
        $this->licencieCoacheCategorieDAO = new LicencieCoacheCategorieDAO($this->getDatabaseConnection());
        $this->licencieJoueDansCategorieDAO = new LicencieJoueDansCategorieDAO($this->getDatabaseConnection());
    }

    public function getCategoriesLicencie($licencieId) {
        $categories = array();
        $categoriesJoueLicencie = $this->licencieJoueDansCategorieDAO->getByLicencieId(
            $licencieId, 
            LICENCIE_JOUE_DANS_CATEGORIE_TABLE_NAME.".".LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID
        );
        
        if (sizeof($categoriesJoueLicencie) > 0) {
            foreach ($categoriesJoueLicencie as $categorieJoueLicencie) {
                $categorie = $this->categorieDAO->getById($categorieJoueLicencie[LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID]);
                $categories[] = $categorie;
            }
        }

        return $categories;
    }

    public function getCategoriesCoacheLicencie($licencieId) {
        $categories = array();
        $licencieCoacheCategories = $this->licencieCoacheCategorieDAO->getByLicencieId(
            $licencieId, 
            LICENCIE_COACHE_CATEGORIE_TABLE_NAME.".".LICENCIE_COACHE_CATEGORIE_EN_SOUTIEN
        );

        if (sizeof($licencieCoacheCategories) > 0) {
            foreach ($licencieCoacheCategories as $licencieCoacheCategorie) {
                $categorie = $this->categorieDAO->getById($licencieCoacheCategorie[LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID]);
                $categorie[COACH_EN_SOUTIEN] = ($licencieCoacheCategorie[LICENCIE_COACHE_CATEGORIE_EN_SOUTIEN] == 1) ? true : false;
                $categories[] = $categorie;
            }
        }

        return $categories;
    }
}

?>