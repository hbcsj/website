<?php
require_once("core/php/lib/abstract-manager.php");
require_once("common/php/dao/evenement-dao.php");
require_once("common/php/dao/dispo-dao.php");
require_once("common/php/dao/licencie-dao.php");

define(DISPO_TOTAL_SUFFIX, "_TOTAL");

class DispoManager extends AbstractManager {

    private $dispoDAO;
    private $licencieDAO;

    public function __construct($databaseConnection = null) {
        parent::__construct($databaseConnection);

        $this->dispoDAO = new DispoDAO($this->getDatabaseConnection());
        $this->licencieDAO = new LicencieDAO($this->getDatabaseConnection());
    }

    public function getDisposLicencieByEvenementsAndTypeDispo($licencieId, $evenements, $typeDispoId) {
        $disposArray = array();

        $disposArray[DISPO_DISPONIBLE_REPONSE_ID] = array();
        $disposArray[DISPO_DISPONIBLE_REPONSE_ID.DISPO_TOTAL_SUFFIX] = array();
        $disposArray[DISPO_SI_NECESSAIRE_REPONSE_ID] = array();
        $disposArray[DISPO_SI_NECESSAIRE_REPONSE_ID.DISPO_TOTAL_SUFFIX] = array();
        $disposArray[DISPO_ABSENT_REPONSE_ID] = array();
        $disposArray[DISPO_ABSENT_REPONSE_ID.DISPO_TOTAL_SUFFIX] = array();
        $disposArray[DISPO_PAS_DE_REPONSE_REPONSE_ID] = array();
        $disposArray[DISPO_PAS_DE_REPONSE_REPONSE_ID.DISPO_TOTAL_SUFFIX] = array();

        if (sizeof($evenements) > 0) {
            foreach ($evenements as $evenementsGrouped) {
                foreach ($evenementsGrouped as $evenement) {
                    $dispoId = array();
                    $dispoId[DISPO_LICENCIE_ID] = $licencieId;
                    $dispoId[DISPO_EVENEMENT_ID] = $evenement[EVENEMENT_ID];
                    $dispoId[DISPO_TYPE_DISPO_ID] = $typeDispoId;
                    $dispo = $this->dispoDAO->getById($dispoId);

                    if ($dispo != null) {
                        if ($evenement[EVENEMENT_TYPE_EVENEMENT_ID] == EVENEMENT_MATCH_TYPE_ID) {
                            $disposArray[$dispo[DISPO_REPONSE_DISPO_ID]][] = $dispo;
                            $disposArray[$dispo[DISPO_REPONSE_DISPO_ID].DISPO_TOTAL_SUFFIX][] = $dispo;
                        } else {
                            $disposArray[$dispo[DISPO_REPONSE_DISPO_ID].DISPO_TOTAL_SUFFIX][] = $dispo;
                        }
                    } else {
                        if ($evenement[EVENEMENT_TYPE_EVENEMENT_ID] == EVENEMENT_MATCH_TYPE_ID) {
                            $disposArray[DISPO_PAS_DE_REPONSE_REPONSE_ID][] = $dispo;
                            $disposArray[DISPO_PAS_DE_REPONSE_REPONSE_ID.DISPO_TOTAL_SUFFIX][] = $dispo;
                        } else {
                            $disposArray[DISPO_PAS_DE_REPONSE_REPONSE_ID.DISPO_TOTAL_SUFFIX][] = $dispo;
                        }
                    }
                }
            }
        }

        return $disposArray;
    }

    public function getParticipationsByEvenementsGrouped($evenementsGrouped, $categorieId) {
        $disposArray = array();

        $disposArray[DISPO_DISPONIBLE_REPONSE_ID] = array();
        $disposArray[DISPO_SI_NECESSAIRE_REPONSE_ID] = array();
        $disposArray[DISPO_ABSENT_REPONSE_ID] = array();
        $disposArray[DISPO_PAS_DE_REPONSE_REPONSE_ID] = array();

        if (sizeof($evenementsGrouped) > 0) {
            $disposArray[DISPO_DISPONIBLE_REPONSE_ID] = $this->dispoDAO->getByEvenementIdAndTypeDispoIdAndReponseId($evenementsGrouped[0][EVENEMENT_ID], DISPO_PARTICIPATION_TYPE_ID, DISPO_DISPONIBLE_REPONSE_ID);
            $disposArray[DISPO_SI_NECESSAIRE_REPONSE_ID] = $this->dispoDAO->getByEvenementIdAndTypeDispoIdAndReponseId($evenementsGrouped[0][EVENEMENT_ID], DISPO_PARTICIPATION_TYPE_ID, DISPO_SI_NECESSAIRE_REPONSE_ID);
            $disposArray[DISPO_ABSENT_REPONSE_ID] = $this->dispoDAO->getByEvenementIdAndTypeDispoIdAndReponseId($evenementsGrouped[0][EVENEMENT_ID], DISPO_PARTICIPATION_TYPE_ID, DISPO_ABSENT_REPONSE_ID);
            
            $licenciesNotInDispos = $this->licencieDAO->getIdsByCategorieIdNotInDisposByEvenementIdAndTypeDispoId($categorieId, $evenementsGrouped[0][EVENEMENT_ID], DISPO_PARTICIPATION_TYPE_ID);
            if (sizeof($licenciesNotInDispos) > 0) {
                foreach ($licenciesNotInDispos as $licencieNotInDispos) {
                    $dispoToAddInPasDeReponse = array();
                    $dispoToAddInPasDeReponse[DISPO_LICENCIE_ID] = $licencieNotInDispos[LICENCIE_ID];
                    $dispoToAddInPasDeReponse[DISPO_DISPO_EVENEMENT_ID] = $evenementsGrouped[0][EVENEMENT_ID];
                    $dispoToAddInPasDeReponse[DISPO_TYPE_DISPO_ID] = DISPO_PARTICIPATION_TYPE_ID;
                    $dispoToAddInPasDeReponse[DISPO_REPONSE_DISPO_ID] = DISPO_PAS_DE_REPONSE_REPONSE_ID;
                    $disposArray[DISPO_PAS_DE_REPONSE_REPONSE_ID][] = $dispoToAddInPasDeReponse;
                }
            }
        }

        return $disposArray;
    }
}

?>