<?php
require_once("core/php/lib/abstract-dao.php");

define(SAISON_TABLE_NAME, "hbcsj_saison");
define(SAISON_ID, "id");
define(SAISON_LIBELLE, "libelle");

class SaisonDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(SAISON_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".SAISON_TABLE_NAME." WHERE ".SAISON_ID." = :".SAISON_ID;
        $params = array(
            SAISON_ID => $id
        );
        $columns = array(
            SAISON_ID, 
            SAISON_LIBELLE
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }

    public function getByAnneeDebutSaison($annee) {
        $query = "SELECT * FROM ".SAISON_TABLE_NAME." WHERE ".SAISON_LIBELLE." LIKE :".SAISON_LIBELLE;
        $params = array(
            SAISON_ID => $annee."%"
        );
        $columns = array(
            SAISON_ID, 
            SAISON_LIBELLE
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".SAISON_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            SAISON_ID, 
            SAISON_LIBELLE
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {}
	
	public function update($object) {}
	
	public function delete($id) {}
}

?>