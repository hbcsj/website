<?php
require_once("core/php/lib/abstract-dao.php");

define(RESSOURCES_EXTERNES_TABLE_NAME, "hbcsj_ressources_externes");
define(RESSOURCES_EXTERNES_ID, "id");
define(RESSOURCES_EXTERNES_FACEBOOK_URL, "facebook_url");
define(RESSOURCES_EXTERNES_FACEBOOK_ID, "facebook_id");
define(RESSOURCES_EXTERNES_INSTAGRAM_URL, "instagram_url");
define(RESSOURCES_EXTERNES_INSTAGRAM_ID, "instagram_id");
define(RESSOURCES_EXTERNES_TWITTER_URL, "twitter_url");
define(RESSOURCES_EXTERNES_TWITTER_ID, "twitter_id");
define(RESSOURCES_EXTERNES_YOUTUBE_URL, "youtube_url");
define(RESSOURCES_EXTERNES_YOUTUBE_ID, "youtube_id");
define(RESSOURCES_EXTERNES_GOOGLE_MAP_URL, "google_map_url");

class RessourcesExternesDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(RESSOURCES_EXTERNES_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {}

    public function getSingleton() {
        $singleton = null;

        $list = $this->getAll();
        if (sizeof($list) > 0) {
            $singleton = $list[0];
        }

        return $singleton;
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".RESSOURCES_EXTERNES_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            RESSOURCES_EXTERNES_ID, 
            RESSOURCES_EXTERNES_FACEBOOK_URL, 
            RESSOURCES_EXTERNES_FACEBOOK_ID, 
            RESSOURCES_EXTERNES_INSTAGRAM_URL, 
            RESSOURCES_EXTERNES_INSTAGRAM_ID,
            RESSOURCES_EXTERNES_TWITTER_URL,
            RESSOURCES_EXTERNES_TWITTER_ID,
            RESSOURCES_EXTERNES_YOUTUBE_URL,
            RESSOURCES_EXTERNES_YOUTUBE_ID,
            RESSOURCES_EXTERNES_GOOGLE_MAP_URL
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {}
	
	public function update($object) {}
	
	public function delete($id) {}
}

?>