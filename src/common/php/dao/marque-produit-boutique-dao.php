<?php
require_once("core/php/lib/abstract-dao.php");

define(MARQUE_PRODUIT_BOUTIQUE_TABLE_NAME, "hbcsj_marque_produit_boutique");
define(MARQUE_PRODUIT_BOUTIQUE_ID, "id");
define(MARQUE_PRODUIT_BOUTIQUE_NOM, "nom");

class MarqueProduitBoutiqueDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(MARQUE_PRODUIT_BOUTIQUE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".MARQUE_PRODUIT_BOUTIQUE_TABLE_NAME." WHERE ".MARQUE_PRODUIT_BOUTIQUE_ID." = :".MARQUE_PRODUIT_BOUTIQUE_ID;
        $params = array(
            MARQUE_PRODUIT_BOUTIQUE_ID => $id
        );
        $columns = array(
            MARQUE_PRODUIT_BOUTIQUE_ID, 
            MARQUE_PRODUIT_BOUTIQUE_NOM
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".MARQUE_PRODUIT_BOUTIQUE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            MARQUE_PRODUIT_BOUTIQUE_ID, 
            MARQUE_PRODUIT_BOUTIQUE_NOM
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {}
	
	public function update($object) {}
	
	public function delete($id) {}
}

?>