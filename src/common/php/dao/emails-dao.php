<?php
require_once("core/php/lib/abstract-dao.php");

define(EMAILS_TABLE_NAME, "hbcsj_emails");
define(EMAILS_ID, "id");
define(EMAILS_NOREPLY, "noreply");
define(EMAILS_BOUTIQUE, "boutique");
define(EMAILS_BILLETTERIE, "billetterie");
define(EMAILS_GAMES_ADMIN, "games_admin");

class EmailsDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(EMAILS_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {}

    public function getSingleton() {
        $singleton = null;

        $list = $this->getAll();
        if (sizeof($list) > 0) {
            $singleton = $list[0];
        }

        return $singleton;
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".EMAILS_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            EMAILS_ID, 
            EMAILS_NOREPLY, 
            EMAILS_BOUTIQUE, 
            EMAILS_BILLETTERIE, 
            EMAILS_GAMES_ADMIN
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {}
	
	public function update($object) {}
	
	public function delete($id) {}
}

?>