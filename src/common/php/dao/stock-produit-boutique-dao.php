<?php
require_once("core/php/lib/abstract-dao.php");

define(STOCK_PRODUIT_BOUTIQUE_TABLE_NAME, "hbcsj_stock_produit_boutique");
define(STOCK_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID, "produit_boutique_id");
define(STOCK_PRODUIT_BOUTIQUE_TAILLE, "taille");
define(STOCK_PRODUIT_BOUTIQUE_QUANTITE, "quantite");

define(STOCK_PRODUIT_BOUTIQUE_STOCK_TOTAL, "nb_produits");

define(STOCK_PRODUIT_BOUTIQUE_TAILLE_UNIQUE, "Taille unique");

class StockProduitBoutiqueDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(STOCK_PRODUIT_BOUTIQUE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".STOCK_PRODUIT_BOUTIQUE_TABLE_NAME." WHERE ".STOCK_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID." = :".STOCK_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID." AND ".STOCK_PRODUIT_BOUTIQUE_TAILLE." = :".STOCK_PRODUIT_BOUTIQUE_TAILLE;
        $params = array(
            STOCK_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID => $id[STOCK_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID],
            STOCK_PRODUIT_BOUTIQUE_TAILLE => $id[STOCK_PRODUIT_BOUTIQUE_TAILLE]
        );
        $columns = array(
            STOCK_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID, 
            STOCK_PRODUIT_BOUTIQUE_TAILLE,
            STOCK_PRODUIT_BOUTIQUE_QUANTITE
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".STOCK_PRODUIT_BOUTIQUE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            STOCK_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID, 
            STOCK_PRODUIT_BOUTIQUE_TAILLE,
            STOCK_PRODUIT_BOUTIQUE_QUANTITE
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function getStockTotal() {
        $query = "SELECT SUM(".STOCK_PRODUIT_BOUTIQUE_QUANTITE.") AS ".STOCK_PRODUIT_BOUTIQUE_STOCK_TOTAL." ";
        $query .= "FROM ".STOCK_PRODUIT_BOUTIQUE_TABLE_NAME;
        $columns = array(
            STOCK_PRODUIT_BOUTIQUE_STOCK_TOTAL
        );
        return $this->executeGetUniqueRequest($query, null, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".STOCK_PRODUIT_BOUTIQUE_TABLE_NAME." (";
        $query .= STOCK_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID.", ";
        $query .= STOCK_PRODUIT_BOUTIQUE_TAILLE.", ";
        $query .= STOCK_PRODUIT_BOUTIQUE_QUANTITE.") VALUES (";
        $query .= ":".STOCK_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID.", ";
        $query .= ":".STOCK_PRODUIT_BOUTIQUE_TAILLE.", ";
        $query .= ":".STOCK_PRODUIT_BOUTIQUE_QUANTITE.")";
        $params = array(
            STOCK_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID => $object[STOCK_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID],
            STOCK_PRODUIT_BOUTIQUE_TAILLE => $object[STOCK_PRODUIT_BOUTIQUE_TAILLE],
            STOCK_PRODUIT_BOUTIQUE_QUANTITE => $object[STOCK_PRODUIT_BOUTIQUE_QUANTITE]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {
        $query = "UPDATE ".STOCK_PRODUIT_BOUTIQUE_TABLE_NAME." SET ";
        $query .= STOCK_PRODUIT_BOUTIQUE_QUANTITE." = :".STOCK_PRODUIT_BOUTIQUE_QUANTITE." WHERE ";
        $query .= STOCK_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID." = :".STOCK_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID." AND ";
        $query .= STOCK_PRODUIT_BOUTIQUE_TAILLE." = :".STOCK_PRODUIT_BOUTIQUE_TAILLE;
        $params = array(
            STOCK_PRODUIT_BOUTIQUE_QUANTITE => $object[STOCK_PRODUIT_BOUTIQUE_QUANTITE],
            STOCK_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID => $object[STOCK_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID],
            STOCK_PRODUIT_BOUTIQUE_TAILLE => $object[STOCK_PRODUIT_BOUTIQUE_TAILLE]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".STOCK_PRODUIT_BOUTIQUE_TABLE_NAME." WHERE ".STOCK_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID." = :".STOCK_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID." AND ".STOCK_PRODUIT_BOUTIQUE_TAILLE." = :".STOCK_PRODUIT_BOUTIQUE_TAILLE;
        $params = array(
            STOCK_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID => $id[STOCK_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID],
            STOCK_PRODUIT_BOUTIQUE_TAILLE => $id[STOCK_PRODUIT_BOUTIQUE_TAILLE]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>