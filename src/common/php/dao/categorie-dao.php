<?php
require_once("core/php/lib/abstract-dao.php");

define(CATEGORIE_TABLE_NAME, "hbcsj_categorie");
define(CATEGORIE_ID, "id");
define(CATEGORIE_NOM, "nom");
define(CATEGORIE_ABREVIATION, "abreviation");
define(CATEGORIE_NB_EQUIPES, "nb_equipes");
define(CATEGORIE_REGROUPEMENT_MATCHS, "regroupement_matchs");
define(CATEGORIE_POSITION_AFFICHAGE, "position_affichage");

define(CATEGORIE_NB_CATEGORIES, "nb_categories");
define(CATEGORIE_NB_EQUIPES_INSCRITES, "nb_equipes_inscrites");

class CategorieDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(CATEGORIE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".CATEGORIE_TABLE_NAME." WHERE ".CATEGORIE_ID." = :".CATEGORIE_ID;
        $params = array(
            CATEGORIE_ID => $id
        );
        $columns = array(
            CATEGORIE_ID, 
            CATEGORIE_NOM, 
            CATEGORIE_ABREVIATION, 
            CATEGORIE_NB_EQUIPES, 
            CATEGORIE_REGROUPEMENT_MATCHS, 
            CATEGORIE_POSITION_AFFICHAGE
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".CATEGORIE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            CATEGORIE_ID, 
            CATEGORIE_NOM, 
            CATEGORIE_ABREVIATION, 
            CATEGORIE_NB_EQUIPES, 
            CATEGORIE_REGROUPEMENT_MATCHS, 
            CATEGORIE_POSITION_AFFICHAGE
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function getAvecEquipesInscrites($orderBy = null) {
        $query = "SELECT * FROM ".CATEGORIE_TABLE_NAME." WHERE ".CATEGORIE_NB_EQUIPES." > 0";
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            CATEGORIE_ID, 
            CATEGORIE_NOM, 
            CATEGORIE_ABREVIATION, 
            CATEGORIE_NB_EQUIPES, 
            CATEGORIE_REGROUPEMENT_MATCHS, 
            CATEGORIE_POSITION_AFFICHAGE
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function getNbAvecEquipesInscrites() {
        $query = "SELECT COUNT(DISTINCT ".CATEGORIE_TABLE_NAME.".".CATEGORIE_ID.") AS ".CATEGORIE_NB_CATEGORIES." ";
        $query .= "FROM ".CATEGORIE_TABLE_NAME." ";
        $query .= "WHERE ".CATEGORIE_NB_EQUIPES." > 0";
        $columns = array(
            CATEGORIE_NB_CATEGORIES
        );
        return $this->executeGetUniqueRequest($query, null, $columns);
    }

    public function getNbEquipesInscrites() {
        $query = "SELECT SUM(".CATEGORIE_TABLE_NAME.".".CATEGORIE_NB_EQUIPES.") AS ".CATEGORIE_NB_EQUIPES_INSCRITES." ";
        $query .= "FROM ".CATEGORIE_TABLE_NAME;
        $columns = array(
            CATEGORIE_NB_EQUIPES_INSCRITES
        );
        return $this->executeGetUniqueRequest($query, null, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".CATEGORIE_TABLE_NAME." (";
        $query .= CATEGORIE_NOM.", ";
        $query .= CATEGORIE_ABREVIATION.", ";
        $query .= CATEGORIE_NB_EQUIPES.", ";
        $query .= CATEGORIE_REGROUPEMENT_MATCHS.") VALUES (";
        $query .= ":".CATEGORIE_NOM.", ";
        $query .= ":".CATEGORIE_ABREVIATION.", ";
        $query .= ":".CATEGORIE_NB_EQUIPES.", ";
        $query .= ":".CATEGORIE_REGROUPEMENT_MATCHS.", ";
        $query .= ":".CATEGORIE_POSITION_AFFICHAGE.")";
        $params = array(
            CATEGORIE_NOM => $object[CATEGORIE_NOM],
            CATEGORIE_ABREVIATION => $object[CATEGORIE_ABREVIATION],
            CATEGORIE_NB_EQUIPES => $object[CATEGORIE_NB_EQUIPES],
            CATEGORIE_REGROUPEMENT_MATCHS => $object[CATEGORIE_REGROUPEMENT_MATCHS],
            CATEGORIE_POSITION_AFFICHAGE => $object[CATEGORIE_POSITION_AFFICHAGE]
        );
        return $this->executeCreateRequest($query, $params);}
	
	public function update($object) {
        $query = "UPDATE ".CATEGORIE_TABLE_NAME." SET ";
        $query .= CATEGORIE_NOM." = :".CATEGORIE_NOM.", ";
        $query .= CATEGORIE_ABREVIATION." = :".CATEGORIE_ABREVIATION.", ";
        $query .= CATEGORIE_NB_EQUIPES." = :".CATEGORIE_NB_EQUIPES.", ";
        $query .= CATEGORIE_REGROUPEMENT_MATCHS." = :".CATEGORIE_REGROUPEMENT_MATCHS.", ";
        $query .= CATEGORIE_POSITION_AFFICHAGE." = :".CATEGORIE_POSITION_AFFICHAGE." WHERE ";
        $query .= CATEGORIE_ID." = :".CATEGORIE_ID;
        $params = array(
            CATEGORIE_NOM => $object[CATEGORIE_NOM],
            CATEGORIE_ABREVIATION => $object[CATEGORIE_ABREVIATION],
            CATEGORIE_NB_EQUIPES => $object[CATEGORIE_NB_EQUIPES],
            CATEGORIE_REGROUPEMENT_MATCHS => $object[CATEGORIE_REGROUPEMENT_MATCHS],
            CATEGORIE_POSITION_AFFICHAGE => $object[CATEGORIE_POSITION_AFFICHAGE],
            CATEGORIE_ID => $object[CATEGORIE_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".CATEGORIE_TABLE_NAME." WHERE ".CATEGORIE_ID." = :".CATEGORIE_ID;
        $params = array(
            CATEGORIE_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>