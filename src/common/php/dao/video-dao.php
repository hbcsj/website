<?php
require_once("core/php/lib/abstract-dao.php");

define(VIDEO_TABLE_NAME, "hbcsj_video");
define(VIDEO_ID, "id");
define(VIDEO_NOM, "nom");
define(VIDEO_URL, "url");
define(VIDEO_VISIBLE_SUR_SITE, "visible_sur_site");
define(VIDEO_EMAIL_DERNIER_EDITEUR, "email_dernier_editeur");

define(VIDEO_NB_VIDEOS, "nb_videos");

class VideoDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(VIDEO_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".VIDEO_TABLE_NAME." WHERE ".VIDEO_ID." = :".VIDEO_ID;
        $params = array(
            VIDEO_ID => $id
        );
        $columns = array(
            VIDEO_ID, 
            VIDEO_NOM, 
            VIDEO_URL,
            VIDEO_VISIBLE_SUR_SITE,
            VIDEO_EMAIL_DERNIER_EDITEUR
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".VIDEO_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            VIDEO_ID, 
            VIDEO_NOM, 
            VIDEO_URL,
            VIDEO_VISIBLE_SUR_SITE,
            VIDEO_EMAIL_DERNIER_EDITEUR
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function getNbTotalVisibles() {
        $query = "SELECT COUNT(DISTINCT ".VIDEO_TABLE_NAME.".".VIDEO_ID.") AS ".VIDEO_NB_VIDEOS." ";
        $query .= "FROM ".VIDEO_TABLE_NAME;
        $columns = array(
            VIDEO_NB_VIDEOS
        );
        return $this->executeGetUniqueRequest($query, null, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".VIDEO_TABLE_NAME." (";
        $query .= VIDEO_NOM.", ";
        $query .= VIDEO_URL.", ";
        $query .= VIDEO_VISIBLE_SUR_SITE.", ";
        $query .= VIDEO_EMAIL_DERNIER_EDITEUR.") VALUES (";
        $query .= ":".VIDEO_NOM.", ";
        $query .= ":".VIDEO_URL.", ";
        $query .= ":".VIDEO_VISIBLE_SUR_SITE.", ";
        $query .= ":".VIDEO_EMAIL_DERNIER_EDITEUR.")";
        $params = array(
            VIDEO_NOM => $object[VIDEO_NOM],
            VIDEO_URL => $object[VIDEO_URL],
            VIDEO_VISIBLE_SUR_SITE => $object[VIDEO_VISIBLE_SUR_SITE],
            VIDEO_EMAIL_DERNIER_EDITEUR => $object[VIDEO_EMAIL_DERNIER_EDITEUR]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {
        $query = "UPDATE ".VIDEO_TABLE_NAME." SET ";
        $query .= VIDEO_NOM." = :".VIDEO_NOM.", ";
        $query .= VIDEO_URL." = :".VIDEO_URL.", ";
        $query .= VIDEO_VISIBLE_SUR_SITE." = :".VIDEO_VISIBLE_SUR_SITE.", ";
        $query .= VIDEO_EMAIL_DERNIER_EDITEUR." = :".VIDEO_EMAIL_DERNIER_EDITEUR." WHERE ";
        $query .= VIDEO_ID." = :".VIDEO_ID;
        $params = array(
            VIDEO_NOM => $object[VIDEO_NOM],
            VIDEO_URL => $object[VIDEO_URL],
            VIDEO_VISIBLE_SUR_SITE => $object[VIDEO_VISIBLE_SUR_SITE],
            VIDEO_EMAIL_DERNIER_EDITEUR => $object[VIDEO_EMAIL_DERNIER_EDITEUR],
            VIDEO_ID => $object[VIDEO_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".VIDEO_TABLE_NAME." WHERE ".VIDEO_ID." = :".VIDEO_ID;
        $params = array(
            VIDEO_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>