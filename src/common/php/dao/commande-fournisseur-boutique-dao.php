<?php
require_once("core/php/lib/abstract-dao.php");

define(COMMANDE_FOURNISSEUR_BOUTIQUE_TABLE_NAME, "hbcsj_commande_fournisseur_boutique");
define(COMMANDE_FOURNISSEUR_BOUTIQUE_ID, "id");
define(COMMANDE_FOURNISSEUR_BOUTIQUE_DATE, "date");
define(COMMANDE_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID, "fournisseur_produit_boutique_id");

class CommandeFournisseurBoutiqueDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(COMMANDE_FOURNISSEUR_BOUTIQUE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".COMMANDE_FOURNISSEUR_BOUTIQUE_TABLE_NAME." WHERE ".COMMANDE_FOURNISSEUR_BOUTIQUE_ID." = :".COMMANDE_FOURNISSEUR_BOUTIQUE_ID;
        $params = array(
            COMMANDE_FOURNISSEUR_BOUTIQUE_ID => $id
        );
        $columns = array(
            COMMANDE_FOURNISSEUR_BOUTIQUE_ID, 
            COMMANDE_FOURNISSEUR_BOUTIQUE_DATE, 
            COMMANDE_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".COMMANDE_FOURNISSEUR_BOUTIQUE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            COMMANDE_FOURNISSEUR_BOUTIQUE_ID, 
            COMMANDE_FOURNISSEUR_BOUTIQUE_DATE, 
            COMMANDE_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID
        );
        return $this->executeGetRequest($query, null, $columns);
    }

    public function getByFournisseurProduitBoutiqueId($fournisseurProduitBoutiqueId) {
        $query = "SELECT * FROM ".COMMANDE_FOURNISSEUR_BOUTIQUE_TABLE_NAME." WHERE ".COMMANDE_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID." = :".COMMANDE_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID;
        $params = array(
            COMMANDE_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID => $fournisseurProduitBoutiqueId
        );
        $columns = array(
            COMMANDE_FOURNISSEUR_BOUTIQUE_ID, 
            COMMANDE_FOURNISSEUR_BOUTIQUE_DATE, 
            COMMANDE_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".COMMANDE_FOURNISSEUR_BOUTIQUE_TABLE_NAME." (";
        $query .= COMMANDE_FOURNISSEUR_BOUTIQUE_DATE.", ";
        $query .= COMMANDE_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID.") VALUES (";
        $query .= ":".COMMANDE_FOURNISSEUR_BOUTIQUE_DATE.", ";
        $query .= ":".COMMANDE_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID.")";
        $params = array(
            COMMANDE_FOURNISSEUR_BOUTIQUE_DATE => $object[COMMANDE_FOURNISSEUR_BOUTIQUE_DATE],
            COMMANDE_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID => $object[COMMANDE_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {
        $query = "UPDATE ".COMMANDE_FOURNISSEUR_BOUTIQUE_TABLE_NAME." SET ";
        $query .= COMMANDE_FOURNISSEUR_BOUTIQUE_DATE." = :".COMMANDE_FOURNISSEUR_BOUTIQUE_DATE.", ";
        $query .= COMMANDE_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID." = :".COMMANDE_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID." WHERE ";
        $query .= COMMANDE_FOURNISSEUR_BOUTIQUE_ID." = :".COMMANDE_FOURNISSEUR_BOUTIQUE_ID;
        $params = array(
            COMMANDE_FOURNISSEUR_BOUTIQUE_DATE => $object[COMMANDE_FOURNISSEUR_BOUTIQUE_DATE],
            COMMANDE_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID => $object[COMMANDE_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID],
            COMMANDE_FOURNISSEUR_BOUTIQUE_ID => $object[COMMANDE_FOURNISSEUR_BOUTIQUE_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".COMMANDE_FOURNISSEUR_BOUTIQUE_TABLE_NAME." WHERE ".COMMANDE_FOURNISSEUR_BOUTIQUE_ID." = :".COMMANDE_FOURNISSEUR_BOUTIQUE_ID;
        $params = array(
            COMMANDE_FOURNISSEUR_BOUTIQUE_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>