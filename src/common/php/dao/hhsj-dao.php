<?php
require_once("core/php/lib/abstract-dao.php");

define(HHSJ_TABLE_NAME, "hbcsj_hhsj");
define(HHSJ_ID, "id");
define(HHSJ_DATE, "date");
define(HHSJ_TITRE, "titre");
define(HHSJ_VISIBLE_SUR_SITE, "visible_sur_site");
define(HHSJ_EMAIL_DERNIER_EDITEUR, "email_dernier_editeur");
define(HHSJ_TYPE_HHSJ_ID, "type_hhsj_id");

define(HHSJ_DATE_DEBUT, "date_debut");
define(HHSJ_DATE_FIN, "date_fin");
define(HHSJ_NB_HHSJ, "nb_hhsj");

define(HHSJ_UNE_DE_MAGAZINE_TYPE_ID, "1");
define(HHSJ_MAGAZINE_COMPLET_TYPE_ID, "2");
define(HHSJ_PETIT_SAINT_JEANNAIS_TYPE_ID, "3");

class HHSJDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(HHSJ_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".HHSJ_TABLE_NAME." WHERE ".HHSJ_ID." = :".HHSJ_ID;
        $params = array(
            HHSJ_ID => $id
        );
        $columns = array(
            HHSJ_ID, 
            HHSJ_DATE, 
            HHSJ_TITRE,
            HHSJ_VISIBLE_SUR_SITE,
            HHSJ_EMAIL_DERNIER_EDITEUR,
            HHSJ_TYPE_HHSJ_ID
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".HHSJ_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            HHSJ_ID, 
            HHSJ_DATE, 
            HHSJ_TITRE,
            HHSJ_VISIBLE_SUR_SITE,
            HHSJ_EMAIL_DERNIER_EDITEUR,
            HHSJ_TYPE_HHSJ_ID
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function getAllByParams($dateDebut, $dateFin, $titre, $typeHHSJId, $orderBy = null) {
        $query = "SELECT * FROM ".HHSJ_TABLE_NAME;
        
        $queryParamsArray = array();
        $params = array();
        if ($dateDebut != null && $dateDebut != "") {
            $queryParamsArray[] = HHSJ_DATE." >= :".HHSJ_DATE_DEBUT;
            $params[HHSJ_DATE_DEBUT] = $dateDebut;
        }
        if ($dateFin != null && $dateFin != "") {
            $queryParamsArray[] = HHSJ_DATE." <= :".HHSJ_DATE_FIN;
            $params[HHSJ_DATE_FIN] = $dateFin;
        }
        if ($titre != null && $titre != "") {
            $queryParamsArray[] = HHSJ_TITRE." LIKE :".HHSJ_TITRE;
            $params[HHSJ_TITRE] = "%".$titre."%";
        }
        if ($typeHHSJId != null && $typeHHSJId != "") {
            $queryParamsArray[] = HHSJ_TYPE_HHSJ_ID." = :".HHSJ_TYPE_HHSJ_ID;
            $params[HHSJ_TYPE_HHSJ_ID] = $typeHHSJId;
        }
        if (sizeof($queryParamsArray) > 0) {
            $query .= " WHERE ";
            $query .= implode(" AND ", $queryParamsArray);
        }
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }

        $columns = array(
            HHSJ_ID, 
            HHSJ_DATE, 
            HHSJ_TITRE,
            HHSJ_VISIBLE_SUR_SITE,
            HHSJ_EMAIL_DERNIER_EDITEUR,
            HHSJ_TYPE_HHSJ_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function getVisibles($orderBy = null) {
        $query = "SELECT * FROM ".HHSJ_TABLE_NAME." WHERE ".HHSJ_VISIBLE_SUR_SITE." = 1";
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            HHSJ_ID, 
            HHSJ_DATE, 
            HHSJ_TITRE,
            HHSJ_VISIBLE_SUR_SITE,
            HHSJ_EMAIL_DERNIER_EDITEUR,
            HHSJ_TYPE_HHSJ_ID
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function getVisiblesApresDate($date, $orderBy = null) {
        $query = "SELECT * FROM ".HHSJ_TABLE_NAME." WHERE ".HHSJ_VISIBLE_SUR_SITE." = 1 AND ".HHSJ_DATE." >= :".HHSJ_DATE;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $params = array(
            ARTICLE_PRESSE_DATE => $date
        );
        $columns = array(
            HHSJ_ID, 
            HHSJ_DATE, 
            HHSJ_TITRE,
            HHSJ_VISIBLE_SUR_SITE,
            HHSJ_EMAIL_DERNIER_EDITEUR,
            HHSJ_TYPE_HHSJ_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function getNbTotalVisibles() {
        $query = "SELECT COUNT(DISTINCT ".HHSJ_TABLE_NAME.".".HHSJ_ID.") AS ".HHSJ_NB_HHSJ." ";
        $query .= "FROM ".HHSJ_TABLE_NAME;
        $columns = array(
            HHSJ_NB_HHSJ
        );
        return $this->executeGetUniqueRequest($query, null, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".HHSJ_TABLE_NAME." (";
        $query .= HHSJ_DATE.", ";
        $query .= HHSJ_TITRE.", ";
        $query .= HHSJ_VISIBLE_SUR_SITE.", ";
        $query .= HHSJ_EMAIL_DERNIER_EDITEUR.", ";
        $query .= HHSJ_TYPE_HHSJ_ID.") VALUES (";
        $query .= ":".HHSJ_DATE.", ";
        $query .= ":".HHSJ_TITRE.", ";
        $query .= ":".HHSJ_VISIBLE_SUR_SITE.", ";
        $query .= ":".HHSJ_EMAIL_DERNIER_EDITEUR.", ";
        $query .= ":".HHSJ_TYPE_HHSJ_ID.")";
        $params = array(
            HHSJ_DATE => $object[HHSJ_DATE],
            HHSJ_TITRE => $object[HHSJ_TITRE],
            HHSJ_VISIBLE_SUR_SITE => $object[HHSJ_VISIBLE_SUR_SITE],
            HHSJ_EMAIL_DERNIER_EDITEUR => $object[HHSJ_EMAIL_DERNIER_EDITEUR],
            HHSJ_TYPE_HHSJ_ID => $object[HHSJ_TYPE_HHSJ_ID]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {
        $query = "UPDATE ".HHSJ_TABLE_NAME." SET ";
        $query .= HHSJ_DATE." = :".HHSJ_DATE.", ";
        $query .= HHSJ_TITRE." = :".HHSJ_TITRE.", ";
        $query .= HHSJ_VISIBLE_SUR_SITE." = :".HHSJ_VISIBLE_SUR_SITE.", ";
        $query .= HHSJ_EMAIL_DERNIER_EDITEUR." = :".HHSJ_EMAIL_DERNIER_EDITEUR.", ";
        $query .= HHSJ_TYPE_HHSJ_ID." = :".HHSJ_TYPE_HHSJ_ID." WHERE ";
        $query .= HHSJ_ID." = :".HHSJ_ID;
        $params = array(
            HHSJ_DATE => $object[HHSJ_DATE],
            HHSJ_TITRE => $object[HHSJ_TITRE],
            HHSJ_VISIBLE_SUR_SITE => $object[HHSJ_VISIBLE_SUR_SITE],
            HHSJ_EMAIL_DERNIER_EDITEUR => $object[HHSJ_EMAIL_DERNIER_EDITEUR],
            HHSJ_TYPE_HHSJ_ID => $object[HHSJ_TYPE_HHSJ_ID],
            HHSJ_ID => $object[HHSJ_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".HHSJ_TABLE_NAME." WHERE ".HHSJ_ID." = :".HHSJ_ID;
        $params = array(
            HHSJ_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>