<?php
require_once("core/php/lib/abstract-dao.php");

define(ACTUALITE_TABLE_NAME, "hbcsj_actualite");
define(ACTUALITE_ID, "id");
define(ACTUALITE_DATE_HEURE, "date_heure");
define(ACTUALITE_TITRE, "titre");
define(ACTUALITE_VISIBLE_SUR_SITE, "visible_sur_site");
define(ACTUALITE_EMAIL_DERNIER_EDITEUR, "email_dernier_editeur");

define(ACTUALITE_DATE_HEURE_DEBUT, "date_heure_debut");
define(ACTUALITE_DATE_HEURE_FIN, "date_heure_fin");
define(ACTUALITE_NB_NEWS, "nb_news");

class ActualiteDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(ACTUALITE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".ACTUALITE_TABLE_NAME." WHERE ".ACTUALITE_ID." = :".ACTUALITE_ID;
        $params = array(
            ACTUALITE_ID => $id
        );
        $columns = array(
            ACTUALITE_ID, 
            ACTUALITE_DATE_HEURE, 
            ACTUALITE_TITRE,
            ACTUALITE_VISIBLE_SUR_SITE,
            ACTUALITE_EMAIL_DERNIER_EDITEUR
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".ACTUALITE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            ACTUALITE_ID, 
            ACTUALITE_DATE_HEURE, 
            ACTUALITE_TITRE,
            ACTUALITE_VISIBLE_SUR_SITE,
            ACTUALITE_EMAIL_DERNIER_EDITEUR
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function getAllByParams($dateDebut, $dateFin, $titre, $orderBy = null) {
        $query = "SELECT * FROM ".ACTUALITE_TABLE_NAME;
        
        $queryParamsArray = array();
        $params = array();
        if ($dateDebut != null && $dateDebut != "") {
            $queryParamsArray[] = ACTUALITE_DATE_HEURE." >= :".ACTUALITE_DATE_HEURE_DEBUT;
            $params[ACTUALITE_DATE_HEURE_DEBUT] = $dateDebut;
        }
        if ($dateFin != null && $dateFin != "") {
            $queryParamsArray[] = ACTUALITE_DATE_HEURE." <= :".ACTUALITE_DATE_HEURE_FIN;
            $params[ACTUALITE_DATE_HEURE_FIN] = $dateFin;
        }
        if ($titre != null && $titre != "") {
            $queryParamsArray[] = ACTUALITE_TITRE." LIKE :".ACTUALITE_TITRE;
            $params[ACTUALITE_TITRE] = "%".$titre."%";
        }
        if (sizeof($queryParamsArray) > 0) {
            $query .= " WHERE ";
            $query .= implode(" AND ", $queryParamsArray);
        }
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }

        $columns = array(
            ACTUALITE_ID, 
            ACTUALITE_DATE_HEURE, 
            ACTUALITE_TITRE,
            ACTUALITE_VISIBLE_SUR_SITE,
            ACTUALITE_EMAIL_DERNIER_EDITEUR
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function getVisibles($orderBy = null) {
        $query = "SELECT * FROM ".ACTUALITE_TABLE_NAME." WHERE ".ACTUALITE_VISIBLE_SUR_SITE." = 1";
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            ACTUALITE_ID, 
            ACTUALITE_DATE_HEURE, 
            ACTUALITE_TITRE,
            ACTUALITE_VISIBLE_SUR_SITE,
            ACTUALITE_EMAIL_DERNIER_EDITEUR
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function getVisiblesLimited($limit, $orderBy = null) {
        $query = "SELECT * FROM ".ACTUALITE_TABLE_NAME." WHERE ".ACTUALITE_VISIBLE_SUR_SITE." = 1";
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $query .= " LIMIT ".$limit;
        $columns = array(
            ACTUALITE_ID, 
            ACTUALITE_DATE_HEURE, 
            ACTUALITE_TITRE,
            ACTUALITE_VISIBLE_SUR_SITE,
            ACTUALITE_EMAIL_DERNIER_EDITEUR
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function getVisiblesApresDate($date, $orderBy = null) {
        $query = "SELECT * FROM ".ACTUALITE_TABLE_NAME." WHERE ".ACTUALITE_VISIBLE_SUR_SITE." = 1 AND ".ACTUALITE_DATE_HEURE." >= :".ACTUALITE_DATE_HEURE;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $params = array(
            ACTUALITE_DATE_HEURE => $date
        );
        $columns = array(
            ACTUALITE_ID, 
            ACTUALITE_DATE_HEURE, 
            ACTUALITE_TITRE,
            ACTUALITE_VISIBLE_SUR_SITE,
            ACTUALITE_EMAIL_DERNIER_EDITEUR
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function getNbTotalVisibles() {
        $query = "SELECT COUNT(DISTINCT ".ACTUALITE_TABLE_NAME.".".ACTUALITE_ID.") AS ".ACTUALITE_NB_NEWS." ";
        $query .= "FROM ".ACTUALITE_TABLE_NAME;
        $columns = array(
            ACTUALITE_NB_NEWS
        );
        return $this->executeGetUniqueRequest($query, null, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".ACTUALITE_TABLE_NAME." (";
        $query .= ACTUALITE_DATE_HEURE.", ";
        $query .= ACTUALITE_TITRE.", ";
        $query .= ACTUALITE_VISIBLE_SUR_SITE.", ";
        $query .= ACTUALITE_EMAIL_DERNIER_EDITEUR.") VALUES (";
        $query .= ":".ACTUALITE_DATE_HEURE.", ";
        $query .= ":".ACTUALITE_TITRE.", ";
        $query .= ":".ACTUALITE_VISIBLE_SUR_SITE.", ";
        $query .= ":".ACTUALITE_EMAIL_DERNIER_EDITEUR.")";
        $params = array(
            ACTUALITE_DATE_HEURE => $object[ACTUALITE_DATE_HEURE],
            ACTUALITE_TITRE => $object[ACTUALITE_TITRE],
            ACTUALITE_VISIBLE_SUR_SITE => $object[ACTUALITE_VISIBLE_SUR_SITE],
            ACTUALITE_EMAIL_DERNIER_EDITEUR => $object[ACTUALITE_EMAIL_DERNIER_EDITEUR]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {
        $query = "UPDATE ".ACTUALITE_TABLE_NAME." SET ";
        $query .= ACTUALITE_DATE_HEURE." = :".ACTUALITE_DATE_HEURE.", ";
        $query .= ACTUALITE_TITRE." = :".ACTUALITE_TITRE.", ";
        $query .= ACTUALITE_VISIBLE_SUR_SITE." = :".ACTUALITE_VISIBLE_SUR_SITE.", ";
        $query .= ACTUALITE_EMAIL_DERNIER_EDITEUR." = :".ACTUALITE_EMAIL_DERNIER_EDITEUR." WHERE ";
        $query .= ACTUALITE_ID." = :".ACTUALITE_ID;
        $params = array(
            ACTUALITE_DATE_HEURE => $object[ACTUALITE_DATE_HEURE],
            ACTUALITE_TITRE => $object[ACTUALITE_TITRE],
            ACTUALITE_VISIBLE_SUR_SITE => $object[ACTUALITE_VISIBLE_SUR_SITE],
            ACTUALITE_EMAIL_DERNIER_EDITEUR => $object[ACTUALITE_EMAIL_DERNIER_EDITEUR],
            ACTUALITE_ID => $object[ACTUALITE_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".ACTUALITE_TABLE_NAME." WHERE ".ACTUALITE_ID." = :".ACTUALITE_ID;
        $params = array(
            ACTUALITE_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>