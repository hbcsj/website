<?php
require_once("core/php/lib/abstract-dao.php");

define(FOURNISSEUR_PRODUIT_BOUTIQUE_TABLE_NAME, "hbcsj_fournisseur_produit_boutique");
define(FOURNISSEUR_PRODUIT_BOUTIQUE_ID, "id");
define(FOURNISSEUR_PRODUIT_BOUTIQUE_NOM, "nom");

class FournisseurProduitBoutiqueDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(FOURNISSEUR_PRODUIT_BOUTIQUE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".FOURNISSEUR_PRODUIT_BOUTIQUE_TABLE_NAME." WHERE ".FOURNISSEUR_PRODUIT_BOUTIQUE_ID." = :".FOURNISSEUR_PRODUIT_BOUTIQUE_ID;
        $params = array(
            FOURNISSEUR_PRODUIT_BOUTIQUE_ID => $id
        );
        $columns = array(
            FOURNISSEUR_PRODUIT_BOUTIQUE_ID, 
            FOURNISSEUR_PRODUIT_BOUTIQUE_NOM
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".FOURNISSEUR_PRODUIT_BOUTIQUE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            FOURNISSEUR_PRODUIT_BOUTIQUE_ID, 
            FOURNISSEUR_PRODUIT_BOUTIQUE_NOM
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".FOURNISSEUR_PRODUIT_BOUTIQUE_TABLE_NAME." (";
        $query .= FOURNISSEUR_PRODUIT_BOUTIQUE_NOM.") VALUES (";
        $query .= ":".FOURNISSEUR_PRODUIT_BOUTIQUE_NOM.")";
        $params = array(
            FOURNISSEUR_PRODUIT_BOUTIQUE_NOM => $object[FOURNISSEUR_PRODUIT_BOUTIQUE_NOM]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {
        $query = "UPDATE ".FOURNISSEUR_PRODUIT_BOUTIQUE_TABLE_NAME." SET ";
        $query .= FOURNISSEUR_PRODUIT_BOUTIQUE_NOM." = :".FOURNISSEUR_PRODUIT_BOUTIQUE_NOM." WHERE ";
        $query .= FOURNISSEUR_PRODUIT_BOUTIQUE_ID." = :".FOURNISSEUR_PRODUIT_BOUTIQUE_ID;
        $params = array(
            FOURNISSEUR_PRODUIT_BOUTIQUE_NOM => $object[FOURNISSEUR_PRODUIT_BOUTIQUE_NOM],
            FOURNISSEUR_PRODUIT_BOUTIQUE_ID => $object[FOURNISSEUR_PRODUIT_BOUTIQUE_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".FOURNISSEUR_PRODUIT_BOUTIQUE_TABLE_NAME." WHERE ".FOURNISSEUR_PRODUIT_BOUTIQUE_ID." = :".FOURNISSEUR_PRODUIT_BOUTIQUE_ID;
        $params = array(
            FOURNISSEUR_PRODUIT_BOUTIQUE_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>