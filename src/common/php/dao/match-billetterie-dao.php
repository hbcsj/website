<?php
require_once("core/php/lib/abstract-dao.php");

define(MATCH_BILLETTERIE_TABLE_NAME, "hbcsj_match_billetterie");
define(MATCH_BILLETTERIE_ID, "id");
define(MATCH_BILLETTERIE_DATE_HEURE, "date_heure");
define(MATCH_BILLETTERIE_PRIX_1_PLACE, "prix_1_place");
define(MATCH_BILLETTERIE_PRIX_3_PLACES, "prix_3_places");
define(MATCH_BILLETTERIE_PRIX_PUBLIC, "prix_public");
define(MATCH_BILLETTERIE_EN_VENTE, "en_vente");
define(MATCH_BILLETTERIE_DATE_LIMITE_COMMANDE, "date_limite_commande");
define(MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID, "adversaire_billetterie_id");
define(MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID, "competition_billetterie_id");

define(MATCH_BILLETTERIE_DATE_HEURE_DEBUT, "date_heure_debut");
define(MATCH_BILLETTERIE_DATE_HEURE_FIN, "date_heure_fin");

class MatchBilletterieDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(MATCH_BILLETTERIE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".MATCH_BILLETTERIE_TABLE_NAME." WHERE ".MATCH_BILLETTERIE_ID." = :".MATCH_BILLETTERIE_ID;
        $params = array(
            MATCH_BILLETTERIE_ID => $id
        );
        $columns = array(
            MATCH_BILLETTERIE_ID, 
            MATCH_BILLETTERIE_DATE_HEURE, 
            MATCH_BILLETTERIE_PRIX_1_PLACE,
            MATCH_BILLETTERIE_PRIX_3_PLACES,
            MATCH_BILLETTERIE_PRIX_PUBLIC,
            MATCH_BILLETTERIE_EN_VENTE,
            MATCH_BILLETTERIE_DATE_LIMITE_COMMANDE,
            MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID,
            MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".MATCH_BILLETTERIE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            MATCH_BILLETTERIE_ID, 
            MATCH_BILLETTERIE_DATE_HEURE, 
            MATCH_BILLETTERIE_PRIX_1_PLACE,
            MATCH_BILLETTERIE_PRIX_3_PLACES,
            MATCH_BILLETTERIE_PRIX_PUBLIC,
            MATCH_BILLETTERIE_EN_VENTE,
            MATCH_BILLETTERIE_DATE_LIMITE_COMMANDE,
            MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID,
            MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID
        );
        return $this->executeGetRequest($query, null, $columns);
    }

	public function getAllByParams($dateDebut, $dateFin, $adversaireId, $competitionId, $enVente, $orderBy = null) {
        $query = "SELECT * FROM ".MATCH_BILLETTERIE_TABLE_NAME." ";

        $queryParamsArray = array();
        $params = array();
        if ($dateDebut != null && $dateDebut != "") {
            $queryParamsArray[] = MATCH_BILLETTERIE_DATE_HEURE." >= :".MATCH_BILLETTERIE_DATE_HEURE_DEBUT;
            $params[MATCH_BILLETTERIE_DATE_HEURE_DEBUT] = $dateDebut;
        }
        if ($dateFin != null && $dateFin != "") {
            $queryParamsArray[] = MATCH_BILLETTERIE_DATE_HEURE." <= :".MATCH_BILLETTERIE_DATE_HEURE_FIN;
            $params[MATCH_BILLETTERIE_DATE_HEURE_FIN] = $dateFin;
        }
        if ($adversaireId != null && $adversaireId != "") {
            $queryParamsArray[] = MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID." = :".MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID;
            $params[MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID] = $adversaireId;
        }
        if ($competitionId != null && $competitionId != "") {
            $queryParamsArray[] = MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID." = :".MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID;
            $params[MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID] = $competitionId;
        }
        if ($enVente != null && $enVente != "") {
            if ($enVente == 1) {
                $queryParamsArray[] = "(".MATCH_BILLETTERIE_EN_VENTE." IS NOT NULL AND ".MATCH_BILLETTERIE_EN_VENTE." != '')";
            } else if ($enVente == 0) {
                $queryParamsArray[] = "(".MATCH_BILLETTERIE_EN_VENTE." IS NULL OR ".MATCH_BILLETTERIE_EN_VENTE." = '')";
            }
        }
        
        if (sizeof($queryParamsArray) > 0) {
            $query .= " WHERE ".implode(" AND ", $queryParamsArray);
        }

        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            MATCH_BILLETTERIE_ID, 
            MATCH_BILLETTERIE_DATE_HEURE, 
            MATCH_BILLETTERIE_PRIX_1_PLACE,
            MATCH_BILLETTERIE_PRIX_3_PLACES,
            MATCH_BILLETTERIE_PRIX_PUBLIC,
            MATCH_BILLETTERIE_EN_VENTE,
            MATCH_BILLETTERIE_DATE_LIMITE_COMMANDE,
            MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID,
            MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function getEnVenteAVenirAvantDateLimite($orderBy = null) {
        $query = "SELECT * FROM ".MATCH_BILLETTERIE_TABLE_NAME." ";
        $query .= "WHERE ".MATCH_BILLETTERIE_DATE_HEURE." >= '".date(SQL_DATE_TIME_FORMAT)."' ";
        $query .= "AND ".MATCH_BILLETTERIE_EN_VENTE." = 1 ";
        $query .= "AND ".MATCH_BILLETTERIE_DATE_LIMITE_COMMANDE." > '".date(SQL_DATE_TIME_FORMAT)."'";
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            MATCH_BILLETTERIE_ID, 
            MATCH_BILLETTERIE_DATE_HEURE, 
            MATCH_BILLETTERIE_PRIX_1_PLACE,
            MATCH_BILLETTERIE_PRIX_3_PLACES,
            MATCH_BILLETTERIE_PRIX_PUBLIC,
            MATCH_BILLETTERIE_EN_VENTE,
            MATCH_BILLETTERIE_DATE_LIMITE_COMMANDE,
            MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID,
            MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".MATCH_BILLETTERIE_TABLE_NAME." (";
        $query .= MATCH_BILLETTERIE_DATE_HEURE.", ";
        $query .= MATCH_BILLETTERIE_PRIX_1_PLACE.", ";
        $query .= MATCH_BILLETTERIE_PRIX_3_PLACES.", ";
        $query .= MATCH_BILLETTERIE_PRIX_PUBLIC.", ";
        $query .= MATCH_BILLETTERIE_EN_VENTE.", ";
        $query .= MATCH_BILLETTERIE_DATE_LIMITE_COMMANDE.", ";
        $query .= MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID.", ";
        $query .= MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID.") VALUES (";
        $query .= ":".MATCH_BILLETTERIE_DATE_HEURE.", ";
        $query .= ":".MATCH_BILLETTERIE_PRIX_1_PLACE.", ";
        $query .= ":".MATCH_BILLETTERIE_PRIX_3_PLACES.", ";
        $query .= ":".MATCH_BILLETTERIE_PRIX_PUBLIC.", ";
        $query .= ":".MATCH_BILLETTERIE_EN_VENTE.", ";
        $query .= ":".MATCH_BILLETTERIE_DATE_LIMITE_COMMANDE.", ";
        $query .= ":".MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID.", ";
        $query .= ":".MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID.")";
        $params = array(
            MATCH_BILLETTERIE_DATE_HEURE => $object[MATCH_BILLETTERIE_DATE_HEURE],
            MATCH_BILLETTERIE_PRIX_1_PLACE => $object[MATCH_BILLETTERIE_PRIX_1_PLACE],
            MATCH_BILLETTERIE_PRIX_3_PLACES => $object[MATCH_BILLETTERIE_PRIX_3_PLACES],
            MATCH_BILLETTERIE_PRIX_PUBLIC => $object[MATCH_BILLETTERIE_PRIX_PUBLIC],
            MATCH_BILLETTERIE_EN_VENTE => $object[MATCH_BILLETTERIE_EN_VENTE],
            MATCH_BILLETTERIE_DATE_LIMITE_COMMANDE => $object[MATCH_BILLETTERIE_DATE_LIMITE_COMMANDE],
            MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID => $object[MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID],
            MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID => $object[MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {
        $query = "UPDATE ".MATCH_BILLETTERIE_TABLE_NAME." SET ";
        $query .= MATCH_BILLETTERIE_DATE_HEURE." = :".MATCH_BILLETTERIE_DATE_HEURE.", ";
        $query .= MATCH_BILLETTERIE_PRIX_1_PLACE." = :".MATCH_BILLETTERIE_PRIX_1_PLACE.", ";
        $query .= MATCH_BILLETTERIE_PRIX_3_PLACES." = :".MATCH_BILLETTERIE_PRIX_3_PLACES.", ";
        $query .= MATCH_BILLETTERIE_PRIX_PUBLIC." = :".MATCH_BILLETTERIE_PRIX_PUBLIC.", ";
        $query .= MATCH_BILLETTERIE_EN_VENTE." = :".MATCH_BILLETTERIE_EN_VENTE.", ";
        $query .= MATCH_BILLETTERIE_DATE_LIMITE_COMMANDE." = :".MATCH_BILLETTERIE_DATE_LIMITE_COMMANDE.", ";
        $query .= MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID." = :".MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID.", ";
        $query .= MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID." = :".MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID." WHERE ";
        $query .= MATCH_BILLETTERIE_ID." = :".MATCH_BILLETTERIE_ID;
        $params = array(
            MATCH_BILLETTERIE_DATE_HEURE => $object[MATCH_BILLETTERIE_DATE_HEURE],
            MATCH_BILLETTERIE_PRIX_1_PLACE => $object[MATCH_BILLETTERIE_PRIX_1_PLACE],
            MATCH_BILLETTERIE_PRIX_3_PLACES => $object[MATCH_BILLETTERIE_PRIX_3_PLACES],
            MATCH_BILLETTERIE_PRIX_PUBLIC => $object[MATCH_BILLETTERIE_PRIX_PUBLIC],
            MATCH_BILLETTERIE_EN_VENTE => $object[MATCH_BILLETTERIE_EN_VENTE],
            MATCH_BILLETTERIE_DATE_LIMITE_COMMANDE => $object[MATCH_BILLETTERIE_DATE_LIMITE_COMMANDE],
            MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID => $object[MATCH_BILLETTERIE_ADVERSAIRE_BILLETTERIE_ID],
            MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID => $object[MATCH_BILLETTERIE_COMPETITION_BILLETTERIE_ID],
            MATCH_BILLETTERIE_ID => $object[MATCH_BILLETTERIE_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".MATCH_BILLETTERIE_TABLE_NAME." WHERE ".MATCH_BILLETTERIE_ID." = :".MATCH_BILLETTERIE_ID;
        $params = array(
            MATCH_BILLETTERIE_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>