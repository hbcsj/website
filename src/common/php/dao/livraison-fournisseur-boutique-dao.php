<?php
require_once("core/php/lib/abstract-dao.php");

define(LIVRAISON_FOURNISSEUR_BOUTIQUE_TABLE_NAME, "hbcsj_livraison_fournisseur_boutique");
define(LIVRAISON_FOURNISSEUR_BOUTIQUE_ID, "id");
define(LIVRAISON_FOURNISSEUR_BOUTIQUE_DATE, "date");
define(LIVRAISON_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID, "fournisseur_produit_boutique_id");

class LivraisonFournisseurBoutiqueDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(LIVRAISON_FOURNISSEUR_BOUTIQUE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".LIVRAISON_FOURNISSEUR_BOUTIQUE_TABLE_NAME." WHERE ".LIVRAISON_FOURNISSEUR_BOUTIQUE_ID." = :".LIVRAISON_FOURNISSEUR_BOUTIQUE_ID;
        $params = array(
            LIVRAISON_FOURNISSEUR_BOUTIQUE_ID => $id
        );
        $columns = array(
            LIVRAISON_FOURNISSEUR_BOUTIQUE_ID, 
            LIVRAISON_FOURNISSEUR_BOUTIQUE_DATE, 
            LIVRAISON_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".LIVRAISON_FOURNISSEUR_BOUTIQUE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            LIVRAISON_FOURNISSEUR_BOUTIQUE_ID, 
            LIVRAISON_FOURNISSEUR_BOUTIQUE_DATE, 
            LIVRAISON_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID
        );
        return $this->executeGetRequest($query, null, $columns);
    }

    public function getByFournisseurProduitBoutiqueId($fournisseurProduitBoutiqueId) {
        $query = "SELECT * FROM ".LIVRAISON_FOURNISSEUR_BOUTIQUE_TABLE_NAME." WHERE ".LIVRAISON_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID." = :".LIVRAISON_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID;
        $params = array(
            LIVRAISON_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID => $fournisseurProduitBoutiqueId
        );
        $columns = array(
            LIVRAISON_FOURNISSEUR_BOUTIQUE_ID, 
            LIVRAISON_FOURNISSEUR_BOUTIQUE_DATE, 
            LIVRAISON_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".LIVRAISON_FOURNISSEUR_BOUTIQUE_TABLE_NAME." (";
        $query .= LIVRAISON_FOURNISSEUR_BOUTIQUE_DATE.", ";
        $query .= LIVRAISON_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID.") VALUES (";
        $query .= ":".LIVRAISON_FOURNISSEUR_BOUTIQUE_DATE.", ";
        $query .= ":".LIVRAISON_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID.")";
        $params = array(
            LIVRAISON_FOURNISSEUR_BOUTIQUE_DATE => $object[LIVRAISON_FOURNISSEUR_BOUTIQUE_DATE],
            LIVRAISON_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID => $object[LIVRAISON_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {
        $query = "UPDATE ".LIVRAISON_FOURNISSEUR_BOUTIQUE_TABLE_NAME." SET ";
        $query .= LIVRAISON_FOURNISSEUR_BOUTIQUE_DATE." = :".LIVRAISON_FOURNISSEUR_BOUTIQUE_DATE.", ";
        $query .= LIVRAISON_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID." = :".LIVRAISON_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID." WHERE ";
        $query .= LIVRAISON_FOURNISSEUR_BOUTIQUE_ID." = :".LIVRAISON_FOURNISSEUR_BOUTIQUE_ID;
        $params = array(
            LIVRAISON_FOURNISSEUR_BOUTIQUE_DATE => $object[LIVRAISON_FOURNISSEUR_BOUTIQUE_DATE],
            LIVRAISON_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID => $object[LIVRAISON_FOURNISSEUR_BOUTIQUE_FOURNISSEUR_PRODUIT_BOUTIQUE_ID],
            LIVRAISON_FOURNISSEUR_BOUTIQUE_ID => $object[LIVRAISON_FOURNISSEUR_BOUTIQUE_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".LIVRAISON_FOURNISSEUR_BOUTIQUE_TABLE_NAME." WHERE ".LIVRAISON_FOURNISSEUR_BOUTIQUE_ID." = :".LIVRAISON_FOURNISSEUR_BOUTIQUE_ID;
        $params = array(
            LIVRAISON_FOURNISSEUR_BOUTIQUE_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>