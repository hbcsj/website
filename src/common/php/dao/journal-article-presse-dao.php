<?php
require_once("core/php/lib/abstract-dao.php");

define(JOURNAL_ARTICLE_PRESSE_TABLE_NAME, "hbcsj_journal_article_presse");
define(JOURNAL_ARTICLE_PRESSE_ID, "id");
define(JOURNAL_ARTICLE_PRESSE_NOM, "nom");

class JournalArticlePresseDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(JOURNAL_ARTICLE_PRESSE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".JOURNAL_ARTICLE_PRESSE_TABLE_NAME." WHERE ".JOURNAL_ARTICLE_PRESSE_ID." = :".JOURNAL_ARTICLE_PRESSE_ID;
        $params = array(
            JOURNAL_ARTICLE_PRESSE_ID => $id
        );
        $columns = array(
            JOURNAL_ARTICLE_PRESSE_ID, 
            JOURNAL_ARTICLE_PRESSE_NOM
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".JOURNAL_ARTICLE_PRESSE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            JOURNAL_ARTICLE_PRESSE_ID, 
            JOURNAL_ARTICLE_PRESSE_NOM
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {}
	
	public function update($object) {}
	
	public function delete($id) {}
}

?>