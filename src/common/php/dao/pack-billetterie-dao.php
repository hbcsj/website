<?php
require_once("core/php/lib/abstract-dao.php");

define(PACK_BILLETTERIE_TABLE_NAME, "hbcsj_pack_billetterie");
define(PACK_BILLETTERIE_ID, "id");
define(PACK_BILLETTERIE_NOM, "nom");
define(PACK_BILLETTERIE_NB_PLACES, "nb_places");
define(PACK_BILLETTERIE_PRIX, "prix");

class PackBilletterieDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(PACK_BILLETTERIE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".PACK_BILLETTERIE_TABLE_NAME." WHERE ".PACK_BILLETTERIE_ID." = :".PACK_BILLETTERIE_ID;
        $params = array(
            PACK_BILLETTERIE_ID => $id
        );
        $columns = array(
            PACK_BILLETTERIE_ID, 
            PACK_BILLETTERIE_NOM, 
            PACK_BILLETTERIE_NB_PLACES,
            PACK_BILLETTERIE_PRIX
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".PACK_BILLETTERIE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            PACK_BILLETTERIE_ID, 
            PACK_BILLETTERIE_NOM, 
            PACK_BILLETTERIE_NB_PLACES,
            PACK_BILLETTERIE_PRIX
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".PACK_BILLETTERIE_TABLE_NAME." (";
        $query .= PACK_BILLETTERIE_NOM.", ";
        $query .= PACK_BILLETTERIE_NB_PLACES.", ";
        $query .= PACK_BILLETTERIE_PRIX.") VALUES (";
        $query .= ":".PACK_BILLETTERIE_NOM.", ";
        $query .= ":".PACK_BILLETTERIE_NB_PLACES.", ";
        $query .= ":".PACK_BILLETTERIE_PRIX.")";
        $params = array(
            PACK_BILLETTERIE_NOM => $object[PACK_BILLETTERIE_NOM],
            PACK_BILLETTERIE_NB_PLACES => $object[PACK_BILLETTERIE_NB_PLACES],
            PACK_BILLETTERIE_PRIX => $object[PACK_BILLETTERIE_PRIX]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {
        $query = "UPDATE ".PACK_BILLETTERIE_TABLE_NAME." SET ";
        $query .= PACK_BILLETTERIE_NOM." = :".PACK_BILLETTERIE_NOM.", ";
        $query .= PACK_BILLETTERIE_NB_PLACES." = :".PACK_BILLETTERIE_NB_PLACES.", ";
        $query .= PACK_BILLETTERIE_PRIX." = :".PACK_BILLETTERIE_PRIX." WHERE ";
        $query .= PACK_BILLETTERIE_ID." = :".PACK_BILLETTERIE_ID;
        $params = array(
            PACK_BILLETTERIE_NOM => $object[PACK_BILLETTERIE_NOM],
            PACK_BILLETTERIE_NB_PLACES => $object[PACK_BILLETTERIE_NB_PLACES],
            PACK_BILLETTERIE_PRIX => $object[PACK_BILLETTERIE_PRIX],
            PACK_BILLETTERIE_ID => $object[PACK_BILLETTERIE_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".PACK_BILLETTERIE_TABLE_NAME." WHERE ".PACK_BILLETTERIE_ID." = :".PACK_BILLETTERIE_ID;
        $params = array(
            PACK_BILLETTERIE_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>