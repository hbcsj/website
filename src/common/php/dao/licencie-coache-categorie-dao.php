<?php
require_once("core/php/lib/abstract-dao.php");

define(LICENCIE_COACHE_CATEGORIE_TABLE_NAME, "hbcsj_licencie_coache_categorie");
define(LICENCIE_COACHE_CATEGORIE_LICENCIE_ID, "licencie_id");
define(LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID, "categorie_id");
define(LICENCIE_COACHE_CATEGORIE_EN_SOUTIEN, "en_soutien");

class LicencieCoacheCategorieDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(LICENCIE_COACHE_CATEGORIE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".LICENCIE_COACHE_CATEGORIE_TABLE_NAME." WHERE ".LICENCIE_COACHE_CATEGORIE_LICENCIE_ID." = :".LICENCIE_COACHE_CATEGORIE_LICENCIE_ID." AND ".LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID." = :".LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID;
        $params = array(
            LICENCIE_COACHE_CATEGORIE_LICENCIE_ID => $id[LICENCIE_COACHE_CATEGORIE_LICENCIE_ID],
            LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID => $id[LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID]
        );
        $columns = array(
            LICENCIE_COACHE_CATEGORIE_LICENCIE_ID,
            LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID,
            LICENCIE_COACHE_CATEGORIE_EN_SOUTIEN
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".LICENCIE_COACHE_CATEGORIE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            LICENCIE_COACHE_CATEGORIE_LICENCIE_ID,
            LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID,
            LICENCIE_COACHE_CATEGORIE_EN_SOUTIEN
        );
        return $this->executeGetRequest($query, null, $columns);
    }

    public function getByCategorieId($categorieId, $orderBy) {
        $query = "SELECT * FROM ".LICENCIE_COACHE_CATEGORIE_TABLE_NAME." WHERE ".LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID." = :".LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $params = array(
            LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID => $categorieId
        );
        $columns = array(
            LICENCIE_COACHE_CATEGORIE_LICENCIE_ID,
            LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID,
            LICENCIE_COACHE_CATEGORIE_EN_SOUTIEN
        );
        return $this->executeGetRequest($query, $params, $columns);
    }

    public function getByLicencieId($licencieId, $orderBy) {
        $query = "SELECT * FROM ".LICENCIE_COACHE_CATEGORIE_TABLE_NAME." WHERE ".LICENCIE_COACHE_CATEGORIE_LICENCIE_ID." = :".LICENCIE_COACHE_CATEGORIE_LICENCIE_ID;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $params = array(
            LICENCIE_COACHE_CATEGORIE_LICENCIE_ID => $licencieId
        );
        $columns = array(
            LICENCIE_COACHE_CATEGORIE_LICENCIE_ID,
            LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID,
            LICENCIE_COACHE_CATEGORIE_EN_SOUTIEN
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".LICENCIE_COACHE_CATEGORIE_TABLE_NAME." (";
        $query .= LICENCIE_COACHE_CATEGORIE_LICENCIE_ID.", ";
        $query .= LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID.", ";
        $query .= LICENCIE_COACHE_CATEGORIE_EN_SOUTIEN.") VALUES (";
        $query .= ":".LICENCIE_COACHE_CATEGORIE_LICENCIE_ID.", ";
        $query .= ":".LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID.", ";
        $query .= ":".LICENCIE_COACHE_CATEGORIE_EN_SOUTIEN.")";
        $params = array(
            LICENCIE_COACHE_CATEGORIE_LICENCIE_ID => $object[LICENCIE_COACHE_CATEGORIE_LICENCIE_ID],
            LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID => $object[LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID],
            LICENCIE_COACHE_CATEGORIE_EN_SOUTIEN => $object[LICENCIE_COACHE_CATEGORIE_EN_SOUTIEN]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {
        $query = "UPDATE ".LICENCIE_COACHE_CATEGORIE_TABLE_NAME." SET ";
        $query .= LICENCIE_COACHE_CATEGORIE_EN_SOUTIEN." = :".LICENCIE_COACHE_CATEGORIE_EN_SOUTIEN." WHERE ";
        $query .= LICENCIE_COACHE_CATEGORIE_LICENCIE_ID." = :".LICENCIE_COACHE_CATEGORIE_LICENCIE_ID." ";
        $query .= "AND ".LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID." = :".LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID;
        $params = array(
            LICENCIE_COACHE_CATEGORIE_LICENCIE_ID => $object[LICENCIE_COACHE_CATEGORIE_LICENCIE_ID],
            LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID => $object[LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID],
            LICENCIE_COACHE_CATEGORIE_EN_SOUTIEN => $object[LICENCIE_COACHE_CATEGORIE_EN_SOUTIEN]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".LICENCIE_COACHE_CATEGORIE_TABLE_NAME." WHERE ".LICENCIE_COACHE_CATEGORIE_LICENCIE_ID." = :".LICENCIE_COACHE_CATEGORIE_LICENCIE_ID." AND ".LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID." = :".LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID;
        $params = array(
            LICENCIE_COACHE_CATEGORIE_LICENCIE_ID => $id[LICENCIE_COACHE_CATEGORIE_LICENCIE_ID],
            LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID => $id[LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function deleteByLicencieId($licencieId) {
        $query = "DELETE FROM ".LICENCIE_COACHE_CATEGORIE_TABLE_NAME." WHERE ".LICENCIE_COACHE_CATEGORIE_LICENCIE_ID." = :".LICENCIE_COACHE_CATEGORIE_LICENCIE_ID;
        $params = array(
            LICENCIE_COACHE_CATEGORIE_LICENCIE_ID => $licencieId
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>