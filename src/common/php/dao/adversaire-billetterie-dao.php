<?php
require_once("core/php/lib/abstract-dao.php");

define(ADVERSAIRE_BILLETTERIE_TABLE_NAME, "hbcsj_adversaire_billetterie");
define(ADVERSAIRE_BILLETTERIE_ID, "id");
define(ADVERSAIRE_BILLETTERIE_NOM, "nom");

class AdversaireBilletterieDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(ADVERSAIRE_BILLETTERIE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".ADVERSAIRE_BILLETTERIE_TABLE_NAME." WHERE ".ADVERSAIRE_BILLETTERIE_ID." = :".ADVERSAIRE_BILLETTERIE_ID;
        $params = array(
            ADVERSAIRE_BILLETTERIE_ID => $id
        );
        $columns = array(
            ADVERSAIRE_BILLETTERIE_ID, 
            ADVERSAIRE_BILLETTERIE_NOM
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".ADVERSAIRE_BILLETTERIE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            ADVERSAIRE_BILLETTERIE_ID, 
            ADVERSAIRE_BILLETTERIE_NOM
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {}
	
	public function update($object) {}
	
	public function delete($id) {}
}

?>