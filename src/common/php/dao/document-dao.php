<?php
require_once("core/php/lib/abstract-dao.php");

define(DOCUMENT_TABLE_NAME, "hbcsj_document");
define(DOCUMENT_ID, "id");
define(DOCUMENT_NOM, "nom");
define(DOCUMENT_FICHIER, "fichier");
define(DOCUMENT_URL, "url");
define(DOCUMENT_VISIBLE_SUR_SITE, "visible_sur_site");
define(DOCUMENT_EMAIL_DERNIER_EDITEUR, "email_dernier_editeur");

class DocumentDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(DOCUMENT_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".DOCUMENT_TABLE_NAME." WHERE ".DOCUMENT_ID." = :".DOCUMENT_ID;
        $params = array(
            DOCUMENT_ID => $id
        );
        $columns = array(
            DOCUMENT_ID, 
            DOCUMENT_NOM, 
            DOCUMENT_FICHIER, 
            DOCUMENT_URL, 
            DOCUMENT_VISIBLE_SUR_SITE, 
            DOCUMENT_EMAIL_DERNIER_EDITEUR
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".DOCUMENT_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            DOCUMENT_ID, 
            DOCUMENT_NOM, 
            DOCUMENT_FICHIER, 
            DOCUMENT_URL, 
            DOCUMENT_VISIBLE_SUR_SITE, 
            DOCUMENT_EMAIL_DERNIER_EDITEUR
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function getVisiblesSurSite($orderBy = null) {
        $query = "SELECT * FROM ".DOCUMENT_TABLE_NAME." WHERE ".DOCUMENT_VISIBLE_SUR_SITE." = 1";
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            DOCUMENT_ID, 
            DOCUMENT_NOM, 
            DOCUMENT_FICHIER, 
            DOCUMENT_URL, 
            DOCUMENT_VISIBLE_SUR_SITE, 
            DOCUMENT_EMAIL_DERNIER_EDITEUR
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".DOCUMENT_TABLE_NAME." (";
        $query .= DOCUMENT_NOM.", ";
        $query .= DOCUMENT_FICHIER.", ";
        $query .= DOCUMENT_URL.", ";
        $query .= DOCUMENT_VISIBLE_SUR_SITE.", ";
        $query .= DOCUMENT_EMAIL_DERNIER_EDITEUR.") VALUES (";
        $query .= ":".DOCUMENT_NOM.", ";
        $query .= ":".DOCUMENT_FICHIER.", ";
        $query .= ":".DOCUMENT_URL.", ";
        $query .= ":".DOCUMENT_VISIBLE_SUR_SITE.", ";
        $query .= ":".DOCUMENT_EMAIL_DERNIER_EDITEUR.")";
        $params = array(
            DOCUMENT_NOM => $object[DOCUMENT_NOM],
            DOCUMENT_FICHIER => $object[BULLETIN_ANNEE],
            DOCUMENT_URL => $object[DOCUMENT_URL],
            DOCUMENT_VISIBLE_SUR_SITE => $object[DOCUMENT_VISIBLE_SUR_SITE],
            DOCUMENT_EMAIL_DERNIER_EDITEUR => $object[DOCUMENT_EMAIL_DERNIER_EDITEUR]
        );
        return $this->executeCreateRequest($query, $params);}
	
	public function update($object) {
        $query = "UPDATE ".DOCUMENT_TABLE_NAME." SET ";
        $query .= DOCUMENT_NOM." = :".DOCUMENT_NOM.", ";
        $query .= DOCUMENT_FICHIER." = :".DOCUMENT_FICHIER.", ";
        $query .= DOCUMENT_URL." = :".DOCUMENT_URL.", ";
        $query .= DOCUMENT_VISIBLE_SUR_SITE." = :".DOCUMENT_VISIBLE_SUR_SITE.", ";
        $query .= DOCUMENT_EMAIL_DERNIER_EDITEUR." = :".DOCUMENT_EMAIL_DERNIER_EDITEUR." WHERE ";
        $query .= DOCUMENT_ID." = :".DOCUMENT_ID;
        $params = array(
            DOCUMENT_NOM => $object[DOCUMENT_NOM],
            DOCUMENT_FICHIER => $object[BULLETIN_ANNEE],
            DOCUMENT_URL => $object[DOCUMENT_URL],
            DOCUMENT_VISIBLE_SUR_SITE => $object[DOCUMENT_VISIBLE_SUR_SITE],
            DOCUMENT_EMAIL_DERNIER_EDITEUR => $object[DOCUMENT_EMAIL_DERNIER_EDITEUR],
            DOCUMENT_ID => $object[DOCUMENT_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".DOCUMENT_TABLE_NAME." WHERE ".DOCUMENT_ID." = :".DOCUMENT_ID;
        $params = array(
            DOCUMENT_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>