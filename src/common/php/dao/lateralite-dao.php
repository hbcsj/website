<?php
require_once("core/php/lib/abstract-dao.php");

define(LATERALITE_TABLE_NAME, "hbcsj_lateralite");
define(LATERALITE_ID, "id");
define(LATERALITE_LIBELLE, "libelle");

class LateraliteDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(LATERALITE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".LATERALITE_TABLE_NAME." WHERE ".LATERALITE_ID." = :".LATERALITE_ID;
        $params = array(
            LATERALITE_ID => $id
        );
        $columns = array(LATERALITE_ID, LATERALITE_LIBELLE);
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".LATERALITE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(LATERALITE_ID, LATERALITE_LIBELLE);
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {}
	
	public function update($object) {}
	
	public function delete($id) {}
}

?>