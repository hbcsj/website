<?php
require_once("core/php/lib/abstract-dao.php");

define(TYPE_HHSJ_TABLE_NAME, "hbcsj_type_hhsj");
define(TYPE_HHSJ_ID, "id");
define(TYPE_HHSJ_LIBELLE, "libelle");

class TypeHHSJDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(TYPE_HHSJ_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".TYPE_HHSJ_TABLE_NAME." WHERE ".TYPE_HHSJ_ID." = :".TYPE_HHSJ_ID;
        $params = array(
            TYPE_HHSJ_ID => $id
        );
        $columns = array(TYPE_HHSJ_ID, TYPE_HHSJ_LIBELLE);
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".TYPE_HHSJ_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(TYPE_HHSJ_ID, TYPE_HHSJ_LIBELLE);
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {}
	
	public function update($object) {}
	
	public function delete($id) {}
}

?>