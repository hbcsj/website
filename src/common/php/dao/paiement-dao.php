<?php
require_once("core/php/lib/abstract-dao.php");

define(PAIEMENT_TABLE_NAME, "hbcsj_paiement");
define(PAIEMENT_ID, "id");
define(PAIEMENT_MONTANT, "montant");
define(PAIEMENT_DATE, "date");
define(PAIEMENT_REFERENCE, "reference");
define(PAIEMENT_TITULAIRE_COMPTE, "titulaire_compte");
define(PAIEMENT_BANQUE, "banque");
define(PAIEMENT_COMMENTAIRE, "commentaire");
define(PAIEMENT_TYPE_PAIEMENT_ID, "type_paiement_id");

define(PAIEMENT_DATE_DEBUT, "date_debut");
define(PAIEMENT_DATE_FIN, "date_fin");
define(PAIEMENT_MONTANT_MIN, "montant_min");
define(PAIEMENT_MONTANT_MAX, "montant_max");

class PaiementDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(PAIEMENT_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".PAIEMENT_TABLE_NAME." WHERE ".PAIEMENT_ID." = :".PAIEMENT_ID;
        $params = array(
            PAIEMENT_ID => $id
        );
        $columns = array(
            PAIEMENT_ID, 
            PAIEMENT_MONTANT, 
            PAIEMENT_DATE,
            PAIEMENT_REFERENCE,
            PAIEMENT_TITULAIRE_COMPTE,
            PAIEMENT_BANQUE,
            PAIEMENT_COMMENTAIRE,
            PAIEMENT_TYPE_PAIEMENT_ID
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".PAIEMENT_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            PAIEMENT_ID, 
            PAIEMENT_MONTANT, 
            PAIEMENT_DATE,
            PAIEMENT_REFERENCE,
            PAIEMENT_TITULAIRE_COMPTE,
            PAIEMENT_BANQUE,
            PAIEMENT_COMMENTAIRE,
            PAIEMENT_TYPE_PAIEMENT_ID
        );
        return $this->executeGetRequest($query, null, $columns);
    }

	public function getAllByParams($typePaiementId, $nom, $referenceCommande, $nomCommande, $prenomCommande, $dateDebut, $dateFin, $reference, $commentaire, $montant, $saisonId, $orderBy = null) {
        require_once("common/php/dao/commande-dao.php");
        require_once("common/php/dao/paiement-commande-dao.php");
        
        $query = "SELECT ".PAIEMENT_TABLE_NAME.".* FROM ".PAIEMENT_TABLE_NAME." ";
        if (
            ($referenceCommande != null && $referenceCommande != "") ||
            ($nomCommande != null && $nomCommande != "") ||
            ($prenomCommande != null && $prenomCommande != "") ||
            ($saisonId != null && $saisonId != "")
         ) {
            $query .= ", ".PAIEMENT_COMMANDE_TABLE_NAME.", ".COMMANDE_TABLE_NAME." ";
        }

        $queryParamsArray = array();
        $params = array();
        if ($typePaiementId != null && $typePaiementId != "") {
            $queryParamsArray[] = PAIEMENT_TYPE_PAIEMENT_ID." = :".PAIEMENT_TYPE_PAIEMENT_ID;
            $params[PAIEMENT_TYPE_PAIEMENT_ID] = $typePaiementId;
        }
        if ($referenceCommande != null && $referenceCommande != "") {
            $queryParamsArray[] = COMMANDE_TABLE_NAME.".".COMMANDE_REFERENCE." LIKE :".COMMANDE_REFERENCE;
            $params[COMMANDE_REFERENCE] = "%".$referenceCommande."%";
        }
        if ($nomCommande != null && $nomCommande != "") {
            $queryParamsArray[] = COMMANDE_TABLE_NAME.".".COMMANDE_NOM." LIKE :".COMMANDE_NOM;
            $params[COMMANDE_NOM] = "%".$nomCommande."%";
        }
        if ($prenomCommande != null && $prenomCommande != "") {
            $queryParamsArray[] = COMMANDE_TABLE_NAME.".".COMMANDE_PRENOM." LIKE :".COMMANDE_PRENOM;
            $params[COMMANDE_PRENOM] = "%".$prenomCommande."%";
        }
        if ($dateDebut != null && $dateDebut != "") {
            $queryParamsArray[] = PAIEMENT_DATE." >= :".PAIEMENT_DATE_DEBUT;
            $params[PAIEMENT_DATE_DEBUT] = $dateDebut;
        }
        if ($dateFin != null && $dateFin != "") {
            $queryParamsArray[] = PAIEMENT_DATE." <= :".PAIEMENT_DATE_FIN;
            $params[PAIEMENT_DATE_FIN] = $dateFin;
        }
        if ($reference != null && $reference != "") {
            $queryParamsArray[] = PAIEMENT_TABLE_NAME.".".PAIEMENT_REFERENCE." LIKE :".PAIEMENT_REFERENCE;
            $params[PAIEMENT_REFERENCE] = "%".$reference."%";
        }
        if ($commentaire != null && $commentaire != "") {
            $queryParamsArray[] = PAIEMENT_TABLE_NAME.".".PAIEMENT_COMMENTAIRE." LIKE :".PAIEMENT_COMMENTAIRE;
            $params[PAIEMENT_COMMENTAIRE] = "%".$commentaire."%";
        }
        if ($nom != null && $nom != "") {
            $queryParamsArray[] = PAIEMENT_TITULAIRE_COMPTE." LIKE :".PAIEMENT_TITULAIRE_COMPTE;
            $params[PAIEMENT_TITULAIRE_COMPTE] = "%".$nom."%";
        }
        if ($montant != null && $montant != "") {
            $queryParamsArray[] = "(".PAIEMENT_MONTANT." >= :".PAIEMENT_MONTANT_MIN." AND ".PAIEMENT_MONTANT." < :".PAIEMENT_MONTANT_MAX.")";
            $params[PAIEMENT_MONTANT_MIN] = $montant;
            $params[PAIEMENT_MONTANT_MAX] = $montant + 1;
        }
        if ($saisonId != null && $saisonId != "") {
            $queryParamsArray[] = COMMANDE_SAISON_ID." = :".COMMANDE_SAISON_ID;
            $params[COMMANDE_SAISON_ID] = $saisonId;
        }
        if (sizeof($queryParamsArray) > 0) {
            $query .= " WHERE ";
            if (
                ($referenceCommande != null && $referenceCommande != "") ||
                ($nomCommande != null && $nomCommande != "") ||
                ($prenomCommande != null && $prenomCommande != "") ||
                ($saisonId != null && $saisonId != "")
             ) {
                $query .= PAIEMENT_TABLE_NAME.".".PAIEMENT_ID." = ".PAIEMENT_COMMANDE_TABLE_NAME.".".PAIEMENT_COMMANDE_PAIEMENT_ID." ";
                $query .= "AND ".PAIEMENT_COMMANDE_TABLE_NAME.".".PAIEMENT_COMMANDE_COMMANDE_ID." = ".COMMANDE_TABLE_NAME.".".COMMANDE_ID." AND ";
            }
        }
        if (sizeof($queryParamsArray) > 0) {
            $query .= implode(" AND ", $queryParamsArray);
        }

        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            PAIEMENT_ID, 
            PAIEMENT_MONTANT, 
            PAIEMENT_DATE,
            PAIEMENT_REFERENCE,
            PAIEMENT_TITULAIRE_COMPTE,
            PAIEMENT_BANQUE,
            PAIEMENT_COMMENTAIRE,
            PAIEMENT_TYPE_PAIEMENT_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }

    public function getByCommandeId($commandeId, $orderBy = null) {
        require_once("common/php/dao/paiement-commande-dao.php");

        $query = "SELECT * FROM ".PAIEMENT_TABLE_NAME.", ".PAIEMENT_COMMANDE_TABLE_NAME." ";
        $query .= "WHERE ".PAIEMENT_TABLE_NAME.".".PAIEMENT_ID." = ".PAIEMENT_COMMANDE_TABLE_NAME.".".PAIEMENT_COMMANDE_PAIEMENT_ID." ";
        $query .= "AND ".PAIEMENT_COMMANDE_TABLE_NAME.".".PAIEMENT_COMMANDE_COMMANDE_ID." = :".PAIEMENT_COMMANDE_COMMANDE_ID;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $params = array(
            PAIEMENT_COMMANDE_COMMANDE_ID => $commandeId
        );
        $columns = array(
            PAIEMENT_ID, 
            PAIEMENT_MONTANT, 
            PAIEMENT_DATE,
            PAIEMENT_REFERENCE,
            PAIEMENT_TITULAIRE_COMPTE,
            PAIEMENT_BANQUE,
            PAIEMENT_COMMENTAIRE,
            PAIEMENT_TYPE_PAIEMENT_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".PAIEMENT_TABLE_NAME." (";
        $query .= PAIEMENT_MONTANT.", ";
        $query .= PAIEMENT_DATE.", ";
        $query .= PAIEMENT_REFERENCE.", ";
        $query .= PAIEMENT_TITULAIRE_COMPTE.", ";
        $query .= PAIEMENT_BANQUE.", ";
        $query .= PAIEMENT_COMMENTAIRE.", ";
        $query .= PAIEMENT_TYPE_PAIEMENT_ID.") VALUES (";
        $query .= ":".PAIEMENT_MONTANT.", ";
        $query .= ":".PAIEMENT_DATE.", ";
        $query .= ":".PAIEMENT_REFERENCE.", ";
        $query .= ":".PAIEMENT_TITULAIRE_COMPTE.", ";
        $query .= ":".PAIEMENT_BANQUE.", ";
        $query .= ":".PAIEMENT_COMMENTAIRE.", ";
        $query .= ":".PAIEMENT_TYPE_PAIEMENT_ID.")";
        $params = array(
            PAIEMENT_MONTANT => $object[PAIEMENT_MONTANT],
            PAIEMENT_DATE => $object[PAIEMENT_DATE],
            PAIEMENT_REFERENCE => $object[PAIEMENT_REFERENCE],
            PAIEMENT_TITULAIRE_COMPTE => $object[PAIEMENT_TITULAIRE_COMPTE],
            PAIEMENT_BANQUE => $object[PAIEMENT_BANQUE],
            PAIEMENT_COMMENTAIRE => $object[PAIEMENT_COMMENTAIRE],
            PAIEMENT_TYPE_PAIEMENT_ID => $object[PAIEMENT_TYPE_PAIEMENT_ID]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {
        $query = "UPDATE ".PAIEMENT_TABLE_NAME." SET ";
        $query .= PAIEMENT_MONTANT." = :".PAIEMENT_MONTANT.", ";
        $query .= PAIEMENT_DATE." = :".PAIEMENT_DATE.", ";
        $query .= PAIEMENT_REFERENCE." = :".PAIEMENT_REFERENCE.", ";
        $query .= PAIEMENT_TITULAIRE_COMPTE." = :".PAIEMENT_TITULAIRE_COMPTE.", ";
        $query .= PAIEMENT_BANQUE." = :".PAIEMENT_BANQUE.", ";
        $query .= PAIEMENT_COMMENTAIRE." = :".PAIEMENT_COMMENTAIRE.", ";
        $query .= PAIEMENT_TYPE_PAIEMENT_ID." = :".PAIEMENT_TYPE_PAIEMENT_ID." WHERE ";
        $query .= PAIEMENT_ID." = :".PAIEMENT_ID;
        $params = array(
            PAIEMENT_MONTANT => $object[PAIEMENT_MONTANT],
            PAIEMENT_DATE => $object[PAIEMENT_DATE],
            PAIEMENT_REFERENCE => $object[PAIEMENT_REFERENCE],
            PAIEMENT_TITULAIRE_COMPTE => $object[PAIEMENT_TITULAIRE_COMPTE],
            PAIEMENT_BANQUE => $object[PAIEMENT_BANQUE],
            PAIEMENT_COMMENTAIRE => $object[PAIEMENT_COMMENTAIRE],
            PAIEMENT_TYPE_PAIEMENT_ID => $object[PAIEMENT_TYPE_PAIEMENT_ID],
            PAIEMENT_ID => $object[PAIEMENT_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".PAIEMENT_TABLE_NAME." WHERE ".PAIEMENT_ID." = :".PAIEMENT_ID;
        $params = array(
            PAIEMENT_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>