<?php
require_once("core/php/lib/abstract-dao.php");

define(TYPE_DISPO_TABLE_NAME, "hbcsj_type_dispo");
define(TYPE_DISPO_ID, "id");
define(TYPE_DISPO_LIBELLE, "libelle");

class TypeDispoDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(TYPE_DISPO_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".TYPE_DISPO_TABLE_NAME." WHERE ".TYPE_DISPO_ID." = :".TYPE_DISPO_ID;
        $params = array(
            TYPE_DISPO_ID => $id
        );
        $columns = array(TYPE_DISPO_ID, TYPE_DISPO_LIBELLE);
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".TYPE_DISPO_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(TYPE_DISPO_ID, TYPE_DISPO_LIBELLE);
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {}
	
	public function update($object) {}
	
	public function delete($id) {}
}

?>