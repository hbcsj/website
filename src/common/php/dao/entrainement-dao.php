<?php
require_once("core/php/lib/abstract-dao.php");

define(ENTRAINEMENT_TABLE_NAME, "hbcsj_entrainement");
define(ENTRAINEMENT_ID, "id");
define(ENTRAINEMENT_JOUR, "jour");
define(ENTRAINEMENT_HEURE_DEBUT, "heure_debut");
define(ENTRAINEMENT_HEURE_FIN, "heure_fin");
define(ENTRAINEMENT_CATEGORIE_ID, "categorie_id");

class EntrainementDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(ENTRAINEMENT_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".ENTRAINEMENT_TABLE_NAME." WHERE ".ENTRAINEMENT_ID." = :".ENTRAINEMENT_ID;
        $params = array(
            ENTRAINEMENT_ID => $id
        );
        $columns = array(
            ENTRAINEMENT_ID, 
            ENTRAINEMENT_JOUR, 
            ENTRAINEMENT_HEURE_DEBUT, 
            ENTRAINEMENT_HEURE_FIN, 
            ENTRAINEMENT_CATEGORIE_ID
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".ENTRAINEMENT_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            ENTRAINEMENT_ID, 
            ENTRAINEMENT_JOUR, 
            ENTRAINEMENT_HEURE_DEBUT, 
            ENTRAINEMENT_HEURE_FIN, 
            ENTRAINEMENT_CATEGORIE_ID
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function getByCategorie($categorieId, $orderBy = null) {
        $query = "SELECT * FROM ".ENTRAINEMENT_TABLE_NAME." WHERE ".ENTRAINEMENT_CATEGORIE_ID." = :".ENTRAINEMENT_CATEGORIE_ID;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $params = array(
            ENTRAINEMENT_CATEGORIE_ID => $categorieId
        );
        $columns = array(
            ENTRAINEMENT_ID, 
            ENTRAINEMENT_JOUR, 
            ENTRAINEMENT_HEURE_DEBUT, 
            ENTRAINEMENT_HEURE_FIN, 
            ENTRAINEMENT_CATEGORIE_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".ENTRAINEMENT_TABLE_NAME." (";
        $query .= ENTRAINEMENT_JOUR.", ";
        $query .= ENTRAINEMENT_HEURE_DEBUT.", ";
        $query .= ENTRAINEMENT_HEURE_FIN.", ";
        $query .= ENTRAINEMENT_CATEGORIE_ID.") VALUES (";
        $query .= ":".ENTRAINEMENT_JOUR.", ";
        $query .= ":".ENTRAINEMENT_HEURE_DEBUT.", ";
        $query .= ":".ENTRAINEMENT_HEURE_FIN.", ";
        $query .= ":".ENTRAINEMENT_CATEGORIE_ID.")";
        $params = array(
            ENTRAINEMENT_JOUR => $object[ENTRAINEMENT_JOUR],
            ENTRAINEMENT_HEURE_DEBUT => $object[ENTRAINEMENT_HEURE_DEBUT],
            ENTRAINEMENT_HEURE_FIN => $object[ENTRAINEMENT_HEURE_FIN],
            ENTRAINEMENT_CATEGORIE_ID => $object[ENTRAINEMENT_CATEGORIE_ID]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {
        $query = "UPDATE ".ENTRAINEMENT_TABLE_NAME." SET ";
        $query .= ENTRAINEMENT_JOUR." = :".ENTRAINEMENT_JOUR.", ";
        $query .= ENTRAINEMENT_HEURE_DEBUT." = :".ENTRAINEMENT_HEURE_DEBUT.", ";
        $query .= ENTRAINEMENT_HEURE_FIN." = :".ENTRAINEMENT_HEURE_FIN.", ";
        $query .= ENTRAINEMENT_CATEGORIE_ID." = :".ENTRAINEMENT_CATEGORIE_ID." WHERE ";
        $query .= ENTRAINEMENT_ID." = :".ENTRAINEMENT_ID;
        $params = array(
            ENTRAINEMENT_JOUR => $object[ENTRAINEMENT_JOUR],
            ENTRAINEMENT_HEURE_DEBUT => $object[ENTRAINEMENT_HEURE_DEBUT],
            ENTRAINEMENT_HEURE_FIN => $object[ENTRAINEMENT_HEURE_FIN],
            ENTRAINEMENT_CATEGORIE_ID => $object[ENTRAINEMENT_CATEGORIE_ID],
            ENTRAINEMENT_ID => $object[ENTRAINEMENT_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".ENTRAINEMENT_TABLE_NAME." WHERE ".ENTRAINEMENT_ID." = :".ENTRAINEMENT_ID;
        $params = array(
            ENTRAINEMENT_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>