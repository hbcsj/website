<?php
require_once("core/php/lib/abstract-dao.php");

define(MOTS_DE_PASSES_TABLE_NAME, "hbcsj_mots_de_passes");
define(MOTS_DE_PASSES_ID, "id");
define(MOTS_DE_PASSES_SUPERADMIN, "superadmin");
define(MOTS_DE_PASSES_BUREAU, "bureau");
define(MOTS_DE_PASSES_COACH, "coach");
define(MOTS_DE_PASSES_EDITEUR, "editeur");
define(MOTS_DE_PASSES_COMMERCIAL, "commercial");
define(MOTS_DE_PASSES_DISPOS, "dispos");

class MotsDePassesDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(MOTS_DE_PASSES_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {}

    public function getSingleton() {
        $singleton = null;

        $list = $this->getAll();
        if (sizeof($list) > 0) {
            $singleton = $list[0];
        }

        return $singleton;
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".MOTS_DE_PASSES_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            MOTS_DE_PASSES_ID, 
            MOTS_DE_PASSES_SUPERADMIN, 
            MOTS_DE_PASSES_BUREAU, 
            MOTS_DE_PASSES_COACH, 
            MOTS_DE_PASSES_EDITEUR,
            MOTS_DE_PASSES_COMMERCIAL,
            MOTS_DE_PASSES_DISPOS
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {}
	
	public function update($object) {}
	
	public function delete($id) {}
}

?>