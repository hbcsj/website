<?php
require_once("core/php/lib/abstract-dao.php");

define(GYMNASE_TABLE_NAME, "hbcsj_gymnase");
define(GYMNASE_ID, "id");
define(GYMNASE_NOM, "nom");
define(GYMNASE_ADRESSE, "adresse");
define(GYMNASE_CODE_POSTAL, "code_postal");
define(GYMNASE_VILLE, "ville");
define(GYMNASE_LATITUDE, "latitude");
define(GYMNASE_LONGITUDE, "longitude");
define(GYMNASE_A_DOMICILE, "a_domicile");

define(GYMNASE_NB_GYMNASES, "nb_gymnases");

class GymnaseDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(GYMNASE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".GYMNASE_TABLE_NAME." WHERE ".GYMNASE_ID." = :".GYMNASE_ID;
        $params = array(
            GYMNASE_ID => $id
        );
        $columns = array(
            GYMNASE_ID, 
            GYMNASE_NOM, 
            GYMNASE_ADRESSE, 
            GYMNASE_CODE_POSTAL, 
            GYMNASE_VILLE, 
            GYMNASE_LATITUDE, 
            GYMNASE_LONGITUDE, 
            GYMNASE_A_DOMICILE
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".GYMNASE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            GYMNASE_ID, 
            GYMNASE_NOM, 
            GYMNASE_ADRESSE, 
            GYMNASE_CODE_POSTAL, 
            GYMNASE_VILLE, 
            GYMNASE_LATITUDE, 
            GYMNASE_LONGITUDE, 
            GYMNASE_A_DOMICILE
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function getADomicile() {
        $query = "SELECT * FROM ".GYMNASE_TABLE_NAME." WHERE ".GYMNASE_TABLE_NAME.".".GYMNASE_A_DOMICILE." = 1";
        $columns = array(
            GYMNASE_ID, 
            GYMNASE_NOM, 
            GYMNASE_ADRESSE, 
            GYMNASE_CODE_POSTAL, 
            GYMNASE_VILLE, 
            GYMNASE_LATITUDE, 
            GYMNASE_LONGITUDE, 
            GYMNASE_A_DOMICILE
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function getNbTotal() {
        $query = "SELECT COUNT(DISTINCT ".GYMNASE_TABLE_NAME.".".GYMNASE_ID.") AS ".GYMNASE_NB_GYMNASES." ";
        $query .= "FROM ".GYMNASE_TABLE_NAME;
        $columns = array(
            GYMNASE_NB_GYMNASES
        );
        return $this->executeGetUniqueRequest($query, null, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".GYMNASE_TABLE_NAME." (";
        $query .= GYMNASE_NOM.", ";
        $query .= GYMNASE_ADRESSE.", ";
        $query .= GYMNASE_CODE_POSTAL.", ";
        $query .= GYMNASE_VILLE.", ";
        $query .= GYMNASE_LATITUDE.", ";
        $query .= GYMNASE_LONGITUDE.", ";
        $query .= GYMNASE_A_DOMICILE.") VALUES (";
        $query .= ":".GYMNASE_NOM.", ";
        $query .= ":".GYMNASE_ADRESSE.", ";
        $query .= ":".GYMNASE_CODE_POSTAL.", ";
        $query .= ":".GYMNASE_VILLE.", ";
        $query .= ":".GYMNASE_LATITUDE.", ";
        $query .= ":".GYMNASE_LONGITUDE.", ";
        $query .= ":".GYMNASE_A_DOMICILE.")";
        $params = array(
            GYMNASE_NOM => $object[GYMNASE_NOM],
            GYMNASE_ADRESSE => $object[GYMNASE_ADRESSE],
            GYMNASE_CODE_POSTAL => $object[GYMNASE_CODE_POSTAL],
            GYMNASE_VILLE => $object[GYMNASE_VILLE],
            GYMNASE_LATITUDE => $object[GYMNASE_LATITUDE],
            GYMNASE_LONGITUDE => $object[GYMNASE_LONGITUDE],
            GYMNASE_A_DOMICILE => $object[GYMNASE_A_DOMICILE]
        );
        return $this->executeCreateRequest($query, $params);}
	
	public function update($object) {
        $query = "UPDATE ".GYMNASE_TABLE_NAME." SET ";
        $query .= GYMNASE_NOM." = :".GYMNASE_NOM.", ";
        $query .= GYMNASE_ADRESSE." = :".GYMNASE_ADRESSE.", ";
        $query .= GYMNASE_CODE_POSTAL." = :".GYMNASE_CODE_POSTAL.", ";
        $query .= GYMNASE_VILLE." = :".GYMNASE_VILLE.", ";
        $query .= GYMNASE_LATITUDE." = :".GYMNASE_LATITUDE.", ";
        $query .= GYMNASE_LONGITUDE." = :".GYMNASE_LONGITUDE.", ";
        $query .= GYMNASE_A_DOMICILE." = :".GYMNASE_A_DOMICILE." WHERE ";
        $query .= GYMNASE_ID." = :".GYMNASE_ID;
        $params = array(
            GYMNASE_NOM => $object[GYMNASE_NOM],
            GYMNASE_ADRESSE => $object[GYMNASE_ADRESSE],
            GYMNASE_CODE_POSTAL => $object[GYMNASE_CODE_POSTAL],
            GYMNASE_VILLE => $object[GYMNASE_VILLE],
            GYMNASE_LATITUDE => $object[GYMNASE_LATITUDE],
            GYMNASE_LONGITUDE => $object[GYMNASE_LONGITUDE],
            GYMNASE_A_DOMICILE => $object[GYMNASE_A_DOMICILE],
            GYMNASE_ID => $object[GYMNASE_ID],
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".GYMNASE_TABLE_NAME." WHERE ".GYMNASE_ID." = :".GYMNASE_ID;
        $params = array(
            GYMNASE_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>