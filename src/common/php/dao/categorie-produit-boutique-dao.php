<?php
require_once("core/php/lib/abstract-dao.php");

define(CATEGORIE_PRODUIT_BOUTIQUE_TABLE_NAME, "hbcsj_categorie_produit_boutique");
define(CATEGORIE_PRODUIT_BOUTIQUE_ID, "id");
define(CATEGORIE_PRODUIT_BOUTIQUE_NOM, "nom");

class CategorieProduitBoutiqueDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(CATEGORIE_PRODUIT_BOUTIQUE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".CATEGORIE_PRODUIT_BOUTIQUE_TABLE_NAME." WHERE ".CATEGORIE_PRODUIT_BOUTIQUE_ID." = :".CATEGORIE_PRODUIT_BOUTIQUE_ID;
        $params = array(
            CATEGORIE_PRODUIT_BOUTIQUE_ID => $id
        );
        $columns = array(
            CATEGORIE_PRODUIT_BOUTIQUE_ID, 
            CATEGORIE_PRODUIT_BOUTIQUE_NOM
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".CATEGORIE_PRODUIT_BOUTIQUE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            CATEGORIE_PRODUIT_BOUTIQUE_ID, 
            CATEGORIE_PRODUIT_BOUTIQUE_NOM
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {}
	
	public function update($object) {}
	
	public function delete($id) {}
}

?>