<?php
require_once("core/php/lib/abstract-dao.php");

define(PAIEMENT_COMMANDE_TABLE_NAME, "hbcsj_paiement_commande");
define(PAIEMENT_COMMANDE_COMMANDE_ID, "commande_id");
define(PAIEMENT_COMMANDE_PAIEMENT_ID, "paiement_id");

class PaiementCommandeDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(PAIEMENT_COMMANDE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".PAIEMENT_COMMANDE_TABLE_NAME." WHERE ".PAIEMENT_COMMANDE_COMMANDE_ID." = :".PAIEMENT_COMMANDE_COMMANDE_ID." AND ".PAIEMENT_COMMANDE_PAIEMENT_ID." = :".PAIEMENT_COMMANDE_PAIEMENT_ID;
        $params = array(
            PAIEMENT_COMMANDE_COMMANDE_ID => $id[PAIEMENT_COMMANDE_COMMANDE_ID],
            PAIEMENT_COMMANDE_PAIEMENT_ID => $id[PAIEMENT_COMMANDE_PAIEMENT_ID]
        );
        $columns = array(PAIEMENT_COMMANDE_COMMANDE_ID, PAIEMENT_COMMANDE_PAIEMENT_ID);
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".PAIEMENT_COMMANDE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(PAIEMENT_COMMANDE_COMMANDE_ID, PAIEMENT_COMMANDE_PAIEMENT_ID);
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".PAIEMENT_COMMANDE_TABLE_NAME." (".PAIEMENT_COMMANDE_COMMANDE_ID.", ".PAIEMENT_COMMANDE_PAIEMENT_ID.") VALUES (:".PAIEMENT_COMMANDE_COMMANDE_ID.", :".PAIEMENT_COMMANDE_PAIEMENT_ID.");";
        $params = array(
            PAIEMENT_COMMANDE_COMMANDE_ID => $object[PAIEMENT_COMMANDE_COMMANDE_ID],
            PAIEMENT_COMMANDE_PAIEMENT_ID => $object[PAIEMENT_COMMANDE_PAIEMENT_ID]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {}
	
	public function delete($id) {
        $query = "DELETE FROM ".PAIEMENT_COMMANDE_TABLE_NAME." WHERE ".PAIEMENT_COMMANDE_COMMANDE_ID." = :".PAIEMENT_COMMANDE_COMMANDE_ID." AND ".PAIEMENT_COMMANDE_PAIEMENT_ID." = :".PAIEMENT_COMMANDE_PAIEMENT_ID;
        $params = array(
            PAIEMENT_COMMANDE_COMMANDE_ID => $id[PAIEMENT_COMMANDE_COMMANDE_ID],
            PAIEMENT_COMMANDE_PAIEMENT_ID => $id[PAIEMENT_COMMANDE_PAIEMENT_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function deleteByCommandeId($commandeId) {
        $query = "DELETE FROM ".PAIEMENT_COMMANDE_TABLE_NAME." WHERE ".PAIEMENT_COMMANDE_COMMANDE_ID." = :".PAIEMENT_COMMANDE_COMMANDE_ID;
        $params = array(
            PAIEMENT_COMMANDE_COMMANDE_ID => $commandeId
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function deleteByPaiementId($paiementId) {
        $query = "DELETE FROM ".PAIEMENT_COMMANDE_TABLE_NAME." WHERE ".PAIEMENT_COMMANDE_PAIEMENT_ID." = :".PAIEMENT_COMMANDE_PAIEMENT_ID;
        $params = array(
            PAIEMENT_COMMANDE_PAIEMENT_ID => $paiementId
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>