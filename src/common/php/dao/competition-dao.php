<?php
require_once("core/php/lib/abstract-dao.php");

define(COMPETITION_TABLE_NAME, "hbcsj_competition");
define(COMPETITION_ID, "id");
define(COMPETITION_CODE, "code_competition");
define(COMPETITION_NOM, "nom");
define(COMPETITION_NUM_EQUIPE, "num_equipe");
define(COMPETITION_URL_SITE_FFHB, "url_site_ffhb");
define(COMPETITION_URL_SITE_LIGUE, "url_site_ligue");
define(COMPETITION_EN_COURS, "en_cours");
define(COMPETITION_VISIBLE_SUR_SITE, "visible_sur_site");
define(COMPETITION_EMAIL_DERNIER_EDITEUR, "email_dernier_editeur");
define(COMPETITION_CATEGORIE_ID, "categorie_id");
define(COMPETITION_SAISON_ID, "saison_id");

define(COMPETITION_NB_COMPETITIONS, "nb_competitions");
define(COMPETITION_MAX_NUM_EQUIPE, "max_num_equipe");

class CompetitionDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(COMPETITION_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".COMPETITION_TABLE_NAME." WHERE ".COMPETITION_ID." = :".COMPETITION_ID;
        $params = array(
            COMPETITION_ID => $id
        );
        $columns = array(
            COMPETITION_ID, 
            COMPETITION_CODE,
            COMPETITION_NOM, 
            COMPETITION_NUM_EQUIPE, 
            COMPETITION_URL_SITE_FFHB, 
            COMPETITION_URL_SITE_LIGUE, 
            COMPETITION_EN_COURS, 
            COMPETITION_VISIBLE_SUR_SITE, 
            COMPETITION_EMAIL_DERNIER_EDITEUR,
            COMPETITION_CATEGORIE_ID,
            COMPETITION_SAISON_ID
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".COMPETITION_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            COMPETITION_ID, 
            COMPETITION_CODE,
            COMPETITION_NOM, 
            COMPETITION_NUM_EQUIPE, 
            COMPETITION_URL_SITE_FFHB, 
            COMPETITION_URL_SITE_LIGUE, 
            COMPETITION_EN_COURS, 
            COMPETITION_VISIBLE_SUR_SITE, 
            COMPETITION_EMAIL_DERNIER_EDITEUR,
            COMPETITION_CATEGORIE_ID,
            COMPETITION_SAISON_ID
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function getByCategorieId($categorieId, $orderBy = null) {
        $query = "SELECT * FROM ".COMPETITION_TABLE_NAME." WHERE ".COMPETITION_CATEGORIE_ID." IS NULL OR ".COMPETITION_CATEGORIE_ID." = :".COMPETITION_CATEGORIE_ID;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $params = array(
            COMPETITION_CATEGORIE_ID => $categorieId
        );
        $columns = array(
            COMPETITION_ID, 
            COMPETITION_CODE,
            COMPETITION_NOM, 
            COMPETITION_NUM_EQUIPE, 
            COMPETITION_URL_SITE_FFHB, 
            COMPETITION_URL_SITE_LIGUE, 
            COMPETITION_EN_COURS, 
            COMPETITION_VISIBLE_SUR_SITE, 
            COMPETITION_EMAIL_DERNIER_EDITEUR,
            COMPETITION_CATEGORIE_ID,
            COMPETITION_SAISON_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function getVisiblesEnCoursByCategorie($categorieId, $orderBy = null) {
        $query = "SELECT * FROM ".COMPETITION_TABLE_NAME." WHERE ".COMPETITION_CATEGORIE_ID." = :".COMPETITION_CATEGORIE_ID." AND ".COMPETITION_VISIBLE_SUR_SITE." = 1 AND ".COMPETITION_EN_COURS." = 1";
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $params = array(
            COMPETITION_CATEGORIE_ID => $categorieId
        );
        $columns = array(
            COMPETITION_ID, 
            COMPETITION_CODE,
            COMPETITION_NOM, 
            COMPETITION_NUM_EQUIPE, 
            COMPETITION_URL_SITE_FFHB, 
            COMPETITION_URL_SITE_LIGUE, 
            COMPETITION_EN_COURS, 
            COMPETITION_VISIBLE_SUR_SITE, 
            COMPETITION_EMAIL_DERNIER_EDITEUR,
            COMPETITION_CATEGORIE_ID,
            COMPETITION_SAISON_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }

	public function getAllByParams($nom, $categorieId, $saisonId, $numEquipe, $enCours, $visible, $orderBy = null) {
        $query = "SELECT * FROM ".COMPETITION_TABLE_NAME." ";

        $queryParamsArray = array();
        $params = array();
        if ($nom != null && $nom != "") {
            $queryParamsArray[] = COMPETITION_NOM." LIKE :".COMPETITION_NOM;
            $params[COMPETITION_NOM] = "%".$nom."%";
        }
        if ($categorieId != null && $categorieId != "") {
            $queryParamsArray[] = COMPETITION_CATEGORIE_ID." = :".COMPETITION_CATEGORIE_ID;
            $params[COMPETITION_CATEGORIE_ID] = $categorieId;
        }
        if ($saisonId != null && $saisonId != "") {
            $queryParamsArray[] = COMPETITION_SAISON_ID." = :".COMPETITION_SAISON_ID;
            $params[COMPETITION_SAISON_ID] = $saisonId;
        }
        if ($numEquipe != null && $numEquipe != "") {
            $queryParamsArray[] = COMPETITION_NUM_EQUIPE." = :".COMPETITION_NUM_EQUIPE;
            $params[COMPETITION_NUM_EQUIPE] = $numEquipe;
        }
        if ($enCours != null && $enCours != "") {
            if ($enCours == 1) {
                $queryParamsArray[] = "(".COMPETITION_EN_COURS." IS NOT NULL AND ".COMPETITION_EN_COURS." != '')";
            } else if ($enCours == 0) {
                $queryParamsArray[] = "(".COMPETITION_EN_COURS." IS NULL OR ".COMPETITION_EN_COURS." = '')";
            }
        }
        if ($visible != null && $visible != "") {
            if ($visible == 1) {
                $queryParamsArray[] = "(".COMPETITION_VISIBLE_SUR_SITE." IS NOT NULL AND ".COMPETITION_VISIBLE_SUR_SITE." != '')";
            } else if ($visible == 0) {
                $queryParamsArray[] = "(".COMPETITION_VISIBLE_SUR_SITE." IS NULL OR ".COMPETITION_VISIBLE_SUR_SITE." = '')";
            }
        }
        
        if (sizeof($queryParamsArray) > 0) {
            $query .= " WHERE ".implode(" AND ", $queryParamsArray);
        }

        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            COMPETITION_ID, 
            COMPETITION_CODE,
            COMPETITION_NOM, 
            COMPETITION_NUM_EQUIPE, 
            COMPETITION_URL_SITE_FFHB, 
            COMPETITION_URL_SITE_LIGUE, 
            COMPETITION_EN_COURS, 
            COMPETITION_VISIBLE_SUR_SITE, 
            COMPETITION_EMAIL_DERNIER_EDITEUR,
            COMPETITION_CATEGORIE_ID,
            COMPETITION_SAISON_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function getNbTotal() {
        $query = "SELECT COUNT(DISTINCT ".COMPETITION_TABLE_NAME.".".COMPETITION_ID.") AS ".COMPETITION_NB_COMPETITIONS." ";
        $query .= "FROM ".COMPETITION_TABLE_NAME;
        $columns = array(
            COMPETITION_NB_COMPETITIONS
        );
        return $this->executeGetUniqueRequest($query, null, $columns);
    }
	
	public function getNbEnCours() {
        $query = "SELECT COUNT(DISTINCT ".COMPETITION_TABLE_NAME.".".COMPETITION_ID.") AS ".COMPETITION_NB_COMPETITIONS." ";
        $query .= "FROM ".COMPETITION_TABLE_NAME." ";
        $query .= "WHERE ".COMPETITION_EN_COURS." = 1";
        $columns = array(
            COMPETITION_NB_COMPETITIONS
        );
        return $this->executeGetUniqueRequest($query, null, $columns);
    }
	
	public function getNbVisibles() {
        $query = "SELECT COUNT(DISTINCT ".COMPETITION_TABLE_NAME.".".COMPETITION_ID.") AS ".COMPETITION_NB_COMPETITIONS." ";
        $query .= "FROM ".COMPETITION_TABLE_NAME." ";
        $query .= "WHERE ".COMPETITION_VISIBLE_SUR_SITE." = 1";
        $columns = array(
            COMPETITION_NB_COMPETITIONS
        );
        return $this->executeGetUniqueRequest($query, null, $columns);
    }
	
	public function getNbAValider() {
        $query = "SELECT COUNT(DISTINCT ".COMPETITION_TABLE_NAME.".".COMPETITION_ID.") AS ".COMPETITION_NB_COMPETITIONS." ";
        $query .= "FROM ".COMPETITION_TABLE_NAME." ";
        $query .= "WHERE ".COMPETITION_VISIBLE_SUR_SITE." = 0 ";
        $query .= "AND ".COMPETITION_EMAIL_DERNIER_EDITEUR." != :".COMPETITION_EMAIL_DERNIER_EDITEUR;
        $params = array(
            COMPETITION_EMAIL_DERNIER_EDITEUR => $GLOBALS[CONF][SUPERADMIN_EMAIL]
        );
        $columns = array(
            COMPETITION_NB_COMPETITIONS
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }

    public function getNumEquipeMax() {
        $query = "SELECT MAX(".COMPETITION_TABLE_NAME.".".COMPETITION_NUM_EQUIPE.") AS ".COMPETITION_MAX_NUM_EQUIPE." ";
        $query .= "FROM ".COMPETITION_TABLE_NAME;
        $columns = array(
            COMPETITION_MAX_NUM_EQUIPE
        );
        return $this->executeGetUniqueRequest($query, null, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".COMPETITION_TABLE_NAME." (";
        $query .= COMPETITION_CODE.", ";
        $query .= COMPETITION_NOM.", ";
        $query .= COMPETITION_NUM_EQUIPE.", ";
        $query .= COMPETITION_URL_SITE_FFHB.", ";
        $query .= COMPETITION_URL_SITE_LIGUE.", ";
        $query .= COMPETITION_EN_COURS.", ";
        $query .= COMPETITION_VISIBLE_SUR_SITE.", ";
        $query .= COMPETITION_EMAIL_DERNIER_EDITEUR.", ";
        $query .= COMPETITION_CATEGORIE_ID.", ";
        $query .= COMPETITION_SAISON_ID.") VALUES (";
        $query .= ":".COMPETITION_CODE.", ";
        $query .= ":".COMPETITION_NOM.", ";
        $query .= ":".COMPETITION_NUM_EQUIPE.", ";
        $query .= ":".COMPETITION_URL_SITE_FFHB.", ";
        $query .= ":".COMPETITION_URL_SITE_LIGUE.", ";
        $query .= ":".COMPETITION_EN_COURS.", ";
        $query .= ":".COMPETITION_VISIBLE_SUR_SITE.", ";
        $query .= ":".COMPETITION_EMAIL_DERNIER_EDITEUR.", ";
        $query .= ":".COMPETITION_CATEGORIE_ID.", ";
        $query .= ":".COMPETITION_SAISON_ID.")";
        $params = array(
            COMPETITION_CODE => $object[COMPETITION_CODE],
            COMPETITION_NOM => $object[COMPETITION_NOM],
            COMPETITION_NUM_EQUIPE => $object[COMPETITION_NUM_EQUIPE],
            COMPETITION_URL_SITE_FFHB => $object[COMPETITION_URL_SITE_FFHB],
            COMPETITION_URL_SITE_LIGUE => $object[COMPETITION_URL_SITE_LIGUE],
            COMPETITION_EN_COURS => $object[COMPETITION_EN_COURS],
            COMPETITION_VISIBLE_SUR_SITE => $object[COMPETITION_VISIBLE_SUR_SITE],
            COMPETITION_EMAIL_DERNIER_EDITEUR => $object[COMPETITION_EMAIL_DERNIER_EDITEUR],
            COMPETITION_CATEGORIE_ID => $object[COMPETITION_CATEGORIE_ID],
            COMPETITION_SAISON_ID => $object[COMPETITION_SAISON_ID]
        );
        return $this->executeCreateRequest($query, $params);}
	
	public function update($object) {
        $query = "UPDATE ".COMPETITION_TABLE_NAME." SET ";
        $query .= COMPETITION_CODE." = :".COMPETITION_CODE.", ";
        $query .= COMPETITION_NOM." = :".COMPETITION_NOM.", ";
        $query .= COMPETITION_NUM_EQUIPE." = :".COMPETITION_NUM_EQUIPE.", ";
        $query .= COMPETITION_URL_SITE_FFHB." = :".COMPETITION_URL_SITE_FFHB.", ";
        $query .= COMPETITION_URL_SITE_LIGUE." = :".COMPETITION_URL_SITE_LIGUE.", ";
        $query .= COMPETITION_EN_COURS." = :".COMPETITION_EN_COURS.", ";
        $query .= COMPETITION_VISIBLE_SUR_SITE." = :".COMPETITION_VISIBLE_SUR_SITE.", ";
        $query .= COMPETITION_EMAIL_DERNIER_EDITEUR." = :".COMPETITION_EMAIL_DERNIER_EDITEUR.", ";
        $query .= COMPETITION_CATEGORIE_ID." = :".COMPETITION_CATEGORIE_ID.", ";
        $query .= COMPETITION_SAISON_ID." = :".COMPETITION_SAISON_ID." WHERE ";
        $query .= COMPETITION_ID." = :".COMPETITION_ID;
        $params = array(
            COMPETITION_CODE => $object[COMPETITION_CODE],
            COMPETITION_NOM => $object[COMPETITION_NOM],
            COMPETITION_NUM_EQUIPE => $object[COMPETITION_NUM_EQUIPE],
            COMPETITION_URL_SITE_FFHB => $object[COMPETITION_URL_SITE_FFHB],
            COMPETITION_URL_SITE_LIGUE => $object[COMPETITION_URL_SITE_LIGUE],
            COMPETITION_EN_COURS => $object[COMPETITION_EN_COURS],
            COMPETITION_VISIBLE_SUR_SITE => $object[COMPETITION_VISIBLE_SUR_SITE],
            COMPETITION_EMAIL_DERNIER_EDITEUR => $object[COMPETITION_EMAIL_DERNIER_EDITEUR],
            COMPETITION_CATEGORIE_ID => $object[COMPETITION_CATEGORIE_ID],
            COMPETITION_SAISON_ID => $object[COMPETITION_SAISON_ID],
            COMPETITION_ID => $object[COMPETITION_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".COMPETITION_TABLE_NAME." WHERE ".COMPETITION_ID." = :".COMPETITION_ID;
        $params = array(
            COMPETITION_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>