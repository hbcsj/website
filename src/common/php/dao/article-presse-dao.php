<?php
require_once("core/php/lib/abstract-dao.php");

define(ARTICLE_PRESSE_TABLE_NAME, "hbcsj_article_presse");
define(ARTICLE_PRESSE_ID, "id");
define(ARTICLE_PRESSE_DATE, "date");
define(ARTICLE_PRESSE_TITRE, "titre");
define(ARTICLE_PRESSE_URL, "url");
define(ARTICLE_PRESSE_VISIBLE_SUR_SITE, "visible_sur_site");
define(ARTICLE_PRESSE_EMAIL_DERNIER_EDITEUR, "email_dernier_editeur");
define(ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID, "journal_article_presse_id");

define(ARTICLE_PRESSE_DATE_DEBUT, "date_debut");
define(ARTICLE_PRESSE_DATE_FIN, "date_fin");
define(ARTICLE_PRESSE_NB_ARTICLES, "nb_articles_presse");

class ArticlePresseDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(ARTICLE_PRESSE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".ARTICLE_PRESSE_TABLE_NAME." WHERE ".ARTICLE_PRESSE_ID." = :".ARTICLE_PRESSE_ID;
        $params = array(
            ARTICLE_PRESSE_ID => $id
        );
        $columns = array(
            ARTICLE_PRESSE_ID, 
            ARTICLE_PRESSE_DATE, 
            ARTICLE_PRESSE_TITRE, 
            ARTICLE_PRESSE_URL,
            ARTICLE_PRESSE_VISIBLE_SUR_SITE,
            ARTICLE_PRESSE_EMAIL_DERNIER_EDITEUR,
            ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".ARTICLE_PRESSE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            ARTICLE_PRESSE_ID, 
            ARTICLE_PRESSE_DATE, 
            ARTICLE_PRESSE_TITRE, 
            ARTICLE_PRESSE_URL,
            ARTICLE_PRESSE_VISIBLE_SUR_SITE,
            ARTICLE_PRESSE_EMAIL_DERNIER_EDITEUR,
            ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function getAllByParams($dateDebut, $dateFin, $titre, $journalArticlePresseId, $orderBy = null) {
        $query = "SELECT * FROM ".ARTICLE_PRESSE_TABLE_NAME;
        
        $queryParamsArray = array();
        $params = array();
        if ($dateDebut != null && $dateDebut != "") {
            $queryParamsArray[] = ARTICLE_PRESSE_DATE." >= :".ARTICLE_PRESSE_DATE_DEBUT;
            $params[ARTICLE_PRESSE_DATE_DEBUT] = $dateDebut;
        }
        if ($dateFin != null && $dateFin != "") {
            $queryParamsArray[] = ARTICLE_PRESSE_DATE." <= :".ARTICLE_PRESSE_DATE_FIN;
            $params[ARTICLE_PRESSE_DATE_FIN] = $dateFin;
        }
        if ($titre != null && $titre != "") {
            $queryParamsArray[] = ARTICLE_PRESSE_TITRE." LIKE :".ARTICLE_PRESSE_TITRE;
            $params[ARTICLE_PRESSE_TITRE] = "%".$titre."%";
        }
        if ($journalArticlePresseId != null && $journalArticlePresseId != "") {
            $queryParamsArray[] = ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID." = :".ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID;
            $params[ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID] = $journalArticlePresseId;
        }
        if (sizeof($queryParamsArray) > 0) {
            $query .= " WHERE ";
            $query .= implode(" AND ", $queryParamsArray);
        }
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }

        $columns = array(
            ARTICLE_PRESSE_ID, 
            ARTICLE_PRESSE_DATE, 
            ARTICLE_PRESSE_TITRE, 
            ARTICLE_PRESSE_URL,
            ARTICLE_PRESSE_VISIBLE_SUR_SITE,
            ARTICLE_PRESSE_EMAIL_DERNIER_EDITEUR,
            ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function getVisibles($orderBy = null) {
        $query = "SELECT * FROM ".ARTICLE_PRESSE_TABLE_NAME." WHERE ".ARTICLE_PRESSE_VISIBLE_SUR_SITE." = 1";
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            ARTICLE_PRESSE_ID, 
            ARTICLE_PRESSE_DATE, 
            ARTICLE_PRESSE_TITRE, 
            ARTICLE_PRESSE_URL,
            ARTICLE_PRESSE_VISIBLE_SUR_SITE,
            ARTICLE_PRESSE_EMAIL_DERNIER_EDITEUR,
            ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function getVisiblesApresDate($date, $orderBy = null) {
        $query = "SELECT * FROM ".ARTICLE_PRESSE_TABLE_NAME." WHERE ".ARTICLE_PRESSE_VISIBLE_SUR_SITE." = 1 AND ".ARTICLE_PRESSE_DATE." >= :".ARTICLE_PRESSE_DATE;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $params = array(
            ARTICLE_PRESSE_DATE => $date
        );
        $columns = array(
            ARTICLE_PRESSE_ID, 
            ARTICLE_PRESSE_DATE, 
            ARTICLE_PRESSE_TITRE, 
            ARTICLE_PRESSE_URL,
            ARTICLE_PRESSE_VISIBLE_SUR_SITE,
            ARTICLE_PRESSE_EMAIL_DERNIER_EDITEUR,
            ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function getNbTotalVisibles() {
        $query = "SELECT COUNT(DISTINCT ".ARTICLE_PRESSE_TABLE_NAME.".".ARTICLE_PRESSE_ID.") AS ".ARTICLE_PRESSE_NB_ARTICLES." ";
        $query .= "FROM ".ARTICLE_PRESSE_TABLE_NAME;
        $columns = array(
            ARTICLE_PRESSE_NB_ARTICLES
        );
        return $this->executeGetUniqueRequest($query, null, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".ARTICLE_PRESSE_TABLE_NAME." (";
        $query .= ARTICLE_PRESSE_DATE.", ";
        $query .= ARTICLE_PRESSE_TITRE.", ";
        $query .= ARTICLE_PRESSE_URL.", ";
        $query .= ARTICLE_PRESSE_VISIBLE_SUR_SITE.", ";
        $query .= ARTICLE_PRESSE_EMAIL_DERNIER_EDITEUR.", ";
        $query .= ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID.") VALUES (";
        $query .= ":".ARTICLE_PRESSE_DATE.", ";
        $query .= ":".ARTICLE_PRESSE_TITRE.", ";
        $query .= ":".ARTICLE_PRESSE_URL.", ";
        $query .= ":".ARTICLE_PRESSE_VISIBLE_SUR_SITE.", ";
        $query .= ":".ARTICLE_PRESSE_EMAIL_DERNIER_EDITEUR.", ";
        $query .= ":".ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID.")";
        $params = array(
            ARTICLE_PRESSE_DATE => $object[ARTICLE_PRESSE_DATE],
            ARTICLE_PRESSE_TITRE => $object[ARTICLE_PRESSE_TITRE],
            ARTICLE_PRESSE_URL => $object[ARTICLE_PRESSE_URL],
            ARTICLE_PRESSE_VISIBLE_SUR_SITE => $object[ARTICLE_PRESSE_VISIBLE_SUR_SITE],
            ARTICLE_PRESSE_EMAIL_DERNIER_EDITEUR => $object[ARTICLE_PRESSE_EMAIL_DERNIER_EDITEUR],
            ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID => $object[ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {
        $query = "UPDATE ".ARTICLE_PRESSE_TABLE_NAME." SET ";
        $query .= ARTICLE_PRESSE_DATE." = :".ARTICLE_PRESSE_DATE.", ";
        $query .= ARTICLE_PRESSE_TITRE." = :".ARTICLE_PRESSE_TITRE.", ";
        $query .= ARTICLE_PRESSE_URL." = :".ARTICLE_PRESSE_URL.", ";
        $query .= ARTICLE_PRESSE_VISIBLE_SUR_SITE." = :".ARTICLE_PRESSE_VISIBLE_SUR_SITE.", ";
        $query .= ARTICLE_PRESSE_EMAIL_DERNIER_EDITEUR." = :".ARTICLE_PRESSE_EMAIL_DERNIER_EDITEUR.", ";
        $query .= ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID." = :".ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID." WHERE ";
        $query .= ARTICLE_PRESSE_ID." = :".ARTICLE_PRESSE_ID;
        $params = array(
            ARTICLE_PRESSE_DATE => $object[ARTICLE_PRESSE_DATE],
            ARTICLE_PRESSE_TITRE => $object[ARTICLE_PRESSE_TITRE],
            ARTICLE_PRESSE_URL => $object[ARTICLE_PRESSE_URL],
            ARTICLE_PRESSE_VISIBLE_SUR_SITE => $object[ARTICLE_PRESSE_VISIBLE_SUR_SITE],
            ARTICLE_PRESSE_EMAIL_DERNIER_EDITEUR => $object[ARTICLE_PRESSE_EMAIL_DERNIER_EDITEUR],
            ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID => $object[ARTICLE_PRESSE_JOURNAL_ARTICLE_PRESSE_ID],
            ARTICLE_PRESSE_ID => $object[ARTICLE_PRESSE_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".ARTICLE_PRESSE_TABLE_NAME." WHERE ".ARTICLE_PRESSE_ID." = :".ARTICLE_PRESSE_ID;
        $params = array(
            ARTICLE_PRESSE_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>