<?php
require_once("core/php/lib/abstract-dao.php");

define(HOME_MESSAGE_TABLE_NAME, "hbcsj_home_message");
define(HOME_MESSAGE_ID, "id");
define(HOME_MESSAGE_TEXTE, "texte");
define(HOME_MESSAGE_CRITICITE_ID, "criticite_id");

define(HOME_MESSAGE_INFO_CRITICITE_ID, "1");
define(HOME_MESSAGE_WARNING_CRITICITE_ID, "2");
define(HOME_MESSAGE_DANGER_CRITICITE_ID, "3");

class HomeMessageDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(HOME_MESSAGE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".HOME_MESSAGE_TABLE_NAME." WHERE ".HOME_MESSAGE_ID." = :".HOME_MESSAGE_ID;
        $params = array(
            HOME_MESSAGE_ID => $id
        );
        $columns = array(HOME_MESSAGE_ID, HOME_MESSAGE_TEXTE, HOME_MESSAGE_CRITICITE_ID);
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".HOME_MESSAGE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(HOME_MESSAGE_ID, HOME_MESSAGE_TEXTE, HOME_MESSAGE_CRITICITE_ID);
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".HOME_MESSAGE_TABLE_NAME." (";
        $query .= HOME_MESSAGE_TEXTE.", ";
        $query .= HOME_MESSAGE_CRITICITE_ID.") VALUES (";
        $query .= ":".HOME_MESSAGE_TEXTE.", ";
        $query .= ":".HOME_MESSAGE_CRITICITE_ID.")";
        $params = array(
            HOME_MESSAGE_TEXTE => $object[HOME_MESSAGE_TEXTE],
            HOME_MESSAGE_CRITICITE_ID => $object[HOME_MESSAGE_CRITICITE_ID]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {
        $query = "UPDATE ".HOME_MESSAGE_TABLE_NAME." SET ";
        $query .= HOME_MESSAGE_TEXTE." = :".HOME_MESSAGE_TEXTE.", ";
        $query .= HOME_MESSAGE_CRITICITE_ID." = :".HOME_MESSAGE_CRITICITE_ID." WHERE ";
        $query .= HOME_MESSAGE_ID." = :".HOME_MESSAGE_ID;
        $params = array(
            HOME_MESSAGE_TEXTE => $object[HOME_MESSAGE_TEXTE],
            HOME_MESSAGE_CRITICITE_ID => $object[HOME_MESSAGE_CRITICITE_ID],
            HOME_MESSAGE_ID => $object[HOME_MESSAGE_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".HOME_MESSAGE_TABLE_NAME." WHERE ".HOME_MESSAGE_ID." = :".HOME_MESSAGE_ID;
        $params = array(
            HOME_MESSAGE_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>