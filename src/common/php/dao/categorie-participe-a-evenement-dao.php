<?php
require_once("core/php/lib/abstract-dao.php");

define(CATEGORIE_PARTICIPE_A_EVENEMENT_TABLE_NAME, "hbcsj_categorie_participe_a_evenement");
define(CATEGORIE_PARTICIPE_A_EVENEMENT_EVENEMENT_ID, "evenement_id");
define(CATEGORIE_PARTICIPE_A_EVENEMENT_CATEGORIE_ID, "categorie_id");

class CategorieParticipeAEvenementDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(CATEGORIE_PARTICIPE_A_EVENEMENT_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".CATEGORIE_PARTICIPE_A_EVENEMENT_TABLE_NAME." WHERE ".CATEGORIE_PARTICIPE_A_EVENEMENT_EVENEMENT_ID." = :".CATEGORIE_PARTICIPE_A_EVENEMENT_EVENEMENT_ID." AND ".CATEGORIE_PARTICIPE_A_EVENEMENT_CATEGORIE_ID." = :".CATEGORIE_PARTICIPE_A_EVENEMENT_CATEGORIE_ID;
        $params = array(
            CATEGORIE_PARTICIPE_A_EVENEMENT_EVENEMENT_ID => $id[CATEGORIE_PARTICIPE_A_EVENEMENT_EVENEMENT_ID],
            CATEGORIE_PARTICIPE_A_EVENEMENT_CATEGORIE_ID => $id[CATEGORIE_PARTICIPE_A_EVENEMENT_CATEGORIE_ID]
        );
        $columns = array(
            CATEGORIE_PARTICIPE_A_EVENEMENT_EVENEMENT_ID,
            CATEGORIE_PARTICIPE_A_EVENEMENT_CATEGORIE_ID
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }

    public function getByEvenementId($evenementId) {
        $query = "SELECT * FROM ".CATEGORIE_PARTICIPE_A_EVENEMENT_TABLE_NAME." WHERE ".CATEGORIE_PARTICIPE_A_EVENEMENT_EVENEMENT_ID." = :".CATEGORIE_PARTICIPE_A_EVENEMENT_EVENEMENT_ID;
        $params = array(
            CATEGORIE_PARTICIPE_A_EVENEMENT_EVENEMENT_ID => $evenementId
        );
        $columns = array(
            CATEGORIE_PARTICIPE_A_EVENEMENT_EVENEMENT_ID,
            CATEGORIE_PARTICIPE_A_EVENEMENT_CATEGORIE_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".CATEGORIE_PARTICIPE_A_EVENEMENT_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            CATEGORIE_PARTICIPE_A_EVENEMENT_EVENEMENT_ID,
            CATEGORIE_PARTICIPE_A_EVENEMENT_CATEGORIE_ID
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".CATEGORIE_PARTICIPE_A_EVENEMENT_TABLE_NAME." (";
        $query .= CATEGORIE_PARTICIPE_A_EVENEMENT_EVENEMENT_ID.", ";
        $query .= CATEGORIE_PARTICIPE_A_EVENEMENT_CATEGORIE_ID.") VALUES (";
        $query .= ":".CATEGORIE_PARTICIPE_A_EVENEMENT_EVENEMENT_ID.", ";
        $query .= ":".CATEGORIE_PARTICIPE_A_EVENEMENT_CATEGORIE_ID.")";
        $params = array(
            CATEGORIE_PARTICIPE_A_EVENEMENT_EVENEMENT_ID => $object[CATEGORIE_PARTICIPE_A_EVENEMENT_EVENEMENT_ID],
            CATEGORIE_PARTICIPE_A_EVENEMENT_CATEGORIE_ID => $object[CATEGORIE_PARTICIPE_A_EVENEMENT_CATEGORIE_ID]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {}
	
	public function delete($id) {
        $query = "DELETE FROM ".CATEGORIE_PARTICIPE_A_EVENEMENT_TABLE_NAME." WHERE ".CATEGORIE_PARTICIPE_A_EVENEMENT_EVENEMENT_ID." = :".CATEGORIE_PARTICIPE_A_EVENEMENT_EVENEMENT_ID." AND ".CATEGORIE_PARTICIPE_A_EVENEMENT_CATEGORIE_ID." = :".CATEGORIE_PARTICIPE_A_EVENEMENT_CATEGORIE_ID;
        $params = array(
            CATEGORIE_PARTICIPE_A_EVENEMENT_EVENEMENT_ID => $id[CATEGORIE_PARTICIPE_A_EVENEMENT_EVENEMENT_ID],
            CATEGORIE_PARTICIPE_A_EVENEMENT_CATEGORIE_ID => $id[CATEGORIE_PARTICIPE_A_EVENEMENT_CATEGORIE_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function deleteByEvenementId($evenementId) {
        $query = "DELETE FROM ".CATEGORIE_PARTICIPE_A_EVENEMENT_TABLE_NAME." WHERE ".CATEGORIE_PARTICIPE_A_EVENEMENT_EVENEMENT_ID." = :".CATEGORIE_PARTICIPE_A_EVENEMENT_EVENEMENT_ID;
        $params = array(
            CATEGORIE_PARTICIPE_A_EVENEMENT_EVENEMENT_ID => $evenementId
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>