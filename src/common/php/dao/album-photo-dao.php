<?php
require_once("core/php/lib/abstract-dao.php");

define(ALBUM_PHOTO_TABLE_NAME, "hbcsj_album_photo");
define(ALBUM_PHOTO_ID, "id");
define(ALBUM_PHOTO_NOM, "nom");
define(ALBUM_PHOTO_URL, "url");
define(ALBUM_PHOTO_VISIBLE_SUR_SITE, "visible_sur_site");
define(ALBUM_PHOTO_EMAIL_DERNIER_EDITEUR, "email_dernier_editeur");

define(ALBUM_PHOTO_NB_ALBUMS, "nb_albums_photos");

class AlbumPhotoDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(ALBUM_PHOTO_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".ALBUM_PHOTO_TABLE_NAME." WHERE ".ALBUM_PHOTO_ID." = :".ALBUM_PHOTO_ID;
        $params = array(
            ALBUM_PHOTO_ID => $id
        );
        $columns = array(
            ALBUM_PHOTO_ID, 
            ALBUM_PHOTO_NOM, 
            ALBUM_PHOTO_URL,
            ALBUM_PHOTO_VISIBLE_SUR_SITE,
            ALBUM_PHOTO_EMAIL_DERNIER_EDITEUR
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".ALBUM_PHOTO_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            ALBUM_PHOTO_ID, 
            ALBUM_PHOTO_NOM, 
            ALBUM_PHOTO_URL,
            ALBUM_PHOTO_VISIBLE_SUR_SITE,
            ALBUM_PHOTO_EMAIL_DERNIER_EDITEUR
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function getNbTotalVisibles() {
        $query = "SELECT COUNT(DISTINCT ".ALBUM_PHOTO_TABLE_NAME.".".ALBUM_PHOTO_ID.") AS ".ALBUM_PHOTO_NB_ALBUMS." ";
        $query .= "FROM ".ALBUM_PHOTO_TABLE_NAME;
        $columns = array(
            ALBUM_PHOTO_NB_ALBUMS
        );
        return $this->executeGetUniqueRequest($query, null, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".ALBUM_PHOTO_TABLE_NAME." (";
        $query .= ALBUM_PHOTO_NOM.", ";
        $query .= ALBUM_PHOTO_URL.", ";
        $query .= ALBUM_PHOTO_VISIBLE_SUR_SITE.", ";
        $query .= ALBUM_PHOTO_EMAIL_DERNIER_EDITEUR.") VALUES (";
        $query .= ":".ALBUM_PHOTO_NOM.", ";
        $query .= ":".ALBUM_PHOTO_URL.", ";
        $query .= ":".ALBUM_PHOTO_VISIBLE_SUR_SITE.", ";
        $query .= ":".ALBUM_PHOTO_EMAIL_DERNIER_EDITEUR.")";
        $params = array(
            ALBUM_PHOTO_NOM => $object[ALBUM_PHOTO_NOM],
            ALBUM_PHOTO_URL => $object[ALBUM_PHOTO_URL],
            ALBUM_PHOTO_VISIBLE_SUR_SITE => $object[ALBUM_PHOTO_VISIBLE_SUR_SITE],
            ALBUM_PHOTO_EMAIL_DERNIER_EDITEUR => $object[ALBUM_PHOTO_EMAIL_DERNIER_EDITEUR]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {
        $query = "UPDATE ".ALBUM_PHOTO_TABLE_NAME." SET ";
        $query .= ALBUM_PHOTO_NOM." = :".ALBUM_PHOTO_NOM.", ";
        $query .= ALBUM_PHOTO_URL." = :".ALBUM_PHOTO_URL.", ";
        $query .= ALBUM_PHOTO_VISIBLE_SUR_SITE." = :".ALBUM_PHOTO_VISIBLE_SUR_SITE.", ";
        $query .= ALBUM_PHOTO_EMAIL_DERNIER_EDITEUR." = :".ALBUM_PHOTO_EMAIL_DERNIER_EDITEUR." WHERE ";
        $query .= ALBUM_PHOTO_ID." = :".ALBUM_PHOTO_ID;
        $params = array(
            ALBUM_PHOTO_NOM => $object[ALBUM_PHOTO_NOM],
            ALBUM_PHOTO_URL => $object[ALBUM_PHOTO_URL],
            ALBUM_PHOTO_VISIBLE_SUR_SITE => $object[ALBUM_PHOTO_VISIBLE_SUR_SITE],
            ALBUM_PHOTO_EMAIL_DERNIER_EDITEUR => $object[ALBUM_PHOTO_EMAIL_DERNIER_EDITEUR],
            ALBUM_PHOTO_ID => $object[ALBUM_PHOTO_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".ALBUM_PHOTO_TABLE_NAME." WHERE ".ALBUM_PHOTO_ID." = :".ALBUM_PHOTO_ID;
        $params = array(
            ALBUM_PHOTO_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>