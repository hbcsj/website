<?php
require_once("core/php/lib/abstract-dao.php");

define(COMMANDE_ITEM_BILLETTERIE_TABLE_NAME, "hbcsj_commande_item_billetterie");
define(COMMANDE_ITEM_BILLETTERIE_ID, "id");
define(COMMANDE_ITEM_BILLETTERIE_QUANTITE, "quantite");
define(COMMANDE_ITEM_BILLETTERIE_PRIX, "prix");
define(COMMANDE_ITEM_BILLETTERIE_DONNE, "donne");
define(COMMANDE_ITEM_BILLETTERIE_MATCH_BILLETTERIE_ID, "match_billetterie_id");
define(COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID, "pack_billetterie_id");
define(COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID, "commande_id");

define(COMMANDE_ITEM_BILLETTERIE_NB_PLACES, "nb_places");

class CommandeItemBilletterieDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(COMMANDE_ITEM_BILLETTERIE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".COMMANDE_ITEM_BILLETTERIE_TABLE_NAME." WHERE ".COMMANDE_ITEM_BILLETTERIE_ID." = :".COMMANDE_ITEM_BILLETTERIE_ID;
        $params = array(
            COMMANDE_ITEM_BILLETTERIE_ID => $id
        );
        $columns = array(
            COMMANDE_ITEM_BILLETTERIE_ID, 
            COMMANDE_ITEM_BILLETTERIE_QUANTITE, 
            COMMANDE_ITEM_BILLETTERIE_PRIX,
            COMMANDE_ITEM_BILLETTERIE_DONNE,
            COMMANDE_ITEM_BILLETTERIE_MATCH_BILLETTERIE_ID,
            COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID,
            COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".COMMANDE_ITEM_BILLETTERIE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            COMMANDE_ITEM_BILLETTERIE_ID, 
            COMMANDE_ITEM_BILLETTERIE_QUANTITE, 
            COMMANDE_ITEM_BILLETTERIE_PRIX,
            COMMANDE_ITEM_BILLETTERIE_DONNE,
            COMMANDE_ITEM_BILLETTERIE_MATCH_BILLETTERIE_ID,
            COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID,
            COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID
        );
        return $this->executeGetRequest($query, null, $columns);
    }

	public function getAllByParams($matchId, $packId, $reference, $nom, $prenom, $dateDebut, $dateFin, $donne, $orderBy = null) {
        require_once("common/php/dao/commande-dao.php");

        $query = "SELECT DISTINCT ".COMMANDE_ITEM_BILLETTERIE_TABLE_NAME.".* FROM ".COMMANDE_ITEM_BILLETTERIE_TABLE_NAME.", ".COMMANDE_TABLE_NAME." ";

        $queryParamsArray = array();
        $params = array();
        if ($packId != null && $packId != "") {
            $queryParamsArray[] = COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID." = :".COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID;
            $params[COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID] = $packId;
        }
        if ($matchId != null && $matchId != "") {
            $queryParamsArray[] = COMMANDE_ITEM_BILLETTERIE_MATCH_BILLETTERIE_ID." = :".COMMANDE_ITEM_BILLETTERIE_MATCH_BILLETTERIE_ID;
            $params[COMMANDE_ITEM_BILLETTERIE_MATCH_BILLETTERIE_ID] = $matchId;
        }
        if ($reference != null && $reference != "") {
            $queryParamsArray[] = COMMANDE_REFERENCE." LIKE :".COMMANDE_REFERENCE;
            $params[COMMANDE_REFERENCE] = "%".$reference."%";
        }
        if ($nom != null && $nom != "") {
            $queryParamsArray[] = COMMANDE_NOM." LIKE :".COMMANDE_NOM;
            $params[COMMANDE_NOM] = "%".$nom."%";
        }
        if ($prenom != null && $prenom != "") {
            $queryParamsArray[] = COMMANDE_PRENOM." LIKE :".COMMANDE_PRENOM;
            $params[COMMANDE_PRENOM] = "%".$prenom."%";
        }
        if ($dateDebut != null && $dateDebut != "") {
            $queryParamsArray[] = COMMANDE_DATE_HEURE." >= :".COMMANDE_DATE_HEURE_DEBUT;
            $params[COMMANDE_DATE_HEURE_DEBUT] = $dateDebut;
        }
        if ($dateFin != null && $dateFin != "") {
            $queryParamsArray[] = COMMANDE_DATE_HEURE." <= :".COMMANDE_DATE_HEURE_FIN;
            $params[COMMANDE_DATE_HEURE_FIN] = $dateFin;
        }
        if ($donne != null && $donne != "") {
            if ($donne == 1) {
                $queryParamsArray[] = "(".COMMANDE_ITEM_BILLETTERIE_DONNE." IS NOT NULL AND ".COMMANDE_ITEM_BILLETTERIE_DONNE." != '')";
            } else if ($donne == 0) {
                $queryParamsArray[] = "(".COMMANDE_ITEM_BILLETTERIE_DONNE." IS NULL OR ".COMMANDE_ITEM_BILLETTERIE_DONNE." = '')";
            }
        }
        
        $query .= " WHERE ".COMMANDE_ITEM_BILLETTERIE_TABLE_NAME.".".COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID." = ".COMMANDE_TABLE_NAME.".".COMMANDE_ID." ";
        
        if (sizeof($queryParamsArray) > 0) {
            $query .= "AND ".implode(" AND ", $queryParamsArray);
        }

        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            COMMANDE_ITEM_BILLETTERIE_ID, 
            COMMANDE_ITEM_BILLETTERIE_QUANTITE, 
            COMMANDE_ITEM_BILLETTERIE_PRIX,
            COMMANDE_ITEM_BILLETTERIE_DONNE,
            COMMANDE_ITEM_BILLETTERIE_MATCH_BILLETTERIE_ID,
            COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID,
            COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }

    public function getByCommandeId($commandeId) {
        $query = "SELECT * FROM ".COMMANDE_ITEM_BILLETTERIE_TABLE_NAME." WHERE ".COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID." = :".COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID;
        $params = array(
            COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID => $commandeId
        );
        $columns = array(
            COMMANDE_ITEM_BILLETTERIE_ID, 
            COMMANDE_ITEM_BILLETTERIE_QUANTITE, 
            COMMANDE_ITEM_BILLETTERIE_PRIX,
            COMMANDE_ITEM_BILLETTERIE_DONNE,
            COMMANDE_ITEM_BILLETTERIE_MATCH_BILLETTERIE_ID,
            COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID,
            COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }

    public function getByMatchBilletterieId($matchBilletterieId) {
        $query = "SELECT * FROM ".COMMANDE_ITEM_BILLETTERIE_TABLE_NAME." WHERE ".COMMANDE_ITEM_BILLETTERIE_MATCH_BILLETTERIE_ID." = :".COMMANDE_ITEM_BILLETTERIE_MATCH_BILLETTERIE_ID;
        $params = array(
            COMMANDE_ITEM_BILLETTERIE_MATCH_BILLETTERIE_ID => $matchBilletterieId
        );
        $columns = array(
            COMMANDE_ITEM_BILLETTERIE_ID, 
            COMMANDE_ITEM_BILLETTERIE_QUANTITE, 
            COMMANDE_ITEM_BILLETTERIE_PRIX,
            COMMANDE_ITEM_BILLETTERIE_DONNE,
            COMMANDE_ITEM_BILLETTERIE_MATCH_BILLETTERIE_ID,
            COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID,
            COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }

    public function getByPackBilletterieId($packBilletterieId) {
        $query = "SELECT * FROM ".COMMANDE_ITEM_BILLETTERIE_TABLE_NAME." WHERE ".COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID." = :".COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID;
        $params = array(
            COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID => $packBilletterieId
        );
        $columns = array(
            COMMANDE_ITEM_BILLETTERIE_ID, 
            COMMANDE_ITEM_BILLETTERIE_QUANTITE, 
            COMMANDE_ITEM_BILLETTERIE_PRIX,
            COMMANDE_ITEM_BILLETTERIE_DONNE,
            COMMANDE_ITEM_BILLETTERIE_MATCH_BILLETTERIE_ID,
            COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID,
            COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }

    public function getNbTotalPlacesByPackId($packBilletterieId) {
        $query = "SELECT SUM(".COMMANDE_ITEM_BILLETTERIE_TABLE_NAME.".".COMMANDE_ITEM_BILLETTERIE_QUANTITE.") AS ".COMMANDE_ITEM_BILLETTERIE_NB_PLACES." ";
        $query .= "FROM ".COMMANDE_ITEM_BILLETTERIE_TABLE_NAME." ";
        $query .= "WHERE ".COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID." = :".COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID;
        $params = array(
            COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID => $packBilletterieId
        );
        $columns = array(
            COMMANDE_ITEM_BILLETTERIE_NB_PLACES
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }

    public function getNbTotalPlacesAfterDate($date) {
        require_once("common/php/dao/commande-dao.php");

        $query = "SELECT SUM(".COMMANDE_ITEM_BILLETTERIE_TABLE_NAME.".".COMMANDE_ITEM_BILLETTERIE_QUANTITE.") AS ".COMMANDE_ITEM_BILLETTERIE_NB_PLACES." ";
        $query .= "FROM ".COMMANDE_ITEM_BILLETTERIE_TABLE_NAME.", ".COMMANDE_TABLE_NAME." ";
        $query .= "WHERE ".COMMANDE_ITEM_BILLETTERIE_TABLE_NAME.".".COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID." = ".COMMANDE_TABLE_NAME.".".COMMANDE_ID." ";
        $query .= "AND ".COMMANDE_DATE_HEURE." >= :".COMMANDE_DATE_HEURE_DEBUT;
        $params = array(
            COMMANDE_DATE_HEURE_DEBUT => $date
        );
        $columns = array(
            COMMANDE_ITEM_BILLETTERIE_NB_PLACES
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }

    public function getPersonnesCommandes() {
        require_once("common/php/dao/commande-dao.php");

        $query = "SELECT DISTINCT ".COMMANDE_TABLE_NAME.".".COMMANDE_NOM.", ".COMMANDE_TABLE_NAME.".".COMMANDE_PRENOM." ";
        $query .= "FROM ".COMMANDE_ITEM_BILLETTERIE_TABLE_NAME.", ".COMMANDE_TABLE_NAME." ";
        $query .= "WHERE ".COMMANDE_ITEM_BILLETTERIE_TABLE_NAME.".".COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID." = ".COMMANDE_TABLE_NAME.".".COMMANDE_ID." ";
        $query .= " ORDER BY ".COMMANDE_TABLE_NAME.".".COMMANDE_NOM.", ".COMMANDE_TABLE_NAME.".".COMMANDE_PRENOM;
        $columns = array(
            COMMANDE_NOM,
            COMMANDE_PRENOM
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".COMMANDE_ITEM_BILLETTERIE_TABLE_NAME." (";
        $query .= COMMANDE_ITEM_BILLETTERIE_QUANTITE.", ";
        $query .= COMMANDE_ITEM_BILLETTERIE_PRIX.", ";
        $query .= COMMANDE_ITEM_BILLETTERIE_DONNE.", ";
        $query .= COMMANDE_ITEM_BILLETTERIE_MATCH_BILLETTERIE_ID.", ";
        $query .= COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID.", ";
        $query .= COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID.") VALUES (";
        $query .= ":".COMMANDE_ITEM_BILLETTERIE_QUANTITE.", ";
        $query .= ":".COMMANDE_ITEM_BILLETTERIE_PRIX.", ";
        $query .= ":".COMMANDE_ITEM_BILLETTERIE_DONNE.", ";
        $query .= ":".COMMANDE_ITEM_BILLETTERIE_MATCH_BILLETTERIE_ID.", ";
        $query .= ":".COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID.", ";
        $query .= ":".COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID.")";
        $params = array(
            COMMANDE_ITEM_BILLETTERIE_QUANTITE => $object[COMMANDE_ITEM_BILLETTERIE_QUANTITE],
            COMMANDE_ITEM_BILLETTERIE_PRIX => $object[COMMANDE_ITEM_BILLETTERIE_PRIX],
            COMMANDE_ITEM_BILLETTERIE_DONNE => $object[COMMANDE_ITEM_BILLETTERIE_DONNE],
            COMMANDE_ITEM_BILLETTERIE_MATCH_BILLETTERIE_ID => $object[COMMANDE_ITEM_BILLETTERIE_MATCH_BILLETTERIE_ID],
            COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID => $object[COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID],
            COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID => $object[COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {
        $query = "UPDATE ".COMMANDE_ITEM_BILLETTERIE_TABLE_NAME." SET ";
        $query .= COMMANDE_ITEM_BILLETTERIE_QUANTITE." = :".COMMANDE_ITEM_BILLETTERIE_QUANTITE.", ";
        $query .= COMMANDE_ITEM_BILLETTERIE_PRIX." = :".COMMANDE_ITEM_BILLETTERIE_PRIX.", ";
        $query .= COMMANDE_ITEM_BILLETTERIE_DONNE." = :".COMMANDE_ITEM_BILLETTERIE_DONNE.", ";
        $query .= COMMANDE_ITEM_BILLETTERIE_MATCH_BILLETTERIE_ID." = :".COMMANDE_ITEM_BILLETTERIE_MATCH_BILLETTERIE_ID.", ";
        $query .= COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID." = :".COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID.", ";
        $query .= COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID." = :".COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID." WHERE ";
        $query .= COMMANDE_ITEM_BILLETTERIE_ID." = :".COMMANDE_ITEM_BILLETTERIE_ID;
        $params = array(
            COMMANDE_ITEM_BILLETTERIE_QUANTITE => $object[COMMANDE_ITEM_BILLETTERIE_QUANTITE],
            COMMANDE_ITEM_BILLETTERIE_PRIX => $object[COMMANDE_ITEM_BILLETTERIE_PRIX],
            COMMANDE_ITEM_BILLETTERIE_DONNE => $object[COMMANDE_ITEM_BILLETTERIE_DONNE],
            COMMANDE_ITEM_BILLETTERIE_MATCH_BILLETTERIE_ID => $object[COMMANDE_ITEM_BILLETTERIE_MATCH_BILLETTERIE_ID],
            COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID => $object[COMMANDE_ITEM_BILLETTERIE_PACK_BILLETTERIE_ID],
            COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID => $object[COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID],
            COMMANDE_ITEM_BILLETTERIE_ID => $object[COMMANDE_ITEM_BILLETTERIE_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".COMMANDE_ITEM_BILLETTERIE_TABLE_NAME." WHERE ".COMMANDE_ITEM_BILLETTERIE_ID." = :".COMMANDE_ITEM_BILLETTERIE_ID;
        $params = array(
            COMMANDE_ITEM_BILLETTERIE_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function deleteByCommandeId($commandeId) {
        $query = "DELETE FROM ".COMMANDE_ITEM_BILLETTERIE_TABLE_NAME." WHERE ".COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID." = :".COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID;
        $params = array(
            COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID => $commandeId
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>