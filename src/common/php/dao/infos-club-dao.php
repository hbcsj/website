<?php
require_once("core/php/lib/abstract-dao.php");

define(INFOS_CLUB_TABLE_NAME, "hbcsj_infos_club");
define(INFOS_CLUB_ID, "id");
define(INFOS_CLUB_NOM, "nom");
define(INFOS_CLUB_NOM_SMALL, "nom_small");
define(INFOS_CLUB_NOM_XS, "nom_xs");
define(INFOS_CLUB_ADRESSE, "adresse");
define(INFOS_CLUB_CODE_POSTAL, "code_postal");
define(INFOS_CLUB_VILLE, "ville");
define(INFOS_CLUB_EMAIL, "email");
define(INFOS_CLUB_NOM_RIB, "nom_rib");
define(INFOS_CLUB_IBAN_RIB, "iban_rib");
define(INFOS_CLUB_BIC_SWIFT_RIB, "bic_swift_rib");

class InfosClubDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(INFOS_CLUB_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {}

    public function getSingleton() {
        $singleton = null;

        $list = $this->getAll();
        if (sizeof($list) > 0) {
            $singleton = $list[0];
        }

        return $singleton;
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".INFOS_CLUB_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            INFOS_CLUB_ID, 
            INFOS_CLUB_NOM, 
            INFOS_CLUB_NOM_SMALL, 
            INFOS_CLUB_NOM_XS, 
            INFOS_CLUB_ADRESSE, 
            INFOS_CLUB_CODE_POSTAL,
            INFOS_CLUB_VILLE,
            INFOS_CLUB_EMAIL,
            INFOS_CLUB_NOM_RIB,
            INFOS_CLUB_IBAN_RIB,
            INFOS_CLUB_BIC_SWIFT_RIB
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {}
	
	public function update($object) {}
	
	public function delete($id) {}
}

?>