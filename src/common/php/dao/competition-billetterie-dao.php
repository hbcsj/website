<?php
require_once("core/php/lib/abstract-dao.php");

define(COMPETITION_BILLETTERIE_TABLE_NAME, "hbcsj_competition_billetterie");
define(COMPETITION_BILLETTERIE_ID, "id");
define(COMPETITION_BILLETTERIE_NOM, "nom");

class CompetitionBilletterieDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(COMPETITION_BILLETTERIE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".COMPETITION_BILLETTERIE_TABLE_NAME." WHERE ".COMPETITION_BILLETTERIE_ID." = :".COMPETITION_BILLETTERIE_ID;
        $params = array(
            COMPETITION_BILLETTERIE_ID => $id
        );
        $columns = array(
            COMPETITION_BILLETTERIE_ID, 
            COMPETITION_BILLETTERIE_NOM
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".COMPETITION_BILLETTERIE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            COMPETITION_BILLETTERIE_ID, 
            COMPETITION_BILLETTERIE_NOM
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {}
	
	public function update($object) {}
	
	public function delete($id) {}
}

?>