<?php
require_once("core/php/lib/abstract-dao.php");

define(EVENEMENT_TABLE_NAME, "hbcsj_evenement");
define(EVENEMENT_ID, "id");
define(EVENEMENT_CODE, "code_rencontre");
define(EVENEMENT_NOM, "nom");
define(EVENEMENT_DATE_HEURE, "date_heure");
define(EVENEMENT_NUM_EQUIPE, "num_equipe");
define(EVENEMENT_ADVERSAIRE, "adversaire");
define(EVENEMENT_ADRESSE, "adresse");
define(EVENEMENT_CODE_POSTAL, "code_postal");
define(EVENEMENT_VILLE, "ville");
define(EVENEMENT_LATITUDE, "latitude");
define(EVENEMENT_LONGITUDE, "longitude");
define(EVENEMENT_A_DOMICILE, "a_domicile");
define(EVENEMENT_SCORE_HBCSJ, "score_hbcsj");
define(EVENEMENT_SCORE_ADVERSAIRE, "score_adversaire");
define(EVENEMENT_VISIBLE_SUR_SITE, "visible_sur_site");
define(EVENEMENT_EMAIL_DERNIER_EDITEUR, "email_dernier_editeur");
define(EVENEMENT_GYMNASE_ID, "gymnase_id");
define(EVENEMENT_COMPETITION_ID, "competition_id");
define(EVENEMENT_TYPE_EVENEMENT_ID, "type_evenement_id");

define(EVENEMENT_NB_EVENEMENTS, "nb_evenements");
define(EVENEMENT_MAX_NUM_EQUIPE, "max_num_equipe");
define(EVENEMENT_DATE_HEURE_DEBUT, "date_heure_debut");
define(EVENEMENT_DATE_HEURE_FIN, "date_heure_fin");

define(EVENEMENT_MATCH_TYPE_ID, "1");
define(EVENEMENT_TOURNOI_TYPE_ID, "2");
define(EVENEMENT_ENTRAINEMENT_TYPE_ID, "3");
define(EVENEMENT_SHOOTING_PHOTO_TYPE_ID, "4");
define(EVENEMENT_PERMANENCE_INSCRIPTION_TYPE_ID, "5");
define(EVENEMENT_COMMANDE_BOUTIQUE_TYPE_ID, "6");
define(EVENEMENT_COMMANDE_MATCH_FENIX_TYPE_ID, "7");
define(EVENEMENT_EVENEMENT_CLUB_TYPE_ID, "8");
define(EVENEMENT_AUTRE_TYPE_ID, "9");

class EvenementDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(EVENEMENT_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".EVENEMENT_TABLE_NAME." WHERE ".EVENEMENT_ID." = :".EVENEMENT_ID;
        $params = array(
            EVENEMENT_ID => $id
        );
        $columns = array(
            EVENEMENT_ID, 
            EVENEMENT_CODE,
            EVENEMENT_NOM, 
            EVENEMENT_DATE_HEURE, 
            EVENEMENT_NUM_EQUIPE, 
            EVENEMENT_ADVERSAIRE, 
            EVENEMENT_ADRESSE, 
            EVENEMENT_CODE_POSTAL, 
            EVENEMENT_VILLE, 
            EVENEMENT_LATITUDE, 
            EVENEMENT_LONGITUDE, 
            EVENEMENT_A_DOMICILE,
            EVENEMENT_SCORE_HBCSJ,
            EVENEMENT_SCORE_ADVERSAIRE,
            EVENEMENT_VISIBLE_SUR_SITE,
            EVENEMENT_EMAIL_DERNIER_EDITEUR,
            EVENEMENT_GYMNASE_ID,
            EVENEMENT_COMPETITION_ID,
            EVENEMENT_TYPE_EVENEMENT_ID
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".EVENEMENT_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            EVENEMENT_ID, 
            EVENEMENT_CODE,
            EVENEMENT_NOM, 
            EVENEMENT_DATE_HEURE, 
            EVENEMENT_NUM_EQUIPE, 
            EVENEMENT_ADVERSAIRE, 
            EVENEMENT_ADRESSE, 
            EVENEMENT_CODE_POSTAL, 
            EVENEMENT_VILLE, 
            EVENEMENT_LATITUDE, 
            EVENEMENT_LONGITUDE, 
            EVENEMENT_A_DOMICILE,
            EVENEMENT_SCORE_HBCSJ,
            EVENEMENT_SCORE_ADVERSAIRE,
            EVENEMENT_VISIBLE_SUR_SITE,
            EVENEMENT_EMAIL_DERNIER_EDITEUR,
            EVENEMENT_GYMNASE_ID,
            EVENEMENT_COMPETITION_ID,
            EVENEMENT_TYPE_EVENEMENT_ID
        );
        return $this->executeGetRequest($query, null, $columns);
    }

	public function getAllByParams($dateDebut, $dateFin, $categorieId, $numEquipe, $type, $competitionId, $adversaire, $ville, $aDomicile, $visible, $orderBy = null, $limit = null) {
        require_once("common/php/dao/categorie-participe-a-evenement-dao.php");

        $query = "SELECT DISTINCT ".EVENEMENT_TABLE_NAME.".* FROM ".EVENEMENT_TABLE_NAME." ";
        if ($categorieId != null && $categorieId != "") {
            $query .= ", ".CATEGORIE_PARTICIPE_A_EVENEMENT_TABLE_NAME." ";
        }

        $queryParamsArray = array();
        $params = array();
        if ($dateDebut != null && $dateDebut != "") {
            $queryParamsArray[] = EVENEMENT_DATE_HEURE." >= :".EVENEMENT_DATE_HEURE_DEBUT;
            $params[EVENEMENT_DATE_HEURE_DEBUT] = $dateDebut;
        }
        if ($dateFin != null && $dateFin != "") {
            $queryParamsArray[] = EVENEMENT_DATE_HEURE." <= :".EVENEMENT_DATE_HEURE_FIN;
            $params[EVENEMENT_DATE_HEURE_FIN] = $dateFin;
        }
        if ($categorieId != null && $categorieId != "") {
            $queryParamsArray[] = CATEGORIE_PARTICIPE_A_EVENEMENT_CATEGORIE_ID." = :".CATEGORIE_PARTICIPE_A_EVENEMENT_CATEGORIE_ID;
            $params[CATEGORIE_PARTICIPE_A_EVENEMENT_CATEGORIE_ID] = $categorieId;
        }
        if ($numEquipe != null && $numEquipe != "") {
            $queryParamsArray[] = EVENEMENT_NUM_EQUIPE." = :".EVENEMENT_NUM_EQUIPE;
            $params[EVENEMENT_NUM_EQUIPE] = $numEquipe;
        }
        if ($type != null && $type != "") {
            $queryParamsArray[] = EVENEMENT_TYPE_EVENEMENT_ID." = :".EVENEMENT_TYPE_EVENEMENT_ID;
            $params[EVENEMENT_TYPE_EVENEMENT_ID] = $type;
        }
        if ($competitionId != null && $competitionId != "") {
            $queryParamsArray[] = EVENEMENT_COMPETITION_ID." = :".EVENEMENT_COMPETITION_ID;
            $params[EVENEMENT_COMPETITION_ID] = $competitionId;
        }
        if ($adversaire != null && $adversaire != "") {
            $queryParamsArray[] = EVENEMENT_ADVERSAIRE." LIKE :".EVENEMENT_ADVERSAIRE;
            $params[EVENEMENT_ADVERSAIRE] = "%".$adversaire."%";
        }
        if ($ville != null && $ville != "") {
            $queryParamsArray[] = EVENEMENT_VILLE." LIKE :".EVENEMENT_VILLE;
            $params[EVENEMENT_VILLE] = "%".$ville."%";
        }
        if ($aDomicile != null && $aDomicile != "") {
            if ($aDomicile == 1) {
                $queryParamsArray[] = "(".EVENEMENT_A_DOMICILE." IS NOT NULL AND ".EVENEMENT_A_DOMICILE." != '')";
            } else if ($aDomicile == 0) {
                $queryParamsArray[] = "(".EVENEMENT_A_DOMICILE." IS NULL OR ".EVENEMENT_A_DOMICILE." = '')";
            }
        }
        if ($visible != null && $visible != "") {
            if ($visible == 1) {
                $queryParamsArray[] = "(".EVENEMENT_VISIBLE_SUR_SITE." IS NOT NULL AND ".EVENEMENT_VISIBLE_SUR_SITE." != '')";
            } else if ($visible == 0) {
                $queryParamsArray[] = "(".EVENEMENT_VISIBLE_SUR_SITE." IS NULL OR ".EVENEMENT_VISIBLE_SUR_SITE." = '')";
            }
        }
        if (sizeof($queryParamsArray) > 0) {
            $query .= " WHERE ";
            if ($categorieId != null && $categorieId != "") {
                $query .= EVENEMENT_TABLE_NAME.".".EVENEMENT_ID." = ".CATEGORIE_PARTICIPE_A_EVENEMENT_TABLE_NAME.".".CATEGORIE_PARTICIPE_A_EVENEMENT_EVENEMENT_ID." AND ";
            }
            $query .= implode(" AND ", $queryParamsArray);
        }
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        if ($limit != null) {
            $query .= " LIMIT ".$limit;
        }
        $columns = array(
            EVENEMENT_ID, 
            EVENEMENT_CODE,
            EVENEMENT_NOM, 
            EVENEMENT_DATE_HEURE, 
            EVENEMENT_NUM_EQUIPE, 
            EVENEMENT_ADVERSAIRE, 
            EVENEMENT_ADRESSE, 
            EVENEMENT_CODE_POSTAL, 
            EVENEMENT_VILLE, 
            EVENEMENT_LATITUDE, 
            EVENEMENT_LONGITUDE, 
            EVENEMENT_A_DOMICILE,
            EVENEMENT_SCORE_HBCSJ,
            EVENEMENT_SCORE_ADVERSAIRE,
            EVENEMENT_VISIBLE_SUR_SITE,
            EVENEMENT_EMAIL_DERNIER_EDITEUR,
            EVENEMENT_GYMNASE_ID,
            EVENEMENT_COMPETITION_ID,
            EVENEMENT_TYPE_EVENEMENT_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }

    public function getVisiblesAvantDateByTypeLimited($dateHeure, $type, $limit) {
        $query = "SELECT * FROM ".EVENEMENT_TABLE_NAME." WHERE ".EVENEMENT_VISIBLE_SUR_SITE." = 1 AND ".EVENEMENT_DATE_HEURE." < :".EVENEMENT_DATE_HEURE." AND ".EVENEMENT_TYPE_EVENEMENT_ID." = :".EVENEMENT_TYPE_EVENEMENT_ID." ORDER BY ".EVENEMENT_DATE_HEURE." DESC LIMIT ".$limit;
        $params = array(
            EVENEMENT_DATE_HEURE => $dateHeure,
            EVENEMENT_TYPE_EVENEMENT_ID => $type
        );
        $columns = array(
            EVENEMENT_ID, 
            EVENEMENT_CODE,
            EVENEMENT_NOM, 
            EVENEMENT_DATE_HEURE, 
            EVENEMENT_NUM_EQUIPE, 
            EVENEMENT_ADVERSAIRE, 
            EVENEMENT_ADRESSE, 
            EVENEMENT_CODE_POSTAL, 
            EVENEMENT_VILLE, 
            EVENEMENT_LATITUDE, 
            EVENEMENT_LONGITUDE, 
            EVENEMENT_A_DOMICILE,
            EVENEMENT_SCORE_HBCSJ,
            EVENEMENT_SCORE_ADVERSAIRE,
            EVENEMENT_VISIBLE_SUR_SITE,
            EVENEMENT_EMAIL_DERNIER_EDITEUR,
            EVENEMENT_GYMNASE_ID,
            EVENEMENT_COMPETITION_ID,
            EVENEMENT_TYPE_EVENEMENT_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }

    public function getVisiblesApresDateByTypeLimited($dateHeure, $type, $limit) {
        $query = "SELECT * FROM ".EVENEMENT_TABLE_NAME." WHERE ".EVENEMENT_VISIBLE_SUR_SITE." = 1 AND ".EVENEMENT_DATE_HEURE." >= :".EVENEMENT_DATE_HEURE." AND ".EVENEMENT_TYPE_EVENEMENT_ID." = :".EVENEMENT_TYPE_EVENEMENT_ID." ORDER BY ".EVENEMENT_DATE_HEURE." LIMIT ".$limit;
        $params = array(
            EVENEMENT_DATE_HEURE => $dateHeure,
            EVENEMENT_TYPE_EVENEMENT_ID => $type
        );
        $columns = array(
            EVENEMENT_ID, 
            EVENEMENT_CODE,
            EVENEMENT_NOM, 
            EVENEMENT_DATE_HEURE, 
            EVENEMENT_NUM_EQUIPE, 
            EVENEMENT_ADVERSAIRE, 
            EVENEMENT_ADRESSE, 
            EVENEMENT_CODE_POSTAL, 
            EVENEMENT_VILLE, 
            EVENEMENT_LATITUDE, 
            EVENEMENT_LONGITUDE, 
            EVENEMENT_A_DOMICILE,
            EVENEMENT_SCORE_HBCSJ,
            EVENEMENT_SCORE_ADVERSAIRE,
            EVENEMENT_VISIBLE_SUR_SITE,
            EVENEMENT_EMAIL_DERNIER_EDITEUR,
            EVENEMENT_GYMNASE_ID,
            EVENEMENT_COMPETITION_ID,
            EVENEMENT_TYPE_EVENEMENT_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }

    public function getVisiblesEntreDatesByTypeLimited($dateHeureDebut, $dateHeureFin, $type, $limit) {
        $query = "SELECT * FROM ".EVENEMENT_TABLE_NAME." WHERE ".EVENEMENT_VISIBLE_SUR_SITE." = 1 AND ".EVENEMENT_DATE_HEURE." <= :".EVENEMENT_DATE_HEURE_FIN." AND ".EVENEMENT_DATE_HEURE." >= :".EVENEMENT_DATE_HEURE_DEBUT." AND ".EVENEMENT_TYPE_EVENEMENT_ID." = :".EVENEMENT_TYPE_EVENEMENT_ID." ORDER BY ".EVENEMENT_DATE_HEURE." DESC LIMIT ".$limit;
        $params = array(
            EVENEMENT_DATE_HEURE_DEBUT => $dateHeureDebut,
            EVENEMENT_DATE_HEURE_FIN => $dateHeureFin,
            EVENEMENT_TYPE_EVENEMENT_ID => $type
        );
        $columns = array(
            EVENEMENT_ID, 
            EVENEMENT_CODE,
            EVENEMENT_NOM, 
            EVENEMENT_DATE_HEURE, 
            EVENEMENT_NUM_EQUIPE, 
            EVENEMENT_ADVERSAIRE, 
            EVENEMENT_ADRESSE, 
            EVENEMENT_CODE_POSTAL, 
            EVENEMENT_VILLE, 
            EVENEMENT_LATITUDE, 
            EVENEMENT_LONGITUDE, 
            EVENEMENT_A_DOMICILE,
            EVENEMENT_SCORE_HBCSJ,
            EVENEMENT_SCORE_ADVERSAIRE,
            EVENEMENT_VISIBLE_SUR_SITE,
            EVENEMENT_EMAIL_DERNIER_EDITEUR,
            EVENEMENT_GYMNASE_ID,
            EVENEMENT_COMPETITION_ID,
            EVENEMENT_TYPE_EVENEMENT_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function getByCompetitionId($competitionId) {
        $query = "SELECT * FROM ".EVENEMENT_TABLE_NAME." WHERE ".EVENEMENT_COMPETITION_ID." = :".EVENEMENT_COMPETITION_ID;
        $params = array(
            EVENEMENT_COMPETITION_ID => $competitionId
        );
        $columns = array(
            EVENEMENT_ID, 
            EVENEMENT_CODE,
            EVENEMENT_NOM, 
            EVENEMENT_DATE_HEURE, 
            EVENEMENT_NUM_EQUIPE, 
            EVENEMENT_ADVERSAIRE, 
            EVENEMENT_ADRESSE, 
            EVENEMENT_CODE_POSTAL, 
            EVENEMENT_VILLE, 
            EVENEMENT_LATITUDE, 
            EVENEMENT_LONGITUDE, 
            EVENEMENT_A_DOMICILE,
            EVENEMENT_SCORE_HBCSJ,
            EVENEMENT_SCORE_ADVERSAIRE,
            EVENEMENT_VISIBLE_SUR_SITE,
            EVENEMENT_EMAIL_DERNIER_EDITEUR,
            EVENEMENT_GYMNASE_ID,
            EVENEMENT_COMPETITION_ID,
            EVENEMENT_TYPE_EVENEMENT_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function getByGymnaseId($gymnaseId) {
        $query = "SELECT * FROM ".EVENEMENT_TABLE_NAME." WHERE ".EVENEMENT_GYMNASE_ID." = :".EVENEMENT_GYMNASE_ID;
        $params = array(
            EVENEMENT_GYMNASE_ID => $gymnaseId
        );
        $columns = array(
            EVENEMENT_ID, 
            EVENEMENT_CODE,
            EVENEMENT_NOM, 
            EVENEMENT_DATE_HEURE, 
            EVENEMENT_NUM_EQUIPE, 
            EVENEMENT_ADVERSAIRE, 
            EVENEMENT_ADRESSE, 
            EVENEMENT_CODE_POSTAL, 
            EVENEMENT_VILLE, 
            EVENEMENT_LATITUDE, 
            EVENEMENT_LONGITUDE, 
            EVENEMENT_A_DOMICILE,
            EVENEMENT_SCORE_HBCSJ,
            EVENEMENT_SCORE_ADVERSAIRE,
            EVENEMENT_VISIBLE_SUR_SITE,
            EVENEMENT_EMAIL_DERNIER_EDITEUR,
            EVENEMENT_GYMNASE_ID,
            EVENEMENT_COMPETITION_ID,
            EVENEMENT_TYPE_EVENEMENT_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function getNbTotal() {
        $query = "SELECT COUNT(DISTINCT ".EVENEMENT_TABLE_NAME.".".EVENEMENT_ID.") AS ".EVENEMENT_NB_EVENEMENTS." ";
        $query .= "FROM ".EVENEMENT_TABLE_NAME;
        $columns = array(
            EVENEMENT_NB_EVENEMENTS
        );
        return $this->executeGetUniqueRequest($query, null, $columns);
    }
	
	public function getNbAVenir() {
        require_once("common/php/lib/date-utils.php");

        $query = "SELECT COUNT(DISTINCT ".EVENEMENT_TABLE_NAME.".".EVENEMENT_ID.") AS ".EVENEMENT_NB_EVENEMENTS." ";
        $query .= "FROM ".EVENEMENT_TABLE_NAME." ";
        $query .= "WHERE ".EVENEMENT_DATE_HEURE." > '".date(SQL_DATE_TIME_FORMAT)."'";
        $columns = array(
            EVENEMENT_NB_EVENEMENTS
        );
        return $this->executeGetUniqueRequest($query, null, $columns);
    }
	
	public function getNbVisibles() {
        $query = "SELECT COUNT(DISTINCT ".EVENEMENT_TABLE_NAME.".".EVENEMENT_ID.") AS ".EVENEMENT_NB_EVENEMENTS." ";
        $query .= "FROM ".EVENEMENT_TABLE_NAME." ";
        $query .= "WHERE ".EVENEMENT_VISIBLE_SUR_SITE." = 1";
        $columns = array(
            EVENEMENT_NB_EVENEMENTS
        );
        return $this->executeGetUniqueRequest($query, null, $columns);
    }
	
	public function getNbAValider() {
        $query = "SELECT COUNT(DISTINCT ".EVENEMENT_TABLE_NAME.".".EVENEMENT_ID.") AS ".EVENEMENT_NB_EVENEMENTS." ";
        $query .= "FROM ".EVENEMENT_TABLE_NAME." ";
        $query .= "WHERE ".EVENEMENT_VISIBLE_SUR_SITE." = 0 ";
        $query .= "AND ".EVENEMENT_EMAIL_DERNIER_EDITEUR." != '".$GLOBALS[CONF][SUPERADMIN_EMAIL]."'";
        $columns = array(
            EVENEMENT_NB_EVENEMENTS
        );
        return $this->executeGetUniqueRequest($query, null, $columns);
    }

    public function getNumEquipeMax() {
        $query = "SELECT MAX(".EVENEMENT_TABLE_NAME.".".EVENEMENT_NUM_EQUIPE.") AS ".EVENEMENT_MAX_NUM_EQUIPE." ";
        $query .= "FROM ".EVENEMENT_TABLE_NAME;
        $columns = array(
            EVENEMENT_MAX_NUM_EQUIPE
        );
        return $this->executeGetUniqueRequest($query, null, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".EVENEMENT_TABLE_NAME." (";
        $query .= EVENEMENT_CODE.", ";
        $query .= EVENEMENT_NOM.", ";
        $query .= EVENEMENT_DATE_HEURE.", ";
        $query .= EVENEMENT_NUM_EQUIPE.", ";
        $query .= EVENEMENT_ADVERSAIRE.", ";
        $query .= EVENEMENT_ADRESSE.", ";
        $query .= EVENEMENT_CODE_POSTAL.", ";
        $query .= EVENEMENT_VILLE.", ";
        $query .= EVENEMENT_LATITUDE.", ";
        $query .= EVENEMENT_LONGITUDE.", ";
        $query .= EVENEMENT_A_DOMICILE.", ";
        $query .= EVENEMENT_SCORE_HBCSJ.", ";
        $query .= EVENEMENT_SCORE_ADVERSAIRE.", ";
        $query .= EVENEMENT_VISIBLE_SUR_SITE.", ";
        $query .= EVENEMENT_EMAIL_DERNIER_EDITEUR.", ";
        $query .= EVENEMENT_GYMNASE_ID.", ";
        $query .= EVENEMENT_COMPETITION_ID.", ";
        $query .= EVENEMENT_TYPE_EVENEMENT_ID.") VALUES (";
        $query .= ":".EVENEMENT_CODE.", ";
        $query .= ":".EVENEMENT_NOM.", ";
        $query .= ":".EVENEMENT_DATE_HEURE.", ";
        $query .= ":".EVENEMENT_NUM_EQUIPE.", ";
        $query .= ":".EVENEMENT_ADVERSAIRE.", ";
        $query .= ":".EVENEMENT_ADRESSE.", ";
        $query .= ":".EVENEMENT_CODE_POSTAL.", ";
        $query .= ":".EVENEMENT_VILLE.", ";
        $query .= ":".EVENEMENT_LATITUDE.", ";
        $query .= ":".EVENEMENT_LONGITUDE.", ";
        $query .= ":".EVENEMENT_A_DOMICILE.", ";
        $query .= ":".EVENEMENT_SCORE_HBCSJ.", ";
        $query .= ":".EVENEMENT_SCORE_ADVERSAIRE.", ";
        $query .= ":".EVENEMENT_VISIBLE_SUR_SITE.", ";
        $query .= ":".EVENEMENT_EMAIL_DERNIER_EDITEUR.", ";
        $query .= ":".EVENEMENT_GYMNASE_ID.", ";
        $query .= ":".EVENEMENT_COMPETITION_ID.", ";
        $query .= ":".EVENEMENT_TYPE_EVENEMENT_ID.")";
        $params = array(
            EVENEMENT_CODE => $object[EVENEMENT_CODE],
            EVENEMENT_NOM => $object[EVENEMENT_NOM],
            EVENEMENT_DATE_HEURE => $object[EVENEMENT_DATE_HEURE],
            EVENEMENT_NUM_EQUIPE => $object[EVENEMENT_NUM_EQUIPE],
            EVENEMENT_ADVERSAIRE => $object[EVENEMENT_ADVERSAIRE],
            EVENEMENT_ADRESSE => $object[EVENEMENT_ADRESSE],
            EVENEMENT_CODE_POSTAL => $object[EVENEMENT_CODE_POSTAL],
            EVENEMENT_VILLE => $object[EVENEMENT_VILLE],
            EVENEMENT_LATITUDE => $object[EVENEMENT_LATITUDE],
            EVENEMENT_LONGITUDE => $object[EVENEMENT_LONGITUDE],
            EVENEMENT_A_DOMICILE => $object[EVENEMENT_A_DOMICILE],
            EVENEMENT_SCORE_HBCSJ => $object[EVENEMENT_SCORE_HBCSJ],
            EVENEMENT_SCORE_ADVERSAIRE => $object[EVENEMENT_SCORE_ADVERSAIRE],
            EVENEMENT_VISIBLE_SUR_SITE => $object[EVENEMENT_VISIBLE_SUR_SITE],
            EVENEMENT_EMAIL_DERNIER_EDITEUR => $object[EVENEMENT_EMAIL_DERNIER_EDITEUR],
            EVENEMENT_GYMNASE_ID => $object[EVENEMENT_GYMNASE_ID],
            EVENEMENT_COMPETITION_ID => $object[EVENEMENT_COMPETITION_ID],
            EVENEMENT_TYPE_EVENEMENT_ID => $object[EVENEMENT_TYPE_EVENEMENT_ID]
        );
        return $this->executeCreateRequest($query, $params);}
	
	public function update($object) {
        $query = "UPDATE ".EVENEMENT_TABLE_NAME." SET ";
        $query .= EVENEMENT_CODE." = :".EVENEMENT_CODE.", ";
        $query .= EVENEMENT_NOM." = :".EVENEMENT_NOM.", ";
        $query .= EVENEMENT_DATE_HEURE." = :".EVENEMENT_DATE_HEURE.", ";
        $query .= EVENEMENT_NUM_EQUIPE." = :".EVENEMENT_NUM_EQUIPE.", ";
        $query .= EVENEMENT_ADVERSAIRE." = :".EVENEMENT_ADVERSAIRE.", ";
        $query .= EVENEMENT_ADRESSE." = :".EVENEMENT_ADRESSE.", ";
        $query .= EVENEMENT_CODE_POSTAL." = :".EVENEMENT_CODE_POSTAL.", ";
        $query .= EVENEMENT_VILLE." = :".EVENEMENT_VILLE.", ";
        $query .= EVENEMENT_LATITUDE." = :".EVENEMENT_LATITUDE.", ";
        $query .= EVENEMENT_LONGITUDE." = :".EVENEMENT_LONGITUDE.", ";
        $query .= EVENEMENT_A_DOMICILE." = :".EVENEMENT_A_DOMICILE.", ";
        $query .= EVENEMENT_SCORE_HBCSJ." = :".EVENEMENT_SCORE_HBCSJ.", ";
        $query .= EVENEMENT_SCORE_ADVERSAIRE." = :".EVENEMENT_SCORE_ADVERSAIRE.", ";
        $query .= EVENEMENT_VISIBLE_SUR_SITE." = :".EVENEMENT_VISIBLE_SUR_SITE.", ";
        $query .= EVENEMENT_EMAIL_DERNIER_EDITEUR." = :".EVENEMENT_EMAIL_DERNIER_EDITEUR.", ";
        $query .= EVENEMENT_GYMNASE_ID." = :".EVENEMENT_GYMNASE_ID.", ";
        $query .= EVENEMENT_COMPETITION_ID." = :".EVENEMENT_COMPETITION_ID.", ";
        $query .= EVENEMENT_TYPE_EVENEMENT_ID." = :".EVENEMENT_TYPE_EVENEMENT_ID." WHERE ";
        $query .= EVENEMENT_ID." = :".EVENEMENT_ID;
        $params = array(
            EVENEMENT_CODE => $object[EVENEMENT_CODE],
            EVENEMENT_NOM => $object[EVENEMENT_NOM],
            EVENEMENT_DATE_HEURE => $object[EVENEMENT_DATE_HEURE],
            EVENEMENT_NUM_EQUIPE => $object[EVENEMENT_NUM_EQUIPE],
            EVENEMENT_ADVERSAIRE => $object[EVENEMENT_ADVERSAIRE],
            EVENEMENT_ADRESSE => $object[EVENEMENT_ADRESSE],
            EVENEMENT_CODE_POSTAL => $object[EVENEMENT_CODE_POSTAL],
            EVENEMENT_VILLE => $object[EVENEMENT_VILLE],
            EVENEMENT_LATITUDE => $object[EVENEMENT_LATITUDE],
            EVENEMENT_LONGITUDE => $object[EVENEMENT_LONGITUDE],
            EVENEMENT_A_DOMICILE => $object[EVENEMENT_A_DOMICILE],
            EVENEMENT_SCORE_HBCSJ => $object[EVENEMENT_SCORE_HBCSJ],
            EVENEMENT_SCORE_ADVERSAIRE => $object[EVENEMENT_SCORE_ADVERSAIRE],
            EVENEMENT_VISIBLE_SUR_SITE => $object[EVENEMENT_VISIBLE_SUR_SITE],
            EVENEMENT_EMAIL_DERNIER_EDITEUR => $object[EVENEMENT_EMAIL_DERNIER_EDITEUR],
            EVENEMENT_GYMNASE_ID => $object[EVENEMENT_GYMNASE_ID],
            EVENEMENT_COMPETITION_ID => $object[EVENEMENT_COMPETITION_ID],
            EVENEMENT_TYPE_EVENEMENT_ID => $object[EVENEMENT_TYPE_EVENEMENT_ID],
            EVENEMENT_ID => $object[EVENEMENT_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".EVENEMENT_TABLE_NAME." WHERE ".EVENEMENT_ID." = :".EVENEMENT_ID;
        $params = array(
            EVENEMENT_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>