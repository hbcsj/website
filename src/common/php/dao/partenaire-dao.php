<?php
require_once("core/php/lib/abstract-dao.php");

define(PARTENAIRE_TABLE_NAME, "hbcsj_partenaire");
define(PARTENAIRE_ID, "id");
define(PARTENAIRE_NOM, "nom");
define(PARTENAIRE_URL, "url_site");

class PartenaireDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(PARTENAIRE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".PARTENAIRE_TABLE_NAME." WHERE ".PARTENAIRE_ID." = :".PARTENAIRE_ID;
        $params = array(
            PARTENAIRE_ID => $id
        );
        $columns = array(
            PARTENAIRE_ID, 
            PARTENAIRE_NOM, 
            PARTENAIRE_URL
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".PARTENAIRE_TABLE_NAME;
        $columns = array(
            PARTENAIRE_ID, 
            PARTENAIRE_NOM, 
            PARTENAIRE_URL
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".PARTENAIRE_TABLE_NAME." (";
        $query .= PARTENAIRE_NOM.", ";
        $query .= PARTENAIRE_URL.") VALUES (";
        $query .= ":".PARTENAIRE_NOM.", ";
        $query .= ":".PARTENAIRE_URL.")";
        $params = array(
            PARTENAIRE_NOM => $object[PARTENAIRE_NOM],
            PARTENAIRE_URL => $object[PARTENAIRE_URL]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {
        $query = "UPDATE ".PARTENAIRE_TABLE_NAME." SET ";
        $query .= PARTENAIRE_NOM." = :".PARTENAIRE_NOM.", ";
        $query .= PARTENAIRE_URL." = :".PARTENAIRE_URL." WHERE ";
        $query .= PARTENAIRE_ID." = :".PARTENAIRE_ID;
        $params = array(
            PARTENAIRE_NOM => $object[PARTENAIRE_NOM],
            PARTENAIRE_URL => $object[PARTENAIRE_URL],
            PARTENAIRE_ID => $object[PARTENAIRE_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".PARTENAIRE_TABLE_NAME." WHERE ".PARTENAIRE_ID." = :".PARTENAIRE_ID;
        $params = array(
            PARTENAIRE_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>