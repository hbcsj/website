<?php
require_once("core/php/lib/abstract-dao.php");

define(TYPE_ACTION_CLUB_TABLE_NAME, "hbcsj_type_action_club");
define(TYPE_ACTION_CLUB_ID, "id");
define(TYPE_ACTION_CLUB_LIBELLE, "libelle");

class TypeActionClubDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(TYPE_ACTION_CLUB_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".TYPE_ACTION_CLUB_TABLE_NAME." WHERE ".TYPE_ACTION_CLUB_ID." = :".TYPE_ACTION_CLUB_ID;
        $params = array(
            TYPE_ACTION_CLUB_ID => $id
        );
        $columns = array(TYPE_ACTION_CLUB_ID, TYPE_ACTION_CLUB_LIBELLE);
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".TYPE_ACTION_CLUB_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(TYPE_ACTION_CLUB_ID, TYPE_ACTION_CLUB_LIBELLE);
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {}
	
	public function update($object) {}
	
	public function delete($id) {}
}

?>