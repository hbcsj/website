<?php
require_once("core/php/lib/abstract-dao.php");

define(LICENCIE_JOUE_DANS_CATEGORIE_TABLE_NAME, "hbcsj_licencie_joue_dans_categorie");
define(LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID, "licencie_id");
define(LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID, "categorie_id");

define(LICENCIE_JOUE_DANS_CATEGORIE_NB_LICENCIES, "nb_licencies");

class LicencieJoueDansCategorieDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(LICENCIE_JOUE_DANS_CATEGORIE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".LICENCIE_JOUE_DANS_CATEGORIE_TABLE_NAME." WHERE ".LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID." = :".LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID." AND ".LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID." = :".LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID;
        $params = array(
            LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID => $id[LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID],
            LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID => $id[LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID]
        );
        $columns = array(
            LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID,
            LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".LICENCIE_JOUE_DANS_CATEGORIE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID,
            LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID
        );
        return $this->executeGetRequest($query, null, $columns);
    }

    public function getByLicencieId($licencieId, $orderBy = null) {
        $query = "SELECT * FROM ".LICENCIE_JOUE_DANS_CATEGORIE_TABLE_NAME." WHERE ".LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID." = :".LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $params = array(
            LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID => $licencieId
        );
        $columns = array(
            LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID,
            LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function getNbByCategorieId($categorieId) {
        $query = "SELECT COUNT(DISTINCT ".LICENCIE_JOUE_DANS_CATEGORIE_TABLE_NAME.".".LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID.") AS ".LICENCIE_JOUE_DANS_CATEGORIE_NB_LICENCIES." ";
        $query .= "FROM ".LICENCIE_JOUE_DANS_CATEGORIE_TABLE_NAME." ";
        $query .= "WHERE ".LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID." = :".LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID;
        $params = array(
            LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID => $categorieId
        );
        $columns = array(
            LICENCIE_JOUE_DANS_CATEGORIE_NB_LICENCIES
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".LICENCIE_JOUE_DANS_CATEGORIE_TABLE_NAME." (";
        $query .= LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID.", ";
        $query .= LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID.") VALUES (";
        $query .= ":".LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID.", ";
        $query .= ":".LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID.")";
        $params = array(
            LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID => $object[LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID],
            LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID => $object[LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {}
	
	public function delete($id) {
        $query = "DELETE FROM ".LICENCIE_JOUE_DANS_CATEGORIE_TABLE_NAME." WHERE ".LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID." = :".LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID." AND ".LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID." = :".LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID;
        $params = array(
            LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID => $id[LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID],
            LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID => $id[LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function deleteByLicencieId($licencieId) {
        $query = "DELETE FROM ".LICENCIE_JOUE_DANS_CATEGORIE_TABLE_NAME." WHERE ".LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID." = :".LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID;
        $params = array(
            LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID => $licencieId
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>