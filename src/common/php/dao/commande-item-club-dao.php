<?php
require_once("core/php/lib/abstract-dao.php");

define(COMMANDE_ITEM_CLUB_TABLE_NAME, "hbcsj_commande_item_club");
define(COMMANDE_ITEM_CLUB_ID, "id");
define(COMMANDE_ITEM_CLUB_PRODUIT, "produit");
define(COMMANDE_ITEM_CLUB_QUANTITE, "quantite");
define(COMMANDE_ITEM_CLUB_COMMANDE_CLUB_ID, "commande_club_id");
define(COMMANDE_ITEM_CLUB_LIVRAISON_FOURNISSEUR_BOUTIQUE_ID, "livraison_fournisseur_boutique_id");

class CommandeItemClubDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(COMMANDE_ITEM_CLUB_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".COMMANDE_ITEM_CLUB_TABLE_NAME." WHERE ".COMMANDE_ITEM_CLUB_ID." = :".COMMANDE_ITEM_CLUB_ID;
        $params = array(
            COMMANDE_ITEM_CLUB_ID => $id
        );
        $columns = array(
            COMMANDE_ITEM_CLUB_ID, 
            COMMANDE_ITEM_CLUB_PRODUIT,
            COMMANDE_ITEM_CLUB_QUANTITE,
            COMMANDE_ITEM_CLUB_COMMANDE_CLUB_ID,
            COMMANDE_ITEM_CLUB_LIVRAISON_FOURNISSEUR_BOUTIQUE_ID
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".COMMANDE_ITEM_CLUB_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            COMMANDE_ITEM_CLUB_ID, 
            COMMANDE_ITEM_CLUB_PRODUIT,
            COMMANDE_ITEM_CLUB_QUANTITE,
            COMMANDE_ITEM_CLUB_COMMANDE_CLUB_ID,
            COMMANDE_ITEM_CLUB_LIVRAISON_FOURNISSEUR_BOUTIQUE_ID
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".COMMANDE_ITEM_CLUB_TABLE_NAME." (";
        $query .= COMMANDE_ITEM_CLUB_PRODUIT.", ";
        $query .= COMMANDE_ITEM_CLUB_QUANTITE.", ";
        $query .= COMMANDE_ITEM_CLUB_COMMANDE_CLUB_ID.", ";
        $query .= COMMANDE_ITEM_CLUB_LIVRAISON_FOURNISSEUR_BOUTIQUE_ID.") VALUES (";
        $query .= ":".COMMANDE_ITEM_CLUB_PRODUIT.", ";
        $query .= ":".COMMANDE_ITEM_CLUB_QUANTITE.", ";
        $query .= ":".COMMANDE_ITEM_CLUB_COMMANDE_CLUB_ID.", ";
        $query .= ":".COMMANDE_ITEM_CLUB_LIVRAISON_FOURNISSEUR_BOUTIQUE_ID.")";
        $params = array(
            COMMANDE_ITEM_CLUB_PRODUIT => $object[COMMANDE_ITEM_CLUB_PRODUIT],
            COMMANDE_ITEM_CLUB_QUANTITE => $object[COMMANDE_ITEM_CLUB_QUANTITE],
            COMMANDE_ITEM_CLUB_COMMANDE_CLUB_ID => $object[COMMANDE_ITEM_CLUB_COMMANDE_CLUB_ID],
            COMMANDE_ITEM_CLUB_LIVRAISON_FOURNISSEUR_BOUTIQUE_ID => $object[COMMANDE_ITEM_CLUB_LIVRAISON_FOURNISSEUR_BOUTIQUE_ID]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {
        $query = "UPDATE ".COMMANDE_ITEM_CLUB_TABLE_NAME." SET ";
        $query .= COMMANDE_ITEM_CLUB_PRODUIT." = :".COMMANDE_ITEM_CLUB_PRODUIT.", ";
        $query .= COMMANDE_ITEM_CLUB_QUANTITE." = :".COMMANDE_ITEM_CLUB_QUANTITE.", ";
        $query .= COMMANDE_ITEM_CLUB_COMMANDE_CLUB_ID." = :".COMMANDE_ITEM_CLUB_COMMANDE_CLUB_ID.", ";
        $query .= COMMANDE_ITEM_CLUB_LIVRAISON_FOURNISSEUR_BOUTIQUE_ID." = :".COMMANDE_ITEM_CLUB_LIVRAISON_FOURNISSEUR_BOUTIQUE_ID." WHERE ";
        $query .= COMMANDE_ITEM_CLUB_ID." = :".COMMANDE_ITEM_CLUB_ID;
        $params = array(
            COMMANDE_ITEM_CLUB_PRODUIT => $object[COMMANDE_ITEM_CLUB_PRODUIT],
            COMMANDE_ITEM_CLUB_QUANTITE => $object[COMMANDE_ITEM_CLUB_QUANTITE],
            COMMANDE_ITEM_CLUB_COMMANDE_CLUB_ID => $object[COMMANDE_ITEM_CLUB_COMMANDE_CLUB_ID],
            COMMANDE_ITEM_CLUB_LIVRAISON_FOURNISSEUR_BOUTIQUE_ID => $object[COMMANDE_ITEM_CLUB_LIVRAISON_FOURNISSEUR_BOUTIQUE_ID],
            COMMANDE_ITEM_CLUB_ID => $object[COMMANDE_ITEM_CLUB_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".COMMANDE_ITEM_CLUB_TABLE_NAME." WHERE ".COMMANDE_ITEM_CLUB_ID." = :".COMMANDE_ITEM_CLUB_ID;
        $params = array(
            COMMANDE_ITEM_CLUB_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>