<?php
require_once("core/php/lib/abstract-dao.php");

define(PRIX_PRODUIT_BOUTIQUE_TABLE_NAME, "hbcsj_prix_produit_boutique");
define(PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID, "produit_boutique_id");
define(PRIX_PRODUIT_BOUTIQUE_TAILLE, "taille");
define(PRIX_PRODUIT_BOUTIQUE_PRIX, "prix");
define(PRIX_PRODUIT_BOUTIQUE_PRIX_FOURNISSEUR, "prix_fournisseur");

class PrixProduitBoutiqueDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(PRIX_PRODUIT_BOUTIQUE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".PRIX_PRODUIT_BOUTIQUE_TABLE_NAME." WHERE ".PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID." = :".PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID." AND ".PRIX_PRODUIT_BOUTIQUE_TAILLE." = :".PRIX_PRODUIT_BOUTIQUE_TAILLE;
        $params = array(
            PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID => $id[PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID],
            PRIX_PRODUIT_BOUTIQUE_TAILLE => $id[PRIX_PRODUIT_BOUTIQUE_TAILLE]
        );
        $columns = array(
            PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID, 
            PRIX_PRODUIT_BOUTIQUE_TAILLE, 
            PRIX_PRODUIT_BOUTIQUE_PRIX,
            PRIX_PRODUIT_BOUTIQUE_PRIX_FOURNISSEUR
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".PRIX_PRODUIT_BOUTIQUE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID, 
            PRIX_PRODUIT_BOUTIQUE_TAILLE, 
            PRIX_PRODUIT_BOUTIQUE_PRIX,
            PRIX_PRODUIT_BOUTIQUE_PRIX_FOURNISSEUR
        );
        return $this->executeGetRequest($query, null, $columns);
    }

    public function getByProduitId($produitId) {
        $query = "SELECT * FROM ".PRIX_PRODUIT_BOUTIQUE_TABLE_NAME." WHERE ".PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID." = :".PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID." ORDER BY ".PRIX_PRODUIT_BOUTIQUE_PRIX;
        $params = array(
            PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID => $produitId
        );
        $columns = array(
            PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID, 
            PRIX_PRODUIT_BOUTIQUE_TAILLE, 
            PRIX_PRODUIT_BOUTIQUE_PRIX,
            PRIX_PRODUIT_BOUTIQUE_PRIX_FOURNISSEUR
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".PRIX_PRODUIT_BOUTIQUE_TABLE_NAME." (";
        $query .= PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID.", ";
        $query .= PRIX_PRODUIT_BOUTIQUE_TAILLE.", ";
        $query .= PRIX_PRODUIT_BOUTIQUE_PRIX.", ";
        $query .= PRIX_PRODUIT_BOUTIQUE_PRIX_FOURNISSEUR.") VALUES (";
        $query .= ":".PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID.", ";
        $query .= ":".PRIX_PRODUIT_BOUTIQUE_TAILLE.", ";
        $query .= ":".PRIX_PRODUIT_BOUTIQUE_PRIX.", ";
        $query .= ":".PRIX_PRODUIT_BOUTIQUE_PRIX_FOURNISSEUR.")";
        $params = array(
            PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID => $object[PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID],
            PRIX_PRODUIT_BOUTIQUE_TAILLE => $object[PRIX_PRODUIT_BOUTIQUE_TAILLE],
            PRIX_PRODUIT_BOUTIQUE_PRIX => $object[PRIX_PRODUIT_BOUTIQUE_PRIX],
            PRIX_PRODUIT_BOUTIQUE_PRIX_FOURNISSEUR => $object[PRIX_PRODUIT_BOUTIQUE_PRIX_FOURNISSEUR]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {}
	
	public function delete($id) {
        $query = "DELETE FROM ".PRIX_PRODUIT_BOUTIQUE_TABLE_NAME." WHERE ".PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID." = :".PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID." AND ".PRIX_PRODUIT_BOUTIQUE_TAILLE." = :".PRIX_PRODUIT_BOUTIQUE_TAILLE;
        $params = array(
            PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID => $id[PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID],
            PRIX_PRODUIT_BOUTIQUE_TAILLE => $id[PRIX_PRODUIT_BOUTIQUE_TAILLE]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function deleteByProduitBoutiqueId($produitBoutiqueId) {
        $query = "DELETE FROM ".PRIX_PRODUIT_BOUTIQUE_TABLE_NAME." WHERE ".PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID." = :".PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID;
        $params = array(
            PRIX_PRODUIT_BOUTIQUE_PRODUIT_BOUTIQUE_ID => $produitBoutiqueId
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>