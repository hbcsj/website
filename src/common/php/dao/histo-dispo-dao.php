<?php
require_once("core/php/lib/abstract-dao.php");

define(HISTO_DISPO_TABLE_NAME, "hbcsj_histo_dispo");
define(HISTO_DISPO_ID, "id");
define(HISTO_DISPO_DATE_HEURE, "date_heure");
define(HISTO_DISPO_LICENCIE_ID, "licencie_id");
define(HISTO_DISPO_EVENEMENT_ID, "evenement_id");
define(HISTO_DISPO_TYPE_DISPO_ID, "type_dispo_id");
define(HISTO_DISPO_REPONSE_DISPO_ID, "reponse_dispo_id");

class HistoDispoDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(HISTO_DISPO_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".HISTO_DISPO_TABLE_NAME." WHERE ".HISTO_DISPO_ID." = :".HISTO_DISPO_ID;
        $params = array(
            HISTO_DISPO_ID => $id
        );
        $columns = array(
            HISTO_DISPO_ID, 
            HISTO_DISPO_DATE_HEURE, 
            HISTO_DISPO_LICENCIE_ID,
            HISTO_DISPO_EVENEMENT_ID,
            HISTO_DISPO_TYPE_DISPO_ID,
            HISTO_DISPO_REPONSE_DISPO_ID
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".HISTO_DISPO_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            HISTO_DISPO_ID, 
            HISTO_DISPO_DATE_HEURE, 
            HISTO_DISPO_LICENCIE_ID,
            HISTO_DISPO_EVENEMENT_ID,
            HISTO_DISPO_TYPE_DISPO_ID,
            HISTO_DISPO_REPONSE_DISPO_ID
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function getByEvenementIdAndTypeDispoId($evenementId, $typeDispoId) {
        $query = "SELECT * FROM ".HISTO_DISPO_TABLE_NAME." WHERE ".HISTO_DISPO_EVENEMENT_ID." = :".HISTO_DISPO_EVENEMENT_ID." AND ".HISTO_DISPO_TYPE_DISPO_ID." = :".HISTO_DISPO_TYPE_DISPO_ID." ORDER BY ".HISTO_DISPO_DATE_HEURE." DESC";
        $params = array(
            HISTO_DISPO_EVENEMENT_ID => $evenementId,
            HISTO_DISPO_TYPE_DISPO_ID => $typeDispoId
        );
        $columns = array(
            HISTO_DISPO_ID, 
            HISTO_DISPO_DATE_HEURE, 
            HISTO_DISPO_LICENCIE_ID,
            HISTO_DISPO_EVENEMENT_ID,
            HISTO_DISPO_TYPE_DISPO_ID,
            HISTO_DISPO_REPONSE_DISPO_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".HISTO_DISPO_TABLE_NAME." (";
        $query .= HISTO_DISPO_DATE_HEURE.", ";
        $query .= HISTO_DISPO_LICENCIE_ID.", ";
        $query .= HISTO_DISPO_EVENEMENT_ID.", ";
        $query .= HISTO_DISPO_TYPE_DISPO_ID.", ";
        $query .= HISTO_DISPO_REPONSE_DISPO_ID.") VALUES (";
        $query .= ":".HISTO_DISPO_DATE_HEURE.", ";
        $query .= ":".HISTO_DISPO_LICENCIE_ID.", ";
        $query .= ":".HISTO_DISPO_EVENEMENT_ID.", ";
        $query .= ":".HISTO_DISPO_TYPE_DISPO_ID.", ";
        $query .= ":".HISTO_DISPO_REPONSE_DISPO_ID.")";
        $params = array(
            HISTO_DISPO_DATE_HEURE => $object[HISTO_DISPO_DATE_HEURE],
            HISTO_DISPO_LICENCIE_ID => $object[HISTO_DISPO_LICENCIE_ID],
            HISTO_DISPO_EVENEMENT_ID => $object[HISTO_DISPO_EVENEMENT_ID],
            HISTO_DISPO_TYPE_DISPO_ID => $object[HISTO_DISPO_TYPE_DISPO_ID],
            HISTO_DISPO_REPONSE_DISPO_ID => $object[HISTO_DISPO_REPONSE_DISPO_ID]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {
        $query = "UPDATE ".HISTO_DISPO_TABLE_NAME." SET ";
        $query .= HISTO_DISPO_DATE_HEURE." = :".HISTO_DISPO_DATE_HEURE.", ";
        $query .= HISTO_DISPO_LICENCIE_ID." = :".HISTO_DISPO_LICENCIE_ID.", ";
        $query .= HISTO_DISPO_EVENEMENT_ID." = :".HISTO_DISPO_EVENEMENT_ID.", ";
        $query .= HISTO_DISPO_TYPE_DISPO_ID." = :".HISTO_DISPO_TYPE_DISPO_ID.", ";
        $query .= HISTO_DISPO_REPONSE_DISPO_ID." = :".HISTO_DISPO_REPONSE_DISPO_ID." WHERE ";
        $query .= HISTO_DISPO_ID." = :".HISTO_DISPO_ID;
        $params = array(
            HISTO_DISPO_DATE_HEURE => $object[HISTO_DISPO_DATE_HEURE],
            HISTO_DISPO_LICENCIE_ID => $object[HISTO_DISPO_LICENCIE_ID],
            HISTO_DISPO_EVENEMENT_ID => $object[HISTO_DISPO_EVENEMENT_ID],
            HISTO_DISPO_TYPE_DISPO_ID => $object[HISTO_DISPO_TYPE_DISPO_ID],
            HISTO_DISPO_REPONSE_DISPO_ID => $object[HISTO_DISPO_REPONSE_DISPO_ID],
            HISTO_DISPO_ID => $object[HISTO_DISPO_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".HISTO_DISPO_TABLE_NAME." WHERE ".HISTO_DISPO_ID." = :".HISTO_DISPO_ID;
        $params = array(
            HISTO_DISPO_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function deleteByLicencieId($licencieId) {
        $query = "DELETE FROM ".HISTO_DISPO_TABLE_NAME." WHERE ".HISTO_DISPO_LICENCIE_ID." = :".HISTO_DISPO_LICENCIE_ID;
        $params = array(
            HISTO_DISPO_LICENCIE_ID => $licencieId
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function deleteByEvenementId($evenementId) {
        $query = "DELETE FROM ".HISTO_DISPO_TABLE_NAME." WHERE ".HISTO_DISPO_EVENEMENT_ID." = :".HISTO_DISPO_EVENEMENT_ID;
        $params = array(
            HISTO_DISPO_EVENEMENT_ID => $evenementId
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>