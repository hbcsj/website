<?php
require_once("core/php/lib/abstract-dao.php");

define(CRITICITE_TABLE_NAME, "hbcsj_criticite");
define(CRITICITE_ID, "id");
define(CRITICITE_LIBELLE, "libelle");

class CriticiteDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(CRITICITE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".CRITICITE_TABLE_NAME." WHERE ".CRITICITE_ID." = :".CRITICITE_ID;
        $params = array(
            CRITICITE_ID => $id
        );
        $columns = array(CRITICITE_ID, CRITICITE_LIBELLE);
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".CRITICITE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(CRITICITE_ID, CRITICITE_LIBELLE);
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {}
	
	public function update($object) {}
	
	public function delete($id) {}
}

?>