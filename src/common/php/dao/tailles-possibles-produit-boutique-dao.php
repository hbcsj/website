<?php
require_once("core/php/lib/abstract-dao.php");

define(TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TABLE_NAME, "hbcsj_tailles_possibles_produit_boutique");
define(TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_ID, "id");
define(TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TAILLES_POSSIBLES, "tailles_possibles");

define(TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TAILLES_SEPARATOR, ",");

class TaillesPossiblesProduitBoutiqueDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TABLE_NAME." WHERE ".TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_ID." = :".TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_ID;
        $params = array(
            TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_ID => $id
        );
        $columns = array(
            TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_ID, 
            TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TAILLES_POSSIBLES
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_ID, 
            TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TAILLES_POSSIBLES
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TABLE_NAME." (";
        $query .= TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TAILLES_POSSIBLES.") VALUES (";
        $query .= ":".TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TAILLES_POSSIBLES.")";
        $params = array(
            TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TAILLES_POSSIBLES => $object[TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TAILLES_POSSIBLES]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {
        $query = "UPDATE ".TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TABLE_NAME." SET ";
        $query .= TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TAILLES_POSSIBLES." = :".TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TAILLES_POSSIBLES." WHERE ";
        $query .= TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_ID." = :".TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_ID;
        $params = array(
            TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TAILLES_POSSIBLES => $object[TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TAILLES_POSSIBLES],
            TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_ID => $object[TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_TABLE_NAME." WHERE ".TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_ID." = :".TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_ID;
        $params = array(
            TAILLES_POSSIBLES_PRODUIT_BOUTIQUE_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>