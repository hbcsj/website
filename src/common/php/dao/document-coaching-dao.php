<?php
require_once("core/php/lib/abstract-dao.php");

define(DOCUMENT_COACHING_TABLE_NAME, "hbcsj_document_coaching");
define(DOCUMENT_COACHING_ID, "id");
define(DOCUMENT_COACHING_TITRE, "titre");
define(DOCUMENT_COACHING_MOTS_CLEFS, "mots_clefs");
define(DOCUMENT_COACHING_FICHIER, "fichier");
define(DOCUMENT_COACHING_URL, "url");
define(DOCUMENT_COACHING_NOM_EDITEUR, "nom_editeur");
define(DOCUMENT_COACHING_DATE_HEURE_MODIFICATION, "date_heure_modification");
define(DOCUMENT_COACHING_TYPE_DOCUMENT_COACHING_ID, "type_document_coaching_id");

define(DOCUMENT_NB_DOCUMENTS_COACHING, "nb_documents_coaching");

class DocumentCoachingDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(DOCUMENT_COACHING_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".DOCUMENT_COACHING_TABLE_NAME." WHERE ".DOCUMENT_COACHING_ID." = :".DOCUMENT_COACHING_ID;
        $params = array(
            DOCUMENT_COACHING_ID => $id
        );
        $columns = array(
            DOCUMENT_COACHING_ID, 
            DOCUMENT_COACHING_TITRE, 
            DOCUMENT_COACHING_MOTS_CLEFS, 
            DOCUMENT_COACHING_FICHIER, 
            DOCUMENT_COACHING_URL, 
            DOCUMENT_COACHING_NOM_EDITEUR, 
            DOCUMENT_COACHING_DATE_HEURE_MODIFICATION, 
            DOCUMENT_COACHING_TYPE_DOCUMENT_COACHING_ID
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".DOCUMENT_COACHING_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            DOCUMENT_COACHING_ID, 
            DOCUMENT_COACHING_TITRE, 
            DOCUMENT_COACHING_MOTS_CLEFS, 
            DOCUMENT_COACHING_FICHIER, 
            DOCUMENT_COACHING_URL, 
            DOCUMENT_COACHING_NOM_EDITEUR, 
            DOCUMENT_COACHING_DATE_HEURE_MODIFICATION,
            DOCUMENT_COACHING_TYPE_DOCUMENT_COACHING_ID
        );
        return $this->executeGetRequest($query, null, $columns);
    }

	public function getAllByParams($titre, $motsClefs, $typeDocumentCoachingId, $orderBy = null) {
        $query = "SELECT DISTINCT ".DOCUMENT_COACHING_TABLE_NAME.".* FROM ".DOCUMENT_COACHING_TABLE_NAME." ";

        $queryParamsArray = array();
        $params = array();
        if ($titre != null && $titre != "") {
            $queryParamsArray[] = DOCUMENT_COACHING_TITRE." LIKE :".DOCUMENT_COACHING_TITRE;
            $params[DOCUMENT_COACHING_TITRE] = "%".$titre."%";
        }
        if ($motsClefs != null && sizeof($motsClefs) > 0) {
            $queryMotsClefsArray = array();
            $motClefIndex = 1;
            foreach ($motsClefs as $motClef) {
                if (trim($motClef) != "") {
                    $queryMotsClefsArray[] = DOCUMENT_COACHING_MOTS_CLEFS." LIKE :".DOCUMENT_COACHING_MOTS_CLEFS.$motClefIndex;
                    $params[DOCUMENT_COACHING_MOTS_CLEFS.$motClefIndex] = "%".$motClef."%";
                    $motClefIndex++;
                }
            }
            $queryParamsArray[] = "(".implode(" OR ", $queryMotsClefsArray).")";
        }
        if ($typeDocumentCoachingId != null && $typeDocumentCoachingId != "") {
            $queryParamsArray[] = DOCUMENT_COACHING_TYPE_DOCUMENT_COACHING_ID." = :".DOCUMENT_COACHING_TYPE_DOCUMENT_COACHING_ID;
            $params[DOCUMENT_COACHING_TYPE_DOCUMENT_COACHING_ID] = $typeDocumentCoachingId;
        }
        if (sizeof($queryParamsArray) > 0) {
            $query .= " WHERE ";
            $query .= implode(" AND ", $queryParamsArray);
        }
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            DOCUMENT_COACHING_ID, 
            DOCUMENT_COACHING_TITRE, 
            DOCUMENT_COACHING_MOTS_CLEFS, 
            DOCUMENT_COACHING_FICHIER, 
            DOCUMENT_COACHING_URL, 
            DOCUMENT_COACHING_NOM_EDITEUR, 
            DOCUMENT_COACHING_DATE_HEURE_MODIFICATION,
            DOCUMENT_COACHING_TYPE_DOCUMENT_COACHING_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function getNbTotal() {
        $query = "SELECT COUNT(DISTINCT ".DOCUMENT_COACHING_TABLE_NAME.".".DOCUMENT_COACHING_ID.") AS ".DOCUMENT_NB_DOCUMENTS_COACHING." ";
        $query .= "FROM ".DOCUMENT_COACHING_TABLE_NAME;
        $columns = array(
            DOCUMENT_NB_DOCUMENTS_COACHING
        );
        return $this->executeGetUniqueRequest($query, null, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".DOCUMENT_COACHING_TABLE_NAME." (";
        $query .= DOCUMENT_COACHING_TITRE.", ";
        $query .= DOCUMENT_COACHING_MOTS_CLEFS.", ";
        $query .= DOCUMENT_COACHING_FICHIER.", ";
        $query .= DOCUMENT_COACHING_URL.", ";
        $query .= DOCUMENT_COACHING_NOM_EDITEUR.", ";
        $query .= DOCUMENT_COACHING_DATE_HEURE_MODIFICATION.", ";
        $query .= DOCUMENT_COACHING_TYPE_DOCUMENT_COACHING_ID.") VALUES (";
        $query .= ":".DOCUMENT_COACHING_TITRE.", ";
        $query .= ":".DOCUMENT_COACHING_MOTS_CLEFS.", ";
        $query .= ":".DOCUMENT_COACHING_FICHIER.", ";
        $query .= ":".DOCUMENT_COACHING_URL.", ";
        $query .= ":".DOCUMENT_COACHING_NOM_EDITEUR.", ";
        $query .= ":".DOCUMENT_COACHING_DATE_HEURE_MODIFICATION.", ";
        $query .= ":".DOCUMENT_COACHING_TYPE_DOCUMENT_COACHING_ID.")";
        $params = array(
            DOCUMENT_COACHING_TITRE => $object[DOCUMENT_COACHING_TITRE],
            DOCUMENT_COACHING_MOTS_CLEFS => $object[DOCUMENT_COACHING_MOTS_CLEFS],
            DOCUMENT_COACHING_FICHIER => $object[DOCUMENT_COACHING_FICHIER],
            DOCUMENT_COACHING_URL => $object[DOCUMENT_COACHING_URL],
            DOCUMENT_COACHING_NOM_EDITEUR => $object[DOCUMENT_COACHING_NOM_EDITEUR],
            DOCUMENT_COACHING_DATE_HEURE_MODIFICATION => $object[DOCUMENT_COACHING_DATE_HEURE_MODIFICATION],
            DOCUMENT_COACHING_TYPE_DOCUMENT_COACHING_ID => $object[DOCUMENT_COACHING_TYPE_DOCUMENT_COACHING_ID]
        );
        return $this->executeCreateRequest($query, $params);}
	
	public function update($object) {
        $query = "UPDATE ".DOCUMENT_COACHING_TABLE_NAME." SET ";
        $query .= DOCUMENT_COACHING_TITRE." = :".DOCUMENT_COACHING_TITRE.", ";
        $query .= DOCUMENT_COACHING_MOTS_CLEFS." = :".DOCUMENT_COACHING_MOTS_CLEFS.", ";
        $query .= DOCUMENT_COACHING_FICHIER." = :".DOCUMENT_COACHING_FICHIER.", ";
        $query .= DOCUMENT_COACHING_URL." = :".DOCUMENT_COACHING_URL.", ";
        $query .= DOCUMENT_COACHING_NOM_EDITEUR." = :".DOCUMENT_COACHING_NOM_EDITEUR.", ";
        $query .= DOCUMENT_COACHING_DATE_HEURE_MODIFICATION." = :".DOCUMENT_COACHING_DATE_HEURE_MODIFICATION.", ";
        $query .= DOCUMENT_COACHING_TYPE_DOCUMENT_COACHING_ID." = :".DOCUMENT_COACHING_TYPE_DOCUMENT_COACHING_ID." WHERE ";
        $query .= DOCUMENT_COACHING_ID." = :".DOCUMENT_COACHING_ID;
        $params = array(
            DOCUMENT_COACHING_TITRE => $object[DOCUMENT_COACHING_TITRE],
            DOCUMENT_COACHING_MOTS_CLEFS => $object[DOCUMENT_COACHING_MOTS_CLEFS],
            DOCUMENT_COACHING_FICHIER => $object[DOCUMENT_COACHING_FICHIER],
            DOCUMENT_COACHING_URL => $object[DOCUMENT_COACHING_URL],
            DOCUMENT_COACHING_NOM_EDITEUR => $object[DOCUMENT_COACHING_NOM_EDITEUR],
            DOCUMENT_COACHING_DATE_HEURE_MODIFICATION => $object[DOCUMENT_COACHING_DATE_HEURE_MODIFICATION],
            DOCUMENT_COACHING_TYPE_DOCUMENT_COACHING_ID => $object[DOCUMENT_COACHING_TYPE_DOCUMENT_COACHING_ID],
            DOCUMENT_COACHING_ID => $object[DOCUMENT_COACHING_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".DOCUMENT_COACHING_TABLE_NAME." WHERE ".DOCUMENT_COACHING_ID." = :".DOCUMENT_COACHING_ID;
        $params = array(
            DOCUMENT_COACHING_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>