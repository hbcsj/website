<?php
require_once("core/php/lib/abstract-dao.php");

define(TYPE_PAIEMENT_TABLE_NAME, "hbcsj_type_paiement");
define(TYPE_PAIEMENT_ID, "id");
define(TYPE_PAIEMENT_LIBELLE, "libelle");

class TypePaiementDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(TYPE_PAIEMENT_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".TYPE_PAIEMENT_TABLE_NAME." WHERE ".TYPE_PAIEMENT_ID." = :".TYPE_PAIEMENT_ID;
        $params = array(
            TYPE_PAIEMENT_ID => $id
        );
        $columns = array(TYPE_PAIEMENT_ID, TYPE_PAIEMENT_LIBELLE);
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".TYPE_PAIEMENT_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(TYPE_PAIEMENT_ID, TYPE_PAIEMENT_LIBELLE);
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {}
	
	public function update($object) {}
	
	public function delete($id) {}
}

?>