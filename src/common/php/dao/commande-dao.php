<?php
require_once("core/php/lib/abstract-dao.php");

define(COMMANDE_TABLE_NAME, "hbcsj_commande");
define(COMMANDE_ID, "id");
define(COMMANDE_REFERENCE, "reference");
define(COMMANDE_DATE_HEURE, "date_heure");
define(COMMANDE_NOM, "nom");
define(COMMANDE_PRENOM, "prenom");
define(COMMANDE_EMAIL, "email");
define(COMMANDE_TELEPHONE, "telephone");
define(COMMANDE_PAYE, "paye");
define(COMMANDE_SAISON_ID, "saison_id");

define(COMMANDE_DATE_HEURE_DEBUT, "date_heure_debut");
define(COMMANDE_DATE_HEURE_FIN, "date_heure_fin");
define(COMMANDE_NB_TOTAL, "nb_total");

class CommandeDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(COMMANDE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".COMMANDE_TABLE_NAME." WHERE ".COMMANDE_ID." = :".COMMANDE_ID;
        $params = array(
            COMMANDE_ID => $id
        );
        $columns = array(
            COMMANDE_ID, 
            COMMANDE_REFERENCE, 
            COMMANDE_DATE_HEURE,
            COMMANDE_NOM,
            COMMANDE_PRENOM,
            COMMANDE_EMAIL,
            COMMANDE_TELEPHONE,
            COMMANDE_PAYE,
            COMMANDE_SAISON_ID
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".COMMANDE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            COMMANDE_ID, 
            COMMANDE_REFERENCE, 
            COMMANDE_DATE_HEURE,
            COMMANDE_NOM,
            COMMANDE_PRENOM,
            COMMANDE_EMAIL,
            COMMANDE_TELEPHONE,
            COMMANDE_PAYE,
            COMMANDE_SAISON_ID
        );
        return $this->executeGetRequest($query, null, $columns);
    }

	public function getAllByParams($reference, $nom, $prenom, $dateDebut, $dateFin, $paye, $saisonId, $orderBy = null) {
        $query = "SELECT * FROM ".COMMANDE_TABLE_NAME." ";

        $queryParamsArray = array();
        $params = array();
        if ($reference != null && $reference != "") {
            $queryParamsArray[] = COMMANDE_REFERENCE." LIKE :".COMMANDE_REFERENCE;
            $params[COMMANDE_REFERENCE] = "%".$reference."%";
        }
        if ($nom != null && $nom != "") {
            $queryParamsArray[] = COMMANDE_NOM." LIKE :".COMMANDE_NOM;
            $params[COMMANDE_NOM] = "%".$nom."%";
        }
        if ($prenom != null && $prenom != "") {
            $queryParamsArray[] = COMMANDE_PRENOM." LIKE :".COMMANDE_PRENOM;
            $params[COMMANDE_PRENOM] = "%".$prenom."%";
        }
        if ($dateDebut != null && $dateDebut != "") {
            $queryParamsArray[] = COMMANDE_DATE_HEURE." >= :".COMMANDE_DATE_HEURE_DEBUT;
            $params[COMMANDE_DATE_HEURE_DEBUT] = $dateDebut;
        }
        if ($dateFin != null && $dateFin != "") {
            $queryParamsArray[] = COMMANDE_DATE_HEURE." <= :".COMMANDE_DATE_HEURE_FIN;
            $params[COMMANDE_DATE_HEURE_FIN] = $dateFin;
        }
        if ($paye != null && $paye != "") {
            if ($paye == 1) {
                $queryParamsArray[] = "(".COMMANDE_PAYE." IS NOT NULL AND ".COMMANDE_PAYE." != '')";
            } else if ($paye == 0) {
                $queryParamsArray[] = "(".COMMANDE_PAYE." IS NULL OR ".COMMANDE_PAYE." = '')";
            }
        }
        if ($saisonId != null && $saisonId != "") {
            $queryParamsArray[] = COMMANDE_SAISON_ID." = :".COMMANDE_SAISON_ID;
            $params[COMMANDE_SAISON_ID] = $saisonId;
        }
        
        if (sizeof($queryParamsArray) > 0) {
            $query .= " WHERE ".implode(" AND ", $queryParamsArray);
        }

        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            COMMANDE_ID, 
            COMMANDE_REFERENCE, 
            COMMANDE_DATE_HEURE,
            COMMANDE_NOM,
            COMMANDE_PRENOM,
            COMMANDE_EMAIL,
            COMMANDE_TELEPHONE,
            COMMANDE_PAYE,
            COMMANDE_SAISON_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }

    public function getPersonnesCommandes() {
        $query = "SELECT DISTINCT ".COMMANDE_TABLE_NAME.".".COMMANDE_NOM.", ".COMMANDE_TABLE_NAME.".".COMMANDE_PRENOM." ";
        $query .= "FROM ".COMMANDE_TABLE_NAME." ";
        $query .= " ORDER BY ".COMMANDE_TABLE_NAME.".".COMMANDE_NOM.", ".COMMANDE_TABLE_NAME.".".COMMANDE_PRENOM;
        $columns = array(
            COMMANDE_NOM,
            COMMANDE_PRENOM
        );
        return $this->executeGetRequest($query, null, $columns);
    }

    public function getByPaiementId($paiementId, $orderBy = null) {
        require_once("common/php/dao/paiement-commande-dao.php");

        $query = "SELECT * FROM ".COMMANDE_TABLE_NAME.", ".PAIEMENT_COMMANDE_TABLE_NAME." ";
        $query .= "WHERE ".COMMANDE_TABLE_NAME.".".COMMANDE_ID." = ".PAIEMENT_COMMANDE_TABLE_NAME.".".PAIEMENT_COMMANDE_COMMANDE_ID." ";
        $query .= "AND ".PAIEMENT_COMMANDE_TABLE_NAME.".".PAIEMENT_COMMANDE_PAIEMENT_ID." = :".PAIEMENT_COMMANDE_PAIEMENT_ID;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $params = array(
            PAIEMENT_COMMANDE_PAIEMENT_ID => $paiementId
        );
        $columns = array(
            COMMANDE_ID, 
            COMMANDE_REFERENCE, 
            COMMANDE_DATE_HEURE,
            COMMANDE_NOM,
            COMMANDE_PRENOM,
            COMMANDE_EMAIL,
            COMMANDE_TELEPHONE,
            COMMANDE_PAYE,
            COMMANDE_SAISON_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }

    public function getNotLinkedToPaiementByPaiementId($paiementId, $orderBy = null) {
        require_once("common/php/dao/paiement-commande-dao.php");

        $query = "SELECT * FROM ".COMMANDE_TABLE_NAME." ";
        $query .= "WHERE ".COMMANDE_TABLE_NAME.".".COMMANDE_ID." NOT IN (";
        $query .= "SELECT ".PAIEMENT_COMMANDE_TABLE_NAME.".".PAIEMENT_COMMANDE_COMMANDE_ID." ";
        $query .= "FROM ".PAIEMENT_COMMANDE_TABLE_NAME." ";
        $query .= "WHERE ".PAIEMENT_COMMANDE_PAIEMENT_ID." = :".PAIEMENT_COMMANDE_PAIEMENT_ID;
        $query .= ")";
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $params = array(
            PAIEMENT_COMMANDE_PAIEMENT_ID => $paiementId
        );
        $columns = array(
            COMMANDE_ID, 
            COMMANDE_REFERENCE, 
            COMMANDE_DATE_HEURE,
            COMMANDE_NOM,
            COMMANDE_PRENOM,
            COMMANDE_EMAIL,
            COMMANDE_TELEPHONE,
            COMMANDE_PAYE,
            COMMANDE_SAISON_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }

    public function getNbLinkedToCommandeItemBilletterie($dateDebut) {
        require_once("common/php/dao/commande-item-billetterie-dao.php");

        $query = "SELECT COUNT(DISTINCT ".COMMANDE_TABLE_NAME.".".COMMANDE_ID.") AS ".COMMANDE_NB_TOTAL." FROM ".COMMANDE_TABLE_NAME." ";
        $query .= "WHERE ".COMMANDE_TABLE_NAME.".".COMMANDE_ID." IN (";
        $query .= "SELECT ".COMMANDE_ITEM_BILLETTERIE_TABLE_NAME.".".COMMANDE_ITEM_BILLETTERIE_COMMANDE_ID." ";
        $query .= "FROM ".COMMANDE_ITEM_BILLETTERIE_TABLE_NAME;
        $query .= ") ";
        $query .= "AND ".COMMANDE_TABLE_NAME.".".COMMANDE_DATE_HEURE." >= :".COMMANDE_DATE_HEURE;
        $params = array(
            COMMANDE_DATE_HEURE => $dateDebut
        );
        $columns = array(
            COMMANDE_NB_TOTAL
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }

    public function getNbLinkedToCommandeItemBoutique($dateDebut) {
        require_once("common/php/dao/commande-item-boutique-dao.php");

        $query = "SELECT COUNT(DISTINCT ".COMMANDE_TABLE_NAME.".".COMMANDE_ID.") AS ".COMMANDE_NB_TOTAL." FROM ".COMMANDE_TABLE_NAME." ";
        $query .= "WHERE ".COMMANDE_TABLE_NAME.".".COMMANDE_ID." IN (";
        $query .= "SELECT ".COMMANDE_ITEM_BOUTIQUE_TABLE_NAME.".".COMMANDE_ITEM_BOUTIQUE_COMMANDE_ID." ";
        $query .= "FROM ".COMMANDE_ITEM_BOUTIQUE_TABLE_NAME;
        $query .= ") ";
        $query .= "AND ".COMMANDE_TABLE_NAME.".".COMMANDE_DATE_HEURE." >= :".COMMANDE_DATE_HEURE;
        $params = array(
            COMMANDE_DATE_HEURE => $dateDebut
        );
        $columns = array(
            COMMANDE_NB_TOTAL
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".COMMANDE_TABLE_NAME." (";
        $query .= COMMANDE_REFERENCE.", ";
        $query .= COMMANDE_DATE_HEURE.", ";
        $query .= COMMANDE_NOM.", ";
        $query .= COMMANDE_PRENOM.", ";
        $query .= COMMANDE_EMAIL.", ";
        $query .= COMMANDE_TELEPHONE.", ";
        $query .= COMMANDE_PAYE.", ";
        $query .= COMMANDE_SAISON_ID.") VALUES (";
        $query .= ":".COMMANDE_REFERENCE.", ";
        $query .= ":".COMMANDE_DATE_HEURE.", ";
        $query .= ":".COMMANDE_NOM.", ";
        $query .= ":".COMMANDE_PRENOM.", ";
        $query .= ":".COMMANDE_EMAIL.", ";
        $query .= ":".COMMANDE_TELEPHONE.", ";
        $query .= ":".COMMANDE_PAYE.", ";
        $query .= ":".COMMANDE_SAISON_ID.")";
        $params = array(
            COMMANDE_REFERENCE => $object[COMMANDE_REFERENCE],
            COMMANDE_DATE_HEURE => $object[COMMANDE_DATE_HEURE],
            COMMANDE_NOM => $object[COMMANDE_NOM],
            COMMANDE_PRENOM => $object[COMMANDE_PRENOM],
            COMMANDE_EMAIL => $object[COMMANDE_EMAIL],
            COMMANDE_TELEPHONE => $object[COMMANDE_TELEPHONE],
            COMMANDE_PAYE => $object[COMMANDE_PAYE],
            COMMANDE_SAISON_ID => $object[COMMANDE_SAISON_ID]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {
        $query = "UPDATE ".COMMANDE_TABLE_NAME." SET ";
        $query .= COMMANDE_REFERENCE." = :".COMMANDE_REFERENCE.", ";
        $query .= COMMANDE_DATE_HEURE." = :".COMMANDE_DATE_HEURE.", ";
        $query .= COMMANDE_NOM." = :".COMMANDE_NOM.", ";
        $query .= COMMANDE_PRENOM." = :".COMMANDE_PRENOM.", ";
        $query .= COMMANDE_EMAIL." = :".COMMANDE_EMAIL.", ";
        $query .= COMMANDE_TELEPHONE." = :".COMMANDE_TELEPHONE.", ";
        $query .= COMMANDE_PAYE." = :".COMMANDE_PAYE.", ";
        $query .= COMMANDE_SAISON_ID." = :".COMMANDE_SAISON_ID." WHERE ";
        $query .= COMMANDE_ID." = :".COMMANDE_ID;
        $params = array(
            COMMANDE_REFERENCE => $object[COMMANDE_REFERENCE],
            COMMANDE_DATE_HEURE => $object[COMMANDE_DATE_HEURE],
            COMMANDE_NOM => $object[COMMANDE_NOM],
            COMMANDE_PRENOM => $object[COMMANDE_PRENOM],
            COMMANDE_EMAIL => $object[COMMANDE_EMAIL],
            COMMANDE_TELEPHONE => $object[COMMANDE_TELEPHONE],
            COMMANDE_PAYE => $object[COMMANDE_PAYE],
            COMMANDE_SAISON_ID => $object[COMMANDE_SAISON_ID],
            COMMANDE_ID => $object[COMMANDE_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".COMMANDE_TABLE_NAME." WHERE ".COMMANDE_ID." = :".COMMANDE_ID;
        $params = array(
            COMMANDE_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>