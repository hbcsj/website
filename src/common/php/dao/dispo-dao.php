<?php
require_once("core/php/lib/abstract-dao.php");

define(DISPO_TABLE_NAME, "hbcsj_dispo");
define(DISPO_LICENCIE_ID, "licencie_id");
define(DISPO_EVENEMENT_ID, "evenement_id");
define(DISPO_TYPE_DISPO_ID, "type_dispo_id");
define(DISPO_REPONSE_DISPO_ID, "reponse_dispo_id");

define(DISPO_DISPONIBLE_REPONSE_ID, "1");
define(DISPO_SI_NECESSAIRE_REPONSE_ID, "2");
define(DISPO_ABSENT_REPONSE_ID, "3");
define(DISPO_PAS_DE_REPONSE_REPONSE_ID, "4");

define(DISPO_PARTICIPATION_TYPE_ID, "1");
define(DISPO_ARBITRAGE_TYPE_ID, "2");
define(DISPO_ACTION_CLUB_TYPE_ID, "3");

class DispoDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(DISPO_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".DISPO_TABLE_NAME." WHERE ".DISPO_LICENCIE_ID." = :".DISPO_LICENCIE_ID." AND ".DISPO_EVENEMENT_ID." = :".DISPO_EVENEMENT_ID." AND ".DISPO_TYPE_DISPO_ID." = :".DISPO_TYPE_DISPO_ID;
        $params = array(
            DISPO_LICENCIE_ID => $id[DISPO_LICENCIE_ID],
            DISPO_EVENEMENT_ID => $id[DISPO_EVENEMENT_ID],
            DISPO_TYPE_DISPO_ID => $id[DISPO_TYPE_DISPO_ID]
        );
        $columns = array(
            DISPO_LICENCIE_ID,
            DISPO_EVENEMENT_ID,
            DISPO_TYPE_DISPO_ID,
            DISPO_REPONSE_DISPO_ID
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".DISPO_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            DISPO_LICENCIE_ID,
            DISPO_EVENEMENT_ID,
            DISPO_TYPE_DISPO_ID,
            DISPO_REPONSE_DISPO_ID
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function getByLicencieIdAndTypeDispoId($licencieId, $typeDispoId, $orderBy = null) {
        $query = "SELECT * FROM ".DISPO_TABLE_NAME." WHERE ".DISPO_LICENCIE_ID." = :".DISPO_LICENCIE_ID." AND ".DISPO_TYPE_DISPO_ID." = :".DISPO_TYPE_DISPO_ID;
        $params = array(
            DISPO_LICENCIE_ID => $licencieId,
            DISPO_TYPE_DISPO_ID => $typeDispoId
        );
        $columns = array(
            DISPO_LICENCIE_ID,
            DISPO_EVENEMENT_ID,
            DISPO_TYPE_DISPO_ID,
            DISPO_REPONSE_DISPO_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }

    public function getByEvenementIdAndTypeDispoIdAndReponseId($evenementId, $typeDispoId, $reponseId) {
        $query = "SELECT * FROM ".DISPO_TABLE_NAME." WHERE ".DISPO_EVENEMENT_ID." = :".DISPO_EVENEMENT_ID." AND ".DISPO_TYPE_DISPO_ID." = :".DISPO_TYPE_DISPO_ID." AND ".DISPO_REPONSE_DISPO_ID." = :".DISPO_REPONSE_DISPO_ID;
        $params = array(
            DISPO_EVENEMENT_ID => $evenementId,
            DISPO_TYPE_DISPO_ID => $typeDispoId,
            DISPO_REPONSE_DISPO_ID => $reponseId
        );
        $columns = array(
            DISPO_LICENCIE_ID,
            DISPO_EVENEMENT_ID,
            DISPO_TYPE_DISPO_ID,
            DISPO_REPONSE_DISPO_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".DISPO_TABLE_NAME." (";
        $query .= DISPO_LICENCIE_ID.", ";
        $query .= DISPO_EVENEMENT_ID.", ";
        $query .= DISPO_TYPE_DISPO_ID.", ";
        $query .= DISPO_REPONSE_DISPO_ID.") VALUES (";
        $query .= ":".DISPO_LICENCIE_ID.", ";
        $query .= ":".DISPO_EVENEMENT_ID.", ";
        $query .= ":".DISPO_TYPE_DISPO_ID.", ";
        $query .= ":".DISPO_REPONSE_DISPO_ID.")";
        $params = array(
            DISPO_LICENCIE_ID => $object[DISPO_LICENCIE_ID],
            DISPO_EVENEMENT_ID => $object[DISPO_EVENEMENT_ID],
            DISPO_TYPE_DISPO_ID => $object[DISPO_TYPE_DISPO_ID],
            DISPO_REPONSE_DISPO_ID => $object[DISPO_REPONSE_DISPO_ID]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {}
	
	public function delete($id) {
        $query = "DELETE FROM ".DISPO_TABLE_NAME." WHERE ".DISPO_LICENCIE_ID." = :".DISPO_LICENCIE_ID." AND ".DISPO_EVENEMENT_ID." = :".DISPO_EVENEMENT_ID." AND ".DISPO_TYPE_DISPO_ID." = :".DISPO_TYPE_DISPO_ID;
        $params = array(
            DISPO_LICENCIE_ID => $id[DISPO_LICENCIE_ID],
            DISPO_EVENEMENT_ID => $id[DISPO_EVENEMENT_ID],
            DISPO_TYPE_DISPO_ID => $id[DISPO_TYPE_DISPO_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function deleteByLicencieId($licencieId) {
        $query = "DELETE FROM ".DISPO_TABLE_NAME." WHERE ".DISPO_LICENCIE_ID." = :".DISPO_LICENCIE_ID;
        $params = array(
            DISPO_LICENCIE_ID => $licencieId
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function deleteByEvenementId($evenementId) {
        $query = "DELETE FROM ".DISPO_TABLE_NAME." WHERE ".DISPO_EVENEMENT_ID." = :".DISPO_EVENEMENT_ID;
        $params = array(
            DISPO_EVENEMENT_ID => $evenementId
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>