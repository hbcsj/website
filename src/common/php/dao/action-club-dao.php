<?php
require_once("core/php/lib/abstract-dao.php");

define(ACTION_CLUB_TABLE_NAME, "hbcsj_action_club");
define(ACTION_CLUB_TYPE_ACTION_CLUB_ID, "type_action_club_id");
define(ACTION_CLUB_LICENCIE_ID, "licencie_id");
define(ACTION_CLUB_EVENEMENT_ID, "evenement_id");

define(ACTION_CLUB_DATE_EVENEMENT, "date_evenement");

class ActionClubDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(ACTION_CLUB_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".ACTION_CLUB_TABLE_NAME." WHERE ".ACTION_CLUB_TYPE_ACTION_CLUB_ID." = :".ACTION_CLUB_TYPE_ACTION_CLUB_ID." AND ".ACTION_CLUB_LICENCIE_ID." = :".ACTION_CLUB_LICENCIE_ID." AND ".ACTION_CLUB_EVENEMENT_ID." = :".ACTION_CLUB_EVENEMENT_ID;
        $params = array(
            ACTION_CLUB_TYPE_ACTION_CLUB_ID => $id[ACTION_CLUB_TYPE_ACTION_CLUB_ID],
            ACTION_CLUB_LICENCIE_ID => $id[ACTION_CLUB_LICENCIE_ID],
            ACTION_CLUB_EVENEMENT_ID => $id[ACTION_CLUB_EVENEMENT_ID]
        );
        $columns = array(ACTION_CLUB_TYPE_ACTION_CLUB_ID, ACTION_CLUB_LICENCIE_ID, ACTION_CLUB_EVENEMENT_ID);
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".ACTION_CLUB_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(ACTION_CLUB_TYPE_ACTION_CLUB_ID, ACTION_CLUB_LICENCIE_ID, ACTION_CLUB_EVENEMENT_ID);
        return $this->executeGetRequest($query, null, $columns);
    }

    public function getByLicencieIdAndDateDebutSaison($licencieId, $dateDebutSaison) {
        require_once("common/php/dao/evenement-dao.php");

        $query = "SELECT * FROM ".ACTION_CLUB_TABLE_NAME.", ".EVENEMENT_TABLE_NAME." ";
        $query .= "WHERE ".ACTION_CLUB_LICENCIE_ID." = :".ACTION_CLUB_LICENCIE_ID." ";
        $query .= "AND ".EVENEMENT_TABLE_NAME.".".EVENEMENT_DATE_HEURE." >= :".ACTION_CLUB_DATE_EVENEMENT." ";
        $query .= "AND ".ACTION_CLUB_TABLE_NAME.".".ACTION_CLUB_EVENEMENT_ID." = ".EVENEMENT_TABLE_NAME.".".EVENEMENT_ID." ";
        $query .= "ORDER BY ".EVENEMENT_TABLE_NAME.".".EVENEMENT_DATE_HEURE;
        $params = array(
            ACTION_CLUB_LICENCIE_ID => $licencieId,
            ACTION_CLUB_DATE_EVENEMENT => $dateDebutSaison
        );
        $columns = array(ACTION_CLUB_TYPE_ACTION_CLUB_ID, ACTION_CLUB_LICENCIE_ID, ACTION_CLUB_EVENEMENT_ID);
        return $this->executeGetRequest($query, $params, $columns);
    }

    public function getByEvenementId($evenementId) {
        $query = "SELECT * FROM ".ACTION_CLUB_TABLE_NAME." WHERE ".ACTION_CLUB_EVENEMENT_ID." = :".ACTION_CLUB_EVENEMENT_ID;
        $params = array(
            ACTION_CLUB_EVENEMENT_ID => $evenementId
        );
        $columns = array(ACTION_CLUB_TYPE_ACTION_CLUB_ID, ACTION_CLUB_LICENCIE_ID, ACTION_CLUB_EVENEMENT_ID);
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".ACTION_CLUB_TABLE_NAME." (".ACTION_CLUB_TYPE_ACTION_CLUB_ID.", ".ACTION_CLUB_LICENCIE_ID.", ".ACTION_CLUB_EVENEMENT_ID.") VALUES (:".ACTION_CLUB_TYPE_ACTION_CLUB_ID.", :".ACTION_CLUB_LICENCIE_ID.", :".ACTION_CLUB_EVENEMENT_ID.");";
        $params = array(
            ACTION_CLUB_TYPE_ACTION_CLUB_ID => $object[ACTION_CLUB_TYPE_ACTION_CLUB_ID],
            ACTION_CLUB_LICENCIE_ID => $object[ACTION_CLUB_LICENCIE_ID],
            ACTION_CLUB_EVENEMENT_ID => $object[ACTION_CLUB_EVENEMENT_ID]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {}
	
	public function delete($id) {
        $query = "DELETE FROM ".ACTION_CLUB_TABLE_NAME." WHERE ".ACTION_CLUB_TYPE_ACTION_CLUB_ID." = :".ACTION_CLUB_TYPE_ACTION_CLUB_ID." AND ".ACTION_CLUB_LICENCIE_ID." = :".ACTION_CLUB_LICENCIE_ID." AND ".ACTION_CLUB_EVENEMENT_ID." = :".ACTION_CLUB_EVENEMENT_ID;
        $params = array(
            ACTION_CLUB_TYPE_ACTION_CLUB_ID => $id[ACTION_CLUB_TYPE_ACTION_CLUB_ID],
            ACTION_CLUB_LICENCIE_ID => $id[ACTION_CLUB_LICENCIE_ID],
            ACTION_CLUB_EVENEMENT_ID => $id[ACTION_CLUB_EVENEMENT_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function deleteByLicencieId($licencieId) {
        $query = "DELETE FROM ".ACTION_CLUB_TABLE_NAME." WHERE ".ACTION_CLUB_LICENCIE_ID." = :".ACTION_CLUB_LICENCIE_ID;
        $params = array(
            ACTION_CLUB_LICENCIE_ID => $licencieId
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function deleteByEvenementId($evenementId) {
        $query = "DELETE FROM ".ACTION_CLUB_TABLE_NAME." WHERE ".ACTION_CLUB_EVENEMENT_ID." = :".ACTION_CLUB_EVENEMENT_ID;
        $params = array(
            ACTION_CLUB_EVENEMENT_ID => $evenementId
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>