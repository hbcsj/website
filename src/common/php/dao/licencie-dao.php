<?php
require_once("core/php/lib/abstract-dao.php");

define(LICENCIE_TABLE_NAME, "hbcsj_licencie");
define(LICENCIE_ID, "id");
define(LICENCIE_NOM, "nom");
define(LICENCIE_PRENOM, "prenom");
define(LICENCIE_DATE_NAISSANCE, "date_naissance");
define(LICENCIE_ADRESSE, "adresse");
define(LICENCIE_CODE_POSTAL, "code_postal");
define(LICENCIE_VILLE, "ville");
define(LICENCIE_TELEPHONE_1, "telephone_1");
define(LICENCIE_TELEPHONE_2, "telephone_2");
define(LICENCIE_TELEPHONE_3, "telephone_3");
define(LICENCIE_EMAIL_1, "email_1");
define(LICENCIE_EMAIL_2, "email_2");
define(LICENCIE_EMAIL_3, "email_3");
define(LICENCIE_GARDIEN, "gardien");
define(LICENCIE_ARBITRE, "arbitre");
define(LICENCIE_BUREAU, "bureau");
define(LICENCIE_CA, "ca");
define(LICENCIE_ACTIF, "actif");
define(LICENCIE_LATERALITE_ID, "lateralite_id");

define(LICENCIE_NB_LICENCIES, "nb_licencies");

class LicencieDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(LICENCIE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".LICENCIE_TABLE_NAME." WHERE ".LICENCIE_ID." = :".LICENCIE_ID;
        $params = array(
            LICENCIE_ID => $id
        );
        $columns = array(
            LICENCIE_ID, 
            LICENCIE_NOM, 
            LICENCIE_PRENOM, 
            LICENCIE_DATE_NAISSANCE, 
            LICENCIE_ADRESSE, 
            LICENCIE_CODE_POSTAL, 
            LICENCIE_VILLE, 
            LICENCIE_TELEPHONE_1, 
            LICENCIE_TELEPHONE_2, 
            LICENCIE_TELEPHONE_3,
            LICENCIE_EMAIL_1,
            LICENCIE_EMAIL_2,
            LICENCIE_EMAIL_3,
            LICENCIE_GARDIEN,
            LICENCIE_ARBITRE,
            LICENCIE_BUREAU,
            LICENCIE_CA,
            LICENCIE_ACTIF,
            LICENCIE_LATERALITE_ID
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".LICENCIE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            LICENCIE_ID, 
            LICENCIE_NOM, 
            LICENCIE_PRENOM, 
            LICENCIE_DATE_NAISSANCE, 
            LICENCIE_ADRESSE, 
            LICENCIE_CODE_POSTAL, 
            LICENCIE_VILLE, 
            LICENCIE_TELEPHONE_1, 
            LICENCIE_TELEPHONE_2, 
            LICENCIE_TELEPHONE_3,
            LICENCIE_EMAIL_1,
            LICENCIE_EMAIL_2,
            LICENCIE_EMAIL_3,
            LICENCIE_GARDIEN,
            LICENCIE_ARBITRE,
            LICENCIE_BUREAU,
            LICENCIE_CA,
            LICENCIE_ACTIF,
            LICENCIE_LATERALITE_ID
        );
        return $this->executeGetRequest($query, null, $columns);
    }

    public function getNesADate($dateNaissance, $orderBy = null) {
        $query = "SELECT * FROM ".LICENCIE_TABLE_NAME." WHERE ".LICENCIE_DATE_NAISSANCE." LIKE :".LICENCIE_DATE_NAISSANCE;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $params = array(
            LICENCIE_DATE_NAISSANCE => "%".substr($dateNaissance, 5)
        );
        $columns = array(
            LICENCIE_ID, 
            LICENCIE_NOM, 
            LICENCIE_PRENOM, 
            LICENCIE_DATE_NAISSANCE, 
            LICENCIE_ADRESSE, 
            LICENCIE_CODE_POSTAL, 
            LICENCIE_VILLE, 
            LICENCIE_TELEPHONE_1, 
            LICENCIE_TELEPHONE_2, 
            LICENCIE_TELEPHONE_3,
            LICENCIE_EMAIL_1,
            LICENCIE_EMAIL_2,
            LICENCIE_EMAIL_3,
            LICENCIE_GARDIEN,
            LICENCIE_ARBITRE,
            LICENCIE_BUREAU,
            LICENCIE_CA,
            LICENCIE_ACTIF,
            LICENCIE_LATERALITE_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }

	public function getAllByParams($nom, $prenom, $email, $categorieId, $gardien, $arbitre, $bureau, $ca, $actif, $anneeNaissance, $orderBy = null) {
        require_once("common/php/dao/licencie-joue-dans-categorie-dao.php");

        $query = "SELECT DISTINCT ".LICENCIE_TABLE_NAME.".* FROM ".LICENCIE_TABLE_NAME." ";
        if ($categorieId != null && $categorieId != "") {
            $query .= ", ".LICENCIE_JOUE_DANS_CATEGORIE_TABLE_NAME." ";
        }

        $queryParamsArray = array();
        $params = array();
        if ($nom != null && $nom != "") {
            $queryParamsArray[] = LICENCIE_NOM." LIKE :".LICENCIE_NOM;
            $params[LICENCIE_NOM] = "%".$nom."%";
        }
        if ($prenom != null && $prenom != "") {
            $queryParamsArray[] = LICENCIE_PRENOM." LIKE :".LICENCIE_PRENOM;
            $params[LICENCIE_PRENOM] = "%".$prenom."%";
        }
        if ($email != null && $email != "") {
            $queryParamsArray[] = "(".LICENCIE_EMAIL_1." LIKE :".LICENCIE_EMAIL_1." OR ".LICENCIE_EMAIL_2." LIKE :".LICENCIE_EMAIL_2." OR ".LICENCIE_EMAIL_3." LIKE :".LICENCIE_EMAIL_3.")";
            $params[LICENCIE_EMAIL_1] = "%".$email."%";
            $params[LICENCIE_EMAIL_2] = "%".$email."%";
            $params[LICENCIE_EMAIL_3] = "%".$email."%";
        }
        if ($categorieId != null && $categorieId != "") {
            $queryParamsArray[] = LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID." = :".LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID;
            $params[LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID] = $categorieId;
        }
        if ($gardien != null && $gardien != "") {
            if ($gardien == 1) {
                $queryParamsArray[] = "(".LICENCIE_GARDIEN." IS NOT NULL AND ".LICENCIE_GARDIEN." != '')";
            } else if ($gardien == 0) {
                $queryParamsArray[] = "(".LICENCIE_GARDIEN." IS NULL OR ".LICENCIE_GARDIEN." = '')";
            }
        }
        if ($arbitre != null && $arbitre != "") {
            if ($arbitre == 1) {
                $queryParamsArray[] = "(".LICENCIE_ARBITRE." IS NOT NULL AND ".LICENCIE_ARBITRE." != '')";
            } else if ($arbitre == 0) {
                $queryParamsArray[] = "(".LICENCIE_ARBITRE." IS NULL OR ".LICENCIE_ARBITRE." = '')";
            }
        }
        if ($bureau != null && $bureau != "") {
            if ($bureau == 1) {
                $queryParamsArray[] = "(".LICENCIE_BUREAU." IS NOT NULL AND ".LICENCIE_BUREAU." != '')";
            } else if ($bureau == 0) {
                $queryParamsArray[] = "(".LICENCIE_BUREAU." IS NULL OR ".LICENCIE_BUREAU." = '')";
            }
        }
        if ($ca != null && $ca != "") {
            if ($ca == 1) {
                $queryParamsArray[] = "(".LICENCIE_CA." IS NOT NULL AND ".LICENCIE_CA." != '')";
            } else if ($ca == 0) {
                $queryParamsArray[] = "(".LICENCIE_CA." IS NULL OR ".LICENCIE_CA." = '')";
            }
        }
        if ($actif != null && $actif != "") {
            if ($actif == 1) {
                $queryParamsArray[] = "(".LICENCIE_ACTIF." IS NOT NULL AND ".LICENCIE_ACTIF." != '')";
            } else if ($actif == 0) {
                $queryParamsArray[] = "(".LICENCIE_ACTIF." IS NULL OR ".LICENCIE_ACTIF." = '')";
            }
        }
        if ($anneeNaissance != null && $anneeNaissance != "") {
            $queryParamsArray[] = LICENCIE_DATE_NAISSANCE." LIKE :".LICENCIE_DATE_NAISSANCE;
            $params[LICENCIE_DATE_NAISSANCE] = $anneeNaissance."-%";
        }
        if (sizeof($queryParamsArray) > 0) {
            $query .= " WHERE ";
            if ($categorieId != null && $categorieId != "") {
                $query .= LICENCIE_TABLE_NAME.".".LICENCIE_ID." = ".LICENCIE_JOUE_DANS_CATEGORIE_TABLE_NAME.".".LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID." AND ";
            }
        }
        if (sizeof($queryParamsArray) > 0) {
            $query .= implode(" AND ", $queryParamsArray);
        }

        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            LICENCIE_ID, 
            LICENCIE_NOM, 
            LICENCIE_PRENOM, 
            LICENCIE_DATE_NAISSANCE, 
            LICENCIE_ADRESSE, 
            LICENCIE_CODE_POSTAL, 
            LICENCIE_VILLE, 
            LICENCIE_TELEPHONE_1, 
            LICENCIE_TELEPHONE_2, 
            LICENCIE_TELEPHONE_3,
            LICENCIE_EMAIL_1,
            LICENCIE_EMAIL_2,
            LICENCIE_EMAIL_3,
            LICENCIE_GARDIEN,
            LICENCIE_ARBITRE,
            LICENCIE_BUREAU,
            LICENCIE_CA,
            LICENCIE_ACTIF,
            LICENCIE_LATERALITE_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function getInIds($ids, $orderBy = null) {
        $query = "SELECT * FROM ".LICENCIE_TABLE_NAME." WHERE ".LICENCIE_ID." IN (".$ids.")";
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            LICENCIE_ID, 
            LICENCIE_NOM, 
            LICENCIE_PRENOM, 
            LICENCIE_DATE_NAISSANCE, 
            LICENCIE_ADRESSE, 
            LICENCIE_CODE_POSTAL, 
            LICENCIE_VILLE, 
            LICENCIE_TELEPHONE_1, 
            LICENCIE_TELEPHONE_2, 
            LICENCIE_TELEPHONE_3,
            LICENCIE_EMAIL_1,
            LICENCIE_EMAIL_2,
            LICENCIE_EMAIL_3,
            LICENCIE_GARDIEN,
            LICENCIE_ARBITRE,
            LICENCIE_BUREAU,
            LICENCIE_CA,
            LICENCIE_ACTIF,
            LICENCIE_LATERALITE_ID
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function getNbTotalActifs() {
        $query = "SELECT COUNT(DISTINCT ".LICENCIE_TABLE_NAME.".".LICENCIE_ID.") AS ".LICENCIE_NB_LICENCIES." ";
        $query .= "FROM ".LICENCIE_TABLE_NAME." ";
        $query .= "WHERE ".LICENCIE_ACTIF." = 1";
        $columns = array(
            LICENCIE_NB_LICENCIES
        );
        return $this->executeGetUniqueRequest($query, null, $columns);
    }
    
    public function getJoueursAndCoachsByCategorieId($categorieId, $orderBy = null) {
        require_once("common/php/dao/licencie-joue-dans-categorie-dao.php");
        require_once("common/php/dao/licencie-coache-categorie-dao.php");
        require_once("common/php/dao/dispo-dao.php");

        $query = "SELECT DISTINCT ".LICENCIE_TABLE_NAME.".* FROM ".LICENCIE_TABLE_NAME.", ".LICENCIE_JOUE_DANS_CATEGORIE_TABLE_NAME.", ".LICENCIE_COACHE_CATEGORIE_TABLE_NAME." ";
        $query .= "WHERE (".LICENCIE_JOUE_DANS_CATEGORIE_TABLE_NAME.".".LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID." = ".LICENCIE_TABLE_NAME.".".LICENCIE_ID." ";
        $query .= "AND ".LICENCIE_JOUE_DANS_CATEGORIE_TABLE_NAME.".".LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID." = :".LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID.") ";
        $query .= "OR (".LICENCIE_COACHE_CATEGORIE_TABLE_NAME.".".LICENCIE_COACHE_CATEGORIE_LICENCIE_ID." = ".LICENCIE_TABLE_NAME.".".LICENCIE_ID." ";
        $query .= "AND ".LICENCIE_COACHE_CATEGORIE_TABLE_NAME.".".LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID." = :".LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID.") ";
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $params = array(
            LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID => $categorieId,
            LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID => $categorieId
        );
        $columns = array(
            LICENCIE_ID, 
            LICENCIE_NOM, 
            LICENCIE_PRENOM, 
            LICENCIE_DATE_NAISSANCE, 
            LICENCIE_ADRESSE, 
            LICENCIE_CODE_POSTAL, 
            LICENCIE_VILLE, 
            LICENCIE_TELEPHONE_1, 
            LICENCIE_TELEPHONE_2, 
            LICENCIE_TELEPHONE_3,
            LICENCIE_EMAIL_1,
            LICENCIE_EMAIL_2,
            LICENCIE_EMAIL_3,
            LICENCIE_GARDIEN,
            LICENCIE_ARBITRE,
            LICENCIE_BUREAU,
            LICENCIE_CA,
            LICENCIE_ACTIF,
            LICENCIE_LATERALITE_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }

    public function getIdsByCategorieIdNotInDisposByEvenementIdAndTypeDispoId($categorieId, $evenementId, $typeDispoId) {
        require_once("common/php/dao/licencie-joue-dans-categorie-dao.php");
        require_once("common/php/dao/licencie-coache-categorie-dao.php");
        require_once("common/php/dao/dispo-dao.php");

        $query = "SELECT DISTINCT ".LICENCIE_TABLE_NAME.".".LICENCIE_ID." FROM ".LICENCIE_TABLE_NAME.", ".LICENCIE_JOUE_DANS_CATEGORIE_TABLE_NAME.", ".LICENCIE_COACHE_CATEGORIE_TABLE_NAME." ";
        $query .= "WHERE ((".LICENCIE_JOUE_DANS_CATEGORIE_TABLE_NAME.".".LICENCIE_JOUE_DANS_CATEGORIE_LICENCIE_ID." = ".LICENCIE_TABLE_NAME.".".LICENCIE_ID." ";
        $query .= "AND ".LICENCIE_JOUE_DANS_CATEGORIE_TABLE_NAME.".".LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID." = :".LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID.") ";
        $query .= "OR (".LICENCIE_COACHE_CATEGORIE_TABLE_NAME.".".LICENCIE_COACHE_CATEGORIE_LICENCIE_ID." = ".LICENCIE_TABLE_NAME.".".LICENCIE_ID." ";
        $query .= "AND ".LICENCIE_COACHE_CATEGORIE_TABLE_NAME.".".LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID." = :".LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID.")) ";
        $query .= "AND (".LICENCIE_TABLE_NAME.".".LICENCIE_ID." NOT IN (";
        $query .= "SELECT ".DISPO_TABLE_NAME.".".DISPO_LICENCIE_ID." ";
        $query .= "FROM ".DISPO_TABLE_NAME." ";
        $query .= "WHERE ".DISPO_TABLE_NAME.".".DISPO_EVENEMENT_ID." = :".DISPO_EVENEMENT_ID." ";
        $query .= "AND ".DISPO_TABLE_NAME.".".DISPO_TYPE_DISPO_ID." = :".DISPO_TYPE_DISPO_ID;
        $query .= "))";
        $params = array(
            LICENCIE_JOUE_DANS_CATEGORIE_CATEGORIE_ID => $categorieId,
            LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID => $categorieId,
            DISPO_EVENEMENT_ID => $evenementId,
            DISPO_TYPE_DISPO_ID => $typeDispoId
        );
        $columns = array(
            LICENCIE_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }

    public function getNotInTypeDispoIdByEvenementIdAndReponseId($typeDispoId, $evenementId, $reponseId, $arbitre, $orderBy = null) {
        require_once("common/php/dao/dispo-dao.php");

        $query = "SELECT * FROM ".LICENCIE_TABLE_NAME." ";
        $query .= "WHERE ".LICENCIE_TABLE_NAME.".".LICENCIE_ID." NOT IN (";
        $query .= "SELECT ".DISPO_TABLE_NAME.".".DISPO_LICENCIE_ID." ";
        $query .= "FROM ".DISPO_TABLE_NAME." ";
        $query .= "WHERE ".DISPO_TABLE_NAME.".".DISPO_EVENEMENT_ID." = :".DISPO_EVENEMENT_ID." ";
        $query .= "AND ".DISPO_TABLE_NAME.".".DISPO_TYPE_DISPO_ID." = :".DISPO_TYPE_DISPO_ID." ";
        $query .= "AND ".DISPO_TABLE_NAME.".".DISPO_REPONSE_DISPO_ID." = :".DISPO_REPONSE_DISPO_ID." ";
        $query .= ")";
        if ($arbitre != null && $arbitre != "") {
            if ($arbitre == 1) {
                $query .= " AND (".LICENCIE_ARBITRE." IS NOT NULL AND ".LICENCIE_ARBITRE." != '')";
            } else if ($arbitre == 0) {
                $query .= " AND (".LICENCIE_ARBITRE." IS NULL OR ".LICENCIE_ARBITRE." = '')";
            }
        }
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $params = array(
            DISPO_EVENEMENT_ID => $evenementId,
            DISPO_TYPE_DISPO_ID => $typeDispoId,
            DISPO_REPONSE_DISPO_ID => $reponseId
        );
        $columns = array(
            LICENCIE_ID, 
            LICENCIE_NOM, 
            LICENCIE_PRENOM, 
            LICENCIE_DATE_NAISSANCE, 
            LICENCIE_ADRESSE, 
            LICENCIE_CODE_POSTAL, 
            LICENCIE_VILLE, 
            LICENCIE_TELEPHONE_1, 
            LICENCIE_TELEPHONE_2, 
            LICENCIE_TELEPHONE_3,
            LICENCIE_EMAIL_1,
            LICENCIE_EMAIL_2,
            LICENCIE_EMAIL_3,
            LICENCIE_GARDIEN,
            LICENCIE_ARBITRE,
            LICENCIE_BUREAU,
            LICENCIE_CA,
            LICENCIE_ACTIF,
            LICENCIE_LATERALITE_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }

    public function getNotCoachsByCategorieId($categorieId, $orderBy) {
        require_once("common/php/dao/licencie-coache-categorie-dao.php");

        $query = "SELECT * FROM ".LICENCIE_TABLE_NAME." ";
        $query .= "WHERE ".LICENCIE_TABLE_NAME.".".LICENCIE_ID." NOT IN (";
        $query .= "SELECT ".LICENCIE_COACHE_CATEGORIE_TABLE_NAME.".".LICENCIE_COACHE_CATEGORIE_LICENCIE_ID." ";
        $query .= "FROM ".LICENCIE_COACHE_CATEGORIE_TABLE_NAME." ";
        $query .= "WHERE ".LICENCIE_COACHE_CATEGORIE_TABLE_NAME.".".LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID." = :".LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID." ";
        $query .= ")";
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $params = array(
            LICENCIE_COACHE_CATEGORIE_CATEGORIE_ID => $categorieId
        );
        $columns = array(
            LICENCIE_ID, 
            LICENCIE_NOM, 
            LICENCIE_PRENOM, 
            LICENCIE_DATE_NAISSANCE, 
            LICENCIE_ADRESSE, 
            LICENCIE_CODE_POSTAL, 
            LICENCIE_VILLE, 
            LICENCIE_TELEPHONE_1, 
            LICENCIE_TELEPHONE_2, 
            LICENCIE_TELEPHONE_3,
            LICENCIE_EMAIL_1,
            LICENCIE_EMAIL_2,
            LICENCIE_EMAIL_3,
            LICENCIE_GARDIEN,
            LICENCIE_ARBITRE,
            LICENCIE_BUREAU,
            LICENCIE_CA,
            LICENCIE_ACTIF,
            LICENCIE_LATERALITE_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }

    public function getNotArbitresByEvenementId($evenementId, $orderBy) {
        require_once("common/php/dao/arbitrage-dao.php");

        $query = "SELECT * FROM ".LICENCIE_TABLE_NAME." ";
        $query .= "WHERE ".LICENCIE_TABLE_NAME.".".LICENCIE_ID." NOT IN (";
        $query .= "SELECT ".ARBITRAGE_TABLE_NAME.".".ARBITRAGE_LICENCIE_ID." ";
        $query .= "FROM ".ARBITRAGE_TABLE_NAME." ";
        $query .= "WHERE ".ARBITRAGE_TABLE_NAME.".".ARBITRAGE_EVENEMENT_ID." = :".ARBITRAGE_EVENEMENT_ID." ";
        $query .= ") ";
        $query .= "AND ".LICENCIE_ARBITRE." = 1";
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $params = array(
            ARBITRAGE_EVENEMENT_ID => $evenementId
        );
        $columns = array(
            LICENCIE_ID, 
            LICENCIE_NOM, 
            LICENCIE_PRENOM, 
            LICENCIE_DATE_NAISSANCE, 
            LICENCIE_ADRESSE, 
            LICENCIE_CODE_POSTAL, 
            LICENCIE_VILLE, 
            LICENCIE_TELEPHONE_1, 
            LICENCIE_TELEPHONE_2, 
            LICENCIE_TELEPHONE_3,
            LICENCIE_EMAIL_1,
            LICENCIE_EMAIL_2,
            LICENCIE_EMAIL_3,
            LICENCIE_GARDIEN,
            LICENCIE_ARBITRE,
            LICENCIE_BUREAU,
            LICENCIE_CA,
            LICENCIE_ACTIF,
            LICENCIE_LATERALITE_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".LICENCIE_TABLE_NAME." (";
        $query .= LICENCIE_NOM.", ";
        $query .= LICENCIE_PRENOM.", ";
        $query .= LICENCIE_DATE_NAISSANCE.", ";
        $query .= LICENCIE_ADRESSE.", ";
        $query .= LICENCIE_CODE_POSTAL.", ";
        $query .= LICENCIE_VILLE.", ";
        $query .= LICENCIE_TELEPHONE_1.", ";
        $query .= LICENCIE_TELEPHONE_2.", ";
        $query .= LICENCIE_TELEPHONE_3.", ";
        $query .= LICENCIE_EMAIL_1.", ";
        $query .= LICENCIE_EMAIL_2.", ";
        $query .= LICENCIE_EMAIL_3.", ";
        $query .= LICENCIE_GARDIEN.", ";
        $query .= LICENCIE_ARBITRE.", ";
        $query .= LICENCIE_BUREAU.", ";
        $query .= LICENCIE_CA.", ";
        $query .= LICENCIE_ACTIF.", ";
        $query .= LICENCIE_LATERALITE_ID.") VALUES (";
        $query .= ":".LICENCIE_NOM.", ";
        $query .= ":".LICENCIE_PRENOM.", ";
        $query .= ":".LICENCIE_DATE_NAISSANCE.", ";
        $query .= ":".LICENCIE_ADRESSE.", ";
        $query .= ":".LICENCIE_CODE_POSTAL.", ";
        $query .= ":".LICENCIE_VILLE.", ";
        $query .= ":".LICENCIE_TELEPHONE_1.", ";
        $query .= ":".LICENCIE_TELEPHONE_2.", ";
        $query .= ":".LICENCIE_TELEPHONE_3.", ";
        $query .= ":".LICENCIE_EMAIL_1.", ";
        $query .= ":".LICENCIE_EMAIL_2.", ";
        $query .= ":".LICENCIE_EMAIL_3.", ";
        $query .= ":".LICENCIE_GARDIEN.", ";
        $query .= ":".LICENCIE_ARBITRE.", ";
        $query .= ":".LICENCIE_BUREAU.", ";
        $query .= ":".LICENCIE_CA.", ";
        $query .= ":".LICENCIE_ACTIF.", ";
        $query .= ":".LICENCIE_LATERALITE_ID.")";
        $params = array(
            LICENCIE_NOM => $object[LICENCIE_NOM],
            LICENCIE_PRENOM => $object[LICENCIE_PRENOM],
            LICENCIE_DATE_NAISSANCE => $object[LICENCIE_DATE_NAISSANCE],
            LICENCIE_ADRESSE => $object[LICENCIE_ADRESSE],
            LICENCIE_CODE_POSTAL => $object[LICENCIE_CODE_POSTAL],
            LICENCIE_VILLE => $object[LICENCIE_VILLE],
            LICENCIE_TELEPHONE_1 => $object[LICENCIE_TELEPHONE_1],
            LICENCIE_TELEPHONE_2 => $object[LICENCIE_TELEPHONE_2],
            LICENCIE_TELEPHONE_3 => $object[LICENCIE_TELEPHONE_3],
            LICENCIE_EMAIL_1 => $object[LICENCIE_EMAIL_1],
            LICENCIE_EMAIL_2 => $object[LICENCIE_EMAIL_2],
            LICENCIE_EMAIL_3 => $object[LICENCIE_EMAIL_3],
            LICENCIE_GARDIEN => $object[LICENCIE_GARDIEN],
            LICENCIE_ARBITRE => $object[LICENCIE_ARBITRE],
            LICENCIE_BUREAU => $object[LICENCIE_BUREAU],
            LICENCIE_CA => $object[LICENCIE_CA],
            LICENCIE_ACTIF => $object[LICENCIE_ACTIF],
            LICENCIE_LATERALITE_ID => $object[LICENCIE_LATERALITE_ID]
        );
        return $this->executeCreateRequest($query, $params);}
	
	public function update($object) {
        $query = "UPDATE ".LICENCIE_TABLE_NAME." SET ";
        $query .= LICENCIE_NOM." = :".LICENCIE_NOM.", ";
        $query .= LICENCIE_PRENOM." = :".LICENCIE_PRENOM.", ";
        $query .= LICENCIE_DATE_NAISSANCE." = :".LICENCIE_DATE_NAISSANCE.", ";
        $query .= LICENCIE_ADRESSE." = :".LICENCIE_ADRESSE.", ";
        $query .= LICENCIE_CODE_POSTAL." = :".LICENCIE_CODE_POSTAL.", ";
        $query .= LICENCIE_VILLE." = :".LICENCIE_VILLE.", ";
        $query .= LICENCIE_TELEPHONE_1." = :".LICENCIE_TELEPHONE_1.", ";
        $query .= LICENCIE_TELEPHONE_2." = :".LICENCIE_TELEPHONE_2.", ";
        $query .= LICENCIE_TELEPHONE_3." = :".LICENCIE_TELEPHONE_3.", ";
        $query .= LICENCIE_EMAIL_1." = :".LICENCIE_EMAIL_1.", ";
        $query .= LICENCIE_EMAIL_2." = :".LICENCIE_EMAIL_2.", ";
        $query .= LICENCIE_EMAIL_3." = :".LICENCIE_EMAIL_3.", ";
        $query .= LICENCIE_GARDIEN." = :".LICENCIE_GARDIEN.", ";
        $query .= LICENCIE_ARBITRE." = :".LICENCIE_ARBITRE.", ";
        $query .= LICENCIE_BUREAU." = :".LICENCIE_BUREAU.", ";
        $query .= LICENCIE_CA." = :".LICENCIE_CA.", ";
        $query .= LICENCIE_ACTIF." = :".LICENCIE_ACTIF.", ";
        $query .= LICENCIE_LATERALITE_ID." = :".LICENCIE_LATERALITE_ID." WHERE ";
        $query .= LICENCIE_ID." = :".LICENCIE_ID;
        $params = array(
            LICENCIE_NOM => $object[LICENCIE_NOM],
            LICENCIE_PRENOM => $object[LICENCIE_PRENOM],
            LICENCIE_DATE_NAISSANCE => $object[LICENCIE_DATE_NAISSANCE],
            LICENCIE_ADRESSE => $object[LICENCIE_ADRESSE],
            LICENCIE_CODE_POSTAL => $object[LICENCIE_CODE_POSTAL],
            LICENCIE_VILLE => $object[LICENCIE_VILLE],
            LICENCIE_TELEPHONE_1 => $object[LICENCIE_TELEPHONE_1],
            LICENCIE_TELEPHONE_2 => $object[LICENCIE_TELEPHONE_2],
            LICENCIE_TELEPHONE_3 => $object[LICENCIE_TELEPHONE_3],
            LICENCIE_EMAIL_1 => $object[LICENCIE_EMAIL_1],
            LICENCIE_EMAIL_2 => $object[LICENCIE_EMAIL_2],
            LICENCIE_EMAIL_3 => $object[LICENCIE_EMAIL_3],
            LICENCIE_GARDIEN => $object[LICENCIE_GARDIEN],
            LICENCIE_ARBITRE => $object[LICENCIE_ARBITRE],
            LICENCIE_BUREAU => $object[LICENCIE_BUREAU],
            LICENCIE_CA => $object[LICENCIE_CA],
            LICENCIE_ACTIF => $object[LICENCIE_ACTIF],
            LICENCIE_LATERALITE_ID => $object[LICENCIE_LATERALITE_ID],
            LICENCIE_ID => $object[LICENCIE_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".LICENCIE_TABLE_NAME." WHERE ".LICENCIE_ID." = :".LICENCIE_ID;
        $params = array(
            LICENCIE_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>