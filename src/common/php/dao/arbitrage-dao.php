<?php
require_once("core/php/lib/abstract-dao.php");

define(ARBITRAGE_TABLE_NAME, "hbcsj_arbitrage");
define(ARBITRAGE_LICENCIE_ID, "licencie_id");
define(ARBITRAGE_EVENEMENT_ID, "evenement_id");

define(ARBITRAGE_DATE_EVENEMENT, "date_evenement");

class ArbitrageDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(ARBITRAGE_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".ARBITRAGE_TABLE_NAME." WHERE ".ARBITRAGE_LICENCIE_ID." = :".ARBITRAGE_LICENCIE_ID." AND ".ARBITRAGE_EVENEMENT_ID." = :".ARBITRAGE_EVENEMENT_ID;
        $params = array(
            ARBITRAGE_LICENCIE_ID => $id[ARBITRAGE_LICENCIE_ID],
            ARBITRAGE_EVENEMENT_ID => $id[ARBITRAGE_EVENEMENT_ID]
        );
        $columns = array(
            ARBITRAGE_LICENCIE_ID,
            ARBITRAGE_EVENEMENT_ID
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".ARBITRAGE_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            ARBITRAGE_LICENCIE_ID,
            ARBITRAGE_EVENEMENT_ID
        );
        return $this->executeGetRequest($query, null, $columns);
    }

    public function getByLicencieIdAndDateDebutSaison($licencieId, $dateDebutSaison) {
        require_once("common/php/dao/evenement-dao.php");

        $query = "SELECT * FROM ".ARBITRAGE_TABLE_NAME.", ".EVENEMENT_TABLE_NAME." ";
        $query .= "WHERE ".ARBITRAGE_LICENCIE_ID." = :".ARBITRAGE_LICENCIE_ID." ";
        $query .= "AND ".EVENEMENT_TABLE_NAME.".".EVENEMENT_DATE_HEURE." >= :".ARBITRAGE_DATE_EVENEMENT." ";
        $query .= "AND ".ARBITRAGE_TABLE_NAME.".".ARBITRAGE_EVENEMENT_ID." = ".EVENEMENT_TABLE_NAME.".".EVENEMENT_ID." ";
        $query .= "ORDER BY ".EVENEMENT_TABLE_NAME.".".EVENEMENT_DATE_HEURE;
        $params = array(
            ARBITRAGE_LICENCIE_ID => $licencieId,
            ARBITRAGE_DATE_EVENEMENT => $dateDebutSaison
        );
        $columns = array(
            ARBITRAGE_LICENCIE_ID,
            ARBITRAGE_EVENEMENT_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }

    public function getByEvenementId($evenementId) {
        $query = "SELECT * FROM ".ARBITRAGE_TABLE_NAME." WHERE ".ARBITRAGE_EVENEMENT_ID." = :".ARBITRAGE_EVENEMENT_ID;
        $params = array(
            ARBITRAGE_EVENEMENT_ID => $evenementId
        );
        $columns = array(
            ARBITRAGE_LICENCIE_ID,
            ARBITRAGE_EVENEMENT_ID
        );
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".ARBITRAGE_TABLE_NAME." (";
        $query .= ARBITRAGE_LICENCIE_ID.", ";
        $query .= ARBITRAGE_EVENEMENT_ID.") VALUES (";
        $query .= ":".ARBITRAGE_LICENCIE_ID.", ";
        $query .= ":".ARBITRAGE_EVENEMENT_ID.")";
        $params = array(
            ARBITRAGE_LICENCIE_ID => $object[ARBITRAGE_LICENCIE_ID],
            ARBITRAGE_EVENEMENT_ID => $object[ARBITRAGE_EVENEMENT_ID]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {}
	
	public function delete($id) {
        $query = "DELETE FROM ".ARBITRAGE_TABLE_NAME." WHERE ".ARBITRAGE_LICENCIE_ID." = :".ARBITRAGE_LICENCIE_ID." AND ".ARBITRAGE_EVENEMENT_ID." = :".ARBITRAGE_EVENEMENT_ID;
        $params = array(
            ARBITRAGE_LICENCIE_ID => $id[ARBITRAGE_LICENCIE_ID],
            ARBITRAGE_EVENEMENT_ID => $id[ARBITRAGE_EVENEMENT_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function deleteByLicencieId($licencieId) {
        $query = "DELETE FROM ".ARBITRAGE_TABLE_NAME." WHERE ".ARBITRAGE_LICENCIE_ID." = :".ARBITRAGE_LICENCIE_ID;
        $params = array(
            ARBITRAGE_LICENCIE_ID => $licencieId
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function deleteByEvenementId($evenementId) {
        $query = "DELETE FROM ".ARBITRAGE_TABLE_NAME." WHERE ".ARBITRAGE_EVENEMENT_ID." = :".ARBITRAGE_EVENEMENT_ID;
        $params = array(
            ARBITRAGE_EVENEMENT_ID => $evenementId
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>