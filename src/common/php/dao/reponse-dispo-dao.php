<?php
require_once("core/php/lib/abstract-dao.php");

define(REPONSE_DISPO_TABLE_NAME, "hbcsj_reponse_dispo");
define(REPONSE_DISPO_ID, "id");
define(REPONSE_DISPO_LIBELLE, "libelle");

class ReponseDispoDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(REPONSE_DISPO_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".REPONSE_DISPO_TABLE_NAME." WHERE ".REPONSE_DISPO_ID." = :".REPONSE_DISPO_ID;
        $params = array(
            REPONSE_DISPO_ID => $id
        );
        $columns = array(REPONSE_DISPO_ID, REPONSE_DISPO_LIBELLE);
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".REPONSE_DISPO_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(REPONSE_DISPO_ID, REPONSE_DISPO_LIBELLE);
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {}
	
	public function update($object) {}
	
	public function delete($id) {}
}

?>