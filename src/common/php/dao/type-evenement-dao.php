<?php
require_once("core/php/lib/abstract-dao.php");

define(TYPE_EVENEMENT_TABLE_NAME, "hbcsj_type_evenement");
define(TYPE_EVENEMENT_ID, "id");
define(TYPE_EVENEMENT_LIBELLE, "libelle");

class TypeEvenementDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(TYPE_EVENEMENT_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".TYPE_EVENEMENT_TABLE_NAME." WHERE ".TYPE_EVENEMENT_ID." = :".TYPE_EVENEMENT_ID;
        $params = array(
            TYPE_EVENEMENT_ID => $id
        );
        $columns = array(TYPE_EVENEMENT_ID, TYPE_EVENEMENT_LIBELLE);
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".TYPE_EVENEMENT_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(TYPE_EVENEMENT_ID, TYPE_EVENEMENT_LIBELLE);
        return $this->executeGetRequest($query, null, $columns);
    }

    public function getTypesEvenementsByDate($date) {
        require_once("common/php/dao/evenement-dao.php");
        require_once("common/php/lib/date-utils.php");

        $query = "SELECT DISTINCT ".TYPE_EVENEMENT_TABLE_NAME.".* ";
        $query .= "FROM ".TYPE_EVENEMENT_TABLE_NAME.", ".EVENEMENT_TABLE_NAME." ";
        $query .= "WHERE ".TYPE_EVENEMENT_TABLE_NAME.".".TYPE_EVENEMENT_ID." = ".EVENEMENT_TABLE_NAME.".".EVENEMENT_TYPE_EVENEMENT_ID." ";
        $query .= "AND ".EVENEMENT_TABLE_NAME.".".EVENEMENT_VISIBLE_SUR_SITE." = 1 ";
        $query .= "AND ".EVENEMENT_TABLE_NAME.".".EVENEMENT_DATE_HEURE." >= :".EVENEMENT_DATE_HEURE_DEBUT." ";
        $query .= "AND ".EVENEMENT_TABLE_NAME.".".EVENEMENT_DATE_HEURE." <= :".EVENEMENT_DATE_HEURE_FIN;
        $params = array(
            EVENEMENT_DATE_HEURE_DEBUT => $date.SQL_DATE_TIME_SEPARATOR."00:00:00",
            EVENEMENT_DATE_HEURE_FIN => $date.SQL_DATE_TIME_SEPARATOR."23:59:59"
        );
        $columns = array(TYPE_EVENEMENT_ID, TYPE_EVENEMENT_LIBELLE);
        return $this->executeGetRequest($query, $params, $columns);
    }
	
	public function create($object) {}
	
	public function update($object) {}
	
	public function delete($id) {}
}

?>