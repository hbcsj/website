<?php
require_once("core/php/lib/abstract-dao.php");

define(COMMANDE_CLUB_TABLE_NAME, "hbcsj_commande_club");
define(COMMANDE_CLUB_ID, "id");
define(COMMANDE_CLUB_DATE, "date");
define(COMMANDE_CLUB_PRIX_TOTAL, "prix_total");
define(COMMANDE_CLUB_PAYE, "paye");
define(COMMANDE_CLUB_FOURNISSEUR_PRODUIT_BOUTIQUE_ID, "fournisseur_produit_boutique_id");
define(COMMANDE_CLUB_SAISON_ID, "saison_id");

class CommandeClubDAO extends AbstractDAO {

    public function __construct($databaseConnection) {
        parent::__construct(COMMANDE_CLUB_TABLE_NAME, $databaseConnection);
    }

    public function getById($id) {
        $query = "SELECT * FROM ".COMMANDE_CLUB_TABLE_NAME." WHERE ".COMMANDE_CLUB_ID." = :".COMMANDE_CLUB_ID;
        $params = array(
            COMMANDE_CLUB_ID => $id
        );
        $columns = array(
            COMMANDE_CLUB_ID, 
            COMMANDE_CLUB_DATE,
            COMMANDE_CLUB_PRIX_TOTAL,
            COMMANDE_CLUB_PAYE,
            COMMANDE_CLUB_FOURNISSEUR_PRODUIT_BOUTIQUE_ID,
            COMMANDE_CLUB_SAISON_ID
        );
        return $this->executeGetUniqueRequest($query, $params, $columns);
    }
	
	public function getAll($orderBy = null) {
        $query = "SELECT * FROM ".COMMANDE_CLUB_TABLE_NAME;
        if ($orderBy != null) {
            $query .= " ORDER BY ".$orderBy;
        }
        $columns = array(
            COMMANDE_CLUB_ID, 
            COMMANDE_CLUB_DATE,
            COMMANDE_CLUB_PRIX_TOTAL,
            COMMANDE_CLUB_PAYE,
            COMMANDE_CLUB_FOURNISSEUR_PRODUIT_BOUTIQUE_ID,
            COMMANDE_CLUB_SAISON_ID
        );
        return $this->executeGetRequest($query, null, $columns);
    }
	
	public function create($object) {
        $query = "INSERT INTO ".COMMANDE_CLUB_TABLE_NAME." (";
        $query .= COMMANDE_CLUB_DATE.", ";
        $query .= COMMANDE_CLUB_PRIX_TOTAL.", ";
        $query .= COMMANDE_CLUB_PAYE.", ";
        $query .= COMMANDE_CLUB_FOURNISSEUR_PRODUIT_BOUTIQUE_ID.", ";
        $query .= COMMANDE_CLUB_SAISON_ID.") VALUES (";
        $query .= ":".COMMANDE_CLUB_DATE.", ";
        $query .= ":".COMMANDE_CLUB_PRIX_TOTAL.", ";
        $query .= ":".COMMANDE_CLUB_PAYE.", ";
        $query .= ":".COMMANDE_CLUB_FOURNISSEUR_PRODUIT_BOUTIQUE_ID.", ";
        $query .= ":".COMMANDE_CLUB_SAISON_ID.")";
        $params = array(
            COMMANDE_CLUB_DATE => $object[COMMANDE_CLUB_DATE],
            COMMANDE_CLUB_PRIX_TOTAL => $object[COMMANDE_CLUB_PRIX_TOTAL],
            COMMANDE_CLUB_PAYE => $object[COMMANDE_CLUB_PAYE],
            COMMANDE_CLUB_FOURNISSEUR_PRODUIT_BOUTIQUE_ID => $object[COMMANDE_CLUB_FOURNISSEUR_PRODUIT_BOUTIQUE_ID],
            COMMANDE_CLUB_SAISON_ID => $object[COMMANDE_CLUB_SAISON_ID]
        );
        return $this->executeCreateRequest($query, $params);
    }
	
	public function update($object) {
        $query = "UPDATE ".COMMANDE_CLUB_TABLE_NAME." SET ";
        $query .= COMMANDE_CLUB_DATE." = :".COMMANDE_CLUB_DATE.", ";
        $query .= COMMANDE_CLUB_PRIX_TOTAL." = :".COMMANDE_CLUB_PRIX_TOTAL.", ";
        $query .= COMMANDE_CLUB_PAYE." = :".COMMANDE_CLUB_PAYE.", ";
        $query .= COMMANDE_CLUB_FOURNISSEUR_PRODUIT_BOUTIQUE_ID." = :".COMMANDE_CLUB_FOURNISSEUR_PRODUIT_BOUTIQUE_ID.", ";
        $query .= COMMANDE_CLUB_SAISON_ID." = :".COMMANDE_CLUB_SAISON_ID." WHERE ";
        $query .= COMMANDE_CLUB_ID." = :".COMMANDE_CLUB_ID;
        $params = array(
            COMMANDE_CLUB_DATE => $object[COMMANDE_CLUB_DATE],
            COMMANDE_CLUB_PRIX_TOTAL => $object[COMMANDE_CLUB_PRIX_TOTAL],
            COMMANDE_CLUB_PAYE => $object[COMMANDE_CLUB_PAYE],
            COMMANDE_CLUB_FOURNISSEUR_PRODUIT_BOUTIQUE_ID => $object[COMMANDE_CLUB_FOURNISSEUR_PRODUIT_BOUTIQUE_ID],
            COMMANDE_CLUB_SAISON_ID => $object[COMMANDE_CLUB_SAISON_ID],
            COMMANDE_CLUB_ID => $object[COMMANDE_CLUB_ID]
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
	
	public function delete($id) {
        $query = "DELETE FROM ".COMMANDE_CLUB_TABLE_NAME." WHERE ".COMMANDE_CLUB_ID." = :".COMMANDE_CLUB_ID;
        $params = array(
            COMMANDE_CLUB_ID => $id
        );
        $this->executeUpdateOrDeleteRequest($query, $params);
    }
}

?>