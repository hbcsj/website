<?php
session_start();

require_once("core/php/constants/config-constants.php");
require_once("core/php/constants/path-constants.php");
require_once("core/php/constants/http-constants.php");
require_once("core/php/lib/functions.php");
require_once("core/php/lib/logger.php");
require_once("core/php/lib/string-utils.php");
require_once("core/php/lib/http-utils.php");
require_once("core/php/lib/file-utils.php");
require_once("core/php/lib/include-utils.php");
require_once("core/php/lib/configuration-builder.php");
require_once("core/php/lib/router.php");
require_once("core/php/lib/abstract-dao.php");

ConfigurationBuilder::buildConfig();

$router = new Router();
$router->buildRoute($_GET[URL_GET_PARAM]);

define(ROOT_PATH, $router->getRootPath());

$srcPath = ROOT_PATH;
if ($GLOBALS[CONF][IS_DEV]) {
    $srcPath .= SRC_FOLDER;
}
define(SRC_PATH, $srcPath);

if (file_exists($router->getPath())) {
    require_once($router->getPath());
}

if ($router->getHTTPCode() != null) {
    header($router->getHTTPCode());
}

?>