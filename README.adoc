:toc:

= http://www.hbc-saintjean.fr[Site du HBC Saint-Jean] - Documentation

== Documentation fonctionnelle

===  Hébergement du site

Voir xref:docs/fonctionnel/1_hebergement-site.pdf[le document associé,window=_blank]

===  Correspondance profils / droits sur l'administration

Voir xref:docs/fonctionnel/2_profils-administration.pdf[le document associé,window=_blank]

===  Gestion des licenciés & coaching

Voir xref:docs/fonctionnel/3_licencies-coaching.pdf[le document associé,window=_blank]

===  Gestion des disponibilités & actions clubs

Voir xref:docs/fonctionnel/4_dispos-actions-clubs.pdf[le document associé,window=_blank]

===  Gestion des matchs & événements

Voir xref:docs/fonctionnel/5_matchs-evenements.pdf[le document associé,window=_blank]

===  Gestion des news & médias

Voir xref:docs/fonctionnel/6_news-medias.pdf[le document associé,window=_blank]

===  Gestion du shop (billetterie & boutique)

Voir xref:docs/fonctionnel/7_billetterie-boutique.pdf[le document associé,window=_blank]

===  Gestion des mails & de la newsletter

Voir xref:docs/fonctionnel/8_mails-newsletter.pdf[le document associé,window=_blank]

===  Gestion des réseaux sociaux

Voir xref:docs/fonctionnel/9_reseaux-sociaux.pdf[le document associé,window=_blank]

== Documentation technique

=== Technologies

==== Langages

* HTML 5
* CSS 3
* JavaScript
* PHP 5
* SQL

==== Bibliothèques / Frameworks

* https://jquery.com/[JQuery (v2.2)] - JavaScript
* https://getbootstrap.com/docs/3.3/[Boostrap (v3.3)] - JavaScript / CSS
* http://lesscss.org[LESS (v2.5)] - CSS
* http://daneden.me/animate[Animate (v3.5)] - CSS

=== Installation d'un environnement de développement

==== Les logiciels

* Git
* Client FTP (FileZilla, WinSCP, ...)
* Serveur Apache / PHP / MySQL (WampServer, ...)
* Editeur syntaxique / IDE orienté Web (Visual Studio Code, ...)

==== Récupération des sources

Dans le dossier `www` du serveur Apache, cloner https://gitlab.com/hbcsj/website.git[le projet Git du site] dans un dossier, appelé par exemple `hbcsj-site`.

==== Récupération des données

===== La base de données

La base de données peut être importée grâce à un fichier dump, généré directement depuis https://www.proxgroup.fr/[l'hébergeur du site].

[NOTE]
====
Copier au moins le dernier fichier dump dans le dossier `bdd` de `hbcsj-site`, et le commiter.
====

[NOTE]
====
Pour importer la base de données en local, respecter la configuration décrite dans le fichier `src/conf/config.ini`, et la modifier si nécessaire.

__N.B. La structure du fichier de configuration est décrite dans xref:docs/technique/structure.adoc[ce document,window=_blank].__
====

===== Les fichiers de données

Les fichiers de données peuvent être importés en rapatriant (par FTP) le dossier `_data` du site en ligne vers `hbcsj-site`.

[NOTE]
====
Commiter également le dossier `_data` et son contenu.
====

==== Lancement du site en local

Dans `hbcsj-site`, ajouter un dossier `_test` et un dossier `_logs`.

Le site est désormais disponible sur http://localhost/hbcsj-site

===  Les données

Voir xref:docs/technique/donnees.adoc[le document associé,window=_blank]

=== Structure du site

Voir xref:docs/technique/structure.adoc[le document associé,window=_blank]

=== Quelques modifications techniques courantes

Voir xref:docs/technique/modifications.adoc[le document associé,window=_blank]

=== Déploiement en ligne

Voir xref:docs/technique/deploiement.adoc[le document associé,window=_blank]