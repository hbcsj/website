<?php

function getFolderContent($folderPath) {
    $folderPath = str_replace("\\", "/", $folderPath);
    $files = array();
    if (file_exists($folderPath)) {
        if ($handle = opendir($folderPath)) {
            while ($file = readdir($handle)) {
                if ($file != "." && $file != "..") {
                    $files[] = $file;
                }
            }
        }
    }
    return $files;
}

function getFileExtension($file) {
    $fileSplitted = explode(".", $file);
    return $fileSplitted[sizeof($fileSplitted) - 1];
}

function buildLog($txt, $fpLog) {
    fputs($fpLog, $txt."\r\n");
}

function getCopyCommand($srcFile, $targetFile, $fpLog) {
    $command = "COPY ".$srcFile." ".$targetFile;
    buildLog("WRITE COMMAND\t".$command, $fpLog);
    return "echo ".$command."\r\n".$command."\r\n";
}

function getMkDirCommand($dir, $fpLog) {
    $command = "MKDIR ".$dir;
    buildLog("WRITE COMMAND\t".$command, $fpLog);
    return "echo ".$command."\r\n".$command."\r\n";
}

function getXcopyCommand($srcDir, $targetDir, $fpLog) {
    $command = "XCOPY ".$srcDir."\\* ".$targetDir." /s /i";
    buildLog("WRITE COMMAND\t".$command, $fpLog);
    return "echo ".$command."\r\n".$command."\r\n";
}

function getMoveCommand($srcFile, $targetFile, $fpLog) {
    $command = "MOVE ".$srcFile." ".$targetFile;
    buildLog("WRITE COMMAND\t".$command, $fpLog);
    return "echo ".$command."\r\n".$command."\r\n";
}

function getDelCommand($file, $fpLog) {
    $command = "DEL ".$file;
    buildLog("WRITE COMMAND\t".$command, $fpLog);
    return "echo ".$command."\r\n".$command."\r\n";
}

function getPutProdFileCommands($fileName, $fileNameWithProd, $fpLog) {
    buildLog("PROCESSING PUT PROD FILE '".$fileName."'", $fpLog);
    $command = getDelCommand($fileName, $fpLog);
    $command .= getMoveCommand($fileNameWithProd, $fileName, $fpLog);    
    return $command;
}

function getCopyCssJsPhpFolderCommands($srcDir, $targetDir, $nodeProgram, $lesscProgram, $cssMinProgram, $uglifyJsProgram, $fpLog) {
    buildLog("PROCESSING COPY CSS / JS / PHP FOLDER '".$srcDir."' TO '".$targetDir."'", $fpLog);
    $command = getMkDirCommand($targetDir, $fpLog);
    if (file_exists($srcDir."\\".CSS_FOLDER)) {
        $command .= getCopyCssFolderCommands($srcDir."\\".CSS_FOLDER, $targetDir."\\".CSS_FOLDER, $nodeProgram, $lesscProgram, $cssMinProgram, $fpLog);
    }
    if (file_exists($srcDir."\\".JS_FOLDER)) {
        $command .= getCopyJsFolderCommands($srcDir."\\".JS_FOLDER, $targetDir."\\".JS_FOLDER, $nodeProgram, $uglifyJsProgram, $fpLog);
    }
    if (file_exists($srcDir."\\".PHP_FOLDER)) {
        $command .= getXcopyCommand($srcDir."\\".PHP_FOLDER, $targetDir."\\".PHP_FOLDER, $fpLog);
    }
    return $command;
}

function getCopyCssFolderCommands($srcDir, $targetDir, $nodeProgram, $lesscProgram, $cssMinProgram, $fpLog) {
    buildLog("PROCESSING COPY CSS FOLDER '".$srcDir."' TO '".$targetDir."'", $fpLog);
    $command = "";
    $cssFolderContents = getFolderContent($srcDir);
    if (areFilesPresentsByExtension($srcDir, CSS_EXTENSION, $fpLog)) {
        $command .= getMkDirCommand($targetDir, $fpLog);
        foreach ($cssFolderContents as $cssFolderContent) {
            if (!is_dir($srcDir."\\".$cssFolderContent)) {
                $cssFileExtension = getFileExtension($cssFolderContent);
                if ($cssFileExtension == CSS_EXTENSION) {
                    $command .= getCopyCssFileCommands($srcDir."\\".$cssFolderContent, $targetDir."\\".$cssFolderContent, $nodeProgram, $lesscProgram, $cssMinProgram, false, $fpLog);
                }
            } else {
                $command .= getCopyCssFolderCommands($srcDir."\\".$cssFolderContent, $targetDir."\\".$cssFolderContent, $nodeProgram, $lesscProgram, $cssMinProgram, $fpLog);
            }
        }
    }
    return $command;
}

function getCopyCssFileCommands($srcFile, $targetFile, $nodeProgram, $lesscProgram, $cssMinProgram, $compileLessFiles, $fpLog) {
    buildLog("PROCESSING COPY CSS FILE '".$srcFile."'", $fpLog);
    $command = "";
    $cssFileExtension = getFileExtension($srcFile);
    if (file_exists($srcFile.PROD_INDICATION)) {
        buildLog("PROCESSING CSS PROD FILE '".$srcFile.PROD_INDICATION."' EXISTS", $fpLog);
        $command .= getCopyCommand($srcFile.PROD_INDICATION, $targetFile, $fpLog);
    } else {
        if ($cssFileExtension == LESS_EXTENSION) {
            if ($compileLessFiles) {
                $lesscCommand = $nodeProgram." ".$lesscProgram." ".$srcFile." ".$targetFile;
                buildLog("WRITE COMMAND\t".$lesscCommand, $fpLog);
                $command .= "echo ".$lesscCommand."\r\n".$lesscCommand."\r\n";
                $cssMinCommand = $nodeProgram." ".$cssMinProgram." ".$targetFile." > ".$targetFile.PROD_INDICATION;
                buildLog("WRITE COMMAND\t".$cssMinCommand, $fpLog);
                $command .= "echo ".$cssMinCommand."\r\n".$cssMinCommand."\r\n";
                $command .= getPutProdFileCommands($targetFile, $targetFile.PROD_INDICATION, $fpLog);
            }
        } else if ($cssFileExtension == CSS_EXTENSION) {
            $cssMinCommand = $nodeProgram." ".$cssMinProgram." ".$srcFile." > ".$targetFile;
            buildLog("WRITE COMMAND\t".$cssMinCommand, $fpLog);
            $command .= "echo ".$cssMinCommand."\r\n".$cssMinCommand."\r\n";
        }
    }
    return $command;
}

function getCorrespondingCssFile($file, $fileExtension, $fpLog) {
    $cssFile = $file;
    if ($fileExtension == LESS_EXTENSION) {
        $cssFile = str_replace(".".LESS_EXTENSION, ".".CSS_EXTENSION, $file);
        buildLog("\tCSS CORRESPONDING FILE OF '".$file."' IS '".$cssFile."'", $fpLog);
    }
    return $cssFile;
}

function getCopyJsFolderCommands($srcDir, $targetDir, $nodeProgram, $uglifyJsProgram, $fpLog) {
    buildLog("PROCESSING COPY JS FOLDER '".$srcDir."' TO '".$targetDir."'", $fpLog);
    $command = "";
    $jsFolderContents = getFolderContent($srcDir);
    if (areFilesPresentsByExtension($srcDir, JS_EXTENSION, $fpLog)) {
        $command .= getMkDirCommand($targetDir, $fpLog);
        foreach ($jsFolderContents as $jsFolderContent) {
            if ($jsFolderContent != LESS_FILE) {
                if (!is_dir($srcDir."\\".$jsFolderContent)) {
                    if (getFileExtension($jsFolderContent) == JS_EXTENSION) {
                        $command .= getCopyJsFileCommands($srcDir."\\".$jsFolderContent, $targetDir."\\".$jsFolderContent, $nodeProgram, $uglifyJsProgram, $fpLog);
                    }
                } else {
                    $command .= getCopyJsFolderCommands($srcDir."\\".$jsFolderContent, $targetDir."\\".$jsFolderContent, $nodeProgram, $uglifyJsProgram, $fpLog);
                }
            }
        }
    }
    return $command;
}

function getCopyJsFileCommands($srcFile, $targetFile, $nodeProgram, $uglifyJsProgram, $fpLog) {
    buildLog("PROCESSING COPY JS FILE '".$srcFile."'", $fpLog);
    $command = "";
    if (file_exists($srcFile.PROD_INDICATION)) {
        buildLog("\tJS PROD FILE '".$srcFile.PROD_INDICATION."' EXISTS", $fpLog);
        $command .= getCopyCommand($srcFile.PROD_INDICATION, $targetFile, $fpLog);
    } else {
        $uglifyJsCommand = $nodeProgram." ".$uglifyJsProgram." ".$srcFile." -o ".$targetFile." -c -m";
        buildLog("WRITE COMMAND\t".$uglifyJsCommand, $fpLog);
        $command .= "echo ".$uglifyJsCommand."\r\n".$uglifyJsCommand."\r\n";
    }
    return $command;
}

function areFilesPresentsByExtension($dir, $extension, $fpLog) {
    buildLog("\tARE '".$extension."' PRESENTS IN '".$dir."' ?", $fpLog);
    $res = false;
    $folderContents = getFolderContent($dir);
    if (sizeof($folderContents) > 0) {
        foreach ($folderContents as $folderContent) {
            if (is_dir($dir."\\".$folderContent)) {
                $res = areFilesPresentsByExtension($dir."\\".$folderContent, $extension, $fpLog);
            } else {
                if (getFileExtension($folderContent) == $extension) {
                    buildLog("\t'".$folderContent."' IS A '".$extension."' FILE IN '".$dir."'", $fpLog);
                    return true;
                }
            }
        }
    }
    if (!$res) {
        buildLog("\tNO", $fpLog);
    }
    return $res;
}

function getCopyWebappFolderCommands($srcDir, $targetDir, $nodeProgram, $lesscProgram, $cssMinProgram, $uglifyJsProgram, $fpLog) {
    buildLog("PROCESSING COPY WEBAPP FOLDER '".$srcDir."' TO '".$targetDir."'", $fpLog);
    $command = "";
    $folderContents = getFolderContent($srcDir);
    if (sizeof($folderContents) > 0) {
        $command .= getMkDirCommand($targetDir, $fpLog);
        foreach ($folderContents as $folderContent) {
            if (!is_dir($srcDir."\\".$folderContent)) {
                $fileExtension = getFileExtension($folderContent);
                if ($fileExtension == CSS_EXTENSION || $fileExtension == LESS_EXTENSION) {
                    $command .= getCopyCssFileCommands($srcDir."\\".$folderContent, getCorrespondingCssFile($targetDir."\\".$folderContent, $fileExtension, $fpLog), $nodeProgram, $lesscProgram, $cssMinProgram, true, $fpLog);
                } else if ($fileExtension == JS_EXTENSION) {
                    $command .= getCopyJsFileCommands($srcDir."\\".$folderContent, $targetDir."\\".$folderContent, $nodeProgram, $uglifyJsProgram, $fpLog);
                } else if ($fileExtension == JSON_EXTENSION || $fileExtension == PHP_EXTENSION) {
                    $command .= getCopyCommand($srcDir."\\".$folderContent, $targetDir."\\".$folderContent, $fpLog);
                }
            } else {
                $command .= getCopyWebappFolderCommands($srcDir."\\".$folderContent, $targetDir."\\".$folderContent, $nodeProgram, $lesscProgram, $cssMinProgram, $uglifyJsProgram, $fpLog);
            }
        }
    }
    return $command;
}

?>