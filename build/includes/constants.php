<?php

define(CSS_EXTENSION, "css");
define(LESS_EXTENSION, "less");
define(JS_EXTENSION, "js");
define(JSON_EXTENSION, "json");
define(PHP_EXTENSION, "php");
define(PROD_EXTENSION, "prod");

define(PROD_INDICATION, ".".PROD_EXTENSION);

define(ASSETS_FOLDER, "assets");
define(CONF_FOLDER, "conf");
define(COMMON_FOLDER, "common");
define(CORE_FOLDER, "core");
define(LIB_FOLDER, "lib");
define(WEBAPP_FOLDER, "webapp");
define(CSS_FOLDER, "css");
define(JS_FOLDER, "js");
define(PHP_FOLDER, "php");

define(BUILD_FILE, "build.cmd");
define(BUILD_LOG_FILE, "build.txt");
define(INDEX_FILE, "index.php");
define(FAVICON_FILE, "favicon.ico");
define(HTACCESS_FILE, ".htaccess");
define(HTACCESS_PROD_FILE, "-htaccess".PROD_INDICATION);
define(CONFIG_FILE, "config.ini");
define(CONFIG_PROD_FILE, "config.ini".PROD_INDICATION);
define(LESS_FILE, "less.js");

?>