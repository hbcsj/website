<?php

$buildDir = $argv[1];
$srcDir = $argv[2];
$targetDir = $argv[3];
$nodeProgram = $argv[4];

$path = $buildDir."/";

require_once($path."includes/constants.php");
require_once($path."includes/functions.php");

$conf = parse_ini_file($path."conf/config.ini");

$buildCode = "";
$fpLog = fopen($path.BUILD_LOG_FILE, "w");

buildLog("-- BUILD REPORT -------------------------------------\r\n", $fpLog);

$buildCode .= getCopyCommand($srcDir."\\".INDEX_FILE, $targetDir."\\".INDEX_FILE, $fpLog);
$buildCode .= getCopyCommand($srcDir."\\".FAVICON_FILE, $targetDir."\\".FAVICON_FILE, $fpLog);
$buildCode .= getCopyCommand(HTACCESS_PROD_FILE, $targetDir."\\".HTACCESS_FILE, $fpLog);
$buildCode .= getXcopyCommand($srcDir."\\".ASSETS_FOLDER, $targetDir."\\".ASSETS_FOLDER, $fpLog);
$buildCode .= getXcopyCommand($srcDir."\\".CONF_FOLDER, $targetDir."\\".CONF_FOLDER, $fpLog);
$buildCode .= getPutProdFileCommands($targetDir."\\".CONF_FOLDER."\\".CONFIG_FILE, $targetDir."\\".CONF_FOLDER."\\".CONFIG_PROD_FILE, $fpLog);
$buildCode .= getCopyCssJsPhpFolderCommands($srcDir."\\".COMMON_FOLDER, $targetDir."\\".COMMON_FOLDER, $nodeProgram, $conf["lessc_program"], $conf["css_min_program"], $conf["uglify_js_program"], $fpLog);
$buildCode .= getCopyCssJsPhpFolderCommands($srcDir."\\".CORE_FOLDER, $targetDir."\\".CORE_FOLDER, $nodeProgram, $conf["lessc_program"], $conf["css_min_program"], $conf["uglify_js_program"], $fpLog);
$buildCode .= getCopyCssJsPhpFolderCommands($srcDir."\\".LIB_FOLDER, $targetDir."\\".LIB_FOLDER, $nodeProgram, $conf["lessc_program"], $conf["css_min_program"], $conf["uglify_js_program"], $fpLog);
$buildCode .= getCopyWebappFolderCommands($srcDir."\\".WEBAPP_FOLDER, $targetDir."\\".WEBAPP_FOLDER, $nodeProgram, $conf["lessc_program"], $conf["css_min_program"], $conf["uglify_js_program"], $fpLog);

fclose($fpLog);

$buildFile = $path.BUILD_FILE;
if (file_exists($buildFile)) {
    unlink($buildFile);
}
$fp = fopen($buildFile, "w");
fputs($fp, $buildCode);
fclose($fp);

?>