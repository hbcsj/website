echo COPY src\index.php target\index.php
COPY src\index.php target\index.php
echo COPY src\favicon.ico target\favicon.ico
COPY src\favicon.ico target\favicon.ico
echo COPY -htaccess.prod target\.htaccess
COPY -htaccess.prod target\.htaccess
echo XCOPY src\assets\* target\assets /s /i
XCOPY src\assets\* target\assets /s /i
echo XCOPY src\conf\* target\conf /s /i
XCOPY src\conf\* target\conf /s /i
echo DEL target\conf\config.ini
DEL target\conf\config.ini
echo MOVE target\conf\config.ini.prod target\conf\config.ini
MOVE target\conf\config.ini.prod target\conf\config.ini
echo MKDIR target\common
MKDIR target\common
echo MKDIR target\common\js
MKDIR target\common\js
echo node ..\node_modules\uglify-js\bin\uglifyjs src\common\js\admin-controller.js -o target\common\js\admin-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\common\js\admin-controller.js -o target\common\js\admin-controller.js -c -m
echo COPY src\common\js\clipboard.js.prod target\common\js\clipboard.js
COPY src\common\js\clipboard.js.prod target\common\js\clipboard.js
echo node ..\node_modules\uglify-js\bin\uglifyjs src\common\js\connexion-controller.js -o target\common\js\connexion-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\common\js\connexion-controller.js -o target\common\js\connexion-controller.js -c -m
echo node ..\node_modules\uglify-js\bin\uglifyjs src\common\js\dispos-controller.js -o target\common\js\dispos-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\common\js\dispos-controller.js -o target\common\js\dispos-controller.js -c -m
echo node ..\node_modules\uglify-js\bin\uglifyjs src\common\js\form-recherche-utils.js -o target\common\js\form-recherche-utils.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\common\js\form-recherche-utils.js -o target\common\js\form-recherche-utils.js -c -m
echo node ..\node_modules\uglify-js\bin\uglifyjs src\common\js\form-utils.js -o target\common\js\form-utils.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\common\js\form-utils.js -o target\common\js\form-utils.js -c -m
echo node ..\node_modules\uglify-js\bin\uglifyjs src\common\js\global-controller.js -o target\common\js\global-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\common\js\global-controller.js -o target\common\js\global-controller.js -c -m
echo node ..\node_modules\uglify-js\bin\uglifyjs src\common\js\main-website-controller.js -o target\common\js\main-website-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\common\js\main-website-controller.js -o target\common\js\main-website-controller.js -c -m
echo node ..\node_modules\uglify-js\bin\uglifyjs src\common\js\shop-controller.js -o target\common\js\shop-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\common\js\shop-controller.js -o target\common\js\shop-controller.js -c -m
echo node ..\node_modules\uglify-js\bin\uglifyjs src\common\js\view-utils.js -o target\common\js\view-utils.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\common\js\view-utils.js -o target\common\js\view-utils.js -c -m
echo XCOPY src\common\php\* target\common\php /s /i
XCOPY src\common\php\* target\common\php /s /i
echo MKDIR target\core
MKDIR target\core
echo MKDIR target\core\js
MKDIR target\core\js
echo node ..\node_modules\uglify-js\bin\uglifyjs src\core\js\ajax-utils.js -o target\core\js\ajax-utils.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\core\js\ajax-utils.js -o target\core\js\ajax-utils.js -c -m
echo node ..\node_modules\uglify-js\bin\uglifyjs src\core\js\init-page.js -o target\core\js\init-page.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\core\js\init-page.js -o target\core\js\init-page.js -c -m
echo node ..\node_modules\uglify-js\bin\uglifyjs src\core\js\init-uploader.js -o target\core\js\init-uploader.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\core\js\init-uploader.js -o target\core\js\init-uploader.js -c -m
echo node ..\node_modules\uglify-js\bin\uglifyjs src\core\js\utils.js -o target\core\js\utils.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\core\js\utils.js -o target\core\js\utils.js -c -m
echo XCOPY src\core\php\* target\core\php /s /i
XCOPY src\core\php\* target\core\php /s /i
echo MKDIR target\lib
MKDIR target\lib
echo MKDIR target\lib\css
MKDIR target\lib\css
echo node ..\node_modules\cssmin\bin\cssmin src\lib\css\animate.css > target\lib\css\animate.css
node ..\node_modules\cssmin\bin\cssmin src\lib\css\animate.css > target\lib\css\animate.css
echo MKDIR target\lib\js
MKDIR target\lib\js
echo COPY src\lib\js\bootstrap.js.prod target\lib\js\bootstrap.js
COPY src\lib\js\bootstrap.js.prod target\lib\js\bootstrap.js
echo COPY src\lib\js\jquery.js.prod target\lib\js\jquery.js
COPY src\lib\js\jquery.js.prod target\lib\js\jquery.js
echo XCOPY src\lib\php\* target\lib\php /s /i
XCOPY src\lib\php\* target\lib\php /s /i
echo MKDIR target\webapp
MKDIR target\webapp
echo MKDIR target\webapp\services
MKDIR target\webapp\services
echo MKDIR target\webapp\services\actualite
MKDIR target\webapp\services\actualite
echo COPY src\webapp\services\actualite\actualite-service-controller.php target\webapp\services\actualite\actualite-service-controller.php
COPY src\webapp\services\actualite\actualite-service-controller.php target\webapp\services\actualite\actualite-service-controller.php
echo COPY src\webapp\services\actualite\actualite.service.php target\webapp\services\actualite\actualite.service.php
COPY src\webapp\services\actualite\actualite.service.php target\webapp\services\actualite\actualite.service.php
echo MKDIR target\webapp\services\album-photo
MKDIR target\webapp\services\album-photo
echo COPY src\webapp\services\album-photo\album-photo-service-controller.php target\webapp\services\album-photo\album-photo-service-controller.php
COPY src\webapp\services\album-photo\album-photo-service-controller.php target\webapp\services\album-photo\album-photo-service-controller.php
echo COPY src\webapp\services\album-photo\album-photo.service.php target\webapp\services\album-photo\album-photo.service.php
COPY src\webapp\services\album-photo\album-photo.service.php target\webapp\services\album-photo\album-photo.service.php
echo MKDIR target\webapp\services\article-presse
MKDIR target\webapp\services\article-presse
echo COPY src\webapp\services\article-presse\article-presse-service-controller.php target\webapp\services\article-presse\article-presse-service-controller.php
COPY src\webapp\services\article-presse\article-presse-service-controller.php target\webapp\services\article-presse\article-presse-service-controller.php
echo COPY src\webapp\services\article-presse\article-presse.service.php target\webapp\services\article-presse\article-presse.service.php
COPY src\webapp\services\article-presse\article-presse.service.php target\webapp\services\article-presse\article-presse.service.php
echo MKDIR target\webapp\services\categorie
MKDIR target\webapp\services\categorie
echo COPY src\webapp\services\categorie\categorie-service-controller.php target\webapp\services\categorie\categorie-service-controller.php
COPY src\webapp\services\categorie\categorie-service-controller.php target\webapp\services\categorie\categorie-service-controller.php
echo COPY src\webapp\services\categorie\categorie.service.php target\webapp\services\categorie\categorie.service.php
COPY src\webapp\services\categorie\categorie.service.php target\webapp\services\categorie\categorie.service.php
echo MKDIR target\webapp\services\commande
MKDIR target\webapp\services\commande
echo COPY src\webapp\services\commande\commande-service-controller.php target\webapp\services\commande\commande-service-controller.php
COPY src\webapp\services\commande\commande-service-controller.php target\webapp\services\commande\commande-service-controller.php
echo COPY src\webapp\services\commande\commande.service.php target\webapp\services\commande\commande.service.php
COPY src\webapp\services\commande\commande.service.php target\webapp\services\commande\commande.service.php
echo MKDIR target\webapp\services\commande-billetterie
MKDIR target\webapp\services\commande-billetterie
echo COPY src\webapp\services\commande-billetterie\commande-billetterie-service-controller.php target\webapp\services\commande-billetterie\commande-billetterie-service-controller.php
COPY src\webapp\services\commande-billetterie\commande-billetterie-service-controller.php target\webapp\services\commande-billetterie\commande-billetterie-service-controller.php
echo COPY src\webapp\services\commande-billetterie\commande-billetterie.service.php target\webapp\services\commande-billetterie\commande-billetterie.service.php
COPY src\webapp\services\commande-billetterie\commande-billetterie.service.php target\webapp\services\commande-billetterie\commande-billetterie.service.php
echo MKDIR target\webapp\services\commande-boutique
MKDIR target\webapp\services\commande-boutique
echo COPY src\webapp\services\commande-boutique\commande-boutique-service-controller.php target\webapp\services\commande-boutique\commande-boutique-service-controller.php
COPY src\webapp\services\commande-boutique\commande-boutique-service-controller.php target\webapp\services\commande-boutique\commande-boutique-service-controller.php
echo COPY src\webapp\services\commande-boutique\commande-boutique.service.php target\webapp\services\commande-boutique\commande-boutique.service.php
COPY src\webapp\services\commande-boutique\commande-boutique.service.php target\webapp\services\commande-boutique\commande-boutique.service.php
echo MKDIR target\webapp\services\commande-fournisseur-boutique
MKDIR target\webapp\services\commande-fournisseur-boutique
echo COPY src\webapp\services\commande-fournisseur-boutique\commande-fournisseur-boutique-service-controller.php target\webapp\services\commande-fournisseur-boutique\commande-fournisseur-boutique-service-controller.php
COPY src\webapp\services\commande-fournisseur-boutique\commande-fournisseur-boutique-service-controller.php target\webapp\services\commande-fournisseur-boutique\commande-fournisseur-boutique-service-controller.php
echo COPY src\webapp\services\commande-fournisseur-boutique\commande-fournisseur-boutique.service.php target\webapp\services\commande-fournisseur-boutique\commande-fournisseur-boutique.service.php
COPY src\webapp\services\commande-fournisseur-boutique\commande-fournisseur-boutique.service.php target\webapp\services\commande-fournisseur-boutique\commande-fournisseur-boutique.service.php
echo MKDIR target\webapp\services\competition
MKDIR target\webapp\services\competition
echo COPY src\webapp\services\competition\competition-service-controller.php target\webapp\services\competition\competition-service-controller.php
COPY src\webapp\services\competition\competition-service-controller.php target\webapp\services\competition\competition-service-controller.php
echo COPY src\webapp\services\competition\competition.service.php target\webapp\services\competition\competition.service.php
COPY src\webapp\services\competition\competition.service.php target\webapp\services\competition\competition.service.php
echo MKDIR target\webapp\services\connexion
MKDIR target\webapp\services\connexion
echo COPY src\webapp\services\connexion\connexion-service-controller.php target\webapp\services\connexion\connexion-service-controller.php
COPY src\webapp\services\connexion\connexion-service-controller.php target\webapp\services\connexion\connexion-service-controller.php
echo COPY src\webapp\services\connexion\connexion.service.php target\webapp\services\connexion\connexion.service.php
COPY src\webapp\services\connexion\connexion.service.php target\webapp\services\connexion\connexion.service.php
echo MKDIR target\webapp\services\date
MKDIR target\webapp\services\date
echo COPY src\webapp\services\date\date-service-controller.php target\webapp\services\date\date-service-controller.php
COPY src\webapp\services\date\date-service-controller.php target\webapp\services\date\date-service-controller.php
echo COPY src\webapp\services\date\date.service.php target\webapp\services\date\date.service.php
COPY src\webapp\services\date\date.service.php target\webapp\services\date\date.service.php
echo MKDIR target\webapp\services\dispo
MKDIR target\webapp\services\dispo
echo COPY src\webapp\services\dispo\dispo-service-controller.php target\webapp\services\dispo\dispo-service-controller.php
COPY src\webapp\services\dispo\dispo-service-controller.php target\webapp\services\dispo\dispo-service-controller.php
echo COPY src\webapp\services\dispo\dispo.service.php target\webapp\services\dispo\dispo.service.php
COPY src\webapp\services\dispo\dispo.service.php target\webapp\services\dispo\dispo.service.php
echo MKDIR target\webapp\services\document-coaching
MKDIR target\webapp\services\document-coaching
echo COPY src\webapp\services\document-coaching\document-coaching-service-controller.php target\webapp\services\document-coaching\document-coaching-service-controller.php
COPY src\webapp\services\document-coaching\document-coaching-service-controller.php target\webapp\services\document-coaching\document-coaching-service-controller.php
echo COPY src\webapp\services\document-coaching\document-coaching.service.php target\webapp\services\document-coaching\document-coaching.service.php
COPY src\webapp\services\document-coaching\document-coaching.service.php target\webapp\services\document-coaching\document-coaching.service.php
echo MKDIR target\webapp\services\entrainement
MKDIR target\webapp\services\entrainement
echo COPY src\webapp\services\entrainement\entrainement-service-controller.php target\webapp\services\entrainement\entrainement-service-controller.php
COPY src\webapp\services\entrainement\entrainement-service-controller.php target\webapp\services\entrainement\entrainement-service-controller.php
echo COPY src\webapp\services\entrainement\entrainement.service.php target\webapp\services\entrainement\entrainement.service.php
COPY src\webapp\services\entrainement\entrainement.service.php target\webapp\services\entrainement\entrainement.service.php
echo MKDIR target\webapp\services\evenement
MKDIR target\webapp\services\evenement
echo COPY src\webapp\services\evenement\evenement-service-controller.php target\webapp\services\evenement\evenement-service-controller.php
COPY src\webapp\services\evenement\evenement-service-controller.php target\webapp\services\evenement\evenement-service-controller.php
echo COPY src\webapp\services\evenement\evenement.service.php target\webapp\services\evenement\evenement.service.php
COPY src\webapp\services\evenement\evenement.service.php target\webapp\services\evenement\evenement.service.php
echo MKDIR target\webapp\services\fournisseur-produit-boutique
MKDIR target\webapp\services\fournisseur-produit-boutique
echo COPY src\webapp\services\fournisseur-produit-boutique\fournisseur-produit-boutique-service-controller.php target\webapp\services\fournisseur-produit-boutique\fournisseur-produit-boutique-service-controller.php
COPY src\webapp\services\fournisseur-produit-boutique\fournisseur-produit-boutique-service-controller.php target\webapp\services\fournisseur-produit-boutique\fournisseur-produit-boutique-service-controller.php
echo COPY src\webapp\services\fournisseur-produit-boutique\fournisseur-produit-boutique.service.php target\webapp\services\fournisseur-produit-boutique\fournisseur-produit-boutique.service.php
COPY src\webapp\services\fournisseur-produit-boutique\fournisseur-produit-boutique.service.php target\webapp\services\fournisseur-produit-boutique\fournisseur-produit-boutique.service.php
echo MKDIR target\webapp\services\gymnase
MKDIR target\webapp\services\gymnase
echo COPY src\webapp\services\gymnase\gymnase-service-controller.php target\webapp\services\gymnase\gymnase-service-controller.php
COPY src\webapp\services\gymnase\gymnase-service-controller.php target\webapp\services\gymnase\gymnase-service-controller.php
echo COPY src\webapp\services\gymnase\gymnase.service.php target\webapp\services\gymnase\gymnase.service.php
COPY src\webapp\services\gymnase\gymnase.service.php target\webapp\services\gymnase\gymnase.service.php
echo MKDIR target\webapp\services\hhsj
MKDIR target\webapp\services\hhsj
echo COPY src\webapp\services\hhsj\hhsj-service-controller.php target\webapp\services\hhsj\hhsj-service-controller.php
COPY src\webapp\services\hhsj\hhsj-service-controller.php target\webapp\services\hhsj\hhsj-service-controller.php
echo COPY src\webapp\services\hhsj\hhsj.service.php target\webapp\services\hhsj\hhsj.service.php
COPY src\webapp\services\hhsj\hhsj.service.php target\webapp\services\hhsj\hhsj.service.php
echo MKDIR target\webapp\services\home
MKDIR target\webapp\services\home
echo COPY src\webapp\services\home\home-service-controller.php target\webapp\services\home\home-service-controller.php
COPY src\webapp\services\home\home-service-controller.php target\webapp\services\home\home-service-controller.php
echo COPY src\webapp\services\home\home.service.php target\webapp\services\home\home.service.php
COPY src\webapp\services\home\home.service.php target\webapp\services\home\home.service.php
echo MKDIR target\webapp\services\home-message
MKDIR target\webapp\services\home-message
echo COPY src\webapp\services\home-message\home-message-service-controller.php target\webapp\services\home-message\home-message-service-controller.php
COPY src\webapp\services\home-message\home-message-service-controller.php target\webapp\services\home-message\home-message-service-controller.php
echo COPY src\webapp\services\home-message\home-message.service.php target\webapp\services\home-message\home-message.service.php
COPY src\webapp\services\home-message\home-message.service.php target\webapp\services\home-message\home-message.service.php
echo MKDIR target\webapp\services\licencie
MKDIR target\webapp\services\licencie
echo COPY src\webapp\services\licencie\licencie-service-controller.php target\webapp\services\licencie\licencie-service-controller.php
COPY src\webapp\services\licencie\licencie-service-controller.php target\webapp\services\licencie\licencie-service-controller.php
echo COPY src\webapp\services\licencie\licencie.service.php target\webapp\services\licencie\licencie.service.php
COPY src\webapp\services\licencie\licencie.service.php target\webapp\services\licencie\licencie.service.php
echo MKDIR target\webapp\services\livraison-fournisseur-boutique
MKDIR target\webapp\services\livraison-fournisseur-boutique
echo COPY src\webapp\services\livraison-fournisseur-boutique\livraison-fournisseur-boutique-service-controller.php target\webapp\services\livraison-fournisseur-boutique\livraison-fournisseur-boutique-service-controller.php
COPY src\webapp\services\livraison-fournisseur-boutique\livraison-fournisseur-boutique-service-controller.php target\webapp\services\livraison-fournisseur-boutique\livraison-fournisseur-boutique-service-controller.php
echo COPY src\webapp\services\livraison-fournisseur-boutique\livraison-fournisseur-boutique.service.php target\webapp\services\livraison-fournisseur-boutique\livraison-fournisseur-boutique.service.php
COPY src\webapp\services\livraison-fournisseur-boutique\livraison-fournisseur-boutique.service.php target\webapp\services\livraison-fournisseur-boutique\livraison-fournisseur-boutique.service.php
echo MKDIR target\webapp\services\match-billetterie
MKDIR target\webapp\services\match-billetterie
echo COPY src\webapp\services\match-billetterie\match-billetterie-service-controller.php target\webapp\services\match-billetterie\match-billetterie-service-controller.php
COPY src\webapp\services\match-billetterie\match-billetterie-service-controller.php target\webapp\services\match-billetterie\match-billetterie-service-controller.php
echo COPY src\webapp\services\match-billetterie\match-billetterie.service.php target\webapp\services\match-billetterie\match-billetterie.service.php
COPY src\webapp\services\match-billetterie\match-billetterie.service.php target\webapp\services\match-billetterie\match-billetterie.service.php
echo MKDIR target\webapp\services\newsletter
MKDIR target\webapp\services\newsletter
echo COPY src\webapp\services\newsletter\newsletter-service-controller.php target\webapp\services\newsletter\newsletter-service-controller.php
COPY src\webapp\services\newsletter\newsletter-service-controller.php target\webapp\services\newsletter\newsletter-service-controller.php
echo COPY src\webapp\services\newsletter\newsletter.service.php target\webapp\services\newsletter\newsletter.service.php
COPY src\webapp\services\newsletter\newsletter.service.php target\webapp\services\newsletter\newsletter.service.php
echo MKDIR target\webapp\services\pack-billetterie
MKDIR target\webapp\services\pack-billetterie
echo COPY src\webapp\services\pack-billetterie\pack-billetterie-service-controller.php target\webapp\services\pack-billetterie\pack-billetterie-service-controller.php
COPY src\webapp\services\pack-billetterie\pack-billetterie-service-controller.php target\webapp\services\pack-billetterie\pack-billetterie-service-controller.php
echo COPY src\webapp\services\pack-billetterie\pack-billetterie.service.php target\webapp\services\pack-billetterie\pack-billetterie.service.php
COPY src\webapp\services\pack-billetterie\pack-billetterie.service.php target\webapp\services\pack-billetterie\pack-billetterie.service.php
echo MKDIR target\webapp\services\paiement
MKDIR target\webapp\services\paiement
echo COPY src\webapp\services\paiement\paiement-service-controller.php target\webapp\services\paiement\paiement-service-controller.php
COPY src\webapp\services\paiement\paiement-service-controller.php target\webapp\services\paiement\paiement-service-controller.php
echo COPY src\webapp\services\paiement\paiement.service.php target\webapp\services\paiement\paiement.service.php
COPY src\webapp\services\paiement\paiement.service.php target\webapp\services\paiement\paiement.service.php
echo MKDIR target\webapp\services\produit-boutique
MKDIR target\webapp\services\produit-boutique
echo COPY src\webapp\services\produit-boutique\produit-boutique-service-controller.php target\webapp\services\produit-boutique\produit-boutique-service-controller.php
COPY src\webapp\services\produit-boutique\produit-boutique-service-controller.php target\webapp\services\produit-boutique\produit-boutique-service-controller.php
echo COPY src\webapp\services\produit-boutique\produit-boutique.service.php target\webapp\services\produit-boutique\produit-boutique.service.php
COPY src\webapp\services\produit-boutique\produit-boutique.service.php target\webapp\services\produit-boutique\produit-boutique.service.php
echo MKDIR target\webapp\services\shop-panier
MKDIR target\webapp\services\shop-panier
echo COPY src\webapp\services\shop-panier\shop-panier-service-controller.php target\webapp\services\shop-panier\shop-panier-service-controller.php
COPY src\webapp\services\shop-panier\shop-panier-service-controller.php target\webapp\services\shop-panier\shop-panier-service-controller.php
echo COPY src\webapp\services\shop-panier\shop-panier.service.php target\webapp\services\shop-panier\shop-panier.service.php
COPY src\webapp\services\shop-panier\shop-panier.service.php target\webapp\services\shop-panier\shop-panier.service.php
echo MKDIR target\webapp\services\stock-produit-boutique
MKDIR target\webapp\services\stock-produit-boutique
echo COPY src\webapp\services\stock-produit-boutique\stock-produit-boutique-service-controller.php target\webapp\services\stock-produit-boutique\stock-produit-boutique-service-controller.php
COPY src\webapp\services\stock-produit-boutique\stock-produit-boutique-service-controller.php target\webapp\services\stock-produit-boutique\stock-produit-boutique-service-controller.php
echo COPY src\webapp\services\stock-produit-boutique\stock-produit-boutique.service.php target\webapp\services\stock-produit-boutique\stock-produit-boutique.service.php
COPY src\webapp\services\stock-produit-boutique\stock-produit-boutique.service.php target\webapp\services\stock-produit-boutique\stock-produit-boutique.service.php
echo MKDIR target\webapp\services\video
MKDIR target\webapp\services\video
echo COPY src\webapp\services\video\video-service-controller.php target\webapp\services\video\video-service-controller.php
COPY src\webapp\services\video\video-service-controller.php target\webapp\services\video\video-service-controller.php
echo COPY src\webapp\services\video\video.service.php target\webapp\services\video\video.service.php
COPY src\webapp\services\video\video.service.php target\webapp\services\video\video.service.php
echo MKDIR target\webapp\uploaders
MKDIR target\webapp\uploaders
echo MKDIR target\webapp\uploaders\actualite
MKDIR target\webapp\uploaders\actualite
echo COPY src\webapp\uploaders\actualite\actualite-uploader-config.json target\webapp\uploaders\actualite\actualite-uploader-config.json
COPY src\webapp\uploaders\actualite\actualite-uploader-config.json target\webapp\uploaders\actualite\actualite-uploader-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\uploaders\actualite\actualite-uploader-controller.js -o target\webapp\uploaders\actualite\actualite-uploader-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\uploaders\actualite\actualite-uploader-controller.js -o target\webapp\uploaders\actualite\actualite-uploader-controller.js -c -m
echo COPY src\webapp\uploaders\actualite\actualite-uploader-controller.php target\webapp\uploaders\actualite\actualite-uploader-controller.php
COPY src\webapp\uploaders\actualite\actualite-uploader-controller.php target\webapp\uploaders\actualite\actualite-uploader-controller.php
echo COPY src\webapp\uploaders\actualite\actualite.uploader.php target\webapp\uploaders\actualite\actualite.uploader.php
COPY src\webapp\uploaders\actualite\actualite.uploader.php target\webapp\uploaders\actualite\actualite.uploader.php
echo MKDIR target\webapp\uploaders\album-photo
MKDIR target\webapp\uploaders\album-photo
echo COPY src\webapp\uploaders\album-photo\album-photo-uploader-config.json target\webapp\uploaders\album-photo\album-photo-uploader-config.json
COPY src\webapp\uploaders\album-photo\album-photo-uploader-config.json target\webapp\uploaders\album-photo\album-photo-uploader-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\uploaders\album-photo\album-photo-uploader-controller.js -o target\webapp\uploaders\album-photo\album-photo-uploader-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\uploaders\album-photo\album-photo-uploader-controller.js -o target\webapp\uploaders\album-photo\album-photo-uploader-controller.js -c -m
echo COPY src\webapp\uploaders\album-photo\album-photo-uploader-controller.php target\webapp\uploaders\album-photo\album-photo-uploader-controller.php
COPY src\webapp\uploaders\album-photo\album-photo-uploader-controller.php target\webapp\uploaders\album-photo\album-photo-uploader-controller.php
echo COPY src\webapp\uploaders\album-photo\album-photo.uploader.php target\webapp\uploaders\album-photo\album-photo.uploader.php
COPY src\webapp\uploaders\album-photo\album-photo.uploader.php target\webapp\uploaders\album-photo\album-photo.uploader.php
echo MKDIR target\webapp\uploaders\article-presse
MKDIR target\webapp\uploaders\article-presse
echo COPY src\webapp\uploaders\article-presse\article-presse-uploader-config.json target\webapp\uploaders\article-presse\article-presse-uploader-config.json
COPY src\webapp\uploaders\article-presse\article-presse-uploader-config.json target\webapp\uploaders\article-presse\article-presse-uploader-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\uploaders\article-presse\article-presse-uploader-controller.js -o target\webapp\uploaders\article-presse\article-presse-uploader-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\uploaders\article-presse\article-presse-uploader-controller.js -o target\webapp\uploaders\article-presse\article-presse-uploader-controller.js -c -m
echo COPY src\webapp\uploaders\article-presse\article-presse-uploader-controller.php target\webapp\uploaders\article-presse\article-presse-uploader-controller.php
COPY src\webapp\uploaders\article-presse\article-presse-uploader-controller.php target\webapp\uploaders\article-presse\article-presse-uploader-controller.php
echo COPY src\webapp\uploaders\article-presse\article-presse.uploader.php target\webapp\uploaders\article-presse\article-presse.uploader.php
COPY src\webapp\uploaders\article-presse\article-presse.uploader.php target\webapp\uploaders\article-presse\article-presse.uploader.php
echo MKDIR target\webapp\uploaders\document-coaching
MKDIR target\webapp\uploaders\document-coaching
echo COPY src\webapp\uploaders\document-coaching\document-coaching-uploader-config.json target\webapp\uploaders\document-coaching\document-coaching-uploader-config.json
COPY src\webapp\uploaders\document-coaching\document-coaching-uploader-config.json target\webapp\uploaders\document-coaching\document-coaching-uploader-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\uploaders\document-coaching\document-coaching-uploader-controller.js -o target\webapp\uploaders\document-coaching\document-coaching-uploader-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\uploaders\document-coaching\document-coaching-uploader-controller.js -o target\webapp\uploaders\document-coaching\document-coaching-uploader-controller.js -c -m
echo COPY src\webapp\uploaders\document-coaching\document-coaching-uploader-controller.php target\webapp\uploaders\document-coaching\document-coaching-uploader-controller.php
COPY src\webapp\uploaders\document-coaching\document-coaching-uploader-controller.php target\webapp\uploaders\document-coaching\document-coaching-uploader-controller.php
echo COPY src\webapp\uploaders\document-coaching\document-coaching.uploader.php target\webapp\uploaders\document-coaching\document-coaching.uploader.php
COPY src\webapp\uploaders\document-coaching\document-coaching.uploader.php target\webapp\uploaders\document-coaching\document-coaching.uploader.php
echo MKDIR target\webapp\uploaders\hhsj
MKDIR target\webapp\uploaders\hhsj
echo COPY src\webapp\uploaders\hhsj\hhsj-uploader-config.json target\webapp\uploaders\hhsj\hhsj-uploader-config.json
COPY src\webapp\uploaders\hhsj\hhsj-uploader-config.json target\webapp\uploaders\hhsj\hhsj-uploader-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\uploaders\hhsj\hhsj-uploader-controller.js -o target\webapp\uploaders\hhsj\hhsj-uploader-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\uploaders\hhsj\hhsj-uploader-controller.js -o target\webapp\uploaders\hhsj\hhsj-uploader-controller.js -c -m
echo COPY src\webapp\uploaders\hhsj\hhsj-uploader-controller.php target\webapp\uploaders\hhsj\hhsj-uploader-controller.php
COPY src\webapp\uploaders\hhsj\hhsj-uploader-controller.php target\webapp\uploaders\hhsj\hhsj-uploader-controller.php
echo COPY src\webapp\uploaders\hhsj\hhsj.uploader.php target\webapp\uploaders\hhsj\hhsj.uploader.php
COPY src\webapp\uploaders\hhsj\hhsj.uploader.php target\webapp\uploaders\hhsj\hhsj.uploader.php
echo MKDIR target\webapp\uploaders\match-fdm
MKDIR target\webapp\uploaders\match-fdm
echo COPY src\webapp\uploaders\match-fdm\match-fdm-uploader-config.json target\webapp\uploaders\match-fdm\match-fdm-uploader-config.json
COPY src\webapp\uploaders\match-fdm\match-fdm-uploader-config.json target\webapp\uploaders\match-fdm\match-fdm-uploader-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\uploaders\match-fdm\match-fdm-uploader-controller.js -o target\webapp\uploaders\match-fdm\match-fdm-uploader-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\uploaders\match-fdm\match-fdm-uploader-controller.js -o target\webapp\uploaders\match-fdm\match-fdm-uploader-controller.js -c -m
echo COPY src\webapp\uploaders\match-fdm\match-fdm-uploader-controller.php target\webapp\uploaders\match-fdm\match-fdm-uploader-controller.php
COPY src\webapp\uploaders\match-fdm\match-fdm-uploader-controller.php target\webapp\uploaders\match-fdm\match-fdm-uploader-controller.php
echo COPY src\webapp\uploaders\match-fdm\match-fdm.uploader.php target\webapp\uploaders\match-fdm\match-fdm.uploader.php
COPY src\webapp\uploaders\match-fdm\match-fdm.uploader.php target\webapp\uploaders\match-fdm\match-fdm.uploader.php
echo MKDIR target\webapp\uploaders\produit-boutique
MKDIR target\webapp\uploaders\produit-boutique
echo COPY src\webapp\uploaders\produit-boutique\produit-boutique-uploader-config.json target\webapp\uploaders\produit-boutique\produit-boutique-uploader-config.json
COPY src\webapp\uploaders\produit-boutique\produit-boutique-uploader-config.json target\webapp\uploaders\produit-boutique\produit-boutique-uploader-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\uploaders\produit-boutique\produit-boutique-uploader-controller.js -o target\webapp\uploaders\produit-boutique\produit-boutique-uploader-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\uploaders\produit-boutique\produit-boutique-uploader-controller.js -o target\webapp\uploaders\produit-boutique\produit-boutique-uploader-controller.js -c -m
echo COPY src\webapp\uploaders\produit-boutique\produit-boutique-uploader-controller.php target\webapp\uploaders\produit-boutique\produit-boutique-uploader-controller.php
COPY src\webapp\uploaders\produit-boutique\produit-boutique-uploader-controller.php target\webapp\uploaders\produit-boutique\produit-boutique-uploader-controller.php
echo COPY src\webapp\uploaders\produit-boutique\produit-boutique.uploader.php target\webapp\uploaders\produit-boutique\produit-boutique.uploader.php
COPY src\webapp\uploaders\produit-boutique\produit-boutique.uploader.php target\webapp\uploaders\produit-boutique\produit-boutique.uploader.php
echo MKDIR target\webapp\uploaders\video
MKDIR target\webapp\uploaders\video
echo COPY src\webapp\uploaders\video\video-uploader-config.json target\webapp\uploaders\video\video-uploader-config.json
COPY src\webapp\uploaders\video\video-uploader-config.json target\webapp\uploaders\video\video-uploader-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\uploaders\video\video-uploader-controller.js -o target\webapp\uploaders\video\video-uploader-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\uploaders\video\video-uploader-controller.js -o target\webapp\uploaders\video\video-uploader-controller.js -c -m
echo COPY src\webapp\uploaders\video\video-uploader-controller.php target\webapp\uploaders\video\video-uploader-controller.php
COPY src\webapp\uploaders\video\video-uploader-controller.php target\webapp\uploaders\video\video-uploader-controller.php
echo COPY src\webapp\uploaders\video\video.uploader.php target\webapp\uploaders\video\video.uploader.php
COPY src\webapp\uploaders\video\video.uploader.php target\webapp\uploaders\video\video.uploader.php
echo MKDIR target\webapp\views
MKDIR target\webapp\views
echo MKDIR target\webapp\views\admin
MKDIR target\webapp\views\admin
echo COPY src\webapp\views\admin\admin.html.php target\webapp\views\admin\admin.html.php
COPY src\webapp\views\admin\admin.html.php target\webapp\views\admin\admin.html.php
echo MKDIR target\webapp\views\admin\coaching
MKDIR target\webapp\views\admin\coaching
echo COPY src\webapp\views\admin\coaching\admin-coaching.html.php target\webapp\views\admin\coaching\admin-coaching.html.php
COPY src\webapp\views\admin\coaching\admin-coaching.html.php target\webapp\views\admin\coaching\admin-coaching.html.php
echo MKDIR target\webapp\views\admin\coaching\common
MKDIR target\webapp\views\admin\coaching\common
echo MKDIR target\webapp\views\admin\coaching\common\modals
MKDIR target\webapp\views\admin\coaching\common\modals
echo MKDIR target\webapp\views\admin\coaching\common\modals\mailing-list
MKDIR target\webapp\views\admin\coaching\common\modals\mailing-list
echo COPY src\webapp\views\admin\coaching\common\modals\mailing-list\modal-mailing-list-content.html.php target\webapp\views\admin\coaching\common\modals\mailing-list\modal-mailing-list-content.html.php
COPY src\webapp\views\admin\coaching\common\modals\mailing-list\modal-mailing-list-content.html.php target\webapp\views\admin\coaching\common\modals\mailing-list\modal-mailing-list-content.html.php
echo COPY src\webapp\views\admin\coaching\common\modals\mailing-list\modal-mailing-list-controller.php target\webapp\views\admin\coaching\common\modals\mailing-list\modal-mailing-list-controller.php
COPY src\webapp\views\admin\coaching\common\modals\mailing-list\modal-mailing-list-controller.php target\webapp\views\admin\coaching\common\modals\mailing-list\modal-mailing-list-controller.php
echo COPY src\webapp\views\admin\coaching\common\modals\mailing-list\modal-mailing-list.html.php target\webapp\views\admin\coaching\common\modals\mailing-list\modal-mailing-list.html.php
COPY src\webapp\views\admin\coaching\common\modals\mailing-list\modal-mailing-list.html.php target\webapp\views\admin\coaching\common\modals\mailing-list\modal-mailing-list.html.php
echo MKDIR target\webapp\views\admin\coaching\documents
MKDIR target\webapp\views\admin\coaching\documents
echo COPY src\webapp\views\admin\coaching\documents\admin-coaching-documents-config.json target\webapp\views\admin\coaching\documents\admin-coaching-documents-config.json
COPY src\webapp\views\admin\coaching\documents\admin-coaching-documents-config.json target\webapp\views\admin\coaching\documents\admin-coaching-documents-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\coaching\documents\admin-coaching-documents-controller.js -o target\webapp\views\admin\coaching\documents\admin-coaching-documents-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\coaching\documents\admin-coaching-documents-controller.js -o target\webapp\views\admin\coaching\documents\admin-coaching-documents-controller.js -c -m
echo COPY src\webapp\views\admin\coaching\documents\admin-coaching-documents-controller.php target\webapp\views\admin\coaching\documents\admin-coaching-documents-controller.php
COPY src\webapp\views\admin\coaching\documents\admin-coaching-documents-controller.php target\webapp\views\admin\coaching\documents\admin-coaching-documents-controller.php
echo COPY src\webapp\views\admin\coaching\documents\admin-coaching-documents.html.php target\webapp\views\admin\coaching\documents\admin-coaching-documents.html.php
COPY src\webapp\views\admin\coaching\documents\admin-coaching-documents.html.php target\webapp\views\admin\coaching\documents\admin-coaching-documents.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\admin\coaching\documents\admin-coaching-documents.less target\webapp\views\admin\coaching\documents\admin-coaching-documents.css
node ..\node_modules\less\bin\lessc src\webapp\views\admin\coaching\documents\admin-coaching-documents.less target\webapp\views\admin\coaching\documents\admin-coaching-documents.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\coaching\documents\admin-coaching-documents.css > target\webapp\views\admin\coaching\documents\admin-coaching-documents.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\coaching\documents\admin-coaching-documents.css > target\webapp\views\admin\coaching\documents\admin-coaching-documents.css.prod
echo DEL target\webapp\views\admin\coaching\documents\admin-coaching-documents.css
DEL target\webapp\views\admin\coaching\documents\admin-coaching-documents.css
echo MOVE target\webapp\views\admin\coaching\documents\admin-coaching-documents.css.prod target\webapp\views\admin\coaching\documents\admin-coaching-documents.css
MOVE target\webapp\views\admin\coaching\documents\admin-coaching-documents.css.prod target\webapp\views\admin\coaching\documents\admin-coaching-documents.css
echo MKDIR target\webapp\views\admin\coaching\documents\modals
MKDIR target\webapp\views\admin\coaching\documents\modals
echo MKDIR target\webapp\views\admin\coaching\documents\modals\save
MKDIR target\webapp\views\admin\coaching\documents\modals\save
echo COPY src\webapp\views\admin\coaching\documents\modals\save\modal-save-document-content.html.php target\webapp\views\admin\coaching\documents\modals\save\modal-save-document-content.html.php
COPY src\webapp\views\admin\coaching\documents\modals\save\modal-save-document-content.html.php target\webapp\views\admin\coaching\documents\modals\save\modal-save-document-content.html.php
echo COPY src\webapp\views\admin\coaching\documents\modals\save\modal-save-document-controller.php target\webapp\views\admin\coaching\documents\modals\save\modal-save-document-controller.php
COPY src\webapp\views\admin\coaching\documents\modals\save\modal-save-document-controller.php target\webapp\views\admin\coaching\documents\modals\save\modal-save-document-controller.php
echo COPY src\webapp\views\admin\coaching\documents\modals\save\modal-save-document.html.php target\webapp\views\admin\coaching\documents\modals\save\modal-save-document.html.php
COPY src\webapp\views\admin\coaching\documents\modals\save\modal-save-document.html.php target\webapp\views\admin\coaching\documents\modals\save\modal-save-document.html.php
echo MKDIR target\webapp\views\admin\coaching\documents\views
MKDIR target\webapp\views\admin\coaching\documents\views
echo MKDIR target\webapp\views\admin\coaching\documents\views\recherche
MKDIR target\webapp\views\admin\coaching\documents\views\recherche
echo COPY src\webapp\views\admin\coaching\documents\views\recherche\recherche-documents-controller.php target\webapp\views\admin\coaching\documents\views\recherche\recherche-documents-controller.php
COPY src\webapp\views\admin\coaching\documents\views\recherche\recherche-documents-controller.php target\webapp\views\admin\coaching\documents\views\recherche\recherche-documents-controller.php
echo COPY src\webapp\views\admin\coaching\documents\views\recherche\recherche-documents.html.php target\webapp\views\admin\coaching\documents\views\recherche\recherche-documents.html.php
COPY src\webapp\views\admin\coaching\documents\views\recherche\recherche-documents.html.php target\webapp\views\admin\coaching\documents\views\recherche\recherche-documents.html.php
echo MKDIR target\webapp\views\admin\coaching\documents\views\stats
MKDIR target\webapp\views\admin\coaching\documents\views\stats
echo COPY src\webapp\views\admin\coaching\documents\views\stats\stats-documents-controller.php target\webapp\views\admin\coaching\documents\views\stats\stats-documents-controller.php
COPY src\webapp\views\admin\coaching\documents\views\stats\stats-documents-controller.php target\webapp\views\admin\coaching\documents\views\stats\stats-documents-controller.php
echo COPY src\webapp\views\admin\coaching\documents\views\stats\stats-documents.html.php target\webapp\views\admin\coaching\documents\views\stats\stats-documents.html.php
COPY src\webapp\views\admin\coaching\documents\views\stats\stats-documents.html.php target\webapp\views\admin\coaching\documents\views\stats\stats-documents.html.php
echo MKDIR target\webapp\views\admin\coaching\documents\views\table
MKDIR target\webapp\views\admin\coaching\documents\views\table
echo COPY src\webapp\views\admin\coaching\documents\views\table\table-documents-controller.php target\webapp\views\admin\coaching\documents\views\table\table-documents-controller.php
COPY src\webapp\views\admin\coaching\documents\views\table\table-documents-controller.php target\webapp\views\admin\coaching\documents\views\table\table-documents-controller.php
echo COPY src\webapp\views\admin\coaching\documents\views\table\table-documents.html.php target\webapp\views\admin\coaching\documents\views\table\table-documents.html.php
COPY src\webapp\views\admin\coaching\documents\views\table\table-documents.html.php target\webapp\views\admin\coaching\documents\views\table\table-documents.html.php
echo MKDIR target\webapp\views\admin\coaching\equipes
MKDIR target\webapp\views\admin\coaching\equipes
echo COPY src\webapp\views\admin\coaching\equipes\admin-coaching-equipes-config.json target\webapp\views\admin\coaching\equipes\admin-coaching-equipes-config.json
COPY src\webapp\views\admin\coaching\equipes\admin-coaching-equipes-config.json target\webapp\views\admin\coaching\equipes\admin-coaching-equipes-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\coaching\equipes\admin-coaching-equipes-controller.js -o target\webapp\views\admin\coaching\equipes\admin-coaching-equipes-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\coaching\equipes\admin-coaching-equipes-controller.js -o target\webapp\views\admin\coaching\equipes\admin-coaching-equipes-controller.js -c -m
echo COPY src\webapp\views\admin\coaching\equipes\admin-coaching-equipes-controller.php target\webapp\views\admin\coaching\equipes\admin-coaching-equipes-controller.php
COPY src\webapp\views\admin\coaching\equipes\admin-coaching-equipes-controller.php target\webapp\views\admin\coaching\equipes\admin-coaching-equipes-controller.php
echo COPY src\webapp\views\admin\coaching\equipes\admin-coaching-equipes.html.php target\webapp\views\admin\coaching\equipes\admin-coaching-equipes.html.php
COPY src\webapp\views\admin\coaching\equipes\admin-coaching-equipes.html.php target\webapp\views\admin\coaching\equipes\admin-coaching-equipes.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\admin\coaching\equipes\admin-coaching-equipes.less target\webapp\views\admin\coaching\equipes\admin-coaching-equipes.css
node ..\node_modules\less\bin\lessc src\webapp\views\admin\coaching\equipes\admin-coaching-equipes.less target\webapp\views\admin\coaching\equipes\admin-coaching-equipes.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\coaching\equipes\admin-coaching-equipes.css > target\webapp\views\admin\coaching\equipes\admin-coaching-equipes.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\coaching\equipes\admin-coaching-equipes.css > target\webapp\views\admin\coaching\equipes\admin-coaching-equipes.css.prod
echo DEL target\webapp\views\admin\coaching\equipes\admin-coaching-equipes.css
DEL target\webapp\views\admin\coaching\equipes\admin-coaching-equipes.css
echo MOVE target\webapp\views\admin\coaching\equipes\admin-coaching-equipes.css.prod target\webapp\views\admin\coaching\equipes\admin-coaching-equipes.css
MOVE target\webapp\views\admin\coaching\equipes\admin-coaching-equipes.css.prod target\webapp\views\admin\coaching\equipes\admin-coaching-equipes.css
echo MKDIR target\webapp\views\admin\coaching\equipes\modals
MKDIR target\webapp\views\admin\coaching\equipes\modals
echo MKDIR target\webapp\views\admin\coaching\equipes\modals\coachs
MKDIR target\webapp\views\admin\coaching\equipes\modals\coachs
echo COPY src\webapp\views\admin\coaching\equipes\modals\coachs\modal-coachs-categorie-content.html.php target\webapp\views\admin\coaching\equipes\modals\coachs\modal-coachs-categorie-content.html.php
COPY src\webapp\views\admin\coaching\equipes\modals\coachs\modal-coachs-categorie-content.html.php target\webapp\views\admin\coaching\equipes\modals\coachs\modal-coachs-categorie-content.html.php
echo COPY src\webapp\views\admin\coaching\equipes\modals\coachs\modal-coachs-categorie-controller.php target\webapp\views\admin\coaching\equipes\modals\coachs\modal-coachs-categorie-controller.php
COPY src\webapp\views\admin\coaching\equipes\modals\coachs\modal-coachs-categorie-controller.php target\webapp\views\admin\coaching\equipes\modals\coachs\modal-coachs-categorie-controller.php
echo COPY src\webapp\views\admin\coaching\equipes\modals\coachs\modal-coachs-categorie.html.php target\webapp\views\admin\coaching\equipes\modals\coachs\modal-coachs-categorie.html.php
COPY src\webapp\views\admin\coaching\equipes\modals\coachs\modal-coachs-categorie.html.php target\webapp\views\admin\coaching\equipes\modals\coachs\modal-coachs-categorie.html.php
echo MKDIR target\webapp\views\admin\coaching\equipes\modals\entrainements
MKDIR target\webapp\views\admin\coaching\equipes\modals\entrainements
echo COPY src\webapp\views\admin\coaching\equipes\modals\entrainements\modal-entrainements-categorie-content.html.php target\webapp\views\admin\coaching\equipes\modals\entrainements\modal-entrainements-categorie-content.html.php
COPY src\webapp\views\admin\coaching\equipes\modals\entrainements\modal-entrainements-categorie-content.html.php target\webapp\views\admin\coaching\equipes\modals\entrainements\modal-entrainements-categorie-content.html.php
echo COPY src\webapp\views\admin\coaching\equipes\modals\entrainements\modal-entrainements-categorie-controller.php target\webapp\views\admin\coaching\equipes\modals\entrainements\modal-entrainements-categorie-controller.php
COPY src\webapp\views\admin\coaching\equipes\modals\entrainements\modal-entrainements-categorie-controller.php target\webapp\views\admin\coaching\equipes\modals\entrainements\modal-entrainements-categorie-controller.php
echo COPY src\webapp\views\admin\coaching\equipes\modals\entrainements\modal-entrainements-categorie.html.php target\webapp\views\admin\coaching\equipes\modals\entrainements\modal-entrainements-categorie.html.php
COPY src\webapp\views\admin\coaching\equipes\modals\entrainements\modal-entrainements-categorie.html.php target\webapp\views\admin\coaching\equipes\modals\entrainements\modal-entrainements-categorie.html.php
echo MKDIR target\webapp\views\admin\coaching\equipes\modals\save
MKDIR target\webapp\views\admin\coaching\equipes\modals\save
echo COPY src\webapp\views\admin\coaching\equipes\modals\save\modal-save-categorie-content.html.php target\webapp\views\admin\coaching\equipes\modals\save\modal-save-categorie-content.html.php
COPY src\webapp\views\admin\coaching\equipes\modals\save\modal-save-categorie-content.html.php target\webapp\views\admin\coaching\equipes\modals\save\modal-save-categorie-content.html.php
echo COPY src\webapp\views\admin\coaching\equipes\modals\save\modal-save-categorie-controller.php target\webapp\views\admin\coaching\equipes\modals\save\modal-save-categorie-controller.php
COPY src\webapp\views\admin\coaching\equipes\modals\save\modal-save-categorie-controller.php target\webapp\views\admin\coaching\equipes\modals\save\modal-save-categorie-controller.php
echo COPY src\webapp\views\admin\coaching\equipes\modals\save\modal-save-categorie.html.php target\webapp\views\admin\coaching\equipes\modals\save\modal-save-categorie.html.php
COPY src\webapp\views\admin\coaching\equipes\modals\save\modal-save-categorie.html.php target\webapp\views\admin\coaching\equipes\modals\save\modal-save-categorie.html.php
echo MKDIR target\webapp\views\admin\coaching\equipes\views
MKDIR target\webapp\views\admin\coaching\equipes\views
echo MKDIR target\webapp\views\admin\coaching\equipes\views\stats
MKDIR target\webapp\views\admin\coaching\equipes\views\stats
echo COPY src\webapp\views\admin\coaching\equipes\views\stats\stats-equipes-controller.php target\webapp\views\admin\coaching\equipes\views\stats\stats-equipes-controller.php
COPY src\webapp\views\admin\coaching\equipes\views\stats\stats-equipes-controller.php target\webapp\views\admin\coaching\equipes\views\stats\stats-equipes-controller.php
echo COPY src\webapp\views\admin\coaching\equipes\views\stats\stats-equipes.html.php target\webapp\views\admin\coaching\equipes\views\stats\stats-equipes.html.php
COPY src\webapp\views\admin\coaching\equipes\views\stats\stats-equipes.html.php target\webapp\views\admin\coaching\equipes\views\stats\stats-equipes.html.php
echo MKDIR target\webapp\views\admin\coaching\equipes\views\table
MKDIR target\webapp\views\admin\coaching\equipes\views\table
echo COPY src\webapp\views\admin\coaching\equipes\views\table\table-equipes-controller.php target\webapp\views\admin\coaching\equipes\views\table\table-equipes-controller.php
COPY src\webapp\views\admin\coaching\equipes\views\table\table-equipes-controller.php target\webapp\views\admin\coaching\equipes\views\table\table-equipes-controller.php
echo COPY src\webapp\views\admin\coaching\equipes\views\table\table-equipes.html.php target\webapp\views\admin\coaching\equipes\views\table\table-equipes.html.php
COPY src\webapp\views\admin\coaching\equipes\views\table\table-equipes.html.php target\webapp\views\admin\coaching\equipes\views\table\table-equipes.html.php
echo MKDIR target\webapp\views\admin\coaching\licencies
MKDIR target\webapp\views\admin\coaching\licencies
echo COPY src\webapp\views\admin\coaching\licencies\admin-coaching-licencies-config.json target\webapp\views\admin\coaching\licencies\admin-coaching-licencies-config.json
COPY src\webapp\views\admin\coaching\licencies\admin-coaching-licencies-config.json target\webapp\views\admin\coaching\licencies\admin-coaching-licencies-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\coaching\licencies\admin-coaching-licencies-controller.js -o target\webapp\views\admin\coaching\licencies\admin-coaching-licencies-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\coaching\licencies\admin-coaching-licencies-controller.js -o target\webapp\views\admin\coaching\licencies\admin-coaching-licencies-controller.js -c -m
echo COPY src\webapp\views\admin\coaching\licencies\admin-coaching-licencies-controller.php target\webapp\views\admin\coaching\licencies\admin-coaching-licencies-controller.php
COPY src\webapp\views\admin\coaching\licencies\admin-coaching-licencies-controller.php target\webapp\views\admin\coaching\licencies\admin-coaching-licencies-controller.php
echo COPY src\webapp\views\admin\coaching\licencies\admin-coaching-licencies.html.php target\webapp\views\admin\coaching\licencies\admin-coaching-licencies.html.php
COPY src\webapp\views\admin\coaching\licencies\admin-coaching-licencies.html.php target\webapp\views\admin\coaching\licencies\admin-coaching-licencies.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\admin\coaching\licencies\admin-coaching-licencies.less target\webapp\views\admin\coaching\licencies\admin-coaching-licencies.css
node ..\node_modules\less\bin\lessc src\webapp\views\admin\coaching\licencies\admin-coaching-licencies.less target\webapp\views\admin\coaching\licencies\admin-coaching-licencies.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\coaching\licencies\admin-coaching-licencies.css > target\webapp\views\admin\coaching\licencies\admin-coaching-licencies.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\coaching\licencies\admin-coaching-licencies.css > target\webapp\views\admin\coaching\licencies\admin-coaching-licencies.css.prod
echo DEL target\webapp\views\admin\coaching\licencies\admin-coaching-licencies.css
DEL target\webapp\views\admin\coaching\licencies\admin-coaching-licencies.css
echo MOVE target\webapp\views\admin\coaching\licencies\admin-coaching-licencies.css.prod target\webapp\views\admin\coaching\licencies\admin-coaching-licencies.css
MOVE target\webapp\views\admin\coaching\licencies\admin-coaching-licencies.css.prod target\webapp\views\admin\coaching\licencies\admin-coaching-licencies.css
echo MKDIR target\webapp\views\admin\coaching\licencies\modals
MKDIR target\webapp\views\admin\coaching\licencies\modals
echo MKDIR target\webapp\views\admin\coaching\licencies\modals\actions-club
MKDIR target\webapp\views\admin\coaching\licencies\modals\actions-club
echo COPY src\webapp\views\admin\coaching\licencies\modals\actions-club\modal-actions-club-licencie-content.html.php target\webapp\views\admin\coaching\licencies\modals\actions-club\modal-actions-club-licencie-content.html.php
COPY src\webapp\views\admin\coaching\licencies\modals\actions-club\modal-actions-club-licencie-content.html.php target\webapp\views\admin\coaching\licencies\modals\actions-club\modal-actions-club-licencie-content.html.php
echo COPY src\webapp\views\admin\coaching\licencies\modals\actions-club\modal-actions-club-licencie-controller.php target\webapp\views\admin\coaching\licencies\modals\actions-club\modal-actions-club-licencie-controller.php
COPY src\webapp\views\admin\coaching\licencies\modals\actions-club\modal-actions-club-licencie-controller.php target\webapp\views\admin\coaching\licencies\modals\actions-club\modal-actions-club-licencie-controller.php
echo COPY src\webapp\views\admin\coaching\licencies\modals\actions-club\modal-actions-club-licencie.html.php target\webapp\views\admin\coaching\licencies\modals\actions-club\modal-actions-club-licencie.html.php
COPY src\webapp\views\admin\coaching\licencies\modals\actions-club\modal-actions-club-licencie.html.php target\webapp\views\admin\coaching\licencies\modals\actions-club\modal-actions-club-licencie.html.php
echo MKDIR target\webapp\views\admin\coaching\licencies\modals\arbitrages
MKDIR target\webapp\views\admin\coaching\licencies\modals\arbitrages
echo COPY src\webapp\views\admin\coaching\licencies\modals\arbitrages\modal-arbitrages-licencie-content.html.php target\webapp\views\admin\coaching\licencies\modals\arbitrages\modal-arbitrages-licencie-content.html.php
COPY src\webapp\views\admin\coaching\licencies\modals\arbitrages\modal-arbitrages-licencie-content.html.php target\webapp\views\admin\coaching\licencies\modals\arbitrages\modal-arbitrages-licencie-content.html.php
echo COPY src\webapp\views\admin\coaching\licencies\modals\arbitrages\modal-arbitrages-licencie-controller.php target\webapp\views\admin\coaching\licencies\modals\arbitrages\modal-arbitrages-licencie-controller.php
COPY src\webapp\views\admin\coaching\licencies\modals\arbitrages\modal-arbitrages-licencie-controller.php target\webapp\views\admin\coaching\licencies\modals\arbitrages\modal-arbitrages-licencie-controller.php
echo COPY src\webapp\views\admin\coaching\licencies\modals\arbitrages\modal-arbitrages-licencie.html.php target\webapp\views\admin\coaching\licencies\modals\arbitrages\modal-arbitrages-licencie.html.php
COPY src\webapp\views\admin\coaching\licencies\modals\arbitrages\modal-arbitrages-licencie.html.php target\webapp\views\admin\coaching\licencies\modals\arbitrages\modal-arbitrages-licencie.html.php
echo MKDIR target\webapp\views\admin\coaching\licencies\modals\save
MKDIR target\webapp\views\admin\coaching\licencies\modals\save
echo COPY src\webapp\views\admin\coaching\licencies\modals\save\modal-save-licencie-content.html.php target\webapp\views\admin\coaching\licencies\modals\save\modal-save-licencie-content.html.php
COPY src\webapp\views\admin\coaching\licencies\modals\save\modal-save-licencie-content.html.php target\webapp\views\admin\coaching\licencies\modals\save\modal-save-licencie-content.html.php
echo COPY src\webapp\views\admin\coaching\licencies\modals\save\modal-save-licencie-controller.php target\webapp\views\admin\coaching\licencies\modals\save\modal-save-licencie-controller.php
COPY src\webapp\views\admin\coaching\licencies\modals\save\modal-save-licencie-controller.php target\webapp\views\admin\coaching\licencies\modals\save\modal-save-licencie-controller.php
echo COPY src\webapp\views\admin\coaching\licencies\modals\save\modal-save-licencie.html.php target\webapp\views\admin\coaching\licencies\modals\save\modal-save-licencie.html.php
COPY src\webapp\views\admin\coaching\licencies\modals\save\modal-save-licencie.html.php target\webapp\views\admin\coaching\licencies\modals\save\modal-save-licencie.html.php
echo MKDIR target\webapp\views\admin\coaching\licencies\views
MKDIR target\webapp\views\admin\coaching\licencies\views
echo MKDIR target\webapp\views\admin\coaching\licencies\views\recherche
MKDIR target\webapp\views\admin\coaching\licencies\views\recherche
echo COPY src\webapp\views\admin\coaching\licencies\views\recherche\recherche-licencies-controller.php target\webapp\views\admin\coaching\licencies\views\recherche\recherche-licencies-controller.php
COPY src\webapp\views\admin\coaching\licencies\views\recherche\recherche-licencies-controller.php target\webapp\views\admin\coaching\licencies\views\recherche\recherche-licencies-controller.php
echo COPY src\webapp\views\admin\coaching\licencies\views\recherche\recherche-licencies.html.php target\webapp\views\admin\coaching\licencies\views\recherche\recherche-licencies.html.php
COPY src\webapp\views\admin\coaching\licencies\views\recherche\recherche-licencies.html.php target\webapp\views\admin\coaching\licencies\views\recherche\recherche-licencies.html.php
echo MKDIR target\webapp\views\admin\coaching\licencies\views\stats
MKDIR target\webapp\views\admin\coaching\licencies\views\stats
echo COPY src\webapp\views\admin\coaching\licencies\views\stats\stats-licencies-controller.php target\webapp\views\admin\coaching\licencies\views\stats\stats-licencies-controller.php
COPY src\webapp\views\admin\coaching\licencies\views\stats\stats-licencies-controller.php target\webapp\views\admin\coaching\licencies\views\stats\stats-licencies-controller.php
echo COPY src\webapp\views\admin\coaching\licencies\views\stats\stats-licencies.html.php target\webapp\views\admin\coaching\licencies\views\stats\stats-licencies.html.php
COPY src\webapp\views\admin\coaching\licencies\views\stats\stats-licencies.html.php target\webapp\views\admin\coaching\licencies\views\stats\stats-licencies.html.php
echo MKDIR target\webapp\views\admin\coaching\licencies\views\table
MKDIR target\webapp\views\admin\coaching\licencies\views\table
echo COPY src\webapp\views\admin\coaching\licencies\views\table\table-licencies-controller.php target\webapp\views\admin\coaching\licencies\views\table\table-licencies-controller.php
COPY src\webapp\views\admin\coaching\licencies\views\table\table-licencies-controller.php target\webapp\views\admin\coaching\licencies\views\table\table-licencies-controller.php
echo COPY src\webapp\views\admin\coaching\licencies\views\table\table-licencies.html.php target\webapp\views\admin\coaching\licencies\views\table\table-licencies.html.php
COPY src\webapp\views\admin\coaching\licencies\views\table\table-licencies.html.php target\webapp\views\admin\coaching\licencies\views\table\table-licencies.html.php
echo MKDIR target\webapp\views\admin\coaching\sub-menu
MKDIR target\webapp\views\admin\coaching\sub-menu
echo COPY src\webapp\views\admin\coaching\sub-menu\sub-menu-admin-coaching.html.php target\webapp\views\admin\coaching\sub-menu\sub-menu-admin-coaching.html.php
COPY src\webapp\views\admin\coaching\sub-menu\sub-menu-admin-coaching.html.php target\webapp\views\admin\coaching\sub-menu\sub-menu-admin-coaching.html.php
echo MKDIR target\webapp\views\admin\common
MKDIR target\webapp\views\admin\common
echo MKDIR target\webapp\views\admin\common\nav
MKDIR target\webapp\views\admin\common\nav
echo COPY src\webapp\views\admin\common\nav\nav.html.php target\webapp\views\admin\common\nav\nav.html.php
COPY src\webapp\views\admin\common\nav\nav.html.php target\webapp\views\admin\common\nav\nav.html.php
echo MKDIR target\webapp\views\admin\edition
MKDIR target\webapp\views\admin\edition
echo COPY src\webapp\views\admin\edition\admin-edition.html.php target\webapp\views\admin\edition\admin-edition.html.php
COPY src\webapp\views\admin\edition\admin-edition.html.php target\webapp\views\admin\edition\admin-edition.html.php
echo MKDIR target\webapp\views\admin\edition\hhsj
MKDIR target\webapp\views\admin\edition\hhsj
echo COPY src\webapp\views\admin\edition\hhsj\admin-edition-hhsj-config.json target\webapp\views\admin\edition\hhsj\admin-edition-hhsj-config.json
COPY src\webapp\views\admin\edition\hhsj\admin-edition-hhsj-config.json target\webapp\views\admin\edition\hhsj\admin-edition-hhsj-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\edition\hhsj\admin-edition-hhsj-controller.js -o target\webapp\views\admin\edition\hhsj\admin-edition-hhsj-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\edition\hhsj\admin-edition-hhsj-controller.js -o target\webapp\views\admin\edition\hhsj\admin-edition-hhsj-controller.js -c -m
echo COPY src\webapp\views\admin\edition\hhsj\admin-edition-hhsj-controller.php target\webapp\views\admin\edition\hhsj\admin-edition-hhsj-controller.php
COPY src\webapp\views\admin\edition\hhsj\admin-edition-hhsj-controller.php target\webapp\views\admin\edition\hhsj\admin-edition-hhsj-controller.php
echo COPY src\webapp\views\admin\edition\hhsj\admin-edition-hhsj.html.php target\webapp\views\admin\edition\hhsj\admin-edition-hhsj.html.php
COPY src\webapp\views\admin\edition\hhsj\admin-edition-hhsj.html.php target\webapp\views\admin\edition\hhsj\admin-edition-hhsj.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\admin\edition\hhsj\admin-edition-hhsj.less target\webapp\views\admin\edition\hhsj\admin-edition-hhsj.css
node ..\node_modules\less\bin\lessc src\webapp\views\admin\edition\hhsj\admin-edition-hhsj.less target\webapp\views\admin\edition\hhsj\admin-edition-hhsj.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\edition\hhsj\admin-edition-hhsj.css > target\webapp\views\admin\edition\hhsj\admin-edition-hhsj.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\edition\hhsj\admin-edition-hhsj.css > target\webapp\views\admin\edition\hhsj\admin-edition-hhsj.css.prod
echo DEL target\webapp\views\admin\edition\hhsj\admin-edition-hhsj.css
DEL target\webapp\views\admin\edition\hhsj\admin-edition-hhsj.css
echo MOVE target\webapp\views\admin\edition\hhsj\admin-edition-hhsj.css.prod target\webapp\views\admin\edition\hhsj\admin-edition-hhsj.css
MOVE target\webapp\views\admin\edition\hhsj\admin-edition-hhsj.css.prod target\webapp\views\admin\edition\hhsj\admin-edition-hhsj.css
echo MKDIR target\webapp\views\admin\edition\hhsj\modals
MKDIR target\webapp\views\admin\edition\hhsj\modals
echo MKDIR target\webapp\views\admin\edition\hhsj\modals\save
MKDIR target\webapp\views\admin\edition\hhsj\modals\save
echo COPY src\webapp\views\admin\edition\hhsj\modals\save\modal-save-hhsj-content.html.php target\webapp\views\admin\edition\hhsj\modals\save\modal-save-hhsj-content.html.php
COPY src\webapp\views\admin\edition\hhsj\modals\save\modal-save-hhsj-content.html.php target\webapp\views\admin\edition\hhsj\modals\save\modal-save-hhsj-content.html.php
echo COPY src\webapp\views\admin\edition\hhsj\modals\save\modal-save-hhsj-controller.php target\webapp\views\admin\edition\hhsj\modals\save\modal-save-hhsj-controller.php
COPY src\webapp\views\admin\edition\hhsj\modals\save\modal-save-hhsj-controller.php target\webapp\views\admin\edition\hhsj\modals\save\modal-save-hhsj-controller.php
echo COPY src\webapp\views\admin\edition\hhsj\modals\save\modal-save-hhsj.html.php target\webapp\views\admin\edition\hhsj\modals\save\modal-save-hhsj.html.php
COPY src\webapp\views\admin\edition\hhsj\modals\save\modal-save-hhsj.html.php target\webapp\views\admin\edition\hhsj\modals\save\modal-save-hhsj.html.php
echo MKDIR target\webapp\views\admin\edition\hhsj\views
MKDIR target\webapp\views\admin\edition\hhsj\views
echo MKDIR target\webapp\views\admin\edition\hhsj\views\recherche
MKDIR target\webapp\views\admin\edition\hhsj\views\recherche
echo COPY src\webapp\views\admin\edition\hhsj\views\recherche\recherche-hhsj-controller.php target\webapp\views\admin\edition\hhsj\views\recherche\recherche-hhsj-controller.php
COPY src\webapp\views\admin\edition\hhsj\views\recherche\recherche-hhsj-controller.php target\webapp\views\admin\edition\hhsj\views\recherche\recherche-hhsj-controller.php
echo COPY src\webapp\views\admin\edition\hhsj\views\recherche\recherche-hhsj.html.php target\webapp\views\admin\edition\hhsj\views\recherche\recherche-hhsj.html.php
COPY src\webapp\views\admin\edition\hhsj\views\recherche\recherche-hhsj.html.php target\webapp\views\admin\edition\hhsj\views\recherche\recherche-hhsj.html.php
echo MKDIR target\webapp\views\admin\edition\hhsj\views\stats
MKDIR target\webapp\views\admin\edition\hhsj\views\stats
echo COPY src\webapp\views\admin\edition\hhsj\views\stats\stats-hhsj-controller.php target\webapp\views\admin\edition\hhsj\views\stats\stats-hhsj-controller.php
COPY src\webapp\views\admin\edition\hhsj\views\stats\stats-hhsj-controller.php target\webapp\views\admin\edition\hhsj\views\stats\stats-hhsj-controller.php
echo COPY src\webapp\views\admin\edition\hhsj\views\stats\stats-hhsj.html.php target\webapp\views\admin\edition\hhsj\views\stats\stats-hhsj.html.php
COPY src\webapp\views\admin\edition\hhsj\views\stats\stats-hhsj.html.php target\webapp\views\admin\edition\hhsj\views\stats\stats-hhsj.html.php
echo MKDIR target\webapp\views\admin\edition\hhsj\views\table
MKDIR target\webapp\views\admin\edition\hhsj\views\table
echo COPY src\webapp\views\admin\edition\hhsj\views\table\table-hhsj-controller.php target\webapp\views\admin\edition\hhsj\views\table\table-hhsj-controller.php
COPY src\webapp\views\admin\edition\hhsj\views\table\table-hhsj-controller.php target\webapp\views\admin\edition\hhsj\views\table\table-hhsj-controller.php
echo COPY src\webapp\views\admin\edition\hhsj\views\table\table-hhsj.html.php target\webapp\views\admin\edition\hhsj\views\table\table-hhsj.html.php
COPY src\webapp\views\admin\edition\hhsj\views\table\table-hhsj.html.php target\webapp\views\admin\edition\hhsj\views\table\table-hhsj.html.php
echo MKDIR target\webapp\views\admin\edition\home
MKDIR target\webapp\views\admin\edition\home
echo COPY src\webapp\views\admin\edition\home\admin-edition-home-config.json target\webapp\views\admin\edition\home\admin-edition-home-config.json
COPY src\webapp\views\admin\edition\home\admin-edition-home-config.json target\webapp\views\admin\edition\home\admin-edition-home-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\edition\home\admin-edition-home-controller.js -o target\webapp\views\admin\edition\home\admin-edition-home-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\edition\home\admin-edition-home-controller.js -o target\webapp\views\admin\edition\home\admin-edition-home-controller.js -c -m
echo COPY src\webapp\views\admin\edition\home\admin-edition-home-controller.php target\webapp\views\admin\edition\home\admin-edition-home-controller.php
COPY src\webapp\views\admin\edition\home\admin-edition-home-controller.php target\webapp\views\admin\edition\home\admin-edition-home-controller.php
echo COPY src\webapp\views\admin\edition\home\admin-edition-home.html.php target\webapp\views\admin\edition\home\admin-edition-home.html.php
COPY src\webapp\views\admin\edition\home\admin-edition-home.html.php target\webapp\views\admin\edition\home\admin-edition-home.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\admin\edition\home\admin-edition-home.less target\webapp\views\admin\edition\home\admin-edition-home.css
node ..\node_modules\less\bin\lessc src\webapp\views\admin\edition\home\admin-edition-home.less target\webapp\views\admin\edition\home\admin-edition-home.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\edition\home\admin-edition-home.css > target\webapp\views\admin\edition\home\admin-edition-home.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\edition\home\admin-edition-home.css > target\webapp\views\admin\edition\home\admin-edition-home.css.prod
echo DEL target\webapp\views\admin\edition\home\admin-edition-home.css
DEL target\webapp\views\admin\edition\home\admin-edition-home.css
echo MOVE target\webapp\views\admin\edition\home\admin-edition-home.css.prod target\webapp\views\admin\edition\home\admin-edition-home.css
MOVE target\webapp\views\admin\edition\home\admin-edition-home.css.prod target\webapp\views\admin\edition\home\admin-edition-home.css
echo MKDIR target\webapp\views\admin\edition\home\modals
MKDIR target\webapp\views\admin\edition\home\modals
echo MKDIR target\webapp\views\admin\edition\home\modals\save-home-message
MKDIR target\webapp\views\admin\edition\home\modals\save-home-message
echo COPY src\webapp\views\admin\edition\home\modals\save-home-message\modal-save-home-message-content.html.php target\webapp\views\admin\edition\home\modals\save-home-message\modal-save-home-message-content.html.php
COPY src\webapp\views\admin\edition\home\modals\save-home-message\modal-save-home-message-content.html.php target\webapp\views\admin\edition\home\modals\save-home-message\modal-save-home-message-content.html.php
echo COPY src\webapp\views\admin\edition\home\modals\save-home-message\modal-save-home-message-controller.php target\webapp\views\admin\edition\home\modals\save-home-message\modal-save-home-message-controller.php
COPY src\webapp\views\admin\edition\home\modals\save-home-message\modal-save-home-message-controller.php target\webapp\views\admin\edition\home\modals\save-home-message\modal-save-home-message-controller.php
echo COPY src\webapp\views\admin\edition\home\modals\save-home-message\modal-save-home-message.html.php target\webapp\views\admin\edition\home\modals\save-home-message\modal-save-home-message.html.php
COPY src\webapp\views\admin\edition\home\modals\save-home-message\modal-save-home-message.html.php target\webapp\views\admin\edition\home\modals\save-home-message\modal-save-home-message.html.php
echo MKDIR target\webapp\views\admin\edition\home\views
MKDIR target\webapp\views\admin\edition\home\views
echo MKDIR target\webapp\views\admin\edition\home\views\table-home-messages
MKDIR target\webapp\views\admin\edition\home\views\table-home-messages
echo COPY src\webapp\views\admin\edition\home\views\table-home-messages\table-home-messages-controller.php target\webapp\views\admin\edition\home\views\table-home-messages\table-home-messages-controller.php
COPY src\webapp\views\admin\edition\home\views\table-home-messages\table-home-messages-controller.php target\webapp\views\admin\edition\home\views\table-home-messages\table-home-messages-controller.php
echo COPY src\webapp\views\admin\edition\home\views\table-home-messages\table-home-messages.html.php target\webapp\views\admin\edition\home\views\table-home-messages\table-home-messages.html.php
COPY src\webapp\views\admin\edition\home\views\table-home-messages\table-home-messages.html.php target\webapp\views\admin\edition\home\views\table-home-messages\table-home-messages.html.php
echo MKDIR target\webapp\views\admin\edition\news
MKDIR target\webapp\views\admin\edition\news
echo COPY src\webapp\views\admin\edition\news\admin-edition-news-config.json target\webapp\views\admin\edition\news\admin-edition-news-config.json
COPY src\webapp\views\admin\edition\news\admin-edition-news-config.json target\webapp\views\admin\edition\news\admin-edition-news-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\edition\news\admin-edition-news-controller.js -o target\webapp\views\admin\edition\news\admin-edition-news-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\edition\news\admin-edition-news-controller.js -o target\webapp\views\admin\edition\news\admin-edition-news-controller.js -c -m
echo COPY src\webapp\views\admin\edition\news\admin-edition-news-controller.php target\webapp\views\admin\edition\news\admin-edition-news-controller.php
COPY src\webapp\views\admin\edition\news\admin-edition-news-controller.php target\webapp\views\admin\edition\news\admin-edition-news-controller.php
echo COPY src\webapp\views\admin\edition\news\admin-edition-news.html.php target\webapp\views\admin\edition\news\admin-edition-news.html.php
COPY src\webapp\views\admin\edition\news\admin-edition-news.html.php target\webapp\views\admin\edition\news\admin-edition-news.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\admin\edition\news\admin-edition-news.less target\webapp\views\admin\edition\news\admin-edition-news.css
node ..\node_modules\less\bin\lessc src\webapp\views\admin\edition\news\admin-edition-news.less target\webapp\views\admin\edition\news\admin-edition-news.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\edition\news\admin-edition-news.css > target\webapp\views\admin\edition\news\admin-edition-news.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\edition\news\admin-edition-news.css > target\webapp\views\admin\edition\news\admin-edition-news.css.prod
echo DEL target\webapp\views\admin\edition\news\admin-edition-news.css
DEL target\webapp\views\admin\edition\news\admin-edition-news.css
echo MOVE target\webapp\views\admin\edition\news\admin-edition-news.css.prod target\webapp\views\admin\edition\news\admin-edition-news.css
MOVE target\webapp\views\admin\edition\news\admin-edition-news.css.prod target\webapp\views\admin\edition\news\admin-edition-news.css
echo MKDIR target\webapp\views\admin\edition\news\modals
MKDIR target\webapp\views\admin\edition\news\modals
echo MKDIR target\webapp\views\admin\edition\news\modals\save
MKDIR target\webapp\views\admin\edition\news\modals\save
echo COPY src\webapp\views\admin\edition\news\modals\save\modal-save-actualite-content.html.php target\webapp\views\admin\edition\news\modals\save\modal-save-actualite-content.html.php
COPY src\webapp\views\admin\edition\news\modals\save\modal-save-actualite-content.html.php target\webapp\views\admin\edition\news\modals\save\modal-save-actualite-content.html.php
echo COPY src\webapp\views\admin\edition\news\modals\save\modal-save-actualite-controller.php target\webapp\views\admin\edition\news\modals\save\modal-save-actualite-controller.php
COPY src\webapp\views\admin\edition\news\modals\save\modal-save-actualite-controller.php target\webapp\views\admin\edition\news\modals\save\modal-save-actualite-controller.php
echo COPY src\webapp\views\admin\edition\news\modals\save\modal-save-actualite.html.php target\webapp\views\admin\edition\news\modals\save\modal-save-actualite.html.php
COPY src\webapp\views\admin\edition\news\modals\save\modal-save-actualite.html.php target\webapp\views\admin\edition\news\modals\save\modal-save-actualite.html.php
echo MKDIR target\webapp\views\admin\edition\news\views
MKDIR target\webapp\views\admin\edition\news\views
echo MKDIR target\webapp\views\admin\edition\news\views\recherche
MKDIR target\webapp\views\admin\edition\news\views\recherche
echo COPY src\webapp\views\admin\edition\news\views\recherche\recherche-news-controller.php target\webapp\views\admin\edition\news\views\recherche\recherche-news-controller.php
COPY src\webapp\views\admin\edition\news\views\recherche\recherche-news-controller.php target\webapp\views\admin\edition\news\views\recherche\recherche-news-controller.php
echo COPY src\webapp\views\admin\edition\news\views\recherche\recherche-news.html.php target\webapp\views\admin\edition\news\views\recherche\recherche-news.html.php
COPY src\webapp\views\admin\edition\news\views\recherche\recherche-news.html.php target\webapp\views\admin\edition\news\views\recherche\recherche-news.html.php
echo MKDIR target\webapp\views\admin\edition\news\views\stats
MKDIR target\webapp\views\admin\edition\news\views\stats
echo COPY src\webapp\views\admin\edition\news\views\stats\stats-news-controller.php target\webapp\views\admin\edition\news\views\stats\stats-news-controller.php
COPY src\webapp\views\admin\edition\news\views\stats\stats-news-controller.php target\webapp\views\admin\edition\news\views\stats\stats-news-controller.php
echo COPY src\webapp\views\admin\edition\news\views\stats\stats-news.html.php target\webapp\views\admin\edition\news\views\stats\stats-news.html.php
COPY src\webapp\views\admin\edition\news\views\stats\stats-news.html.php target\webapp\views\admin\edition\news\views\stats\stats-news.html.php
echo MKDIR target\webapp\views\admin\edition\news\views\table
MKDIR target\webapp\views\admin\edition\news\views\table
echo COPY src\webapp\views\admin\edition\news\views\table\table-news-controller.php target\webapp\views\admin\edition\news\views\table\table-news-controller.php
COPY src\webapp\views\admin\edition\news\views\table\table-news-controller.php target\webapp\views\admin\edition\news\views\table\table-news-controller.php
echo COPY src\webapp\views\admin\edition\news\views\table\table-news.html.php target\webapp\views\admin\edition\news\views\table\table-news.html.php
COPY src\webapp\views\admin\edition\news\views\table\table-news.html.php target\webapp\views\admin\edition\news\views\table\table-news.html.php
echo MKDIR target\webapp\views\admin\edition\photos-videos
MKDIR target\webapp\views\admin\edition\photos-videos
echo COPY src\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos-config.json target\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos-config.json
COPY src\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos-config.json target\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos-controller.js -o target\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos-controller.js -o target\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos-controller.js -c -m
echo COPY src\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos-controller.php target\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos-controller.php
COPY src\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos-controller.php target\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos-controller.php
echo COPY src\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos.html.php target\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos.html.php
COPY src\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos.html.php target\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos.less target\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos.css
node ..\node_modules\less\bin\lessc src\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos.less target\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos.css > target\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos.css > target\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos.css.prod
echo DEL target\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos.css
DEL target\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos.css
echo MOVE target\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos.css.prod target\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos.css
MOVE target\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos.css.prod target\webapp\views\admin\edition\photos-videos\admin-edition-photos-videos.css
echo MKDIR target\webapp\views\admin\edition\photos-videos\modals
MKDIR target\webapp\views\admin\edition\photos-videos\modals
echo MKDIR target\webapp\views\admin\edition\photos-videos\modals\save-album-photo
MKDIR target\webapp\views\admin\edition\photos-videos\modals\save-album-photo
echo COPY src\webapp\views\admin\edition\photos-videos\modals\save-album-photo\modal-save-album-photo-content.html.php target\webapp\views\admin\edition\photos-videos\modals\save-album-photo\modal-save-album-photo-content.html.php
COPY src\webapp\views\admin\edition\photos-videos\modals\save-album-photo\modal-save-album-photo-content.html.php target\webapp\views\admin\edition\photos-videos\modals\save-album-photo\modal-save-album-photo-content.html.php
echo COPY src\webapp\views\admin\edition\photos-videos\modals\save-album-photo\modal-save-album-photo-controller.php target\webapp\views\admin\edition\photos-videos\modals\save-album-photo\modal-save-album-photo-controller.php
COPY src\webapp\views\admin\edition\photos-videos\modals\save-album-photo\modal-save-album-photo-controller.php target\webapp\views\admin\edition\photos-videos\modals\save-album-photo\modal-save-album-photo-controller.php
echo COPY src\webapp\views\admin\edition\photos-videos\modals\save-album-photo\modal-save-album-photo.html.php target\webapp\views\admin\edition\photos-videos\modals\save-album-photo\modal-save-album-photo.html.php
COPY src\webapp\views\admin\edition\photos-videos\modals\save-album-photo\modal-save-album-photo.html.php target\webapp\views\admin\edition\photos-videos\modals\save-album-photo\modal-save-album-photo.html.php
echo MKDIR target\webapp\views\admin\edition\photos-videos\modals\save-video
MKDIR target\webapp\views\admin\edition\photos-videos\modals\save-video
echo COPY src\webapp\views\admin\edition\photos-videos\modals\save-video\modal-save-video-content.html.php target\webapp\views\admin\edition\photos-videos\modals\save-video\modal-save-video-content.html.php
COPY src\webapp\views\admin\edition\photos-videos\modals\save-video\modal-save-video-content.html.php target\webapp\views\admin\edition\photos-videos\modals\save-video\modal-save-video-content.html.php
echo COPY src\webapp\views\admin\edition\photos-videos\modals\save-video\modal-save-video-controller.php target\webapp\views\admin\edition\photos-videos\modals\save-video\modal-save-video-controller.php
COPY src\webapp\views\admin\edition\photos-videos\modals\save-video\modal-save-video-controller.php target\webapp\views\admin\edition\photos-videos\modals\save-video\modal-save-video-controller.php
echo COPY src\webapp\views\admin\edition\photos-videos\modals\save-video\modal-save-video.html.php target\webapp\views\admin\edition\photos-videos\modals\save-video\modal-save-video.html.php
COPY src\webapp\views\admin\edition\photos-videos\modals\save-video\modal-save-video.html.php target\webapp\views\admin\edition\photos-videos\modals\save-video\modal-save-video.html.php
echo MKDIR target\webapp\views\admin\edition\photos-videos\views
MKDIR target\webapp\views\admin\edition\photos-videos\views
echo MKDIR target\webapp\views\admin\edition\photos-videos\views\table-albums-photos
MKDIR target\webapp\views\admin\edition\photos-videos\views\table-albums-photos
echo COPY src\webapp\views\admin\edition\photos-videos\views\table-albums-photos\table-albums-photos-controller.php target\webapp\views\admin\edition\photos-videos\views\table-albums-photos\table-albums-photos-controller.php
COPY src\webapp\views\admin\edition\photos-videos\views\table-albums-photos\table-albums-photos-controller.php target\webapp\views\admin\edition\photos-videos\views\table-albums-photos\table-albums-photos-controller.php
echo COPY src\webapp\views\admin\edition\photos-videos\views\table-albums-photos\table-albums-photos.html.php target\webapp\views\admin\edition\photos-videos\views\table-albums-photos\table-albums-photos.html.php
COPY src\webapp\views\admin\edition\photos-videos\views\table-albums-photos\table-albums-photos.html.php target\webapp\views\admin\edition\photos-videos\views\table-albums-photos\table-albums-photos.html.php
echo MKDIR target\webapp\views\admin\edition\photos-videos\views\table-videos
MKDIR target\webapp\views\admin\edition\photos-videos\views\table-videos
echo COPY src\webapp\views\admin\edition\photos-videos\views\table-videos\table-videos-controller.php target\webapp\views\admin\edition\photos-videos\views\table-videos\table-videos-controller.php
COPY src\webapp\views\admin\edition\photos-videos\views\table-videos\table-videos-controller.php target\webapp\views\admin\edition\photos-videos\views\table-videos\table-videos-controller.php
echo COPY src\webapp\views\admin\edition\photos-videos\views\table-videos\table-videos.html.php target\webapp\views\admin\edition\photos-videos\views\table-videos\table-videos.html.php
COPY src\webapp\views\admin\edition\photos-videos\views\table-videos\table-videos.html.php target\webapp\views\admin\edition\photos-videos\views\table-videos\table-videos.html.php
echo MKDIR target\webapp\views\admin\edition\presse
MKDIR target\webapp\views\admin\edition\presse
echo COPY src\webapp\views\admin\edition\presse\admin-edition-presse-config.json target\webapp\views\admin\edition\presse\admin-edition-presse-config.json
COPY src\webapp\views\admin\edition\presse\admin-edition-presse-config.json target\webapp\views\admin\edition\presse\admin-edition-presse-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\edition\presse\admin-edition-presse-controller.js -o target\webapp\views\admin\edition\presse\admin-edition-presse-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\edition\presse\admin-edition-presse-controller.js -o target\webapp\views\admin\edition\presse\admin-edition-presse-controller.js -c -m
echo COPY src\webapp\views\admin\edition\presse\admin-edition-presse-controller.php target\webapp\views\admin\edition\presse\admin-edition-presse-controller.php
COPY src\webapp\views\admin\edition\presse\admin-edition-presse-controller.php target\webapp\views\admin\edition\presse\admin-edition-presse-controller.php
echo COPY src\webapp\views\admin\edition\presse\admin-edition-presse.html.php target\webapp\views\admin\edition\presse\admin-edition-presse.html.php
COPY src\webapp\views\admin\edition\presse\admin-edition-presse.html.php target\webapp\views\admin\edition\presse\admin-edition-presse.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\admin\edition\presse\admin-edition-presse.less target\webapp\views\admin\edition\presse\admin-edition-presse.css
node ..\node_modules\less\bin\lessc src\webapp\views\admin\edition\presse\admin-edition-presse.less target\webapp\views\admin\edition\presse\admin-edition-presse.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\edition\presse\admin-edition-presse.css > target\webapp\views\admin\edition\presse\admin-edition-presse.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\edition\presse\admin-edition-presse.css > target\webapp\views\admin\edition\presse\admin-edition-presse.css.prod
echo DEL target\webapp\views\admin\edition\presse\admin-edition-presse.css
DEL target\webapp\views\admin\edition\presse\admin-edition-presse.css
echo MOVE target\webapp\views\admin\edition\presse\admin-edition-presse.css.prod target\webapp\views\admin\edition\presse\admin-edition-presse.css
MOVE target\webapp\views\admin\edition\presse\admin-edition-presse.css.prod target\webapp\views\admin\edition\presse\admin-edition-presse.css
echo MKDIR target\webapp\views\admin\edition\presse\modals
MKDIR target\webapp\views\admin\edition\presse\modals
echo MKDIR target\webapp\views\admin\edition\presse\modals\save
MKDIR target\webapp\views\admin\edition\presse\modals\save
echo COPY src\webapp\views\admin\edition\presse\modals\save\modal-save-article-presse-content.html.php target\webapp\views\admin\edition\presse\modals\save\modal-save-article-presse-content.html.php
COPY src\webapp\views\admin\edition\presse\modals\save\modal-save-article-presse-content.html.php target\webapp\views\admin\edition\presse\modals\save\modal-save-article-presse-content.html.php
echo COPY src\webapp\views\admin\edition\presse\modals\save\modal-save-article-presse-controller.php target\webapp\views\admin\edition\presse\modals\save\modal-save-article-presse-controller.php
COPY src\webapp\views\admin\edition\presse\modals\save\modal-save-article-presse-controller.php target\webapp\views\admin\edition\presse\modals\save\modal-save-article-presse-controller.php
echo COPY src\webapp\views\admin\edition\presse\modals\save\modal-save-article-presse.html.php target\webapp\views\admin\edition\presse\modals\save\modal-save-article-presse.html.php
COPY src\webapp\views\admin\edition\presse\modals\save\modal-save-article-presse.html.php target\webapp\views\admin\edition\presse\modals\save\modal-save-article-presse.html.php
echo MKDIR target\webapp\views\admin\edition\presse\views
MKDIR target\webapp\views\admin\edition\presse\views
echo MKDIR target\webapp\views\admin\edition\presse\views\recherche
MKDIR target\webapp\views\admin\edition\presse\views\recherche
echo COPY src\webapp\views\admin\edition\presse\views\recherche\recherche-articles-presse-controller.php target\webapp\views\admin\edition\presse\views\recherche\recherche-articles-presse-controller.php
COPY src\webapp\views\admin\edition\presse\views\recherche\recherche-articles-presse-controller.php target\webapp\views\admin\edition\presse\views\recherche\recherche-articles-presse-controller.php
echo COPY src\webapp\views\admin\edition\presse\views\recherche\recherche-articles-presse.html.php target\webapp\views\admin\edition\presse\views\recherche\recherche-articles-presse.html.php
COPY src\webapp\views\admin\edition\presse\views\recherche\recherche-articles-presse.html.php target\webapp\views\admin\edition\presse\views\recherche\recherche-articles-presse.html.php
echo MKDIR target\webapp\views\admin\edition\presse\views\stats
MKDIR target\webapp\views\admin\edition\presse\views\stats
echo COPY src\webapp\views\admin\edition\presse\views\stats\stats-articles-presse-controller.php target\webapp\views\admin\edition\presse\views\stats\stats-articles-presse-controller.php
COPY src\webapp\views\admin\edition\presse\views\stats\stats-articles-presse-controller.php target\webapp\views\admin\edition\presse\views\stats\stats-articles-presse-controller.php
echo COPY src\webapp\views\admin\edition\presse\views\stats\stats-articles-presse.html.php target\webapp\views\admin\edition\presse\views\stats\stats-articles-presse.html.php
COPY src\webapp\views\admin\edition\presse\views\stats\stats-articles-presse.html.php target\webapp\views\admin\edition\presse\views\stats\stats-articles-presse.html.php
echo MKDIR target\webapp\views\admin\edition\presse\views\table
MKDIR target\webapp\views\admin\edition\presse\views\table
echo COPY src\webapp\views\admin\edition\presse\views\table\table-articles-presse-controller.php target\webapp\views\admin\edition\presse\views\table\table-articles-presse-controller.php
COPY src\webapp\views\admin\edition\presse\views\table\table-articles-presse-controller.php target\webapp\views\admin\edition\presse\views\table\table-articles-presse-controller.php
echo COPY src\webapp\views\admin\edition\presse\views\table\table-articles-presse.html.php target\webapp\views\admin\edition\presse\views\table\table-articles-presse.html.php
COPY src\webapp\views\admin\edition\presse\views\table\table-articles-presse.html.php target\webapp\views\admin\edition\presse\views\table\table-articles-presse.html.php
echo MKDIR target\webapp\views\admin\edition\sub-menu
MKDIR target\webapp\views\admin\edition\sub-menu
echo COPY src\webapp\views\admin\edition\sub-menu\sub-menu-admin-edition.html.php target\webapp\views\admin\edition\sub-menu\sub-menu-admin-edition.html.php
COPY src\webapp\views\admin\edition\sub-menu\sub-menu-admin-edition.html.php target\webapp\views\admin\edition\sub-menu\sub-menu-admin-edition.html.php
echo MKDIR target\webapp\views\admin\evenements
MKDIR target\webapp\views\admin\evenements
echo COPY src\webapp\views\admin\evenements\admin-evenements.html.php target\webapp\views\admin\evenements\admin-evenements.html.php
COPY src\webapp\views\admin\evenements\admin-evenements.html.php target\webapp\views\admin\evenements\admin-evenements.html.php
echo MKDIR target\webapp\views\admin\evenements\competitions
MKDIR target\webapp\views\admin\evenements\competitions
echo COPY src\webapp\views\admin\evenements\competitions\admin-evenements-competitions-config.json target\webapp\views\admin\evenements\competitions\admin-evenements-competitions-config.json
COPY src\webapp\views\admin\evenements\competitions\admin-evenements-competitions-config.json target\webapp\views\admin\evenements\competitions\admin-evenements-competitions-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\evenements\competitions\admin-evenements-competitions-controller.js -o target\webapp\views\admin\evenements\competitions\admin-evenements-competitions-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\evenements\competitions\admin-evenements-competitions-controller.js -o target\webapp\views\admin\evenements\competitions\admin-evenements-competitions-controller.js -c -m
echo COPY src\webapp\views\admin\evenements\competitions\admin-evenements-competitions-controller.php target\webapp\views\admin\evenements\competitions\admin-evenements-competitions-controller.php
COPY src\webapp\views\admin\evenements\competitions\admin-evenements-competitions-controller.php target\webapp\views\admin\evenements\competitions\admin-evenements-competitions-controller.php
echo COPY src\webapp\views\admin\evenements\competitions\admin-evenements-competitions.html.php target\webapp\views\admin\evenements\competitions\admin-evenements-competitions.html.php
COPY src\webapp\views\admin\evenements\competitions\admin-evenements-competitions.html.php target\webapp\views\admin\evenements\competitions\admin-evenements-competitions.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\admin\evenements\competitions\admin-evenements-competitions.less target\webapp\views\admin\evenements\competitions\admin-evenements-competitions.css
node ..\node_modules\less\bin\lessc src\webapp\views\admin\evenements\competitions\admin-evenements-competitions.less target\webapp\views\admin\evenements\competitions\admin-evenements-competitions.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\evenements\competitions\admin-evenements-competitions.css > target\webapp\views\admin\evenements\competitions\admin-evenements-competitions.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\evenements\competitions\admin-evenements-competitions.css > target\webapp\views\admin\evenements\competitions\admin-evenements-competitions.css.prod
echo DEL target\webapp\views\admin\evenements\competitions\admin-evenements-competitions.css
DEL target\webapp\views\admin\evenements\competitions\admin-evenements-competitions.css
echo MOVE target\webapp\views\admin\evenements\competitions\admin-evenements-competitions.css.prod target\webapp\views\admin\evenements\competitions\admin-evenements-competitions.css
MOVE target\webapp\views\admin\evenements\competitions\admin-evenements-competitions.css.prod target\webapp\views\admin\evenements\competitions\admin-evenements-competitions.css
echo MKDIR target\webapp\views\admin\evenements\competitions\modals
MKDIR target\webapp\views\admin\evenements\competitions\modals
echo MKDIR target\webapp\views\admin\evenements\competitions\modals\save
MKDIR target\webapp\views\admin\evenements\competitions\modals\save
echo COPY src\webapp\views\admin\evenements\competitions\modals\save\modal-save-competition-content.html.php target\webapp\views\admin\evenements\competitions\modals\save\modal-save-competition-content.html.php
COPY src\webapp\views\admin\evenements\competitions\modals\save\modal-save-competition-content.html.php target\webapp\views\admin\evenements\competitions\modals\save\modal-save-competition-content.html.php
echo COPY src\webapp\views\admin\evenements\competitions\modals\save\modal-save-competition-controller.php target\webapp\views\admin\evenements\competitions\modals\save\modal-save-competition-controller.php
COPY src\webapp\views\admin\evenements\competitions\modals\save\modal-save-competition-controller.php target\webapp\views\admin\evenements\competitions\modals\save\modal-save-competition-controller.php
echo COPY src\webapp\views\admin\evenements\competitions\modals\save\modal-save-competition.html.php target\webapp\views\admin\evenements\competitions\modals\save\modal-save-competition.html.php
COPY src\webapp\views\admin\evenements\competitions\modals\save\modal-save-competition.html.php target\webapp\views\admin\evenements\competitions\modals\save\modal-save-competition.html.php
echo MKDIR target\webapp\views\admin\evenements\competitions\views
MKDIR target\webapp\views\admin\evenements\competitions\views
echo MKDIR target\webapp\views\admin\evenements\competitions\views\recherche
MKDIR target\webapp\views\admin\evenements\competitions\views\recherche
echo COPY src\webapp\views\admin\evenements\competitions\views\recherche\recherche-competitions-controller.php target\webapp\views\admin\evenements\competitions\views\recherche\recherche-competitions-controller.php
COPY src\webapp\views\admin\evenements\competitions\views\recherche\recherche-competitions-controller.php target\webapp\views\admin\evenements\competitions\views\recherche\recherche-competitions-controller.php
echo COPY src\webapp\views\admin\evenements\competitions\views\recherche\recherche-competitions.html.php target\webapp\views\admin\evenements\competitions\views\recherche\recherche-competitions.html.php
COPY src\webapp\views\admin\evenements\competitions\views\recherche\recherche-competitions.html.php target\webapp\views\admin\evenements\competitions\views\recherche\recherche-competitions.html.php
echo MKDIR target\webapp\views\admin\evenements\competitions\views\stats
MKDIR target\webapp\views\admin\evenements\competitions\views\stats
echo COPY src\webapp\views\admin\evenements\competitions\views\stats\stats-competitions-controller.php target\webapp\views\admin\evenements\competitions\views\stats\stats-competitions-controller.php
COPY src\webapp\views\admin\evenements\competitions\views\stats\stats-competitions-controller.php target\webapp\views\admin\evenements\competitions\views\stats\stats-competitions-controller.php
echo COPY src\webapp\views\admin\evenements\competitions\views\stats\stats-competitions.html.php target\webapp\views\admin\evenements\competitions\views\stats\stats-competitions.html.php
COPY src\webapp\views\admin\evenements\competitions\views\stats\stats-competitions.html.php target\webapp\views\admin\evenements\competitions\views\stats\stats-competitions.html.php
echo MKDIR target\webapp\views\admin\evenements\competitions\views\table
MKDIR target\webapp\views\admin\evenements\competitions\views\table
echo COPY src\webapp\views\admin\evenements\competitions\views\table\table-competitions-controller.php target\webapp\views\admin\evenements\competitions\views\table\table-competitions-controller.php
COPY src\webapp\views\admin\evenements\competitions\views\table\table-competitions-controller.php target\webapp\views\admin\evenements\competitions\views\table\table-competitions-controller.php
echo COPY src\webapp\views\admin\evenements\competitions\views\table\table-competitions.html.php target\webapp\views\admin\evenements\competitions\views\table\table-competitions.html.php
COPY src\webapp\views\admin\evenements\competitions\views\table\table-competitions.html.php target\webapp\views\admin\evenements\competitions\views\table\table-competitions.html.php
echo MKDIR target\webapp\views\admin\evenements\evenements
MKDIR target\webapp\views\admin\evenements\evenements
echo COPY src\webapp\views\admin\evenements\evenements\admin-evenements-evenements-config.json target\webapp\views\admin\evenements\evenements\admin-evenements-evenements-config.json
COPY src\webapp\views\admin\evenements\evenements\admin-evenements-evenements-config.json target\webapp\views\admin\evenements\evenements\admin-evenements-evenements-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\evenements\evenements\admin-evenements-evenements-controller.js -o target\webapp\views\admin\evenements\evenements\admin-evenements-evenements-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\evenements\evenements\admin-evenements-evenements-controller.js -o target\webapp\views\admin\evenements\evenements\admin-evenements-evenements-controller.js -c -m
echo COPY src\webapp\views\admin\evenements\evenements\admin-evenements-evenements-controller.php target\webapp\views\admin\evenements\evenements\admin-evenements-evenements-controller.php
COPY src\webapp\views\admin\evenements\evenements\admin-evenements-evenements-controller.php target\webapp\views\admin\evenements\evenements\admin-evenements-evenements-controller.php
echo COPY src\webapp\views\admin\evenements\evenements\admin-evenements-evenements.html.php target\webapp\views\admin\evenements\evenements\admin-evenements-evenements.html.php
COPY src\webapp\views\admin\evenements\evenements\admin-evenements-evenements.html.php target\webapp\views\admin\evenements\evenements\admin-evenements-evenements.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\admin\evenements\evenements\admin-evenements-evenements.less target\webapp\views\admin\evenements\evenements\admin-evenements-evenements.css
node ..\node_modules\less\bin\lessc src\webapp\views\admin\evenements\evenements\admin-evenements-evenements.less target\webapp\views\admin\evenements\evenements\admin-evenements-evenements.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\evenements\evenements\admin-evenements-evenements.css > target\webapp\views\admin\evenements\evenements\admin-evenements-evenements.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\evenements\evenements\admin-evenements-evenements.css > target\webapp\views\admin\evenements\evenements\admin-evenements-evenements.css.prod
echo DEL target\webapp\views\admin\evenements\evenements\admin-evenements-evenements.css
DEL target\webapp\views\admin\evenements\evenements\admin-evenements-evenements.css
echo MOVE target\webapp\views\admin\evenements\evenements\admin-evenements-evenements.css.prod target\webapp\views\admin\evenements\evenements\admin-evenements-evenements.css
MOVE target\webapp\views\admin\evenements\evenements\admin-evenements-evenements.css.prod target\webapp\views\admin\evenements\evenements\admin-evenements-evenements.css
echo MKDIR target\webapp\views\admin\evenements\evenements\modals
MKDIR target\webapp\views\admin\evenements\evenements\modals
echo MKDIR target\webapp\views\admin\evenements\evenements\modals\actions-club
MKDIR target\webapp\views\admin\evenements\evenements\modals\actions-club
echo COPY src\webapp\views\admin\evenements\evenements\modals\actions-club\modal-actions-club-evenement-content.html.php target\webapp\views\admin\evenements\evenements\modals\actions-club\modal-actions-club-evenement-content.html.php
COPY src\webapp\views\admin\evenements\evenements\modals\actions-club\modal-actions-club-evenement-content.html.php target\webapp\views\admin\evenements\evenements\modals\actions-club\modal-actions-club-evenement-content.html.php
echo COPY src\webapp\views\admin\evenements\evenements\modals\actions-club\modal-actions-club-evenement-controller.php target\webapp\views\admin\evenements\evenements\modals\actions-club\modal-actions-club-evenement-controller.php
COPY src\webapp\views\admin\evenements\evenements\modals\actions-club\modal-actions-club-evenement-controller.php target\webapp\views\admin\evenements\evenements\modals\actions-club\modal-actions-club-evenement-controller.php
echo COPY src\webapp\views\admin\evenements\evenements\modals\actions-club\modal-actions-club-evenement.html.php target\webapp\views\admin\evenements\evenements\modals\actions-club\modal-actions-club-evenement.html.php
COPY src\webapp\views\admin\evenements\evenements\modals\actions-club\modal-actions-club-evenement.html.php target\webapp\views\admin\evenements\evenements\modals\actions-club\modal-actions-club-evenement.html.php
echo MKDIR target\webapp\views\admin\evenements\evenements\modals\arbitrages
MKDIR target\webapp\views\admin\evenements\evenements\modals\arbitrages
echo COPY src\webapp\views\admin\evenements\evenements\modals\arbitrages\modal-arbitrages-evenement-content.html.php target\webapp\views\admin\evenements\evenements\modals\arbitrages\modal-arbitrages-evenement-content.html.php
COPY src\webapp\views\admin\evenements\evenements\modals\arbitrages\modal-arbitrages-evenement-content.html.php target\webapp\views\admin\evenements\evenements\modals\arbitrages\modal-arbitrages-evenement-content.html.php
echo COPY src\webapp\views\admin\evenements\evenements\modals\arbitrages\modal-arbitrages-evenement-controller.php target\webapp\views\admin\evenements\evenements\modals\arbitrages\modal-arbitrages-evenement-controller.php
COPY src\webapp\views\admin\evenements\evenements\modals\arbitrages\modal-arbitrages-evenement-controller.php target\webapp\views\admin\evenements\evenements\modals\arbitrages\modal-arbitrages-evenement-controller.php
echo COPY src\webapp\views\admin\evenements\evenements\modals\arbitrages\modal-arbitrages-evenement.html.php target\webapp\views\admin\evenements\evenements\modals\arbitrages\modal-arbitrages-evenement.html.php
COPY src\webapp\views\admin\evenements\evenements\modals\arbitrages\modal-arbitrages-evenement.html.php target\webapp\views\admin\evenements\evenements\modals\arbitrages\modal-arbitrages-evenement.html.php
echo MKDIR target\webapp\views\admin\evenements\evenements\modals\fdm-match
MKDIR target\webapp\views\admin\evenements\evenements\modals\fdm-match
echo COPY src\webapp\views\admin\evenements\evenements\modals\fdm-match\modal-fdm-match-content.html.php target\webapp\views\admin\evenements\evenements\modals\fdm-match\modal-fdm-match-content.html.php
COPY src\webapp\views\admin\evenements\evenements\modals\fdm-match\modal-fdm-match-content.html.php target\webapp\views\admin\evenements\evenements\modals\fdm-match\modal-fdm-match-content.html.php
echo COPY src\webapp\views\admin\evenements\evenements\modals\fdm-match\modal-fdm-match-controller.php target\webapp\views\admin\evenements\evenements\modals\fdm-match\modal-fdm-match-controller.php
COPY src\webapp\views\admin\evenements\evenements\modals\fdm-match\modal-fdm-match-controller.php target\webapp\views\admin\evenements\evenements\modals\fdm-match\modal-fdm-match-controller.php
echo COPY src\webapp\views\admin\evenements\evenements\modals\fdm-match\modal-fdm-match.html.php target\webapp\views\admin\evenements\evenements\modals\fdm-match\modal-fdm-match.html.php
COPY src\webapp\views\admin\evenements\evenements\modals\fdm-match\modal-fdm-match.html.php target\webapp\views\admin\evenements\evenements\modals\fdm-match\modal-fdm-match.html.php
echo MKDIR target\webapp\views\admin\evenements\evenements\modals\historique-dispos
MKDIR target\webapp\views\admin\evenements\evenements\modals\historique-dispos
echo COPY src\webapp\views\admin\evenements\evenements\modals\historique-dispos\modal-historique-dispos-evenement-content.html.php target\webapp\views\admin\evenements\evenements\modals\historique-dispos\modal-historique-dispos-evenement-content.html.php
COPY src\webapp\views\admin\evenements\evenements\modals\historique-dispos\modal-historique-dispos-evenement-content.html.php target\webapp\views\admin\evenements\evenements\modals\historique-dispos\modal-historique-dispos-evenement-content.html.php
echo COPY src\webapp\views\admin\evenements\evenements\modals\historique-dispos\modal-historique-dispos-evenement-controller.php target\webapp\views\admin\evenements\evenements\modals\historique-dispos\modal-historique-dispos-evenement-controller.php
COPY src\webapp\views\admin\evenements\evenements\modals\historique-dispos\modal-historique-dispos-evenement-controller.php target\webapp\views\admin\evenements\evenements\modals\historique-dispos\modal-historique-dispos-evenement-controller.php
echo COPY src\webapp\views\admin\evenements\evenements\modals\historique-dispos\modal-historique-dispos-evenement.html.php target\webapp\views\admin\evenements\evenements\modals\historique-dispos\modal-historique-dispos-evenement.html.php
COPY src\webapp\views\admin\evenements\evenements\modals\historique-dispos\modal-historique-dispos-evenement.html.php target\webapp\views\admin\evenements\evenements\modals\historique-dispos\modal-historique-dispos-evenement.html.php
echo MKDIR target\webapp\views\admin\evenements\evenements\modals\save
MKDIR target\webapp\views\admin\evenements\evenements\modals\save
echo COPY src\webapp\views\admin\evenements\evenements\modals\save\modal-save-evenement-content.html.php target\webapp\views\admin\evenements\evenements\modals\save\modal-save-evenement-content.html.php
COPY src\webapp\views\admin\evenements\evenements\modals\save\modal-save-evenement-content.html.php target\webapp\views\admin\evenements\evenements\modals\save\modal-save-evenement-content.html.php
echo COPY src\webapp\views\admin\evenements\evenements\modals\save\modal-save-evenement-controller.php target\webapp\views\admin\evenements\evenements\modals\save\modal-save-evenement-controller.php
COPY src\webapp\views\admin\evenements\evenements\modals\save\modal-save-evenement-controller.php target\webapp\views\admin\evenements\evenements\modals\save\modal-save-evenement-controller.php
echo COPY src\webapp\views\admin\evenements\evenements\modals\save\modal-save-evenement.html.php target\webapp\views\admin\evenements\evenements\modals\save\modal-save-evenement.html.php
COPY src\webapp\views\admin\evenements\evenements\modals\save\modal-save-evenement.html.php target\webapp\views\admin\evenements\evenements\modals\save\modal-save-evenement.html.php
echo MKDIR target\webapp\views\admin\evenements\evenements\views
MKDIR target\webapp\views\admin\evenements\evenements\views
echo MKDIR target\webapp\views\admin\evenements\evenements\views\recherche
MKDIR target\webapp\views\admin\evenements\evenements\views\recherche
echo COPY src\webapp\views\admin\evenements\evenements\views\recherche\recherche-evenements-controller.php target\webapp\views\admin\evenements\evenements\views\recherche\recherche-evenements-controller.php
COPY src\webapp\views\admin\evenements\evenements\views\recherche\recherche-evenements-controller.php target\webapp\views\admin\evenements\evenements\views\recherche\recherche-evenements-controller.php
echo COPY src\webapp\views\admin\evenements\evenements\views\recherche\recherche-evenements.html.php target\webapp\views\admin\evenements\evenements\views\recherche\recherche-evenements.html.php
COPY src\webapp\views\admin\evenements\evenements\views\recherche\recherche-evenements.html.php target\webapp\views\admin\evenements\evenements\views\recherche\recherche-evenements.html.php
echo MKDIR target\webapp\views\admin\evenements\evenements\views\stats
MKDIR target\webapp\views\admin\evenements\evenements\views\stats
echo COPY src\webapp\views\admin\evenements\evenements\views\stats\stats-evenements-controller.php target\webapp\views\admin\evenements\evenements\views\stats\stats-evenements-controller.php
COPY src\webapp\views\admin\evenements\evenements\views\stats\stats-evenements-controller.php target\webapp\views\admin\evenements\evenements\views\stats\stats-evenements-controller.php
echo COPY src\webapp\views\admin\evenements\evenements\views\stats\stats-evenements.html.php target\webapp\views\admin\evenements\evenements\views\stats\stats-evenements.html.php
COPY src\webapp\views\admin\evenements\evenements\views\stats\stats-evenements.html.php target\webapp\views\admin\evenements\evenements\views\stats\stats-evenements.html.php
echo MKDIR target\webapp\views\admin\evenements\evenements\views\table
MKDIR target\webapp\views\admin\evenements\evenements\views\table
echo COPY src\webapp\views\admin\evenements\evenements\views\table\table-evenements-controller.php target\webapp\views\admin\evenements\evenements\views\table\table-evenements-controller.php
COPY src\webapp\views\admin\evenements\evenements\views\table\table-evenements-controller.php target\webapp\views\admin\evenements\evenements\views\table\table-evenements-controller.php
echo COPY src\webapp\views\admin\evenements\evenements\views\table\table-evenements.html.php target\webapp\views\admin\evenements\evenements\views\table\table-evenements.html.php
COPY src\webapp\views\admin\evenements\evenements\views\table\table-evenements.html.php target\webapp\views\admin\evenements\evenements\views\table\table-evenements.html.php
echo MKDIR target\webapp\views\admin\evenements\gymnases
MKDIR target\webapp\views\admin\evenements\gymnases
echo COPY src\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases-config.json target\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases-config.json
COPY src\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases-config.json target\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases-controller.js -o target\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases-controller.js -o target\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases-controller.js -c -m
echo COPY src\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases-controller.php target\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases-controller.php
COPY src\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases-controller.php target\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases-controller.php
echo COPY src\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases.html.php target\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases.html.php
COPY src\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases.html.php target\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases.less target\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases.css
node ..\node_modules\less\bin\lessc src\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases.less target\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases.css > target\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases.css > target\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases.css.prod
echo DEL target\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases.css
DEL target\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases.css
echo MOVE target\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases.css.prod target\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases.css
MOVE target\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases.css.prod target\webapp\views\admin\evenements\gymnases\admin-evenements-gymnases.css
echo MKDIR target\webapp\views\admin\evenements\gymnases\modals
MKDIR target\webapp\views\admin\evenements\gymnases\modals
echo MKDIR target\webapp\views\admin\evenements\gymnases\modals\save
MKDIR target\webapp\views\admin\evenements\gymnases\modals\save
echo COPY src\webapp\views\admin\evenements\gymnases\modals\save\modal-save-gymnase-content.html.php target\webapp\views\admin\evenements\gymnases\modals\save\modal-save-gymnase-content.html.php
COPY src\webapp\views\admin\evenements\gymnases\modals\save\modal-save-gymnase-content.html.php target\webapp\views\admin\evenements\gymnases\modals\save\modal-save-gymnase-content.html.php
echo COPY src\webapp\views\admin\evenements\gymnases\modals\save\modal-save-gymnase-controller.php target\webapp\views\admin\evenements\gymnases\modals\save\modal-save-gymnase-controller.php
COPY src\webapp\views\admin\evenements\gymnases\modals\save\modal-save-gymnase-controller.php target\webapp\views\admin\evenements\gymnases\modals\save\modal-save-gymnase-controller.php
echo COPY src\webapp\views\admin\evenements\gymnases\modals\save\modal-save-gymnase.html.php target\webapp\views\admin\evenements\gymnases\modals\save\modal-save-gymnase.html.php
COPY src\webapp\views\admin\evenements\gymnases\modals\save\modal-save-gymnase.html.php target\webapp\views\admin\evenements\gymnases\modals\save\modal-save-gymnase.html.php
echo MKDIR target\webapp\views\admin\evenements\gymnases\views
MKDIR target\webapp\views\admin\evenements\gymnases\views
echo MKDIR target\webapp\views\admin\evenements\gymnases\views\stats
MKDIR target\webapp\views\admin\evenements\gymnases\views\stats
echo COPY src\webapp\views\admin\evenements\gymnases\views\stats\stats-gymnases-controller.php target\webapp\views\admin\evenements\gymnases\views\stats\stats-gymnases-controller.php
COPY src\webapp\views\admin\evenements\gymnases\views\stats\stats-gymnases-controller.php target\webapp\views\admin\evenements\gymnases\views\stats\stats-gymnases-controller.php
echo COPY src\webapp\views\admin\evenements\gymnases\views\stats\stats-gymnases.html.php target\webapp\views\admin\evenements\gymnases\views\stats\stats-gymnases.html.php
COPY src\webapp\views\admin\evenements\gymnases\views\stats\stats-gymnases.html.php target\webapp\views\admin\evenements\gymnases\views\stats\stats-gymnases.html.php
echo MKDIR target\webapp\views\admin\evenements\gymnases\views\table
MKDIR target\webapp\views\admin\evenements\gymnases\views\table
echo COPY src\webapp\views\admin\evenements\gymnases\views\table\table-gymnases-controller.php target\webapp\views\admin\evenements\gymnases\views\table\table-gymnases-controller.php
COPY src\webapp\views\admin\evenements\gymnases\views\table\table-gymnases-controller.php target\webapp\views\admin\evenements\gymnases\views\table\table-gymnases-controller.php
echo COPY src\webapp\views\admin\evenements\gymnases\views\table\table-gymnases.html.php target\webapp\views\admin\evenements\gymnases\views\table\table-gymnases.html.php
COPY src\webapp\views\admin\evenements\gymnases\views\table\table-gymnases.html.php target\webapp\views\admin\evenements\gymnases\views\table\table-gymnases.html.php
echo MKDIR target\webapp\views\admin\evenements\sub-menu
MKDIR target\webapp\views\admin\evenements\sub-menu
echo COPY src\webapp\views\admin\evenements\sub-menu\sub-menu-admin-evenements.html.php target\webapp\views\admin\evenements\sub-menu\sub-menu-admin-evenements.html.php
COPY src\webapp\views\admin\evenements\sub-menu\sub-menu-admin-evenements.html.php target\webapp\views\admin\evenements\sub-menu\sub-menu-admin-evenements.html.php
echo MKDIR target\webapp\views\admin\shop
MKDIR target\webapp\views\admin\shop
echo COPY src\webapp\views\admin\shop\admin-shop.html.php target\webapp\views\admin\shop\admin-shop.html.php
COPY src\webapp\views\admin\shop\admin-shop.html.php target\webapp\views\admin\shop\admin-shop.html.php
echo MKDIR target\webapp\views\admin\shop\billetterie
MKDIR target\webapp\views\admin\shop\billetterie
echo COPY src\webapp\views\admin\shop\billetterie\admin-shop-billetterie.html.php target\webapp\views\admin\shop\billetterie\admin-shop-billetterie.html.php
COPY src\webapp\views\admin\shop\billetterie\admin-shop-billetterie.html.php target\webapp\views\admin\shop\billetterie\admin-shop-billetterie.html.php
echo MKDIR target\webapp\views\admin\shop\billetterie\commandes
MKDIR target\webapp\views\admin\shop\billetterie\commandes
echo COPY src\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes-config.json target\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes-config.json
COPY src\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes-config.json target\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes-controller.js -o target\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes-controller.js -o target\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes-controller.js -c -m
echo COPY src\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes-controller.php target\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes-controller.php
COPY src\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes-controller.php target\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes-controller.php
echo COPY src\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes.html.php target\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes.html.php
COPY src\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes.html.php target\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes.less target\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes.css
node ..\node_modules\less\bin\lessc src\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes.less target\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes.css > target\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes.css > target\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes.css.prod
echo DEL target\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes.css
DEL target\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes.css
echo MOVE target\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes.css.prod target\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes.css
MOVE target\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes.css.prod target\webapp\views\admin\shop\billetterie\commandes\admin-shop-billetterie-commandes.css
echo MKDIR target\webapp\views\admin\shop\billetterie\commandes\modals
MKDIR target\webapp\views\admin\shop\billetterie\commandes\modals
echo MKDIR target\webapp\views\admin\shop\billetterie\commandes\modals\link-pack
MKDIR target\webapp\views\admin\shop\billetterie\commandes\modals\link-pack
echo COPY src\webapp\views\admin\shop\billetterie\commandes\modals\link-pack\modal-link-pack-commande-billetterie-content.html.php target\webapp\views\admin\shop\billetterie\commandes\modals\link-pack\modal-link-pack-commande-billetterie-content.html.php
COPY src\webapp\views\admin\shop\billetterie\commandes\modals\link-pack\modal-link-pack-commande-billetterie-content.html.php target\webapp\views\admin\shop\billetterie\commandes\modals\link-pack\modal-link-pack-commande-billetterie-content.html.php
echo COPY src\webapp\views\admin\shop\billetterie\commandes\modals\link-pack\modal-link-pack-commande-billetterie-controller.php target\webapp\views\admin\shop\billetterie\commandes\modals\link-pack\modal-link-pack-commande-billetterie-controller.php
COPY src\webapp\views\admin\shop\billetterie\commandes\modals\link-pack\modal-link-pack-commande-billetterie-controller.php target\webapp\views\admin\shop\billetterie\commandes\modals\link-pack\modal-link-pack-commande-billetterie-controller.php
echo COPY src\webapp\views\admin\shop\billetterie\commandes\modals\link-pack\modal-link-pack-commande-billetterie.html.php target\webapp\views\admin\shop\billetterie\commandes\modals\link-pack\modal-link-pack-commande-billetterie.html.php
COPY src\webapp\views\admin\shop\billetterie\commandes\modals\link-pack\modal-link-pack-commande-billetterie.html.php target\webapp\views\admin\shop\billetterie\commandes\modals\link-pack\modal-link-pack-commande-billetterie.html.php
echo MKDIR target\webapp\views\admin\shop\billetterie\commandes\views
MKDIR target\webapp\views\admin\shop\billetterie\commandes\views
echo MKDIR target\webapp\views\admin\shop\billetterie\commandes\views\recherche
MKDIR target\webapp\views\admin\shop\billetterie\commandes\views\recherche
echo COPY src\webapp\views\admin\shop\billetterie\commandes\views\recherche\recherche-commandes-controller.php target\webapp\views\admin\shop\billetterie\commandes\views\recherche\recherche-commandes-controller.php
COPY src\webapp\views\admin\shop\billetterie\commandes\views\recherche\recherche-commandes-controller.php target\webapp\views\admin\shop\billetterie\commandes\views\recherche\recherche-commandes-controller.php
echo COPY src\webapp\views\admin\shop\billetterie\commandes\views\recherche\recherche-commandes.html.php target\webapp\views\admin\shop\billetterie\commandes\views\recherche\recherche-commandes.html.php
COPY src\webapp\views\admin\shop\billetterie\commandes\views\recherche\recherche-commandes.html.php target\webapp\views\admin\shop\billetterie\commandes\views\recherche\recherche-commandes.html.php
echo MKDIR target\webapp\views\admin\shop\billetterie\commandes\views\stats
MKDIR target\webapp\views\admin\shop\billetterie\commandes\views\stats
echo COPY src\webapp\views\admin\shop\billetterie\commandes\views\stats\stats-commandes-controller.php target\webapp\views\admin\shop\billetterie\commandes\views\stats\stats-commandes-controller.php
COPY src\webapp\views\admin\shop\billetterie\commandes\views\stats\stats-commandes-controller.php target\webapp\views\admin\shop\billetterie\commandes\views\stats\stats-commandes-controller.php
echo COPY src\webapp\views\admin\shop\billetterie\commandes\views\stats\stats-commandes.html.php target\webapp\views\admin\shop\billetterie\commandes\views\stats\stats-commandes.html.php
COPY src\webapp\views\admin\shop\billetterie\commandes\views\stats\stats-commandes.html.php target\webapp\views\admin\shop\billetterie\commandes\views\stats\stats-commandes.html.php
echo MKDIR target\webapp\views\admin\shop\billetterie\commandes\views\table
MKDIR target\webapp\views\admin\shop\billetterie\commandes\views\table
echo COPY src\webapp\views\admin\shop\billetterie\commandes\views\table\table-commandes-controller.php target\webapp\views\admin\shop\billetterie\commandes\views\table\table-commandes-controller.php
COPY src\webapp\views\admin\shop\billetterie\commandes\views\table\table-commandes-controller.php target\webapp\views\admin\shop\billetterie\commandes\views\table\table-commandes-controller.php
echo COPY src\webapp\views\admin\shop\billetterie\commandes\views\table\table-commandes.html.php target\webapp\views\admin\shop\billetterie\commandes\views\table\table-commandes.html.php
COPY src\webapp\views\admin\shop\billetterie\commandes\views\table\table-commandes.html.php target\webapp\views\admin\shop\billetterie\commandes\views\table\table-commandes.html.php
echo MKDIR target\webapp\views\admin\shop\billetterie\header
MKDIR target\webapp\views\admin\shop\billetterie\header
echo COPY src\webapp\views\admin\shop\billetterie\header\header-admin-shop-billetterie.html.php target\webapp\views\admin\shop\billetterie\header\header-admin-shop-billetterie.html.php
COPY src\webapp\views\admin\shop\billetterie\header\header-admin-shop-billetterie.html.php target\webapp\views\admin\shop\billetterie\header\header-admin-shop-billetterie.html.php
echo MKDIR target\webapp\views\admin\shop\billetterie\matchs
MKDIR target\webapp\views\admin\shop\billetterie\matchs
echo COPY src\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs-config.json target\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs-config.json
COPY src\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs-config.json target\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs-controller.js -o target\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs-controller.js -o target\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs-controller.js -c -m
echo COPY src\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs-controller.php target\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs-controller.php
COPY src\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs-controller.php target\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs-controller.php
echo COPY src\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs.html.php target\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs.html.php
COPY src\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs.html.php target\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs.less target\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs.css
node ..\node_modules\less\bin\lessc src\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs.less target\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs.css > target\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs.css > target\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs.css.prod
echo DEL target\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs.css
DEL target\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs.css
echo MOVE target\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs.css.prod target\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs.css
MOVE target\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs.css.prod target\webapp\views\admin\shop\billetterie\matchs\admin-shop-billetterie-matchs.css
echo MKDIR target\webapp\views\admin\shop\billetterie\matchs\modals
MKDIR target\webapp\views\admin\shop\billetterie\matchs\modals
echo MKDIR target\webapp\views\admin\shop\billetterie\matchs\modals\save
MKDIR target\webapp\views\admin\shop\billetterie\matchs\modals\save
echo COPY src\webapp\views\admin\shop\billetterie\matchs\modals\save\modal-save-match-billetterie-content.html.php target\webapp\views\admin\shop\billetterie\matchs\modals\save\modal-save-match-billetterie-content.html.php
COPY src\webapp\views\admin\shop\billetterie\matchs\modals\save\modal-save-match-billetterie-content.html.php target\webapp\views\admin\shop\billetterie\matchs\modals\save\modal-save-match-billetterie-content.html.php
echo COPY src\webapp\views\admin\shop\billetterie\matchs\modals\save\modal-save-match-billetterie-controller.php target\webapp\views\admin\shop\billetterie\matchs\modals\save\modal-save-match-billetterie-controller.php
COPY src\webapp\views\admin\shop\billetterie\matchs\modals\save\modal-save-match-billetterie-controller.php target\webapp\views\admin\shop\billetterie\matchs\modals\save\modal-save-match-billetterie-controller.php
echo COPY src\webapp\views\admin\shop\billetterie\matchs\modals\save\modal-save-match-billetterie.html.php target\webapp\views\admin\shop\billetterie\matchs\modals\save\modal-save-match-billetterie.html.php
COPY src\webapp\views\admin\shop\billetterie\matchs\modals\save\modal-save-match-billetterie.html.php target\webapp\views\admin\shop\billetterie\matchs\modals\save\modal-save-match-billetterie.html.php
echo MKDIR target\webapp\views\admin\shop\billetterie\matchs\views
MKDIR target\webapp\views\admin\shop\billetterie\matchs\views
echo MKDIR target\webapp\views\admin\shop\billetterie\matchs\views\recherche
MKDIR target\webapp\views\admin\shop\billetterie\matchs\views\recherche
echo COPY src\webapp\views\admin\shop\billetterie\matchs\views\recherche\recherche-matchs-controller.php target\webapp\views\admin\shop\billetterie\matchs\views\recherche\recherche-matchs-controller.php
COPY src\webapp\views\admin\shop\billetterie\matchs\views\recherche\recherche-matchs-controller.php target\webapp\views\admin\shop\billetterie\matchs\views\recherche\recherche-matchs-controller.php
echo COPY src\webapp\views\admin\shop\billetterie\matchs\views\recherche\recherche-matchs.html.php target\webapp\views\admin\shop\billetterie\matchs\views\recherche\recherche-matchs.html.php
COPY src\webapp\views\admin\shop\billetterie\matchs\views\recherche\recherche-matchs.html.php target\webapp\views\admin\shop\billetterie\matchs\views\recherche\recherche-matchs.html.php
echo MKDIR target\webapp\views\admin\shop\billetterie\matchs\views\table
MKDIR target\webapp\views\admin\shop\billetterie\matchs\views\table
echo COPY src\webapp\views\admin\shop\billetterie\matchs\views\table\table-matchs-controller.php target\webapp\views\admin\shop\billetterie\matchs\views\table\table-matchs-controller.php
COPY src\webapp\views\admin\shop\billetterie\matchs\views\table\table-matchs-controller.php target\webapp\views\admin\shop\billetterie\matchs\views\table\table-matchs-controller.php
echo COPY src\webapp\views\admin\shop\billetterie\matchs\views\table\table-matchs.html.php target\webapp\views\admin\shop\billetterie\matchs\views\table\table-matchs.html.php
COPY src\webapp\views\admin\shop\billetterie\matchs\views\table\table-matchs.html.php target\webapp\views\admin\shop\billetterie\matchs\views\table\table-matchs.html.php
echo MKDIR target\webapp\views\admin\shop\billetterie\packs
MKDIR target\webapp\views\admin\shop\billetterie\packs
echo COPY src\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs-config.json target\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs-config.json
COPY src\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs-config.json target\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs-controller.js -o target\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs-controller.js -o target\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs-controller.js -c -m
echo COPY src\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs-controller.php target\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs-controller.php
COPY src\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs-controller.php target\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs-controller.php
echo COPY src\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs.html.php target\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs.html.php
COPY src\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs.html.php target\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs.less target\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs.css
node ..\node_modules\less\bin\lessc src\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs.less target\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs.css > target\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs.css > target\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs.css.prod
echo DEL target\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs.css
DEL target\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs.css
echo MOVE target\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs.css.prod target\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs.css
MOVE target\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs.css.prod target\webapp\views\admin\shop\billetterie\packs\admin-shop-billetterie-packs.css
echo MKDIR target\webapp\views\admin\shop\billetterie\packs\modals
MKDIR target\webapp\views\admin\shop\billetterie\packs\modals
echo MKDIR target\webapp\views\admin\shop\billetterie\packs\modals\save
MKDIR target\webapp\views\admin\shop\billetterie\packs\modals\save
echo COPY src\webapp\views\admin\shop\billetterie\packs\modals\save\modal-save-pack-billetterie-content.html.php target\webapp\views\admin\shop\billetterie\packs\modals\save\modal-save-pack-billetterie-content.html.php
COPY src\webapp\views\admin\shop\billetterie\packs\modals\save\modal-save-pack-billetterie-content.html.php target\webapp\views\admin\shop\billetterie\packs\modals\save\modal-save-pack-billetterie-content.html.php
echo COPY src\webapp\views\admin\shop\billetterie\packs\modals\save\modal-save-pack-billetterie-controller.php target\webapp\views\admin\shop\billetterie\packs\modals\save\modal-save-pack-billetterie-controller.php
COPY src\webapp\views\admin\shop\billetterie\packs\modals\save\modal-save-pack-billetterie-controller.php target\webapp\views\admin\shop\billetterie\packs\modals\save\modal-save-pack-billetterie-controller.php
echo COPY src\webapp\views\admin\shop\billetterie\packs\modals\save\modal-save-pack-billetterie.html.php target\webapp\views\admin\shop\billetterie\packs\modals\save\modal-save-pack-billetterie.html.php
COPY src\webapp\views\admin\shop\billetterie\packs\modals\save\modal-save-pack-billetterie.html.php target\webapp\views\admin\shop\billetterie\packs\modals\save\modal-save-pack-billetterie.html.php
echo MKDIR target\webapp\views\admin\shop\billetterie\packs\views
MKDIR target\webapp\views\admin\shop\billetterie\packs\views
echo MKDIR target\webapp\views\admin\shop\billetterie\packs\views\table
MKDIR target\webapp\views\admin\shop\billetterie\packs\views\table
echo COPY src\webapp\views\admin\shop\billetterie\packs\views\table\table-packs-controller.php target\webapp\views\admin\shop\billetterie\packs\views\table\table-packs-controller.php
COPY src\webapp\views\admin\shop\billetterie\packs\views\table\table-packs-controller.php target\webapp\views\admin\shop\billetterie\packs\views\table\table-packs-controller.php
echo COPY src\webapp\views\admin\shop\billetterie\packs\views\table\table-packs.html.php target\webapp\views\admin\shop\billetterie\packs\views\table\table-packs.html.php
COPY src\webapp\views\admin\shop\billetterie\packs\views\table\table-packs.html.php target\webapp\views\admin\shop\billetterie\packs\views\table\table-packs.html.php
echo MKDIR target\webapp\views\admin\shop\boutique
MKDIR target\webapp\views\admin\shop\boutique
echo COPY src\webapp\views\admin\shop\boutique\admin-shop-boutique.html.php target\webapp\views\admin\shop\boutique\admin-shop-boutique.html.php
COPY src\webapp\views\admin\shop\boutique\admin-shop-boutique.html.php target\webapp\views\admin\shop\boutique\admin-shop-boutique.html.php
echo MKDIR target\webapp\views\admin\shop\boutique\commandes
MKDIR target\webapp\views\admin\shop\boutique\commandes
echo COPY src\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes-config.json target\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes-config.json
COPY src\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes-config.json target\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes-controller.js -o target\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes-controller.js -o target\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes-controller.js -c -m
echo COPY src\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes-controller.php target\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes-controller.php
COPY src\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes-controller.php target\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes-controller.php
echo COPY src\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes.html.php target\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes.html.php
COPY src\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes.html.php target\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes.less target\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes.css
node ..\node_modules\less\bin\lessc src\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes.less target\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes.css > target\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes.css > target\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes.css.prod
echo DEL target\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes.css
DEL target\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes.css
echo MOVE target\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes.css.prod target\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes.css
MOVE target\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes.css.prod target\webapp\views\admin\shop\boutique\commandes\admin-shop-boutique-commandes.css
echo MKDIR target\webapp\views\admin\shop\boutique\commandes\modals
MKDIR target\webapp\views\admin\shop\boutique\commandes\modals
echo MKDIR target\webapp\views\admin\shop\boutique\commandes\modals\link-commande-fournisseur
MKDIR target\webapp\views\admin\shop\boutique\commandes\modals\link-commande-fournisseur
echo COPY src\webapp\views\admin\shop\boutique\commandes\modals\link-commande-fournisseur\modal-link-commande-fournisseur-commande-boutique-content.html.php target\webapp\views\admin\shop\boutique\commandes\modals\link-commande-fournisseur\modal-link-commande-fournisseur-commande-boutique-content.html.php
COPY src\webapp\views\admin\shop\boutique\commandes\modals\link-commande-fournisseur\modal-link-commande-fournisseur-commande-boutique-content.html.php target\webapp\views\admin\shop\boutique\commandes\modals\link-commande-fournisseur\modal-link-commande-fournisseur-commande-boutique-content.html.php
echo COPY src\webapp\views\admin\shop\boutique\commandes\modals\link-commande-fournisseur\modal-link-commande-fournisseur-commande-boutique-controller.php target\webapp\views\admin\shop\boutique\commandes\modals\link-commande-fournisseur\modal-link-commande-fournisseur-commande-boutique-controller.php
COPY src\webapp\views\admin\shop\boutique\commandes\modals\link-commande-fournisseur\modal-link-commande-fournisseur-commande-boutique-controller.php target\webapp\views\admin\shop\boutique\commandes\modals\link-commande-fournisseur\modal-link-commande-fournisseur-commande-boutique-controller.php
echo COPY src\webapp\views\admin\shop\boutique\commandes\modals\link-commande-fournisseur\modal-link-commande-fournisseur-commande-boutique.html.php target\webapp\views\admin\shop\boutique\commandes\modals\link-commande-fournisseur\modal-link-commande-fournisseur-commande-boutique.html.php
COPY src\webapp\views\admin\shop\boutique\commandes\modals\link-commande-fournisseur\modal-link-commande-fournisseur-commande-boutique.html.php target\webapp\views\admin\shop\boutique\commandes\modals\link-commande-fournisseur\modal-link-commande-fournisseur-commande-boutique.html.php
echo MKDIR target\webapp\views\admin\shop\boutique\commandes\modals\link-livraison-fournisseur
MKDIR target\webapp\views\admin\shop\boutique\commandes\modals\link-livraison-fournisseur
echo COPY src\webapp\views\admin\shop\boutique\commandes\modals\link-livraison-fournisseur\modal-link-livraison-fournisseur-commande-boutique-content.html.php target\webapp\views\admin\shop\boutique\commandes\modals\link-livraison-fournisseur\modal-link-livraison-fournisseur-commande-boutique-content.html.php
COPY src\webapp\views\admin\shop\boutique\commandes\modals\link-livraison-fournisseur\modal-link-livraison-fournisseur-commande-boutique-content.html.php target\webapp\views\admin\shop\boutique\commandes\modals\link-livraison-fournisseur\modal-link-livraison-fournisseur-commande-boutique-content.html.php
echo COPY src\webapp\views\admin\shop\boutique\commandes\modals\link-livraison-fournisseur\modal-link-livraison-fournisseur-commande-boutique-controller.php target\webapp\views\admin\shop\boutique\commandes\modals\link-livraison-fournisseur\modal-link-livraison-fournisseur-commande-boutique-controller.php
COPY src\webapp\views\admin\shop\boutique\commandes\modals\link-livraison-fournisseur\modal-link-livraison-fournisseur-commande-boutique-controller.php target\webapp\views\admin\shop\boutique\commandes\modals\link-livraison-fournisseur\modal-link-livraison-fournisseur-commande-boutique-controller.php
echo COPY src\webapp\views\admin\shop\boutique\commandes\modals\link-livraison-fournisseur\modal-link-livraison-fournisseur-commande-boutique.html.php target\webapp\views\admin\shop\boutique\commandes\modals\link-livraison-fournisseur\modal-link-livraison-fournisseur-commande-boutique.html.php
COPY src\webapp\views\admin\shop\boutique\commandes\modals\link-livraison-fournisseur\modal-link-livraison-fournisseur-commande-boutique.html.php target\webapp\views\admin\shop\boutique\commandes\modals\link-livraison-fournisseur\modal-link-livraison-fournisseur-commande-boutique.html.php
echo MKDIR target\webapp\views\admin\shop\boutique\commandes\modals\link-source
MKDIR target\webapp\views\admin\shop\boutique\commandes\modals\link-source
echo COPY src\webapp\views\admin\shop\boutique\commandes\modals\link-source\modal-link-source-commande-boutique-content.html.php target\webapp\views\admin\shop\boutique\commandes\modals\link-source\modal-link-source-commande-boutique-content.html.php
COPY src\webapp\views\admin\shop\boutique\commandes\modals\link-source\modal-link-source-commande-boutique-content.html.php target\webapp\views\admin\shop\boutique\commandes\modals\link-source\modal-link-source-commande-boutique-content.html.php
echo COPY src\webapp\views\admin\shop\boutique\commandes\modals\link-source\modal-link-source-commande-boutique-controller.php target\webapp\views\admin\shop\boutique\commandes\modals\link-source\modal-link-source-commande-boutique-controller.php
COPY src\webapp\views\admin\shop\boutique\commandes\modals\link-source\modal-link-source-commande-boutique-controller.php target\webapp\views\admin\shop\boutique\commandes\modals\link-source\modal-link-source-commande-boutique-controller.php
echo COPY src\webapp\views\admin\shop\boutique\commandes\modals\link-source\modal-link-source-commande-boutique.html.php target\webapp\views\admin\shop\boutique\commandes\modals\link-source\modal-link-source-commande-boutique.html.php
COPY src\webapp\views\admin\shop\boutique\commandes\modals\link-source\modal-link-source-commande-boutique.html.php target\webapp\views\admin\shop\boutique\commandes\modals\link-source\modal-link-source-commande-boutique.html.php
echo MKDIR target\webapp\views\admin\shop\boutique\commandes\views
MKDIR target\webapp\views\admin\shop\boutique\commandes\views
echo MKDIR target\webapp\views\admin\shop\boutique\commandes\views\recherche
MKDIR target\webapp\views\admin\shop\boutique\commandes\views\recherche
echo COPY src\webapp\views\admin\shop\boutique\commandes\views\recherche\recherche-commandes-controller.php target\webapp\views\admin\shop\boutique\commandes\views\recherche\recherche-commandes-controller.php
COPY src\webapp\views\admin\shop\boutique\commandes\views\recherche\recherche-commandes-controller.php target\webapp\views\admin\shop\boutique\commandes\views\recherche\recherche-commandes-controller.php
echo COPY src\webapp\views\admin\shop\boutique\commandes\views\recherche\recherche-commandes.html.php target\webapp\views\admin\shop\boutique\commandes\views\recherche\recherche-commandes.html.php
COPY src\webapp\views\admin\shop\boutique\commandes\views\recherche\recherche-commandes.html.php target\webapp\views\admin\shop\boutique\commandes\views\recherche\recherche-commandes.html.php
echo MKDIR target\webapp\views\admin\shop\boutique\commandes\views\stats
MKDIR target\webapp\views\admin\shop\boutique\commandes\views\stats
echo COPY src\webapp\views\admin\shop\boutique\commandes\views\stats\stats-commandes-controller.php target\webapp\views\admin\shop\boutique\commandes\views\stats\stats-commandes-controller.php
COPY src\webapp\views\admin\shop\boutique\commandes\views\stats\stats-commandes-controller.php target\webapp\views\admin\shop\boutique\commandes\views\stats\stats-commandes-controller.php
echo COPY src\webapp\views\admin\shop\boutique\commandes\views\stats\stats-commandes.html.php target\webapp\views\admin\shop\boutique\commandes\views\stats\stats-commandes.html.php
COPY src\webapp\views\admin\shop\boutique\commandes\views\stats\stats-commandes.html.php target\webapp\views\admin\shop\boutique\commandes\views\stats\stats-commandes.html.php
echo MKDIR target\webapp\views\admin\shop\boutique\commandes\views\table
MKDIR target\webapp\views\admin\shop\boutique\commandes\views\table
echo COPY src\webapp\views\admin\shop\boutique\commandes\views\table\table-commandes-controller.php target\webapp\views\admin\shop\boutique\commandes\views\table\table-commandes-controller.php
COPY src\webapp\views\admin\shop\boutique\commandes\views\table\table-commandes-controller.php target\webapp\views\admin\shop\boutique\commandes\views\table\table-commandes-controller.php
echo COPY src\webapp\views\admin\shop\boutique\commandes\views\table\table-commandes.html.php target\webapp\views\admin\shop\boutique\commandes\views\table\table-commandes.html.php
COPY src\webapp\views\admin\shop\boutique\commandes\views\table\table-commandes.html.php target\webapp\views\admin\shop\boutique\commandes\views\table\table-commandes.html.php
echo MKDIR target\webapp\views\admin\shop\boutique\fournisseur
MKDIR target\webapp\views\admin\shop\boutique\fournisseur
echo COPY src\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur-config.json target\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur-config.json
COPY src\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur-config.json target\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur-controller.js -o target\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur-controller.js -o target\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur-controller.js -c -m
echo COPY src\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur-controller.php target\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur-controller.php
COPY src\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur-controller.php target\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur-controller.php
echo COPY src\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur.html.php target\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur.html.php
COPY src\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur.html.php target\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur.less target\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur.css
node ..\node_modules\less\bin\lessc src\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur.less target\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur.css > target\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur.css > target\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur.css.prod
echo DEL target\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur.css
DEL target\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur.css
echo MOVE target\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur.css.prod target\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur.css
MOVE target\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur.css.prod target\webapp\views\admin\shop\boutique\fournisseur\admin-shop-boutique-fournisseur.css
echo MKDIR target\webapp\views\admin\shop\boutique\fournisseur\modals
MKDIR target\webapp\views\admin\shop\boutique\fournisseur\modals
echo MKDIR target\webapp\views\admin\shop\boutique\fournisseur\modals\save-commande-fournisseur
MKDIR target\webapp\views\admin\shop\boutique\fournisseur\modals\save-commande-fournisseur
echo COPY src\webapp\views\admin\shop\boutique\fournisseur\modals\save-commande-fournisseur\modal-save-commande-fournisseur-content.html.php target\webapp\views\admin\shop\boutique\fournisseur\modals\save-commande-fournisseur\modal-save-commande-fournisseur-content.html.php
COPY src\webapp\views\admin\shop\boutique\fournisseur\modals\save-commande-fournisseur\modal-save-commande-fournisseur-content.html.php target\webapp\views\admin\shop\boutique\fournisseur\modals\save-commande-fournisseur\modal-save-commande-fournisseur-content.html.php
echo COPY src\webapp\views\admin\shop\boutique\fournisseur\modals\save-commande-fournisseur\modal-save-commande-fournisseur-controller.php target\webapp\views\admin\shop\boutique\fournisseur\modals\save-commande-fournisseur\modal-save-commande-fournisseur-controller.php
COPY src\webapp\views\admin\shop\boutique\fournisseur\modals\save-commande-fournisseur\modal-save-commande-fournisseur-controller.php target\webapp\views\admin\shop\boutique\fournisseur\modals\save-commande-fournisseur\modal-save-commande-fournisseur-controller.php
echo COPY src\webapp\views\admin\shop\boutique\fournisseur\modals\save-commande-fournisseur\modal-save-commande-fournisseur.html.php target\webapp\views\admin\shop\boutique\fournisseur\modals\save-commande-fournisseur\modal-save-commande-fournisseur.html.php
COPY src\webapp\views\admin\shop\boutique\fournisseur\modals\save-commande-fournisseur\modal-save-commande-fournisseur.html.php target\webapp\views\admin\shop\boutique\fournisseur\modals\save-commande-fournisseur\modal-save-commande-fournisseur.html.php
echo MKDIR target\webapp\views\admin\shop\boutique\fournisseur\modals\save-fournisseur
MKDIR target\webapp\views\admin\shop\boutique\fournisseur\modals\save-fournisseur
echo COPY src\webapp\views\admin\shop\boutique\fournisseur\modals\save-fournisseur\modal-save-fournisseur-content.html.php target\webapp\views\admin\shop\boutique\fournisseur\modals\save-fournisseur\modal-save-fournisseur-content.html.php
COPY src\webapp\views\admin\shop\boutique\fournisseur\modals\save-fournisseur\modal-save-fournisseur-content.html.php target\webapp\views\admin\shop\boutique\fournisseur\modals\save-fournisseur\modal-save-fournisseur-content.html.php
echo COPY src\webapp\views\admin\shop\boutique\fournisseur\modals\save-fournisseur\modal-save-fournisseur-controller.php target\webapp\views\admin\shop\boutique\fournisseur\modals\save-fournisseur\modal-save-fournisseur-controller.php
COPY src\webapp\views\admin\shop\boutique\fournisseur\modals\save-fournisseur\modal-save-fournisseur-controller.php target\webapp\views\admin\shop\boutique\fournisseur\modals\save-fournisseur\modal-save-fournisseur-controller.php
echo COPY src\webapp\views\admin\shop\boutique\fournisseur\modals\save-fournisseur\modal-save-fournisseur.html.php target\webapp\views\admin\shop\boutique\fournisseur\modals\save-fournisseur\modal-save-fournisseur.html.php
COPY src\webapp\views\admin\shop\boutique\fournisseur\modals\save-fournisseur\modal-save-fournisseur.html.php target\webapp\views\admin\shop\boutique\fournisseur\modals\save-fournisseur\modal-save-fournisseur.html.php
echo MKDIR target\webapp\views\admin\shop\boutique\fournisseur\modals\save-livraison-fournisseur
MKDIR target\webapp\views\admin\shop\boutique\fournisseur\modals\save-livraison-fournisseur
echo COPY src\webapp\views\admin\shop\boutique\fournisseur\modals\save-livraison-fournisseur\modal-save-livraison-fournisseur-content.html.php target\webapp\views\admin\shop\boutique\fournisseur\modals\save-livraison-fournisseur\modal-save-livraison-fournisseur-content.html.php
COPY src\webapp\views\admin\shop\boutique\fournisseur\modals\save-livraison-fournisseur\modal-save-livraison-fournisseur-content.html.php target\webapp\views\admin\shop\boutique\fournisseur\modals\save-livraison-fournisseur\modal-save-livraison-fournisseur-content.html.php
echo COPY src\webapp\views\admin\shop\boutique\fournisseur\modals\save-livraison-fournisseur\modal-save-livraison-fournisseur-controller.php target\webapp\views\admin\shop\boutique\fournisseur\modals\save-livraison-fournisseur\modal-save-livraison-fournisseur-controller.php
COPY src\webapp\views\admin\shop\boutique\fournisseur\modals\save-livraison-fournisseur\modal-save-livraison-fournisseur-controller.php target\webapp\views\admin\shop\boutique\fournisseur\modals\save-livraison-fournisseur\modal-save-livraison-fournisseur-controller.php
echo COPY src\webapp\views\admin\shop\boutique\fournisseur\modals\save-livraison-fournisseur\modal-save-livraison-fournisseur.html.php target\webapp\views\admin\shop\boutique\fournisseur\modals\save-livraison-fournisseur\modal-save-livraison-fournisseur.html.php
COPY src\webapp\views\admin\shop\boutique\fournisseur\modals\save-livraison-fournisseur\modal-save-livraison-fournisseur.html.php target\webapp\views\admin\shop\boutique\fournisseur\modals\save-livraison-fournisseur\modal-save-livraison-fournisseur.html.php
echo MKDIR target\webapp\views\admin\shop\boutique\fournisseur\views
MKDIR target\webapp\views\admin\shop\boutique\fournisseur\views
echo MKDIR target\webapp\views\admin\shop\boutique\fournisseur\views\table-commandes-fournisseur
MKDIR target\webapp\views\admin\shop\boutique\fournisseur\views\table-commandes-fournisseur
echo COPY src\webapp\views\admin\shop\boutique\fournisseur\views\table-commandes-fournisseur\table-commandes-fournisseur-controller.php target\webapp\views\admin\shop\boutique\fournisseur\views\table-commandes-fournisseur\table-commandes-fournisseur-controller.php
COPY src\webapp\views\admin\shop\boutique\fournisseur\views\table-commandes-fournisseur\table-commandes-fournisseur-controller.php target\webapp\views\admin\shop\boutique\fournisseur\views\table-commandes-fournisseur\table-commandes-fournisseur-controller.php
echo COPY src\webapp\views\admin\shop\boutique\fournisseur\views\table-commandes-fournisseur\table-commandes-fournisseur.html.php target\webapp\views\admin\shop\boutique\fournisseur\views\table-commandes-fournisseur\table-commandes-fournisseur.html.php
COPY src\webapp\views\admin\shop\boutique\fournisseur\views\table-commandes-fournisseur\table-commandes-fournisseur.html.php target\webapp\views\admin\shop\boutique\fournisseur\views\table-commandes-fournisseur\table-commandes-fournisseur.html.php
echo MKDIR target\webapp\views\admin\shop\boutique\fournisseur\views\table-fournisseurs
MKDIR target\webapp\views\admin\shop\boutique\fournisseur\views\table-fournisseurs
echo COPY src\webapp\views\admin\shop\boutique\fournisseur\views\table-fournisseurs\table-fournisseurs-controller.php target\webapp\views\admin\shop\boutique\fournisseur\views\table-fournisseurs\table-fournisseurs-controller.php
COPY src\webapp\views\admin\shop\boutique\fournisseur\views\table-fournisseurs\table-fournisseurs-controller.php target\webapp\views\admin\shop\boutique\fournisseur\views\table-fournisseurs\table-fournisseurs-controller.php
echo COPY src\webapp\views\admin\shop\boutique\fournisseur\views\table-fournisseurs\table-fournisseurs.html.php target\webapp\views\admin\shop\boutique\fournisseur\views\table-fournisseurs\table-fournisseurs.html.php
COPY src\webapp\views\admin\shop\boutique\fournisseur\views\table-fournisseurs\table-fournisseurs.html.php target\webapp\views\admin\shop\boutique\fournisseur\views\table-fournisseurs\table-fournisseurs.html.php
echo MKDIR target\webapp\views\admin\shop\boutique\fournisseur\views\table-livraisons-fournisseur
MKDIR target\webapp\views\admin\shop\boutique\fournisseur\views\table-livraisons-fournisseur
echo COPY src\webapp\views\admin\shop\boutique\fournisseur\views\table-livraisons-fournisseur\table-livraisons-fournisseur-controller.php target\webapp\views\admin\shop\boutique\fournisseur\views\table-livraisons-fournisseur\table-livraisons-fournisseur-controller.php
COPY src\webapp\views\admin\shop\boutique\fournisseur\views\table-livraisons-fournisseur\table-livraisons-fournisseur-controller.php target\webapp\views\admin\shop\boutique\fournisseur\views\table-livraisons-fournisseur\table-livraisons-fournisseur-controller.php
echo COPY src\webapp\views\admin\shop\boutique\fournisseur\views\table-livraisons-fournisseur\table-livraisons-fournisseur.html.php target\webapp\views\admin\shop\boutique\fournisseur\views\table-livraisons-fournisseur\table-livraisons-fournisseur.html.php
COPY src\webapp\views\admin\shop\boutique\fournisseur\views\table-livraisons-fournisseur\table-livraisons-fournisseur.html.php target\webapp\views\admin\shop\boutique\fournisseur\views\table-livraisons-fournisseur\table-livraisons-fournisseur.html.php
echo MKDIR target\webapp\views\admin\shop\boutique\header
MKDIR target\webapp\views\admin\shop\boutique\header
echo COPY src\webapp\views\admin\shop\boutique\header\header-admin-shop-boutique.html.php target\webapp\views\admin\shop\boutique\header\header-admin-shop-boutique.html.php
COPY src\webapp\views\admin\shop\boutique\header\header-admin-shop-boutique.html.php target\webapp\views\admin\shop\boutique\header\header-admin-shop-boutique.html.php
echo MKDIR target\webapp\views\admin\shop\boutique\produits
MKDIR target\webapp\views\admin\shop\boutique\produits
echo COPY src\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits-config.json target\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits-config.json
COPY src\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits-config.json target\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits-controller.js -o target\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits-controller.js -o target\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits-controller.js -c -m
echo COPY src\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits-controller.php target\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits-controller.php
COPY src\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits-controller.php target\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits-controller.php
echo COPY src\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits.html.php target\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits.html.php
COPY src\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits.html.php target\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits.less target\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits.css
node ..\node_modules\less\bin\lessc src\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits.less target\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits.css > target\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits.css > target\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits.css.prod
echo DEL target\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits.css
DEL target\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits.css
echo MOVE target\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits.css.prod target\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits.css
MOVE target\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits.css.prod target\webapp\views\admin\shop\boutique\produits\admin-shop-boutique-produits.css
echo MKDIR target\webapp\views\admin\shop\boutique\produits\modals
MKDIR target\webapp\views\admin\shop\boutique\produits\modals
echo MKDIR target\webapp\views\admin\shop\boutique\produits\modals\prix
MKDIR target\webapp\views\admin\shop\boutique\produits\modals\prix
echo COPY src\webapp\views\admin\shop\boutique\produits\modals\prix\modal-prix-produit-boutique-content.html.php target\webapp\views\admin\shop\boutique\produits\modals\prix\modal-prix-produit-boutique-content.html.php
COPY src\webapp\views\admin\shop\boutique\produits\modals\prix\modal-prix-produit-boutique-content.html.php target\webapp\views\admin\shop\boutique\produits\modals\prix\modal-prix-produit-boutique-content.html.php
echo COPY src\webapp\views\admin\shop\boutique\produits\modals\prix\modal-prix-produit-boutique-controller.php target\webapp\views\admin\shop\boutique\produits\modals\prix\modal-prix-produit-boutique-controller.php
COPY src\webapp\views\admin\shop\boutique\produits\modals\prix\modal-prix-produit-boutique-controller.php target\webapp\views\admin\shop\boutique\produits\modals\prix\modal-prix-produit-boutique-controller.php
echo COPY src\webapp\views\admin\shop\boutique\produits\modals\prix\modal-prix-produit-boutique.html.php target\webapp\views\admin\shop\boutique\produits\modals\prix\modal-prix-produit-boutique.html.php
COPY src\webapp\views\admin\shop\boutique\produits\modals\prix\modal-prix-produit-boutique.html.php target\webapp\views\admin\shop\boutique\produits\modals\prix\modal-prix-produit-boutique.html.php
echo MKDIR target\webapp\views\admin\shop\boutique\produits\modals\save
MKDIR target\webapp\views\admin\shop\boutique\produits\modals\save
echo COPY src\webapp\views\admin\shop\boutique\produits\modals\save\modal-save-produit-boutique-content.html.php target\webapp\views\admin\shop\boutique\produits\modals\save\modal-save-produit-boutique-content.html.php
COPY src\webapp\views\admin\shop\boutique\produits\modals\save\modal-save-produit-boutique-content.html.php target\webapp\views\admin\shop\boutique\produits\modals\save\modal-save-produit-boutique-content.html.php
echo COPY src\webapp\views\admin\shop\boutique\produits\modals\save\modal-save-produit-boutique-controller.php target\webapp\views\admin\shop\boutique\produits\modals\save\modal-save-produit-boutique-controller.php
COPY src\webapp\views\admin\shop\boutique\produits\modals\save\modal-save-produit-boutique-controller.php target\webapp\views\admin\shop\boutique\produits\modals\save\modal-save-produit-boutique-controller.php
echo COPY src\webapp\views\admin\shop\boutique\produits\modals\save\modal-save-produit-boutique.html.php target\webapp\views\admin\shop\boutique\produits\modals\save\modal-save-produit-boutique.html.php
COPY src\webapp\views\admin\shop\boutique\produits\modals\save\modal-save-produit-boutique.html.php target\webapp\views\admin\shop\boutique\produits\modals\save\modal-save-produit-boutique.html.php
echo MKDIR target\webapp\views\admin\shop\boutique\produits\views
MKDIR target\webapp\views\admin\shop\boutique\produits\views
echo MKDIR target\webapp\views\admin\shop\boutique\produits\views\recherche
MKDIR target\webapp\views\admin\shop\boutique\produits\views\recherche
echo COPY src\webapp\views\admin\shop\boutique\produits\views\recherche\recherche-produits-controller.php target\webapp\views\admin\shop\boutique\produits\views\recherche\recherche-produits-controller.php
COPY src\webapp\views\admin\shop\boutique\produits\views\recherche\recherche-produits-controller.php target\webapp\views\admin\shop\boutique\produits\views\recherche\recherche-produits-controller.php
echo COPY src\webapp\views\admin\shop\boutique\produits\views\recherche\recherche-produits.html.php target\webapp\views\admin\shop\boutique\produits\views\recherche\recherche-produits.html.php
COPY src\webapp\views\admin\shop\boutique\produits\views\recherche\recherche-produits.html.php target\webapp\views\admin\shop\boutique\produits\views\recherche\recherche-produits.html.php
echo MKDIR target\webapp\views\admin\shop\boutique\produits\views\stats
MKDIR target\webapp\views\admin\shop\boutique\produits\views\stats
echo COPY src\webapp\views\admin\shop\boutique\produits\views\stats\stats-produits-controller.php target\webapp\views\admin\shop\boutique\produits\views\stats\stats-produits-controller.php
COPY src\webapp\views\admin\shop\boutique\produits\views\stats\stats-produits-controller.php target\webapp\views\admin\shop\boutique\produits\views\stats\stats-produits-controller.php
echo COPY src\webapp\views\admin\shop\boutique\produits\views\stats\stats-produits.html.php target\webapp\views\admin\shop\boutique\produits\views\stats\stats-produits.html.php
COPY src\webapp\views\admin\shop\boutique\produits\views\stats\stats-produits.html.php target\webapp\views\admin\shop\boutique\produits\views\stats\stats-produits.html.php
echo MKDIR target\webapp\views\admin\shop\boutique\produits\views\table
MKDIR target\webapp\views\admin\shop\boutique\produits\views\table
echo COPY src\webapp\views\admin\shop\boutique\produits\views\table\table-produits-controller.php target\webapp\views\admin\shop\boutique\produits\views\table\table-produits-controller.php
COPY src\webapp\views\admin\shop\boutique\produits\views\table\table-produits-controller.php target\webapp\views\admin\shop\boutique\produits\views\table\table-produits-controller.php
echo COPY src\webapp\views\admin\shop\boutique\produits\views\table\table-produits.html.php target\webapp\views\admin\shop\boutique\produits\views\table\table-produits.html.php
COPY src\webapp\views\admin\shop\boutique\produits\views\table\table-produits.html.php target\webapp\views\admin\shop\boutique\produits\views\table\table-produits.html.php
echo MKDIR target\webapp\views\admin\shop\boutique\stock
MKDIR target\webapp\views\admin\shop\boutique\stock
echo COPY src\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock-config.json target\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock-config.json
COPY src\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock-config.json target\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock-controller.js -o target\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock-controller.js -o target\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock-controller.js -c -m
echo COPY src\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock-controller.php target\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock-controller.php
COPY src\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock-controller.php target\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock-controller.php
echo COPY src\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock.html.php target\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock.html.php
COPY src\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock.html.php target\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock.less target\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock.css
node ..\node_modules\less\bin\lessc src\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock.less target\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock.css > target\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock.css > target\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock.css.prod
echo DEL target\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock.css
DEL target\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock.css
echo MOVE target\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock.css.prod target\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock.css
MOVE target\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock.css.prod target\webapp\views\admin\shop\boutique\stock\admin-shop-boutique-stock.css
echo MKDIR target\webapp\views\admin\shop\boutique\stock\views
MKDIR target\webapp\views\admin\shop\boutique\stock\views
echo MKDIR target\webapp\views\admin\shop\boutique\stock\views\recherche
MKDIR target\webapp\views\admin\shop\boutique\stock\views\recherche
echo COPY src\webapp\views\admin\shop\boutique\stock\views\recherche\recherche-stock-controller.php target\webapp\views\admin\shop\boutique\stock\views\recherche\recherche-stock-controller.php
COPY src\webapp\views\admin\shop\boutique\stock\views\recherche\recherche-stock-controller.php target\webapp\views\admin\shop\boutique\stock\views\recherche\recherche-stock-controller.php
echo COPY src\webapp\views\admin\shop\boutique\stock\views\recherche\recherche-stock.html.php target\webapp\views\admin\shop\boutique\stock\views\recherche\recherche-stock.html.php
COPY src\webapp\views\admin\shop\boutique\stock\views\recherche\recherche-stock.html.php target\webapp\views\admin\shop\boutique\stock\views\recherche\recherche-stock.html.php
echo MKDIR target\webapp\views\admin\shop\boutique\stock\views\stats
MKDIR target\webapp\views\admin\shop\boutique\stock\views\stats
echo COPY src\webapp\views\admin\shop\boutique\stock\views\stats\stats-stock-controller.php target\webapp\views\admin\shop\boutique\stock\views\stats\stats-stock-controller.php
COPY src\webapp\views\admin\shop\boutique\stock\views\stats\stats-stock-controller.php target\webapp\views\admin\shop\boutique\stock\views\stats\stats-stock-controller.php
echo COPY src\webapp\views\admin\shop\boutique\stock\views\stats\stats-stock.html.php target\webapp\views\admin\shop\boutique\stock\views\stats\stats-stock.html.php
COPY src\webapp\views\admin\shop\boutique\stock\views\stats\stats-stock.html.php target\webapp\views\admin\shop\boutique\stock\views\stats\stats-stock.html.php
echo MKDIR target\webapp\views\admin\shop\boutique\stock\views\table
MKDIR target\webapp\views\admin\shop\boutique\stock\views\table
echo COPY src\webapp\views\admin\shop\boutique\stock\views\table\table-stock-controller.php target\webapp\views\admin\shop\boutique\stock\views\table\table-stock-controller.php
COPY src\webapp\views\admin\shop\boutique\stock\views\table\table-stock-controller.php target\webapp\views\admin\shop\boutique\stock\views\table\table-stock-controller.php
echo COPY src\webapp\views\admin\shop\boutique\stock\views\table\table-stock.html.php target\webapp\views\admin\shop\boutique\stock\views\table\table-stock.html.php
COPY src\webapp\views\admin\shop\boutique\stock\views\table\table-stock.html.php target\webapp\views\admin\shop\boutique\stock\views\table\table-stock.html.php
echo MKDIR target\webapp\views\admin\shop\commandes
MKDIR target\webapp\views\admin\shop\commandes
echo COPY src\webapp\views\admin\shop\commandes\admin-shop-commandes-config.json target\webapp\views\admin\shop\commandes\admin-shop-commandes-config.json
COPY src\webapp\views\admin\shop\commandes\admin-shop-commandes-config.json target\webapp\views\admin\shop\commandes\admin-shop-commandes-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\shop\commandes\admin-shop-commandes-controller.js -o target\webapp\views\admin\shop\commandes\admin-shop-commandes-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\shop\commandes\admin-shop-commandes-controller.js -o target\webapp\views\admin\shop\commandes\admin-shop-commandes-controller.js -c -m
echo COPY src\webapp\views\admin\shop\commandes\admin-shop-commandes-controller.php target\webapp\views\admin\shop\commandes\admin-shop-commandes-controller.php
COPY src\webapp\views\admin\shop\commandes\admin-shop-commandes-controller.php target\webapp\views\admin\shop\commandes\admin-shop-commandes-controller.php
echo COPY src\webapp\views\admin\shop\commandes\admin-shop-commandes.html.php target\webapp\views\admin\shop\commandes\admin-shop-commandes.html.php
COPY src\webapp\views\admin\shop\commandes\admin-shop-commandes.html.php target\webapp\views\admin\shop\commandes\admin-shop-commandes.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\admin\shop\commandes\admin-shop-commandes.less target\webapp\views\admin\shop\commandes\admin-shop-commandes.css
node ..\node_modules\less\bin\lessc src\webapp\views\admin\shop\commandes\admin-shop-commandes.less target\webapp\views\admin\shop\commandes\admin-shop-commandes.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\shop\commandes\admin-shop-commandes.css > target\webapp\views\admin\shop\commandes\admin-shop-commandes.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\shop\commandes\admin-shop-commandes.css > target\webapp\views\admin\shop\commandes\admin-shop-commandes.css.prod
echo DEL target\webapp\views\admin\shop\commandes\admin-shop-commandes.css
DEL target\webapp\views\admin\shop\commandes\admin-shop-commandes.css
echo MOVE target\webapp\views\admin\shop\commandes\admin-shop-commandes.css.prod target\webapp\views\admin\shop\commandes\admin-shop-commandes.css
MOVE target\webapp\views\admin\shop\commandes\admin-shop-commandes.css.prod target\webapp\views\admin\shop\commandes\admin-shop-commandes.css
echo MKDIR target\webapp\views\admin\shop\commandes\modals
MKDIR target\webapp\views\admin\shop\commandes\modals
echo MKDIR target\webapp\views\admin\shop\commandes\modals\paiements
MKDIR target\webapp\views\admin\shop\commandes\modals\paiements
echo COPY src\webapp\views\admin\shop\commandes\modals\paiements\modal-paiements-commande-content.html.php target\webapp\views\admin\shop\commandes\modals\paiements\modal-paiements-commande-content.html.php
COPY src\webapp\views\admin\shop\commandes\modals\paiements\modal-paiements-commande-content.html.php target\webapp\views\admin\shop\commandes\modals\paiements\modal-paiements-commande-content.html.php
echo COPY src\webapp\views\admin\shop\commandes\modals\paiements\modal-paiements-commande-controller.php target\webapp\views\admin\shop\commandes\modals\paiements\modal-paiements-commande-controller.php
COPY src\webapp\views\admin\shop\commandes\modals\paiements\modal-paiements-commande-controller.php target\webapp\views\admin\shop\commandes\modals\paiements\modal-paiements-commande-controller.php
echo COPY src\webapp\views\admin\shop\commandes\modals\paiements\modal-paiements-commande.html.php target\webapp\views\admin\shop\commandes\modals\paiements\modal-paiements-commande.html.php
COPY src\webapp\views\admin\shop\commandes\modals\paiements\modal-paiements-commande.html.php target\webapp\views\admin\shop\commandes\modals\paiements\modal-paiements-commande.html.php
echo MKDIR target\webapp\views\admin\shop\commandes\views
MKDIR target\webapp\views\admin\shop\commandes\views
echo MKDIR target\webapp\views\admin\shop\commandes\views\recherche
MKDIR target\webapp\views\admin\shop\commandes\views\recherche
echo COPY src\webapp\views\admin\shop\commandes\views\recherche\recherche-commandes-controller.php target\webapp\views\admin\shop\commandes\views\recherche\recherche-commandes-controller.php
COPY src\webapp\views\admin\shop\commandes\views\recherche\recherche-commandes-controller.php target\webapp\views\admin\shop\commandes\views\recherche\recherche-commandes-controller.php
echo COPY src\webapp\views\admin\shop\commandes\views\recherche\recherche-commandes.html.php target\webapp\views\admin\shop\commandes\views\recherche\recherche-commandes.html.php
COPY src\webapp\views\admin\shop\commandes\views\recherche\recherche-commandes.html.php target\webapp\views\admin\shop\commandes\views\recherche\recherche-commandes.html.php
echo MKDIR target\webapp\views\admin\shop\commandes\views\stats
MKDIR target\webapp\views\admin\shop\commandes\views\stats
echo COPY src\webapp\views\admin\shop\commandes\views\stats\stats-commandes-controller.php target\webapp\views\admin\shop\commandes\views\stats\stats-commandes-controller.php
COPY src\webapp\views\admin\shop\commandes\views\stats\stats-commandes-controller.php target\webapp\views\admin\shop\commandes\views\stats\stats-commandes-controller.php
echo COPY src\webapp\views\admin\shop\commandes\views\stats\stats-commandes.html.php target\webapp\views\admin\shop\commandes\views\stats\stats-commandes.html.php
COPY src\webapp\views\admin\shop\commandes\views\stats\stats-commandes.html.php target\webapp\views\admin\shop\commandes\views\stats\stats-commandes.html.php
echo MKDIR target\webapp\views\admin\shop\commandes\views\table
MKDIR target\webapp\views\admin\shop\commandes\views\table
echo COPY src\webapp\views\admin\shop\commandes\views\table\table-commandes-controller.php target\webapp\views\admin\shop\commandes\views\table\table-commandes-controller.php
COPY src\webapp\views\admin\shop\commandes\views\table\table-commandes-controller.php target\webapp\views\admin\shop\commandes\views\table\table-commandes-controller.php
echo COPY src\webapp\views\admin\shop\commandes\views\table\table-commandes.html.php target\webapp\views\admin\shop\commandes\views\table\table-commandes.html.php
COPY src\webapp\views\admin\shop\commandes\views\table\table-commandes.html.php target\webapp\views\admin\shop\commandes\views\table\table-commandes.html.php
echo MKDIR target\webapp\views\admin\shop\paiements
MKDIR target\webapp\views\admin\shop\paiements
echo COPY src\webapp\views\admin\shop\paiements\admin-shop-paiements-config.json target\webapp\views\admin\shop\paiements\admin-shop-paiements-config.json
COPY src\webapp\views\admin\shop\paiements\admin-shop-paiements-config.json target\webapp\views\admin\shop\paiements\admin-shop-paiements-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\shop\paiements\admin-shop-paiements-controller.js -o target\webapp\views\admin\shop\paiements\admin-shop-paiements-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\shop\paiements\admin-shop-paiements-controller.js -o target\webapp\views\admin\shop\paiements\admin-shop-paiements-controller.js -c -m
echo COPY src\webapp\views\admin\shop\paiements\admin-shop-paiements-controller.php target\webapp\views\admin\shop\paiements\admin-shop-paiements-controller.php
COPY src\webapp\views\admin\shop\paiements\admin-shop-paiements-controller.php target\webapp\views\admin\shop\paiements\admin-shop-paiements-controller.php
echo COPY src\webapp\views\admin\shop\paiements\admin-shop-paiements.html.php target\webapp\views\admin\shop\paiements\admin-shop-paiements.html.php
COPY src\webapp\views\admin\shop\paiements\admin-shop-paiements.html.php target\webapp\views\admin\shop\paiements\admin-shop-paiements.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\admin\shop\paiements\admin-shop-paiements.less target\webapp\views\admin\shop\paiements\admin-shop-paiements.css
node ..\node_modules\less\bin\lessc src\webapp\views\admin\shop\paiements\admin-shop-paiements.less target\webapp\views\admin\shop\paiements\admin-shop-paiements.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\shop\paiements\admin-shop-paiements.css > target\webapp\views\admin\shop\paiements\admin-shop-paiements.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\shop\paiements\admin-shop-paiements.css > target\webapp\views\admin\shop\paiements\admin-shop-paiements.css.prod
echo DEL target\webapp\views\admin\shop\paiements\admin-shop-paiements.css
DEL target\webapp\views\admin\shop\paiements\admin-shop-paiements.css
echo MOVE target\webapp\views\admin\shop\paiements\admin-shop-paiements.css.prod target\webapp\views\admin\shop\paiements\admin-shop-paiements.css
MOVE target\webapp\views\admin\shop\paiements\admin-shop-paiements.css.prod target\webapp\views\admin\shop\paiements\admin-shop-paiements.css
echo MKDIR target\webapp\views\admin\shop\paiements\modals
MKDIR target\webapp\views\admin\shop\paiements\modals
echo MKDIR target\webapp\views\admin\shop\paiements\modals\commande
MKDIR target\webapp\views\admin\shop\paiements\modals\commande
echo COPY src\webapp\views\admin\shop\paiements\modals\commande\modal-commande-paiement-content.html.php target\webapp\views\admin\shop\paiements\modals\commande\modal-commande-paiement-content.html.php
COPY src\webapp\views\admin\shop\paiements\modals\commande\modal-commande-paiement-content.html.php target\webapp\views\admin\shop\paiements\modals\commande\modal-commande-paiement-content.html.php
echo COPY src\webapp\views\admin\shop\paiements\modals\commande\modal-commande-paiement-controller.php target\webapp\views\admin\shop\paiements\modals\commande\modal-commande-paiement-controller.php
COPY src\webapp\views\admin\shop\paiements\modals\commande\modal-commande-paiement-controller.php target\webapp\views\admin\shop\paiements\modals\commande\modal-commande-paiement-controller.php
echo COPY src\webapp\views\admin\shop\paiements\modals\commande\modal-commande-paiement.html.php target\webapp\views\admin\shop\paiements\modals\commande\modal-commande-paiement.html.php
COPY src\webapp\views\admin\shop\paiements\modals\commande\modal-commande-paiement.html.php target\webapp\views\admin\shop\paiements\modals\commande\modal-commande-paiement.html.php
echo MKDIR target\webapp\views\admin\shop\paiements\modals\link-commande
MKDIR target\webapp\views\admin\shop\paiements\modals\link-commande
echo COPY src\webapp\views\admin\shop\paiements\modals\link-commande\modal-link-commande-paiement-content.html.php target\webapp\views\admin\shop\paiements\modals\link-commande\modal-link-commande-paiement-content.html.php
COPY src\webapp\views\admin\shop\paiements\modals\link-commande\modal-link-commande-paiement-content.html.php target\webapp\views\admin\shop\paiements\modals\link-commande\modal-link-commande-paiement-content.html.php
echo COPY src\webapp\views\admin\shop\paiements\modals\link-commande\modal-link-commande-paiement-controller.php target\webapp\views\admin\shop\paiements\modals\link-commande\modal-link-commande-paiement-controller.php
COPY src\webapp\views\admin\shop\paiements\modals\link-commande\modal-link-commande-paiement-controller.php target\webapp\views\admin\shop\paiements\modals\link-commande\modal-link-commande-paiement-controller.php
echo COPY src\webapp\views\admin\shop\paiements\modals\link-commande\modal-link-commande-paiement.html.php target\webapp\views\admin\shop\paiements\modals\link-commande\modal-link-commande-paiement.html.php
COPY src\webapp\views\admin\shop\paiements\modals\link-commande\modal-link-commande-paiement.html.php target\webapp\views\admin\shop\paiements\modals\link-commande\modal-link-commande-paiement.html.php
echo MKDIR target\webapp\views\admin\shop\paiements\modals\save
MKDIR target\webapp\views\admin\shop\paiements\modals\save
echo COPY src\webapp\views\admin\shop\paiements\modals\save\modal-save-paiement-content.html.php target\webapp\views\admin\shop\paiements\modals\save\modal-save-paiement-content.html.php
COPY src\webapp\views\admin\shop\paiements\modals\save\modal-save-paiement-content.html.php target\webapp\views\admin\shop\paiements\modals\save\modal-save-paiement-content.html.php
echo COPY src\webapp\views\admin\shop\paiements\modals\save\modal-save-paiement-controller.php target\webapp\views\admin\shop\paiements\modals\save\modal-save-paiement-controller.php
COPY src\webapp\views\admin\shop\paiements\modals\save\modal-save-paiement-controller.php target\webapp\views\admin\shop\paiements\modals\save\modal-save-paiement-controller.php
echo COPY src\webapp\views\admin\shop\paiements\modals\save\modal-save-paiement.html.php target\webapp\views\admin\shop\paiements\modals\save\modal-save-paiement.html.php
COPY src\webapp\views\admin\shop\paiements\modals\save\modal-save-paiement.html.php target\webapp\views\admin\shop\paiements\modals\save\modal-save-paiement.html.php
echo MKDIR target\webapp\views\admin\shop\paiements\views
MKDIR target\webapp\views\admin\shop\paiements\views
echo MKDIR target\webapp\views\admin\shop\paiements\views\recherche
MKDIR target\webapp\views\admin\shop\paiements\views\recherche
echo COPY src\webapp\views\admin\shop\paiements\views\recherche\recherche-paiements-controller.php target\webapp\views\admin\shop\paiements\views\recherche\recherche-paiements-controller.php
COPY src\webapp\views\admin\shop\paiements\views\recherche\recherche-paiements-controller.php target\webapp\views\admin\shop\paiements\views\recherche\recherche-paiements-controller.php
echo COPY src\webapp\views\admin\shop\paiements\views\recherche\recherche-paiements.html.php target\webapp\views\admin\shop\paiements\views\recherche\recherche-paiements.html.php
COPY src\webapp\views\admin\shop\paiements\views\recherche\recherche-paiements.html.php target\webapp\views\admin\shop\paiements\views\recherche\recherche-paiements.html.php
echo MKDIR target\webapp\views\admin\shop\paiements\views\table
MKDIR target\webapp\views\admin\shop\paiements\views\table
echo COPY src\webapp\views\admin\shop\paiements\views\table\table-paiements-controller.php target\webapp\views\admin\shop\paiements\views\table\table-paiements-controller.php
COPY src\webapp\views\admin\shop\paiements\views\table\table-paiements-controller.php target\webapp\views\admin\shop\paiements\views\table\table-paiements-controller.php
echo COPY src\webapp\views\admin\shop\paiements\views\table\table-paiements.html.php target\webapp\views\admin\shop\paiements\views\table\table-paiements.html.php
COPY src\webapp\views\admin\shop\paiements\views\table\table-paiements.html.php target\webapp\views\admin\shop\paiements\views\table\table-paiements.html.php
echo MKDIR target\webapp\views\admin\shop\sub-menu
MKDIR target\webapp\views\admin\shop\sub-menu
echo COPY src\webapp\views\admin\shop\sub-menu\sub-menu-admin-shop.html.php target\webapp\views\admin\shop\sub-menu\sub-menu-admin-shop.html.php
COPY src\webapp\views\admin\shop\sub-menu\sub-menu-admin-shop.html.php target\webapp\views\admin\shop\sub-menu\sub-menu-admin-shop.html.php
echo MKDIR target\webapp\views\admin\superadmin
MKDIR target\webapp\views\admin\superadmin
echo COPY src\webapp\views\admin\superadmin\admin-superadmin.html.php target\webapp\views\admin\superadmin\admin-superadmin.html.php
COPY src\webapp\views\admin\superadmin\admin-superadmin.html.php target\webapp\views\admin\superadmin\admin-superadmin.html.php
echo MKDIR target\webapp\views\admin\superadmin\newsletter
MKDIR target\webapp\views\admin\superadmin\newsletter
echo COPY src\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter-config.json target\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter-config.json
COPY src\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter-config.json target\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter-controller.js -o target\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter-controller.js -o target\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter-controller.js -c -m
echo COPY src\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter-controller.php target\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter-controller.php
COPY src\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter-controller.php target\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter-controller.php
echo COPY src\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter.html.php target\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter.html.php
COPY src\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter.html.php target\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter.less target\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter.css
node ..\node_modules\less\bin\lessc src\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter.less target\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter.css > target\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter.css > target\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter.css.prod
echo DEL target\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter.css
DEL target\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter.css
echo MOVE target\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter.css.prod target\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter.css
MOVE target\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter.css.prod target\webapp\views\admin\superadmin\newsletter\admin-superadmin-newsletter.css
echo MKDIR target\webapp\views\admin\superadmin\newsletter\modals
MKDIR target\webapp\views\admin\superadmin\newsletter\modals
echo MKDIR target\webapp\views\admin\superadmin\newsletter\modals\newsletter-preview
MKDIR target\webapp\views\admin\superadmin\newsletter\modals\newsletter-preview
echo COPY src\webapp\views\admin\superadmin\newsletter\modals\newsletter-preview\modal-newsletter-preview-content.html.php target\webapp\views\admin\superadmin\newsletter\modals\newsletter-preview\modal-newsletter-preview-content.html.php
COPY src\webapp\views\admin\superadmin\newsletter\modals\newsletter-preview\modal-newsletter-preview-content.html.php target\webapp\views\admin\superadmin\newsletter\modals\newsletter-preview\modal-newsletter-preview-content.html.php
echo COPY src\webapp\views\admin\superadmin\newsletter\modals\newsletter-preview\modal-newsletter-preview-controller.php target\webapp\views\admin\superadmin\newsletter\modals\newsletter-preview\modal-newsletter-preview-controller.php
COPY src\webapp\views\admin\superadmin\newsletter\modals\newsletter-preview\modal-newsletter-preview-controller.php target\webapp\views\admin\superadmin\newsletter\modals\newsletter-preview\modal-newsletter-preview-controller.php
echo COPY src\webapp\views\admin\superadmin\newsletter\modals\newsletter-preview\modal-newsletter-preview.html.php target\webapp\views\admin\superadmin\newsletter\modals\newsletter-preview\modal-newsletter-preview.html.php
COPY src\webapp\views\admin\superadmin\newsletter\modals\newsletter-preview\modal-newsletter-preview.html.php target\webapp\views\admin\superadmin\newsletter\modals\newsletter-preview\modal-newsletter-preview.html.php
echo MKDIR target\webapp\views\admin\superadmin\sub-menu
MKDIR target\webapp\views\admin\superadmin\sub-menu
echo COPY src\webapp\views\admin\superadmin\sub-menu\sub-menu-admin-superadmin.html.php target\webapp\views\admin\superadmin\sub-menu\sub-menu-admin-superadmin.html.php
COPY src\webapp\views\admin\superadmin\sub-menu\sub-menu-admin-superadmin.html.php target\webapp\views\admin\superadmin\sub-menu\sub-menu-admin-superadmin.html.php
echo MKDIR target\webapp\views\agenda
MKDIR target\webapp\views\agenda
echo COPY src\webapp\views\agenda\agenda-config.json target\webapp\views\agenda\agenda-config.json
COPY src\webapp\views\agenda\agenda-config.json target\webapp\views\agenda\agenda-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\agenda\agenda-controller.js -o target\webapp\views\agenda\agenda-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\agenda\agenda-controller.js -o target\webapp\views\agenda\agenda-controller.js -c -m
echo COPY src\webapp\views\agenda\agenda-controller.php target\webapp\views\agenda\agenda-controller.php
COPY src\webapp\views\agenda\agenda-controller.php target\webapp\views\agenda\agenda-controller.php
echo COPY src\webapp\views\agenda\agenda.html.php target\webapp\views\agenda\agenda.html.php
COPY src\webapp\views\agenda\agenda.html.php target\webapp\views\agenda\agenda.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\agenda\agenda.less target\webapp\views\agenda\agenda.css
node ..\node_modules\less\bin\lessc src\webapp\views\agenda\agenda.less target\webapp\views\agenda\agenda.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\agenda\agenda.css > target\webapp\views\agenda\agenda.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\agenda\agenda.css > target\webapp\views\agenda\agenda.css.prod
echo DEL target\webapp\views\agenda\agenda.css
DEL target\webapp\views\agenda\agenda.css
echo MOVE target\webapp\views\agenda\agenda.css.prod target\webapp\views\agenda\agenda.css
MOVE target\webapp\views\agenda\agenda.css.prod target\webapp\views\agenda\agenda.css
echo MKDIR target\webapp\views\club
MKDIR target\webapp\views\club
echo COPY src\webapp\views\club\club.html.php target\webapp\views\club\club.html.php
COPY src\webapp\views\club\club.html.php target\webapp\views\club\club.html.php
echo MKDIR target\webapp\views\club\documents
MKDIR target\webapp\views\club\documents
echo COPY src\webapp\views\club\documents\club-documents-config.json target\webapp\views\club\documents\club-documents-config.json
COPY src\webapp\views\club\documents\club-documents-config.json target\webapp\views\club\documents\club-documents-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\club\documents\club-documents-controller.js -o target\webapp\views\club\documents\club-documents-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\club\documents\club-documents-controller.js -o target\webapp\views\club\documents\club-documents-controller.js -c -m
echo COPY src\webapp\views\club\documents\club-documents-controller.php target\webapp\views\club\documents\club-documents-controller.php
COPY src\webapp\views\club\documents\club-documents-controller.php target\webapp\views\club\documents\club-documents-controller.php
echo COPY src\webapp\views\club\documents\club-documents.html.php target\webapp\views\club\documents\club-documents.html.php
COPY src\webapp\views\club\documents\club-documents.html.php target\webapp\views\club\documents\club-documents.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\club\documents\club-documents.less target\webapp\views\club\documents\club-documents.css
node ..\node_modules\less\bin\lessc src\webapp\views\club\documents\club-documents.less target\webapp\views\club\documents\club-documents.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\club\documents\club-documents.css > target\webapp\views\club\documents\club-documents.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\club\documents\club-documents.css > target\webapp\views\club\documents\club-documents.css.prod
echo DEL target\webapp\views\club\documents\club-documents.css
DEL target\webapp\views\club\documents\club-documents.css
echo MOVE target\webapp\views\club\documents\club-documents.css.prod target\webapp\views\club\documents\club-documents.css
MOVE target\webapp\views\club\documents\club-documents.css.prod target\webapp\views\club\documents\club-documents.css
echo MKDIR target\webapp\views\club\equipes
MKDIR target\webapp\views\club\equipes
echo COPY src\webapp\views\club\equipes\club-equipes-config.json target\webapp\views\club\equipes\club-equipes-config.json
COPY src\webapp\views\club\equipes\club-equipes-config.json target\webapp\views\club\equipes\club-equipes-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\club\equipes\club-equipes-controller.js -o target\webapp\views\club\equipes\club-equipes-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\club\equipes\club-equipes-controller.js -o target\webapp\views\club\equipes\club-equipes-controller.js -c -m
echo COPY src\webapp\views\club\equipes\club-equipes-controller.php target\webapp\views\club\equipes\club-equipes-controller.php
COPY src\webapp\views\club\equipes\club-equipes-controller.php target\webapp\views\club\equipes\club-equipes-controller.php
echo COPY src\webapp\views\club\equipes\club-equipes.html.php target\webapp\views\club\equipes\club-equipes.html.php
COPY src\webapp\views\club\equipes\club-equipes.html.php target\webapp\views\club\equipes\club-equipes.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\club\equipes\club-equipes.less target\webapp\views\club\equipes\club-equipes.css
node ..\node_modules\less\bin\lessc src\webapp\views\club\equipes\club-equipes.less target\webapp\views\club\equipes\club-equipes.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\club\equipes\club-equipes.css > target\webapp\views\club\equipes\club-equipes.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\club\equipes\club-equipes.css > target\webapp\views\club\equipes\club-equipes.css.prod
echo DEL target\webapp\views\club\equipes\club-equipes.css
DEL target\webapp\views\club\equipes\club-equipes.css
echo MOVE target\webapp\views\club\equipes\club-equipes.css.prod target\webapp\views\club\equipes\club-equipes.css
MOVE target\webapp\views\club\equipes\club-equipes.css.prod target\webapp\views\club\equipes\club-equipes.css
echo MKDIR target\webapp\views\club\equipes\equipe
MKDIR target\webapp\views\club\equipes\equipe
echo COPY src\webapp\views\club\equipes\equipe\club-equipes-equipe-config.json target\webapp\views\club\equipes\equipe\club-equipes-equipe-config.json
COPY src\webapp\views\club\equipes\equipe\club-equipes-equipe-config.json target\webapp\views\club\equipes\equipe\club-equipes-equipe-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\club\equipes\equipe\club-equipes-equipe-controller.js -o target\webapp\views\club\equipes\equipe\club-equipes-equipe-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\club\equipes\equipe\club-equipes-equipe-controller.js -o target\webapp\views\club\equipes\equipe\club-equipes-equipe-controller.js -c -m
echo COPY src\webapp\views\club\equipes\equipe\club-equipes-equipe-controller.php target\webapp\views\club\equipes\equipe\club-equipes-equipe-controller.php
COPY src\webapp\views\club\equipes\equipe\club-equipes-equipe-controller.php target\webapp\views\club\equipes\equipe\club-equipes-equipe-controller.php
echo COPY src\webapp\views\club\equipes\equipe\club-equipes-equipe.html.php target\webapp\views\club\equipes\equipe\club-equipes-equipe.html.php
COPY src\webapp\views\club\equipes\equipe\club-equipes-equipe.html.php target\webapp\views\club\equipes\equipe\club-equipes-equipe.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\club\equipes\equipe\club-equipes-equipe.less target\webapp\views\club\equipes\equipe\club-equipes-equipe.css
node ..\node_modules\less\bin\lessc src\webapp\views\club\equipes\equipe\club-equipes-equipe.less target\webapp\views\club\equipes\equipe\club-equipes-equipe.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\club\equipes\equipe\club-equipes-equipe.css > target\webapp\views\club\equipes\equipe\club-equipes-equipe.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\club\equipes\equipe\club-equipes-equipe.css > target\webapp\views\club\equipes\equipe\club-equipes-equipe.css.prod
echo DEL target\webapp\views\club\equipes\equipe\club-equipes-equipe.css
DEL target\webapp\views\club\equipes\equipe\club-equipes-equipe.css
echo MOVE target\webapp\views\club\equipes\equipe\club-equipes-equipe.css.prod target\webapp\views\club\equipes\equipe\club-equipes-equipe.css
MOVE target\webapp\views\club\equipes\equipe\club-equipes-equipe.css.prod target\webapp\views\club\equipes\equipe\club-equipes-equipe.css
echo MKDIR target\webapp\views\club\presentation
MKDIR target\webapp\views\club\presentation
echo COPY src\webapp\views\club\presentation\club-presentation-config.json target\webapp\views\club\presentation\club-presentation-config.json
COPY src\webapp\views\club\presentation\club-presentation-config.json target\webapp\views\club\presentation\club-presentation-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\club\presentation\club-presentation-controller.js -o target\webapp\views\club\presentation\club-presentation-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\club\presentation\club-presentation-controller.js -o target\webapp\views\club\presentation\club-presentation-controller.js -c -m
echo COPY src\webapp\views\club\presentation\club-presentation-controller.php target\webapp\views\club\presentation\club-presentation-controller.php
COPY src\webapp\views\club\presentation\club-presentation-controller.php target\webapp\views\club\presentation\club-presentation-controller.php
echo COPY src\webapp\views\club\presentation\club-presentation.html.php target\webapp\views\club\presentation\club-presentation.html.php
COPY src\webapp\views\club\presentation\club-presentation.html.php target\webapp\views\club\presentation\club-presentation.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\club\presentation\club-presentation.less target\webapp\views\club\presentation\club-presentation.css
node ..\node_modules\less\bin\lessc src\webapp\views\club\presentation\club-presentation.less target\webapp\views\club\presentation\club-presentation.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\club\presentation\club-presentation.css > target\webapp\views\club\presentation\club-presentation.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\club\presentation\club-presentation.css > target\webapp\views\club\presentation\club-presentation.css.prod
echo DEL target\webapp\views\club\presentation\club-presentation.css
DEL target\webapp\views\club\presentation\club-presentation.css
echo MOVE target\webapp\views\club\presentation\club-presentation.css.prod target\webapp\views\club\presentation\club-presentation.css
MOVE target\webapp\views\club\presentation\club-presentation.css.prod target\webapp\views\club\presentation\club-presentation.css
echo MKDIR target\webapp\views\club\reglement
MKDIR target\webapp\views\club\reglement
echo COPY src\webapp\views\club\reglement\club-reglement-config.json target\webapp\views\club\reglement\club-reglement-config.json
COPY src\webapp\views\club\reglement\club-reglement-config.json target\webapp\views\club\reglement\club-reglement-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\club\reglement\club-reglement-controller.js -o target\webapp\views\club\reglement\club-reglement-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\club\reglement\club-reglement-controller.js -o target\webapp\views\club\reglement\club-reglement-controller.js -c -m
echo COPY src\webapp\views\club\reglement\club-reglement-controller.php target\webapp\views\club\reglement\club-reglement-controller.php
COPY src\webapp\views\club\reglement\club-reglement-controller.php target\webapp\views\club\reglement\club-reglement-controller.php
echo COPY src\webapp\views\club\reglement\club-reglement.html.php target\webapp\views\club\reglement\club-reglement.html.php
COPY src\webapp\views\club\reglement\club-reglement.html.php target\webapp\views\club\reglement\club-reglement.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\club\reglement\club-reglement.less target\webapp\views\club\reglement\club-reglement.css
node ..\node_modules\less\bin\lessc src\webapp\views\club\reglement\club-reglement.less target\webapp\views\club\reglement\club-reglement.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\club\reglement\club-reglement.css > target\webapp\views\club\reglement\club-reglement.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\club\reglement\club-reglement.css > target\webapp\views\club\reglement\club-reglement.css.prod
echo DEL target\webapp\views\club\reglement\club-reglement.css
DEL target\webapp\views\club\reglement\club-reglement.css
echo MOVE target\webapp\views\club\reglement\club-reglement.css.prod target\webapp\views\club\reglement\club-reglement.css
MOVE target\webapp\views\club\reglement\club-reglement.css.prod target\webapp\views\club\reglement\club-reglement.css
echo MKDIR target\webapp\views\club\sub-menu
MKDIR target\webapp\views\club\sub-menu
echo COPY src\webapp\views\club\sub-menu\sub-menu-club.html.php target\webapp\views\club\sub-menu\sub-menu-club.html.php
COPY src\webapp\views\club\sub-menu\sub-menu-club.html.php target\webapp\views\club\sub-menu\sub-menu-club.html.php
echo MKDIR target\webapp\views\common
MKDIR target\webapp\views\common
echo MKDIR target\webapp\views\common\error
MKDIR target\webapp\views\common\error
echo MKDIR target\webapp\views\common\error\400
MKDIR target\webapp\views\common\error\400
echo COPY src\webapp\views\common\error\400\400.html.php target\webapp\views\common\error\400\400.html.php
COPY src\webapp\views\common\error\400\400.html.php target\webapp\views\common\error\400\400.html.php
echo MKDIR target\webapp\views\common\error\401
MKDIR target\webapp\views\common\error\401
echo COPY src\webapp\views\common\error\401\401.html.php target\webapp\views\common\error\401\401.html.php
COPY src\webapp\views\common\error\401\401.html.php target\webapp\views\common\error\401\401.html.php
echo MKDIR target\webapp\views\common\error\404
MKDIR target\webapp\views\common\error\404
echo COPY src\webapp\views\common\error\404\404.html.php target\webapp\views\common\error\404\404.html.php
COPY src\webapp\views\common\error\404\404.html.php target\webapp\views\common\error\404\404.html.php
echo MKDIR target\webapp\views\common\footer
MKDIR target\webapp\views\common\footer
echo COPY src\webapp\views\common\footer\footer-controller.php target\webapp\views\common\footer\footer-controller.php
COPY src\webapp\views\common\footer\footer-controller.php target\webapp\views\common\footer\footer-controller.php
echo COPY src\webapp\views\common\footer\footer.html.php target\webapp\views\common\footer\footer.html.php
COPY src\webapp\views\common\footer\footer.html.php target\webapp\views\common\footer\footer.html.php
echo MKDIR target\webapp\views\common\header
MKDIR target\webapp\views\common\header
echo COPY src\webapp\views\common\header\header-controller.php target\webapp\views\common\header\header-controller.php
COPY src\webapp\views\common\header\header-controller.php target\webapp\views\common\header\header-controller.php
echo COPY src\webapp\views\common\header\header.html.php target\webapp\views\common\header\header.html.php
COPY src\webapp\views\common\header\header.html.php target\webapp\views\common\header\header.html.php
echo MKDIR target\webapp\views\common\mdp-form
MKDIR target\webapp\views\common\mdp-form
echo COPY src\webapp\views\common\mdp-form\mdp-form.html.php target\webapp\views\common\mdp-form\mdp-form.html.php
COPY src\webapp\views\common\mdp-form\mdp-form.html.php target\webapp\views\common\mdp-form\mdp-form.html.php
echo MKDIR target\webapp\views\common\modals
MKDIR target\webapp\views\common\modals
echo MKDIR target\webapp\views\common\modals\alert
MKDIR target\webapp\views\common\modals\alert
echo COPY src\webapp\views\common\modals\alert\modal-alert.html.php target\webapp\views\common\modals\alert\modal-alert.html.php
COPY src\webapp\views\common\modals\alert\modal-alert.html.php target\webapp\views\common\modals\alert\modal-alert.html.php
echo MKDIR target\webapp\views\common\modals\img
MKDIR target\webapp\views\common\modals\img
echo COPY src\webapp\views\common\modals\img\modal-img.html.php target\webapp\views\common\modals\img\modal-img.html.php
COPY src\webapp\views\common\modals\img\modal-img.html.php target\webapp\views\common\modals\img\modal-img.html.php
echo MKDIR target\webapp\views\common\modals\prompt
MKDIR target\webapp\views\common\modals\prompt
echo COPY src\webapp\views\common\modals\prompt\modal-prompt.html.php target\webapp\views\common\modals\prompt\modal-prompt.html.php
COPY src\webapp\views\common\modals\prompt\modal-prompt.html.php target\webapp\views\common\modals\prompt\modal-prompt.html.php
echo MKDIR target\webapp\views\common\nav
MKDIR target\webapp\views\common\nav
echo COPY src\webapp\views\common\nav\nav.html.php target\webapp\views\common\nav\nav.html.php
COPY src\webapp\views\common\nav\nav.html.php target\webapp\views\common\nav\nav.html.php
echo MKDIR target\webapp\views\dispos
MKDIR target\webapp\views\dispos
echo MKDIR target\webapp\views\dispos\actions
MKDIR target\webapp\views\dispos\actions
echo COPY src\webapp\views\dispos\actions\dispos-actions-config.json target\webapp\views\dispos\actions\dispos-actions-config.json
COPY src\webapp\views\dispos\actions\dispos-actions-config.json target\webapp\views\dispos\actions\dispos-actions-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\dispos\actions\dispos-actions-controller.js -o target\webapp\views\dispos\actions\dispos-actions-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\dispos\actions\dispos-actions-controller.js -o target\webapp\views\dispos\actions\dispos-actions-controller.js -c -m
echo COPY src\webapp\views\dispos\actions\dispos-actions-controller.php target\webapp\views\dispos\actions\dispos-actions-controller.php
COPY src\webapp\views\dispos\actions\dispos-actions-controller.php target\webapp\views\dispos\actions\dispos-actions-controller.php
echo COPY src\webapp\views\dispos\actions\dispos-actions.html.php target\webapp\views\dispos\actions\dispos-actions.html.php
COPY src\webapp\views\dispos\actions\dispos-actions.html.php target\webapp\views\dispos\actions\dispos-actions.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\dispos\actions\dispos-actions.less target\webapp\views\dispos\actions\dispos-actions.css
node ..\node_modules\less\bin\lessc src\webapp\views\dispos\actions\dispos-actions.less target\webapp\views\dispos\actions\dispos-actions.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\dispos\actions\dispos-actions.css > target\webapp\views\dispos\actions\dispos-actions.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\dispos\actions\dispos-actions.css > target\webapp\views\dispos\actions\dispos-actions.css.prod
echo DEL target\webapp\views\dispos\actions\dispos-actions.css
DEL target\webapp\views\dispos\actions\dispos-actions.css
echo MOVE target\webapp\views\dispos\actions\dispos-actions.css.prod target\webapp\views\dispos\actions\dispos-actions.css
MOVE target\webapp\views\dispos\actions\dispos-actions.css.prod target\webapp\views\dispos\actions\dispos-actions.css
echo MKDIR target\webapp\views\dispos\actions\modals
MKDIR target\webapp\views\dispos\actions\modals
echo MKDIR target\webapp\views\dispos\actions\modals\save
MKDIR target\webapp\views\dispos\actions\modals\save
echo COPY src\webapp\views\dispos\actions\modals\save\modal-save-action-content.html.php target\webapp\views\dispos\actions\modals\save\modal-save-action-content.html.php
COPY src\webapp\views\dispos\actions\modals\save\modal-save-action-content.html.php target\webapp\views\dispos\actions\modals\save\modal-save-action-content.html.php
echo COPY src\webapp\views\dispos\actions\modals\save\modal-save-action-controller.php target\webapp\views\dispos\actions\modals\save\modal-save-action-controller.php
COPY src\webapp\views\dispos\actions\modals\save\modal-save-action-controller.php target\webapp\views\dispos\actions\modals\save\modal-save-action-controller.php
echo COPY src\webapp\views\dispos\actions\modals\save\modal-save-action.html.php target\webapp\views\dispos\actions\modals\save\modal-save-action.html.php
COPY src\webapp\views\dispos\actions\modals\save\modal-save-action.html.php target\webapp\views\dispos\actions\modals\save\modal-save-action.html.php
echo MKDIR target\webapp\views\dispos\actions\views
MKDIR target\webapp\views\dispos\actions\views
echo MKDIR target\webapp\views\dispos\actions\views\recap-actions
MKDIR target\webapp\views\dispos\actions\views\recap-actions
echo COPY src\webapp\views\dispos\actions\views\recap-actions\form-recap-actions-controller.php target\webapp\views\dispos\actions\views\recap-actions\form-recap-actions-controller.php
COPY src\webapp\views\dispos\actions\views\recap-actions\form-recap-actions-controller.php target\webapp\views\dispos\actions\views\recap-actions\form-recap-actions-controller.php
echo COPY src\webapp\views\dispos\actions\views\recap-actions\form-recap-actions.html.php target\webapp\views\dispos\actions\views\recap-actions\form-recap-actions.html.php
COPY src\webapp\views\dispos\actions\views\recap-actions\form-recap-actions.html.php target\webapp\views\dispos\actions\views\recap-actions\form-recap-actions.html.php
echo MKDIR target\webapp\views\dispos\categorie
MKDIR target\webapp\views\dispos\categorie
echo COPY src\webapp\views\dispos\categorie\dispos-categorie-config.json target\webapp\views\dispos\categorie\dispos-categorie-config.json
COPY src\webapp\views\dispos\categorie\dispos-categorie-config.json target\webapp\views\dispos\categorie\dispos-categorie-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\dispos\categorie\dispos-categorie-controller.js -o target\webapp\views\dispos\categorie\dispos-categorie-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\dispos\categorie\dispos-categorie-controller.js -o target\webapp\views\dispos\categorie\dispos-categorie-controller.js -c -m
echo COPY src\webapp\views\dispos\categorie\dispos-categorie-controller.php target\webapp\views\dispos\categorie\dispos-categorie-controller.php
COPY src\webapp\views\dispos\categorie\dispos-categorie-controller.php target\webapp\views\dispos\categorie\dispos-categorie-controller.php
echo COPY src\webapp\views\dispos\categorie\dispos-categorie.html.php target\webapp\views\dispos\categorie\dispos-categorie.html.php
COPY src\webapp\views\dispos\categorie\dispos-categorie.html.php target\webapp\views\dispos\categorie\dispos-categorie.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\dispos\categorie\dispos-categorie.less target\webapp\views\dispos\categorie\dispos-categorie.css
node ..\node_modules\less\bin\lessc src\webapp\views\dispos\categorie\dispos-categorie.less target\webapp\views\dispos\categorie\dispos-categorie.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\dispos\categorie\dispos-categorie.css > target\webapp\views\dispos\categorie\dispos-categorie.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\dispos\categorie\dispos-categorie.css > target\webapp\views\dispos\categorie\dispos-categorie.css.prod
echo DEL target\webapp\views\dispos\categorie\dispos-categorie.css
DEL target\webapp\views\dispos\categorie\dispos-categorie.css
echo MOVE target\webapp\views\dispos\categorie\dispos-categorie.css.prod target\webapp\views\dispos\categorie\dispos-categorie.css
MOVE target\webapp\views\dispos\categorie\dispos-categorie.css.prod target\webapp\views\dispos\categorie\dispos-categorie.css
echo MKDIR target\webapp\views\dispos\common
MKDIR target\webapp\views\dispos\common
echo MKDIR target\webapp\views\dispos\common\nav
MKDIR target\webapp\views\dispos\common\nav
echo COPY src\webapp\views\dispos\common\nav\nav.html.php target\webapp\views\dispos\common\nav\nav.html.php
COPY src\webapp\views\dispos\common\nav\nav.html.php target\webapp\views\dispos\common\nav\nav.html.php
echo COPY src\webapp\views\dispos\dispos-config.json target\webapp\views\dispos\dispos-config.json
COPY src\webapp\views\dispos\dispos-config.json target\webapp\views\dispos\dispos-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\dispos\dispos-controller.js -o target\webapp\views\dispos\dispos-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\dispos\dispos-controller.js -o target\webapp\views\dispos\dispos-controller.js -c -m
echo COPY src\webapp\views\dispos\dispos-controller.php target\webapp\views\dispos\dispos-controller.php
COPY src\webapp\views\dispos\dispos-controller.php target\webapp\views\dispos\dispos-controller.php
echo COPY src\webapp\views\dispos\dispos.html.php target\webapp\views\dispos\dispos.html.php
COPY src\webapp\views\dispos\dispos.html.php target\webapp\views\dispos\dispos.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\dispos\dispos.less target\webapp\views\dispos\dispos.css
node ..\node_modules\less\bin\lessc src\webapp\views\dispos\dispos.less target\webapp\views\dispos\dispos.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\dispos\dispos.css > target\webapp\views\dispos\dispos.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\dispos\dispos.css > target\webapp\views\dispos\dispos.css.prod
echo DEL target\webapp\views\dispos\dispos.css
DEL target\webapp\views\dispos\dispos.css
echo MOVE target\webapp\views\dispos\dispos.css.prod target\webapp\views\dispos\dispos.css
MOVE target\webapp\views\dispos\dispos.css.prod target\webapp\views\dispos\dispos.css
echo MKDIR target\webapp\views\dispos\evenement
MKDIR target\webapp\views\dispos\evenement
echo COPY src\webapp\views\dispos\evenement\dispos-evenement-config.json target\webapp\views\dispos\evenement\dispos-evenement-config.json
COPY src\webapp\views\dispos\evenement\dispos-evenement-config.json target\webapp\views\dispos\evenement\dispos-evenement-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\dispos\evenement\dispos-evenement-controller.js -o target\webapp\views\dispos\evenement\dispos-evenement-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\dispos\evenement\dispos-evenement-controller.js -o target\webapp\views\dispos\evenement\dispos-evenement-controller.js -c -m
echo COPY src\webapp\views\dispos\evenement\dispos-evenement-controller.php target\webapp\views\dispos\evenement\dispos-evenement-controller.php
COPY src\webapp\views\dispos\evenement\dispos-evenement-controller.php target\webapp\views\dispos\evenement\dispos-evenement-controller.php
echo COPY src\webapp\views\dispos\evenement\dispos-evenement.html.php target\webapp\views\dispos\evenement\dispos-evenement.html.php
COPY src\webapp\views\dispos\evenement\dispos-evenement.html.php target\webapp\views\dispos\evenement\dispos-evenement.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\dispos\evenement\dispos-evenement.less target\webapp\views\dispos\evenement\dispos-evenement.css
node ..\node_modules\less\bin\lessc src\webapp\views\dispos\evenement\dispos-evenement.less target\webapp\views\dispos\evenement\dispos-evenement.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\dispos\evenement\dispos-evenement.css > target\webapp\views\dispos\evenement\dispos-evenement.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\dispos\evenement\dispos-evenement.css > target\webapp\views\dispos\evenement\dispos-evenement.css.prod
echo DEL target\webapp\views\dispos\evenement\dispos-evenement.css
DEL target\webapp\views\dispos\evenement\dispos-evenement.css
echo MOVE target\webapp\views\dispos\evenement\dispos-evenement.css.prod target\webapp\views\dispos\evenement\dispos-evenement.css
MOVE target\webapp\views\dispos\evenement\dispos-evenement.css.prod target\webapp\views\dispos\evenement\dispos-evenement.css
echo MKDIR target\webapp\views\dispos\licencie
MKDIR target\webapp\views\dispos\licencie
echo COPY src\webapp\views\dispos\licencie\dispos-licencie-config.json target\webapp\views\dispos\licencie\dispos-licencie-config.json
COPY src\webapp\views\dispos\licencie\dispos-licencie-config.json target\webapp\views\dispos\licencie\dispos-licencie-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\dispos\licencie\dispos-licencie-controller.js -o target\webapp\views\dispos\licencie\dispos-licencie-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\dispos\licencie\dispos-licencie-controller.js -o target\webapp\views\dispos\licencie\dispos-licencie-controller.js -c -m
echo COPY src\webapp\views\dispos\licencie\dispos-licencie-controller.php target\webapp\views\dispos\licencie\dispos-licencie-controller.php
COPY src\webapp\views\dispos\licencie\dispos-licencie-controller.php target\webapp\views\dispos\licencie\dispos-licencie-controller.php
echo COPY src\webapp\views\dispos\licencie\dispos-licencie.html.php target\webapp\views\dispos\licencie\dispos-licencie.html.php
COPY src\webapp\views\dispos\licencie\dispos-licencie.html.php target\webapp\views\dispos\licencie\dispos-licencie.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\dispos\licencie\dispos-licencie.less target\webapp\views\dispos\licencie\dispos-licencie.css
node ..\node_modules\less\bin\lessc src\webapp\views\dispos\licencie\dispos-licencie.less target\webapp\views\dispos\licencie\dispos-licencie.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\dispos\licencie\dispos-licencie.css > target\webapp\views\dispos\licencie\dispos-licencie.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\dispos\licencie\dispos-licencie.css > target\webapp\views\dispos\licencie\dispos-licencie.css.prod
echo DEL target\webapp\views\dispos\licencie\dispos-licencie.css
DEL target\webapp\views\dispos\licencie\dispos-licencie.css
echo MOVE target\webapp\views\dispos\licencie\dispos-licencie.css.prod target\webapp\views\dispos\licencie\dispos-licencie.css
MOVE target\webapp\views\dispos\licencie\dispos-licencie.css.prod target\webapp\views\dispos\licencie\dispos-licencie.css
echo MKDIR target\webapp\views\dispos\licencie\views
MKDIR target\webapp\views\dispos\licencie\views
echo MKDIR target\webapp\views\dispos\licencie\views\reponse-dispo
MKDIR target\webapp\views\dispos\licencie\views\reponse-dispo
echo COPY src\webapp\views\dispos\licencie\views\reponse-dispo\form-reponse-dispo-controller.php target\webapp\views\dispos\licencie\views\reponse-dispo\form-reponse-dispo-controller.php
COPY src\webapp\views\dispos\licencie\views\reponse-dispo\form-reponse-dispo-controller.php target\webapp\views\dispos\licencie\views\reponse-dispo\form-reponse-dispo-controller.php
echo COPY src\webapp\views\dispos\licencie\views\reponse-dispo\form-reponse-dispo.html.php target\webapp\views\dispos\licencie\views\reponse-dispo\form-reponse-dispo.html.php
COPY src\webapp\views\dispos\licencie\views\reponse-dispo\form-reponse-dispo.html.php target\webapp\views\dispos\licencie\views\reponse-dispo\form-reponse-dispo.html.php
echo MKDIR target\webapp\views\entrainements
MKDIR target\webapp\views\entrainements
echo COPY src\webapp\views\entrainements\entrainements-config.json target\webapp\views\entrainements\entrainements-config.json
COPY src\webapp\views\entrainements\entrainements-config.json target\webapp\views\entrainements\entrainements-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\entrainements\entrainements-controller.js -o target\webapp\views\entrainements\entrainements-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\entrainements\entrainements-controller.js -o target\webapp\views\entrainements\entrainements-controller.js -c -m
echo COPY src\webapp\views\entrainements\entrainements-controller.php target\webapp\views\entrainements\entrainements-controller.php
COPY src\webapp\views\entrainements\entrainements-controller.php target\webapp\views\entrainements\entrainements-controller.php
echo COPY src\webapp\views\entrainements\entrainements.html.php target\webapp\views\entrainements\entrainements.html.php
COPY src\webapp\views\entrainements\entrainements.html.php target\webapp\views\entrainements\entrainements.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\entrainements\entrainements.less target\webapp\views\entrainements\entrainements.css
node ..\node_modules\less\bin\lessc src\webapp\views\entrainements\entrainements.less target\webapp\views\entrainements\entrainements.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\entrainements\entrainements.css > target\webapp\views\entrainements\entrainements.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\entrainements\entrainements.css > target\webapp\views\entrainements\entrainements.css.prod
echo DEL target\webapp\views\entrainements\entrainements.css
DEL target\webapp\views\entrainements\entrainements.css
echo MOVE target\webapp\views\entrainements\entrainements.css.prod target\webapp\views\entrainements\entrainements.css
MOVE target\webapp\views\entrainements\entrainements.css.prod target\webapp\views\entrainements\entrainements.css
echo MKDIR target\webapp\views\error
MKDIR target\webapp\views\error
echo MKDIR target\webapp\views\error\400
MKDIR target\webapp\views\error\400
echo COPY src\webapp\views\error\400\error-400-config.json target\webapp\views\error\400\error-400-config.json
COPY src\webapp\views\error\400\error-400-config.json target\webapp\views\error\400\error-400-config.json
echo COPY src\webapp\views\error\400\error-400-controller.php target\webapp\views\error\400\error-400-controller.php
COPY src\webapp\views\error\400\error-400-controller.php target\webapp\views\error\400\error-400-controller.php
echo COPY src\webapp\views\error\400\error-400.html.php target\webapp\views\error\400\error-400.html.php
COPY src\webapp\views\error\400\error-400.html.php target\webapp\views\error\400\error-400.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\error\400\error-400.less target\webapp\views\error\400\error-400.css
node ..\node_modules\less\bin\lessc src\webapp\views\error\400\error-400.less target\webapp\views\error\400\error-400.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\error\400\error-400.css > target\webapp\views\error\400\error-400.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\error\400\error-400.css > target\webapp\views\error\400\error-400.css.prod
echo DEL target\webapp\views\error\400\error-400.css
DEL target\webapp\views\error\400\error-400.css
echo MOVE target\webapp\views\error\400\error-400.css.prod target\webapp\views\error\400\error-400.css
MOVE target\webapp\views\error\400\error-400.css.prod target\webapp\views\error\400\error-400.css
echo MKDIR target\webapp\views\error\401
MKDIR target\webapp\views\error\401
echo COPY src\webapp\views\error\401\error-401-config.json target\webapp\views\error\401\error-401-config.json
COPY src\webapp\views\error\401\error-401-config.json target\webapp\views\error\401\error-401-config.json
echo COPY src\webapp\views\error\401\error-401-controller.php target\webapp\views\error\401\error-401-controller.php
COPY src\webapp\views\error\401\error-401-controller.php target\webapp\views\error\401\error-401-controller.php
echo COPY src\webapp\views\error\401\error-401.html.php target\webapp\views\error\401\error-401.html.php
COPY src\webapp\views\error\401\error-401.html.php target\webapp\views\error\401\error-401.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\error\401\error-401.less target\webapp\views\error\401\error-401.css
node ..\node_modules\less\bin\lessc src\webapp\views\error\401\error-401.less target\webapp\views\error\401\error-401.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\error\401\error-401.css > target\webapp\views\error\401\error-401.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\error\401\error-401.css > target\webapp\views\error\401\error-401.css.prod
echo DEL target\webapp\views\error\401\error-401.css
DEL target\webapp\views\error\401\error-401.css
echo MOVE target\webapp\views\error\401\error-401.css.prod target\webapp\views\error\401\error-401.css
MOVE target\webapp\views\error\401\error-401.css.prod target\webapp\views\error\401\error-401.css
echo MKDIR target\webapp\views\error\403
MKDIR target\webapp\views\error\403
echo COPY src\webapp\views\error\403\error-403-config.json target\webapp\views\error\403\error-403-config.json
COPY src\webapp\views\error\403\error-403-config.json target\webapp\views\error\403\error-403-config.json
echo COPY src\webapp\views\error\403\error-403-controller.php target\webapp\views\error\403\error-403-controller.php
COPY src\webapp\views\error\403\error-403-controller.php target\webapp\views\error\403\error-403-controller.php
echo COPY src\webapp\views\error\403\error-403.html.php target\webapp\views\error\403\error-403.html.php
COPY src\webapp\views\error\403\error-403.html.php target\webapp\views\error\403\error-403.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\error\403\error-403.less target\webapp\views\error\403\error-403.css
node ..\node_modules\less\bin\lessc src\webapp\views\error\403\error-403.less target\webapp\views\error\403\error-403.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\error\403\error-403.css > target\webapp\views\error\403\error-403.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\error\403\error-403.css > target\webapp\views\error\403\error-403.css.prod
echo DEL target\webapp\views\error\403\error-403.css
DEL target\webapp\views\error\403\error-403.css
echo MOVE target\webapp\views\error\403\error-403.css.prod target\webapp\views\error\403\error-403.css
MOVE target\webapp\views\error\403\error-403.css.prod target\webapp\views\error\403\error-403.css
echo MKDIR target\webapp\views\error\404
MKDIR target\webapp\views\error\404
echo COPY src\webapp\views\error\404\error-404-config.json target\webapp\views\error\404\error-404-config.json
COPY src\webapp\views\error\404\error-404-config.json target\webapp\views\error\404\error-404-config.json
echo COPY src\webapp\views\error\404\error-404-controller.php target\webapp\views\error\404\error-404-controller.php
COPY src\webapp\views\error\404\error-404-controller.php target\webapp\views\error\404\error-404-controller.php
echo COPY src\webapp\views\error\404\error-404.html.php target\webapp\views\error\404\error-404.html.php
COPY src\webapp\views\error\404\error-404.html.php target\webapp\views\error\404\error-404.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\error\404\error-404.less target\webapp\views\error\404\error-404.css
node ..\node_modules\less\bin\lessc src\webapp\views\error\404\error-404.less target\webapp\views\error\404\error-404.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\error\404\error-404.css > target\webapp\views\error\404\error-404.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\error\404\error-404.css > target\webapp\views\error\404\error-404.css.prod
echo DEL target\webapp\views\error\404\error-404.css
DEL target\webapp\views\error\404\error-404.css
echo MOVE target\webapp\views\error\404\error-404.css.prod target\webapp\views\error\404\error-404.css
MOVE target\webapp\views\error\404\error-404.css.prod target\webapp\views\error\404\error-404.css
echo MKDIR target\webapp\views\home
MKDIR target\webapp\views\home
echo COPY src\webapp\views\home\home-config.json target\webapp\views\home\home-config.json
COPY src\webapp\views\home\home-config.json target\webapp\views\home\home-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\home\home-controller.js -o target\webapp\views\home\home-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\home\home-controller.js -o target\webapp\views\home\home-controller.js -c -m
echo COPY src\webapp\views\home\home-controller.php target\webapp\views\home\home-controller.php
COPY src\webapp\views\home\home-controller.php target\webapp\views\home\home-controller.php
echo COPY src\webapp\views\home\home.html.php target\webapp\views\home\home.html.php
COPY src\webapp\views\home\home.html.php target\webapp\views\home\home.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\home\home.less target\webapp\views\home\home.css
node ..\node_modules\less\bin\lessc src\webapp\views\home\home.less target\webapp\views\home\home.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\home\home.css > target\webapp\views\home\home.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\home\home.css > target\webapp\views\home\home.css.prod
echo DEL target\webapp\views\home\home.css
DEL target\webapp\views\home\home.css
echo MOVE target\webapp\views\home\home.css.prod target\webapp\views\home\home.css
MOVE target\webapp\views\home\home.css.prod target\webapp\views\home\home.css
echo MKDIR target\webapp\views\home\sub-menu
MKDIR target\webapp\views\home\sub-menu
echo COPY src\webapp\views\home\sub-menu\sub-menu-home-controller.php target\webapp\views\home\sub-menu\sub-menu-home-controller.php
COPY src\webapp\views\home\sub-menu\sub-menu-home-controller.php target\webapp\views\home\sub-menu\sub-menu-home-controller.php
echo COPY src\webapp\views\home\sub-menu\sub-menu-home.html.php target\webapp\views\home\sub-menu\sub-menu-home.html.php
COPY src\webapp\views\home\sub-menu\sub-menu-home.html.php target\webapp\views\home\sub-menu\sub-menu-home.html.php
echo MKDIR target\webapp\views\inscription
MKDIR target\webapp\views\inscription
echo COPY src\webapp\views\inscription\inscription-config.json target\webapp\views\inscription\inscription-config.json
COPY src\webapp\views\inscription\inscription-config.json target\webapp\views\inscription\inscription-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\inscription\inscription-controller.js -o target\webapp\views\inscription\inscription-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\inscription\inscription-controller.js -o target\webapp\views\inscription\inscription-controller.js -c -m
echo COPY src\webapp\views\inscription\inscription-controller.php target\webapp\views\inscription\inscription-controller.php
COPY src\webapp\views\inscription\inscription-controller.php target\webapp\views\inscription\inscription-controller.php
echo COPY src\webapp\views\inscription\inscription.html.php target\webapp\views\inscription\inscription.html.php
COPY src\webapp\views\inscription\inscription.html.php target\webapp\views\inscription\inscription.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\inscription\inscription.less target\webapp\views\inscription\inscription.css
node ..\node_modules\less\bin\lessc src\webapp\views\inscription\inscription.less target\webapp\views\inscription\inscription.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\inscription\inscription.css > target\webapp\views\inscription\inscription.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\inscription\inscription.css > target\webapp\views\inscription\inscription.css.prod
echo DEL target\webapp\views\inscription\inscription.css
DEL target\webapp\views\inscription\inscription.css
echo MOVE target\webapp\views\inscription\inscription.css.prod target\webapp\views\inscription\inscription.css
MOVE target\webapp\views\inscription\inscription.css.prod target\webapp\views\inscription\inscription.css
echo MKDIR target\webapp\views\medias
MKDIR target\webapp\views\medias
echo MKDIR target\webapp\views\medias\hhsj
MKDIR target\webapp\views\medias\hhsj
echo COPY src\webapp\views\medias\hhsj\medias-hhsj-config.json target\webapp\views\medias\hhsj\medias-hhsj-config.json
COPY src\webapp\views\medias\hhsj\medias-hhsj-config.json target\webapp\views\medias\hhsj\medias-hhsj-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\medias\hhsj\medias-hhsj-controller.js -o target\webapp\views\medias\hhsj\medias-hhsj-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\medias\hhsj\medias-hhsj-controller.js -o target\webapp\views\medias\hhsj\medias-hhsj-controller.js -c -m
echo COPY src\webapp\views\medias\hhsj\medias-hhsj-controller.php target\webapp\views\medias\hhsj\medias-hhsj-controller.php
COPY src\webapp\views\medias\hhsj\medias-hhsj-controller.php target\webapp\views\medias\hhsj\medias-hhsj-controller.php
echo COPY src\webapp\views\medias\hhsj\medias-hhsj.html.php target\webapp\views\medias\hhsj\medias-hhsj.html.php
COPY src\webapp\views\medias\hhsj\medias-hhsj.html.php target\webapp\views\medias\hhsj\medias-hhsj.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\medias\hhsj\medias-hhsj.less target\webapp\views\medias\hhsj\medias-hhsj.css
node ..\node_modules\less\bin\lessc src\webapp\views\medias\hhsj\medias-hhsj.less target\webapp\views\medias\hhsj\medias-hhsj.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\medias\hhsj\medias-hhsj.css > target\webapp\views\medias\hhsj\medias-hhsj.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\medias\hhsj\medias-hhsj.css > target\webapp\views\medias\hhsj\medias-hhsj.css.prod
echo DEL target\webapp\views\medias\hhsj\medias-hhsj.css
DEL target\webapp\views\medias\hhsj\medias-hhsj.css
echo MOVE target\webapp\views\medias\hhsj\medias-hhsj.css.prod target\webapp\views\medias\hhsj\medias-hhsj.css
MOVE target\webapp\views\medias\hhsj\medias-hhsj.css.prod target\webapp\views\medias\hhsj\medias-hhsj.css
echo COPY src\webapp\views\medias\medias.html.php target\webapp\views\medias\medias.html.php
COPY src\webapp\views\medias\medias.html.php target\webapp\views\medias\medias.html.php
echo MKDIR target\webapp\views\medias\photos
MKDIR target\webapp\views\medias\photos
echo COPY src\webapp\views\medias\photos\medias-photos-config.json target\webapp\views\medias\photos\medias-photos-config.json
COPY src\webapp\views\medias\photos\medias-photos-config.json target\webapp\views\medias\photos\medias-photos-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\medias\photos\medias-photos-controller.js -o target\webapp\views\medias\photos\medias-photos-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\medias\photos\medias-photos-controller.js -o target\webapp\views\medias\photos\medias-photos-controller.js -c -m
echo COPY src\webapp\views\medias\photos\medias-photos-controller.php target\webapp\views\medias\photos\medias-photos-controller.php
COPY src\webapp\views\medias\photos\medias-photos-controller.php target\webapp\views\medias\photos\medias-photos-controller.php
echo COPY src\webapp\views\medias\photos\medias-photos.html.php target\webapp\views\medias\photos\medias-photos.html.php
COPY src\webapp\views\medias\photos\medias-photos.html.php target\webapp\views\medias\photos\medias-photos.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\medias\photos\medias-photos.less target\webapp\views\medias\photos\medias-photos.css
node ..\node_modules\less\bin\lessc src\webapp\views\medias\photos\medias-photos.less target\webapp\views\medias\photos\medias-photos.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\medias\photos\medias-photos.css > target\webapp\views\medias\photos\medias-photos.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\medias\photos\medias-photos.css > target\webapp\views\medias\photos\medias-photos.css.prod
echo DEL target\webapp\views\medias\photos\medias-photos.css
DEL target\webapp\views\medias\photos\medias-photos.css
echo MOVE target\webapp\views\medias\photos\medias-photos.css.prod target\webapp\views\medias\photos\medias-photos.css
MOVE target\webapp\views\medias\photos\medias-photos.css.prod target\webapp\views\medias\photos\medias-photos.css
echo MKDIR target\webapp\views\medias\presse
MKDIR target\webapp\views\medias\presse
echo COPY src\webapp\views\medias\presse\medias-presse-config.json target\webapp\views\medias\presse\medias-presse-config.json
COPY src\webapp\views\medias\presse\medias-presse-config.json target\webapp\views\medias\presse\medias-presse-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\medias\presse\medias-presse-controller.js -o target\webapp\views\medias\presse\medias-presse-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\medias\presse\medias-presse-controller.js -o target\webapp\views\medias\presse\medias-presse-controller.js -c -m
echo COPY src\webapp\views\medias\presse\medias-presse-controller.php target\webapp\views\medias\presse\medias-presse-controller.php
COPY src\webapp\views\medias\presse\medias-presse-controller.php target\webapp\views\medias\presse\medias-presse-controller.php
echo COPY src\webapp\views\medias\presse\medias-presse.html.php target\webapp\views\medias\presse\medias-presse.html.php
COPY src\webapp\views\medias\presse\medias-presse.html.php target\webapp\views\medias\presse\medias-presse.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\medias\presse\medias-presse.less target\webapp\views\medias\presse\medias-presse.css
node ..\node_modules\less\bin\lessc src\webapp\views\medias\presse\medias-presse.less target\webapp\views\medias\presse\medias-presse.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\medias\presse\medias-presse.css > target\webapp\views\medias\presse\medias-presse.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\medias\presse\medias-presse.css > target\webapp\views\medias\presse\medias-presse.css.prod
echo DEL target\webapp\views\medias\presse\medias-presse.css
DEL target\webapp\views\medias\presse\medias-presse.css
echo MOVE target\webapp\views\medias\presse\medias-presse.css.prod target\webapp\views\medias\presse\medias-presse.css
MOVE target\webapp\views\medias\presse\medias-presse.css.prod target\webapp\views\medias\presse\medias-presse.css
echo MKDIR target\webapp\views\medias\sub-menu
MKDIR target\webapp\views\medias\sub-menu
echo COPY src\webapp\views\medias\sub-menu\sub-menu-medias.html.php target\webapp\views\medias\sub-menu\sub-menu-medias.html.php
COPY src\webapp\views\medias\sub-menu\sub-menu-medias.html.php target\webapp\views\medias\sub-menu\sub-menu-medias.html.php
echo MKDIR target\webapp\views\medias\videos
MKDIR target\webapp\views\medias\videos
echo COPY src\webapp\views\medias\videos\medias-videos-config.json target\webapp\views\medias\videos\medias-videos-config.json
COPY src\webapp\views\medias\videos\medias-videos-config.json target\webapp\views\medias\videos\medias-videos-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\medias\videos\medias-videos-controller.js -o target\webapp\views\medias\videos\medias-videos-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\medias\videos\medias-videos-controller.js -o target\webapp\views\medias\videos\medias-videos-controller.js -c -m
echo COPY src\webapp\views\medias\videos\medias-videos-controller.php target\webapp\views\medias\videos\medias-videos-controller.php
COPY src\webapp\views\medias\videos\medias-videos-controller.php target\webapp\views\medias\videos\medias-videos-controller.php
echo COPY src\webapp\views\medias\videos\medias-videos.html.php target\webapp\views\medias\videos\medias-videos.html.php
COPY src\webapp\views\medias\videos\medias-videos.html.php target\webapp\views\medias\videos\medias-videos.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\medias\videos\medias-videos.less target\webapp\views\medias\videos\medias-videos.css
node ..\node_modules\less\bin\lessc src\webapp\views\medias\videos\medias-videos.less target\webapp\views\medias\videos\medias-videos.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\medias\videos\medias-videos.css > target\webapp\views\medias\videos\medias-videos.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\medias\videos\medias-videos.css > target\webapp\views\medias\videos\medias-videos.css.prod
echo DEL target\webapp\views\medias\videos\medias-videos.css
DEL target\webapp\views\medias\videos\medias-videos.css
echo MOVE target\webapp\views\medias\videos\medias-videos.css.prod target\webapp\views\medias\videos\medias-videos.css
MOVE target\webapp\views\medias\videos\medias-videos.css.prod target\webapp\views\medias\videos\medias-videos.css
echo MKDIR target\webapp\views\news
MKDIR target\webapp\views\news
echo MKDIR target\webapp\views\news\actu
MKDIR target\webapp\views\news\actu
echo COPY src\webapp\views\news\actu\news-actu-config.json target\webapp\views\news\actu\news-actu-config.json
COPY src\webapp\views\news\actu\news-actu-config.json target\webapp\views\news\actu\news-actu-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\news\actu\news-actu-controller.js -o target\webapp\views\news\actu\news-actu-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\news\actu\news-actu-controller.js -o target\webapp\views\news\actu\news-actu-controller.js -c -m
echo COPY src\webapp\views\news\actu\news-actu-controller.php target\webapp\views\news\actu\news-actu-controller.php
COPY src\webapp\views\news\actu\news-actu-controller.php target\webapp\views\news\actu\news-actu-controller.php
echo COPY src\webapp\views\news\actu\news-actu.html.php target\webapp\views\news\actu\news-actu.html.php
COPY src\webapp\views\news\actu\news-actu.html.php target\webapp\views\news\actu\news-actu.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\news\actu\news-actu.less target\webapp\views\news\actu\news-actu.css
node ..\node_modules\less\bin\lessc src\webapp\views\news\actu\news-actu.less target\webapp\views\news\actu\news-actu.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\news\actu\news-actu.css > target\webapp\views\news\actu\news-actu.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\news\actu\news-actu.css > target\webapp\views\news\actu\news-actu.css.prod
echo DEL target\webapp\views\news\actu\news-actu.css
DEL target\webapp\views\news\actu\news-actu.css
echo MOVE target\webapp\views\news\actu\news-actu.css.prod target\webapp\views\news\actu\news-actu.css
MOVE target\webapp\views\news\actu\news-actu.css.prod target\webapp\views\news\actu\news-actu.css
echo COPY src\webapp\views\news\news-config.json target\webapp\views\news\news-config.json
COPY src\webapp\views\news\news-config.json target\webapp\views\news\news-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\news\news-controller.js -o target\webapp\views\news\news-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\news\news-controller.js -o target\webapp\views\news\news-controller.js -c -m
echo COPY src\webapp\views\news\news-controller.php target\webapp\views\news\news-controller.php
COPY src\webapp\views\news\news-controller.php target\webapp\views\news\news-controller.php
echo COPY src\webapp\views\news\news.html.php target\webapp\views\news\news.html.php
COPY src\webapp\views\news\news.html.php target\webapp\views\news\news.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\news\news.less target\webapp\views\news\news.css
node ..\node_modules\less\bin\lessc src\webapp\views\news\news.less target\webapp\views\news\news.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\news\news.css > target\webapp\views\news\news.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\news\news.css > target\webapp\views\news\news.css.prod
echo DEL target\webapp\views\news\news.css
DEL target\webapp\views\news\news.css
echo MOVE target\webapp\views\news\news.css.prod target\webapp\views\news\news.css
MOVE target\webapp\views\news\news.css.prod target\webapp\views\news\news.css
echo MKDIR target\webapp\views\shop
MKDIR target\webapp\views\shop
echo MKDIR target\webapp\views\shop\billetterie
MKDIR target\webapp\views\shop\billetterie
echo MKDIR target\webapp\views\shop\billetterie\modals
MKDIR target\webapp\views\shop\billetterie\modals
echo MKDIR target\webapp\views\shop\billetterie\modals\save-places-panier
MKDIR target\webapp\views\shop\billetterie\modals\save-places-panier
echo COPY src\webapp\views\shop\billetterie\modals\save-places-panier\modal-save-places-panier-content.html.php target\webapp\views\shop\billetterie\modals\save-places-panier\modal-save-places-panier-content.html.php
COPY src\webapp\views\shop\billetterie\modals\save-places-panier\modal-save-places-panier-content.html.php target\webapp\views\shop\billetterie\modals\save-places-panier\modal-save-places-panier-content.html.php
echo COPY src\webapp\views\shop\billetterie\modals\save-places-panier\modal-save-places-panier-controller.php target\webapp\views\shop\billetterie\modals\save-places-panier\modal-save-places-panier-controller.php
COPY src\webapp\views\shop\billetterie\modals\save-places-panier\modal-save-places-panier-controller.php target\webapp\views\shop\billetterie\modals\save-places-panier\modal-save-places-panier-controller.php
echo COPY src\webapp\views\shop\billetterie\modals\save-places-panier\modal-save-places-panier.html.php target\webapp\views\shop\billetterie\modals\save-places-panier\modal-save-places-panier.html.php
COPY src\webapp\views\shop\billetterie\modals\save-places-panier\modal-save-places-panier.html.php target\webapp\views\shop\billetterie\modals\save-places-panier\modal-save-places-panier.html.php
echo COPY src\webapp\views\shop\billetterie\shop-billetterie-config.json target\webapp\views\shop\billetterie\shop-billetterie-config.json
COPY src\webapp\views\shop\billetterie\shop-billetterie-config.json target\webapp\views\shop\billetterie\shop-billetterie-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\shop\billetterie\shop-billetterie-controller.js -o target\webapp\views\shop\billetterie\shop-billetterie-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\shop\billetterie\shop-billetterie-controller.js -o target\webapp\views\shop\billetterie\shop-billetterie-controller.js -c -m
echo COPY src\webapp\views\shop\billetterie\shop-billetterie-controller.php target\webapp\views\shop\billetterie\shop-billetterie-controller.php
COPY src\webapp\views\shop\billetterie\shop-billetterie-controller.php target\webapp\views\shop\billetterie\shop-billetterie-controller.php
echo COPY src\webapp\views\shop\billetterie\shop-billetterie.html.php target\webapp\views\shop\billetterie\shop-billetterie.html.php
COPY src\webapp\views\shop\billetterie\shop-billetterie.html.php target\webapp\views\shop\billetterie\shop-billetterie.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\shop\billetterie\shop-billetterie.less target\webapp\views\shop\billetterie\shop-billetterie.css
node ..\node_modules\less\bin\lessc src\webapp\views\shop\billetterie\shop-billetterie.less target\webapp\views\shop\billetterie\shop-billetterie.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\shop\billetterie\shop-billetterie.css > target\webapp\views\shop\billetterie\shop-billetterie.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\shop\billetterie\shop-billetterie.css > target\webapp\views\shop\billetterie\shop-billetterie.css.prod
echo DEL target\webapp\views\shop\billetterie\shop-billetterie.css
DEL target\webapp\views\shop\billetterie\shop-billetterie.css
echo MOVE target\webapp\views\shop\billetterie\shop-billetterie.css.prod target\webapp\views\shop\billetterie\shop-billetterie.css
MOVE target\webapp\views\shop\billetterie\shop-billetterie.css.prod target\webapp\views\shop\billetterie\shop-billetterie.css
echo MKDIR target\webapp\views\shop\boutique
MKDIR target\webapp\views\shop\boutique
echo MKDIR target\webapp\views\shop\boutique\modals
MKDIR target\webapp\views\shop\boutique\modals
echo MKDIR target\webapp\views\shop\boutique\modals\save-article-panier
MKDIR target\webapp\views\shop\boutique\modals\save-article-panier
echo COPY src\webapp\views\shop\boutique\modals\save-article-panier\modal-save-article-panier-content.html.php target\webapp\views\shop\boutique\modals\save-article-panier\modal-save-article-panier-content.html.php
COPY src\webapp\views\shop\boutique\modals\save-article-panier\modal-save-article-panier-content.html.php target\webapp\views\shop\boutique\modals\save-article-panier\modal-save-article-panier-content.html.php
echo COPY src\webapp\views\shop\boutique\modals\save-article-panier\modal-save-article-panier-controller.php target\webapp\views\shop\boutique\modals\save-article-panier\modal-save-article-panier-controller.php
COPY src\webapp\views\shop\boutique\modals\save-article-panier\modal-save-article-panier-controller.php target\webapp\views\shop\boutique\modals\save-article-panier\modal-save-article-panier-controller.php
echo COPY src\webapp\views\shop\boutique\modals\save-article-panier\modal-save-article-panier.html.php target\webapp\views\shop\boutique\modals\save-article-panier\modal-save-article-panier.html.php
COPY src\webapp\views\shop\boutique\modals\save-article-panier\modal-save-article-panier.html.php target\webapp\views\shop\boutique\modals\save-article-panier\modal-save-article-panier.html.php
echo COPY src\webapp\views\shop\boutique\shop-boutique-config.json target\webapp\views\shop\boutique\shop-boutique-config.json
COPY src\webapp\views\shop\boutique\shop-boutique-config.json target\webapp\views\shop\boutique\shop-boutique-config.json
echo node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\shop\boutique\shop-boutique-controller.js -o target\webapp\views\shop\boutique\shop-boutique-controller.js -c -m
node ..\node_modules\uglify-js\bin\uglifyjs src\webapp\views\shop\boutique\shop-boutique-controller.js -o target\webapp\views\shop\boutique\shop-boutique-controller.js -c -m
echo COPY src\webapp\views\shop\boutique\shop-boutique-controller.php target\webapp\views\shop\boutique\shop-boutique-controller.php
COPY src\webapp\views\shop\boutique\shop-boutique-controller.php target\webapp\views\shop\boutique\shop-boutique-controller.php
echo COPY src\webapp\views\shop\boutique\shop-boutique.html.php target\webapp\views\shop\boutique\shop-boutique.html.php
COPY src\webapp\views\shop\boutique\shop-boutique.html.php target\webapp\views\shop\boutique\shop-boutique.html.php
echo node ..\node_modules\less\bin\lessc src\webapp\views\shop\boutique\shop-boutique.less target\webapp\views\shop\boutique\shop-boutique.css
node ..\node_modules\less\bin\lessc src\webapp\views\shop\boutique\shop-boutique.less target\webapp\views\shop\boutique\shop-boutique.css
echo node ..\node_modules\cssmin\bin\cssmin target\webapp\views\shop\boutique\shop-boutique.css > target\webapp\views\shop\boutique\shop-boutique.css.prod
node ..\node_modules\cssmin\bin\cssmin target\webapp\views\shop\boutique\shop-boutique.css > target\webapp\views\shop\boutique\shop-boutique.css.prod
echo DEL target\webapp\views\shop\boutique\shop-boutique.css
DEL target\webapp\views\shop\boutique\shop-boutique.css
echo MOVE target\webapp\views\shop\boutique\shop-boutique.css.prod target\webapp\views\shop\boutique\shop-boutique.css
MOVE target\webapp\views\shop\boutique\shop-boutique.css.prod target\webapp\views\shop\boutique\shop-boutique.css
echo MKDIR target\webapp\views\shop\boutique\sub-menu
MKDIR target\webapp\views\shop\boutique\sub-menu
echo COPY src\webapp\views\shop\boutique\sub-menu\sub-menu-shop-boutique-controller.php target\webapp\views\shop\boutique\sub-menu\sub-menu-shop-boutique-controller.php
COPY src\webapp\views\shop\boutique\sub-menu\sub-menu-shop-boutique-controller.php target\webapp\views\shop\boutique\sub-menu\sub-menu-shop-boutique-controller.php
echo COPY src\webapp\views\shop\boutique\sub-menu\sub-menu-shop-boutique.html.php target\webapp\views\shop\boutique\sub-menu\sub-menu-shop-boutique.html.php
COPY src\webapp\views\shop\boutique\sub-menu\sub-menu-shop-boutique.html.php target\webapp\views\shop\boutique\sub-menu\sub-menu-shop-boutique.html.php
echo MKDIR target\webapp\views\shop\common
MKDIR target\webapp\views\shop\common
echo MKDIR target\webapp\views\shop\common\modals
MKDIR target\webapp\views\shop\common\modals
echo MKDIR target\webapp\views\shop\common\modals\panier
MKDIR target\webapp\views\shop\common\modals\panier
echo COPY src\webapp\views\shop\common\modals\panier\modal-panier-content.html.php target\webapp\views\shop\common\modals\panier\modal-panier-content.html.php
COPY src\webapp\views\shop\common\modals\panier\modal-panier-content.html.php target\webapp\views\shop\common\modals\panier\modal-panier-content.html.php
echo COPY src\webapp\views\shop\common\modals\panier\modal-panier-controller.php target\webapp\views\shop\common\modals\panier\modal-panier-controller.php
COPY src\webapp\views\shop\common\modals\panier\modal-panier-controller.php target\webapp\views\shop\common\modals\panier\modal-panier-controller.php
echo COPY src\webapp\views\shop\common\modals\panier\modal-panier.html.php target\webapp\views\shop\common\modals\panier\modal-panier.html.php
COPY src\webapp\views\shop\common\modals\panier\modal-panier.html.php target\webapp\views\shop\common\modals\panier\modal-panier.html.php
echo MKDIR target\webapp\views\shop\common\nav
MKDIR target\webapp\views\shop\common\nav
echo COPY src\webapp\views\shop\common\nav\nav.html.php target\webapp\views\shop\common\nav\nav.html.php
COPY src\webapp\views\shop\common\nav\nav.html.php target\webapp\views\shop\common\nav\nav.html.php
echo COPY src\webapp\views\shop\shop.html.php target\webapp\views\shop\shop.html.php
COPY src\webapp\views\shop\shop.html.php target\webapp\views\shop\shop.html.php
