@echo off

set PHP_PROGRAM="C:\wamp64\bin\php\php7.4.9\php.exe"
set NODE_PROGRAM="node"
set BUILD_PHP_FILE=build.php
set BUILD_DIR=build
set SRC_DIR=src
set TARGET_DIR=target

if exist %TARGET_DIR% (
	rmdir /s /q %TARGET_DIR%
)

mkdir %TARGET_DIR%

call %PHP_PROGRAM% %BUILD_DIR%\%BUILD_PHP_FILE% %BUILD_DIR% %SRC_DIR% %TARGET_DIR% %NODE_PROGRAM%
call %BUILD_DIR%\build.cmd